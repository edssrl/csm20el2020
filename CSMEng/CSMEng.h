// CSMEng.h : main header file for the CSMENG DLL
//

#if !defined(AFX_CSMENG_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_)
#define AFX_CSMENG_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCSMEngApp
// See CSMEng.cpp for the implementation of this class
//

class CCSMEngApp : public CWinApp
{
public:
	CCSMEngApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSMEngApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCSMEngApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSMENG_H__A0FB7A36_725A_11D2_A770_00C026A019B7__INCLUDED_)
