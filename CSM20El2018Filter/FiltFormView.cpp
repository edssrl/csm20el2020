// FiltFormView.cpp : file di implementazione
//

#include "stdafx.h"
#include "Csm20El2018Filter.h"
#include "Csm20El2018FilterDoc.h"
#include "FiltFormView.h"

// CFiltFormView

IMPLEMENT_DYNCREATE(CFiltFormView, CFormView)

CFiltFormView::CFiltFormView()
	: CFormView(CFiltFormView::IDD)
{

	m_destFolder = _T("");
	m_pinholeA = 0.0;
	m_pinholeB = 0.0;
	m_pinholeBig = 0.0;
	m_pinholeC = 0.0;
	m_pinholeD = 0.0;
	m_misUnit = 0;
	m_clearOutside = FALSE;

	//  m_ctrlLeftOffset = 0;
}

CFiltFormView::~CFiltFormView()
{

}

void CFiltFormView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DEST_FOLDER, m_ctrlDestFolder);
	DDX_Text(pDX, IDC_DEST_FOLDER, m_destFolder);
	DDX_Control(pDX, IDC_LISTCTRL_BOUNDARY, m_ctrlListBoundary);
	DDX_Control(pDX, IDC_MISUNIT_A, m_ctrlMisUnitA);
	DDX_Control(pDX, IDC_MISUNIT_B, m_ctrlMisUnitB);
	DDX_Control(pDX, IDC_MISUNIT_BIG, m_ctrlMisUnitBig);
	DDX_Control(pDX, IDC_MISUNIT_C, m_ctrlMisUnitC);
	DDX_Control(pDX, IDC_MISUNIT_D, m_ctrlMisUnitD);
	DDX_Control(pDX, IDC_NUM_PINHOLE_A, m_ctrlPinholeA);
	DDX_Text(pDX, IDC_NUM_PINHOLE_A, m_pinholeA);
	DDX_Control(pDX, IDC_NUM_PINHOLE_B, m_ctrlPinholeB);
	DDX_Text(pDX, IDC_NUM_PINHOLE_B, m_pinholeB);
	DDX_Control(pDX, IDC_NUM_PINHOLE_BIG, m_ctrlPinholeBig);
	DDX_Text(pDX, IDC_NUM_PINHOLE_BIG, m_pinholeBig);
	DDX_Control(pDX, IDC_NUM_PINHOLE_C, m_ctrlPinholeC);
	DDX_Text(pDX, IDC_NUM_PINHOLE_C, m_pinholeC);
	DDX_Control(pDX, IDC_NUM_PINHOLE_D, m_ctrlPinholeD);
	DDX_Text(pDX, IDC_NUM_PINHOLE_D, m_pinholeD);
	DDX_Radio(pDX, IDC_RADIO_MISUNIT1, m_misUnit);
	DDX_Control(pDX, IDC_STATIC_COILNAME, m_ctrlCoilName);
	DDX_Control(pDX, IDC_STATIC_LENGTH, m_ctrlLength);
	DDX_Control(pDX, IDC_STATIC_STATUS, m_ctrlStatus);
	DDX_Control(pDX, IDC_STATIC_WIDTH, m_ctrlWidth);
	DDX_Check(pDX, IDC_CHECK_CLEAR_OUTSIDE, m_clearOutside);
	DDX_Control(pDX, IDC_BUTTON_DELETEROW, m_ctrlButtonDeleteRow);
	DDX_Control(pDX, IDC_BUTTON_NEWROW, m_ctrlButtonNewRow);
	DDX_Control(pDX, IDC_GO_BUTTON, m_ctrlButtonGo);
	DDX_Control(pDX, IDC_STATIC_CLS_A, m_ctrlClsA);
	DDX_Control(pDX, IDC_STATIC_CLS_B, m_ctrlClsB);
	DDX_Control(pDX, IDC_STATIC_CLS_BIG, m_ctrlClsBig);
	DDX_Control(pDX, IDC_STATIC_CLS_C, m_ctrlClsC);
	DDX_Control(pDX, IDC_STATIC_CLS_D, m_ctrlClsD);
	DDX_Control(pDX, IDC_STATIC_DENS_A, m_ctrlDensA);
	DDX_Control(pDX, IDC_STATIC_DENS_B, m_ctrlDensB);
	DDX_Control(pDX, IDC_STATIC_DENS_C, m_ctrlDensC);
	DDX_Control(pDX, IDC_STATIC_DENS_D, m_ctrlDensD);
	DDX_Control(pDX, IDC_STATIC_LEFT_OFFSET, m_ctrlLeftOffset);
}

BEGIN_MESSAGE_MAP(CFiltFormView, CFormView)
ON_BN_CLICKED(IDC_BROWSE_BUTTON, &CFiltFormView::OnClickedBrowseButton)
ON_BN_CLICKED(IDC_GO_BUTTON, &CFiltFormView::OnClickedGoButton)
ON_COMMAND(IDC_RADIO_MISUNIT1, &CFiltFormView::OnRadioMisunit1)
ON_COMMAND(IDC_RADIO_MISUNIT2, &CFiltFormView::OnRadioMisunit2)
ON_COMMAND(IDC_RADIO_MISUNIT3, &CFiltFormView::OnRadioMisunit3)
ON_BN_CLICKED(IDC_BUTTON_NEWROW, &CFiltFormView::OnClickedButtonNewrow)
ON_BN_CLICKED(IDC_BUTTON_DELETEROW, &CFiltFormView::OnClickedButtonDeleterow)
ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFiltFormView::OnFilePrintPreview)
ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, &CFiltFormView::OnUpdateFilePrintPreview)
ON_BN_CLICKED(IDC_BUTTON_SAVE, &CFiltFormView::OnClickedButtonSave)
END_MESSAGE_MAP()


// diagnostica di CFiltFormView

#ifdef _DEBUG
void CFiltFormView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CFiltFormView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


//------------------------------
bool CFiltFormView::enableBoundarySelection(bool enable)
{

m_ctrlListBoundary.EnableWindow(enable);
m_ctrlButtonDeleteRow.EnableWindow(enable);
m_ctrlButtonNewRow.EnableWindow(enable);
m_ctrlButtonGo.EnableWindow(enable);

return enable;
}



// gestori di messaggi CFiltFormView


void CFiltFormView::OnClickedBrowseButton()
{
UpdateData(TRUE);

CFolderPickerDialog folderPickerDialog(m_destFolder, OFN_FILEMUSTEXIST | OFN_ENABLESIZING, this,
        sizeof(OPENFILENAME));

CString folderPath;

if (folderPickerDialog.DoModal() == IDOK)
    {
    POSITION pos = folderPickerDialog.GetStartPosition();
    if (pos)
		{
        m_destFolder = folderPickerDialog.GetNextPathName(pos);
		CProfile profile;
		profile.writeProfileString(_T("ReportFolder"),_T("SaveOriginalReportFolder"), m_destFolder);

		UpdateData(FALSE);
        }
    }


}


//void CFiltFormView::OnClickedCheckWholeCoil()
//{
//	// TODO: aggiungere qui il codice per la gestione della notifica del controllo.
//}



void CFiltFormView::OnClickedGoButton()
{
CCsm20El2018FilterDoc* pDoc;
pDoc = (CCsm20El2018FilterDoc*) GetDocument();



//	1) check document loaded
//	2) check boundary set
VectorBounds vb;
RectBound rb;
// vettore di couple per controllo overlapping
vector < std::pair<int, long> > sortVect;
std::pair<int, long> couple;	// coppia top/id in vb

// fill vector with bounds
for (int i=0;i<m_ctrlListBoundary.GetItemCount();i++)
	{
	rb.c_left = pDoc->getRelativePos(m_ctrlListBoundary.getItemDouble(i,1));
	rb.c_top = m_ctrlListBoundary.getItemDouble(i,2);
	rb.c_right = pDoc->getRelativePos(m_ctrlListBoundary.getItemDouble(i,3));
	rb.c_bottom = m_ctrlListBoundary.getItemDouble(i,4);
	rb.c_clsA = _tstof((LPCTSTR)m_ctrlListBoundary.GetItemText(i,5));
	rb.c_clsB = _tstof((LPCTSTR)m_ctrlListBoundary.GetItemText(i,6));
	rb.c_clsC = _tstof((LPCTSTR)m_ctrlListBoundary.GetItemText(i,7));
	rb.c_clsD = _tstof((LPCTSTR)m_ctrlListBoundary.GetItemText(i,8));
	rb.c_clsBig = _tstof((LPCTSTR)m_ctrlListBoundary.GetItemText(i,9));

	if (!rb.checkLimit(pDoc->getRelativePos(0),pDoc->getRelativePos(pDoc->c_larghezza),0.,pDoc->c_difCoil.getMeter()))
		{
		CString s;
		s.Format(_T("Values on row n.%d are ouside limits"),i+1);
		AfxMessageBox(s,MB_ICONERROR);
		return;
		}
	else
		{
		vb.push_back(rb);
		couple.first = rb.c_top;
		couple.second = i;
		sortVect.push_back(couple);
		}
	}
if((vb.size()==0)||(sortVect.size()==0))
	{
	CString s;
	s.Format(_T("Boundary region is empty, please insert a boundary region")); 
	AfxMessageBox(s,MB_ICONERROR);
	return;
	}


// check overlapping block on longitudinal length
// sort vectorBound on top position sulla base di first
std::sort(sortVect.begin(),sortVect.end());
// loop per check overlapping
// top(i+1) > bottom (i)
for (int i=0;i<(sortVect.size()-1);i++)
	{
	if (vb[sortVect[i].second].c_bottom > sortVect[i+1].first )
		{
		CString s;
		s.Format(_T("Boundary region are overlapping in length, check top (%5.0lf) on row %d and bottom (%5.0lf) on row %d"),
			vb[sortVect[i+1].second].c_top,sortVect[i+1].second+1,
			vb[sortVect[i].second].c_bottom,sortVect[i].second+1); 
		AfxMessageBox(s,MB_ICONERROR);
		return;
		}
	}

// update parameter from dialog
UpdateData(TRUE);
// check if clearOutside=on -> single boundary region
if (m_clearOutside && (vb.size() > 1))
	{
	CString s;
	s.Format(_T("The option clear outside is available only in single region mode, check number of region (%d)"),
		vb.size()); 
	AfxMessageBox(s,MB_ICONERROR);
	return;
	}

setStatus(_T("Filtering..... please wait"));
CWaitCursor wait;
FilterParam fp;
fp.c_clearOutside = m_clearOutside;
fp.c_filterType = m_misUnit; // il tipo di unita` di misura indica il tipo di filtraggio
fp.c_limitA = m_pinholeA;
fp.c_limitB = m_pinholeB;
fp.c_limitC = m_pinholeC;
fp.c_limitD = m_pinholeD;
fp.c_limitBig = m_pinholeBig;

if (pDoc->applyFilter(vb,fp))
	{
	setStatus(_T("OK Filter"));
	}
else
	{
	setStatus( _T("Impossible to apply Filter"));
	}

// update pinhole counter on dialog
updatePinholeCnt();

for (int i=0;i<m_ctrlListBoundary.GetItemCount();i++)
	m_ctrlListBoundary.updatePinholeValues(i);
wait.Restore();

}



void CFiltFormView::updatePinholeCnt(void)
{
CCsm20El2018FilterDoc* pDoc;
pDoc = (CCsm20El2018FilterDoc*) GetDocument();

CString s;
// cls A
long numPinhole = pDoc->c_difCoil.getTotDifetti('A',0,(int)pDoc->c_difCoil.getMeter());
s.Format(_T("Cls.A=%d [h]"),numPinhole);
m_ctrlClsA = s;
// cls B
numPinhole = pDoc->c_difCoil.getTotDifetti('B',0,(int)pDoc->c_difCoil.getMeter());
s.Format(_T("Cls.B=%d [h]"),numPinhole);
m_ctrlClsB = s;
// cls C
numPinhole = pDoc->c_difCoil.getTotDifetti('C',0,(int)pDoc->c_difCoil.getMeter());
s.Format(_T("Cls.C=%d [h]"),numPinhole);
m_ctrlClsC = s;
// cls D
numPinhole = pDoc->c_difCoil.getTotDifetti('D',0,(int)pDoc->c_difCoil.getMeter());
s.Format(_T("Cls.D=%d [h]"),numPinhole);
m_ctrlClsD = s;
// cls Big
numPinhole = pDoc->c_difCoil.getTotBigHole(0,(int)pDoc->c_difCoil.getMeter());
s.Format(_T("Cls.Big=%d [h]"),numPinhole);
m_ctrlClsBig = s;

// cls A
double densPinhole = pDoc->c_difCoil.getTotalDensity('A');
s.Format(_T("Dens.A=%4.04lf [h/m2]"),densPinhole);
m_ctrlDensA = s;
// cls B
densPinhole = pDoc->c_difCoil.getTotalDensity('B');
s.Format(_T("Dens.B=%4.04lf [h/m2]"),densPinhole);
m_ctrlDensB = s;
// cls C
densPinhole = pDoc->c_difCoil.getTotalDensity('C');
s.Format(_T("Dens.C=%4.04lf [h/m2]"),densPinhole);
m_ctrlDensC = s;
// cls D
densPinhole = pDoc->c_difCoil.getTotalDensity('D');
s.Format(_T("Dens.D=%4.04lf [h/m2]"),densPinhole);
m_ctrlDensD = s;

}


void CFiltFormView::OnInitialUpdate()
{
CProfile profile;
CFormView::OnInitialUpdate();
OnRadioMisunit1();

m_ctrlListBoundary.setPDoc((CLineDoc*) GetDocument());
setLocalPDoc(GetDocument()); 

m_ctrlListBoundary.LoadColumnHeadings(ID_BOUNDARY_LIST_HEADER);
// Disabilita-Abilita edit mode per tutta listCtrl
m_ctrlListBoundary.modeEdit(true);

//------------------------
m_ctrlCoilName.SetFont(_T("Arial"),120);
m_ctrlCoilName.m_bAutoUpdate = true;
setCoilName(_T(""));
m_ctrlLength.SetFont(_T("Arial"),140);
m_ctrlLength.m_bAutoUpdate = true;
setLength(-1.0);
m_ctrlStatus.SetFont(_T("Arial"),140);
m_ctrlStatus.m_bAutoUpdate = true;
setStatus(_T(""));
m_ctrlWidth.SetFont(_T("Arial"),140);
m_ctrlWidth.m_bAutoUpdate = true;
setWidth(-1.0);
m_ctrlLeftOffset.SetFont(_T("Arial"),90);
m_ctrlLeftOffset.m_bAutoUpdate = true;
setLeftOffset(-1.0);

// contatori
m_ctrlClsA.SetFont(_T("Arial"),100);
m_ctrlClsA.m_bAutoUpdate = true;
m_ctrlClsB.SetFont(_T("Arial"),100);
m_ctrlClsB.m_bAutoUpdate = true;
m_ctrlClsC.SetFont(_T("Arial"),100);
m_ctrlClsC.m_bAutoUpdate = true;
m_ctrlClsD.SetFont(_T("Arial"),100);
m_ctrlClsD.m_bAutoUpdate = true;
m_ctrlClsBig.SetFont(_T("Arial"),100);
m_ctrlClsBig.m_bAutoUpdate = true;

// densita
m_ctrlDensA.SetFont(_T("Arial"),100);
m_ctrlDensA.m_bAutoUpdate = true;
m_ctrlDensB.SetFont(_T("Arial"),100);
m_ctrlDensB.m_bAutoUpdate = true;
m_ctrlDensC.SetFont(_T("Arial"),100);
m_ctrlDensC.m_bAutoUpdate = true;
m_ctrlDensD.SetFont(_T("Arial"),100);
m_ctrlDensD.m_bAutoUpdate = true;

// update pinhole counter on dialog
updatePinholeCnt();

m_destFolder = profile.getProfileString(_T("ReportFolder"),_T("SaveOriginalReportFolder"),_T(""));

UpdateData(FALSE);
enableBoundarySelection(false);
}


void CFiltFormView::OnRadioMisunit1()
{
setMisUnit(MISUNIT_HM2);
}


void CFiltFormView::OnRadioMisunit2()
{
setMisUnit(MISUNIT_HM);
}


void CFiltFormView::OnRadioMisunit3()
{
setMisUnit(MISUNIT_H);
}

void CFiltFormView::setMisUnit(int misUnitId)
{
CString sMisUnit;

switch(misUnitId)
	{
	default:
	case MISUNIT_HM2:
		sMisUnit =_T("pinhole/m2");
	break;
	case MISUNIT_HM:
		sMisUnit =_T("pinhole/m");
	break;
	case MISUNIT_H:
		sMisUnit =_T("pinhole");
	break;
	}

m_ctrlMisUnitA.SetWindowText(sMisUnit);	
m_ctrlMisUnitB.SetWindowText(sMisUnit);	
m_ctrlMisUnitC.SetWindowText(sMisUnit);	
m_ctrlMisUnitD.SetWindowText(sMisUnit);	
m_ctrlMisUnitBig.SetWindowText(sMisUnit);	

}

void CFiltFormView::OnClickedButtonNewrow()
{
CCsm20El2018FilterDoc* pDoc;
pDoc = (CCsm20El2018FilterDoc*) GetDocument();

// TODO: aggiungere qui il codice per la gestione della notifica del controllo.
int numRow = m_ctrlListBoundary.GetItemCount();
CString s;
s.Format(_T("%d"),numRow+1);
//Id
m_ctrlListBoundary.AddRow(s.GetString());
// left
m_ctrlListBoundary.AddItem(numRow,1,0);
// top
m_ctrlListBoundary.AddItem(numRow,2,0);
// right
m_ctrlListBoundary.AddItem(numRow,3,(int)pDoc->c_larghezza);
// bottom
m_ctrlListBoundary.AddItem(numRow,4,(int)pDoc->getVTotalLength());

// fill cls.(A - Big) values in rectangle
int a,b,c,d,big;
a = (int) pDoc->c_difCoil.getTotDifetti('A',0,pDoc->getVTotalLength(),0,pDoc->c_larghezza/SIZE_BANDE);
b = (int) pDoc->c_difCoil.getTotDifetti('B',0,pDoc->getVTotalLength(),0,pDoc->c_larghezza/SIZE_BANDE);
c = (int) pDoc->c_difCoil.getTotDifetti('C',0,pDoc->getVTotalLength(),0,pDoc->c_larghezza/SIZE_BANDE);
d = (int) pDoc->c_difCoil.getTotDifetti('D',0,pDoc->getVTotalLength(),0,pDoc->c_larghezza/SIZE_BANDE);
big = (int) pDoc->c_difCoil.getTotBigHole(0,pDoc->getVTotalLength(),0,pDoc->c_larghezza/SIZE_BANDE);

m_ctrlListBoundary.AddItem(numRow,5,a);
m_ctrlListBoundary.AddItem(numRow,6,b);
m_ctrlListBoundary.AddItem(numRow,7,c);
m_ctrlListBoundary.AddItem(numRow,8,d);
m_ctrlListBoundary.AddItem(numRow,9,big);
}


void CFilterBoundaryListCtrl::endCellEdit(int row, int col, LPCTSTR itemText)
{
updatePinholeValues(row);
}

void CFilterBoundaryListCtrl::updatePinholeValues(int row)
{
// get Boundary
int left = (int) getItemDouble(row,1)/SIZE_BANDE;
int top = (int) getItemDouble(row,2);
int right = (int) getItemDouble(row,3)/SIZE_BANDE;
int bottom = (int) getItemDouble(row,4);

// fill cls.(A - Big) values in rectangle
int a,b,c,d,big;
a = (int) c_pDoc->c_difCoil.getTotDifetti('A',top,bottom,left,right);
b = (int) c_pDoc->c_difCoil.getTotDifetti('B',top,bottom,left,right);
c = (int) c_pDoc->c_difCoil.getTotDifetti('C',top,bottom,left,right);
d = (int) c_pDoc->c_difCoil.getTotDifetti('D',top,bottom,left,right);
big = (int) c_pDoc->c_difCoil.getTotBigHole(top,bottom,left,right);

AddItem(row,5,a);
AddItem(row,6,b);
AddItem(row,7,c);
AddItem(row,8,d);
AddItem(row,9,big);
}

void CFiltFormView::OnClickedButtonDeleterow()
{
// TODO: aggiungere qui il codice per la gestione della notifica del controllo.

POSITION pos = m_ctrlListBoundary.GetFirstSelectedItemPosition();

if (pos == NULL)
	{
    TRACE(_T("No items were selected!\n"));
    return;
	}
else
    {
    while (pos)
	   {
	   pos = m_ctrlListBoundary.GetFirstSelectedItemPosition(); 
       int nItem = m_ctrlListBoundary.GetNextSelectedItem(pos);
	   m_ctrlListBoundary.DeleteItem(nItem);
	   }
	}
}


void CFiltFormView::OnFilePrintPreview()
{

CInit::onFilePrintPreview();

}

void CFiltFormView::OnUpdateFilePrintPreview(CCmdUI *pCmdUI)
{
CLineDoc* pDoc = (CLineDoc* )GetDocument();
pCmdUI->Enable(pDoc->loadedData);
}


void CFiltFormView::OnClickedButtonSave()
{
// TODO: aggiungere qui il codice per la gestione della notifica del controllo.
CCsm20El2018FilterDoc* pDoc;
pDoc = (CCsm20El2018FilterDoc*) GetDocument();

CFileBpe fp;
CString filePathName;

#ifdef _DEBUG
// save in local folder
filePathName = m_destFolder;
filePathName += _T("/");
// add DD folder
filePathName += pDoc->getMonthFolder();
filePathName += _T("/");
filePathName += pDoc->getFileName();
filePathName += _T(".csm");
#else
filePathName = pDoc->c_originalPathName;

#endif

if (fp.Open(filePathName.GetString(),CFile::modeCreate | CFile::modeWrite))
	{
	pDoc->save(&fp);
	fp.Close();
	setStatus(_T("OK: file Saved"));
	}
else
	setStatus(_T("FAIL: file not Saved"));
}


BOOL CFiltFormView::OnPreparePrinting(CPrintInfo* pInfo)
{

// TODO:  chiamare DoPreparePrinting per richiamare la finestra di dialogo Stampa
return CInit::onPreparePrinting(pInfo);
}


void CFiltFormView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
// TODO: aggiungere qui il codice specifico e/o chiamare la classe base
CInit::onEndPrinting(pDC, pInfo);
}


void CFiltFormView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: aggiungere qui il codice specifico e/o chiamare la classe base
	CInit::onBeginPrinting(pDC, pInfo);
}


void CFiltFormView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
// TODO: aggiungere qui il codice specifico e/o chiamare la classe base
CInit::onPrint(pDC, pInfo);
}


void CFiltFormView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView)
{
	// TODO: aggiungere qui il codice specifico e/o chiamare la classe base
	CFormView::OnEndPrintPreview(pDC, pInfo, point, pView);

c_isPrinting = FALSE;
}


void CFiltFormView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{

// TODO: aggiungere qui il codice specifico e/o chiamare la classe base
if (pDC->IsPrinting())
	{
	CSize printSize;
	printSize.cx = pDC->GetDeviceCaps(HORZRES);
	printSize.cy = pDC->GetDeviceCaps(VERTRES);
	pDC->SetViewportExt(printSize.cx,printSize.cy);
	}

CFormView::OnPrepareDC(pDC, pInfo);
}

