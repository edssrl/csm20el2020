// DFolder.cpp : file di implementazione
//

#include "stdafx.h"
#include "Csm20El2018Filter.h"
#include "DFolder.h"
#include "afxdialogex.h"
#include "../Csm20El2013/Profile.h" 

// finestra di dialogo CDFolder

IMPLEMENT_DYNAMIC(CDFolder, CDialog)

CDFolder::CDFolder(CWnd* pParent /*=NULL*/)
	: CDialog(CDFolder::IDD, pParent)
{

	m_dbFolder1 = _T("");
	m_dbFolder2 = _T("");
	m_dbFolder3 = _T("");
	m_dbFolder4 = _T("");
	m_reportFolder1 = _T("");
	m_reportFolder2 = _T("");
	//  m_reportFolder4 = _T("");
	m_reportFolder3 = _T("");
	m_reportFolder4 = _T("");
}

CDFolder::~CDFolder()
{
}

void CDFolder::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_DB_REPORT1, m_dbFolder1);
	DDX_Text(pDX, IDC_DB_REPORT2, m_dbFolder2);
	DDX_Text(pDX, IDC_DB_REPORT3, m_dbFolder3);
	DDX_Text(pDX, IDC_DB_REPORT4, m_dbFolder4);
	DDX_Text(pDX, IDC_DIR_REPORT1, m_reportFolder1);
	DDX_Text(pDX, IDC_DIR_REPORT2, m_reportFolder2);
	DDX_Text(pDX, IDC_DIR_REPORT3, m_reportFolder3);
	DDX_Text(pDX, IDC_DIR_REPORT4, m_reportFolder4);
}


BEGIN_MESSAGE_MAP(CDFolder, CDialog)
	ON_BN_CLICKED(IDC_BROWSE_DB1, &CDFolder::OnClickedBrowseDb1)
	ON_BN_CLICKED(IDC_BROWSE_DB2, &CDFolder::OnClickedBrowseDb2)
	ON_BN_CLICKED(IDC_BROWSE_DB3, &CDFolder::OnClickedBrowseDb3)
	ON_BN_CLICKED(IDC_BROWSE_DB4, &CDFolder::OnClickedBrowseDb4)
	ON_BN_CLICKED(IDC_BROWSE_DIR1, &CDFolder::OnClickedBrowseDir1)
	ON_BN_CLICKED(IDC_BROWSE_DIR2, &CDFolder::OnClickedBrowseDir2)
	ON_BN_CLICKED(IDC_BROWSE_DIR3, &CDFolder::OnClickedBrowseDir3)
	ON_BN_CLICKED(IDC_BROWSE_DIR4, &CDFolder::OnClickedBrowseDir4)
END_MESSAGE_MAP()


void CDFolder::loadFromProfile(void)
{
CProfile profile;

m_reportFolder1 = profile.getProfileString(_T("ReportFolder"),_T("ReportFolder1"),_T("C:/User/eds/hole/Csm20El2013"));
m_reportFolder2 = profile.getProfileString(_T("ReportFolder"),_T("ReportFolder2"),_T("C:/User/eds/hole/Csm20El2013"));
m_reportFolder3 = profile.getProfileString(_T("ReportFolder"),_T("ReportFolder3"),_T("C:/User/eds/hole/Csm20El2013"));
m_reportFolder4 = profile.getProfileString(_T("ReportFolder"),_T("ReportFolder4"),_T("C:/User/eds/hole/Csm20El2013"));

m_dbFolder1 = profile.getProfileString(_T("ReportFolder"),_T("DbName1"),_T("./Dbase/hole.mdb"));
m_dbFolder2 = profile.getProfileString(_T("ReportFolder"),_T("DbName2"),_T("./Dbase/hole.mdb"));
m_dbFolder3 = profile.getProfileString(_T("ReportFolder"),_T("DbName3"),_T("./Dbase/hole.mdb"));
m_dbFolder4 = profile.getProfileString(_T("ReportFolder"),_T("DbName4"),_T("./Dbase/hole.mdb"));


}

void CDFolder::saveToProfile(void)
{
CProfile profile;

profile.writeProfileString(_T("ReportFolder"),_T("ReportFolder1"),m_reportFolder1);
profile.writeProfileString(_T("ReportFolder"),_T("ReportFolder2"),m_reportFolder2);
profile.writeProfileString(_T("ReportFolder"),_T("ReportFolder3"),m_reportFolder3);
profile.writeProfileString(_T("ReportFolder"),_T("ReportFolder4"),m_reportFolder4);

profile.writeProfileString(_T("ReportFolder"),_T("DbFolder1"),m_dbFolder1);
profile.writeProfileString(_T("ReportFolder"),_T("DbFolder2"),m_dbFolder2);
profile.writeProfileString(_T("ReportFolder"),_T("DbFolder3"),m_dbFolder3);
profile.writeProfileString(_T("ReportFolder"),_T("DbFolder4"),m_dbFolder4);

}

// gestori di messaggi CDFolder
CString CDFolder::SelectFolder(CString initialFolder)
{

CFolderPickerDialog folderPickerDialog(initialFolder, OFN_FILEMUSTEXIST | OFN_ENABLESIZING, this,
        sizeof(OPENFILENAME));

CString folderPath;

folderPath = _T("");
if (folderPickerDialog.DoModal() == IDOK)
    {
    POSITION pos = folderPickerDialog.GetStartPosition();
    folderPath = folderPickerDialog.GetNextPathName(pos);
    }


return folderPath;
}


void CDFolder::OnClickedBrowseDb1()
{
// select folder
CString folder;

folder = SelectFolder(m_dbFolder1);
if (folder != _T(""))
	{
	m_dbFolder1 = folder;
	UpdateData(FALSE);
	}
}


void CDFolder::OnClickedBrowseDb2()
{
// select folder
CString folder;

folder = SelectFolder(m_dbFolder2);
if (folder != _T(""))
	{
	m_dbFolder2 = folder;
	UpdateData(FALSE);
	}
}


void CDFolder::OnClickedBrowseDb3()
{
// select folder
CString folder;

folder = SelectFolder(m_dbFolder3);
if (folder != _T(""))
	{
	m_dbFolder3 = folder;
	UpdateData(FALSE);
	}
}


void CDFolder::OnClickedBrowseDb4()
{
// select folder
CString folder;

folder = SelectFolder(m_dbFolder4);
if (folder != _T(""))
	{
	m_dbFolder4 = folder;
	UpdateData(FALSE);
	}
}


void CDFolder::OnClickedBrowseDir1()
{
// select folder
CString folder;

folder = SelectFolder(m_reportFolder1);
if (folder != _T(""))
	{
	m_reportFolder1 = folder;
	UpdateData(FALSE);
	}

}


void CDFolder::OnClickedBrowseDir2()
{
// select folder
CString folder;

folder = SelectFolder(m_reportFolder2);
if (folder != _T(""))
	{
	m_reportFolder2 = folder;
	UpdateData(FALSE);
	}
}



void CDFolder::OnClickedBrowseDir3()
{
// select folder
CString folder;

folder = SelectFolder(m_reportFolder3);
if (folder != _T(""))
	{
	m_reportFolder3 = folder;
	UpdateData(FALSE);
	}
}


void CDFolder::OnClickedBrowseDir4()
{
// select folder
CString folder;

folder = SelectFolder(m_reportFolder4);
if (folder != _T(""))
	{
	m_reportFolder4 = folder;
	UpdateData(FALSE);
	}
}
