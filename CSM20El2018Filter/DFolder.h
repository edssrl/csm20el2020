#pragma once


// finestra di dialogo CDFolder

class CDFolder : public CDialog
{
	DECLARE_DYNAMIC(CDFolder)

public:
	CDFolder(CWnd* pParent = NULL);   // costruttore standard
	virtual ~CDFolder();

	void  loadFromProfile(void);
	void  saveToProfile(void);

	CString SelectFolder(CString fromFolder);


// Dati della finestra di dialogo
	enum { IDD = IDD_SEL_FOLDER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Supporto DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_dbFolder1;
	CString m_dbFolder2;
	CString m_dbFolder3;
	CString m_dbFolder4;
	CString m_reportFolder1;
	CString m_reportFolder2;
	CString m_reportFolder3;
	CString m_reportFolder4;
	afx_msg void OnClickedBrowseDb1();
	afx_msg void OnClickedBrowseDb2();
	afx_msg void OnClickedBrowseDb3();
	afx_msg void OnClickedBrowseDb4();
	afx_msg void OnClickedBrowseDir1();
	afx_msg void OnClickedBrowseDir2();
	afx_msg void OnClickedBrowseDir3();
	afx_msg void OnClickedBrowseDir4();

public:

	CString getReportFolder(int id)
		{switch(id){default:
		case 1: return m_reportFolder1;
		case 2: return m_reportFolder2;
		case 3: return m_reportFolder3;
		case 4: return m_reportFolder4;}};
	CString getDbFolder(int id)
		{switch(id){default:
		case 1: return m_dbFolder1;
		case 2: return m_dbFolder2;
		case 3: return m_dbFolder3;
		case 4: return m_dbFolder4;}};

};
