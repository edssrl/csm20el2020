// DirBar.cpp : implementation file
//

#include "stdafx.h"
#include "Csm20El2018Filter.h"
#include "DirBar.h"
#include "../Csm20El2013/Profile.h"
#include "DFolder.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDirBar dialog


CDirBar::CDirBar(void)
{
	//{{AFX_DATA_INIT(CDirBar)
	m_path1 = _T("");
	m_path2 = _T("");
	m_path3 = _T("");
	m_path4 = _T("");
	m_radio = 0;
	//}}AFX_DATA_INIT
}


void CDirBar::DoDataExchange(CDataExchange* pDX)
{
	CResizableDlgBar::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDirBar)
	DDX_Control(pDX, IDC_PATH1, m_ctrlPath1);
	DDX_Control(pDX, IDC_PATH2, m_ctrlPath2);
	DDX_Control(pDX, IDC_PATH3, m_ctrlPath3);
	DDX_Control(pDX, IDC_PATH4, m_ctrlPath4);
	DDX_Control(pDX, IDC_BROWSE1, m_ctrlBrowse1);
	DDX_Control(pDX, IDC_BROWSE2, m_ctrlBrowse2);
	DDX_Control(pDX, IDC_BROWSE3, m_ctrlBrowse3);
	DDX_Control(pDX, IDC_BROWSE4, m_ctrlBrowse4);
	DDX_Control(pDX, IDC_REPORT_LABEL, m_ctrlReportLabel);
	DDX_Control(pDX, IDC_FILE_LIDT, m_fileList);
	DDX_Control(pDX, IDC_DIR_TREE, m_directory);
	DDX_Control(pDX, IDC_RADIO1, m_ctrlRadio1);
	DDX_Control(pDX, IDC_RADIO2, m_ctrlRadio2);
	DDX_Control(pDX, IDC_RADIO3, m_ctrlRadio3);
	DDX_Control(pDX, IDC_RADIO4, m_ctrlRadio4);
	DDX_Text(pDX, IDC_PATH1, m_path1);
	DDX_Text(pDX, IDC_PATH2, m_path2);
	DDX_Text(pDX, IDC_PATH3, m_path3);
	DDX_Text(pDX, IDC_PATH4, m_path4);
	DDX_Radio(pDX, IDC_RADIO1, m_radio);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDirBar, CResizableDlgBar)
	//{{AFX_MSG_MAP(CDirBar)
	ON_NOTIFY(TVN_SELCHANGED, IDC_DIR_TREE, OnSelchangedDirTree)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_RADIO4, OnRadio4)
	//}}AFX_MSG_MAP
    ON_MESSAGE(WM_INITDIALOG, OnInitDialog )    // <-- Add this line.
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDirBar message handlers


CSize CDirBar::CalcDynamicLayout(int nLength, DWORD dwMode)
   {
   
	CSize sz = CResizableDlgBar::CalcDynamicLayout(nLength,dwMode);
	CSize sz1 = sz;
	CRect rcDir,rcFile,rcLabel,rcPath1,rcPath2,rcPath3,rcPath4,rcBrowse1,rcBrowse2,rcBrowse3,rcBrowse4;
	CRect rcRadio1,rcRadio2,rcRadio3,rcRadio4;
	// 
	m_ctrlBrowse1.GetClientRect(&rcBrowse1);
	m_ctrlBrowse2.GetClientRect(&rcBrowse2);
	m_ctrlBrowse3.GetClientRect(&rcBrowse3);
	m_ctrlBrowse4.GetClientRect(&rcBrowse4);
	m_ctrlPath1.GetClientRect(&rcPath1);
	m_ctrlPath2.GetClientRect(&rcPath2);
	m_ctrlPath3.GetClientRect(&rcPath3);
	m_ctrlPath4.GetClientRect(&rcPath4);
	m_ctrlRadio1.GetClientRect(&rcRadio1);
	m_ctrlRadio2.GetClientRect(&rcRadio2);
	m_ctrlRadio1.GetClientRect(&rcRadio3);
	m_ctrlRadio2.GetClientRect(&rcRadio4);
	
	// 
	
	// move button1
	CSize szBrowse1 = rcBrowse1.Size();
	rcBrowse1.top      = 20;
	rcBrowse1.bottom   = rcBrowse1.top + szBrowse1.cy;
	rcBrowse1.right = sz.cx - 20;
	rcBrowse1.left  = rcBrowse1.right - szBrowse1.cx;
	m_ctrlBrowse1.MoveWindow(rcBrowse1);	
	
	// move radio1
	CSize szRadio1 = rcRadio1.Size();
	rcRadio1.top      = rcBrowse1.top + (szBrowse1.cy - szRadio1.cy)/2;
	rcRadio1.bottom   = rcRadio1.top + szRadio1.cy;
	rcRadio1.left	  = 20;
	rcRadio1.right    = rcRadio1.left + szRadio1.cx;
	m_ctrlRadio1.MoveWindow(rcRadio1);	

	// move path1
	rcPath1.top      = rcBrowse1.top;
	rcPath1.bottom   = rcBrowse1.top + szBrowse1.cy;
	rcPath1.right    = rcBrowse1.left - 20;
	rcPath1.left	 = rcRadio1.right + 20;
	m_ctrlPath1.MoveWindow(rcPath1);	

	// move button2
	CSize szBrowse2 = rcBrowse2.Size();
	rcBrowse2.top      = rcPath1.bottom + 20;
	rcBrowse2.bottom   = rcBrowse2.top + szBrowse2.cy;
	rcBrowse2.right = sz.cx - 20;
	rcBrowse2.left  = rcBrowse2.right - szBrowse2.cx;
	m_ctrlBrowse2.MoveWindow(rcBrowse2);	
	
	// move radio2
	CSize szRadio2 = rcRadio2.Size();
	rcRadio2.top      = rcBrowse2.top + (szBrowse2.cy - szRadio2.cy)/2;
	rcRadio2.bottom   = rcRadio2.top + szRadio2.cy;
	rcRadio2.left	  = 20;
	rcRadio2.right    = rcRadio2.left + szRadio2.cx;
	m_ctrlRadio2.MoveWindow(rcRadio2);	

	// move path2
	rcPath2.top      = rcBrowse2.top;
	rcPath2.bottom   = rcBrowse2.top + szBrowse2.cy;
	rcPath2.right    = rcBrowse2.left - 20;
	rcPath2.left	 = rcRadio2.right + 20;
	m_ctrlPath2.MoveWindow(rcPath2);	

	// move button3
	CSize szBrowse3 = rcBrowse3.Size();
	rcBrowse3.top      = rcPath2.bottom + 20;
	rcBrowse3.bottom   = rcBrowse3.top + szBrowse3.cy;
	rcBrowse3.right = sz.cx - 20;
	rcBrowse3.left  = rcBrowse3.right - szBrowse3.cx;
	m_ctrlBrowse3.MoveWindow(rcBrowse3);	

	// move radio3
	CSize szRadio3 = rcRadio3.Size();
	rcRadio3.top      = rcBrowse3.top + (szBrowse3.cy - szRadio3.cy)/2;
	rcRadio3.bottom   = rcRadio3.top + szRadio3.cy;
	rcRadio3.left	  = 20;
	rcRadio3.right    = rcRadio3.left + szRadio3.cx;
	m_ctrlRadio3.MoveWindow(rcRadio3);	

	// move path3
	rcPath3.top      = rcBrowse3.top;
	rcPath3.bottom   = rcBrowse3.top + szBrowse3.cy;
	rcPath3.right    = rcBrowse3.left - 20;
	rcPath3.left	 = rcRadio3.right + 20;
	m_ctrlPath3.MoveWindow(rcPath3);	

	// move button4
	CSize szBrowse4 = rcBrowse4.Size();
	rcBrowse4.top      = rcPath3.bottom + 20;
	rcBrowse4.bottom   = rcBrowse4.top + szBrowse4.cy;
	rcBrowse4.right = sz.cx - 20;
	rcBrowse4.left  = rcBrowse4.right - szBrowse4.cx;
	m_ctrlBrowse4.MoveWindow(rcBrowse4);	

	// move radio4
	CSize szRadio4 = rcRadio4.Size();
	rcRadio4.top      = rcBrowse4.top + (szBrowse4.cy - szRadio4.cy)/2;
	rcRadio4.bottom   = rcRadio4.top + szRadio4.cy;
	rcRadio4.left	  = 20;
	rcRadio4.right    = rcRadio4.left + szRadio4.cx;
	m_ctrlRadio4.MoveWindow(rcRadio4);	

	// move path4
	rcPath4.top      = rcBrowse4.top;
	rcPath4.bottom   = rcBrowse4.top + szBrowse4.cy;
	rcPath4.right    = rcBrowse4.left - 20;
	rcPath4.left	 = rcRadio4.right + 20;
	m_ctrlPath4.MoveWindow(rcPath4);	
	
	
	sz1.cy -= rcPath4.bottom + 20;

	// move directory window
	m_directory.GetClientRect(&rcDir);
	rcDir.top  = rcPath4.bottom + 20;
	rcDir.bottom = rcDir.top + sz1.cy/3 - 20; 
	rcDir.left += 10;
	rcDir.right = rcDir.left + sz1.cx - 20;
	m_directory.MoveWindow(rcDir);
	
	sz1.cy -= (sz1.cy/3);
	
	// move label window
	m_ctrlReportLabel.GetClientRect(&rcLabel);
	rcLabel.top  = rcDir.bottom + 20; 
	rcLabel.bottom = rcLabel.top + 20; 
	rcLabel.left += 10;
	rcLabel.right = rcLabel.left + sz1.cx - 20;
	m_ctrlReportLabel.MoveWindow(rcLabel);	


	// move file window
	m_fileList.GetClientRect(&rcFile);
	rcFile.top  = rcLabel.bottom + 10; 
	rcFile.bottom = sz.cy - 20; 
	rcFile.left += 10;
	rcFile.right = rcFile.left + sz.cx - 20;
	m_fileList.MoveWindow(rcFile);	
	
	return sz;
   }

void CDirBar::loadFolderFromProfile(BOOL updateData)
{

CDFolder folder;
folder.loadFromProfile();

m_directory.SetRootFolder(folder.getReportFolder(1));
m_path1 = folder.getReportFolder(1);
m_path2 = folder.getReportFolder(2);
m_path3 = folder.getReportFolder(3);
m_path4 = folder.getReportFolder(4);
if(updateData)
	UpdateData(FALSE);

}



LONG CDirBar::OnInitDialog ( UINT wParam, LONG lParam)
	{
    // <-- with these lines. -->

    BOOL bRet = HandleInitDialog(wParam, lParam);

    if (!UpdateData(FALSE))
		{
        TRACE0("Warning: UpdateData failed during dialog init.\n");
        }

// Do Not show files
m_directory.SetShowFiles(FALSE);

loadFolderFromProfile();

//------------------------------------
m_fileList.setReflectMode(FALSE);
m_fileList.modeFullRowSelect(true);
// Inserisce colonna con testo senza font (nero e font default)
m_fileList.LoadColumnHeadings(ID_FILE_LIST_HEADER);

m_fileList.AdjustColumnWidths(TRUE);

UpdateData(FALSE);

return bRet;
}


void CDirBar::OnSelchangedDirTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
// TODO: Add your control notification handler code here
*pResult = 0;

m_fileList.DeleteAllItems();

// trovare item selected
CString folder;
folder = m_directory.GetSelectedFolder();

if (folder.Left(2)!="DD")
	return;	

CString path;
path = m_directory.GetSelectedPath();


// OK abbiamo directory dati
CFileFind fileSearch;

path += "/*.csm";



if (fileSearch.FindFile (path))
	{
	BOOL go = TRUE;
	int a = 0;
	while (go )
		{
		go = fileSearch.FindNextFile();
		if (!fileSearch.IsDirectory())
			{
			CString fileName;
			fileName = fileSearch.GetFileName();
			//fileName2Field(fileName,folder);
			// m_fileList.AddFileFind(fileSearch);
			m_fileList.AddFile(fileName,folder);
			a++;
			}
//		if (a == 2)
//			break;
		}
//	m_fileList.sortFile();
	}
//---------------------------------------------------


fileSearch.Close();
*pResult = 0;
}

void CDirBar::fileName2Field(CString& fileName,CString& folder) 
{
// formato directory doppio
// DDMMYY e DDYYYYMM
// nel secondo caso 
// Formato coil_SGGHHMMSS.csm
CString coil;
int pos = fileName.Find("_");
coil = fileName.Left(pos);
int nItem = m_fileList.AddRow((LPCSTR)coil);
// side 
pos += 1;
CString side = fileName.GetAt(pos);
// data
pos += 1;
CString date;
date = fileName.Mid(pos,2);
if (folder.GetLength() <= 6)
	// vecchio formato
	date += folder.Right(4);
else
	{// nuovo formato
	// mese
	date += folder.Right(2);
	date += folder.Mid(2,4);
	}

m_fileList.AddItem(nItem,1,date);
// ora
pos += 2;
CString time;
time = fileName.Mid(pos,6);
m_fileList.AddItem(nItem,2,time);
// side
m_fileList.AddItem(nItem,3,side);

}


void CDirBar::fileFind2Field(CFileFind& fileSearch) 
{
// Formato coil_SGGHHMMSS.csm
CString coil;
coil = fileSearch.GetFileName();
int nItem = m_fileList.AddRow((LPCSTR)coil);
// date  
CTime t;
fileSearch.GetCreationTime(t);
CString stringDate;
stringDate = t.Format("%Y-%m-%d");
m_fileList.AddItem(nItem,1,stringDate);
// time
CString stringTime;
stringTime = t.Format("%H:%M:%S");
m_fileList.AddItem(nItem,2,stringTime);

}

CString CDirBar::field2filePath(int row) 
{
CString filePath;

filePath = m_directory.GetSelectedPath();
filePath += "\\";
filePath += field2fileName(row); 

return filePath;
}

CString CDirBar::field2fileName(int row) 
{
CString fileName;

// Formato coil.csm
// coil
/*
fileName = m_fileList.GetItemText(row,0);
if (fileName.Right(4) != ".csm")
	// estensione
	fileName += ".csm";
*/


// Formato coil_SGGHHMMSS.csm
// coil
fileName = m_fileList.GetItemText(row,0);
// separator
fileName += "_";
// Side
fileName += m_fileList.GetItemText(row,3);
// GG
#ifdef EURO_DATE	
fileName += (m_fileList.GetItemText(row,1)).Left(2);
#else
fileName += (m_fileList.GetItemText(row,1)).Right(2);
#endif
// ora
fileName += m_fileList.GetItemText(row,2);
// estensione
fileName += ".csm";


return fileName;
}

void CDirBar::OnRadio1() 
{
UpdateData(TRUE);

// TODO: Add your control notification handler code here
m_directory.SetRootFolder(m_path1);
// close and reopen db

CDFolder dFolder;
dFolder.loadFromProfile();

CMainFrame* pFrame;
pFrame = (CMainFrame*)AfxGetMainWnd();
if (pFrame!=NULL)
	if(!pFrame->openDBase(dFolder.m_dbFolder1))
		AfxGetMainWnd()->MessageBox (_T("Cannot open dbase"),_T("dBase.Open"));

}

void CDirBar::OnRadio2() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

// TODO: Add your control notification handler code here
m_directory.SetRootFolder(m_path2);

CDFolder dFolder;
dFolder.loadFromProfile();

CMainFrame* pFrame;
pFrame = (CMainFrame*)AfxGetMainWnd();
if (pFrame!=NULL)
	if(!pFrame->openDBase(dFolder.m_dbFolder2))
		AfxGetMainWnd()->MessageBox (_T("Cannot open dbase"),_T("dBase.Open"));



}

void CDirBar::OnRadio3() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

// TODO: Add your control notification handler code here
m_directory.SetRootFolder(m_path3);
	
CDFolder dFolder;
dFolder.loadFromProfile();

CMainFrame* pFrame;
pFrame = (CMainFrame*)AfxGetMainWnd();
if (pFrame!=NULL)
	if(!pFrame->openDBase(dFolder.m_dbFolder3))
		AfxGetMainWnd()->MessageBox (_T("Cannot open dbase"),_T("dBase.Open"));


}

void CDirBar::OnRadio4() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

// TODO: Add your control notification handler code here
m_directory.SetRootFolder(m_path4);
	
CDFolder dFolder;
dFolder.loadFromProfile();

CMainFrame* pFrame;
pFrame = (CMainFrame*)AfxGetMainWnd();
if (pFrame!=NULL)
	if(!pFrame->openDBase(dFolder.m_dbFolder4))
		AfxGetMainWnd()->MessageBox (_T("Cannot open dbase"),_T("dBase.Open"));

}

void CDirBar::OnBrowse1() 
{
// TODO: Add your control notification handler code here
CFileDialog dialog(TRUE,NULL,NULL,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,NULL,NULL);

dialog.DoModal();
	

// save to profile and reload
}

	


