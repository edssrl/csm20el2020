#pragma once


#include "../../../../../Library/UniCodeListCtrl/ListCtrl.h"
#include "ExtStatic.h"

#include "../Csm20El2013/graphview.h"

#define MISUNIT_HM2		0	// hole/m2
#define	MISUNIT_HM		1	// hole/m
#define MISUNIT_H		2	// hole

// Visualizzazione form CFiltFormView


class CFilterBoundaryListCtrl : public gxListCtrl
{
CLineDoc* c_pDoc;
public:
CFilterBoundaryListCtrl(){c_pDoc=NULL;};
void updatePinholeValues(int row);
virtual void endCellEdit(int row, int col, LPCTSTR itemText);
void setPDoc(CLineDoc* pDoc)	{c_pDoc = pDoc;};
};

class CFiltFormView : public CFormView, public CInit
{
	DECLARE_DYNCREATE(CFiltFormView)

protected:
	CFiltFormView();           // costruttore protetto utilizzato dalla creazione dinamica
	virtual ~CFiltFormView();

	void setMisUnit(int misUnitId);

public:
	enum { IDD = IDD_FILTER_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

//	clear boundary 
	void	clearBoundary(void)
	{m_ctrlListBoundary.DeleteAllItems();};


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Supporto DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_ctrlDestFolder;
	CString m_destFolder;
	CFilterBoundaryListCtrl m_ctrlListBoundary;
	CStatic m_ctrlMisUnitA;
	CStatic m_ctrlMisUnitB;
	CStatic m_ctrlMisUnitBig;
	CStatic m_ctrlMisUnitC;
	CStatic m_ctrlMisUnitD;
	CEdit m_ctrlPinholeA;
	double m_pinholeA;
	CEdit m_ctrlPinholeB;
	double m_pinholeB;
	CEdit m_ctrlPinholeBig;
	double m_pinholeBig;
	CEdit m_ctrlPinholeC;
	double m_pinholeC;
	CEdit m_ctrlPinholeD;
	double m_pinholeD;
	afx_msg void OnClickedBrowseButton();
	afx_msg void OnClickedGoButton();
	virtual void OnInitialUpdate();
	afx_msg void OnRadioMisunit1();
	afx_msg void OnRadioMisunit2();
	afx_msg void OnRadioMisunit3();
	int m_misUnit;
	afx_msg void OnClickedButtonNewrow();
	afx_msg void OnClickedButtonDeleterow();
	CExtStatic m_ctrlCoilName;
	CExtStatic m_ctrlLength;
	CExtStatic m_ctrlStatus;
	CExtStatic m_ctrlWidth;
	BOOL m_clearOutside;

	//--------------
	bool enableBoundarySelection(bool enable);
	void setCoilName(CString coilName)
		{CString s;s = _T("Coil: ");
		if (coilName == _T("")) coilName = _T("----");
		s += coilName;m_ctrlCoilName=s;};
	void setStatus(CString st)
		{CString s;s = _T("Status: ");
		if (st == _T("")) st = _T("----");
		s += st;m_ctrlStatus=s;};
	void setLength(double length)
		{CString s;
		if (length < 0)s.Format(_T("Length: ---- (m)"));
		else
			s.Format(_T("Length: %4.0lf (m)"),length);
		m_ctrlLength=s;};
	void setLeftOffset(double leftO)
		{CString s;
		if (leftO < 0)s.Format(_T("Left Offset: ---- (mm)"));
		else
			s.Format(_T("Left Offset: %4.0lf (mm)"),leftO);
		m_ctrlLeftOffset=s;};
	void setWidth(double width)
		{CString s;
		if (width < 0)s.Format(_T("Width: ---- (mm)"));
		else
			s.Format(_T("Width: %4.0lf (mm)"),width);
		m_ctrlWidth=s;};

	// update string on form with pinhole count
	void updatePinholeCnt(void);

	CButton m_ctrlButtonDeleteRow;
	CButton m_ctrlButtonNewRow;
	CButton m_ctrlButtonGo;
	afx_msg void OnFilePrintPreview();
	afx_msg void OnUpdateFilePrintPreview(CCmdUI *pCmdUI);
	afx_msg void OnClickedButtonSave();
	CExtStatic m_ctrlClsA;
	CExtStatic m_ctrlClsB;
	CExtStatic m_ctrlClsBig;
	CExtStatic m_ctrlClsC;
	CExtStatic m_ctrlClsD;
	CExtStatic m_ctrlDensA;
	CExtStatic m_ctrlDensB;
//	CExtStatic m_ctrlDensBig;
	CExtStatic m_ctrlDensC;
	CExtStatic m_ctrlDensD;
	CExtStatic m_ctrlLeftOffset;
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);

	virtual BOOL DoPreparePrinting (CPrintInfo* pInfo)
		{return CFormView::DoPreparePrinting(pInfo);};
	virtual void GetClientRect(LPRECT rcBounds)
		{return CFormView::GetClientRect(rcBounds);};
	virtual void OnFilePrPview(void)
		{return CFormView::OnFilePrintPreview();};


//	int m_ctrlLeftOffset;
	
};


