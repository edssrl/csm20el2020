#if !defined(AFX_EXTSTATIC_H__7F8E5FF1_F070_11D4_AA31_00C026A019B7__INCLUDED_)
#define AFX_EXTSTATIC_H__7F8E5FF1_F070_11D4_AA31_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtStatic.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExtStatic window

class CExtStatic : public CStatic, public CString
{
// Construction
public:
	CExtStatic();

// Attributes
public:
    COLORREF     m_colorUnvisited;    // color for not visited 
    COLORREF     m_colorVisited;      // color for visited
	COLORREF     m_bkColor;			  // backGround color
    BOOL         m_bVisited;          // whether visited or not 
    BOOL         m_bAutoUpdate;       // True aggiornamento automatico
									  // False: uso meccanismo dodataexchange dialog parent

	CString& operator=(CString &source);
	CString& operator=(LPCTSTR  source);
	
    // URL/filename/text for non-text controls (e.g., icon, bitmap) or when link is
    // different from window text. If you don't set this, CStaticIcon will
    // use GetWindowText to get the link.
//	CString     m_link;				// Testo 

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExtStatic)
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL c_lockUpdate;
	void update(void);
	void SetFont (LPCTSTR fontName,int size,BOOL underLined=FALSE, BOOL bold=400, BOOL italic=FALSE);
	virtual ~CExtStatic();

	// Generated message map functions
protected:
  //  DECLARE_DYNAMIC(CExtStatic)
	//{{AFX_MSG(CExtStatic)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
    CFont  m_font;                  // underline font for text control

    // message handlers
    afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
    afx_msg void    OnClicked();

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTSTATIC_H__7F8E5FF1_F070_11D4_AA31_00C026A019B7__INCLUDED_)


