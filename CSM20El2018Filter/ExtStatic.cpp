// ExtStatic.cpp : implementation file
//

#include "stdafx.h"

#include "ExtStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtStatic

CExtStatic::~CExtStatic()
{
if ((HFONT)m_font) 
	m_font.DeleteObject();

}


BEGIN_MESSAGE_MAP(CExtStatic, CStatic)
	//{{AFX_MSG_MAP(CExtStatic)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
    ON_WM_CTLCOLOR_REFLECT()
    ON_CONTROL_REFLECT(STN_CLICKED, OnClicked)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CExtStatic message handlers

///////////////////
// Constructor sets default colors = blue/purple.
//
CExtStatic::CExtStatic()
{
	Empty();
    m_colorUnvisited = RGB(0,0,255);       // blue
    m_colorVisited   = RGB(128,0,128);     // purple
	m_bkColor  = -1;
    m_bVisited       = FALSE;              // not visited yet
	m_bAutoUpdate = FALSE;					// use dialog mechanism
	c_lockUpdate = FALSE;

}
//////////////////// Handle reflected WM_CTLCOLOR to set custom control color.
// For a text control, use visited/unvisited colors and underline font.
// For non-text controls, do nothing. Also ensures SS_NOTIFY is on.
//
HBRUSH CExtStatic::CtlColor(CDC* pDC, UINT nCtlColor)
{

HBRUSH hbr = NULL;

if (c_lockUpdate)
	{
	return hbr;
	}

    ASSERT(nCtlColor == CTLCOLOR_STATIC);
    DWORD dwStyle = GetStyle();
    
	if (!(dwStyle & SS_NOTIFY)) 
		{
        // Turn on notify flag to get mouse messages and STN_CLICKED.
        // Otherwise, I'll never get any mouse clicks!
		// Disabilitato se vogliamo il click abilitiamo in resource
		// ::SetWindowLong(m_hWnd, GWL_STYLE, dwStyle | SS_NOTIFY);
    }
    
    if ((dwStyle & 0xFF) <= SS_RIGHT) 
		{
        // this is a text control: set up font and colors
        if (!(HFONT)m_font) 
			{
            // first time init: create font
            LOGFONT lf;
            GetFont()->GetObject(sizeof(lf), &lf);
            m_font.CreateFontIndirect(&lf);
			}
		
		if (m_bkColor == -1)
			m_bkColor = GetSysColor(COLOR_BTNFACE);

        // use underline font and visited/unvisited colors
        pDC->SelectObject(&m_font);
        pDC->SetTextColor(m_bVisited ? m_colorVisited : m_colorUnvisited);
		//pDC->SetBkMode(TRANSPARENT);
		pDC->SetBkColor(m_bkColor);
		pDC->SetBkMode(OPAQUE);

		// Resize window rect 
		CSize sz;
		// Calcolo dimensione y anche per stringhe vuote 
		// in caso che esista bordo
		CString sTest;
		sTest = (*(dynamic_cast<CString*>(this)));
		if (sTest.IsEmpty())
			sTest = "O";
		sz = pDC->GetOutputTextExtent((LPCTSTR)sTest);
		sz.cx += sz.cx/10;
		sz.cy += sz.cy/10;
		CRect oldRect;
		GetWindowRect((LPRECT)oldRect);
		BOOL modified = FALSE;
		if(sz.cx < oldRect.Size().cx)
			{
			modified = TRUE;
			sz.cx = oldRect.Size().cx;
			}
		if(sz.cy < oldRect.Size().cy)
			{
			modified = TRUE;
			sz.cy = oldRect.Size().cy;
			}

		// Resize grafico
		if (modified)
			{
			SetWindowPos(NULL,0,0,sz.cx,sz.cy,SWP_NOMOVE|SWP_NOZORDER);
			}

		if (m_bAutoUpdate&&(!IsEmpty()))
			{
			c_lockUpdate = TRUE;
			SetWindowText((LPCTSTR)(*(dynamic_cast<CString*>(this))));
			c_lockUpdate = FALSE;
			}

        // return hollow brush to preserve parent background color
        //hbr = (HBRUSH)::GetStockObject(HOLLOW_BRUSH);
		hbr = GetSysColorBrush(COLOR_BTNFACE);
		
	}
   return hbr;
}
/////////////////
// Handle mouse click: open URL/file.
//
void CExtStatic::OnClicked()
{
if (IsEmpty())         // if URL/filename not set..
	{
	CString s;
	GetWindowText(s);    // ..get it from window text
	*(dynamic_cast<CString*>(this)) = s;
	}

SetWindowText ((LPCTSTR)*(dynamic_cast<CString*>(this)));

m_bVisited = TRUE;

CRect cr;
GetClientRect(&cr);
InvalidateRect(&cr);

}

void CExtStatic::SetFont(LPCTSTR fontName, int pointSize, 
						 BOOL underLined, int bold, BOOL italic)
{

if ((HFONT)m_font)
	m_font.DeleteObject();

m_font.CreatePointFont(pointSize,fontName);

LOGFONT lf;
m_font.GetLogFont(&lf);

m_font.DeleteObject();

lf.lfItalic = italic;
lf.lfUnderline = underLined;

// si puo assegnare valori da 0 a 1000
// 400 e` normale
// 700 e` bold
lf.lfWeight= bold;

m_font.CreateFontIndirect(&lf);

}

void CExtStatic::update()
{
if (GetSafeHwnd() != NULL)
	{
	CRect cr;
	GetClientRect(&cr);
	InvalidateRect(&cr);
	}
}

CString& CExtStatic::operator=(CString &source)
{

*(dynamic_cast<CString*>(this)) = source;


if (CWnd::GetSafeHwnd() != NULL)
	{
	SetWindowText((LPCTSTR)source);
	}

return(*this);
}

CString& CExtStatic::operator=(LPCTSTR source)
{

*(dynamic_cast<CString*>(this)) = source;

return(*this);

}
