// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"

#include "../Csm20El2013/Profile.h"

#include "Csm20El2018Filter.h"
#include "Csm20El2018FilterDoc.h"

#include "MainFrm.h"
#include "DFolder.h"

#include "../Csm20El2013/DReport.h"	
#include "../Csm20El2013/DbSet.h"		// Dialog
#include "../Csm20El2013/DClassColor.h"

#include "../Csm20El2013/DReportSheet.h"
#include "../Csm20El2013/DReportParam.h"
#include "../Csm20El2013/DReportSection.h"

#include "FiltFormView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)
	 
BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_COMMAND_EX(CG_ID_VIEW_CARTELLA, OnBarCheck)
	ON_UPDATE_COMMAND_UI(CG_ID_VIEW_CARTELLA, OnUpdateControlBarMenu)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_NOTIFY(NM_DBLCLK, IDC_FILE_LIDT, OnDblclkFileLidt)
	ON_COMMAND(ID_VIEW_CSM20, OnViewCsm20)
	ON_COMMAND(ID_FILE_CONFIGURE_COLOR, OnFileConfigureColor)
	ON_COMMAND(ID_CONFIGURA_REPORT, OnConfiguraReport)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_OPEN, &CMainFrame::OnFileOpen)
	ON_COMMAND(ID_CONFIGURA_FOLDER, &CMainFrame::OnConfiguraFolder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
}; 

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}


CProgressCtrl* CMainFrame::getProgressCtrl(void)
	{
	return (NULL);
	}
BOOL CMainFrame::progressCreate(void)
	{
	return (TRUE);
	}
void CMainFrame::progressDestroy(void)
{}
void CMainFrame::progressSetRange(int from,int to)
{}
void CMainFrame::progressSetStep(int s)
{}
void CMainFrame::progressStepIt(void)
{}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

	// TODO: Add a menu item that will toggle the visibility of the
	// dialog bar named "Cartella":
	//   1. In ResourceView, open the menu resource that is used by
	//      the CMainFrame class
	//   2. Select the View submenu
	//   3. Double-click on the blank item at the bottom of the submenu
	//   4. Assign the new item an ID: CG_ID_VIEW_CARTELLA
	//   5. Assign the item a Caption: Cartella

	// TODO: Change the value of CG_ID_VIEW_CARTELLA to an appropriate value:
	//   1. Open the file resource.h
	// CG: The following block was inserted by the 'Dialog Bar' component
	{

		// Initialize dialog bar m_wndDirTreeCtrl
		if (!m_wndDirTree.Create(this, CG_IDD_CARTELLA,
			CBRS_SIZE_DYNAMIC | CBRS_LEFT | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_HIDE_INPLACE,
			CG_ID_VIEW_CARTELLA))
		{
			TRACE0("Failed to create dialog bar m_wndDirTreeCtrl\n");
			return -1;		// fail to create
		}

		m_wndDirTree.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
		EnableDocking(CBRS_ALIGN_ANY);
		DockControlBar(&m_wndDirTree);
	}

// load dockState -> dialogBar
loadDockState();



// Init Server Process
CString strValue;
CProfile profile;
strValue = profile.getProfileString(_T("ReportFolder"), _T("DbDefinition"), 
			_T("DbHole.def"));

// Init DataBase Object And Open It
dBase->load(strValue);

// usato dbName1 di CDFolder
//strValue = profile.getProfileString(_T("ReportFolder"), _T("DbName1"),_T("HOLE.MDB"));

// allinea strip su bordo destro
// c_rightAlignStrip = profile.getProfileBool(_T("init"), _T("AlignStripOnRightBorder"),FALSE);

// ora funzione !
/*
try{dBase->Open(strValue);}
catch (CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dBase.Open");
	return -1;
	}
*/

// usato per default prima cartella
CDFolder dFolder;
dFolder.loadFromProfile();
strValue = dFolder.m_dbFolder1;

if(!openDBase(strValue))
	{
	AfxGetMainWnd()->MessageBox (_T("Cannot open dbase"),_T("dBase.Open"));
	return -1;
	}

return 0;
}


BOOL CMainFrame::openDBase(CString path)
		{
		if(dBase->IsOpen())
			dBase->Close();
		try{dBase->Open(path);}
		catch (CDaoException *e)
			{
			return 0;
			}
		return TRUE;
		}



BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
static LPCSTR className = NULL;

if( !CFrameWnd::PreCreateWindow(cs) )
	return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs


if (className == NULL) 
	{
    // One-time class registration
    // The only purpose is to make the class name something meaningful
    // instead of "Afx:0x4d:27:32:hup1hup:hike!"
    WNDCLASS wndcls;
	::GetClassInfo(AfxGetInstanceHandle(), cs.lpszClass, &wndcls);
  	wndcls.lpszClassName = CSM_FILTER_CLASSNAME;
    wndcls.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    VERIFY(AfxRegisterClass(&wndcls));
    className= CSM_FILTER_CLASSNAME;
    }
cs.lpszClass = className;
// Not system menu
// cs.style &= ~WS_SYSMENU;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


BOOL CMainFrame::Pump (void)
{
MSG msg;
//
// Retrieve and dispatch any waiting messages.
//
while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)) 
	{
	if (!AfxGetApp ()->PumpMessage ()) 
		{
		::PostQuitMessage (0);
		return FALSE;
		}
	}

//
// Simulate the framework's idle processing mechanism.
//
LONG lIdle = 0;
while (AfxGetApp ()->OnIdle (lIdle++));
return TRUE;
}




/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
// doubleClick on nome file -> load data
void CMainFrame::OnDblclkFileLidt(NMHDR* pNMHDR, LRESULT* pResult) 
{
// TODO: Add your control notification handler code here
CCsm20El2018FilterDoc* pDoc;
pDoc = (CCsm20El2018FilterDoc*) GetActiveDocument();

if (pNMHDR->code == NM_DBLCLK) 
	{
	DWORD dwPos;
	LVHITTESTINFO hti; 
	dwPos = GetMessagePos ();
	int x,y;

    hti.pt.x = (int) LOWORD (dwPos);
    x = (int) LOWORD (dwPos);
	hti.pt.y = (int) HIWORD (dwPos);
	y = (int) HIWORD (dwPos);
    m_wndDirTree.m_fileList.ScreenToClient (&hti.pt);
	CPoint p = hti.pt;
	int row = m_wndDirTree.m_fileList.HitTestEx (p,NULL);
	// get filePathName
	CString filePath = m_wndDirTree.field2filePath(row);
	// save file path on doc
	if( pDoc != NULL)
		pDoc->c_originalPathName = filePath;

	CFiltFormView* pFormView;
	pFormView = (CFiltFormView*) this->GetActiveView();
	if ((pFormView != NULL)&&
		(pFormView->m_destFolder == _T("")))
		{
		CString	 msg;
		msg = _T("Please set save folder now empty");
		AfxMessageBox(msg,MB_ICONERROR);
		return;
		}
	CFileBpe cf;
	CFileException e;
	
	// open
	if( !cf.Open((LPCSTR) filePath, CFile::modeRead | CFile::typeBinary, &e ) )
		{
		AfxGetMainWnd()->MessageBox("ERROR OPEN","Error");
		return;
		}

	
	if (pDoc != NULL)
		pDoc->load(&cf);
	
	cf.Close();

	// update data on form
	if (pFormView != NULL)
		{
		// clear boundary selection
		pFormView->clearBoundary();
		// enable add row and delete row
		pFormView->enableBoundarySelection(true);
		pFormView->setCoilName(pDoc->c_rotolo);
		pFormView->setLength((double)pDoc->getVTotalLength());
		pFormView->setWidth(pDoc->c_larghezza);
		pFormView->setLeftOffset(pDoc->getRelativePos(0.));
		pFormView->setStatus(_T("Ready"));

		// update pinhole counter on dialog
		pFormView->updatePinholeCnt();


		// save data on local folder
#ifndef _DEBUG
// release version save as soon as load on local folder
		CFileBpe fp;
		CString filePathName;
		filePathName = pFormView->m_destFolder;
		filePathName += _T("/");
		// add DD folder
		filePathName += pDoc->getMonthFolder();
		// create folder if it doesn't exist
		SECURITY_ATTRIBUTES	 sa;
		sa.nLength= sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor=NULL;
		sa.bInheritHandle = TRUE;
		::CreateDirectory((LPCTSTR)filePathName,&sa);
		//------------------------------------------
		filePathName += _T("/");
		filePathName += pDoc->getFileName();
		filePathName += _T(".csm");

		if (fp.Open(filePathName.GetString(),CFile::modeCreate | CFile::modeWrite))
			{
			pDoc->save(&fp);
			fp.Close();
			}
		else
			AfxMessageBox(_T("Cannot create file to save"));
		// 
#endif

		}
	}
	

*pResult= NULL;
}
   

void CMainFrame::OnViewCsm20() 
{
// TODO: Add your command handler code here
CProfile profile;
CString procReport;
CString dirReport;

procReport = profile.getProfileString(_T("SERVER"), _T("ReportProg"), 
				"CSM20El2013Report.exe");
dirReport = profile.getProfileString(_T("SERVER"), _T("ReportDir"), 
				"./");

CWnd *csm20 = CWnd::FindWindow(CSM20_REPORT_CLASSNAME,NULL);
if (!csm20)
	{
	PROCESS_INFORMATION ProcessInformation;
	STARTUPINFO StartUpInfo;
	
	::GetStartupInfo (&StartUpInfo);
	BOOL fSuccess = CreateProcess((LPCSTR)procReport,NULL,
				NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,
				NULL,(LPCSTR)dirReport,
				&StartUpInfo,&ProcessInformation); 

	if (fSuccess) 
		{
		// Wait Creation Window RpcServer
		int res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
		if (res != 0)
			{
			res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
			if (res != 0)
				{
				CString s;
				s = "Fail Wait Idle Report " + procReport;
				AfxMessageBox (s);
				}
			}
	 	CloseHandle(ProcessInformation.hThread);
		CloseHandle(ProcessInformation.hProcess);
		CWnd *report = CWnd::FindWindow(CSM_REPORT_CLASSNAME,NULL);
		if(report)
			{
			report->ShowWindow(SW_SHOWMAXIMIZED);
			report->SetFocus( );
			report->SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0,
			  SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
			}

		}
	else
		{
		CString s;
		s = "Unable to Create Process " + procReport;
		AfxMessageBox (s);
		return;
		}
	}
else
	{// already open set zOrder TopMost
	// close	
	this->ShowWindow(SW_SHOWMINIMIZED);

	// already open set zOrder TopMost	
	csm20->ShowWindow(SW_SHOWMAXIMIZED);
	csm20->SetFocus( );
	csm20->SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0,
      SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
	}


}

BOOL CMainFrame::DestroyWindow() 
{
	
// TODO: Add your specialized code here and/or call the base class

	
if (dBase != NULL)
	{
	dBase->Close();
	delete dBase;
	dBase = NULL;
	}
	
return CFrameWnd::DestroyWindow();
}


void	CMainFrame::saveDockState(void)
{

m_wndDirTree.saveToProfile("DockState");


CDockState dockState;

// Get dock state
GetDockState (dockState);
// Save into profile
dockState.SaveState ("DockState");
	
}


void	CMainFrame::loadDockState(void)
{

m_wndDirTree.loadFromProfile("DockState");

CDockState dockState;
// Load default state
GetDockState (dockState);

CProfile profile;
if (profile.getProfileBool ("DockState",(LPCSTR)"LoadFromProfile",FALSE))
	{
	// Try load from profile
	dockState.LoadState ("DockState");
	if (dockState.GetVersion() > 1)
		SetDockState (dockState);
	}
else
	{
	dockState.Clear();
	dockState.SaveState("DockState");
	}

}



void CMainFrame::OnFileConfigureColor() 
{
// TODO: Add your command handler code here

CDClassColor dialog;

dialog.loadFromProfile();

if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	CSM20ClassColorInvalidate();
	}
}



// importare da CMainframe originario
void CMainFrame::OnConfiguraReport() 
{
// create tab dialog
CDReportSheet   reportDlg( "Report Configure Dialog" ); // 		
CDReportSection*    pSectionPage	= new CDReportSection;  
CDReportParam*		pParamPage		= new CDReportParam;  
	  
ASSERT( pSectionPage );  
ASSERT( pParamPage );    
	  
reportDlg.AddPage( pSectionPage ); 
reportDlg.AddPage( pParamPage ); 


// TODO: Add your command handler code here
DBReport dbReport(dBase);
// dBase already Open

if (dbReport.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	pParamPage->m_Allarmi = dbReport.m_SOGLIE_ALLARME;
	// dialog.m_Cliente = dbReport.m_DATI_CLIENTE;
	// dialog.m_DataLavoraz = dbReport.m_DATA_LAVORAZIONE;
	pParamPage->m_difettiPer = dbReport.m_DESCR_PERIODICI;
	pParamPage->m_difettiRand = dbReport.m_DESCR_RANDOM;
	pParamPage->m_alzate  = dbReport.m_DESCR_ALZATE;

	// dialog.m_Lunghezza = dbReport.m_LUNGHEZZA_BOBINA;
	pParamPage->m_mappa = dbReport.m_MAPPA100;
	//-----------------
	pParamPage->m_graph1 = dbReport.m_DESCR_GRAPH1;
	pParamPage->m_graph2 = dbReport.m_DESCR_GRAPH2;
	pParamPage->m_graph3 = dbReport.m_DESCR_GRAPH3;
	pParamPage->m_graph4 = dbReport.m_DESCR_GRAPH4;

	pParamPage->m_classA = dbReport.m_DESCR_CLASSA;
	pParamPage->m_classB = dbReport.m_DESCR_CLASSB;
	pParamPage->m_classC = dbReport.m_DESCR_CLASSC;
	pParamPage->m_classD = dbReport.m_DESCR_CLASSD;
	pParamPage->m_classBig = dbReport.m_DESCR_CLASSBIG;
	//----------------------------
	// Eliminato 
//	dialog.m_alMetro = (int )dbReport.m_ALMETRO;
//	dialog.m_dalMetro = (int )dbReport.m_DALMETRO;

	pSectionPage->m_interv1 = (int )dbReport.m_INTERVALLO1;
	if (dbReport.m_INTERVALLO1 > 0.)
		pSectionPage->m_enableInt1 = TRUE;
	pSectionPage->m_interv2 = (int )dbReport.m_INTERVALLO2;
	if (dbReport.m_INTERVALLO2 > 0.)
		pSectionPage->m_enableInt2 = TRUE;
	pSectionPage->m_interv3 = (int )dbReport.m_INTERVALLO3;
	if (dbReport.m_INTERVALLO3 > 0.)
		pSectionPage->m_enableInt3 = TRUE;
	pSectionPage->m_interv4 = (int )dbReport.m_INTERVALLO4;
	if (dbReport.m_INTERVALLO4 > 0.)
		pSectionPage->m_enableInt4 = TRUE;
	pSectionPage->m_interv5 = (int )dbReport.m_INTERVALLO5;
	if (dbReport.m_INTERVALLO5 > 0.)
		pSectionPage->m_enableInt5 = TRUE;
	pSectionPage->m_interv6 = (int )dbReport.m_INTERVALLO6;
	if (dbReport.m_INTERVALLO6 > 0.)
		pSectionPage->m_enableInt6 = TRUE;
	pSectionPage->m_interv7 = (int )dbReport.m_INTERVALLO7;
	if (dbReport.m_INTERVALLO7 > 0.)
		pSectionPage->m_enableInt7 = TRUE;
	pSectionPage->m_interv8 = (int )dbReport.m_INTERVALLO8;
	if (dbReport.m_INTERVALLO8 > 0.)
		pSectionPage->m_enableInt8 = TRUE;
	pSectionPage->m_interv9 = (int )dbReport.m_INTERVALLO9;
	if (dbReport.m_INTERVALLO9 > 0.)
		pSectionPage->m_enableInt9 = TRUE;
	pSectionPage->m_interv10 = (int )dbReport.m_INTERVALLO10;
	if (dbReport.m_INTERVALLO10 > 0.)
		pSectionPage->m_enableInt10 = TRUE;
	pSectionPage->m_interv11 = (int )dbReport.m_INTERVALLO11;
	if (dbReport.m_INTERVALLO11 > 0.)
		pSectionPage->m_enableInt11 = TRUE;
	pSectionPage->m_interv12 = (int )dbReport.m_INTERVALLO12;
	if (dbReport.m_INTERVALLO12 > 0.)
		pSectionPage->m_enableInt12 = TRUE;
	pSectionPage->m_interv13 = (int )dbReport.m_INTERVALLO13;
	if (dbReport.m_INTERVALLO13 > 0.)
		pSectionPage->m_enableInt13 = TRUE;
	pSectionPage->m_interv14 = (int )dbReport.m_INTERVALLO14;
	if (dbReport.m_INTERVALLO14 > 0.)
		pSectionPage->m_enableInt14 = TRUE;
	pSectionPage->m_interv15 = (int )dbReport.m_INTERVALLO15;
	if (dbReport.m_INTERVALLO15 > 0.)
		pSectionPage->m_enableInt15 = TRUE;
	pSectionPage->m_interv16 = (int )dbReport.m_INTERVALLO16;
	if (dbReport.m_INTERVALLO16 > 0.)
		pSectionPage->m_enableInt16 = TRUE;
	pSectionPage->m_interv17 = (int )dbReport.m_INTERVALLO17;
	if (dbReport.m_INTERVALLO17 > 0.)
		pSectionPage->m_enableInt17 = TRUE;
	pSectionPage->m_interv18 = (int )dbReport.m_INTERVALLO18;
	if (dbReport.m_INTERVALLO18 > 0.)
		pSectionPage->m_enableInt18 = TRUE;
	pSectionPage->m_interv19 = (int )dbReport.m_INTERVALLO19;
	if (dbReport.m_INTERVALLO19 > 0.)
		pSectionPage->m_enableInt19 = TRUE;



	if (reportDlg.DoModal() == IDOK)
		{
		try
			{
			dbReport.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
			dbReport.Close();
			return ;
			}

		dbReport.m_SOGLIE_ALLARME = pParamPage->m_Allarmi;
		// dbReport.m_DATI_CLIENTE = dialog.m_Cliente;
		// dbReport.m_DATA_LAVORAZIONE = dialog.m_DataLavoraz;
		dbReport.m_DESCR_PERIODICI = pParamPage->m_difettiPer;
		dbReport.m_DESCR_RANDOM = pParamPage->m_difettiRand;
		dbReport.m_DESCR_ALZATE	= pParamPage->m_alzate;
		// dbReport.m_LUNGHEZZA_BOBINA = dialog.m_Lunghezza;
		dbReport.m_MAPPA100 = pParamPage->m_mappa;
//		dbReport.m_ALMETRO = (double)dialog.m_alMetro;
//		dbReport.m_DALMETRO = (double)dialog.m_dalMetro;
		//-----------------
		dbReport.m_DESCR_GRAPH1 = pParamPage->m_graph1;
		dbReport.m_DESCR_GRAPH2 = pParamPage->m_graph2;
		dbReport.m_DESCR_GRAPH3 = pParamPage->m_graph3;
		dbReport.m_DESCR_GRAPH4 = pParamPage->m_graph4;

		dbReport.m_DESCR_CLASSA = pParamPage->m_classA;
		dbReport.m_DESCR_CLASSB = pParamPage->m_classB;
		dbReport.m_DESCR_CLASSC = pParamPage->m_classC;
		dbReport.m_DESCR_CLASSD = pParamPage->m_classD;
		dbReport.m_DESCR_CLASSBIG = pParamPage->m_classBig;
		//----------------------------


		if (pSectionPage->m_enableInt1)
			dbReport.m_INTERVALLO1 = (double)pSectionPage->m_interv1;
		else
			dbReport.m_INTERVALLO1 = 0.;
		if (pSectionPage->m_enableInt2)
			dbReport.m_INTERVALLO2 = (double)pSectionPage->m_interv2;
		else
			dbReport.m_INTERVALLO2 = 0.;
		if (pSectionPage->m_enableInt3)
			dbReport.m_INTERVALLO3 = (double)pSectionPage->m_interv3;
		else
			dbReport.m_INTERVALLO3 = 0.;
		if (pSectionPage->m_enableInt4)
			dbReport.m_INTERVALLO4 = (double)pSectionPage->m_interv4;
		else
			dbReport.m_INTERVALLO4 = 0.;
		if (pSectionPage->m_enableInt5)
			dbReport.m_INTERVALLO5 = (double)pSectionPage->m_interv5;
		else
			dbReport.m_INTERVALLO5 = 0.;
		if (pSectionPage->m_enableInt6)
			dbReport.m_INTERVALLO6 = (double)pSectionPage->m_interv6;
		else
			dbReport.m_INTERVALLO6 = 0.;
		if (pSectionPage->m_enableInt7)
			dbReport.m_INTERVALLO7 = 
				(double)pSectionPage->m_interv7;
		else
			dbReport.m_INTERVALLO7 = 0.;
		if (pSectionPage->m_enableInt8)
			dbReport.m_INTERVALLO8 = 
				(double)pSectionPage->m_interv8;
		else
			dbReport.m_INTERVALLO8 = 0.;
		if (pSectionPage->m_enableInt9)
			dbReport.m_INTERVALLO9 = 
				(double)pSectionPage->m_interv9;
		else
			dbReport.m_INTERVALLO9 = 0.;
		if (pSectionPage->m_enableInt10)
			dbReport.m_INTERVALLO10 = 
				(double)pSectionPage->m_interv10;
		else
			dbReport.m_INTERVALLO10 = 0.;
		if (pSectionPage->m_enableInt11)
			dbReport.m_INTERVALLO11 = 
				(double)pSectionPage->m_interv11;
		else
			dbReport.m_INTERVALLO11 = 0.;
		if (pSectionPage->m_enableInt12)
			dbReport.m_INTERVALLO12 = 
				(double)pSectionPage->m_interv12;
		else
			dbReport.m_INTERVALLO12 = 0.;
		if (pSectionPage->m_enableInt13)
			dbReport.m_INTERVALLO13 = 
				(double)pSectionPage->m_interv13;
		else
			dbReport.m_INTERVALLO13 = 0.;
		if (pSectionPage->m_enableInt14)
			dbReport.m_INTERVALLO14 = 
				(double)pSectionPage->m_interv14;
		else
			dbReport.m_INTERVALLO14 = 0.;
		if (pSectionPage->m_enableInt15)
			dbReport.m_INTERVALLO15 = 
				(double)pSectionPage->m_interv15;
		else
			dbReport.m_INTERVALLO15 = 0.;
		if (pSectionPage->m_enableInt16)
			dbReport.m_INTERVALLO16 = 
				(double)pSectionPage->m_interv16;
		else
			dbReport.m_INTERVALLO16 = 0.;
		if (pSectionPage->m_enableInt17)
			dbReport.m_INTERVALLO17 = 
				(double)pSectionPage->m_interv17;
		else
			dbReport.m_INTERVALLO17 = 0.;
		if (pSectionPage->m_enableInt18)
			dbReport.m_INTERVALLO18 = 
				(double)pSectionPage->m_interv18;
		else
			dbReport.m_INTERVALLO18 = 0.;
		if (pSectionPage->m_enableInt19)
			dbReport.m_INTERVALLO19 = 
				(double)pSectionPage->m_interv19;
		else
			dbReport.m_INTERVALLO19 = 0.;
		try
			{
			dbReport.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
			dbReport.Close();
			return ;
			}

		}
	}
dbReport.Close();

delete pSectionPage;
delete pParamPage;

}

/*
void CMainFrame::OnConfiguraReport() 
{
	// TODO: Add your command handler code here
DBReport dbReport(dBase);
CDReport dialog;
// dBase already Open

CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();

// dialog.c_legacyMode = pDoc->isLegacyDoc();


if (dbReport.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_Allarmi = dbReport.m_SOGLIE_ALLARME;
	// dialog.m_Cliente = dbReport.m_DATI_CLIENTE;
	// dialog.m_DataLavoraz = dbReport.m_DATA_LAVORAZIONE;
	dialog.m_difettiPer = dbReport.m_DESCR_PERIODICI;
	dialog.m_difettiRand = dbReport.m_DESCR_RANDOM;
	dialog.m_alzate  = dbReport.m_DESCR_ALZATE;

	//------------
	// nuovo sistema di ordine configurabile
	dialog.m_graph = dbReport.m_GRAPH;

	dialog.m_orderGraph = dbReport.m_ORDER_GRAPH-1;
	dialog.m_orderRandom = dbReport.m_ORDER_RANDOM-1;
	dialog.m_orderRising = dbReport.m_ORDER_RISING-1;
	dialog.m_orderAlarm = dbReport.m_ORDER_ALARM-1;
	//

	// dialog.m_Lunghezza = dbReport.m_LUNGHEZZA_BOBINA;
	// dialog.m_mappa = dbReport.m_MAPPA100;
	// Eliminato 
//	dialog.m_alMetro = (int )dbReport.m_ALMETRO;
//	dialog.m_dalMetro = (int )dbReport.m_DALMETRO;

	dialog.m_interv1 = (int )dbReport.m_INTERVALLO1;
	if (dbReport.m_INTERVALLO1 > 0.)
		dialog.m_enableInt1 = TRUE;
	dialog.m_interv2 = (int )dbReport.m_INTERVALLO2;
	if (dbReport.m_INTERVALLO2 > 0.)
		dialog.m_enableInt2 = TRUE;
	dialog.m_interv3 = (int )dbReport.m_INTERVALLO3;
	if (dbReport.m_INTERVALLO3 > 0.)
		dialog.m_enableInt3 = TRUE;
	dialog.m_interv4 = (int )dbReport.m_INTERVALLO4;
	if (dbReport.m_INTERVALLO4 > 0.)
		dialog.m_enableInt4 = TRUE;
	dialog.m_interv5 = (int )dbReport.m_INTERVALLO5;
	if (dbReport.m_INTERVALLO5 > 0.)
		dialog.m_enableInt5 = TRUE;
	dialog.m_interv6 = (int )dbReport.m_INTERVALLO6;
	if (dbReport.m_INTERVALLO6 > 0.)
		dialog.m_enableInt6 = TRUE;
	dialog.m_interv7 = (int )dbReport.m_INTERVALLO7;
	if (dbReport.m_INTERVALLO7 > 0.)
		dialog.m_enableInt7 = TRUE;
	dialog.m_interv8 = (int )dbReport.m_INTERVALLO8;
	if (dbReport.m_INTERVALLO8 > 0.)
		dialog.m_enableInt8 = TRUE;
	dialog.m_interv9 = (int )dbReport.m_INTERVALLO9;
	if (dbReport.m_INTERVALLO9 > 0.)
		dialog.m_enableInt9 = TRUE;
	dialog.m_interv10 = (int )dbReport.m_INTERVALLO10;
	if (dbReport.m_INTERVALLO10 > 0.)
		dialog.m_enableInt10 = TRUE;
	dialog.m_interv11 = (int )dbReport.m_INTERVALLO11;
	if (dbReport.m_INTERVALLO11 > 0.)
		dialog.m_enableInt11 = TRUE;
	dialog.m_interv12 = (int )dbReport.m_INTERVALLO12;
	if (dbReport.m_INTERVALLO12 > 0.)
		dialog.m_enableInt12 = TRUE;
	dialog.m_interv13 = (int )dbReport.m_INTERVALLO13;
	if (dbReport.m_INTERVALLO13 > 0.)
		dialog.m_enableInt13 = TRUE;
	dialog.m_interv14 = (int )dbReport.m_INTERVALLO14;
	if (dbReport.m_INTERVALLO14 > 0.)
		dialog.m_enableInt14 = TRUE;
	dialog.m_interv15 = (int )dbReport.m_INTERVALLO15;
	if (dbReport.m_INTERVALLO15 > 0.)
		dialog.m_enableInt15 = TRUE;
	dialog.m_interv16 = (int )dbReport.m_INTERVALLO16;
	if (dbReport.m_INTERVALLO16 > 0.)
		dialog.m_enableInt16 = TRUE;
	dialog.m_interv17 = (int )dbReport.m_INTERVALLO17;
	if (dbReport.m_INTERVALLO17 > 0.)
		dialog.m_enableInt17 = TRUE;
	dialog.m_interv18 = (int )dbReport.m_INTERVALLO18;
	if (dbReport.m_INTERVALLO18 > 0.)
		dialog.m_enableInt18 = TRUE;
	dialog.m_interv19 = (int )dbReport.m_INTERVALLO19;
	if (dbReport.m_INTERVALLO19 > 0.)
		dialog.m_enableInt19 = TRUE;
	
	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbReport.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
			dbReport.Close();
			return ;
			}

		dbReport.m_SOGLIE_ALLARME = dialog.m_Allarmi;
		// dbReport.m_DATI_CLIENTE = dialog.m_Cliente;
		// dbReport.m_DATA_LAVORAZIONE = dialog.m_DataLavoraz;
		dbReport.m_DESCR_PERIODICI = dialog.m_difettiPer;
		dbReport.m_DESCR_RANDOM = dialog.m_difettiRand;
		dbReport.m_DESCR_ALZATE	= dialog.m_alzate;
		// dbReport.m_LUNGHEZZA_BOBINA = dialog.m_Lunghezza;
		// dbReport.m_MAPPA100 = dialog.m_mappa;
//		dbReport.m_ALMETRO = (double)dialog.m_alMetro;
//		dbReport.m_DALMETRO = (double)dialog.m_dalMetro;
		if (dialog.m_enableInt1)
			dbReport.m_INTERVALLO1 = (double)dialog.m_interv1;
		else
			dbReport.m_INTERVALLO1 = 0.;
		if (dialog.m_enableInt2)
			dbReport.m_INTERVALLO2 = (double)dialog.m_interv2;
		else
			dbReport.m_INTERVALLO2 = 0.;
		if (dialog.m_enableInt3)
			dbReport.m_INTERVALLO3 = (double)dialog.m_interv3;
		else
			dbReport.m_INTERVALLO3 = 0.;
		if (dialog.m_enableInt4)
			dbReport.m_INTERVALLO4 = (double)dialog.m_interv4;
		else
			dbReport.m_INTERVALLO4 = 0.;
		if (dialog.m_enableInt5)
			dbReport.m_INTERVALLO5 = (double)dialog.m_interv5;
		else
			dbReport.m_INTERVALLO5 = 0.;
		if (dialog.m_enableInt6)
			dbReport.m_INTERVALLO6 = (double)dialog.m_interv6;
		else
			dbReport.m_INTERVALLO6 = 0.;
		if (dialog.m_enableInt7)
			dbReport.m_INTERVALLO7 = 
				(double)dialog.m_interv7;
		else
			dbReport.m_INTERVALLO7 = 0.;
		if (dialog.m_enableInt8)
			dbReport.m_INTERVALLO8 = 
				(double)dialog.m_interv8;
		else
			dbReport.m_INTERVALLO8 = 0.;
		if (dialog.m_enableInt9)
			dbReport.m_INTERVALLO9 = 
				(double)dialog.m_interv9;
		else
			dbReport.m_INTERVALLO9 = 0.;
		if (dialog.m_enableInt10)
			dbReport.m_INTERVALLO10 = 
				(double)dialog.m_interv10;
		else
			dbReport.m_INTERVALLO10 = 0.;
		if (dialog.m_enableInt11)
			dbReport.m_INTERVALLO11 = 
				(double)dialog.m_interv11;
		else
			dbReport.m_INTERVALLO11 = 0.;
		if (dialog.m_enableInt12)
			dbReport.m_INTERVALLO12 = 
				(double)dialog.m_interv12;
		else
			dbReport.m_INTERVALLO12 = 0.;
		if (dialog.m_enableInt13)
			dbReport.m_INTERVALLO13 = 
				(double)dialog.m_interv13;
		else
			dbReport.m_INTERVALLO13 = 0.;
		if (dialog.m_enableInt14)
			dbReport.m_INTERVALLO14 = 
				(double)dialog.m_interv14;
		else
			dbReport.m_INTERVALLO14 = 0.;
		if (dialog.m_enableInt15)
			dbReport.m_INTERVALLO15 = 
				(double)dialog.m_interv15;
		else
			dbReport.m_INTERVALLO15 = 0.;
		if (dialog.m_enableInt16)
			dbReport.m_INTERVALLO16 = 
				(double)dialog.m_interv16;
		else
			dbReport.m_INTERVALLO16 = 0.;
		if (dialog.m_enableInt17)
			dbReport.m_INTERVALLO17 = 
				(double)dialog.m_interv17;
		else
			dbReport.m_INTERVALLO17 = 0.;
		if (dialog.m_enableInt18)
			dbReport.m_INTERVALLO18 = 
				(double)dialog.m_interv18;
		else
			dbReport.m_INTERVALLO18 = 0.;
		if (dialog.m_enableInt19)
			dbReport.m_INTERVALLO19 = 
				(double)dialog.m_interv19;
		else
			dbReport.m_INTERVALLO19 = 0.;

		dbReport.m_GRAPH = dialog.m_graph; 
		dbReport.m_ORDER_GRAPH = dialog.m_orderGraph+1;
		dbReport.m_ORDER_RANDOM = dialog.m_orderRandom+1;
		dbReport.m_ORDER_RISING = dialog.m_orderRising+1;
		dbReport.m_ORDER_ALARM = dialog.m_orderAlarm+1;


		try
			{
			dbReport.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
			dbReport.Close();
			return ;
			}

		}
	}
dbReport.Close();

}
*/



void CMainFrame::OnFileOpen()
{
	// TODO: Add your command handler code here
CString szFilter ("CSM Files (*.csm)|*.csm|All Files (*.*)|*.*||");

char dir [128];

// Save curDir
GetCurrentDirectory(127,dir);  

// set iniDefault dir
CProfile profile;
CString sDir;
sDir = profile.getProfileString("init","RootDir",".");
SetCurrentDirectory((LPCSTR) sDir);  				

CFileDialog dialog (TRUE,(LPCSTR) "*.csm",
	(LPCSTR) "*.csm",OFN_HIDEREADONLY,(LPCSTR) szFilter);

if (dialog.DoModal() == IDOK)
	{
	// Load Data 
	CFileBpe cf;
	CFileException e;
	CString title;
	title =  dialog.GetPathName();
	
	CFiltFormView* pFormView;
	pFormView = (CFiltFormView*) this->GetActiveView();
	if ((pFormView != NULL)&&
		(pFormView->m_destFolder == _T("")))
		{
		CString	 msg;
		msg = _T("Please set save folder now empty");
		AfxMessageBox(msg,MB_ICONERROR);
		return;
		}

	// open
	if( !cf.Open((LPCSTR) title, CFile::modeRead | CFile::typeBinary, &e ) )
		{
		AfxGetMainWnd()->MessageBox("ERROR OPEN","Error");
		return;
		}

	CCsm20El2018FilterDoc* pDoc;
	pDoc = (CCsm20El2018FilterDoc*) GetActiveDocument();
	
	if (pDoc != NULL)
		pDoc->load(&cf);

	cf.Close();

	// save file path on doc
	if( pDoc != NULL)
		pDoc->c_originalPathName = title;

	
	// update data on form
	if (pFormView != NULL)
		{
		// clear boundary selection
		pFormView->clearBoundary();
		// enable add row and delete row
		pFormView->enableBoundarySelection(true);
		pFormView->setCoilName(pDoc->c_rotolo);
		pFormView->setLength((double)pDoc->getVTotalLength());
		pFormView->setWidth(pDoc->c_larghezza);
		pFormView->setLeftOffset(pDoc->getRelativePos(0.));
		pFormView->setStatus(_T("Ready"));

		// update pinhole counter on dialog
		pFormView->updatePinholeCnt();


		// save data on local folder
#ifndef _DEBUG
// release version save as soon as load on local folder
		CFileBpe fp;
		CString filePathName;
		filePathName = pFormView->m_destFolder;
		filePathName += _T("/");
		// add DD folder
		filePathName += pDoc->getMonthFolder();
		filePathName += _T("/");
		filePathName += pDoc->getFileName();
		filePathName += _T(".csm");

		if (fp.Open(filePathName.GetString(),CFile::modeCreate | CFile::modeWrite))
			{
			pDoc->save(&fp);
			fp.Close();
			}
		else
			AfxMessageBox(_T("Cannot create file to save"));
		// 
#endif

		}
	}
}


void CMainFrame::OnConfiguraFolder()
{
// TODO: aggiungere qui il codice per la gestione dei comandi.
CDFolder dialog;

dialog.loadFromProfile();


if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	// cambiati folder lettura dati aggiornare left panel and updateData
	m_wndDirTree.loadFolderFromProfile(TRUE);
	}

}
