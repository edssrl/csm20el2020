# Microsoft Developer Studio Project File - Name="Csm20El2018Filter" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Csm20El2018Filter - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Csm20El2018Filter.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Csm20El2018Filter.mak" CFG="Csm20El2018Filter - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Csm20El2018Filter - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Csm20El2018Filter - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Csm20El2018Filter - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "CSM20A_REPORT" /D "Csm20El2013_REPORT" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "Csm20El2013_REPORT" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 listctrl.lib /nologo /subsystem:windows /machine:I386 /libpath:"C:/User/Library/ListCtrl/release"

!ELSEIF  "$(CFG)" == "Csm20El2018Filter - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "Csm20El2013_REPORT" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "Csm20El2013_REPORT" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 listctrl.lib /nologo /subsystem:windows /map /debug /machine:I386 /pdbtype:sept /libpath:"C:/User/Library/ListCtrl/Debug"

!ENDIF 

# Begin Target

# Name "Csm20El2018Filter - Win32 Release"
# Name "Csm20El2018Filter - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\Csm20El2013\CDaoDbCreate.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\CFileBpe.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\PrintLibSrc\CPage.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\PrintLibSrc\CPrinter.cpp
# End Source File
# Begin Source File

SOURCE=.\Csm20El2018Filter.cpp
# End Source File
# Begin Source File

SOURCE=.\Csm20El2018Filter.rc
# End Source File
# Begin Source File

SOURCE=.\Csm20El2018FilterDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\Csm20El2018FilterView.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DAllarmi.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DAlzataDettagli.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DAlzate.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\dbset.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DClassColor.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DF1.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\PrintLibSrc\Dib.cpp
# End Source File
# Begin Source File

SOURCE=.\DirBar.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DSelReportType.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DStripPosition.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\dyntempl.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Elemento.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Endian.cpp
# End Source File
# Begin Source File

SOURCE=.\FileCoilList.cpp
# End Source File
# Begin Source File

SOURCE=.\FileTreeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\GraphDoc.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\graphview.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Label.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Layout.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Profile.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\pwdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ResizableDlgBar.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\RpcMClient.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\RulerWnd.cpp
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\StaticExt.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Target.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Csm20El2013\CDaoDbCreate.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\CFileBpe.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\PrintLibSrc\CPage.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\PrintLibSrc\CPrinter.h
# End Source File
# Begin Source File

SOURCE=.\Csm20El2018Filter.h
# End Source File
# Begin Source File

SOURCE=.\Csm20El2018FilterDoc.h
# End Source File
# Begin Source File

SOURCE=.\Csm20El2018FilterView.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DAllarmi.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DAlzataDettagli.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DAlzate.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\dbset.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DClassColor.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DF1.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\PrintLibSrc\Dib.h
# End Source File
# Begin Source File

SOURCE=.\DirBar.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DSelReportType.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\DStripPosition.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\dyntempl.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Elemento.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Endian.h
# End Source File
# Begin Source File

SOURCE=.\FileCoilList.h
# End Source File
# Begin Source File

SOURCE=.\FileTreeCtrl.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\graphdoc.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\graphview.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Label.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Layout.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Profile.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\pwdlg.h
# End Source File
# Begin Source File

SOURCE=.\ResizableDlgBar.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\RpcMClient.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\RulerWnd.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\StaticExt.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\Csm20El2013\Target.h
# End Source File
# Begin Source File

SOURCE=.\VERSIONE.H
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Csm20El2018Filter.ico
# End Source File
# Begin Source File

SOURCE=.\res\Csm20El2018Filter.rc2
# End Source File
# Begin Source File

SOURCE=.\res\Csm20El2018FilterDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\logoEds.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tfdropcopy.cur
# End Source File
# Begin Source File

SOURCE=.\res\tfnodropcopy.cur
# End Source File
# Begin Source File

SOURCE=.\res\tfnodropmove.cur
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "Config"

# PROP Default_Filter "ini"
# Begin Source File

SOURCE=..\..\..\..\WINNT\Csm20El2018Filter.INI
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
