// Csm20El2018FilterView.cpp : implementation of the CCsm20El2018FilterView class
//

#include "stdafx.h"
#include "Csm20El2018Filter.h"

#include "Csm20El2018FilterDoc.h"
#include "Csm20El2018FilterView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterView

IMPLEMENT_DYNCREATE(CCsm20El2018FilterView, CInitView)

BEGIN_MESSAGE_MAP(CCsm20El2018FilterView, CInitView)
	//{{AFX_MSG_MAP(CCsm20El2018FilterView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CInitView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CInitView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CInitView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterView construction/destruction

CCsm20El2018FilterView::CCsm20El2018FilterView()
{
	// TODO: add construction code here

}

CCsm20El2018FilterView::~CCsm20El2018FilterView()
{
}

BOOL CCsm20El2018FilterView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CInitView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterView drawing

void CCsm20El2018FilterView::OnDraw(CDC* pDC)
{
	CCsm20El2018FilterDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CInitView::OnDraw(pDC);
}


/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterView diagnostics

#ifdef _DEBUG
void CCsm20El2018FilterView::AssertValid() const
{
	CInitView::AssertValid();
}

void CCsm20El2018FilterView::Dump(CDumpContext& dc) const
{
	CInitView::Dump(dc);
}

CCsm20El2018FilterDoc* CCsm20El2018FilterView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCsm20El2018FilterDoc)));
	return (CCsm20El2018FilterDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterView message handlers
