// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A2D2B9E5_B14D_4665_BD55_D6B1CEDDD2B4__INCLUDED_)
#define AFX_STDAFX_H__A2D2B9E5_B14D_4665_BD55_D6B1CEDDD2B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "targetver.h"
// leak detector should appear before '#include <afxwin.h>' in file stdafx.h
#include <vld.h>

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <math.h>
#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

// Use for print preview ClipRgn
#include <afxpriv.h>
#include <math.h>

#include <afxcview.h>
#include <afxctl.h>

// SingleLock Multithread
#include <afxmt.h>

#include "../Csm20El2013/Profile.h"



#include <afxtempl.h>

#define PI 3.14159265359

#define MAX_NUMCLASSI 4+1
#define MAX_NUMLABEL 24
#define MAX_NUMSTRIP 80

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#pragma warning(disable: 4018)
#pragma warning(disable: 4244)
#pragma warning(disable: 4995)
#pragma warning(disable: 4996)



#endif // !defined(AFX_STDAFX_H__A2D2B9E5_B14D_4665_BD55_D6B1CEDDD2B4__INCLUDED_)
