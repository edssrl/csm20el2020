   ///////////////////////////////////////////////////////////////////// 

   // ResizableDlgBar.cpp : implementation file
   // 

   #include "stdafx.h"
   #include "ResizableDlgBar.h"

   //////////////////////////////////////////////////////////////////// 
   // CResizableDlgBar Construction/Destruction

   BOOL CResizableDlgBar::Create( CWnd* pParentWnd, UINT nIDTemplate,
                                  UINT nStyle, UINT nID, BOOL bChange)
   {
        if(!CDialogBar::Create(pParentWnd,nIDTemplate,nStyle,nID))
             return FALSE;

        m_bChangeDockedSize = bChange;
        m_sizeFloating = m_sizeDocked = m_sizeDefault;
        return TRUE;
   }

   BOOL CResizableDlgBar::Create( CWnd* pParentWnd,
                                  LPCTSTR lpszTemplateName, UINT nStyle,
                                  UINT nID, BOOL bChange)
   {
       if (!CDialogBar::Create( pParentWnd, lpszTemplateName,
                                                 nStyle, nID))
           return FALSE;

       m_bChangeDockedSize = bChange;
       m_sizeFloating = m_sizeDocked = m_sizeDefault;
       return TRUE;
   }

   //////////////////////////////////////////////////////////////////// 
   // Overloaded functions

   CSize CResizableDlgBar::CalcDynamicLayout(int nLength, DWORD dwMode)
   {
       // Return default if it is being docked or floated
       if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
       {
           if (dwMode & LM_STRETCH) // if not docked stretch to fit
               return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx,
                            (dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
             else
               return m_sizeDocked;
       }
       if (dwMode & LM_MRUWIDTH)
           return m_sizeFloating;
       // In all other cases, accept the dynamic length
       if (dwMode & LM_LENGTHY)
           return CSize(m_sizeFloating.cx, (m_bChangeDockedSize) ?
                        m_sizeFloating.cy = m_sizeDocked.cy = nLength :
                        m_sizeFloating.cy = nLength);
        else
           return CSize((m_bChangeDockedSize) ?
                        m_sizeFloating.cx = m_sizeDocked.cx = nLength :
                        m_sizeFloating.cx = nLength, m_sizeFloating.cy);
   }

BEGIN_MESSAGE_MAP(CResizableDlgBar, CDialogBar)
//{{AFX_MSG_MAP(CResizableDlgBar)
// NOTE - the ClassWizard will add and remove mapping macros here
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

///////////////////////////////////////////////////////////////////// 
// CResizableDlgBar message handlers
///////////////////////////////////////

BOOL	CResizableDlgBar::saveToProfile(LPCSTR strRoot)
{
CProfile profile;

//
CString s;
s.Format("%d",m_sizeDocked.cx);
profile.writeProfileString (strRoot,"SizeDockedX",s);
s.Format("%d",m_sizeDocked.cy);
profile.writeProfileString (strRoot,"SizeDockedY",s);

s.Format("%d",m_sizeFloating.cx);
profile.writeProfileString (strRoot,"SizeFloatingX",s);
s.Format("%d",m_sizeFloating.cy);
profile.writeProfileString (strRoot,"SizeFloatingY",s);
return TRUE;
}

BOOL	CResizableDlgBar::loadFromProfile(LPCSTR strRoot)
{
CProfile profile;

m_sizeDocked.cx = profile.getProfileInt (strRoot,"SizeDockedX",m_sizeDefault.cx);
m_sizeDocked.cy = profile.getProfileInt (strRoot,"SizeDockedY",m_sizeDefault.cy);
m_sizeFloating.cx = profile.getProfileInt (strRoot,"SizeDockedX",m_sizeDefault.cx);
m_sizeFloating.cy = profile.getProfileInt (strRoot,"SizeDockedY",m_sizeDefault.cy);

return TRUE;
}

