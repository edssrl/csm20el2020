// Csm20El2018FilterDoc.cpp : implementation of the CCsm20El2018FilterDoc class
//

#include "stdafx.h"

#include "Csm20El2018Filter.h"
#include "MainFrm.h"

#define MAIN
#include "Csm20El2018FilterDoc.h"
#include "FiltFormView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterDoc

IMPLEMENT_DYNCREATE(CCsm20El2018FilterDoc, CLineDoc)

BEGIN_MESSAGE_MAP(CCsm20El2018FilterDoc, CLineDoc)
	//{{AFX_MSG_MAP(CCsm20El2018FilterDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterDoc construction/destruction

CCsm20El2018FilterDoc::CCsm20El2018FilterDoc() 
{
	// TODO: add one-time construction code here

}

CCsm20El2018FilterDoc::~CCsm20El2018FilterDoc()
{
}

BOOL CCsm20El2018FilterDoc::OnNewDocument()
{
// non usiamo CLIneDoc::OnNewDocument())
// xChe` altrimenti cerca di inizializzare CMainFrm che e` diverso
if (!CDocument::OnNewDocument())
	return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
//if (!UpdateBaseDoc())
//	return FALSE;


	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterDoc serialization

void CCsm20El2018FilterDoc::Serialize(CArchive& ar)
{
	CLineDoc::Serialize(ar);

}

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterDoc diagnostics

#ifdef _DEBUG
void CCsm20El2018FilterDoc::AssertValid() const
{
	CLineDoc::AssertValid();
}

void CCsm20El2018FilterDoc::Dump(CDumpContext& dc) const
{
	CLineDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2018FilterDoc commands

/*-----------------------------------------------------------------

			SWAPLONG () 		routine swap long format
	
-----------------------------------------------------------------*/

union bytelong
	{
	unsigned char byte [4];
	unsigned long word;
	};

void swaplong (unsigned long *vett,int number)
{
union bytelong local;
int i;

unsigned long *lfrom;
unsigned char *bto;

lfrom = vett;
bto = (unsigned char *) vett;

for (i=0;i<number;i++)
	{
	local.word = *lfrom;     /* carico valore in union */
	
	bto [0] = local.byte[3];	/* swap */
	bto [1] = local.byte[2];
	bto [2] = local.byte[1];
	bto [3] = local.byte[0];
	bto += sizeof (long);	/* next word */
	lfrom ++;
	}
}	

int round2int(double d)
{
int iv = d;
if ((d - iv) >= 0.5) return iv+1;
	return iv;
}

	

bool	CCsm20El2018FilterDoc::applyFilter(VectorBounds vb,FilterParam fp)
{

// clear previous limit
c_vectorBound.clear();

// check limit
if(vb.checkLimit(getRelativePos(0.),getRelativePos(c_larghezza),0.,(double)getVTotalLength()))
	{
	// assign new limit
	c_vectorBound = vb;
	// set parameter
	// incrementa contatore filtraggi applicati
	fp.c_filterTimes = c_filterParam.c_filterTimes + 1;
	c_filterParam = fp;
	//
	// per eventuale frazione modo MISUNIT_H
	double totalA,totalB,totalC,totalD,totalBig;
	totalA = c_difCoil.getTotDifetti('A',0,(int)c_difCoil.getMeter());
	totalB = c_difCoil.getTotDifetti('B',0,(int)c_difCoil.getMeter());
	totalC = c_difCoil.getTotDifetti('C',0,(int)c_difCoil.getMeter());
	totalD = c_difCoil.getTotDifetti('D',0,(int)c_difCoil.getMeter());
	totalBig = c_difCoil.getTotBigHole(0,(int)c_difCoil.getMeter());
	
	for(int i=0; i< c_vectorBound.size(); i++)
		{
		FilterBoundValues fvb;
		fvb.c_bot = vb[i].c_bottom;
		fvb.c_bandaLeft = vb[i].c_left/SIZE_BANDE;
		fvb.c_top = vb[i].c_top;
		fvb.c_bandaRight = vb[i].c_right/SIZE_BANDE;
		// if (fp.c_filterType != MISUNIT_H)
		if(1)
			{
			fvb.c_limit[0] = fp.c_limitA;
			fvb.c_limit[1] = fp.c_limitB;
			fvb.c_limit[2] = fp.c_limitC;
			fvb.c_limit[3] = fp.c_limitD;
			fvb.c_limit[4] = fp.c_limitBig;
			fvb.c_value[0] = vb[i].c_clsA;
			fvb.c_value[1] = vb[i].c_clsB;
			fvb.c_value[2] = vb[i].c_clsC;
			fvb.c_value[3] = vb[i].c_clsD;
			fvb.c_value[4] = vb[i].c_clsBig;
			}
		else
			{// ripartisco proporzionalemente
			double propFactor = (totalA>0)?(double)vb[i].c_clsA/totalA:1;
			fvb.c_limit[0] = round2int(fp.c_limitA * propFactor);
			propFactor = (totalB>0)?(double)vb[i].c_clsB/totalB:1;
			fvb.c_limit[1] = round2int(fp.c_limitB * propFactor);
			propFactor = (totalC>0)?(double)vb[i].c_clsC/totalC:1;
			fvb.c_limit[2] = round2int(fp.c_limitC * propFactor);
			propFactor = (totalD>0)?(double)vb[i].c_clsD/totalD:1;
			fvb.c_limit[3] = round2int(fp.c_limitD * propFactor);
			propFactor = (totalBig>0)?(double)vb[i].c_clsBig/totalBig:1;
			fvb.c_limit[4] = round2int(fp.c_limitBig * propFactor);
			}
		fvb.c_mode = fp.c_filterType;
		fvb.c_clearOutside = fp.c_clearOutside;
		c_difCoil.applyFilter(fvb);

		}
	return true;
	}

return false;
}


BOOL CCsm20El2018FilterDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
// TODO: Add your specialized code here and/or call the base class

	
// Save docking control state
CMainFrame *mainFrame = (CMainFrame *) AfxGetMainWnd();
mainFrame->saveDockState();
	
return CLineDoc::CanCloseFrame(pFrame);
}
