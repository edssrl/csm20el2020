// Csm20El2018FilterDoc.h : interface of the CCsm20El2018FilterDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_Csm20El2018FilterDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_)
#define AFX_Csm20El2018FilterDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "../Csm20El2013/graphdoc.h"
#include "../Csm20El2013/Cdaodbcreate.h"


#ifdef MAIN
CDaoDbCreate *dBase = NULL;
#else
extern CDaoDbCreate *dBase;
#endif

struct RectBound
{
double	c_top;
double	c_bottom;
double	c_left;
double	c_right;
bool	checkLimit(double l, double r,double t,double b)
{return((c_top >= t)&&(c_bottom <= b)&&(c_left >= l)&&(c_right <= r));};
int		c_clsA;		// valori interni all'area
int		c_clsB;
int		c_clsC;
int		c_clsD;
int		c_clsBig;
};

typedef vector <RectBound> VBound;

class	VectorBounds : public VBound
{

public:	
VectorBounds(void){;};

bool checkLimit(double l, double r,double t,double b)
	{bool ret = true;
	for(int i=0;i<this->size();i++)
		{ret  = ret && (this->at(i)).checkLimit(l,r,t,b);}
	return ret;}

};


class CCsm20El2018FilterDoc : public CLineDoc
{
protected: // create from serialization only
	CCsm20El2018FilterDoc();
	DECLARE_DYNCREATE(CCsm20El2018FilterDoc)

// Attributes
public:
	VectorBounds	c_vectorBound;

	CString			c_originalPathName;

// Operations
public:
	bool	applyFilter(VectorBounds vb,FilterParam fp);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCsm20El2018FilterDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCsm20El2018FilterDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCsm20El2018FilterDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Csm20El2018FilterDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_)
