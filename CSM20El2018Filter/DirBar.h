#if !defined(AFX_DIRBAR_H__50701106_E02F_42ED_A399_FB9FC6C13C5B__INCLUDED_)
#define AFX_DIRBAR_H__50701106_E02F_42ED_A399_FB9FC6C13C5B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DirBar.h : header file
//

#include "FileTreeCtrl.h"
#include "FileCoilList.h"
#include "ResizableDlgBar.h"

/////////////////////////////////////////////////////////////////////////////
// CDirBar dialog

// class CDirBar : public CDialogBar
class CDirBar : public CResizableDlgBar
{
// Construction
public:
	CDirBar(void);   // standard constructor
	void OnBrowse1();

//------------------------------------------
CString field2fileName(int row);
CString field2filePath(int row); 
void fileName2Field(CString& fileName,CString& folder); 
void fileFind2Field(CFileFind& fileSearch); 

void loadFolderFromProfile(BOOL updateData=FALSE);

// Dialog Data
	//{{AFX_DATA(CDirBar)
	enum { IDD = CG_IDD_CARTELLA };
	CEdit	m_ctrlPath1;
	CEdit	m_ctrlPath2;
	CEdit	m_ctrlPath3;
	CEdit	m_ctrlPath4;

	CButton	m_ctrlBrowse2;
	CButton	m_ctrlBrowse1;
	CButton	m_ctrlBrowse3;
	CButton	m_ctrlBrowse4;

	CStatic	m_ctrlReportLabel;
	CFileCoilList	m_fileList;
	CTreeFileCtrl	m_directory;
	
	CString	m_path1;
	CString	m_path2;
	CString	m_path3;
	CString	m_path4;

	CButton	m_ctrlRadio1;
	CButton	m_ctrlRadio2;
	CButton	m_ctrlRadio3;
	CButton	m_ctrlRadio4;
	int		m_radio;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDirBar)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
      virtual CSize CalcDynamicLayout( int nLength, DWORD dwMode );

	// Generated message map functions
	//{{AFX_MSG(CDirBar)
	afx_msg LONG OnInitDialog ( UINT, LONG );   // <-Add this line.
	afx_msg void OnSelchangedDirTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	afx_msg void OnRadio3();
	afx_msg void OnRadio4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIRBAR_H__50701106_E02F_42ED_A399_FB9FC6C13C5B__INCLUDED_)
