// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__845B65B5_5D69_44F0_8B4B_CBA40146F118__INCLUDED_)
#define AFX_MAINFRM_H__845B65B5_5D69_44F0_8B4B_CBA40146F118__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "DirBar.h"

//Nome Server Window
#define CSM_FILTER_CLASSNAME "Csm20El2018Filter"
#define CSM20_REPORT_CLASSNAME "Csm20El2013Report"

void CSM20ClassColorInvalidate (void);

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public: 

// Operations
public:
	BOOL progressBusy;
	BOOL openDBase(CString path);


CProgressCtrl* getProgressCtrl(void);
BOOL progressCreate(void);
void progressDestroy(void);
void progressSetRange(int from,int to);
void progressSetStep(int s);
void progressStepIt(void);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG 
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

BOOL Pump (void);

void	saveDockState(void);
void	loadDockState(void);

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

// Generated message map functions
protected:
	CDirBar m_wndDirTree;

	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDblclkFileLidt(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnViewCsm20();
	afx_msg void OnFileConfigureColor();
	afx_msg void OnConfiguraReport();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	
	afx_msg void OnFileOpen();
	afx_msg void OnConfiguraFolder();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__845B65B5_5D69_44F0_8B4B_CBA40146F118__INCLUDED_)
