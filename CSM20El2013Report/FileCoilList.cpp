// FileCoilList.cpp : implementation file
//

#include "stdafx.h"
#include "Csm20El2013Report.h"
#include "FileCoilList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileCoilList

CFileCoilList::CFileCoilList()
{

for (int i=0;i<4;i++)
	c_colSorted[i] = FALSE;

this->setReflectMode (FALSE);

}

CFileCoilList::~CFileCoilList()
{
}


BEGIN_MESSAGE_MAP(CFileCoilList, gxListCtrl)
	//{{AFX_MSG_MAP(CFileCoilList)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileCoilList message handlers

struct	ArgPass
{
CFileCoilList* pListCtrl;
int		colIndex;
BOOL	forward;
}arg;;

static int CALLBACK 
ListStringCompProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
// lParamSort contains a pointer to the struct with list view control and col.
ArgPass* pArg = (ArgPass*) lParamSort;

CString    strItem1;
CString    strItem2; 
switch(pArg->colIndex)
	{
	case 0:
		{
		strItem1 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam1).col0;
		strItem2 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam2).col0;
		}
		break;
	case 1:
		{
		strItem1 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam1).col1;
		strItem2 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam2).col1;
		}
		break;
	case 2:
		{
		strItem1 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam1).col2;
		strItem2 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam2).col2;
		}
		break;
	case 3:
		{
		strItem1 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam1).col3;
		strItem2 = pArg->pListCtrl->c_stringFileArray.GetAt(lParam2).col3;
		}
		break;
	}

if (pArg->forward)
	return strcmp((LPCSTR)strItem1,(LPCSTR) strItem2);
else
	return strcmp((LPCSTR)strItem2,(LPCSTR) strItem1);
	
}



// virtual
// default sort column
void CFileCoilList::onColclick(int col, int x,int y)
{
// se si vogliono ordinare stringhe sostituire funzione di sort

arg.pListCtrl = this;
arg.colIndex = col;
arg.forward = c_colSorted[col];

SortItems(ListStringCompProc, (LPARAM) &arg);

c_colSorted[col] = !c_colSorted[col];

}

void CFileCoilList::AddFileFind(CFileFind& fileSearch) 
{
// Formato coil_SGGHHMMSS.csm
CString coil;
coil = fileSearch.GetFileName();

int pos = coil.Find(".csm");
if (pos < 0)
	// not csm file
	return;
coil = coil.Left(pos);

int iRow = GetItemCount();
int nItem = AddRow((LPCSTR)coil);
SetItemData(nItem,iRow);

// date  
CTime t;
fileSearch.GetCreationTime(t);
CString stringDate;
stringDate = t.Format("%Y-%m-%d");
AddItem(nItem,1,stringDate,iRow);

// time
CString stringTime;
stringTime = t.Format("%H:%M:%S");
AddItem(nItem,2,stringTime,iRow);

ListElement elem;
elem.col0 = GetItemText(nItem, 0);
elem.col1 = GetItemText(nItem, 1);
elem.col2 = GetItemText(nItem, 2);

c_stringFileArray.Add(elem);


}


void CFileCoilList::AddFile(CString fileName,CString folder)
{
// Formato coil
CString coil;
int pos = fileName.Find(".csm");
if (pos < 0)
	// not csm file
	return;

/*
coil = fileName.Left(pos);
int iRow = GetItemCount();
int nItem = AddRow((LPCSTR)coil);
SetItemData(nItem,iRow);
return;
*/



// Formato coil_SGGHHMMSS.csm

// CString coil;
pos = fileName.Find("_");
coil = fileName.Left(pos);
int iRow = GetItemCount();
int nItem = AddRow((LPCSTR)coil);
SetItemData(nItem,iRow);
// side 
pos += 1;
CString side = fileName.GetAt(pos);
// data
pos += 1;
CString date;

#ifdef EURO_DATE	// data DDMMYY
date = fileName.Mid(pos,2);
if (folder.GetLength() <= 6)
	date += folder.Right(4);
else
	{// nuovo formato
	// mese
	date += folder.Right(2);
	date += folder.Mid(2,4);
	}
#else	// invertita data YYMMDD
date = _T("");
if (folder.GetLength() <= 6)
	{
	date += folder.Right(2);
	date += folder.Mid(2,2);
	}
else
	{// nuovo formato
	// mese
	date += folder.Mid(2,4);
	date += folder.Right(2);
	}
date += fileName.Mid(pos,2);
#endif

AddItem(nItem,1,date,iRow);
// ora
pos += 2;
CString time;
time = fileName.Mid(pos,6);
AddItem(nItem,2,time,iRow);
// side
AddItem(nItem,3,side,iRow);




/* Formato coil_SGGHHMMSS.csm
CString coil;
int pos = fileName.Find("_");
coil = fileName.Left(pos);
int nItem = GetItemCount();
AddRow((LPCSTR)coil);
SetItemData(nItem,nItem);

// side 
pos += 1;
CString side = fileName.GetAt(pos);
// data
pos += 1;
CString date;
date = fileName.Mid(pos,2);
date += folder.Right(4);
AddItem(nItem,1,(LPCSTR)date,nItem);
// ora
pos += 2;
CString time;
time = fileName.Mid(pos,6);
AddItem(nItem,2,(LPCSTR)time,nItem);
// side
AddItem(nItem,3,(LPCSTR)side,nItem);
*/


/*
// parse filename 
// Formato coil_SGGHHMMSS.csm
CString coil;


CString sThick1,sThick2;
// Elimino spessore 
int pos = fileName.Find('_');
//sThick1 = fileName.Left(pos);
//fileName = fileName.Right(fileName.GetLength() - (pos+1));
//pos = fileName.Find('_');
//sThick2 = fileName.Left(pos);
//fileName = fileName.Right(fileName.GetLength() - (pos+1));
// estraggo nomeCoil
//pos = fileName.Find('_');
coil = fileName.Left(pos);
int nItem = GetItemCount();
AddRow((LPCSTR)(coil));
SetItemData(nItem,nItem);

AddItem(nItem,1,(LPCSTR)coil,nItem);
// side 
pos += 1;
CString side = fileName.GetAt(pos);
// data
pos += 1;
CString date;
date = fileName.Mid(pos,2);
date += folder.Right(4);
AddItem(nItem,2,(LPCSTR)date,nItem);
// ora
pos += 2;
CString time;
time = fileName.Mid(pos,6);
AddItem(nItem,3,(LPCSTR)time,nItem);
// side
AddItem(nItem,4,(LPCSTR)side,nItem);
*/

ListElement elem;
elem.col0 = GetItemText(nItem, 0);
elem.col1 = GetItemText(nItem, 1);
elem.col2 = GetItemText(nItem, 2);
elem.col3 = GetItemText(nItem, 3);

c_stringFileArray.Add(elem);

}
