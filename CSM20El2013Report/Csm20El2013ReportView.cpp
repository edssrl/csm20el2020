// Csm20El2013ReportView.cpp : implementation of the CCsm20El2013ReportView class
//

#include "stdafx.h"
#include "Csm20El2013Report.h"

#include "Csm20El2013ReportDoc.h"
#include "Csm20El2013ReportView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportView

IMPLEMENT_DYNCREATE(CCsm20El2013ReportView, CInitView)

BEGIN_MESSAGE_MAP(CCsm20El2013ReportView, CInitView)
	//{{AFX_MSG_MAP(CCsm20El2013ReportView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CInitView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CInitView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CInitView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportView construction/destruction

CCsm20El2013ReportView::CCsm20El2013ReportView()
{
	// TODO: add construction code here

}

CCsm20El2013ReportView::~CCsm20El2013ReportView()
{
}

BOOL CCsm20El2013ReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CInitView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportView drawing

void CCsm20El2013ReportView::OnDraw(CDC* pDC)
{
	CCsm20El2013ReportDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
	CInitView::OnDraw(pDC);
}


/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportView diagnostics

#ifdef _DEBUG
void CCsm20El2013ReportView::AssertValid() const
{
	CInitView::AssertValid();
}

void CCsm20El2013ReportView::Dump(CDumpContext& dc) const
{
	CInitView::Dump(dc);
}

CCsm20El2013ReportDoc* CCsm20El2013ReportView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCsm20El2013ReportDoc)));
	return (CCsm20El2013ReportDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportView message handlers
