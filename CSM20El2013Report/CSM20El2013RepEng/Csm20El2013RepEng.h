// Csm20El2013RepEng.h : main header file for the Csm20El2013RepEng DLL
//

#if !defined(AFX_Csm20El2013RepEng_H__42C48640_FE81_43DC_9AEA_F47B5853E222__INCLUDED_)
#define AFX_Csm20El2013RepEng_H__42C48640_FE81_43DC_9AEA_F47B5853E222__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013RepEngApp
// See Csm20El2013RepEng.cpp for the implementation of this class
//

class CCsm20El2013RepEngApp : public CWinApp
{
public:
	CCsm20El2013RepEngApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCsm20El2013RepEngApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCsm20El2013RepEngApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
//	afx_msg void OnFilePrintSetup();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Csm20El2013RepEng_H__42C48640_FE81_43DC_9AEA_F47B5853E222__INCLUDED_)
