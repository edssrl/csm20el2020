// Csm20El2013ReportDoc.h : interface of the CCsm20El2013ReportDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_Csm20El2013ReportDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_)
#define AFX_Csm20El2013ReportDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "../Csm20El2013/graphdoc.h"
#include "../Csm20El2013/Cdaodbcreate.h"


#ifdef MAIN
CDaoDbCreate *dBase = NULL;
#else
extern CDaoDbCreate *dBase;
#endif


class CCsm20El2013ReportDoc : public CLineDoc
{
protected: // create from serialization only
	CCsm20El2013ReportDoc();
	DECLARE_DYNCREATE(CCsm20El2013ReportDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCsm20El2013ReportDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCsm20El2013ReportDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCsm20El2013ReportDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Csm20El2013ReportDOC_H__0D284F5C_5642_46AD_882C_C0B801666AF9__INCLUDED_)
