// Csm20El2013Report.h : main header file for the Csm20El2013Report application
//

#if !defined(AFX_Csm20El2013Report_H__FD4F7884_89F0_4AB5_A4B5_8357FE2F0B7D__INCLUDED_)
#define AFX_Csm20El2013Report_H__FD4F7884_89F0_4AB5_A4B5_8357FE2F0B7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

#include "..\Csm20El2013\EnumPrinters.h"

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportApp:
// See Csm20El2013Report.cpp for the implementation of this class
//



class CCsm20El2013ReportApp : public CWinApp
{
HMODULE		hModRes;	// Resource handle modificato 
HMODULE		hDefRes;	// Resource handle originario	


CString		c_logFileName;


CEnumPrinters	m_PrinterControl ;

public:
	CCsm20El2013ReportApp();

// Print setting
void SetPrintLandscape(void);
void SetPrintPortrait(void);
// return the name of the currently selected printer
CString GetDefaultPrinter(void);

void  SetIniPath();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCsm20El2013ReportApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCsm20El2013ReportApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void OnFilePrintSetup();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Csm20El2013Report_H__FD4F7884_89F0_4AB5_A4B5_8357FE2F0B7D__INCLUDED_)
