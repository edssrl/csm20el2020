// DirBar.cpp : implementation file
//

#include "stdafx.h"
#include "Csm20El2013Report.h"
#include "DirBar.h"
#include "../Csm20El2013/Profile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDirBar dialog


CDirBar::CDirBar(void)
{
	//{{AFX_DATA_INIT(CDirBar)
	//}}AFX_DATA_INIT
}


void CDirBar::DoDataExchange(CDataExchange* pDX)
{
	CResizableDlgBar::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDirBar)
	DDX_Control(pDX, IDC_REPORT_LABEL, m_ctrlReportLabel);
	DDX_Control(pDX, IDC_FILE_LIDT, m_fileList);
	DDX_Control(pDX, IDC_DIR_TREE, m_directory);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDirBar, CResizableDlgBar)
	//{{AFX_MSG_MAP(CDirBar)
	ON_NOTIFY(TVN_SELCHANGED, IDC_DIR_TREE, OnSelchangedDirTree)
	//}}AFX_MSG_MAP
    ON_MESSAGE(WM_INITDIALOG, OnInitDialog )    // <-- Add this line.
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDirBar message handlers


CSize CDirBar::CalcDynamicLayout(int nLength, DWORD dwMode)
   {
   
	CSize sz = CResizableDlgBar::CalcDynamicLayout(nLength,dwMode);
	CRect rcDir,rcFile,rcLabel;
	// move directory window
	m_directory.GetClientRect(&rcDir);
	rcDir.top  += 10; 
	rcDir.bottom = rcDir.top + sz.cy/3 - 20; 
	rcDir.left += 10;
	rcDir.right = rcDir.left + sz.cx - 20;
	m_directory.MoveWindow(rcDir);	
	
	// move label window
	m_ctrlReportLabel.GetClientRect(&rcLabel);
	rcLabel.top  = rcDir.bottom + 20; 
	rcLabel.bottom = rcLabel.top + 20; 
	rcLabel.left += 10;
	rcLabel.right = rcLabel.left + sz.cx - 20;
	m_ctrlReportLabel.MoveWindow(rcLabel);	


	// move file window
	m_fileList.GetClientRect(&rcFile);
	rcFile.top  = rcLabel.bottom + 10; 
	rcFile.bottom = rcFile.top + 2*sz.cy/3 - 20; 
	rcFile.left += 10;
	rcFile.right = rcFile.left + sz.cx - 20;
	m_fileList.MoveWindow(rcFile);	
	
	return sz;
   }




LONG CDirBar::OnInitDialog ( UINT wParam, LONG lParam)
	{
    // <-- with these lines. -->

    BOOL bRet = HandleInitDialog(wParam, lParam);

    if (!UpdateData(FALSE))
		{
        TRACE0("Warning: UpdateData failed during dialog init.\n");
        }

// Do Not show files
m_directory.SetShowFiles(FALSE);

CProfile profile;
CString rootFolder;
rootFolder = profile.getProfileString("init","RootFolder","C:/User/eds/hole/csm20A");

m_directory.SetRootFolder(rootFolder);

//------------------------------------
m_fileList.setReflectMode(FALSE);
m_fileList.modeFullRowSelect(true);
// Inserisce colonna con testo senza font (nero e font default)
m_fileList.LoadColumnHeadings(ID_FILE_LIST_HEADER);


m_fileList.AdjustColumnWidths(TRUE);

return bRet;
}


void CDirBar::OnSelchangedDirTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
// TODO: Add your control notification handler code here
*pResult = 0;

m_fileList.DeleteAllItems();

// trovare item selected
CString folder;
folder = m_directory.GetSelectedFolder();

if (folder.Left(2)!="DD")
	return;	

CString path;
path = m_directory.GetSelectedPath();


// OK abbiamo directory dati
CFileFind fileSearch;

path += "/*.csm";



if (fileSearch.FindFile (path))
	{
	BOOL go = TRUE;
	int a = 0;
	while (go )
		{
		go = fileSearch.FindNextFile();
		if (!fileSearch.IsDirectory())
			{
			CString fileName;
			fileName = fileSearch.GetFileName();
			//fileName2Field(fileName,folder);
			m_fileList.AddFile(fileName,folder);
			a++;
			}
//		if (a == 2)
//			break;
		}
//	m_fileList.sortFile();

//-------------------------------------------
// Old Code
/*
	while (go )
		{
		go = fileSearch.FindNextFile();
		if (!fileSearch.IsDirectory())
			{
			CString fileName;
			fileName = fileSearch.GetFileName();
			// parse filename 
			// Formato coil_SGGHHMMSS.csm
			CString coil;
			int pos = fileName.Find("_");
			coil = fileName.Left(pos);
			int nItem = m_fileList.AddRow((LPCSTR)coil);
			// side 
			pos += 1;
			CString side = fileName.GetAt(pos);
			// data
			pos += 1;
			CString date;
			date = fileName.Mid(pos,2);
			date += folder.Right(4);
			m_fileList.AddItem(nItem,1,date);
			// ora
			pos += 2;
			CString time;
			time = fileName.Mid(pos,6);
			m_fileList.AddItem(nItem,2,time);
			// side
			m_fileList.AddItem(nItem,3,side);

			}
		}
*/

	}
//---------------------------------------------------


fileSearch.Close();
*pResult = 0;
}

void CDirBar::fileName2Field(CString& fileName,CString& folder) 
{

// Formato coil_SGGHHMMSS.csm
CString coil;
int pos = fileName.Find("_");
coil = fileName.Left(pos);
int nItem = m_fileList.AddRow((LPCSTR)coil);
// side 
pos += 1;
CString side = fileName.GetAt(pos);
// data
pos += 1;
CString date;
date = fileName.Mid(pos,2);
date += folder.Right(4);
m_fileList.AddItem(nItem,1,date);
// ora
pos += 2;
CString time;
time = fileName.Mid(pos,6);
m_fileList.AddItem(nItem,2,time);
// side
m_fileList.AddItem(nItem,3,side);

}

CString CDirBar::field2filePath(int row) 
{
CString filePath;

filePath = m_directory.GetSelectedPath();
filePath += "\\";
filePath += field2fileName(row); 

return filePath;
}

CString CDirBar::field2fileName(int row) 
{
CString fileName;

// Formato coil_SGGHHMMSS.csm
// coil
fileName = m_fileList.GetItemText(row,0);
// separator
fileName += "_";
// Side
fileName += m_fileList.GetItemText(row,3);
// GG
fileName += (m_fileList.GetItemText(row,1)).Left(2);
// ora
fileName += m_fileList.GetItemText(row,2);
// estensione
fileName += ".csm";

return fileName;
}
