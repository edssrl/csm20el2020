// Csm20El2013ReportView.h : interface of the CCsm20El2013ReportView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_Csm20El2013ReportVIEW_H__DD37F537_F5BE_4699_9BD0_95A95CBE8D20__INCLUDED_)
#define AFX_Csm20El2013ReportVIEW_H__DD37F537_F5BE_4699_9BD0_95A95CBE8D20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../Csm20El2013/graphview.h"

class CCsm20El2013ReportView : public CInitView
{
protected: // create from serialization only
	CCsm20El2013ReportView();
	DECLARE_DYNCREATE(CCsm20El2013ReportView)

// Attributes
public:
	CCsm20El2013ReportDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCsm20El2013ReportView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCsm20El2013ReportView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCsm20El2013ReportView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in Csm20El2013ReportView.cpp
inline CCsm20El2013ReportDoc* CCsm20El2013ReportView::GetDocument()
   { return (CCsm20El2013ReportDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Csm20El2013ReportVIEW_H__DD37F537_F5BE_4699_9BD0_95A95CBE8D20__INCLUDED_)
