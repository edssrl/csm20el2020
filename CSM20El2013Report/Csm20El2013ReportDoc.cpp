// Csm20El2013ReportDoc.cpp : implementation of the CCsm20El2013ReportDoc class
//

#include "stdafx.h"
#include "Csm20El2013Report.h"
#include "MainFrm.h"

#define MAIN


#include "Csm20El2013ReportDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportDoc

IMPLEMENT_DYNCREATE(CCsm20El2013ReportDoc, CLineDoc)

BEGIN_MESSAGE_MAP(CCsm20El2013ReportDoc, CLineDoc)
	//{{AFX_MSG_MAP(CCsm20El2013ReportDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportDoc construction/destruction

CCsm20El2013ReportDoc::CCsm20El2013ReportDoc() 
{
	// TODO: add one-time construction code here

}

CCsm20El2013ReportDoc::~CCsm20El2013ReportDoc()
{
}

BOOL CCsm20El2013ReportDoc::OnNewDocument()
{
// non usiamo CLIneDoc::OnNewDocument())
// xChe` altrimenti cerca di inizializzare CMainFrm che e` diverso
if (!CDocument::OnNewDocument())
	return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
//if (!UpdateBaseDoc())
//	return FALSE;


	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportDoc serialization

void CCsm20El2013ReportDoc::Serialize(CArchive& ar)
{
	CLineDoc::Serialize(ar);

}

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportDoc diagnostics

#ifdef _DEBUG
void CCsm20El2013ReportDoc::AssertValid() const
{
	CLineDoc::AssertValid();
}

void CCsm20El2013ReportDoc::Dump(CDumpContext& dc) const
{
	CLineDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCsm20El2013ReportDoc commands

/*-----------------------------------------------------------------

			SWAPLONG () 		routine swap long format
	
-----------------------------------------------------------------*/

union bytelong
	{
	unsigned char byte [4];
	unsigned long word;
	};

void swaplong (unsigned long *vett,int number)
{
union bytelong local;
int i;

unsigned long *lfrom;
unsigned char *bto;

lfrom = vett;
bto = (unsigned char *) vett;

for (i=0;i<number;i++)
	{
	local.word = *lfrom;     /* carico valore in union */
	
	bto [0] = local.byte[3];	/* swap */
	bto [1] = local.byte[2];
	bto [2] = local.byte[1];
	bto [3] = local.byte[0];
	bto += sizeof (long);	/* next word */
	lfrom ++;
	}
}	


BOOL CCsm20El2013ReportDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
// TODO: Add your specialized code here and/or call the base class

	
// Save docking control state
CMainFrame *mainFrame = (CMainFrame *) AfxGetMainWnd();
mainFrame->saveDockState();
	
return CLineDoc::CanCloseFrame(pFrame);
}
