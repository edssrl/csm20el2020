#if !defined(AFX_DALZATADETTAGLI_H__A63C509C_B080_457C_A040_F1E1E5C65A2B__INCLUDED_)
#define AFX_DALZATADETTAGLI_H__A63C509C_B080_457C_A040_F1E1E5C65A2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DAlzataDettagli.h : header file
//

#include "DAlzate.h"
#include "StaticExt.h"


/////////////////////////////////////////////////////////////////////////////
// CDAlzataDettagli dialog
#include "..\..\..\..\..\Library\UnicodeListCtrl\ListCtrl.h"

struct AlzataDettagli
	{
	int		c_numStrip;
	int     *c_difettiA;
	int     *c_difettiB;
	int     *c_difettiC;
	int     *c_difettiD;
	int  	*c_difettiTotale;
	int		*c_numBigHole;
	int     *c_numPerB;
	int     *c_difLamination;
	double  *c_densitaA;
	double  *c_densitaB;
	double  *c_densitaC;
	double  *c_densitaD;
	double  *c_densitaTotale;
	BOOL	c_laminationAlarm;
	AlzataDettagli()
	{c_difettiA=c_difettiB=c_difettiC=c_difettiD=c_difettiTotale=NULL;
	c_densitaA=c_densitaB=c_densitaC=c_densitaD=c_densitaTotale=NULL;c_difLamination=NULL;
	c_numBigHole = 0;c_numPerB=0;c_laminationAlarm = 0;};
	~AlzataDettagli(){Destroy();};
	void Destroy(void)
	{if (c_difettiA!=NULL){delete [] c_difettiA;c_difettiA=NULL;}
	if (c_difettiB!=NULL){delete [] c_difettiB;c_difettiB=NULL;}
	if (c_difettiC!=NULL){delete [] c_difettiC;c_difettiC=NULL;}
	if (c_difettiD!=NULL){delete [] c_difettiD;c_difettiD=NULL;}
	if (c_numBigHole!=NULL){delete [] c_numBigHole;c_numBigHole=NULL;}
	if (c_numPerB!=NULL){delete [] c_numPerB;c_numPerB=NULL;}
	if (c_difettiTotale!=NULL){delete [] c_difettiTotale;c_difettiTotale=NULL;}
	if (c_densitaA!=NULL){delete [] c_densitaA;c_densitaA=NULL;}
	if (c_densitaB!=NULL){delete [] c_densitaB;c_densitaB=NULL;}
	if (c_densitaC!=NULL){delete [] c_densitaC;c_densitaC=NULL;}
	if (c_densitaD!=NULL){delete [] c_densitaD;c_densitaD=NULL;}
	if (c_difLamination!=NULL){delete [] c_difLamination;c_difLamination=NULL;}
	if (c_densitaTotale!=NULL){delete [] c_densitaTotale;c_densitaTotale=NULL;}};
	void create (int numstrip)
	{c_numStrip = numstrip;
	c_difettiA = new int [c_numStrip+1];
	c_difettiB = new int  [c_numStrip+1];
	c_difettiC = new int [c_numStrip+1];
	c_difettiD = new int  [c_numStrip+1];
	c_numBigHole = new int  [c_numStrip+1];
	c_numPerB = new int  [c_numStrip+1];
	c_difettiTotale = new int [c_numStrip+1];
	c_densitaA = new double [c_numStrip+1];
	c_densitaB = new double [c_numStrip+1];
	c_densitaC = new double [c_numStrip+1];
	c_densitaD = new double [c_numStrip+1];
	c_densitaTotale = new double [c_numStrip+1];
	c_difLamination = new int [c_numStrip+1];};
};

 
class CDAlzataDettagli : public CDialog
{
// Construction
public:
	CDAlzataDettagli(CWnd* pParent = NULL);   // standard constructor
	void createStripPos (CLineDoc*	pDoc,FingerPos* pFingerPos,int elem);

	BOOL populateAlzataDettagli(CLineDoc *pDoc,AlzataDettagli *pAlzataDettagli,
		int elem,int alzata,int classBitmap=0xff); // default tutte le classi di difetti

	BOOL	checkDifLamination(int A,int B,int C, int D, int Big);

	void visualizza(CWnd *pWnd);

	CLineDoc*	c_pDocZ;
	CLineDoc*	c_pDocS;

// Dialog Data
	//{{AFX_DATA(CDAlzataDettagli)
	enum { IDD = IDD_ALZATA_DETTAGLI };
	CStatic	m_ctrlOkMsg2B;
	CStatic	m_ctrlOkMsg1B;
	CStatic	m_ctrlOkMsg2;
	CStatic	m_ctrlOkMsg1;
	CStaticExt	m_ctrlOkAlzata2;
	CStaticExt	m_ctrlOkAlzata1;
	gxListCtrl	m_ctrlDettagliZ;
	gxListCtrl	m_ctrlDettagliS;
	CString	m_alarmAS;
	CString	m_alarmAZ;
	CString	m_alarmBS;
	CString	m_alarmBZ;
	CString	m_thresholdAS;
	CString	m_thresholdAZ;
	CString	m_thresholdBS;
	CString	m_thresholdBZ;
	CString	m_alarmCS;
	CString	m_alarmCZ;
	CString	m_alarmDS;
	CString	m_alarmDZ;
	CString	m_thresholdCS;
	CString	m_thresholdCZ;
	CString	m_thresholdDS;
	CString	m_thresholdDZ;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDAlzataDettagli)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDAlzataDettagli)
	virtual BOOL OnInitDialog();
	afx_msg void OnAlzataContinua();
	afx_msg void OnAlzataNuova();
	afx_msg void OnAlzataScarto();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DALZATADETTAGLI_H__A63C509C_B080_457C_A040_F1E1E5C65A2B__INCLUDED_)
