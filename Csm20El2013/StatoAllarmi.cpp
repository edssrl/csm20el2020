//----------------------------------------------------------
//
//			Gestione stato allami
//
//----------------------------------------------------------

#include "stdafx.h"
#include "StatoAllarmi.h"

#define SEPCPU	6

#define SEPR1	8
#define SEPR2	6
#define SEPR3	16

#define SEPL1	8		// era 12, 22-04-10 messo 8 
#define SEPL2	4		// era 2

#define YSEP1	2

#define SIZE1	5		// era 3, 22-04-10 messo 5 

void StatoAllarmi::draw (CDC *pdc,CRect rect,BOOL topSide)
{

// ogni oggetto occupa SEP1 pixel 5 per simbolo 3 per separatore
// ogni SEP1 oggetti separatore  1.5
CPoint p0 (rect.left,rect.top+5);
CPoint p1 (p0);
CPen penOff,penOn,penFault,*oldPen;
penOff.CreatePen(PS_SOLID | PS_ENDCAP_SQUARE   ,SIZE1,RGB(96,96,96));
penOn.CreatePen(PS_SOLID | PS_ENDCAP_SQUARE   ,SIZE1,RGB(0,255,0));
penFault.CreatePen(PS_SOLID | PS_ENDCAP_SQUARE ,SIZE1,RGB(255,0,0));
oldPen = pdc->SelectObject(&penOff);

if (1)
{
for (int i=0;i<alNumCpu;i++)
	{
	p0.x += SEPCPU;
//	if ((i%SEP1) == 0)
	// No separatore aggiuntivo
//		p0.x +=SEP2;
	p1 = p0;
	pdc->MoveTo(p1);
	p1.y = rect.bottom - 6;
	switch(cpu[i])
		{
		default:
		case AL_OFF :
			pdc->SelectObject(&penOff);
			break;
		case AL_OK :
			pdc->SelectObject(&penOn);
			break;
		case AL_ON :
			pdc->SelectObject(&penFault);
			break;
		}
	pdc->LineTo(p1);
	}
}

if (1)
{
// ricevitori
p0.x += SEPR1;
for (int i=0;i<alNumReceiver;i+=2)
	{
	p0.x += SEPR1;
	if ((i%SEPR3) == 0)
		// separatore aggiuntivo
		p0.x +=SEPR2;
	// scheda bassa
	p1 = p0;
	// tolgo 5 sopra e 6 sotto = 11 a meta`
	// + 2 in basso spazio in mezzo SEP2
	p1.y += ((rect.Size().cy-11)/2 + YSEP1);
	pdc->MoveTo(p1);
	// Fine riga
	p1.y = rect.bottom - 6;
	switch(receiver[i])
		{
		default:
		case AL_OFF :
			pdc->SelectObject(&penOff);
			break;

		case AL_OK :
			pdc->SelectObject(&penOn);
			break;
		case AL_ON :
			pdc->SelectObject(&penFault);
			break;
		}
	pdc->LineTo(p1);
	
	// Caso numero bande dispari
	// viene disegnata mezza scheda non attiva
	// scheda sopra
	p1 = p0;
	pdc->MoveTo(p1);
	// Fine mezza riga 
	p1.y += ((rect.Size().cy-11)/2 - YSEP1);
	switch(receiver[i+1])
		{
		default:
		case AL_OFF :
			pdc->SelectObject(&penOff);
			break;
		case AL_OK :
			pdc->SelectObject(&penOn);
			break;
		case AL_ON :
			pdc->SelectObject(&penFault);
			break;
		}
	pdc->LineTo(p1);
	}
}

if (1)
{
p0.x += SEPL1;
for (int i=0;i<alNumLaser;i++)
	{
	p0.x += SEPL2;
	if ((i%SEPL1) == 0)
		// separatore aggiuntivo
		p0.x += SEPL2;
	if (i %2 == 1)
		{
		p1 = p0;
		pdc->MoveTo(p1);
		// Fine meta` riga 
		// parte alta
		p1.y += ((rect.Size().cy -11)/2)-YSEP1;
		}
	else
		{
		p1 = p0;
		p1.y += ((rect.Size().cy-11)/2 +YSEP1);
		pdc->MoveTo(p1);
		// Fine riga
		p1 = p0;	
		p1.y = rect.bottom - 6 ;
		}
	
	switch(laserKO[i])
		{
		default:
		case AL_OFF :
			pdc->SelectObject(&penOff);
			break;
		case AL_OK :
			pdc->SelectObject(&penOn);
			break;
		case AL_ON :
			pdc->SelectObject(&penFault);
			break;
		}
	pdc->LineTo(p1);
	}
}

CFont font,*pOldFont;
font.CreatePointFont (160,"Arial",pdc);

pOldFont = pdc->SelectObject(&font);

pdc->SetTextAlign(TA_LEFT | TA_TOP);
pdc->SetBkMode (TRANSPARENT);

CString str;
/*
if (topSide)
	str = "TOP";
else
	str = "BOTTOM";
*/
str = c_label;
p0.y = (rect.bottom - rect.top)/4;
p0.x += 16;

if (topSide)
	pdc->SetTextColor (RGB(0,255,0));

pdc->ExtTextOut (p0.x,p0.y,0,
		NULL,str,str.GetLength(),NULL);

pdc->SelectObject(pOldFont);

font.DeleteObject();

pdc->SelectObject(oldPen);
penOff.DeleteObject();
penOn.DeleteObject();
penFault.DeleteObject();
}
