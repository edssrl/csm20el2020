#if !defined(AFX_DOUBLEMULTILINE_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
#define AFX_DOUBLEMULTILINEVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MLineView.h : header file
//

// ByFC
// Attenzione Esiste baco in SubLine.draw
// se vuoi usare qs file verifica corrispondente graphview.cpp
// in SubLine::draw

/////////////////////////////////////////////////////////////////////////////
// CDoubleMLineView view

class CDoubleMLineView : public CView
{
protected:
	CDoubleMLineView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDoubleMLineView)
	ViewType vType; 

// Attributes
public:
SubLine trendLineTop[4];
SubLine trendLineBottom[4];
LineInfo lineInfoTop[4];
LineInfo lineInfoBottom[4];


Layout  layoutTop;
Layout  layoutBottom;
double lastActualMeterTop;
double lastActualMeterBot;
// valori in comune top e bottom
int c_numStepX;
int c_stepScaler;

// Operations
public:
	ViewType getViewType(void) {return(vType);};
	DOC_CLASS* getTopDocument();
	DOC_CLASS* getBottomDocument();

	CRect getRectTop(void);
	CRect getRectBottom(void);
	CRect getDrawRectTop(void);
	CRect getDrawRectBottom(void);

	CRect getMeterRect(void);
	void calcNumStepX (DOC_CLASS* pDoc);

	void updateTrend(CDC *pDC,DOC_CLASS* pDoc,
			SubLine *pTrendLine,CRect rectB,LineInfo* pLineInfo);
	void updateLabel(DOC_CLASS* pDoc,Layout* pLayout);


	void draw(void){CClientDC dc(this);OnDraw(&dc);};
	void onDraw(CDC* pDC);      // overridden to draw this view
	void onDraw(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,
		SubLine *pTrendLine,CRect rcBounds,LineInfo* pLineInfo);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDoubleMLineView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CDoubleMLineView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CDoubleMLineView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MULTILINEVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
