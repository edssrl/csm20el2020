#pragma once
//--------------------------------------------------------------------
//
// 05-07-21 - aggiunta definizione pdf printer 
// 05-07-21 - aggiunta gestione funzione virtual DoPreparePrinting per abilitazione print 2 file se docPath != ""
//
//
#define MICROSOFT_PDF_PRINTER _T("Microsoft Print to PDF")

// visualizzazione CPdfPrintView

class CPdfPrintView : public CView
{
	DECLARE_DYNCREATE(CPdfPrintView)

	CString		c_docName;
	CString		c_docPath;

protected:
	CPdfPrintView();           // costruttore protetto utilizzato dalla creazione dinamica
	virtual ~CPdfPrintView();

public:
	virtual void OnDraw(CDC* pDC);      // sottoposto a override per la creazione di questa visualizzazione
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
	void setDocName(CString docName){c_docName=docName;};
	void setDocPath(CString docPath){c_docPath=docPath;};
	void OnFilePrint();
	virtual BOOL DoPreparePrinting(CPrintInfo* pInfo);
protected:
	DECLARE_MESSAGE_MAP()
};


