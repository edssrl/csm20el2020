#if !defined(AFX_DOUBLETOWERVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
#define AFX_DOUBLETOWERVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TowerView.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CDoubleTowerView view

class CDoubleTowerView : public CView
{
protected:
	CDoubleTowerView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDoubleTowerView)
	ViewType vType; 

// Attributes
public:
CArray <ViewDensUnit,ViewDensUnit &> towerViewTop;
CArray <ViewDensUnit,ViewDensUnit &> towerViewBottom;
Layout  layoutTop;
Layout  layoutBottom;

double maxValY;

// Operations
public:
	DOC_CLASS* getTopDocument();
	DOC_CLASS* getBottomDocument();

	ViewType getViewType(void) {return(vType);};
	CRect getMeterRect(void);
	CRect getRectTop(void);
	CRect getRectBottom(void);
	CRect getDrawRectTop(void);
	CRect getDrawRectBottom(void);

	
	void draw(void){CClientDC dc(this);OnDraw(&dc);};
	void drawPosBobine1(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,CRect rBound);
	void drawPosBobine2(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,CRect rBound);
	void textPosBobine2(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB);
	void drawPosDiaframma(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB);
	void onDraw(CDC* pDC);      // overridden to draw this view
	void onDraw(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,
		CArray <ViewDensUnit,ViewDensUnit &> *pTowerView,CRect rcBounds);


	void updateLabel(DOC_CLASS* pDoc,Layout* pLayout);
	void updateExtra(CDC  *pDC, DOC_CLASS* pDoc,Layout* pLayout, CRect rectB);
	void updateTower(CDC  *pDC,DOC_CLASS* pDoc,Layout* pLayout, 
		   CArray <ViewDensUnit,ViewDensUnit &> *pTowerView, CRect rectB);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDoubleTowerView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CDoubleTowerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CDoubleTowerView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOWERVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
