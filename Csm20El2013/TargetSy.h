//------------------------------------------------------------
//
//
//
//					Target.h
//
//
//
//
//-------------------------------------------------------------



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "FontInfo.h"

// oggetto che viene visualizzato e scrolla
class CTarget
{
CSize size;
int			banda;				// identificativo
COLORREF	color;				// colore
public:
CTarget (void );
void setBanda(int b){banda = b;};
int getBanda (void ){return(banda);};
void draw(CPoint p,CDC *pdc );	
COLORREF	setColor(COLORREF rgb);
CSize	setSize(CSize &s);
CSize	getSize(void ){return(size);};

};

enum CTARGETSTATE {CTNEW,CTINSCOPE,CTOUTSCOPE};

class CTargetRow : public CArray <CTarget ,CTarget &>
{
int				pos;	// Posizione in metri sull'asse
CTARGETSTATE	state;	// stato di visualizzazione	
BOOL	c_excludeYText;
public:
	CTargetRow(void){pos = 0;state=CTNEW;c_excludeYText=FALSE;};
CTargetRow (CTargetRow& source);
~CTargetRow(void) {RemoveAll();};
CTargetRow &operator=(CTargetRow& source);
int getPos(void){return (pos);};
void setPos (int p){pos = p;};
void draw(CPoint p,CDC *pdc,int pxFC,int pxC,int nbC,int pxR);	
void setScope (CTARGETSTATE st){state = st;};
void excludeYText(BOOL v){c_excludeYText = v;};
CTARGETSTATE getScope (void) {return (state);};
};

class CTargetBoard : public CArray <CTargetRow ,CTargetRow &>
{
int lastPos;

// Dati di disegno
FontInfo font;
COLORREF textBkColor;
COLORREF drawBkColor;
int pxFirstCol;
int pxPerCol;		// numero di pixel per colonna
int nBandePerCol;	// numero di bande per colonna
int pxPerRow;
public:
	void reDraw(int index,CRect r,CDC *pdc);

CTargetBoard(void) {setFont(8);textBkColor = RGB(192,192,192);
				drawBkColor = RGB(0,0,0);lastPos=0;
				// cresce di 1000 metri per volta
				m_nGrowBy = 1000;};	
~CTargetBoard(void) {setEmpty();};
void setEmpty (void){for (int i=0;i<GetSize();i++)
						ElementAt(i).RemoveAll();
					RemoveAll();
					};
void draw(CRect r,CDC	*pdc);
void plot(CRect r,CDC	*pdc,double posTop,double posBottom);	

void scroll(int nr,CRect rInt,CRect rClip,CDC	*pdc)	;
// Pos in mm.
void add (int pos,CTarget &t,BOOL excludeLeftText=FALSE);
void setTextBkColor(COLORREF cl){textBkColor = cl;};
void setDrawBkColor(COLORREF cl){drawBkColor = cl;};

void setDrawAttrib(int pxFCol,int pxCol,int pxRow,int nbpCol)
	{pxFirstCol = pxFCol;pxPerCol=pxCol;pxPerRow=pxRow;nBandePerCol=nbpCol;};
void setFont (int sz,LPCTSTR name = "TimesNewRoman")
	{font.name = name;font.size = sz*10;};
int getLastPos(void ){return(lastPos);};
void setLastPos(int p){lastPos = p;};
};
