
#ifndef DIF_COIL_INCLUDE

#define DIF_COIL_INCLUDE

#define UPDATE_MASK_A	 0x01
#define UPDATE_MASK_B	 0x02
#define UPDATE_MASK_C	 0x04
#define UPDATE_MASK_D	 0x08
#define UPDATE_MASK_BIG  0x10

#define UPDATE_MASK_FROM_CLASS(cl) (0x01 << cl)
#define UPDATE_MASK_ALL  UPDATE_MASK_BIG|UPDATE_MASK_A|UPDATE_MASK_B
 

struct FilterBoundValues
{
// boundary
double	c_top;
double	c_bot;
int		c_bandaLeft;
int		c_bandaRight;

// limiti superiori
double c_limit[5];
// valori attuali
double c_value[5];
bool	c_clearOutside;
// tipo filtraggio
int		c_mode;		// 0=densita` h/m2, 1=densita` h/m, 2= numero h
FilterBoundValues(void)
{c_top=c_bot=0.;c_clearOutside=false;
c_bandaLeft=c_bandaRight=0;
c_limit[0]=c_limit[1]=c_limit[2]=c_limit[3]=c_limit[4]=0.;
c_value[0]=c_value[1]=c_value[2]=c_value[3]=c_value[4]=0.;
c_mode = 0;};
};


struct DifBanda 
{
int banda;
int numClassi;
int classi [NMAXCLASSI];
int c_bigHole;
int c_perB;		// periodici B
int modClassi[NMAXCLASSI+2];

DifBanda (void){numClassi = 0;banda = 0;c_bigHole=0;c_perB=0;
				classi[0]=classi[1]=classi[2]=classi[3]=0;
				clearMod();};
				
// Nuovo gruppo di funzioni per gestione dif in bande
// Symetal 30-01-09
void setNumClassi(int nc)
	{(nc<=NMAXCLASSI)?numClassi=nc:numClassi=NMAXCLASSI;};
int getNumClassi(void){return(numClassi);};

BOOL Set (int classe,int v){if (classe<=numClassi)
					{classi[classe] = v;
					// la considero modificata se aggiorno val>0
					if (v)	modClassi[classe]=TRUE;
					return TRUE;}
				return (FALSE);};
//---------------------------------
// funzione di filtraggio replativa ad una cella
// qui il vettore threshold contiene i valori massimi di qs cella
BOOL Filter (int threshold, int cl){
	if (cl < NMAXCLASSI)
		{// A-D
		if ((cl<numClassi)&&(classi[cl]>threshold)){classi[cl]=threshold;modClassi[cl]=TRUE;}
		}
	else
		// bighole
		if (c_bigHole>threshold){c_bigHole=threshold;modClassi[NMAXCLASSI]=TRUE;}
	return TRUE;};
//-----------------------------				
				
BOOL Add (int v,int cls){if (cls<NMAXCLASSI)
		{if(numClassi<(cls + 1))
				numClassi = cls+1;
			// la considero modificata se aggiorno val>0
			if (v)
				modClassi[cls]=TRUE;
			classi[cls] = v;
			return TRUE;}
		return (FALSE);};
void AddBigHole (int v){c_bigHole = v;
			// la considero modificata se aggiorno val>0
			if (v)	modClassi[NMAXCLASSI]=TRUE;};

void SetPerB (int v){c_perB = v;
			// la considero modificata se aggiorno val>0
			if (v)	modClassi[NMAXCLASSI+1]=TRUE;};


int BigHoleAt(void){return (c_bigHole);};
int PerBAt(void){return (c_perB);};
int laminCheck(void){
	int A,B,C,D,Big;
	A = classi[0];
	B = classi[1];
	C = classi[2];
	D = classi[3];
	Big = c_bigHole;
	if ((A+B+C+D+Big) < 3)
		return FALSE;
	if (((C+D+Big) == 0)&&
		(B <= 5)&&(A<50))
		return FALSE;

	if ((A+B+C+D+Big) < 3)
		return FALSE;
	return TRUE;
	;};


int ElementAt(int i){if (i<numClassi)return(classi[i]);
					else
						// 03-03-06
						// ritorna zero se no classe con difetti
						//return (-1);};
						return (0);};

// Copy constructor
DifBanda (DifBanda &source)
	{
	for (int i=0;i<source.numClassi;i++)
		classi[i] = source.classi[i];
	for (int i=0;i<source.numClassi+2;i++)
		modClassi[i] = source.modClassi[i];
	numClassi = source.numClassi;
	banda = source.banda;
	c_bigHole = source.c_bigHole;
	c_perB = source.c_perB;
	};

~DifBanda (void) {;};
void clear (void) {numClassi = 0;classi[0]=classi[1]=classi[2]=classi[3]=0;
	clearMod();};
void setBanda (int b) {banda = b;};
int  GetSize(void ) {return (numClassi);};
void clearMod (void){modClassi[0]=modClassi[1]=modClassi[2]=modClassi[3]=modClassi[4]=modClassi[5]=0;};

BOOL save(CFile *cf)
	{
	cf->Write((void *)&banda,sizeof(banda));
	cf->Write((void *)&numClassi,sizeof(numClassi));
	cf->Write((void *)&c_bigHole,sizeof(c_bigHole));
	cf->Write((void *)&c_perB,sizeof(c_perB));
	for (int i=0;i<numClassi;i++)
		{
		int v;
		v = classi[i];
		cf->Write((void *)&v,sizeof(v));
		}
	return TRUE;};
BOOL load(CFile *cf,int ver)
	{int nc;
	cf->Read((void *)&banda,sizeof(banda));
	cf->Read((void *)&nc,sizeof(nc));
	cf->Read((void *)&c_bigHole,sizeof(c_bigHole));
	cf->Read((void *)&c_perB,sizeof(c_perB));
	for (int i=0;i<nc;i++)
		{
		int v;
		cf->Read((void *)&v,sizeof(v));
		Add(v,i);
		}
	return TRUE;};

DifBanda& operator = (DifBanda& source)
{
clear();
for (int i=0;i<source.numClassi;i++)
	Add(source.classi[i],i);
for (int i=0;i<source.numClassi+2;i++)
	modClassi[i] = source.modClassi[i];
banda = source.banda;
c_bigHole = source.c_bigHole;
c_perB = source.c_perB;
return (*this);};
};


//-------------------------------------------------------------------
//
//	Csm20Tlmmetal
//	06-02-2010  AGGIUNTA funzione di hashing per bande in difMetro 
//  Max Numero di bande 256!!!!
//

// #define USE_DIFMETRO_SLOW_SEARCH
#include <algorithm>
#include <vector>


// preprocessor per gestire bighole come index 5
#define VAL_BYCLASS(x,cl) ((cl<4)?ElementAt(x).classi[cl]:ElementAt(x).c_bigHole)
#ifdef MAIN_DOC
// funzione di comparazione per sort filtraggio 
bool ValueComparefunction (std::pair<int, long> p1, std::pair<int, long> p2) { return (p1.second > p2.second);};
bool IndexComparefunction (std::pair<int, long> p1, std::pair<int, long> p2) { return (p1.first > p2.first); };
#else
extern bool ValueComparefunction (std::pair<int, long> p1, std::pair<int, long> p2);
extern bool IndexComparefunction (std::pair<int, long> p1, std::pair<int, long> p2);
#endif
/*
void std::sort(v.begin(), v.end(), [](const std::pair<int,int> &left, const std::pair<int,int> &right) {
    return left.second < right.second;
});
*/

struct DifMetro : public CArray <DifBanda,DifBanda &>
{
// Attribute
// 06-02-10 aggiunta fuznione di hash
// hash table delle bande, inizializzata a -1
// non posso + utilizzare bisezione xche` ora bande arrivano non piu` ordinate!
// passato da 256 a 512 size di hashTable, abbiamo 272 bande
int hashTable[512];
// riferimento metraggio
int posizione;
int totalCode [NMAXCLASSI];	// somma totali per qs metro per ogni code
int c_totalBigHole ;			// somma totali per qs metro bigHole
 // Method
DifMetro (void){clear();};
// Copy constructor
DifMetro (DifMetro &source)
	{
	memcpy((void *)totalCode,(void *)source.totalCode,sizeof(totalCode));
	c_totalBigHole = source.c_totalBigHole;
	posizione = source.posizione;
	SetSize(source.GetSize());
	for (int i=0;i<source.GetSize();i++)
		  SetAt(i,source.ElementAt(i));
	// 06-02-10 aggiunta funzione di hash
	memcpy(hashTable,source.hashTable,sizeof(hashTable));
	};
~DifMetro (void)
	{
	RemoveAll();
	};
void clear (void) {posizione = 0;
			for (int i=0;i<NMAXCLASSI;i++) totalCode[i] = 0;
			c_totalBigHole = 0;
			for (int i=0;i<GetSize();i++)ElementAt(i).clear();
			// 09-04-06 aggiunto RemoveAll 
			RemoveAll();
			// 06-02-10 aggiunta fuznione di hash
			memset(hashTable,-1,sizeof(hashTable));};
void clearBandaMod(void)
	{for (int i=0;i<GetSize();i++)ElementAt(i).clearMod();};

//--------------------------------------------------------
// funzione filtro di un metro lineare
// qui il vettore threshold contiene i valori massimi relativi al metro lineare
// torna valore di pinhole eliminati effettivamente

int Filter2Class (int pin2Left, int cl, int cellLeft, int cellRight,bool clearOutside){	
// partiamo contando quante celle difettose abbiamo per ogni classe, e calcoliamo totale 
// calcoliamo differenza tra valore attuale e soglia ( questo e` il numero di pinhole da eliminare )
// costruiamo vettore ordinato con numeri decrescenti ( indice zero -> valore massimo )
// togliamo un pinhole da ogni cella nel vettore ordinato, ripetendo il processo finche 
// il numero di pinhole da eliminare e` andato  zero
std::pair<int, long> couple;	// coppia cella/valore in una data classe
int vret = FALSE;
vector < std::pair<int, long> > sortVect;
vector < std::pair<int, long> > clearVect;
// classe A
int totalClass=0;
int numCell	= 0;
int numClear = 0;
for (int i=0;i<GetSize();i++)
	{
	int cell = ElementAt(i).banda;
	couple.first = i;
	couple.second = VAL_BYCLASS(i,cl);
	if ((cell>=cellLeft)&&
		(cell<=cellRight))
		{
		totalClass += couple.second;
		if (VAL_BYCLASS(i,cl) > 0)
			{// abbiamo pinhole di qs classe?	
			sortVect.push_back(couple);
			numCell ++;
			}
		}
	else
		{
		if(clearOutside&&
			(VAL_BYCLASS(i,cl) > 0))
			{// se esterno
			// abbiamo pinhole di qs classe?	
			clearVect.push_back(couple);
			numClear ++;
			}
		}
	}

int pinholeToSub = totalClass - pin2Left;
if ((numCell > 0)&&
	(pinholeToSub>0))
	{
	// ok ora sort array
	std::sort(sortVect.begin(),sortVect.end(),ValueComparefunction);
	// modifichiamo valori nel vettore
	// quanti ne devo eliminare?
	while (pinholeToSub > 0)
		{
		for(int i=0;i<sortVect.size();i++)
			{// decremento
			if ((sortVect.at(i).second > 0)&&
				(pinholeToSub > 0))
				{
				sortVect.at(i).second--;
				pinholeToSub--;
				}
			else
				break;
			}
		}
	// ok applichiamo il filtraggio
	for (int i=0;i<sortVect.size();i++)
		{
		ElementAt(sortVect[i].first).Filter(sortVect[i].second,cl);
		}
	}

if (numClear > 0)
	{// ok applichiamo il clear
	for (int i=0;i<clearVect.size();i++)
		{
		ElementAt(clearVect[i].first).Filter(0,cl);
		}
	}

calcTotalCode (TRUE,UPDATE_MASK_FROM_CLASS(cl));
return pinholeToSub;
}

//---------------------------------------------------------
// void fill  (int v) {for (int i=0;i<getNumSchede();i++)dBanda[i].fill(v);};

// Torna numero fori totali in qs metro per classe code
double getValNum(int code){double val = 0.;
		val = totalCode [code-'A'];
		return(val);};

BOOL addModify (DifBanda db)
	{
	int idx = getIndexFromScheda(db.banda);
	if (idx >= 0)
		{
		ElementAt(idx) = db;
		return TRUE;
		}
	// else
	Add(db);
	hashTable[db.banda] = GetUpperBound();
	return FALSE;
	}

DifBanda getDifBanda(int scheda)
	{DifBanda db;
	int idx;
	idx = getIndexFromScheda(scheda);
	if (idx >= 0)
		db = ElementAt(idx);
	return db;
	};


void calcTotalCode (BOOL doClear,int mask)
	{
	if (doClear)
		c_totalBigHole = 0;
	for (int code=0;code < NMAXCLASSI;code++)
		{
		if (!((0x001 << code) & mask))
			continue;
		if (doClear)
			{
			totalCode[code] = 0;		
			}
		for (int i=0;i<GetSize();i++)
			{
			int iv;
			// Torna -1 se invalido
			iv = ElementAt(i).ElementAt(code);
			if (iv > 0)
				totalCode[code] += iv;
			}
		}
	if (mask & UPDATE_MASK_BIG)
		{
		for (int i=0;i<GetSize();i++)
			{
			int iv;
			iv = ElementAt(i).BigHoleAt();
			if (iv > 0)
				c_totalBigHole += iv; 
			}
		}
	}

double getTotalBig(void)
	{
	return (c_totalBigHole);
	};

// 2006 Csm20D torna numero fori bigHole da bandaSx a bandaDx
// 2008 se bandaSx <0 allora leftmost banda
//		se bandaDx <0 allora rightMost banda
double getValBigHole(int bandaSx,int bandaDx)
	{
	if (GetSize() <= 0)
		return 0;
	double val = 0.;
	if (bandaSx < 0)
		bandaSx = ElementAt(0).banda;
	if (bandaDx < 0)
		// +1 perche` se e` la piu` dx verrebbe esclusa
		bandaDx = ElementAt(GetSize()-1).banda+1;
	for (int i=0;i<GetSize();i++)
		{
		if ((ElementAt(i).banda >= bandaSx)&&
			(ElementAt(i).banda < bandaDx))
			val += (double)ElementAt(i).BigHoleAt();
		}
		return(val);};

// 2006 Csm20D torna numero repB da bandaSx a bandaDx
double getValPerB(int bandaSx,int bandaDx)
	{
	double val = 0.;
	for (int i=0;i<GetSize();i++)
		{// aggiunto <= per includere banda di fine 
		if ((ElementAt(i).banda >= bandaSx)&&
			(ElementAt(i).banda <= bandaDx))
			val += (double)ElementAt(i).PerBAt();
		}
		return(val);};

// 2014 torna BOOL laminHole da bandaSx a bandaDx
BOOL existLaminDef(int bandaSx,int bandaDx)
	{
	
	int Big = getValBigHole(bandaSx,bandaDx);
	int A = getValNum(bandaSx,bandaDx,'A');
	int B = getValNum(bandaSx,bandaDx,'B');
	int C = getValNum(bandaSx,bandaDx,'C');
	int D = getValNum(bandaSx,bandaDx,'D');

	if ((A+B+C+D+Big) < 3)
		return FALSE;
	if (((C+D+Big) == 0)&&
		(B <= 5)&&(A<50))
		return FALSE;
	
	return TRUE;};

// 2008 Csm20D boolean esiste 
double existValPerB(int bandaSx,int bandaDx)
	{
	return((getValPerB(bandaSx,bandaDx)>0)?1:0);
	}


// torna numero fori bigHole per la banda scheda
double getValBigHole(int scheda)
	{
	double val = 0.;
	int idx = getIndexFromScheda(scheda);
	if (idx >= 0)
		val = (double)ElementAt(idx).BigHoleAt();
	return(val);};

// torna numero fori bigHole per la banda scheda
double getValBigHoleUpdated(int scheda)
	{
	double val = 0.;
	int idx = getIndexFromScheda(scheda);
	if (idx >= 0)
		{
		if (ElementAt(idx).modClassi[4])
			val = (double)ElementAt(idx).BigHoleAt();
		}
	return(val);};

// torna fori PerB per la banda scheda
double getValPerB(int scheda)
	{
	double val = 0.;
	int idx = getIndexFromScheda(scheda);
	if (idx >= 0)
		val = (double)ElementAt(idx).PerBAt();
		return(val);};

// torna fori PerB per la banda scheda
double getValPerBUpdated(int scheda)
	{
	double val = 0.;
	int idx = getIndexFromScheda(scheda);
	if (idx >= 0)
		{
		if (ElementAt(idx).modClassi[5])
			val = (double)ElementAt(idx).PerBAt();
		}	
	return(val);};

double existValPerB(int scheda)
	{
	return(getValPerB(scheda)>0?1.0:0.0);
	}


// 2004 Csm20CA torna numero fori di classe code da bandaSx a bandaDx
// si usa getIndexFromPos con hash table
// !! Modificata ora bande non piu` ordinate !!!
double getValNum(int bandaSx,int bandaDx,int code)
	{
	double val = 0.;
	int idx;
	for (int i=bandaSx;i<=bandaDx;i++)
		{
		idx = getIndexFromScheda(i);
		if (idx >= 0)
			{
			val += (double)ElementAt(idx).ElementAt(code-'A');
			}
		}
	return(val);};

// torna numero fori di classe code per la banda scheda
double getValNum(int scheda,int code)
	{
	double val = 0.;
	int idx = getIndexFromScheda(scheda);
	if (idx < 0)
		return 0.;
	if (ElementAt(idx).banda == scheda)
		{
		val = (double)ElementAt(idx).ElementAt(code-'A');
		}
	return(val);};

// se updated allora torna fori solo per bande recentemente modificate
double getValNumUpdated(int scheda,int code)
	{
	double val = 0.;
	int idx = getIndexFromScheda(scheda);
	if (idx < 0)
		return 0.;
	if (ElementAt(idx).banda == scheda)
		{
		if (ElementAt(idx).modClassi[code-'A'])
			val = (double)ElementAt(idx).ElementAt(code-'A');
		}
	return(val);};

// torna numero fori  per la banda scheda
double getValNumInScheda(int scheda)
	{double val = 0.;
	int sz = GetSize();
	int idx = getIndexFromScheda(scheda);
	if (idx < 0)
		return 0.;
	if (ElementAt(idx).banda == scheda)
		{
		int szEl = ElementAt(idx).GetSize();
		for (int j=0;j<szEl;j++)
			val += (double)ElementAt(idx).ElementAt(j);
		}
	return(val);};


// Eliminata ricerca per bisezione:
// con 1 pacchetto per classe non si puo` usare
// bande !ordinate ma ordine casuali
// inserita funzione di hash
int getIndexFromScheda(int scheda)
	{int idx = -1;
	// Non ci sono elementi
	if (GetSize() <= 0) 
		return (idx);
#ifdef USE_DIFMETRO_SLOW_SEARCH
	for (int i=0;i<GetSize();i++)
		{
		if (ElementAt(i).banda == scheda)
			{
			return i;
			}
		}
	return -1;
#else
//------------------------------------------------------------
// Hash function !
//

	idx = hashTable[scheda];

return (idx);
#endif
};



BOOL save(CFile *cf)
	{int numSchede;
	cf->Write((void *)&totalCode,sizeof(totalCode));
	cf->Write((void *)&c_totalBigHole,sizeof(c_totalBigHole));
	// Free Unused memory
	FreeExtra();
	cf->Write((void *)&posizione,sizeof(posizione));
	numSchede = GetSize();
	cf->Write((void *)&numSchede,sizeof(numSchede));
	for (int i=0;i<numSchede;i++)
		if (!ElementAt(i).save(cf)) return FALSE;
	return TRUE;};

BOOL load(CFile *cf,int ver)
	{int numSchede;
	cf->Read((void *)&totalCode,sizeof(totalCode));
	if (ver >= 3)
		cf->Read((void *)&c_totalBigHole,sizeof(c_totalBigHole));
	cf->Read((void *)&posizione,sizeof(posizione));
	cf->Read((void *)&numSchede,sizeof(numSchede));
	SetSize(numSchede);
	for (int i=0;i<numSchede;i++)
		{
		if (!ElementAt(i).load(cf,ver)) return FALSE;
		int banda = ElementAt(i).banda;
		hashTable[banda] = i;
		}
	return TRUE;};
DifMetro& operator = (DifMetro& source)
	 	{
		clear();
		posizione = source.posizione;
		if (source.GetSize() != GetSize())
			SetSize(source.GetSize());
		for (int i=0;i<source.GetSize();i++)
			  SetAt(i,source.ElementAt(i));
		memcpy((void *)totalCode,(void *)source.totalCode,sizeof(totalCode));
		c_totalBigHole = source.c_totalBigHole;
		// 06-02-10 aggiunta funzione di hash
		memcpy(hashTable,source.hashTable,sizeof(hashTable));
		return (*this);};

};

struct QuattroClassi
	{
	double totalA;
 	double totalB;
	double totalC;
	double totalD;
	double totalBig;
	QuattroClassi(void){totalA = 0;totalB = 0;totalC = 0;totalD = 0;totalBig = 0;};
	};



typedef  CArray <int,int > FingerList;
typedef  CArray <int,int > ClassBFilterList;

	

class DifCoil : public CArray <DifMetro,DifMetro &>
{
double meter;			// posizione attuale
// gestione rotoli saldati
// il rotolo puo` contenere difetti di un rotolo precedente: startMeter > 0
// il rotolo puo` contenere difetti di un rotolo successivo: endMeter < meter  
double c_startMeter;	// posizione primo metro reale di qs rotolo madre

// double trueSizeEncod;	// Dimensione corretta tra due interrupt
double largeSize;		// larghezza singole bande mm
int numeroClassi;
int numeroSchede;
int c_numSchedeLeft;	// numero chede ( bande ) sulla testa top (left)
int c_baseSplitSchede;	// indice scheda su cui pacchetti splittati
						// inibisce in SetAt duplicazione totali per classe/scheda
						// del primo gruppo gia` conteggiato 

int baseStepX;			// in mm

CArray <QuattroClassi,QuattroClassi &> c_totali;


public:
CArray <double,double &> c_sogliaAllarme;


// nuova parte di filtraggio
bool	applyFilter2(FilterBoundValues fp)
	{
	long iFrom = getLocalIndexFromPos(fp.c_top,true);
	long iTo = getLocalIndexFromPos(fp.c_bot,false);
	long bandaSx = fp.c_bandaLeft;
	long bandaDx = fp.c_bandaRight;

// costruisco vecto da ordinare per partire con i piu` difettosi
	std::pair<int, long> couple;	// coppia cella/valore in una data classe
	int vret = FALSE;
	vector < std::pair<int, long> > sortVect;

	// ottenuti indici estremi filtraggio
	// calcolo pinhole da eliminare per metro, dipende dalla modalita` di filtraggio
	for(int cl=0;cl<5;cl++)
	{
	sortVect.clear();
	sortVect.reserve(this->GetCount()+1);
	// sort delle posizioni
	double totalPin=0;
	double averagePin=0.;
	for(int i=iFrom;i<=iTo;i++)
		{
		couple.second = i;
		couple.first = ElementAt(i).getValNum('A'+cl);
		if (couple.first > 0)
			{// forse per qs classe non ci sono difetti in qs metro
			totalPin += couple.first;
			sortVect.push_back(couple);	
			}
		}
	if (sortVect.size() == 0)
		continue;
	averagePin = totalPin/((sortVect.size())>0?sortVect.size():1);
	std::sort(sortVect.begin(),sortVect.end(),ValueComparefunction);
	//---------
	double pin2Left=0;		// pin da lasciare
	switch(fp.c_mode)
		{
		case 0: // limite a m2
			{double metroArea = numeroSchede*largeSize; //larghezza totale in mm 
			metroArea /= 1000.;		// larghezza totale in m, lunghezza 1m quindi e` anche area in m
			// limite a metro lineare
			pin2Left = fp.c_limit[cl]*metroArea;
			}break;
		case 1: // limite a m lineare
			{
			pin2Left = fp.c_limit[cl];
			}
			break;
		case 2: // limite numero fori in tutta area, quindi lunghezza
			{//numero fori per metro difettoso reale nel range 
			pin2Left = fp.c_limit[cl]/sortVect.size();
			}break;
		}
	int pinholeMinus = 0;
	for(int i=0;i<sortVect.size();i++)
		{
		// ora dobbiamo spalmare i resti 
		int nh = (int) pin2Left;		// parte intera	
//		int rh = (int)((pin2Left-nh)>0?1./(pin2Left-nh):0);
		// calcolo resto con complemento a 1
		int rh = (int)(10-(pin2Left-nh)*10);
		int restoPin = ((rh>0)&&(i%rh)==0)?1:0;
		// per ogni metro, torna pinhole ancora da sottrarre, se zero abbiamo levato tutto quello che si doveva 
		if ((nh+restoPin) < sortVect[i].first)
			// solo se abbiamo abbastanza fori
			pinholeMinus += ElementAt(sortVect[i].second).Filter2Class(nh+restoPin,cl,bandaSx,bandaDx,fp.c_clearOutside);
		}
	if (pinholeMinus > 0)
		{

		}
	// clear outside
	if (fp.c_clearOutside)
		{
		for(int i=0;i<iFrom;i++)
			{
			ElementAt(i).Filter2Class(0,cl,0,numeroSchede,fp.c_clearOutside);
			}
		for(int i=iTo+1;i<=this->GetUpperBound();i++)
			{
			ElementAt(i).Filter2Class(0,cl,0,numeroSchede,fp.c_clearOutside);
			}
	
		}
	}
// ricalcolo totali
calcTotalCode();
// update cache
return true;
};


// nuova parte di filtraggio
bool	applyFilter(FilterBoundValues fp)
	{
	long iFrom = getLocalIndexFromPos(fp.c_top,true);
	long iTo = getLocalIndexFromPos(fp.c_bot,false);
	long bandaSx = fp.c_bandaLeft;
	long bandaDx = fp.c_bandaRight;
	if((iFrom < 0)||(iTo<0))
		// non ci sono elementi
		return false;

	if ((ElementAt(iFrom).posizione < fp.c_top)&&
		(iFrom < (this->GetCount()-1) ) )
		iFrom ++;
	if (
		(ElementAt(iTo).posizione > fp.c_bot)&&
		(iTo > 0) )
		iTo --;
	if (iFrom > iTo) iFrom = iTo;
		
	if(ElementAt(iFrom).posizione < fp.c_top) return false;
	if(ElementAt(iTo).posizione > fp.c_bot) return false;


// costruisco vecto da ordinare per partire con i piu` difettosi
	std::pair<int, long> couple;	// coppia cella/valore in una data classe
	int vret = FALSE;
	vector < std::pair<int, long> > sortVect;

	// ottenuti indici estremi filtraggio
	// calcolo pinhole da eliminare per metro, dipende dalla modalita` di filtraggio
	for(int cl=0;cl<5;cl++)
	{
	sortVect.clear();
	sortVect.reserve(this->GetCount()+1);
	// sort delle posizioni
	double totalPin=0;
	double averagePin=0.;
	for(int i=iFrom;i<=iTo;i++)
		{
		couple.second = i;
		couple.first = ElementAt(i).getValNum('A'+cl);
		if (couple.first > 0)
			{// forse per qs classe non ci sono difetti in qs metro
			totalPin += couple.first;
			sortVect.push_back(couple);	
			}
		}
	if (sortVect.size() == 0)
		continue;

	averagePin = totalPin/((sortVect.size())>0?sortVect.size():1);
	// modo 2 
	// sottraiamo ad ogni metro 1 difetto alla volta
	std::sort(sortVect.begin(),sortVect.end(),ValueComparefunction);
	int pin2Remove = totalPin - fp.c_limit[cl]; 
	//---------
	double pin2Left=0;		// pin da lasciare
	switch(fp.c_mode)
		{
		case 0: // limite a m2
			{double metroArea = largeSize; //larghezza totale in mm 
			metroArea /= 1000.;		// larghezza totale in m, lunghezza 1m quindi e` anche area in m2
			// limite a metro lineare
			pin2Left = fp.c_limit[cl]*metroArea;
			}break;
		case 1: // limite a m lineare
			{
			pin2Left = fp.c_limit[cl];
			}
			break;
		case 2: // limite numero fori in tutta area, quindi lunghezza
			{
			pin2Remove = totalPin - fp.c_limit[cl];
			while(pin2Remove>0)
				{
				for(int i=0;i<sortVect.size();i++)
					{
					if (sortVect[i].first>0)
						{
						sortVect[i].first--;
						pin2Remove--;
						}
					if(pin2Remove<=0)break;
					}
				}		
			}break;
		}
	int nh=pin2Left;
	for(int i=0;i<sortVect.size();i++)
		{
		if(fp.c_mode==2) nh = (int)sortVect[i].first;
		ElementAt(sortVect[i].second).Filter2Class(nh,cl,bandaSx,bandaDx,fp.c_clearOutside);
		}
	// clear outside
	if (fp.c_clearOutside)
		{
		for(int i=0;i<iFrom;i++)
			{
			ElementAt(i).Filter2Class(0,cl,0,numeroSchede,fp.c_clearOutside);
			}
		for(int i=iTo+1;i<=this->GetUpperBound();i++)
			{
			ElementAt(i).Filter2Class(0,cl,0,numeroSchede,fp.c_clearOutside);
			}
	
		}
	}
// ricalcolo totali
calcTotalCode();
// update cache
return true;
};


public:
int Add (DifMetro &dMetro,int updateTotaliMask)
	{
	updateTotal(dMetro,updateTotaliMask);
	return(CArray<DifMetro,DifMetro &>::Add(dMetro));
	};

// funzione privata di load e Add somma dif dMetro a totali
int updateTotal (DifMetro &dMetro,int updateTotaliMask)
	{
	for (int i=0;i<numeroSchede;i++)
		{
		if ((numeroClassi > 0)&&(updateTotaliMask&UPDATE_MASK_A))
			c_totali[i].totalA += dMetro.getValNum(i,'A');
		if ((numeroClassi > 1)&&(updateTotaliMask&UPDATE_MASK_B))
			c_totali[i].totalB += dMetro.getValNum(i,'B');
		if ((numeroClassi > 2)&&(updateTotaliMask&UPDATE_MASK_C))
			c_totali[i].totalC += dMetro.getValNum(i,'C');
		if ((numeroClassi > 3)&&(updateTotaliMask&UPDATE_MASK_D))
			c_totali[i].totalD += dMetro.getValNum(i,'D');
		// bigHole
		if (updateTotaliMask&UPDATE_MASK_BIG)
			c_totali[i].totalBig += dMetro.getValBigHole(i);
		}
	return 1;
	};

// inserita 02-11-2004
// 2-06-09 inserita updateTotali, se usata x A e B duplica totali
void SetAt (int index,DifMetro &dMetro,int updateTotaliMask)
	{
	for (int i=c_baseSplitSchede;(i<numeroSchede);i++)
		{
		if ((numeroClassi > 0)&&(updateTotaliMask&UPDATE_MASK_A))
			c_totali[i].totalA += dMetro.getValNumUpdated(i,'A');
		if ((numeroClassi > 1)&&(updateTotaliMask&UPDATE_MASK_B))
			c_totali[i].totalB += dMetro.getValNumUpdated(i,'B');
		if ((numeroClassi > 2)&&(updateTotaliMask&UPDATE_MASK_C))
			c_totali[i].totalC += dMetro.getValNumUpdated(i,'C');
		if ((numeroClassi > 3)&&(updateTotaliMask&UPDATE_MASK_D))
			c_totali[i].totalD += dMetro.getValNumUpdated(i,'D');
		// bigHole
		if (updateTotaliMask&UPDATE_MASK_BIG)
			c_totali[i].totalBig += dMetro.getValBigHoleUpdated(i);
		}
	CArray<DifMetro,DifMetro &>::SetAt(index,dMetro);
	};
int getMeterBaseStepX(void) {return(baseStepX/1000);};
void setMmBaseStepX(int millimetri) {baseStepX = millimetri;};
void setBaseSplitSchede(int index) {c_baseSplitSchede = index;};

// torna valore densita` per metro quadrato
double getTotalDensity(int classe)
	{double d,area;
	area = getMeter()*(0.001)*largeSize;
	if (area==0.) area = DBL_MIN;
		//return -1; 
	d = (double) getTotDifetti(classe,0,getMeter())/area;
	return(d);};

// torna valore densita` per metro quadrato
double getTotalDensity(int classe,double from,double to)
	{double d,area;
	area = (0.001)*(to - from)*largeSize;
	if (area==0.) area = DBL_MIN;
		//return -1; 
	d = (double) getTotDifetti(classe,from,to)/area;
	return(d);};
// torna valore densita` per metro quadrato in strip, left e right sono bande
double getTotalDensity(int classe,double from,double to,double left,double right,double width)
	{double d,area;
	if (width <= 0)
		area = (0.001)*(to - from)*(right-left)*SIZE_BANDE;
	else
		area = (0.001)*width*(to-from);
	if (area==0.) area = DBL_MIN;
		//return -1; 
	d = (double) getTotDifetti(classe,from,to,left,right)/area;
	return(d);};

// elval 2013
// torna valore densita` per metro quadrato in strip, width in mm
double getTotalDensity(int classe,double from,double to,double width)
	{double d,area;
	area = (0.001)*width*(to-from);
	if (area==0.) area = DBL_MIN;
		//return -1; 
	d = (double) getTotDifetti(classe,from,to)/area;
	return(d);};

double getDensityLevel(int classe,double densityCalcLength,double pos=-1.,double minFrom=0.)
	{// default position
	if (pos == -1.)
		pos = getMeter();
	// larghezza in metri
	double width = largeSize ;
	if (width <= 0) width = DBL_MIN;
	double from = pos - ((densityCalcLength*1000.)/width);
	// 30-04-14 inserito limite inferiore posizione
	// gestiamo in quasto caso eventuali posizioni intermedie 
	if (from < minFrom)
		from = minFrom;
		// difetti
	double densityLevel = getTotDifetti(classe,from,pos);
		// difetti Per m2
		// densityLevel /=  ((position-from)/1000.)*width; 
		// difetti per m2 in 1000 m
		// densityLevel *= 1000.;
		// difetti per m2 in x metri che dipende dalle impostazioni threshold
	return densityLevel;};
	
// calcola totali per metro
void calcTotalCode(void)
{
// clear and init totali
c_totali.RemoveAll();
c_totali.SetSize(numeroSchede);
// calcolo totali sempre da metro 0
// ora con saldature bisogna trovare indice 

int zeroIndex = getIndexFromPos(1);
if (zeroIndex < 0)
	return;

for (int i=zeroIndex;i<GetSize();i++)
	{
	// calcola totali per metro
	ElementAt(i).calcTotalCode(TRUE,UPDATE_MASK_ALL);
	// aggiorna totali per schede
	for (int j=0;j<numeroSchede;j++)
		{
		if (numeroClassi > 0)
			c_totali[j].totalA += ElementAt(i).getValNum(j,'A');
		if (numeroClassi > 1)
			c_totali[j].totalB += ElementAt(i).getValNum(j,'B');
		if (numeroClassi > 2)
			c_totali[j].totalC += ElementAt(i).getValNum(j,'C');
		if (numeroClassi > 3)
			c_totali[j].totalD += ElementAt(i).getValNum(j,'D');
		// bigHole
		c_totali[j].totalBig += ElementAt(i).getValBigHole(j);
		}
	}
}

DifCoil(void )
	{meter = 0.;// Init Schede
	c_startMeter = 0.;
	largeSize = 1500.; // Default 1.5 mt
	numeroClassi = 4;
	// 20=04-20
	c_numSchedeLeft = 0;
	numeroSchede = 7;
	// 02-11-2004
	c_baseSplitSchede = 0;	// indice prima scheda/difetti in arrivo con secondo pacchetto
	m_nGrowBy = 1000;	// cresce di 1000 metri per volta
	baseStepX = 1000;	// step ogni metro default
	};
~DifCoil (void)
		{
		for (int i=0;i<GetSize();i++)
			ElementAt(i).RemoveAll();
		RemoveAll();
		c_sogliaAllarme.RemoveAll();
		c_totali.RemoveAll();
		};

// Copy operator
DifCoil& operator = (DifCoil& source)
{
clear();
meter = source.meter;						// posizione attuale
largeSize = source.largeSize;		// larghezza singole bande mm
numeroClassi = source.numeroClassi;
numeroSchede = source.numeroSchede;
c_numSchedeLeft = source.c_numSchedeLeft;
baseStepX = source.baseStepX;
c_totali.RemoveAll();


for (int i=0;i<source.c_totali.GetSize();i++)
	{
	c_totali.Add (source.c_totali.ElementAt(i));
	}

for (int i=0;i<source.GetSize();i++)
	{
	Add (source.ElementAt(i),0);
	}
return (*this);
};


void clear(int numSchede,int nClassi,int numSchedeLeft){
			numeroClassi = nClassi;
			numeroSchede = numSchede;
			c_numSchedeLeft = numSchedeLeft;
			clear();};
void clear(void )
			{
			for (int i=0;i < GetSize();i++)
				ElementAt(i).clear();
			RemoveAll();
			meter = 0.;
			c_startMeter = 0.;
			c_totali.RemoveAll();
			c_totali.SetSize(numeroSchede);
			};
// Implementazione delle interfacce di GroupSchede 
public:

// utilizzare getMeter per tutti i riferimenti alla posizione reale nell'asse
double getMeter (void)
		{return(meter);};
// utilizzare getStripMeter
double getStripMeter (void){return(meter);};



double getAllarme (int code){if ((code - 'A') < c_sogliaAllarme.GetSize()) 
			return(c_sogliaAllarme[code-'A']);
			return (0.);};
int getNumSchede (void){return(numeroSchede);};
int getNumClassi (void){return(numeroClassi);};
int getNumSchedeLeft (void){return(c_numSchedeLeft);};

// da utilizzare per impostare posiz
void setMeter (double m)
		{meter = m;};

void setStartMeter (double m){c_startMeter=m;};

void setLargeSize (int mm){largeSize = (double ) mm;};
double getLargeSize (void){return(largeSize);};
//void setTrueSizeEncod (double dmm){trueSizeEncod = dmm;};

double getIncMeter (void){return(1.);};

// torna numero difetti della classe code
// disp metri dalla posizione attuale 
double getValAlarmNumLStep(int code,int disp = 0)
	{double v = getValNumAt(code,(int) (getMeter()-disp));
	if (v >= getAllarme(code))
		return (1.); // era v
	return (0.);};

double getValNumLStep(int code,int disp = 0)
	{
	return(getValNumAt(code,(int) (getMeter()-disp)));};

int getMeterIdxLStep(int disp = 0)
	{
	int pos = (int) (getMeter()-disp);
	int idx = getIndexFromPos(pos);
	return (idx);
	};

double getValNumLBig(int disp = 0)
	{
	return(getValBigAt((int) (getMeter()-disp)));};

// algoritmo di calcolo dell'indice
// a partire dalla posizione in metri
// torna -1 se index non trovato
// Sfrutto monotonicita` delle posizioni crescenti
int getOIndexFromPos(int pos)
	{int idx = -1;
	// Non ci sono elementi
	if (GetSize() <= 0) 
		return (idx);
	// Ultima posizione memorizzata < della posizione cercata
	// sicuramente NON esiste
	if ((int )ElementAt(GetUpperBound()).posizione < pos)
		return (idx);
	// procedo con ricerche a ritroso
	for (int i=GetUpperBound();i>=0;i--)
		{
		if ((int )ElementAt(i).posizione == pos)
			{
			idx = i;
			break;
			}
		}
	return (idx);};

// somma valori di tutte le schede per quel codice
double getValNumAt(int code,int pos)
	{
	double val=0.;
	int idx = getIndexFromPos(pos);
	if (idx >= 0)
		val = ElementAt(idx).getValNum(code);
	return(val);};

// somma valori di tutte le schede per quel codice
double getValBigAt(int pos)
	{
	double val=0.;
	int idx = getIndexFromPos(pos);
	if (idx >= 0)
		val = ElementAt(idx).getTotalBig();
	return(val);};

// sostituita una ricerca per bisezione
int getIndexFromPos(int pos)
	{int idx = -1;
	// Non ci sono elementi
	if (GetSize() <= 0) 
		return (idx);
	// Ultima posizione memorizzata < della posizione cercata
	// sicuramente NON esiste
	if ((int )ElementAt(GetUpperBound()).posizione < pos)
		return (idx);
	// sostituita una ricerca per bisezione
	// procedo con ricerche a ritroso

	// provo con idx == scheda
	if ((pos >= 0)&&
		(GetUpperBound() >= pos))
		{
		if ((int )ElementAt(pos).posizione == pos)
			return (pos);
		}

	int lIndex = 0;
	int rIndex = GetUpperBound();
	// fix bug 26-04-05 da > a >= 
	// se 1 solo difetto non lo trova!
	while ((rIndex - lIndex) >= 0)
		{
		int lPos;
		int rPos;
		rPos = (int )ElementAt(rIndex).posizione;
		if (rPos == pos)
			{
			idx = rIndex;
			break;
			}
		lPos = (int )ElementAt(lIndex).posizione;
		if (lPos == pos)
			{
			idx = lIndex;
			break;
			}
		int mIndex = lIndex + (rIndex - lIndex)/2;
		int mPos = (int )ElementAt(mIndex).posizione;
		// Che fortuna !!
		if (pos == mPos)
			{
			idx = mIndex;
			break;
			}
		// cambio estremi
		if (pos > mPos)
			{// Bisezione funzioni continue
			// not found
			if (lIndex == mIndex)
				break;
			lIndex = mIndex;
			}
		if (pos < mPos)
			{// Not found
			if (rIndex == mIndex)
				break;
			rIndex = mIndex;
			}
		}
	return (idx);};




// sostituita una ricerca per bisezione
// torna indice prima riga difettosa, forward = true ascending order
// forward = false descending order
// usata nel filtraggio
int getLocalIndexFromPos(int pos,bool forward)
	{int idx = -1;
	// Non ci sono elementi
	if (GetSize() <= 0) 
		return (idx);
	// Ultima posizione memorizzata < della posizione cercata in modo forward
	// sicuramente NON esiste
	if (((int )ElementAt(GetUpperBound()).posizione < pos)&&
		forward)
		return (idx);
	// sostituita una ricerca per bisezione
	// procedo con ricerche a ritroso

	// provo con idx == scheda
	if ((pos >= 0)&&
		(GetUpperBound() >= pos))
		{
		if ((int )ElementAt(pos).posizione == pos)
			return (pos);
		}
	int a = 0;
	if(pos == 114)
		a=1;
	int lIndex = 0;
	int rIndex = GetUpperBound();
	// fix bug 26-04-05 da > a >= 
	// se 1 solo difetto non lo trova!
	while ((rIndex - lIndex) >= 0)
		{
		int lPos;
		int rPos;
		rPos = (int )ElementAt(rIndex).posizione;
		if (rPos == pos)
			{// uguale lato dx
			idx = rIndex;
			break;
			}
		lPos = (int )ElementAt(lIndex).posizione;
		if (lPos == pos)
			{// uguale lato sx
			idx = lIndex;
			break;
			}
		int mIndex;
		if (forward)
			mIndex = lIndex + (rIndex - lIndex)/2;
		else
			mIndex = rIndex - (rIndex - lIndex)/2;

		int mPos = (int )ElementAt(mIndex).posizione;
		// Che fortuna !!
		if (pos == mPos)
			{// uguale pos intermedia
			idx = mIndex;
			break;
			}
		// cambio estremi
		if (pos > mPos)
			{// Bisezione funzioni continue
			if (lIndex == mIndex)
				{// not found
				idx = mIndex;
				break;
				}
			lIndex = mIndex;
			}
		if (pos < mPos)
			{// Not found
			if (rIndex == mIndex)
				{
				idx = mIndex;
				break;
				}
			rIndex = mIndex;
			}
		}
	return (idx);};



// torna numero totale di posizioni difettose fra
// from e to e fra bandaSx e bandaDx
int getNumIndex(int from,int to,int bandaSx,int bandaDx)
	{
	int  val=0;
	int idxFrom=-1;
	while(idxFrom<0)
		{
		idxFrom = getIndexFromPos(from);
		from ++;
		if(from >= to)
			break;
		}
	int idxTo = -1;
	while(idxTo<0)
		{
		idxTo = getIndexFromPos(to);
		to --;
		if(to < from)
			break;
		}
	if ((from<0)||(to<0))
		return 0;
	
	for (int i=idxFrom;i<=idxTo;i++)
		{
		long nDif=0;
		for (int k=0;k<getNumClassi();k++)
			{
			nDif += ElementAt(i).getValNum(bandaSx,bandaDx,'A'+k);
			}
		if (nDif > 0)
			val ++;
		}
	return(val);};

// torna numero totale di posizioni * classi difettose fra
// from e to e fra bandaSx e bandaDx
// Pesantemente modificato 30-04-2010
int getNumClassIndex(int from,int to,int bandaSx,int bandaDx)
	{
	int  val=0;
	int valBig = 0;
	int valPer = 0;
	int idxFrom=-1;
	while(idxFrom<0)
		{
		idxFrom = getIndexFromPos(from);
		if(idxFrom<0)
			{
			if(from >= to)
				break;
			else
				from ++;
			}
		}
	int idxTo = -1;
	while(idxTo<0)
		{
		idxTo = getIndexFromPos(to);
		if(idxTo < 0)
			{
			if(to < from)
				break;
			else
				to --;
			}
		} 
	// ora accettate pos < 0
	if (to<0)
		return 0;
	if ((idxFrom<0)||(idxTo<0))
		return 0;
	
	// per ogni difetto!
// 30-04-2010 Grosso BACO! 
// da aggiornare codice che verifica ogni banda!
// 11-05-10 da < a <= idxTo
// 05-02-14 da <= a < e > bandaSx
	for (int i=idxFrom;i<=idxTo;i++)
		{
		long nDif[4]={0,0,0,0};
		long nDifBig = 0;
		long nDifPer = 0;
		// nel metro ogni banda
		for (int j=0;j<ElementAt(i).GetSize();j++)
			{
			int bnd = ElementAt(i).ElementAt(j).banda;
			if ((bnd < bandaSx)||
				(bnd > bandaDx))
				continue;
			for (int k=0;k<getNumClassi();k++)
				{
				nDif[k] += ElementAt(i).getValNum(bnd,'A'+k);
				}
	
			//11-09-2007 compresi anche i bigHole nel n. tot difetti		
			nDifBig += ElementAt(i).getValBigHole(bnd);
			// Ogni PerB
			nDifPer += ElementAt(i).getValPerB(bnd);
			}

		for (int k=0;k<getNumClassi();k++)
			if (nDif[k] > 0)
			val ++;

		if (nDifBig > 0)
			valBig ++;
		if (nDifPer > 0)
			valPer ++;
// fine fixBug!
		}
	// Periodici stessa cella dei B!
	//return(val+valBig+valPer);};
	return(val+valBig);};
	
// Somma tutti i difetti di code fra metro from e metro To
// appartenenti a scheda
double getAllDifetti(int scheda,int code)
{
double val=0.;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);
if (scheda < c_totali.GetSize())
	{
	switch(code)
		{
		case 'A' : val = (double)c_totali[scheda].totalA;
				break;
		case 'B' : val = (double)c_totali[scheda].totalB;
				break;
		case 'C' : val = (double)c_totali[scheda].totalC;
				break;
		case 'D' : val = (double)c_totali[scheda].totalD;
				break;
		}
	}

return (val);
}

// Somma tutti i bigHole 
// appartenenti a scheda
double getAllBigHole(int scheda)
{
double val=0.;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);
if (scheda < c_totali.GetSize())
	{
	val = (double)c_totali[scheda].totalBig;
	}
return (val);
}

// Somma tutti i difetti bigHole fra metro from e metro To
// per schede comprese fra sc0 ed sc1
double getTotBigHole(int from,int to,int schedaFrom=-1,int schedaTo=-1)
{
double val=0.;
BOOL modFrom = FALSE;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);

	int idxFrom=-1;
	while(idxFrom<0)
		{
		idxFrom = getIndexFromPos(from);
		if (idxFrom < 0)
			{
			modFrom = TRUE;
			from ++;
			if(from >= to)
				break;
			}
		}
	int idxTo = -1;
	while(idxTo<0)
		{
		idxTo = getIndexFromPos(to);		
		if (idxTo <0)
			{
			to --;
			if(to < from)
				break;
			}
		}
	if ((from<0)||(to<0))
		return 0.;
	if ((idxFrom<0)||(idxTo<0))
		{// modificato! 12-05-10 se uno solo idx< 0 torna 0 
		// ma puo` esserci big nell'altro estremo
		if (idxFrom >= 0)
			idxTo = idxFrom;
		if (idxTo >= 0)
			idxFrom = idxTo;
		if ((idxFrom<0)&&(idxTo<0))
			return 0.;
		}

for (int i=idxTo;i>=idxFrom;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if (((pos > from)||(modFrom&&(pos==from)))&&
		(pos <= to)) 
		{
		int schF,schT;
		if (schedaFrom == -1)
			schF = 0;
		else
			schF = schedaFrom;
		if (schedaTo == -1)
			schT = numeroSchede;
		else
			schT = schedaTo;
		// passare a <= per includere anche scheda di fine 
		for (int j=schF;j<=schT;j++)
			val += ElementAt(i).getValBigHole(j);
		}


	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// Somma tutti i difetti perB fra metro from e metro To
// per schede comprese fra sc0 ed sc1
double getTotPerB(int from,int to,int schedaFrom=-1,int schedaTo=-1)
{
double val=0.;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);

for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{
		int schF,schT;
		if (schedaFrom == -1)
			schF = 0;
		else
			schF = schedaFrom;
		if (schedaTo == -1)
			schT = numeroSchede;
		else
			schT = schedaTo;

// sostituito totali bande con R a totale metri con R
		val += ElementAt(i).getValPerB(schF,schT);
		}

	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// Somma tutti i difetti di code fra metro from e metro To
// appartenenti a scheda
double getTotDifetti(int scheda,int code,int from,int to)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
if (scheda == 41)
	val = 0;
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{
		val += ElementAt(i).getValNum(scheda,code);
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// Somma tutti i difetti di code fra metro from e metro To
// di qualunque scheda
double getTotDifetti(int code,int from,int to)
{
double val=0.;
BOOL modFrom=FALSE;
// Nessun elemento nell'array
int a = GetSize();
if (GetSize() == 0)
	return (val);

	int idxFrom=-1;
	while(idxFrom<0)
		{
		idxFrom = getIndexFromPos(from);
		if (idxFrom < 0)
			{
			modFrom =TRUE;
			from ++;
			// era >= ma tornava zero se c'era una sola casella
			// coincidente su from e to
			if(from > to)
				break;
			}
		}
	int idxTo = -1;
	while(idxTo<0)
		{
		idxTo = getIndexFromPos(to);
		if (idxTo < 0)
			{
			to --;
			if(to < from)
				break;
			}
		}
	// ci sono saldature allora from puo` essere < 0
//	if ((from<0)||(to<0))
//		return 0;
	if ((idxFrom<0)||(idxTo<0))
		{// modificato! 12-05-10 se uno solo idx< 0 torna 0 
		// ma puo` esserci big nell'altro estremo
		if (idxFrom >= 0)
			idxTo = idxFrom;
		if (idxTo >= 0)
			idxFrom = idxTo;
		if ((idxFrom<0)&&(idxTo<0))
			return 0.;
		}

for (int i=idxTo;i>=idxFrom;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if (((pos > from)||(modFrom&&(pos==from)))&&
		(pos <= to)) 
		{
		val += ElementAt(i).getValNum(code);
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// verifica presenza difetti di laminazione per sezioni e strip
// intervallo posizione longitudinale

BOOL existLamindef(int from,int to, int schedaFrom,int schedaTo)
{

// Nessun elemento nell'array
if (GetSize() == 0)
	return FALSE;
for (int i=GetUpperBound();i>=0;i--)
	{// parto dal fondo
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{// 18-01-10 era < schedaFrom ora <= per 
		// uniformita` con riepilogo dettaglio fori 
		if (ElementAt(i).existLaminDef(schedaFrom,schedaTo))
			return TRUE;
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}



return FALSE;
}



// Somma tutti i difetti di code fra metro from e metro To
// per schede comprese fra sc0 ed sc1
double getTotDifetti(int code,int from,int to,int schedaFrom,int schedaTo)
{
double val=0.;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{// 18-01-10 era < schedaFrom ora <= per 
		// uniformita` con riepilogo dettaglio fori 
		for (int j=schedaFrom;j<=schedaTo;j++)
			val += ElementAt(i).getValNum(j,code);
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// Somma tutti i difetti presenti nel metro index
double getTotDifetti(int index)
{
double val=0.;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);

for (int k=0;k<ElementAt(index).GetSize();k++)
	val += ElementAt(index).getValNum('A'+k);
return (val);
}


// Somma tutti i difetti fra metro from e metro To
// di qualunque scheda e tutti i code
double getTotDifetti(int from,int to)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos >  from)&&
		(pos <= to)) 
		{
//		for (int k=0;k<ElementAt(i).GetSize();k++)
			for (int j=0;j<getNumClassi();j++)
				val += ElementAt(i).getValNum('A'+j);
		}
	// sfrutto monotonicita`
	if (pos < from)
		break;
	}
return (val);
}

// valore massimo per qualunque scheda
double getMaxDensFromTo(int from ,int to)
	{
	double Mval=0.;
	// Nessun elemnto nell'array
	if (GetSize() == 0)
		return (Mval);
	for (int k=0;k<getNumSchede();k++)
		{
		double dv = 0.;
			int idxFrom=-1;
		while(idxFrom<0)
			{
			idxFrom = getIndexFromPos(from);
			if (idxFrom <0)
				{
				from ++;
				if(from >= to)
				break;
				}
			}
		int idxTo = -1;
		while(idxTo<0)
			{
			idxTo = getIndexFromPos(to);
			if (idxTo < 0)
				{
				to --;
				if(to < from)
					break;
				}
			}
		if ((from<0)||(to<0))
			continue;
		if ((idxFrom<0)||(idxTo<0))
			{// modificato! 12-05-10 se uno solo idx< 0 torna 0 
			// ma puo` esserci big nell'altro estremo
			if (idxFrom >= 0)
				idxTo = idxFrom;
			if (idxTo >= 0)
				idxFrom = idxTo;
			if ((idxFrom<0)&&(idxTo<0))
				return 0.;
			}

		for (int i=idxTo;i>=idxFrom;i--)
			{
			int pos = (int )ElementAt(i).posizione;
			if (((pos > from)&&
				(pos <= to)) ||
				(from == to))// caso 1 solo metro
				dv += ElementAt(i).getValNumInScheda(k);
			// sfrutto monotonicita`
			if (pos < from)
				break;
			}
		Mval = max(dv,Mval);
		}
	return (Mval);
	};

// Valore max trendl tra metro from e metro to
double getMaxTrendLFromTo(int code,int from,int to)
	{
	double Mval=0.;
	// Nessun elemnto nell'array
	if (GetSize() == 0)
		return (Mval);
	for (int i=GetUpperBound();i>=0;i--)
		{
		int pos = (int )ElementAt(i).posizione;
		if ((pos > from)&&
			(pos <= to)) 
			{
			Mval = max(ElementAt(i).getValNum(code),Mval);
			}
		// sfrutto monotonicita`
		if (pos < from)
			break;
		}
	return (Mval);
	};

// Valore min trendl tra metro from e metro to
double getMinTrendLFromTo(int code,int from,int to)
	{
	double mval=0.;
	// Nessun elemnto nell'array
	if (GetSize() == 0)
		return (mval);
	for (int i=GetUpperBound();i>=0;i--)
		{
		int pos = (int )ElementAt(i).posizione;
		if ((pos > from)&&
			(pos <= to)) 
			{
			mval = min(ElementAt(i).getValNum(code),mval);
			}
		// sfrutto monotonicita`
		if (pos < from)
			break;
		}
	return (mval);
	};

double getValRelativeTLStep(int code,int disp = 0)
	{double val;
	val = getValNumLStep(code,disp);
	val = val / getValMeanTL(code);
	return (val);
	};

// Indici su getMaxTrend
double getMaxRelativeTL(int code,int from,int to)
	{double val;
	val = getMaxTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};

double getMinRelativeTL(int code,int from,int to)
	{double val;
	val = getMinTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};


double getValMeanTL(int code)
{
double val=0.;
// Nessun elemnto nell'array
if (GetSize() == 0)
	return (val);
for (int i=GetUpperBound();i>=0;i--)
	{
	val += ElementAt(i).getValNum(code);
	}
// trovo tutti i difetti di tipo code
// li divido per la lunghezza del supporto
val /= getMeter(); 
return (val);
};

//--------------------------------------------------
// da utilizzare per lunResStop
// riversa in dCoil i difetti dei metri da from a to
// torna numero di metri difettosi
int dump (DifCoil &dCoil,int from,int to)
{
if (GetSize() <= 0)
	return 0;
int n=0;
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos > from)&&
		(pos <= to)) 
		{
		DifMetro dM;
		dM = ElementAt(i);
		dM.posizione = (int ) ( (to - from) + dM.posizione - meter);
		if (dM.posizione < 0)
			dM.posizione = 0;
		dCoil.InsertAt(0,dM);
		n ++;
		}
	// sfrutto monotonicita` per velocita` 
	if (pos < from)
		break;
	}
return (n);};

//--------------------------------------------------
// da utilizzare per dumpStrip
// riversa in dCoil i difetti dei metri da from a to
// torna numero di metri difettosi
int dumpStrip (DifCoil &dCoil,int from,int to)
{
if (GetSize() <= 0)
	return 0;
int n=0;
for (int i=GetUpperBound();i>=0;i--)
	{
	int pos = (int )ElementAt(i).posizione;
	if ((pos >= from)&&
		(pos <= to)) 
		{
		DifMetro dM;
		dM = ElementAt(i);
		// posizione < 0
		dM.posizione = (int ) (dM.posizione - to);
		//if (dM.posizione < 0)
		//	dM.posizione = 0;
		dCoil.InsertAt(0,dM);
		n ++;
		}
	// sfrutto monotonicita` per velocita` 
	if (pos < from)
		break;
	}
return (n);};

//----------------------------
// Save and load
BOOL save(CFile *cf)
	{
	// Free Unused memory
	FreeExtra();
	cf->Write((void *)&meter,sizeof(meter));
	//cf->Write((void *)&trueSizeEncod,sizeof(trueSizeEncod));
	cf->Write((void *)&largeSize,sizeof(largeSize));
	cf->Write((void *)&numeroClassi,sizeof(numeroClassi));
	cf->Write((void *)&numeroSchede,sizeof(numeroSchede));
	// only Csm20El2020 cf->Write((void *)&c_numSchedeLeft,sizeof(c_numSchedeLeft));
	cf->Write((void *)&baseStepX,sizeof(baseStepX));
	int nDifMetri;
	nDifMetri = GetSize();
	cf->Write((void *)&nDifMetri,sizeof(nDifMetri));
	for (int i=0;i<nDifMetri;i++)
		if (!ElementAt(i).save(cf)) return FALSE;
	int n;
	n = c_sogliaAllarme.GetSize();
	cf->Write((void *)&n,sizeof(n));
	for (int i=0;i<n;i++)
		{
		double d;
		d = c_sogliaAllarme[i];
		cf->Write((void *)&d,sizeof(d));
		}
	return TRUE;};

BOOL load(CFile *cf,int ver)
	{
	// spostato clear dopo il load di numschede,
	// clear mette a zeor anche lunghezza!
	double localPos;
	cf->Read((void *)&localPos,sizeof(localPos));
	// ------
	// cf->Read((void *)&trueSizeEncod,sizeof(trueSizeEncod));
	cf->Read((void *)&largeSize,sizeof(largeSize));
	cf->Read((void *)&numeroClassi,sizeof(numeroClassi));
	cf->Read((void *)&numeroSchede,sizeof(numeroSchede));
	// only Csm20El2020 cf->Read((void *)&c_numSchedeLeft,sizeof(c_numSchedeLeft));
	// Elval2013 FixBug
	// reset data structure with correct size from numerschede and numero classi 
	clear();
	meter = localPos;
	// --------
	cf->Read((void *)&baseStepX,sizeof(baseStepX));
	int nDifMetri;
	cf->Read((void *)&nDifMetri,sizeof(nDifMetri));
	SetSize(nDifMetri);
	for (int i=0;i<nDifMetri;i++)
		{
		if (!ElementAt(i).load(cf,ver)) return FALSE;
		updateTotal(ElementAt(i),0x1f);
		}
	int nSAlarm;
	cf->Read((void *)&nSAlarm,sizeof(nSAlarm));
	c_sogliaAllarme.SetSize(nSAlarm);
	for (int i=0;i<nSAlarm;i++)
		{
		double d;
		cf->Read((void *)&d,sizeof(d));
		c_sogliaAllarme[i] = d;
		}
	// recalc totali a,b,c.d e bigHole
	return TRUE;};

};


// eliminata classe scheda OBSOLETA!
#endif

