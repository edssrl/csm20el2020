//------------------------------------------------------------
//
//
//
//					Target.h
//
//
//
//
//-------------------------------------------------------------



#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "FontInfo.h"

typedef CArray < COLORREF, COLORREF > ColorArray ;

// oggetto che viene visualizzato e scrolla
class CTarget
{
CSize size;
int			banda;				// identificativo
COLORREF	color;				// colore
public:
CTarget (void );
void setBanda(int b){banda = b;};
int getBanda (void ){return(banda);};
void draw(CPoint p,CDC *pdc );	
COLORREF	setColor(COLORREF rgb);
CSize	setSize(CSize &s);
CSize	getSize(void ){return(size);};
COLORREF getColor(void) {return color;};

};

enum CTARGETSTATE {CTNEW,CTINSCOPE,CTOUTSCOPE};

class CTargetRow : public CArray <CTarget ,CTarget &>
{
int				pos;	// Posizione in metri sull'asse
CTARGETSTATE	state;	// stato di visualizzazione	
COLORREF drawAltBkColor;
BOOL	c_excludeYText;
public:
	CTargetRow(void){pos = 0;state=CTNEW; drawAltBkColor = RGB(128,128,128);c_excludeYText=FALSE;};
CTargetRow (CTargetRow& source);
~CTargetRow(void) {RemoveAll();};
CTargetRow &operator=(CTargetRow& source);

int getPos(void){return (pos);};
void setPos (int p){pos = p;};
void draw(CPoint p,CPoint pr,CDC *pdc,int pxFC,int pxC,
			int nbC,int pxR,BOOL mirror,ColorArray *pColorArray,BOOL useColorArray);
void drawAltBackground(CDC *pDC,CRect r);

// funzione di disegno con bande tutte diverse
void drawEx(CPoint p,CPoint pr,CDC	*pdc,int pxFC,int nbC,int pxR,
			CArray <int,int> &stripSize,BOOL mirror,BOOL useGrid,ColorArray *pColorArray,BOOL useColorArray);
void setScope (CTARGETSTATE st){state = st;};
void excludeYText(BOOL v){c_excludeYText = v;};
CTARGETSTATE getScope (void) {return (state);};
};



class CTargetBoard : public CArray <CTargetRow ,CTargetRow &>
{
int lastPos;

ColorArray	c_excludeDotColor;

// Dati di disegno
FontInfo font;
COLORREF textBkColor;
COLORREF drawBkColor;
int pxFirstCol;
int pxPerCol;		// numero di pixel per colonna
int nBandePerCol;	// numero di bande per colonna
int pxPerRow;
// descrittore delle dimensioni delle singole bande
// se la dimensione e` < 0 indica colore sfondo alternato 
// dimensione abs()
CArray <int,int> c_stripSize;
// mirror ora intero = 0=NoMirror, 1=Mirrodef, 2=mirrorStrip
int c_mirror;
BOOL c_useGrid;
int c_mask;	
BOOL c_useColorArray;

public:
	void reDraw(int index,CRect r,CDC *pdc);

CTargetBoard(void) {setFont(8);textBkColor = RGB(192,192,192);
				drawBkColor = RGB(0,0,0);
				pxFirstCol = 0;
				lastPos=0;// cresce di 1000 metri per volta
				m_nGrowBy = 1000;c_mirror = 0;c_useGrid=0;
				c_useColorArray=FALSE;};	
~CTargetBoard(void) {setEmpty();c_stripSize.RemoveAll();resetExcludeDotColor();};
void setEmpty (void){for (int i=0;i<GetSize();i++)
						ElementAt(i).RemoveAll();
					RemoveAll();
					};
void draw(CRect r,CDC	*pdc)	;
void scroll(int nr,CRect rInt,CRect rClip,CDC	*pdc)	;
void plot(CRect r,CDC	*pdc,double posTop,double posBottom);	

// Pos in mm.
void add (int pos,CTarget &t,BOOL excludeLeftText=FALSE);
void setTextBkColor(COLORREF cl){textBkColor = cl;};
void setDrawBkColor(COLORREF cl){drawBkColor = cl;};

// gestione pallini nascosti su double click classi
void resetExcludeDotColor(void){c_excludeDotColor.RemoveAll();};
void addExcludeDotColor (COLORREF color) {c_excludeDotColor.Add(color);};
void removeExcludeDotColor (COLORREF color) {for (int i=0;i<c_excludeDotColor.GetSize();i++)
				{if (c_excludeDotColor[i] == color)c_excludeDotColor.RemoveAt(i);}};
void setUseColorArray(BOOL use){c_useColorArray = use;};
BOOL isExcludedDotColor(COLORREF color)
	{for (int i=0;i<c_excludeDotColor.GetSize();i++)
				if (c_excludeDotColor[i] == color)return TRUE;
				return FALSE;};
//-----------------
void setDrawAttrib(int pxFCol,int pxCol,int pxRow,int nbpCol,BOOL mirror=FALSE,BOOL useGrid=FALSE)
	{c_mirror = mirror;
	pxFirstCol = pxFCol;pxPerCol=pxCol;pxPerRow=pxRow;nBandePerCol=nbpCol;};

// mirror ora intero = 0=NoMirror, 1=Mirrodef, 2=mirrorStrip
void setDrawAttrib(int pxFCol,int pxRow,int nbpCol,CArray <int,int> &stripSize,int mirror,BOOL useGrid)
	{c_mirror = mirror;c_useGrid = useGrid;
	pxFirstCol = pxFCol;pxPerRow=pxRow;nBandePerCol=nbpCol;
	c_stripSize.RemoveAll();
	for (int i=0;i<stripSize.GetSize();i++)
		c_stripSize.Add(stripSize[i]);};
void setFont (int sz,LPCTSTR name = "TimesNewRoman")
	{font.name = name;font.size = sz*10;};
int getLastPos(void ){return(lastPos);};
void setLastPos(int p){lastPos = p;};

BOOL isColorExcluded(COLORREF c)
	{
	for (int j=0; j< c_excludeDotColor.GetSize();j++)
		{
		if (c == c_excludeDotColor.ElementAt(j))
			return TRUE;
		}
	return FALSE;
	};

};
