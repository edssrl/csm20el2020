// PdfInfo.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"

#include "PdfPrintView.h"
#include "choosedirdlg.h"
#include "Profile.h"

// modificato versione  per MICROSOFT_PDF_PRINTER
//
#include "PdfInfoMsf.h"

// import file idl 


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPdfInfo dialog


CPdfInfo::CPdfInfo(CWnd* pParent /*=NULL*/)
	: CDialog(CPdfInfo::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPdfInfo)
	m_saveFolder = _T("");
	m_pdfPrinterName = _T("");
	m_timeout = 0;
	//}}AFX_DATA_INIT
	m_pdfVer = _T("");
	

}

CPdfInfo::~CPdfInfo()
{

}

void CPdfInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPdfInfo)
	DDX_Text(pDX, IDC_SAVE_FOLDER, m_saveFolder);
	DDX_Text(pDX, IDC_PDF_PRINTER_NAME, m_pdfPrinterName);
	DDX_Text(pDX, IDC_TIMEOUT, m_timeout);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_PDF_VER, m_pdfVer);
}


BEGIN_MESSAGE_MAP(CPdfInfo, CDialog)
	//{{AFX_MSG_MAP(CPdfInfo)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPdfInfo message handlers

BOOL CPdfInfo::OnInitDialog() 
{

CDialog::OnInitDialog();

CMenu* pSysMenu = GetSystemMenu(FALSE);

if (pSysMenu != NULL)
	{
    pSysMenu->DeleteMenu(1,MF_BYPOSITION);
    }

m_pdfVer = MICROSOFT_PDF_PRINTER;

loadFromProfile();	

UpdateData(FALSE);

return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL	CPdfInfo::saveToProfile(void)
{
CProfile profile;

//
profile.writeProfileString ("PdfSetting","Folder",m_saveFolder);
profile.writeProfileString ("PdfSetting","PdfPrinterName",m_pdfPrinterName);
CString sTimeout;
sTimeout.Format("%d",m_timeout);
profile.writeProfileString ("PdfSetting","Timeout",sTimeout);

return TRUE;
}


BOOL	CPdfInfo::loadFromProfile(void)
{
CProfile profile;

m_saveFolder = profile.getProfileString ("PdfSetting","Folder","");
m_pdfPrinterName = profile.getProfileString ("PdfSetting","PdfPrinterName","PDFCreator");
m_timeout = profile.getProfileInt ("PdfSetting","Timeout",2);

return TRUE;
}


void CPdfInfo::OnBrowse() 
{
// TODO: Add your control notification handler code here
CString s;
CChooseDirDlg chooseDirDialog;

if (chooseDirDialog.GetDirectory(s,this,FALSE,"Select Folder")== TRUE)
	{ 
	m_saveFolder=s;
	UpdateData(FALSE);
	}
}

void CPdfInfo::OnOK() 
{

UpdateData(TRUE);
saveToProfile();	

CDialog::OnOK();
}

void CPdfInfo::OnCancel() 
{
// TODO: Add extra cleanup here


CDialog::OnCancel();
}

// check if file pdf on folder
BOOL CPdfInfo::isPdfFileReady(LPCSTR fileName,LPCSTR folder)
{

CString saveFolder, filePath;;
saveFolder = m_saveFolder;

if ((saveFolder[saveFolder.GetLength()-1] != '\\')&&
	(saveFolder[saveFolder.GetLength()-1] != '/'))
	saveFolder += "\\";
saveFolder += folder;
// end create folder
if ((saveFolder[saveFolder.GetLength()-1] != '\\')&&
	(saveFolder[saveFolder.GetLength()-1] != '/'))
	saveFolder += "\\";

filePath = saveFolder + fileName;

filePath += ".pdf";
CFileFind finder;

BOOL bResult = finder.FindFile((LPCSTR)filePath);

if (bResult)
	return TRUE;
else
	return FALSE;
	

}

void CPdfInfo::OnDestroy() 
{
CDialog::OnDestroy();
// TODO: Add your message handler code here	
}

CString CPdfInfo::getPdfFilePath(CString fileName, CString ddFolder)
	{
	CString saveFolder;
		// folder base
		saveFolder = m_saveFolder;

		if ((saveFolder[saveFolder.GetLength()-1] != '\\')&&
			(saveFolder[saveFolder.GetLength()-1] != '/'))
			saveFolder += "\\";
	
		saveFolder += ddFolder;

		// create folder
		SECURITY_ATTRIBUTES	 sa;
		sa.nLength= sizeof(SECURITY_ATTRIBUTES);
		sa.lpSecurityDescriptor=NULL;
		sa.bInheritHandle = TRUE;
		::CreateDirectory((LPCTSTR)saveFolder,&sa);
		// end create folder

		if ((saveFolder[saveFolder.GetLength()-1] != '\\')&&
			(saveFolder[saveFolder.GetLength()-1] != '/'))
			saveFolder += "\\";
		saveFolder += fileName;
		saveFolder += _T(".pdf");
	return saveFolder;
	}
