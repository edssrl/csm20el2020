/*-----------------------------------------------------------------

			Endian.cpp		routines swap format
	
-----------------------------------------------------------------*/
#include "stdafx.h"
#include "Endian.h"

/*-----------------------------------------------------------------

			SWAPLONG () 		routine swap long format
	
-----------------------------------------------------------------*/

union bytelong
	{
	unsigned char byte [4];
	unsigned long  word;
	};

void swaplong (long *source,long *dest,int number)
{
union bytelong local;
int i;

long *lfrom;
unsigned char *bto;

lfrom = source;
bto = (unsigned char *) dest;

for (i=0;i<number;i++)
	{
	local.word = *lfrom;     /* carico valore in union */
	
	bto [0] = local.byte[3];	/* swap */
	bto [1] = local.byte[2];
	bto [2] = local.byte[1];
	bto [3] = local.byte[0];
	bto += sizeof (long);	/* next word */
	lfrom ++;
	}
}	


/*-----------------------------------------------------------------

			SWAPDOUBLE () 		routine swap double format
	
-----------------------------------------------------------------*/

union bytedouble
	{
	unsigned char byte [8];
	double word;
	};

void swapdouble (double *source,double *dest,int number)
{
union bytedouble local;
int i,j;

double *lfrom;
unsigned char *bto;

lfrom = source;
bto = (unsigned char *) dest;

char *p;
for (i=0;i<number;i++)
	{
	// local.word = *lfrom;     /* carico valore in union */
	p = (char *)lfrom;			/* Problema ottimizzatore */
		for (j=0;j<8;j++)
			local.byte[j] = p[j];
	
	bto [0] = local.byte[7];	/* swap */
	bto [1] = local.byte[6];
	bto [2] = local.byte[5];
	bto [3] = local.byte[4];
	bto [4] = local.byte[3];	/* swap */
	bto [5] = local.byte[2];
	bto [6] = local.byte[1];
	bto [7] = local.byte[0];
	bto += sizeof (double);	/* next word */
	lfrom ++;
	}
}	

