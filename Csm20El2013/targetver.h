#pragma once

// Including SDKDDKVer.h defines the highest available Windows platform.

// If you wish to build your application for a previous Windows platform, include WinSDKVer.h and
// set the _WIN32_WINNT macro to the platform you wish to support before including SDKDDKVer.h.
#include <WinSDKVer.h>

#define _WIN32_WINN  _WIN32_WINNT_WIN8    // windows 7


#include <SDKDDKVer.h>

// disable warnings per strcpy - strcpy_s etc 
#define _CRT_SECURE_NO_WARNINGS

// disable warnings per old CDaoRecordset class
#pragma warning(disable : 4995)
// disable argument' : conversion from 'double' to 'int', possible loss of data
#pragma warning(disable : 4244)
