#if !defined(AFX_MULTILINE_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
#define AFX_MULTILINEVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MLineView.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CMLineView view

class CMLineView : public CView
{
protected:
	CMLineView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CMLineView)
	ViewType vType; 

// Attributes
public:
SubLine trendLine[4];
Layout  layout;
LineInfo lineInfo[4];

double maxValY;
double lastActualMeter;
int c_numStepX;
int c_stepScaler;

// Operations
public:
	ViewType getViewType(void) {return(vType);};
	CRect getDrawRect(void);
	CRect getMeterRect(void);


	void draw(void){CClientDC dc(this);OnDraw(&dc);};
	void updateLabel();
	void updateTrend(CDC *pDC,CRect rectB);
	void calcNumStepX (DOC_CLASS* pDoc);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMLineView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CMLineView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CMLineView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MULTILINEVIEW_H__92114CD1_CF80_11D1_A6AE_00C026A019B7__INCLUDED_)
