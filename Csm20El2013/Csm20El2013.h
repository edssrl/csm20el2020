// CSM.h : main header file for the CSM application
//

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols




/////////////////////////////////////////////////////////////////////////////
// CCSMApp:
// See CSM.cpp for the implementation of this class
//

#include "CDaoDbCreate.h"

#include "EnumPrinters.h"



#ifdef MAIN_CSM20
CDaoDbCreate *dBase = NULL;
#else
extern CDaoDbCreate *dBase;
#endif





class CCSMApp : public CWinApp
{
HMODULE		hModRes;	// Resource handle modificato 
HMODULE		hDefRes;	// Resource handle originario	

CEnumPrinters	m_PrinterControl ;

CString		c_logFileName;

public:
	CCSMApp();
	~CCSMApp(){if (dBase != NULL) 
			{
			if (dBase->IsOpen())
				dBase->Close();
			delete dBase;
			dBase = NULL;}
			};

// DBase Declaration
CDynViewDocTemplate* pDocTemplate;
CLineDoc *GetDocumentTop (void ); 
CLineDoc *GetDocumentBottom (void );
int GetNumDocument (void ); 

// Print setting
void SetPrintLandscape(void);
void SetPrintPortrait(void);
// return the name of the currently selected printer
CString GetDefaultPrinter(void);
void  SetIniPath();

int easyLogConfig(CStringA confFileName,CStringA logFileName);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSMApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCSMApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFilePrintSetup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
