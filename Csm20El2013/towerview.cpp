// TowerView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"

#include "TowerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTowerView


IMPLEMENT_DYNCREATE(CTowerView, CView)

CTowerView::CTowerView()
{
vType = TOWERVIEW;

// Init layout Fix Attributes

CProfile profile;
CString s;

layout.fCaption.size = profile.getProfileInt("TowerLayout","CaptionSize",120);
layout.fLabel.size =  profile.getProfileInt("TowerLayout","LabelSize",100);
layout.fNormal.size =  profile.getProfileInt("TowerLayout","NormalSize",110);

layout.setMirrorLabel(FALSE);

s = profile.getProfileString("TowerLayout","NewLine","3,6");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString("TowerLayout","LabelOrder","0,1,2,3,4,5,6,7,8,9");
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

s = profile.getProfileString("TowerLayout","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layout.c_pxSepLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

layout.c_pxSepLabelEdit = profile.getProfileInt("TowerLayout","PixelSepLabelEdit",10);

// layout.setLabel ("label 1");
layout.setViewType(TOWERVIEW);
layout.setMode (LINEAR | USE_STRIPRULER);


// Passato in risorse
//s = profile.getProfileString("TowerLayout","SCaption",
//							 "CROSS WEB DISTRIBUTION");
s.LoadString(CSM_GRAPHVIEW_TOWERCAPTION);

layout.setCaption(s);

//for (int i=0;i<MAX_NUMLABEL;i++)
//	{
//	CString sIndex;
//	sIndex.Format ("SLabel%d",i);
//	s = profile.getProfileString("TowerLayout",sIndex,sIndex);
//	if (s != sIndex)
//		layout.c_sLabel[i] = s;
//	}

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL0);
layout.c_sLabel[0] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL1);
layout.c_sLabel[1] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL2);
layout.c_sLabel[2] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL3);
layout.c_sLabel[3] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL4);
layout.c_sLabel[4] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL5);
layout.c_sLabel[5] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL6);
layout.c_sLabel[6] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL7);
layout.c_sLabel[7] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL8);
layout.c_sLabel[8] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL9);
layout.c_sLabel[9] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL10);
layout.c_sLabel[10] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL11);
layout.c_sLabel[11] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL12);
layout.c_sLabel[12] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL13);
layout.c_sLabel[13] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL14);
layout.c_sLabel[14] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL15);
layout.c_sLabel[15] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL16);
layout.c_sLabel[16] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL17);
layout.c_sLabel[17] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL18);
layout.c_sLabel[18] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL19);
layout.c_sLabel[19] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL20);
layout.c_sLabel[20] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL21);
layout.c_sLabel[21] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL22);
layout.c_sLabel[22] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL23);
layout.c_sLabel[23] = s;

for (int i=0;(i<=4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	if (i == 4)
		s = "Big";
	layout.c_cLabel[i] = s;
	layout.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layout.c_cLabel[i].setTipoIcon(IPALLINO);
	}

// CalcDrawRect
layout.c_viewTopPerc = profile.getProfileInt("TowerLayout","ViewTop%",10);
layout.c_viewBottomPerc = profile.getProfileInt("TowerLayout","ViewBottom%",25);
layout.c_viewLeftPerc = profile.getProfileInt("TowerLayout","ViewLeft%",8);
layout.c_viewRightPerc = profile.getProfileInt("TowerLayout","ViewRight%",5);

	
}

CTowerView::~CTowerView()
{
}


BEGIN_MESSAGE_MAP(CTowerView, CView)
	//{{AFX_MSG_MAP(CTowerView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTowerView drawing


// area disegno interna
CRect CTowerView::getDrawRect(void)
{
CRect rcBounds,rect;
GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rect);
return (rect);
}



void CTowerView::OnDraw(CDC* pDC)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

// TODO: add draw code here
// Call Base Class Draw

CRect rectB;
CRect rcBounds;

GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);
// Call Base Class Draw
//-----------------------------------------

updateLabel();

//--------------------

layout.setNumSchede(pDoc->c_difCoil.getNumSchede());
int numClassi = pDoc->c_difCoil.getNumClassi();
layout.setNumClassi(numClassi);
layout.setNumLabelClassi(numClassi);
layout.setNumCpu(pDoc->c_numCpu);

layout.draw(pDC);

// Trovo Finestra disegno (grigia)
layout.CalcDrawRect (rcBounds,rectB);

updateExtra(pDC,rectB);

// Parametrizzata scala orizzontale su layout
double Xsize = 1. * layout.c_sizeBanda / layout.c_bandePerCol;

CRect clientRect;
clientRect = rectB;
clientRect.right =   clientRect.left;

CFont font,*pOldFont;
font.CreatePointFont (70,"Times New Roman",pDC);
pOldFont = pDC->SelectObject(&font); 


for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	double *dVal = new double [pDoc->c_difCoil.getNumClassi()+1];	// +1 Big
	int j;
	for (j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		// Sostituita densita` con totali per classi
		// dVal[j] = pDoc->c_difCoil.getValDensita(i,'A'+j,pDoc->GetTrendDScalaX());
		// sostituita funzione veloce che usa totali	20-01-99 
		//dVal[j] = pDoc->c_difCoil.getTotDifetti(i,'A'+j,0,
		//	(int) pDoc->c_difCoil.getMeter());
		int a;
		dVal[j] = pDoc->c_difCoil.getAllDifetti(i,'A'+j);
		if (dVal[j] > 0)
			a=0;
		}
	// big 
	dVal[j] = pDoc->c_difCoil.getAllBigHole(i);
	
	// dVal[j] = pDoc->c_difCoil.getTotBigHole(0,(int)pDoc->c_difCoil.getMeter(),i,i+1);

	// inserito offset inizio grafico

	if (layout.getMirrorLabel())
		{
		clientRect.left  = rectB.right - layout.c_sizeFBanda - (int ) (Xsize * i);
		clientRect.right = rectB.right - layout.c_sizeFBanda - (int ) (Xsize * (i+1));
		}
	else
		{
		clientRect.left  = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * i);
		clientRect.right = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * (i+1));
		}
			
	if (layout.getMirrorLabel())
		{
		towerView[(pDoc->c_difCoil.getNumSchede()-1)-i].setScale(clientRect,pDoc->GetTrendDScalaY(),dVal);
		towerView[(pDoc->c_difCoil.getNumSchede()-1)-i].setPos(pDoc->c_difCoil.getNumSchede()-i);
		towerView[(pDoc->c_difCoil.getNumSchede()-1)-i].draw(pDC);
		}
	else
		{
		towerView[i].setScale(clientRect,pDoc->GetTrendDScalaY(),dVal);
		towerView[i].setPos(i+1);
		towerView[i].draw(pDC);
		}

		
	delete [] dVal;
	}

// Restore old font 
pDC->SelectObject(pOldFont); 
}



/////////////////////////////////////////////////////////////////////////////
// CTowerView diagnostics

#ifdef _DEBUG
void CTowerView::AssertValid() const
{
	CView::AssertValid();
}

void CTowerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTowerView message handlers

BOOL CTowerView::OnEraseBkgnd(CDC* pDC) 
{

// TODO: Add your message handler code here and/or call default
CRect r;
GetClientRect(&r);
layout.OnEraseBkgnd(pDC,r);

return TRUE;	
}

BOOL CTowerView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{

// TODO: Add your specialized code here and/or call the base class
if (!CView::Create(lpszClassName, lpszWindowName, 
	dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

towerView.SetSize (pDoc->c_difCoil.getNumSchede()); 

layout.Create(this);

// init colors pen and brush
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{// 16-02-2010 No Big
	towerView[i].init (pDoc->c_difCoil.getNumClassi());  // +1 BIG
	towerView[i].setMode (TOWER);
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)   // +1 Big
		{
		towerView[i].setPenTB (j,CSM20GetClassColor(j));	  // Top and Bottom color
		towerView[i].setPenLR (j,layout.getRgbBackColor());	  // Red
		towerView[i].setBrush (j,CSM20GetClassColor(j));
		}
	}
return TRUE;
}




BOOL CTowerView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString s;
	s.Format("Got Notify Message from %d",(int)wParam);
	AfxMessageBox (s);
	return CView::OnNotify(wParam, lParam, pResult);
}

void CTowerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if ((lHint==0) && (pHint == NULL))
	{
	// int newRow = fillCtrl(nr);
	CView::OnUpdate(pSender,lHint,pHint);	
	}
else
	{
	CRect rcBounds,rectB;
	GetClientRect (rcBounds);
	layout.CalcDrawRect(rcBounds,rectB);
	CRect clipRect(rectB);
	CDC  *pDC;
	pDC = GetDC ();
	pDC->IntersectClipRect (clipRect);
	updateTower(pDC,rectB);
	updateLabel();
	updateExtra(pDC,rectB);
	ReleaseDC(pDC);
	}
}



void CTowerView::drawPosDiaframma(CDC *pDC, CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
if (!pDoc->c_useDiaframmi)
	return;


CPen pen1,pen2,pen3,pen4,*oldPen;
	// red
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
	{
	pen1.CreatePen(PS_SOLID,1,RGB(196,0,0));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left +layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	pen2.CreatePen(PS_SOLID,1,RGB(196,0,0));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight = rectB.left + layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	if (pDoc->c_mode == 1)
		{// disegno diaframmi testa 2 merge SingleCoil
		pen3.CreatePen(PS_SOLID,1,RGB(196,0,0));
		pDC->SelectObject(&pen3);
		double posLeft = pDoc->c_posDiaframmaSx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		int scPosLeft = rectB.left +layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		pen4.CreatePen(PS_SOLID,1,RGB(196,0,0));
		pDC->SelectObject(&pen4);
		double posRight = pDoc->c_posDiaframmaDx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		int scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosRight,rectB.top);
		pDC->LineTo(scPosRight,rectB.bottom);
		
		pDC->SelectObject(oldPen);
		pen3.DeleteObject();
		pen4.DeleteObject();
		}
	// non importa anche se seleziono la stessa pen due volte
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------
	}

}



void CTowerView::drawPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();


CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
if (pDoc->c_rightAlignStrip)	
	{
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	// era 180 alposto di numSchede
	// double globalWidth = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
	// double globalWidth = 180 * SIZE_BANDE;
	// fixBug per sistema doppia configurazione
	// double globalWidth = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;

	double posRight;
	posRight = pDoc->getPosColtello(lastElem);

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda
	int scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=pDoc->c_rotoloMapping[lastElem].size()-1;i>=0;i--)
		{
		// disegno solo inizio strip 
		// fine strip solo per ultimo strip
		double endPosLastStrip = 0.;

		// prima riga a dx strip
		pDC->SelectObject(&pen0);
	 	//////

		posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// disegno strip lato dx della banda
		posRight -= SIZE_BANDE;

		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));



		//////
		// posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		//---------------------------------
		// seconda riga a sx strip
		pDC->SelectObject(&pen1);	
		
		posRight -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		// disegno strip lato dx della banda
		posRight += SIZE_BANDE;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();

	}
else
	//---------------------------------------------------------
	// Disegno Posizione bobine allineate a sx	
	{
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga a dx strip
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		//---------------------------------
		// seconda riga a sx strip
		pDC->SelectObject(&pen1);	
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
}

// visualizzazione posizione bobine per utente
void CTowerView::drawPosBobine1(CDC *pDC,CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
if (pDoc->c_rightAlignStrip)	
	{
//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,2,RGB(0,0,0));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,2,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posRight;
	// double globalWidth = 180 * SIZE_BANDE;
	// fixBug per sistema doppia configurazione
	//double globalWidth = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
	posRight = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	// int scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	int scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

//
// disegno solo inizio strip 
// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	for (int i=pDoc->c_rotoloMapping[lastElem].size()-1;i>=0;i--)
		{
		// prima riga sinistra
		pDC->SelectObject(&pen0);
	 	
		posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// disegno strip lato dx della banda
		//posRight += SIZE_BANDE;

		// check distanza con fine strip precedente
		if (i < pDoc->c_rotoloMapping[lastElem].size()-1)
			{
			posRight -= (fabs(posRight - endPosLastStrip)/2);
			}

		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
					- (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		if (i == 0)
			{
			// seconda riga destra
			pDC->SelectObject(&pen1);	

			posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			posRight -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
			// disegno strip lato dx della banda
			// posRight += SIZE_BANDE;

			// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
			// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
else
	{	
//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,2,RGB(0,0,0));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,2,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

//
// disegno solo inizio strip 
// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga sinistra
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		
		// check distanza con fine strip precedente
		if (i > 0)
			{
			posLeft -= (fabs(posLeft - endPosLastStrip)/2);
			}

		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
					+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		if (i == (pDoc->c_rotoloMapping[lastElem].size()-1))
			{
			// seconda riga destra
			pDC->SelectObject(&pen1);	

			posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
			// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
			scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
}


void CTowerView::textPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
if (pDoc->c_rightAlignStrip) 
	{
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);

	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double rightSide = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
	double posRight;
	posRight =  pDoc->getPosColtello(lastElem);
	// double globalWidth = 180 * SIZE_BANDE;
	// fixBug per sistema doppia configurazione
	//double globalWidth = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	//int scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	int scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
	 	
		posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// disegno strip lato dx della banda
		posRight += SIZE_BANDE;


		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		//scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));


		// riferimenti numerici
		CString sl;
		pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		sl.Format(_T("%3.0lf"),posRight/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);

		//---------------------------------
		// seconda riga a sx strip
		
		posRight -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		// disegno strip lato dx della banda
		posRight += SIZE_BANDE;

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		sl.Format(_T("%3.0lf"),posRight/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
		}
	pDC->SelectObject(oldFont);
	font.DeleteObject();
	}
else
	{
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);

	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		// riferimenti numerici
		CString sl;
		pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		sl.Format(_T("%3.0lf"),posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);

		//---------------------------------
		// seconda riga a sx strip
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		sl.Format(_T("%3.0lf"),posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
		}
	pDC->SelectObject(oldFont);
	font.DeleteObject();
	}
	// Fine disegno pos. bobine
	//---------------------------------------------------------

}




// vecchia drawPos
/*
void CTowerView::drawPosDiaframma(CDC *pDC, CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
if (!pDoc->c_useDiaframmi)
	return;


CPen pen1,pen2,pen3,pen4,*oldPen;
	// red
	
	pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft;
	if (layout.getMirrorLabel())
		scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left +layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	pen2.CreatePen(PS_SOLID,1,RGB(0,0,0));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight;
	if (layout.getMirrorLabel())
		scPosRight = rectB.right - layout.c_sizeFBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	else
		scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		
	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	if (pDoc->c_mode == 1)
		{// disegno diaframmi testa 2 merge SingleCoil
		pen3.CreatePen(PS_SOLID,1,RGB(0,0,0));
		pDC->SelectObject(&pen3);
		double posLeft = pDoc->c_posDiaframmaSx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		if (layout.getMirrorLabel())
			scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		pen4.CreatePen(PS_SOLID,1,RGB(0,0,0));
		pDC->SelectObject(&pen4);
		double posRight = pDoc->c_posDiaframmaDx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		if (layout.getMirrorLabel())
			scPosRight = rectB.right - layout.c_sizeFBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		else
			scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		pDC->MoveTo(scPosRight,rectB.top);
		pDC->LineTo(scPosRight,rectB.bottom);
		
		pDC->SelectObject(oldPen);
		pen3.DeleteObject();
		pen4.DeleteObject();
		}
	// non importa anche se seleziono la stessa pen due volte
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------


}



void CTowerView::drawPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	

	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft;
	if (layout.getMirrorLabel())
		scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));


	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga a dx strip
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		if (layout.getMirrorLabel())
			scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		//---------------------------------
		// seconda riga a sx strip
		pDC->SelectObject(&pen1);	
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		if (layout.getMirrorLabel())
			scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------

}

// visualizzazione posizione bobine per utente
void CTowerView::drawPosBobine1(CDC *pDC,CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,2,RGB(0,0,0));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,2,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft;
	
	if (layout.getMirrorLabel())
		scPosLeft = rectB.right - layout.c_sizeFBanda-(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

//
// disegno solo inizio strip 
// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga sinistra
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		
		// check distanza con fine strip precedente
		if (i > 0)
			{
			posLeft -= (fabs(posLeft - endPosLastStrip)/2);
			}

		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
					+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		if (layout.getMirrorLabel())
			scPosLeft = rectB.right - layout.c_sizeFBanda - (1. * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + layout.c_sizeFBanda + (1. * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		if (i == (pDoc->c_rotoloMapping[lastElem].size()-1))
			{
			// seconda riga destra
			pDC->SelectObject(&pen1);	

			posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
			// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
			if (layout.getMirrorLabel())
				scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			else
				scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
}


void CTowerView::textPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);

	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft;
	if (layout.getMirrorLabel())
		scPosLeft = rectB.right - layout.c_sizeFBanda-(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		if (layout.getMirrorLabel())
			scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		// riferimenti numerici
		CString sl;
		if (layout.getMirrorLabel())
			pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		else
			pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);

		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);

		//---------------------------------
		// seconda riga a sx strip
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		if (layout.getMirrorLabel())
			scPosLeft = rectB.right - layout.c_sizeFBanda- (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));


		if (layout.getMirrorLabel())
			pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		else
			pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
		}

pDC->SelectObject(oldFont);
font.DeleteObject();

	// Fine disegno pos. bobine
	//---------------------------------------------------------

}
*/






void CTowerView::updateLabel(void)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

// Init vLabel 
// layout.setVFori	(pDoc->getFori());

layout.c_vLabel[0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendDVPositionDal();

// densita D
	{
	CString str;
	layout.setMaxY('D',pDoc->GetTrendDScalaY());
	layout.setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	layout.c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		layout.c_vLabel[2].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[2].setBColor(RGB(0,255,0));
	}
//
// densita C
	{
	CString str;
	layout.setMaxY('C',pDoc->GetTrendDScalaY());
	layout.setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	layout.c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		layout.c_vLabel[3].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[3].setBColor(RGB(0,255,0));
	}
// densita B
	{
	CString str;
	layout.setMaxY('B',pDoc->GetTrendDScalaY());
	layout.setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	layout.c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		layout.c_vLabel[4].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[4].setBColor(RGB(0,255,0));
	}
//
// densita A
	{
	CString str;
	layout.setMaxY('A',pDoc->GetTrendDScalaY());
	layout.setMinY('A',0.);

	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format(_T("%6.0lf"),densita);
	layout.c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		layout.c_vLabel[5].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[5].setBColor(RGB(0,255,0));
	}
//
layout.c_vLabel [6] = pDoc->getRotolo();
layout.c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
layout.c_vLabel [8] = d.Format( _T("%A, %B %d, %Y") );

// Visualizzazione alzata
int metriAlzata;
int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format(_T("%d"),metriAlzata);
layout.c_vLabel [9] = s;

s.Format(_T("%6.0lf"),pDoc->c_densityCalcLength);
layout.c_vLabel [10] = s;

//s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size());
// -1 indica tutti
//s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).getCurAlzNumber(-1));
s = pDoc->getCurAlzString();
layout.c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format(_T("%6.0lf"),va);
layout.c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format(_T("%6.0lf"),vb);
layout.c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format(_T("%6.0lf"),vc);
layout.c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format(_T("%6.0lf"),vd);
layout.c_vLabel [15] = s;
// Total Holes A+B+C
s.Format(_T("%6.0lf"),va+vb+vc+vd);
layout.c_vLabel [16] = s;
// Limit A
s.Format(_T("%4.0lf"),pDoc->sogliaA);
layout.c_vLabel [17] = s;
// Limit B
s.Format(_T("%4.0lf"),pDoc->sogliaB);
layout.c_vLabel [18] = s;
// Limit C
s.Format(_T("%4.0lf"),pDoc->sogliaC);
layout.c_vLabel [19] = s;
// Limit D
s.Format(_T("%4.0lf"),pDoc->sogliaD);
layout.c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format(_T("%6.02lf"),de);
layout.c_vLabel [21] = s;

}


void CTowerView::updateExtra(CDC  *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

// Disegno Posizione bobine
if(pDoc->c_viewStripBorder)
	// Disegno Posizione bobine doppia riga
	drawPosBobine2(pDC,rectB);
else
	// Disegno Posizione bobine singola riga
	drawPosBobine1(pDC,rectB);

if(pDoc->c_viewStripBorderPosition)
	textPosBobine2(pDC,rectB);
	
// Disegno Posizione diaframmi
if (pDoc->c_useDiaframmi)
	drawPosDiaframma(pDC,rectB);

}


void CTowerView::updateTower(CDC  *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
double *dVal = new double [pDoc->c_difCoil.getNumClassi()+1];	// +1 Big

for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	int j;
	for (j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		int a;
		dVal[j] = pDoc->c_difCoil.getAllDifetti(i,'A'+j);
		if (dVal[j] > 0)
			a=0;
		}
	// big 
	// dVal[j] = pDoc->c_difCoil.getTotBigHole(0,(int)pDoc->c_difCoil.getMeter(),i,i+1);
	dVal[j] = pDoc->c_difCoil.getAllBigHole(i);

	// inserito offset inizio grafico
	//clientRect.left  = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * i);
	//clientRect.right = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * (i+1));

	// towerView[i].setScale(clientRect,pDoc->GetTrendDScalaY(),dVal);
	if (layout.getMirrorLabel())
		{
		towerView[(pDoc->c_difCoil.getNumSchede()-1)-i].setVal(dVal);
		towerView[(pDoc->c_difCoil.getNumSchede()-1)-i].setPos(pDoc->c_difCoil.getNumSchede()-i);
		towerView[(pDoc->c_difCoil.getNumSchede()-1)-i].draw(pDC);
		}
	else
		{
		towerView[i].setVal(dVal);
		towerView[i].setPos(i+1); 
		towerView[i].draw(pDC);	
		}
	}
delete [] dVal;

}