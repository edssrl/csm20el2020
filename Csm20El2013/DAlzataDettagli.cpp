// DAlzataDettagli.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "MainFrm.h"
#include "GraphDoc.h"

#include "DAlzataDettagli.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDAlzataDettagli dialog

//#define USE_BOTTOM_HEAD

CDAlzataDettagli::CDAlzataDettagli(CWnd* pParent /*=NULL*/)
	: CDialog(CDAlzataDettagli::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDAlzataDettagli)
	m_alarmAS = _T("");
	m_alarmAZ = _T("");
	m_alarmBS = _T("");
	m_alarmBZ = _T("");
	m_thresholdAS = _T("");
	m_thresholdAZ = _T("");
	m_thresholdBS = _T("");
	m_thresholdBZ = _T("");
	m_alarmCS = _T("");
	m_alarmCZ = _T("");
	m_alarmDS = _T("");
	m_alarmDZ = _T("");
	m_thresholdCS = _T("");
	m_thresholdCZ = _T("");
	m_thresholdDS = _T("");
	m_thresholdDZ = _T("");
	//}}AFX_DATA_INIT

c_pDocS=NULL;
c_pDocZ=NULL;

}


void CDAlzataDettagli::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDAlzataDettagli)
	DDX_Control(pDX, IDC_OK_MSG2B, m_ctrlOkMsg2B);
	DDX_Control(pDX, IDC_OK_MSG2, m_ctrlOkMsg2);
	DDX_Control(pDX, IDC_OK_ALZATA2, m_ctrlOkAlzata2);
	DDX_Control(pDX, IDC_DETTAGLI_Z, m_ctrlDettagliZ);
	DDX_Text(pDX, IDC_ALARM_AZ, m_alarmAZ);
	DDX_Text(pDX, IDC_ALARM_BZ, m_alarmBZ);
	DDX_Text(pDX, IDC_THRESHOLD_AZ, m_thresholdAZ);
	DDX_Text(pDX, IDC_THRESHOLD_BZ, m_thresholdBZ);
	DDX_Text(pDX, IDC_ALARM_CZ, m_alarmCZ);
	DDX_Text(pDX, IDC_ALARM_DZ, m_alarmDZ);
	DDX_Text(pDX, IDC_THRESHOLD_CZ, m_thresholdCZ);
	DDX_Text(pDX, IDC_THRESHOLD_DZ, m_thresholdDZ);
#ifdef USE_BOTTOM_HEAD
	DDX_Control(pDX, IDC_OK_MSG1B, m_ctrlOkMsg1B);
	DDX_Control(pDX, IDC_OK_MSG1, m_ctrlOkMsg1);
	DDX_Control(pDX, IDC_OK_ALZATA1, m_ctrlOkAlzata1);
	DDX_Control(pDX, IDC_DETTAGLI_S, m_ctrlDettagliS);
	DDX_Text(pDX, IDC_ALARM_AS, m_alarmAS);
	DDX_Text(pDX, IDC_ALARM_BS, m_alarmBS);
	DDX_Text(pDX, IDC_THRESHOLD_AS, m_thresholdAS);
	DDX_Text(pDX, IDC_THRESHOLD_BS, m_thresholdBS);
	DDX_Text(pDX, IDC_ALARM_CS, m_alarmCS);
	DDX_Text(pDX, IDC_ALARM_DS, m_alarmDS);
	DDX_Text(pDX, IDC_THRESHOLD_CS, m_thresholdCS);
	DDX_Text(pDX, IDC_THRESHOLD_DS, m_thresholdDS);
#endif
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDAlzataDettagli, CDialog)
	//{{AFX_MSG_MAP(CDAlzataDettagli)
	ON_BN_CLICKED(IDC_ALZATA_CONTINUA, OnAlzataContinua)
	ON_BN_CLICKED(IDC_ALZATA_NUOVA, OnAlzataNuova)
	ON_BN_CLICKED(IDC_ALZATA_SCARTO, OnAlzataScarto)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDAlzataDettagli message handlers

BOOL CDAlzataDettagli::OnInitDialog() 
{

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

	CDialog::OnInitDialog();
	
// TODO: Add extra initialization here
#ifdef USE_BOTTOM_HEAD
	m_ctrlDettagliS.DeleteAllItems();
	m_ctrlDettagliS.removeHeader();
#endif
	m_ctrlDettagliZ.DeleteAllItems();
	m_ctrlDettagliZ.removeHeader();

// Populate ListCtrl
#ifdef USE_BOTTOM_HEAD
if (c_pDocS != NULL)
	{
	BOOL	okInsp = TRUE;
	// Parametri sezioni fori 
	m_thresholdAS.Format("%4.0lf",c_pDocS->sogliaA);
	// Limit B
	m_thresholdBS.Format("%4.0lf",c_pDocS->sogliaB);
	// Limit C
	m_thresholdCS.Format("%4.0lf",c_pDocS->sogliaC);
	// Limit D
	m_thresholdDS.Format("%4.0lf",c_pDocS->sogliaD);

	// soglie allarmi
	m_alarmAS.Format("%4.0lf",c_pDocS->c_difCoil.getAllarme('A'));
	// Limit B
	m_alarmBS.Format("%4.0lf",c_pDocS->c_difCoil.getAllarme('B'));
	// Limit C
	m_alarmCS.Format("%4.0lf",c_pDocS->c_difCoil.getAllarme('C'));
	// Limit D
	m_alarmDS.Format("%4.0lf",c_pDocS->c_difCoil.getAllarme('D'));

	// sezione tabella
	CString s,s1;
	int colIndex = 0;
	int rowIndex = 0;
	int lastElem = c_pDocS->c_rotoloMapping.c_lastElemento;
	FingerPos* pStripPos;	
	pStripPos = new FingerPos[c_pDocS->c_rotoloMapping[lastElem].size()+1];
	// totali per colonne +1 xChe` anche totali A+B
	int *subTotal;
	subTotal = new int [c_pDocS->c_rotoloMapping[lastElem].size()+1];
	// m_ctrlDettagliS.AddColumn("          Dati          ",colIndex++,IDC_NOEDITCELL);
	s.LoadString(CSM_ALZATADETTAGLI_DATI);
	m_ctrlDettagliS.AddColumn(s,colIndex++,IDC_NOEDITCELL);
	int i;
	for (i=0;i<c_pDocS->c_rotoloMapping[lastElem].size();i++)
		{
		// s.Format("  Strip %d  ",i+1);
		s1.LoadString(CSM_ALZATADETTAGLI_STRIP);
		if (pFrame->c_rightAlignStrip)
			s.Format(s1,c_pDocS->c_rotoloMapping[lastElem].size()-i);
		else
			s.Format(s1,i+1);
		m_ctrlDettagliS.AddColumn(s,colIndex++,IDC_NOEDITCELL);
		}
	// totali
	//s.Format (" Totale ");
	s.LoadString (CSM_ALZATADETTAGLI_TOTALE);
	m_ctrlDettagliS.AddColumn(s,colIndex++,IDC_NOEDITCELL);

	// Aggiunta dati
	AlzataDettagli alzataDettagli;
	alzataDettagli.create (c_pDocS->c_rotoloMapping[lastElem].size()+1);
	int alzata = c_pDocS->c_rotoloMapping.at(lastElem).c_alzate.size()-1;
	populateAlzataDettagli(c_pDocS,&alzataDettagli,lastElem,alzata);
	
	// 3+3 righe
	//---------------------------------------------
	// Classe A
	// s.Format("Fori Classe A");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_A);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	rowIndex = 0;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiA[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// totale A
	s.Format(" %d ",alzataDettagli.c_difettiA[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe A
	//------------------------------------------
	
	//---------------------------------------------
	// Classe B
	//s.Format("Fori Classe B");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_B);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;

	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiB[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// totale B
	s.Format(" %d ",alzataDettagli.c_difettiB[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe A
	//------------------------------------------

	//---------------------------------------------
	// Classe C
	//s.Format("Fori Classe C");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_C);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiC[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// totale C
	s.Format(" %d ",alzataDettagli.c_difettiC[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe C
	//------------------------------------------

	//---------------------------------------------
	// Classe D
	//s.Format("Fori Classe D");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_D);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiD[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// totale D
	s.Format(" %d ",alzataDettagli.c_difettiD[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe D
	//------------------------------------------

	//---------------------------------------------
	// Classe BigHole per strip
	//s.Format("BigHole");
	s.LoadString(CSM_ALZATADETTAGLI_BIGHOLE);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	int totBigHole = 0;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		// visualizzo i parziali con i precedenti
		int v = alzataDettagli.c_numBigHole[i];
		s.Format(" %d ",v);
		totBigHole += v;
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// totale BigHole
	s.Format(" %d ",totBigHole);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe BigHole
	//------------------------------------------

	//---------------------------------------------
	// Classe A+B+C+D per strip
	//s.Format("Totale Fori");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_TOTALE);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		// visualizzo i parziali con i precedenti
		s.Format(" %d ",alzataDettagli.c_difettiTotale[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// totale ABCD
	s.Format(" %d ",alzataDettagli.c_difettiTotale[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe A+B+C+D
	//------------------------------------------


	//---------------------------------------------
	// Classe PerB per strip
	//s.Format("PerB");
	s.LoadString(CSM_ALZATADETTAGLI_PERB);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	int totPerB = 0;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		// visualizzo i parziali con i precedenti
		int v = alzataDettagli.c_numPerB[i];
		s.Format(" %d ",v);
		totPerB += v;
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// totale PerB
	s.Format(" %d ",totPerB);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe BigHole
	//------------------------------------------
	
	//---------------------------------------------
	// Densita` Classe A
	// s.Format("Densita` A");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_A);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaA[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// Densita` totale A
	s.Format("%6.03lf",alzataDettagli.c_densitaA[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaA[i] > c_pDocS->c_difCoil.getAllarme('A'))
	//	okInsp = FALSE;
	// Fine Densita` Classe A
	//------------------------------------------


	//---------------------------------------------
	// Densita` Classe B
	// s.Format("Densita` B");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_B);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaB[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	s.Format("%6.03lf",alzataDettagli.c_densitaB[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaB[i] > c_pDocS->c_difCoil.getAllarme('B'))
	//	okInsp = FALSE;

	// Fine Densita` B
	//------------------------------------------

	//---------------------------------------------
	// Densita` Classe C
	// s.Format("Densita` C");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_C);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaC[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	s.Format("%6.03lf",alzataDettagli.c_densitaC[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaC[i] > c_pDocS->c_difCoil.getAllarme('C'))
	//	okInsp = FALSE;

	// Fine Densita` C
	//------------------------------------------

	//---------------------------------------------
	// Densita` Classe D
	// s.Format("Densita` D");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_D);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaD[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	s.Format("%6.03lf",alzataDettagli.c_densitaD[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaD[i] > c_pDocS->c_difCoil.getAllarme('D'))
	//	okInsp = FALSE;

	// Fine Densita` D
	//------------------------------------------

	//---------------------------------------------
	// Densita` Totale A+B
	//s.Format("Totale Densita`");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_TOTALE);
	m_ctrlDettagliS.AddRow(s);
	colIndex = 1;
	for (i=0;i<c_pDocS->c_rotoloMapping[lastElem].size();i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaTotale[i]);
		m_ctrlDettagliS.AddItem(rowIndex,colIndex++,s);
		}

	// Densita` totale A+B+B+C
	s.Format("%6.03lf",alzataDettagli.c_densitaTotale[i]);
	m_ctrlDettagliS.AddItem(rowIndex++,colIndex++,s);
	// Fine Densita` A+B
	//------------------------------------------
	// check presenza difetti bigHole
	m_ctrlOkMsg1.SetWindowText(CString(""));
	m_ctrlOkMsg1B.SetWindowText(CString(""));
	if(c_pDocS->c_densityAlarmActive)
		{
		okInsp = FALSE;
		CString s = "Density Threshold Alarm";
		m_ctrlOkMsg1B.SetWindowText(s);
		}
	if(totBigHole > 0)
		{
		okInsp = FALSE;
		CString s = "Big Hole Alarm";
		m_ctrlOkMsg1.SetWindowText(s);
		}

	if (okInsp)
		m_ctrlOkAlzata1.addString(CString("OK    "),CString("Arial"),110,RGB(0,255,0),RGB(192,192,192));
	else
		m_ctrlOkAlzata1.addString(CString("NOT OK"),CString("Arial"),110,RGB(255,0,0),RGB(192,192,192));
	
	delete [] pStripPos;
	delete [] subTotal;
	}
#endif

if (c_pDocZ != NULL)
	{
	BOOL	okInsp = TRUE;
	// Parametri sezioni fori 
	m_thresholdAZ.Format("%4.0lf",c_pDocZ->sogliaA);
	// Limit B
	m_thresholdBZ.Format("%4.0lf",c_pDocZ->sogliaB);
	// Limit C
	m_thresholdCZ.Format("%4.0lf",c_pDocZ->sogliaC);
	// Limit D
	m_thresholdDZ.Format("%4.0lf",c_pDocZ->sogliaD);
	// soglie allarmi
	m_alarmAZ.Format("%4.0lf",c_pDocZ->c_difCoil.getAllarme('A'));
	// Limit B
	m_alarmBZ.Format("%4.0lf",c_pDocZ->c_difCoil.getAllarme('B'));
	// Limit C
	m_alarmCZ.Format("%4.0lf",c_pDocZ->c_difCoil.getAllarme('C'));
	// Limit D
	m_alarmDZ.Format("%4.0lf",c_pDocZ->c_difCoil.getAllarme('D'));

	// sezione tabella
	int colIndex = 0;
	int rowIndex = 0;
	CString s,s1;
	int lastElem = c_pDocZ->c_rotoloMapping.c_lastElemento;
	FingerPos* pStripPos;	
	pStripPos = new FingerPos[c_pDocZ->c_rotoloMapping[lastElem].size()+1];
	int *subTotal;
	subTotal = new int [c_pDocZ->c_rotoloMapping[lastElem].size()+1];

	// m_ctrlDettagliZ.AddColumn("          Dati          ",colIndex++,IDC_NOEDITCELL);
	s.LoadString(CSM_ALZATADETTAGLI_DATI);
	m_ctrlDettagliZ.AddColumn(s,colIndex++,IDC_NOEDITCELL);
	int i;
	for (i=0;i<c_pDocZ->c_rotoloMapping[lastElem].size();i++)
		{
		//s.Format("  Strip %d  ",i+1);
		s1.LoadString(CSM_ALZATADETTAGLI_STRIP);
		if (c_pDocZ->c_rightAlignStrip)
			s.Format(s1,c_pDocS->c_rotoloMapping[lastElem].size()-i);
		else
			s.Format(s1,i+1);
		m_ctrlDettagliZ.AddColumn(s,colIndex++,IDC_NOEDITCELL);
		}
	// totali
	//s.Format (" Totale ");
	s.LoadString (CSM_ALZATADETTAGLI_TOTALE);
	m_ctrlDettagliZ.AddColumn(s,colIndex++,IDC_NOEDITCELL);

	// Aggiunta dati
	AlzataDettagli alzataDettagli;
	alzataDettagli.create (c_pDocZ->c_rotoloMapping[lastElem].size()+1);
	int alzata = c_pDocZ->c_rotoloMapping.at(lastElem).c_alzate.size()-1;
	populateAlzataDettagli(c_pDocZ,&alzataDettagli,lastElem,alzata);


	// 3+3 righe
	//---------------------------------------------
	// Classe A
	//s.Format("Fori Classe A");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_A);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiA[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// totale A
	s.Format(" %d ",alzataDettagli.c_difettiA[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe A
	//------------------------------------------
	
	//---------------------------------------------
	// Classe B
	//s.Format("Fori Classe B");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_B);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiB[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// totale B
	s.Format(" %d ",alzataDettagli.c_difettiB[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe A
	//------------------------------------------

	//---------------------------------------------
	// Classe C
	//s.Format("Fori Classe C");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_C);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiC[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// totale C
	s.Format(" %d ",alzataDettagli.c_difettiC[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe C
	//------------------------------------------

	//---------------------------------------------
	// Classe D
	//s.Format("Fori Classe D");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_D);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format(" %d ",alzataDettagli.c_difettiD[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// totale D
	s.Format(" %d ",alzataDettagli.c_difettiD[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe D
	//------------------------------------------

	//---------------------------------------------
	// Classe BigHole per strip
	//s.Format("BigHole");
	s.LoadString(CSM_ALZATADETTAGLI_BIGHOLE);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	int totBigHole = 0;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		// visualizzo i parziali con i precedenti
		int v = alzataDettagli.c_numBigHole[i];
		s.Format(" %d ",v);
		totBigHole += v;
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// totale BigHole
	s.Format(" %d ",totBigHole);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe BigHole
	//------------------------------------------
	
	//---------------------------------------------
	// Classe ABCD per strip
	//s.Format("Totale Fori");
	s.LoadString(CSM_ALZATADETTAGLI_FORI_TOTALE);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		// visualizzo i parziali con i precedenti
		s.Format(" %d ",alzataDettagli.c_difettiTotale[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// totale ABCD
	s.Format(" %d ",alzataDettagli.c_difettiTotale[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe ABCD
	//------------------------------------------


	//---------------------------------------------
	// Classe PerB per strip
	//s.Format("PerB");
	s.LoadString(CSM_ALZATADETTAGLI_PERB);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	int totPerB = 0;
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		// visualizzo i parziali con i precedenti
		int v = alzataDettagli.c_numPerB[i];
		s.Format(" %d ",v);
		totPerB += v;
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// totale PerB
	s.Format(" %d ",totPerB);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Classe BigHole
	//------------------------------------------

	//---------------------------------------------
	// Densita` Classe A
	//s.Format("Densita` A");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_A);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B
	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaA[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// Densita` totale A
	s.Format("%6.03lf",alzataDettagli.c_densitaA[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaA[i] > c_pDocS->c_difCoil.getAllarme('A'))
	//	okInsp = FALSE;

	// Fine Densita` Classe A
	//------------------------------------------


	//---------------------------------------------
	// Densita` Classe B
	//s.Format("Densita` B");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_B);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaB[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	s.Format("%6.03lf",alzataDettagli.c_densitaB[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaB[i] > c_pDocS->c_difCoil.getAllarme('B'))
	//	okInsp = FALSE;

	// Fine Densita` B
	//------------------------------------------

	//---------------------------------------------
	// Densita` Classe C
	//s.Format("Densita` C");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_C);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaC[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	s.Format("%6.03lf",alzataDettagli.c_densitaC[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaC[i] > c_pDocS->c_difCoil.getAllarme('C'))
	//	okInsp = FALSE;
	// Fine Densita` C
	//------------------------------------------

	//---------------------------------------------
	// Densita` Classe D
	//s.Format("Densita` D");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_D);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<alzataDettagli.c_numStrip;i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaD[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	s.Format("%6.03lf",alzataDettagli.c_densitaD[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// flag scritta allarme
	//if (alzataDettagli.c_densitaD[i] > c_pDocS->c_difCoil.getAllarme('D'))
	//	okInsp = FALSE;
	// Fine Densita` D
	//------------------------------------------

	//---------------------------------------------
	// Densita` Totale A+B
	//s.Format("Totale Densita`");
	s.LoadString(CSM_ALZATADETTAGLI_DENSITA_TOTALE);
	m_ctrlDettagliZ.AddRow(s);
	colIndex = 1;
	for (i=0;i<c_pDocZ->c_rotoloMapping[lastElem].size();i++)
		{
		s.Format("%6.03lf",alzataDettagli.c_densitaTotale[i]);
		m_ctrlDettagliZ.AddItem(rowIndex,colIndex++,s);
		}

	// Densita` totale A+B
	s.Format("%6.03lf",alzataDettagli.c_densitaTotale[i]);
	m_ctrlDettagliZ.AddItem(rowIndex++,colIndex++,s);
	// Fine Densita` A+B
	//------------------------------------------
	// check presenza difetti bigHole
	m_ctrlOkMsg2.SetWindowText(CString(""));
	m_ctrlOkMsg2B.SetWindowText(CString(""));
	if(c_pDocZ->c_densityAlarmActive)
		{
		okInsp = FALSE;
		CString s = "Density Threshold Alarm";
		m_ctrlOkMsg2B.SetWindowText(s);
		}
	if(totBigHole > 0)
		{
		okInsp = FALSE;
		CString s = "Big Hole Alarm";
		m_ctrlOkMsg2.SetWindowText(s);
		}

	if (okInsp)
		m_ctrlOkAlzata2.addString(CString("OK    "),CString("Arial"),110,RGB(0,255,0),RGB(192,192,192));
	else
		m_ctrlOkAlzata2.addString(CString("NOT OK"),CString("Arial"),110,RGB(255,0,0),RGB(192,192,192));

	
	delete [] pStripPos;
	delete [] subTotal;

	}

UpdateData(FALSE);

return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}





void CDAlzataDettagli::createStripPos (CLineDoc* pDoc,FingerPos* pStripPos,int elem)
{


// per ogni strip o bobina ho memorizzato posizione x e dimensione
// 
if(pDoc->c_rightAlignStrip)
	{
	for (int i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{// overlapping da 9 a 2 (19-10-17 )
		// pStripPos[i].xRight = (pDoc->c_rotoloMapping[elem])[i].c_xPos + 9;
		pStripPos[i].xRight = (pDoc->c_rotoloMapping[elem])[i].c_xPos + 2;
		//pStripPos[i].xLeft = (pDoc->c_rotoloMapping[elem])[i].c_xPos -
		//						(pDoc->c_rotoloMapping[elem])[i].c_larghezza - 9; 
		pStripPos[i].xLeft = (pDoc->c_rotoloMapping[elem])[i].c_xPos -
								(pDoc->c_rotoloMapping[elem])[i].c_larghezza -2; 
		}
	}
else
	{
	for (int i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{// overlapping da 9 a 2 (19-10-17 )
		pStripPos[i].xLeft = (pDoc->c_rotoloMapping[elem])[i].c_xPos - 2;
		pStripPos[i].xRight = (pDoc->c_rotoloMapping[elem])[i].c_xPos +
								(pDoc->c_rotoloMapping[elem])[i].c_larghezza + 2; 
		}
	}
}

// ultimo parametro bitmap inclusione classi difetti
// A=bit0, B=bit1, C=bit2, D=bit3, Big=bit4
BOOL CDAlzataDettagli::populateAlzataDettagli(CLineDoc *pDoc,AlzataDettagli *pAlzataDettagli,
											  int elem,int alzata,int classBitmap)
	{
	FingerPos* pStripPos;	
	pStripPos = new FingerPos[pDoc->c_rotoloMapping[elem].size()+1];
	// totali per colonne +1 xChe` anche totali A+B
	int *subTotal;
	subTotal = new int [pDoc->c_rotoloMapping[elem].size()+1];

	pAlzataDettagli->c_numStrip = pDoc->c_rotoloMapping[elem].size();

	// get updated larghezza from shutter and trimLeft
	// double totalLarge = pDoc->c_larghezza;

	// 3+3 righe
	//---------------------------------------------
	// Classe A
	int iTotal=0;
	createStripPos(pDoc,pStripPos,elem);
	int from = pDoc->c_rotoloMapping.at(elem).c_alzate.at(alzata).c_yPos;
	int to;
	// ultima alzata?
	if (alzata == (pDoc->c_rotoloMapping.at(elem).c_alzate.size()-1))
		to = pDoc->GetVPosition();
	else
		to = pDoc->c_rotoloMapping.at(elem).c_alzate.at(alzata+1).c_yPos;
	int i;
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;	
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;


		int fori = pDoc->c_difCoil.getTotDifetti('A',from,to,leftCell,rightCell);
		// includere classe A nei totali?
		iTotal += (classBitmap & 0x01)?fori:0;
		subTotal[i] = (classBitmap & 0x01)?fori:0;
		pAlzataDettagli->c_difettiA[i] = fori; 
		}

	// totale A
	subTotal[i] = iTotal;
	pAlzataDettagli->c_difettiA[i] = iTotal;
	// Fine Classe A
	//------------------------------------------
	
	//---------------------------------------------
	// Classe B
	iTotal=0;
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;	
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;

		
		int fori = pDoc->c_difCoil.getTotDifetti('B',from,to,leftCell,rightCell);
		// includere classe B nei totali?
		iTotal += (classBitmap & 0x02)?fori:0;
		subTotal[i] += (classBitmap & 0x02)?fori:0;
		pAlzataDettagli->c_difettiB[i] = fori;
		}

	// totale B
	subTotal[i] += iTotal;
	pAlzataDettagli->c_difettiB[i] = iTotal;
	// Fine Classe B
	//------------------------------------------
	
	//---------------------------------------------
	// Classe C
	iTotal=0;
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
				// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;	
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;

		int fori = pDoc->c_difCoil.getTotDifetti('C',from,to,leftCell,rightCell);
		// includere classe C nei totali?
		iTotal += (classBitmap & 0x04)?fori:0;
		subTotal[i] += (classBitmap & 0x04)?fori:0;
		pAlzataDettagli->c_difettiC[i] = fori;
		}

	// totale C
	subTotal[i] += iTotal;
	pAlzataDettagli->c_difettiC[i] = iTotal;
	// Fine Classe C
	//------------------------------------------

	//---------------------------------------------
	// Classe D
	iTotal=0;
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;	
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;
		int fori = pDoc->c_difCoil.getTotDifetti('D',from,to,leftCell,rightCell);
		// includere classe D nei totali?
		iTotal += (classBitmap & 0x08)?fori:0;
		subTotal[i] += (classBitmap & 0x08)?fori:0; 
		pAlzataDettagli->c_difettiD[i] = fori;
		}

	// totale D
	subTotal[i] += iTotal;
	pAlzataDettagli->c_difettiD[i] = iTotal;
	// Fine Classe D
	//------------------------------------------

	// BigHole e perB
	iTotal=0;
	int perTotal = 0;
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;
		int fori = pDoc->c_difCoil.getTotBigHole(from,to,leftCell,rightCell);
		pAlzataDettagli->c_numBigHole[i] = fori;
		// includere classe Big nei totali?
		iTotal += (classBitmap & 0x10)?fori:0;
		subTotal[i] += (classBitmap & 0x10)?fori:0;
		pAlzataDettagli->c_numPerB[i] = pDoc->c_difCoil.getTotPerB(from,to,leftCell,rightCell);
		perTotal += pAlzataDettagli->c_numPerB[i];
		}
	// totale Big
	subTotal[i] += iTotal;
	pAlzataDettagli->c_numBigHole[i] = iTotal;
	// totale perB
	pAlzataDettagli->c_numPerB[i] = perTotal;
	// Fine Classe Big

	//---------------------------------------------
	// Classe A+B per strip
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		// visualizzo i parziali con i precedenti
		pAlzataDettagli->c_difettiTotale[i] = subTotal[i];
		}

	// totale A+B+Big
	pAlzataDettagli->c_difettiTotale[i] = subTotal[i];
	// Fine Classe A+B
	//------------------------------------------
	// check difetti laminazione
	pAlzataDettagli->c_laminationAlarm = FALSE;
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		pAlzataDettagli->c_difLamination[i] = checkDifLamination(
				pAlzataDettagli->c_difettiA[i],
				pAlzataDettagli->c_difettiB[i],
				pAlzataDettagli->c_difettiC[i],
				pAlzataDettagli->c_difettiD[i],
				pAlzataDettagli->c_numBigHole[i]);
		pAlzataDettagli->c_laminationAlarm |= pAlzataDettagli->c_difLamination[i];
		}


	//---------------------------------------------
	// Densita` Classe A
	// totali per colonne +1 xChe` anche totali A+B
	double *subDensity;
	subDensity = new double [pDoc->c_rotoloMapping[elem].size()+1];
	double totalLarge=0.;
	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;
		totalLarge += (pStripPos[i].xRight-pStripPos[i].xLeft);
		double width = pStripPos[i].xRight-pStripPos[i].xLeft;
		double dens = pDoc->c_difCoil.getTotalDensity('A',from,to,leftCell,rightCell,width);
		subDensity[i] = dens;
		pAlzataDettagli->c_densitaA[i] = dens;
		}

	// Densita` totale A
	double totalArea = 0.001*totalLarge * (to-from);
	double dens;
	if (totalArea == 0.)
		dens = -1.;
	else
		//dens = pDoc->c_difCoil.getTotDifetti('A',from,to)/totalArea;
		//				
		dens = pAlzataDettagli->c_difettiA[i]/totalArea;
	
	subDensity[i] = dens;
	pAlzataDettagli->c_densitaA[i] = subDensity[i];
	
	// Fine Densita` Classe A
	//------------------------------------------


	//---------------------------------------------
	// Densita` Classe B
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;
		double width = pStripPos[i].xRight-pStripPos[i].xLeft;
		double dens = pDoc->c_difCoil.getTotalDensity('B',from,to,leftCell,rightCell,width);
		subDensity[i] += dens;
		pAlzataDettagli->c_densitaB[i] = dens;
		}

	// Densita` totale B

	if (totalArea == 0.)
		dens = -1.;
	else
		// dens = pDoc->c_difCoil.getTotDifetti('B',from,to)/totalArea;
		dens = pAlzataDettagli->c_difettiB[i]/totalArea;

	subDensity[i] += dens;
	pAlzataDettagli->c_densitaB[i] = dens;
	// Fine Densita` B
	//------------------------------------------

	//---------------------------------------------
	// Densita` Classe C
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;
		double width = pStripPos[i].xRight-pStripPos[i].xLeft;
		double dens = pDoc->c_difCoil.getTotalDensity('C',from,to,leftCell,rightCell,width);
		subDensity[i] += dens;
		pAlzataDettagli->c_densitaC[i] = dens;
		}

	// Densita` totale C
	if (totalArea == 0.)
		dens = -1.;
	else
		// dens = pDoc->c_difCoil.getTotDifetti('C',from,to)/totalArea;
		dens = pAlzataDettagli->c_difettiC[i]/totalArea;

	subDensity[i] += dens;
	pAlzataDettagli->c_densitaC[i] = dens;
	// Fine Densita` C
	//------------------------------------------

	//---------------------------------------------
	// Densita` Classe D
	// totali per colonne +1 xChe` anche totali A+B

	for (i=0;i<pDoc->c_rotoloMapping[elem].size();i++)
		{
		int leftCell,rightCell;
		leftCell = pStripPos[i].xLeft / SIZE_BANDE;
		rightCell = pStripPos[i].xRight / SIZE_BANDE;
		// se cella superiore non a cavallo allora esclusa 
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)==0)
			rightCell--;
		// se invece la cella e` a cavallo dprendo anche quella dopo
		if (( (int)pStripPos[i].xRight % (int)SIZE_BANDE)>0)
			rightCell++;
		double width = pStripPos[i].xRight-pStripPos[i].xLeft;
		double dens = pDoc->c_difCoil.getTotalDensity('D',from,to,leftCell,rightCell,width);
		subDensity[i] += dens;
		pAlzataDettagli->c_densitaD[i] = dens;
		}

	// Densita` totale D
	if (totalArea == 0.)
		dens = -1.;
	else
		// dens = pDoc->c_difCoil.getTotDifetti('D',from,to)/totalArea;
		dens = pAlzataDettagli->c_difettiD[i]/totalArea;

	subDensity[i] += dens;
	pAlzataDettagli->c_densitaD[i] = dens;
	// Fine Densita` D
	//------------------------------------------

	//---------------------------------------------
	// Densita` Totale A+B
	for (i=0;i<pDoc->c_rotoloMapping[elem].size()+1;i++)
		{
		pAlzataDettagli->c_densitaTotale[i] = ((classBitmap>>i) & 0x01)?subDensity[i]:0;
		}

	// Densita` totale A+B
	// Fine Densita` A+B
	//------------------------------------------

	delete [] pStripPos;
	delete [] subTotal;
	delete [] subDensity;
	
return TRUE;
}


// Visualizza dialog modeless
void CDAlzataDettagli::visualizza(CWnd *pWnd)
{

if (!(::IsWindow(m_hWnd)))
	Create(IDD_ALZATA_DETTAGLI,pWnd);

OnInitDialog();
UpdateData(FALSE);

ShowWindow(SW_SHOW);
AfxGetMainWnd()->SetFocus();

}

BOOL CDAlzataDettagli::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	return CDialog::PreTranslateMessage(pMsg);
}

void CDAlzataDettagli::OnAlzataContinua() 
{
// TODO: Add your control notification handler code here
EndDialog(ALZATA_CONTINUA);
}

void CDAlzataDettagli::OnAlzataNuova() 
{
	// TODO: Add your control notification handler code here
EndDialog(ALZATA_NUOVA);
	
}

void CDAlzataDettagli::OnAlzataScarto() 
{
// TODO: Add your control notification handler code here
EndDialog(ALZATA_SCARTO);
}


BOOL	CDAlzataDettagli::checkDifLamination(int A,int B, int C, int D, int Big)
{

if ((A+B+C+D+Big) < 3)
	return FALSE;

if (((C+D+Big) == 0)&&
	(B <= 5)&&(A<50))
	return FALSE;

if ((A+B+C+D+Big) < 3)
	return FALSE;

return TRUE;

}
