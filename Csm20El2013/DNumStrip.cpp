// DNumStrip.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "dyntempl.h"

#include "Csm20El2013.h"

#include "DNumStrip.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDNumStrip dialog


CDNumStrip::CDNumStrip(CWnd* pParent /*=NULL*/)
	: CDialog(CDNumStrip::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDNumStrip)
	m_numStrip = 0;
	//}}AFX_DATA_INIT
}


void CDNumStrip::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDNumStrip)
	DDX_Text(pDX, IDC_NUM_STRIP, m_numStrip);
	DDV_MinMaxInt(pDX, m_numStrip, 0, 100);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDNumStrip, CDialog)
	//{{AFX_MSG_MAP(CDNumStrip)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDNumStrip message handlers
