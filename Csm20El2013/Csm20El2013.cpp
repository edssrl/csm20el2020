// CSM.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"

#define MAIN_CSM20
#include "MainFrm.h"
#include "resource.h"

#include "Dialog.h"			// Dialog

#include "GraphDoc.h"
#include "GraphView.h"
#include "RollView.h"
#include "DoubleRollView.h"
#include "TowerView.h"
#include "DoubleTowerView.h"
#include "MLineView.h"
#include "DoubleMLineView.h"
#include "MarkerView.h"

#include "dyntempl.h"

#include "smChildFrame.h"
#include "Csm20El2013.h"

#include "Csm20El2013Doc.h"
#include "LogOff.h"

#include "versione.h"


#include <dos.h>
#include <direct.h>
#include "pwdlg.h"
#include "FInputOutput.h"

#include "EnumPrinters.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// init easylogging
_INITIALIZE_EASYLOGGINGPP


/////////////////////////////////////////////////////////////////////////////
// CCSMApp

BEGIN_MESSAGE_MAP(CCSMApp, CWinApp)
	//{{AFX_MSG_MAP(CCSMApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_PRINT_SETUP, OnFilePrintSetup)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
//	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCSMApp construction

CCSMApp::CCSMApp()
{
hModRes = NULL;
hDefRes = NULL;
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCSMApp object

CCSMApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCSMApp initialization

// To add another view, put it in this table.
// Default view is first, must be in same order as "View As" menu commands.
//
static const DYNAMICVIEWINFO MyViewClasses[] = {
   { RUNTIME_CLASS(CInitView), "CERCAFORI" },
   { RUNTIME_CLASS(CMarkerView), "Marker Map" },
   { RUNTIME_CLASS(CRollView), "Rolling MAP" },
   { RUNTIME_CLASS(CTowerView),"DENSITA' TRASVERSALE" },
   { RUNTIME_CLASS(CMLineView), "TREND LONGITUDINALE" },
   { RUNTIME_CLASS(CFInputOutput), "INPUT OUTPUT" },
//   { RUNTIME_CLASS(CDoubleRollView), "Rolling MAP" },
//   { RUNTIME_CLASS(CDoubleTowerView),"DENSITA' TRASVERSALE" },
//   { RUNTIME_CLASS(CDoubleMLineView), "TREND LONGITUDINALE" },
  { NULL, NULL }
};




BOOL CCSMApp::InitInstance()
{
// ini file in current directory
SetIniPath();


{	// BLOCK: doc template registration
		// Register the document template.  Document templates serve
		// as the connection between documents, frame windows and views.
		// Attach this form to another document or frame window by changing
		// the document or frame class in the constructor below.
//		CMultiDocTemplate* pNewDocTemplate = new CMultiDocTemplate(
//			IDR_FINPUTOUTPUT_TMPL,
//	 		RUNTIME_CLASS(CLineDoc),		// document class
//			RUNTIME_CLASS(CMDIChildWnd),		// frame class
//			RUNTIME_CLASS(CFInputOutput));		// view class
//		AddDocTemplate(pNewDocTemplate);
	}

	// CG: This line was added by the OLE Control Containment component
	AfxEnableControlContainer();
 

	// CG: This code was inserted by the Password Dialog Component
	/*{
	if (!CPasswordDlg::CheckPassword())
		return FALSE;
	} */
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)


	CString logFileName;
#ifdef _DEBUG
	logFileName = m_pszAppName;
#else
	CString appName = m_pszExeName;		// Get the "MyExe" portion of "MyExe.exe". Or, "MyDll" if RunDll32 is used.
	appName.Append(_T(".exe"));			// Now has "MyExe.exe" (or "MyDll.dll").
	// open or create dbase, name = appName+Config.sdb
	HMODULE hmod = GetModuleHandle(appName);  
 	CString fullPath;
	DWORD pathLen = ::GetModuleFileName( hmod, fullPath.GetBufferSetLength(MAX_PATH+1), MAX_PATH); // hmod of zero gets the main EXE
	fullPath.ReleaseBuffer( pathLen ); // Note that ReleaseBuffer doesn't need a +1 for the null byte.
	
	logFileName = fullPath.Left(fullPath.GetLength() - 4);// tolgo .exe
#endif
	// log fileName
	logFileName+= _T("Log.log");

	easyLogConfig("",CStringA(logFileName));

	LINFO << "Csm20El2013 Starting..";


	// spostato qui perche` altrimenti i TRACE presenti nel codice  ReadLocalPrinters() ;	
	// cercano di scrivere in LDEBUG e assert exception ( solo in modalita` debug )
	m_PrinterControl.init();


	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	CString resVerIt;
	if (!resVerIt.LoadString(CSM_RESVERDLL))
		{
		AfxMessageBox ("Attenzione Stringa Versione non caricabile");
		return (FALSE);		
		}
	CProfile profile;
	CString dllName;
	dllName = profile.getProfileString("init","LangDll","NONE");
	if (dllName != "NONE")
		{
		if ((hModRes = AfxLoadLibrary((LPCSTR)dllName)) != NULL)
			{
			hDefRes = AfxGetResourceHandle();
			AfxSetResourceHandle (hModRes);	
			CString s;
			if (!s.LoadString(CSM_RESVERDLL))
				{
				AfxMessageBox ("Attenzione DLL di localizzazione non corretta");
				return (FALSE);		
				}
			if (s != resVerIt)
				{
				AfxMessageBox ("Attenzione Versione DLL di localizzazione non corretta");
				return (FALSE);		
				}
			}
		}

	// restore default printer
	m_PrinterControl.RestorePrinterSelection(m_hDevMode, m_hDevNames) ;
	
	//CSingleDocTemplate* pDocTemplate;
	//pDocTemplate = new CSingleDocTemplate(
	pDocTemplate = new CDynViewDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(Csm20El2013Doc),
		RUNTIME_CLASS(CsmChildFrame),    // main Child frame window
//		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		MyViewClasses);

	AddDocTemplate(pDocTemplate);

	dBase = new  CDaoDbCreate();
	if (dBase == NULL)
		return FALSE;

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// ByFC
	m_nCmdShow = SW_SHOWMAXIMIZED;

	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	//ByFC
	//SetPrintLandscape();
	SetPrintPortrait();

// start first frame
	OnFileNew();

// start second frame
	OnFileNew();

	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	CLineDoc *pDoc;
	pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	if (pDoc != NULL)
		pDoc->setSide(CSM_TOP_SIDE);

	pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	if (pDoc != NULL)
		pDoc->setSide(CSM_BOTTOM_SIDE);

	// Dispatch commands specified on the command line
	// if (!ProcessShellCommand(cmdInfo))
	// 	return FALSE;

	return TRUE;
}

void CCSMApp::OnFilePrintSetup() 
{
	// TODO: Add your command handler code here

if (!CPasswordDlg::CheckPassword())
	return;

CWinApp::OnFilePrintSetup();
}


// return the name of the currently selected printer
CString CCSMApp::GetDefaultPrinter(void)
{
	PRINTDLG	pd ;
	CString		printer("Failed") ;
	
	pd.lStructSize = (DWORD)sizeof(PRINTDLG) ;
	BOOL bRet = GetPrinterDeviceDefaults(&pd) ;
	if (bRet)
		{
		// protect memory handle with ::GlobalLock and ::GlobalUnlock
		DEVMODE *pDevMode = (DEVMODE*)::GlobalLock(m_hDevMode) ;
		printer = pDevMode->dmDeviceName ;
		::GlobalUnlock(m_hDevMode) ;
		}
return printer ;
}




int CCSMApp::ExitInstance() 
{
// TODO: Add your specialized code here and/or call the base class

if (hDefRes != NULL)
	{
	AfxSetResourceHandle (hDefRes);	
	AfxFreeLibrary (hModRes);
	}
	
// save the printer selection for a next run restore
m_PrinterControl.SavePrinterSelection(m_hDevMode, m_hDevNames) ;

return CWinApp::ExitInstance();
}


void CCSMApp::SetPrintLandscape()
    {
    // Get default printer settings.
    PRINTDLG   pd;
    pd.lStructSize = (DWORD) sizeof(PRINTDLG);
    if (GetPrinterDeviceDefaults(&pd))
        {
        // Lock memory handle.
        DEVMODE FAR* pDevMode =
            (DEVMODE FAR*)::GlobalLock(m_hDevMode);
        if (pDevMode)
            {
            // Change printer settings in here.
            pDevMode->dmOrientation = DMORIENT_LANDSCAPE;
 
            // Unlock memory handle.
            ::GlobalUnlock(m_hDevMode);
            }
        }
    }


void CCSMApp::SetPrintPortrait()
    {
    // Get default printer settings.
    PRINTDLG   pd;
    pd.lStructSize = (DWORD) sizeof(PRINTDLG);
    if (GetPrinterDeviceDefaults(&pd))
        {
        // Lock memory handle.
        DEVMODE FAR* pDevMode =
            (DEVMODE FAR*)::GlobalLock(m_hDevMode);
        if (pDevMode)
            {
            // Change printer settings in here.
            pDevMode->dmOrientation =  DMORIENT_PORTRAIT;
 
            // Unlock memory handle.
            ::GlobalUnlock(m_hDevMode);
            }
        }
	 }

CLineDoc *CCSMApp::GetDocumentTop (void ) 
{

POSITION pos = pDocTemplate->GetFirstDocPosition(); 
if (pos == NULL)
	return NULL;

CLineDoc* pDoc;
pDoc = (CLineDoc*) pDocTemplate->GetNextDoc(pos);

if (pDoc == NULL)
	return NULL;

if((pDoc->c_side == CSM_TOP_SIDE)||
	(pDoc->c_side == CSM_SINGLE_SIDE))
	return pDoc;
else
	{
	if (pos == NULL)
		return NULL;
	pDoc = (CLineDoc*) pDocTemplate->GetNextDoc(pos);
	if((pDoc->c_side == CSM_TOP_SIDE)||
		(pDoc->c_side == CSM_SINGLE_SIDE))
		return pDoc;
	else
		return NULL;
	}
}


CLineDoc *CCSMApp::GetDocumentBottom (void ) 
{
POSITION pos = pDocTemplate->GetFirstDocPosition(); 

if (pos == NULL)
	return NULL; 

CLineDoc* pDoc;
pDoc = (CLineDoc*) pDocTemplate->GetNextDoc(pos);

if (pDoc == NULL)
	return NULL;

if(pDoc->c_side == CSM_BOTTOM_SIDE)
	return pDoc;
else
	{
	pDoc = (CLineDoc*) pDocTemplate->GetNextDoc(pos);
	if(pDoc->c_side == CSM_BOTTOM_SIDE)
		return pDoc;
	else
		return NULL;
	}
}

int CCSMApp::GetNumDocument (void ) 
{
POSITION pos = pDocTemplate->GetFirstDocPosition(); 
 
CLineDoc* pDoc;
pDoc = (CLineDoc*) pDocTemplate->GetNextDoc(pos);

int numDoc=0;
if (pDoc != NULL)
	numDoc ++;
while (pos != NULL)
	{
	numDoc ++;
	pDoc = (CLineDoc*) pDocTemplate->GetNextDoc(pos);
	}
return numDoc;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CString	m_head;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	m_head = _T("");
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Text(pDX, IDC_HEAD, m_head);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CCSMApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CCSMApp commands

BOOL CAboutDlg::OnInitDialog()
{
	// CG: Folowing code is added by System Info Component
	{
	CDialog::OnInitDialog();

	CString strFreeDiskSpace;
	CString strFreeMemory;
	CString strFmt;

	// Fill available memory
	MEMORYSTATUS MemStat;
	MemStat.dwLength = sizeof(MEMORYSTATUS);
	GlobalMemoryStatus(&MemStat);
	strFmt.LoadString(CG_IDS_PHYSICAL_MEM);
	strFreeMemory.Format(strFmt, MemStat.dwTotalPhys / 1024L);

	//TODO: Add a static control to your About Box to receive the memory
	//      information.  Initialize the control with code like this:
	SetDlgItemText(IDC_PHYSICAL_MEM, strFreeMemory);

	// Fill disk free information
	struct _diskfree_t diskfree;
	int nDrive = _getdrive(); // use current default drive
	if (_getdiskfree(nDrive, &diskfree) == 0)
	{
		strFmt.LoadString(CG_IDS_DISK_SPACE);
		strFreeDiskSpace.Format(strFmt,
			(DWORD)diskfree.avail_clusters *
			(DWORD)diskfree.sectors_per_cluster *
			(DWORD)diskfree.bytes_per_sector / (DWORD)1024L,
			nDrive-1 + _T('A'));
	}
 	else
 		strFreeDiskSpace.LoadString(CG_IDS_DISK_SPACE_UNAVAIL);

	//TODO: Add a static control to your About Box to receive the memory
	//      information.  Initialize the control with code like this:
	SetDlgItemText(IDC_DISK_SPACE, strFreeDiskSpace);
	
#ifdef _DEBUG
	char m = 'd';
#else
	char m = 'r';
#endif

	CString Ver;
	Ver.Format ("Csm20El2013 Ver. %4.02lf%c del %s",VERSIONE,m,DATAVERSIONE);
	SetDlgItemText(IDC_VERSIONE,Ver);

	CLineDoc *pTopDoc;
	CLineDoc *pBotDoc;
	pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
	pBotDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
	int numHead = ((CCSMApp *)AfxGetApp())->GetNumDocument();
	CString headInfo,headTop,headBottom;
	
	if (pTopDoc != NULL)
		headTop = pTopDoc->c_side;
	else
		headTop = " NULL ";
	
	if (pBotDoc != NULL)
		headBottom = pBotDoc->c_side;
	else
		headBottom = " NULL ";

	headInfo.Format("Head %d , [ %s %s ]",numHead,headTop,headBottom);
	SetDlgItemText(IDC_HEAD,headInfo);

	}

	return TRUE;
}


void  CCSMApp::SetIniPath()
{
	
	CString appName = m_pszExeName;		// Get the "MyExe" portion of "MyExe.exe". Or, "MyDll" if RunDll32 is used.
	appName.Append(_T(".exe"));			// Now has "MyExe.exe" (or "MyDll.dll").
	// open or create dbase, name = appName+Config.sdb
	HMODULE hmod = GetModuleHandle(appName);  
 	CString fullPath;
	DWORD pathLen = ::GetModuleFileName( hmod, fullPath.GetBufferSetLength(MAX_PATH+1), MAX_PATH); // hmod of zero gets the main EXE
	fullPath.ReleaseBuffer( pathLen ); // Note that ReleaseBuffer doesn't need a +1 for the null byte.
	
	CString iniFileName;


	iniFileName = fullPath.Left(fullPath.GetLength() - 4);// tolgo .exe
	c_logFileName = fullPath.Left(fullPath.GetLength() - 4);// tolgo .exe


	// 
	iniFileName+= _T(".ini");
	c_logFileName+= _T("Log.log");	

	free((void*)m_pszProfileName);
	m_pszProfileName = _tcsdup(iniFileName);
	
}


#define EASYLOG_NUM_CONF 15
static const char EASY_CONF_STRING [EASYLOG_NUM_CONF][128] = {
	"* ALL:\n",
    "FORMAT	= %level %user %host %log\n",
    "FILENAME	= logs/Csm21Log.log\n",
    "ENABLED			=	true\n",
    "TO_FILE			=	true\n",
    "TO_STANDARD_OUTPUT		=	false\n",
    "MILLISECONDS_WIDTH		=	3\n",
    "PERFORMANCE_TRACKING	=	false\n",
	"ROLL_OUT_SIZE           =  10485760\n",		// Throw log files away after 10MB
	"* INFO:\n", 
		"FORMAT	= %datetime %level %log\n",
	"* TRACE:\n",
		"ENABLED			=	true\n",
		"TO_FILE			=	false\n",
		"TO_STANDARD_OUTPUT		=	true\n"
	};




// Configure easyLog
int CCSMApp::easyLogConfig(CStringA confFileName,CStringA logFileName)
{
if (confFileName != "")
	{
	// Convert a TCHAR string to a LPCSTR
	easyloggingpp::Configurations confFromFile((LPCSTR)confFileName); 
	easyloggingpp::Loggers::reconfigureAllLoggers(confFromFile); // Re-configures all the loggers to current configuration file
	confFromFile.clear();
	}
else
	{
	char eConfString [EASYLOG_NUM_CONF][128];

	memcpy((void *)eConfString,EASY_CONF_STRING,sizeof(EASY_CONF_STRING));
	if (logFileName != "")
		{
		CStringA str2;
		str2 = "FILENAME	= ";
		str2 += logFileName;
		str2 += "\n";
		strcpy(eConfString[2],str2.GetString()); 
		}

	// create single string from array
	std::string		stringConf;
	for (int i=0;i<EASYLOG_NUM_CONF;i++)
		// stringConf += EASY_CONF_STRING[i];
		stringConf += eConfString[i];
	// now parse conf
	easyloggingpp::Configurations confFromString;
    confFromString.setToDefault();
	bool res = confFromString.parseFromText(stringConf);
	// apply conf
	easyloggingpp::Loggers::reconfigureAllLoggers(confFromString);
	// Clears everything because configurations uses heap so we want to retain it.
	// otherwise it is retained by internal memory management at the end of program
	// execution
	confFromString.clear();
	LERROR_IF(!res) << "Failed to load EasyLog config from string";
	}
	return 0;
}
