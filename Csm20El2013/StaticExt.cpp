// StaticExt.cpp : implementation file
//

#include "stdafx.h"
#include "StaticExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStaticExt

CStaticExt::CStaticExt()
{
}

CStaticExt::~CStaticExt()
{
this->c_stringVector.clear();
}


BEGIN_MESSAGE_MAP(CStaticExt, CStatic)
	//{{AFX_MSG_MAP(CStaticExt)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStaticExt message handlers

int CStaticExt::addString(CString s,CString fontName, int fontSize, 
						  COLORREF color,COLORREF backColor)
{
ColorFontChars cfc;

cfc.fontName = fontName;
cfc.fontColor = color;
cfc.backColor = backColor;
cfc.fontSize = fontSize;
cfc.fontSubString = s;

this->c_stringVector.push_back(cfc);
	
return 1;	
}

void CStaticExt::OnPaint() 
{
CPaintDC dc(this); // device context for painting
// TODO: Add your message handler code here


CSize sz;
sz.cx = 0;
sz.cy = 0;
for (int i=0;i<c_stringVector.size();i++)
	{
	CFont font,*oldFont;
	font.CreatePointFont(c_stringVector[i].fontSize,c_stringVector[i].fontName,&dc);
	oldFont = dc.SelectObject(&font);
	sz += dc.GetOutputTextExtent((LPCSTR)c_stringVector[i].fontSubString);
	dc.SelectObject(oldFont);
	font.DeleteObject();
	}


sz.cx += sz.cx/10;
sz.cy += sz.cy/10;
CRect oldRect;
GetWindowRect((LPRECT)oldRect);
BOOL modified = FALSE;
if(sz.cx < oldRect.Size().cx)
	{
	modified = TRUE;
	sz.cx = oldRect.Size().cx;
	}
if(sz.cy < oldRect.Size().cy)
	{
	modified = TRUE;
	sz.cy = oldRect.Size().cy;
	}

// Resize grafico
if (modified)
	{
	SetWindowPos(NULL,0,0,sz.cx,sz.cy,SWP_NOMOVE|SWP_NOZORDER);
	}

CSize curSz;
CPoint p;
p.x = 0;
p.y = 0;
for (int i=0;i<c_stringVector.size();i++)
	{
	CFont font,*oldFont;
	font.CreatePointFont(c_stringVector[i].fontSize,c_stringVector[i].fontName,&dc);
	oldFont = dc.SelectObject(&font);
	curSz = dc.GetOutputTextExtent((LPCSTR)c_stringVector[i].fontSubString);
	dc.SetTextColor(c_stringVector[i].fontColor);
	dc.SetBkColor(c_stringVector[i].backColor);
	dc.TextOut(p.x,p.y,c_stringVector[i].fontSubString);
	p.x += curSz.cx;
	dc.SelectObject(oldFont);
	font.DeleteObject();
	}


	

	
// Do not call CStatic::OnPaint() for painting messages
}
