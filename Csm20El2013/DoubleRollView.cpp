// DoubleRollView.cpp : implementation file
//

#include "stdafx.h"

#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"
// #include "ctrlext.h"
// #include "ListCtrlUp.h"

#include "DoubleRollView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDoubleRollView

IMPLEMENT_DYNCREATE(CDoubleRollView, CView)

CDoubleRollView::CDoubleRollView()
{


vType = ROLLVIEW;

c_timerId = 0;
//
layoutTop.setViewType(ROLLVIEW);
layoutTop.setMode(NOLABELV);
//
layoutBottom.setViewType(ROLLVIEW);
layoutBottom.setMode(NOLABELV);

// Init layoutTop Fix Attributes
CProfile profile;
CString s;


layoutTop.fCaption.size = profile.getProfileInt("RollmaplayoutTop","CaptionSize",110);
layoutTop.fLabel.size = profile.getProfileInt("RollmaplayoutTop","LabelSize",100);
layoutTop.fNormal.size = profile.getProfileInt("RollmaplayoutTop","NormalSize",70);

layoutBottom.fCaption.size = profile.getProfileInt("RollmaplayoutBottom","CaptionSize",110);
layoutBottom.fLabel.size = profile.getProfileInt("RollmaplayoutBottom","LabelSize",100);
layoutBottom.fNormal.size = profile.getProfileInt("RollmaplayoutBottom","NormalSize",70);

s = profile.getProfileString("RollmaplayoutTop","NewLine","8");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutTop.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString("RollmaplayoutTop","LabelOrder","0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutTop.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

s = profile.getProfileString("RollmaplayoutTop","PixelSepLabel",
							 "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layoutTop.c_pxSepLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

layoutTop.c_pxSepLabelEdit = profile.getProfileInt("RollmaplayoutTop","PixelSepLabelEdit",10);

// Bottom
s = profile.getProfileString("RollmaplayoutBottom","NewLine","8");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutBottom.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString("RollmaplayoutBottom","LabelOrder","0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutBottom.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

s = profile.getProfileString("RollmaplayoutBottom","PixelSepLabel",
							 "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layoutBottom.c_pxSepLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

layoutBottom.c_pxSepLabelEdit = profile.getProfileInt("RollmaplayoutBottom","PixelSepLabelEdit",10);


//s = profile.getProfileString("RollmaplayoutTop","SCaption",
//							 "ROLLING MAP");

s.LoadString(CSM_GRAPHVIEW_ROLLCAPTION);
layoutTop.setCaption(s);

layoutBottom.setCaption(s);

//for (int i=0;i<MAX_NUMLABEL;i++)
//	{
//	CString sIndex;
//	sIndex.Format ("SLabel%d",i);
//	s = profile.getProfileString("RollmaplayoutTop",sIndex,sIndex);
//	if (s != sIndex)
//		layoutTop.c_sLabel[i] = s;
//	}



s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL0);
layoutTop.c_sLabel[0] = s;
layoutBottom.c_sLabel[0] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL1);
layoutTop.c_sLabel[1] = s;
layoutBottom.c_sLabel[1] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL2);
layoutTop.c_sLabel[2] = s;
layoutBottom.c_sLabel[2] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL3);
layoutTop.c_sLabel[3] = s;
layoutBottom.c_sLabel[3] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL4);
layoutTop.c_sLabel[4] = s;
layoutBottom.c_sLabel[4] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL5);
layoutTop.c_sLabel[5] = s;
layoutBottom.c_sLabel[5] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL6);
layoutTop.c_sLabel[6] = s;
layoutBottom.c_sLabel[6] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL7);
layoutTop.c_sLabel[7] = s;
layoutBottom.c_sLabel[7] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL8);
layoutTop.c_sLabel[8] = s;
layoutBottom.c_sLabel[8] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL9);
layoutTop.c_sLabel[9] = s;
layoutBottom.c_sLabel[9] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL10);
layoutTop.c_sLabel[10] = s;
layoutBottom.c_sLabel[10] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL11);
layoutTop.c_sLabel[11] = s;
layoutBottom.c_sLabel[11] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL12);
layoutTop.c_sLabel[12] = s;
layoutBottom.c_sLabel[12] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL13);
layoutTop.c_sLabel[13] = s;
layoutBottom.c_sLabel[13] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL14);
layoutTop.c_sLabel[14] = s;
layoutBottom.c_sLabel[14] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL15);
layoutTop.c_sLabel[15] = s;
layoutBottom.c_sLabel[15] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL16);
layoutTop.c_sLabel[16] = s;
layoutBottom.c_sLabel[16] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL17);
layoutTop.c_sLabel[17] = s;
layoutBottom.c_sLabel[17] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL18);
layoutTop.c_sLabel[18] = s;
layoutBottom.c_sLabel[18] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL19);
layoutTop.c_sLabel[19] = s;
layoutBottom.c_sLabel[19] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL20);
layoutTop.c_sLabel[20] = s;
layoutBottom.c_sLabel[20] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL21);
layoutTop.c_sLabel[21] = s;
layoutBottom.c_sLabel[21] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL22);
layoutTop.c_sLabel[22] = s;
layoutBottom.c_sLabel[22] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL23);
layoutTop.c_sLabel[23] = s;
layoutBottom.c_sLabel[23] = s;

for (int i=0;(i<=4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	if (i == 4)
		s = "Big";
	layoutTop.c_cLabel[i] = s;
	layoutTop.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layoutTop.c_cLabel[i].setTipoIcon(IPALLINO);
	//
	layoutBottom.c_cLabel[i] = s;
	layoutBottom.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layoutBottom.c_cLabel[i].setTipoIcon(IPALLINO);
	}

// CalcDrawRect
layoutTop.c_viewTopPerc = profile.getProfileInt("RollmaplayoutTop","ViewTop%",10);
layoutTop.c_viewBottomPerc = profile.getProfileInt("RollmaplayoutTop","ViewBottom%",25);
layoutTop.c_viewLeftPerc = profile.getProfileInt("RollmaplayoutTop","ViewLeft%",8);
layoutTop.c_viewRightPerc = profile.getProfileInt("RollmaplayoutTop","ViewRight%",5);

layoutBottom.c_viewTopPerc = profile.getProfileInt("RollmaplayoutBottom","ViewTop%",10);
layoutBottom.c_viewBottomPerc = profile.getProfileInt("RollmaplayoutBottom","ViewBottom%",25);
layoutBottom.c_viewLeftPerc = profile.getProfileInt("RollmaplayoutBottom","ViewLeft%",8);
layoutBottom.c_viewRightPerc = profile.getProfileInt("RollmaplayoutBottom","ViewRight%",5);
// dimensione pallini
c_pxBRoll = profile.getProfileInt("RollmaplayoutTop","RollSize",12);



// Don't update internal background
layoutTop.setRgbDrawBackColor(RGB(0,0,0));
layoutBottom.setRgbDrawBackColor(RGB(0,0,0));

}

CDoubleRollView::~CDoubleRollView()
{
}


BEGIN_MESSAGE_MAP(CDoubleRollView, CView)
	//{{AFX_MSG_MAP(CDoubleRollView)
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDoubleRollView drawing

DOC_CLASS* CDoubleRollView::getTopDocument()
{
DOC_CLASS* pDocTop = (DOC_CLASS* )GetDocument();
if (!pDocTop->isTopSide())
	{
	CDocTemplate* pDocTemplate = pDocTop->GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	while(!pDocTop->isTopSide())
		pDocTop = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	}
return (pDocTop);
}

DOC_CLASS* CDoubleRollView::getBottomDocument()
{
DOC_CLASS* pDocBottom = (DOC_CLASS* )GetDocument();
if (pDocBottom->isTopSide())
	{
	CDocTemplate* pDocTemplate = pDocBottom->GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	while(pDocBottom->isTopSide())
		pDocBottom = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	}
return (pDocBottom);
}

CRect CDoubleRollView::getRectTop(void)
{
CRect rcBoundsTop;

GetClientRect (rcBoundsTop);
CPoint pt;

pt = rcBoundsTop.BottomRight();
pt.y -= rcBoundsTop.Size().cy /2;
rcBoundsTop.BottomRight() = pt;

return (rcBoundsTop);
}

CRect CDoubleRollView::getDrawRectTop(void)
{
CRect rcBoundsTop,rect;

rcBoundsTop = getRectTop();

layoutTop.CalcDrawRect(rcBoundsTop,rect);
return (rect);
}

CRect CDoubleRollView::getRectBottom(void)
{
CRect rcBoundsBottom,rect;

GetClientRect (rcBoundsBottom);


CPoint pt;
pt = rcBoundsBottom.TopLeft();
pt.y += rcBoundsBottom.Size().cy /2;
rcBoundsBottom.TopLeft() = pt;

return (rcBoundsBottom);
}

CRect CDoubleRollView::getDrawRectBottom(void)
{
CRect rcBoundsBottom,rect;

rcBoundsBottom = getRectBottom();

layoutBottom.CalcDrawRect(rcBoundsBottom,rect);
return (rect);
}

void CDoubleRollView::OnDraw(CDC* pDC)
{
CDC pdcMem;
CBitmap bitMapMem,*bitMapOld;
// rc regione totale disegnocx

CRect rectB;
CRect rc;
GetClientRect (rc);
// Set base Rect

SIZE sz;
sz.cx = rc.right - rc.left;
sz.cy = rc.bottom - rc.top;

//AtlPixelToHiMetric (&sz, &m_sizeExtent);
// store natural extent
//m_sizeNatural = m_sizeExtent;

// Create mem DC
pdcMem.CreateCompatibleDC(pDC);

// Create mem bitmap
bitMapMem.CreateCompatibleBitmap(pDC,rc.right-rc.left,
								   rc.bottom-rc.top);
// Select  mem bitmap
bitMapOld = (CBitmap*)  pdcMem.SelectObject(&bitMapMem);

//draw (di.hdcDraw,rc);
CRect rcTop,rcBottom;
rcTop = getRectTop();
layoutTop.OnEraseBkgnd(&pdcMem,rcTop);

rcBottom = getRectBottom();
layoutBottom.OnEraseBkgnd(&pdcMem,rcBottom);
// Draw on bitmap 
onDraw (&pdcMem);

//onDraw (pDC);

// replay view with bitmap
pDC->BitBlt(rc.left,rc.top,rc.right-rc.left,
			rc.bottom-rc.top,&pdcMem,0,0,SRCCOPY);

// dispose object
pdcMem.SelectObject(bitMapOld);
bitMapMem.DeleteObject();
pdcMem.DeleteDC();

}



void CDoubleRollView::onDraw(CDC* pDC)
{
// TODO: add draw code here
// TODO: add draw code here
// Call Base Class Draw
CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);

DOC_CLASS* pDocTop = getTopDocument();
DOC_CLASS* pDocBottom = getBottomDocument();

// top space
CRect rcBoundsTop;
rcBoundsTop = getRectTop();

CRect rcBoundsBottom;
rcBoundsBottom = getRectBottom();

// Set base Rect
layoutTop.setDrawRect(rcBoundsTop);
layoutBottom.setDrawRect(rcBoundsBottom);

onDraw(pDC,pDocBottom,&layoutBottom,rcBoundsBottom);

onDraw(pDC,pDocTop,&layoutTop,rcBoundsTop);

}

void CDoubleRollView::onDraw(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,CRect rcBounds)
{
// Call Base Class Draw

//-----------------------------------------
// Init vLabel 
// pLayout->setVFori	(pDoc->getFori());
pLayout->c_vLabel [0] = pDoc->GetVPosition();
pLayout->c_vLabel [1] = pDoc->GetTrendLVPositionDal();
// densita D Top
	{
	CString str;
	pLayout->setMaxY('D',pDoc->GetTrendDScalaY());
	pLayout->setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		pLayout->c_vLabel[2].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[2].setBColor(RGB(0,255,0));
	}
//

// densita C Top
	{
	CString str;
	pLayout->setMaxY('C',pDoc->GetTrendDScalaY());
	pLayout->setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		pLayout->c_vLabel[3].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[3].setBColor(RGB(0,255,0));
	}
// densita B Top
	{
	CString str;
	pLayout->setMaxY('B',pDoc->GetTrendDScalaY());
	pLayout->setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		pLayout->c_vLabel[4].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[4].setBColor(RGB(0,255,0));
	}
//

// densita A Top
	{
	CString str;
	pLayout->setMaxY('A',pDoc->GetTrendDScalaY());
	pLayout->setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		pLayout->c_vLabel[5].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[5].setBColor(RGB(0,255,0));
	}

//-----
pLayout->c_vLabel [6] = pDoc->getRotolo();
pLayout->c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
pLayout->c_vLabel [8] = d.Format( "%d-%m-%Y" );
// Visualizzazione alzata
int metriAlzata;

if(pDoc->c_rotoloMapping.c_lastElemento < 0 )
	return;	

int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
pLayout->c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
pLayout->c_vLabel [10] = s;

s = pDoc->getCurAlzString();
pLayout->c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
pLayout->c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
pLayout->c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
pLayout->c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
pLayout->c_vLabel [15] = s;
// Total Holes A+B
s.Format("%6.0lf",va+vb+vc+vd);
pLayout->c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
pLayout->c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
pLayout->c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
pLayout->c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
pLayout->c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
pLayout->c_vLabel [21] = s;

// numero totale strip
//s.Format("%d",pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].size());
//pLayout->c_vLabel [16] = s;


//--------------------

int numClassi = pDoc->c_difCoil.getNumClassi();
// aggiungo anche classe Big
pLayout->setNumClassi(numClassi+1);
pLayout->setNumLabelClassi(numClassi+1);
pLayout->setNumSchede(pDoc->c_difCoil.getNumSchede());
pLayout->setNumCpu(pDoc->c_numCpu);

pLayout->draw(pDC);
CRect rectB;
pLayout->CalcDrawRect(rcBounds,rectB); 

// Disegno Posizione bobine
if(pDoc->c_viewStripBorder)
	// Disegno Posizione bobine doppia riga
	drawPosBobine2(pDC,pDoc,pLayout,rectB);
else
	// Disegno Posizione bobine singola riga
	drawPosBobine1(pDC,pDoc,pLayout,rectB);

if(pDoc->c_viewStripBorderPosition)
	textPosBobine2(pDC,pDoc,pLayout,rectB);
		
// Disegno Posizione diaframmi
if (pDoc->c_useDiaframmi)
	drawPosDiaframma(pDC,pDoc,pLayout,rectB);


	{
	//pLayout->OnEraseDrawBkgnd(pDC);
	pDoc->c_targetBoard.setDrawAttrib((int)pLayout->c_sizeFBanda,
							(int)pLayout->c_sizeBanda,
							c_pxBRoll,pLayout->c_bandePerCol);
	// Clipping eliminato xche` altrimenti impossibile 
	// disegnare in sequenza le due rollingMap
//	CRect clipRect(rectB);
//	clipRect.TopLeft().x = rcBounds.TopLeft().x;
//	clipRect.BottomRight().x = rcBounds.BottomRight().x;
//	pDC->IntersectClipRect (clipRect);
	pDoc->c_targetBoard.draw(rectB,pDC);

	}
// c_listCtrl.MoveWindow(rectB);

// Sincronizzo contenuto 
//	c_listCtrl.UpdateWindow();

}

/////////////////////////////////////////////////////////////////////////////
// CDoubleRollView diagnostics

#ifdef _DEBUG
void CDoubleRollView::AssertValid() const
{
	CView::AssertValid();
}

void CDoubleRollView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDoubleRollView message handlers

void CDoubleRollView::OnInitialUpdate() 
{
CView::OnInitialUpdate();

CRect gRect,dRect;
GetClientRect(gRect);


	
// TODO: Add your specialized code here and/or call the base class
//layoutTop.CalcDrawRect (gRect,dRect);

// if (!c_listCtrl.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT,dRect,this,0))
//	AfxMessageBox ("Error Create listCtrl");

// c_listCtrl.SetTextColor (RGB(0,0,0));

// c_listCtrl.image.Create(IDB_BITMAP_DIF1,16,1,RGB(255,255,255));	
// c_listCtrl.SetImageList(&c_listCtrl.image,TVSIL_NORMAL);

/*
CString sH ("POSITION");
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	CString s;
	s.Format (",%2d",i+1);
	sH += s;
	}
*/


c_timerId = SetTimer(11,3000,NULL);
DOC_CLASS* pDocTop = getTopDocument();
DOC_CLASS* pDocBottom = getBottomDocument();

CRect rectB;
rectB = getDrawRectTop();
int nr = rectB.Size().cy / c_pxBRoll;
fillCtrl(nr,pDocTop);

rectB = getDrawRectBottom();
nr = rectB.Size().cy / c_pxBRoll;
fillCtrl(nr,pDocBottom);

//CRect rcBounds,rectB;
//GetClientRect (rcBounds);
//layoutTop.CalcDrawRect(rcBounds,rectB);

}


// Torna numero di righe aggiunte
// Inserisce tutte le volte al piu` N righe dove N sono  quelle visualizzabili
// Default inserisce solo al piu` nMaxRow
int CDoubleRollView::fillCtrl(int nmaxRow,DOC_CLASS* pDoc)
{ 

int valret = 0;
//DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

int toIndex = pDoc->c_difCoil.GetSize();
int fromIndex = max (toIndex - nmaxRow,0);

pDoc->c_targetBoard.setEmpty();
for (int index=fromIndex;index <toIndex;index ++)
	{
	BOOL found = FALSE;
	// cerco schede
	int mSize = pDoc->c_difCoil.ElementAt(index).GetSize();
	for (int scIndex=0;scIndex<mSize;scIndex ++)
		{
		// add BigHole
		if (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).BigHoleAt() > 0)
			{
			// found 
			CTarget t;
			t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
			t.setSize (CSize(c_pxBRoll,c_pxBRoll));
			//t.setColor (RGB(255,255,255));
			t.setColor (CSM20GetClassColor(4));
			pDoc->c_targetBoard.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t);
			found = TRUE;
			}
		int clSize = pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).GetSize();
		// for (int clIndex=0;clIndex<clSize;clIndex ++)
		for (int clIndex=(clSize-1);clIndex>=0;clIndex --)
			{// invertita priorita` di visualizzazione fori D + prioritari degli A
			if (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).ElementAt(clIndex) > 0)
				{
				// found 
				CTarget t;
				t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
				t.setSize (CSize(c_pxBRoll,c_pxBRoll));
				t.setColor (CSM20GetClassColor(clIndex));
				pDoc->c_targetBoard.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t);
				found = TRUE;
				}
			}
		}
	if (found)
		{
		int p1,p2;
		p1 = pDoc->c_difCoil.ElementAt(index).posizione*1000;
		p2 = pDoc->c_targetBoard.getLastPos();
		if (p1 > p2)
			valret ++;
		}
	}
if (pDoc->c_targetBoard.GetSize() > 0)
	pDoc->c_targetBoard.setLastPos(pDoc->c_targetBoard.ElementAt(pDoc->c_targetBoard.GetUpperBound()).getPos());
else
	pDoc->c_targetBoard.setLastPos(0);
return (valret);
}


BOOL CDoubleRollView::OnEraseBkgnd(CDC* pDC) 
{
// TODO: Add your message handler code here and/or call default
return 1;
CRect r;

r = getDrawRectTop();
layoutTop.OnEraseBkgnd(pDC,r);

// semaforo repaint all 
// vedi OnUpdate
// layoutTop.setExcludeBkgr(!c_repaintAll);
// layoutTop.setExcludeBkgLV(!c_repaintAll);

r = getDrawRectBottom();
layoutBottom.OnEraseBkgnd(pDC,r);

return TRUE;
}



BOOL CDoubleRollView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if (!CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);


return (layoutTop.Create(this)&&layoutBottom.Create(this));

}



void CDoubleRollView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
// TODO: Add your specialized code here and/or call the base class
//return;

DOC_CLASS* pDocTop = getTopDocument();
DOC_CLASS* pDocBottom = getBottomDocument();

if ((lHint==0) && (pHint == NULL))
	{//redraw completo
	//CRect rcBounds,rectB;
	//GetClientRect (rcBounds);
	// top
	// layoutTop.CalcDrawRect(rcBounds,rectB);
	CRect rectB;
	rectB = getDrawRectTop();
	int nr = rectB.Size().cy / c_pxBRoll;
	int newRow = fillCtrl(nr,pDocTop);
	// Bottom
	rectB = getDrawRectBottom();
	nr = rectB.Size().cy / c_pxBRoll;
	newRow = fillCtrl(nr,pDocBottom);
	CView::OnUpdate(pSender,lHint,pHint);	
	}
else
	{
	CRect rcBounds;
	rcBounds = getRectTop();
	update(lHint,pDocTop,&layoutTop,rcBounds); 
	rcBounds = getRectBottom();
	update(lHint,pDocBottom,&layoutBottom,rcBounds); 
	}
}

void CDoubleRollView::update(LPARAM lHint,DOC_CLASS* pDoc,Layout* pLayout,CRect rcBounds) 
{

CRect rectB;
//GetClientRect (rcBounds);
pLayout->CalcDrawRect(rcBounds,rectB);

int nr = rectB.Size().cy / c_pxBRoll;
int newRow = fillCtrl(nr,pDoc);

//DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
if (newRow > 0)
	{// aggiornamento nuova riga
	pDoc->c_targetBoard.setDrawAttrib((int)pLayout->c_sizeFBanda,
							(int)pLayout->c_sizeBanda,
							c_pxBRoll,pLayout->c_bandePerCol);
	CRect clipRect(rectB);
	clipRect.TopLeft().x = rcBounds.TopLeft().x;
	CDC  *pDC;
	pDC = GetDC ();
	pDC->IntersectClipRect (clipRect);
	// Disegno Posizione bobine
	if(pDoc->c_viewStripBorder)
		// Disegno Posizione bobine doppia riga
		drawPosBobine2(pDC,pDoc,pLayout,rectB);
	else
		// Disegno Posizione bobine singola riga
		drawPosBobine1(pDC,pDoc,pLayout,rectB);

	if(pDoc->c_viewStripBorderPosition)
		textPosBobine2(pDC,pDoc,pLayout,rectB);
		
	// Disegno Posizione diaframmi
	if (pDoc->c_useDiaframmi)
		drawPosDiaframma(pDC,pDoc,pLayout,rectB);
	
	pDoc->c_targetBoard.scroll(newRow,rectB,clipRect,pDC);	
	ReleaseDC(pDC);
	}
else
	{
	if (lHint == 2)
		{ // aggiornamento stessa riga
		pDoc->c_targetBoard.setDrawAttrib((int)pLayout->c_sizeFBanda,
									(int)pLayout->c_sizeBanda,
									c_pxBRoll,pLayout->c_bandePerCol);
		CRect clipRect(rectB);
		clipRect.TopLeft().x = rcBounds.TopLeft().x;
		CDC  *pDC;
		pDC = GetDC ();
		pDC->IntersectClipRect (clipRect);
		// Disegno Posizione bobine
		if(pDoc->c_viewStripBorder)
			// Disegno Posizione bobine doppia riga
			drawPosBobine2(pDC,pDoc,pLayout,rectB);
		else
			// Disegno Posizione bobine singola riga
			drawPosBobine1(pDC,pDoc,pLayout,rectB);

		if(pDoc->c_viewStripBorderPosition)
			textPosBobine2(pDC,pDoc,pLayout,rectB);
			
		// Disegno Posizione diaframmi
		if (pDoc->c_useDiaframmi)
			drawPosDiaframma(pDC,pDoc,pLayout,rectB);

		pDoc->c_targetBoard.reDraw(pDoc->c_targetBoard.GetUpperBound(),rectB,pDC);
		ReleaseDC(pDC);
		}
	else
		int x = 99;
	}

updateLabel(pDoc,pLayout); 
}


void CDoubleRollView::updateLabel(DOC_CLASS* pDoc,Layout* pLayout)
{
// pLayout->setVFori	(pDoc->getFori());
pLayout->c_vLabel [0] = pDoc->GetVPosition();
pLayout->c_vLabel [1] = pDoc->GetTrendLVPositionDal();
// densita D Top
	{
	CString str;
	pLayout->setMaxY('D',pDoc->GetTrendDScalaY());
	pLayout->setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		pLayout->c_vLabel[2].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[2].setBColor(RGB(0,255,0));
	}
//

// densita C Top
	{
	CString str;
	pLayout->setMaxY('C',pDoc->GetTrendDScalaY());
	pLayout->setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		pLayout->c_vLabel[3].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[3].setBColor(RGB(0,255,0));
	}
// densita B Top
	{
	CString str;
	pLayout->setMaxY('B',pDoc->GetTrendDScalaY());
	pLayout->setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		pLayout->c_vLabel[4].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[4].setBColor(RGB(0,255,0));
	}
//

// densita A Top
	{
	CString str;
	pLayout->setMaxY('A',pDoc->GetTrendDScalaY());
	pLayout->setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		pLayout->c_vLabel[5].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[5].setBColor(RGB(0,255,0));
	}

//-----
pLayout->c_vLabel [6] = pDoc->getRotolo();
pLayout->c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
pLayout->c_vLabel [8] = d.Format( "%d-%m-%Y" );
// Visualizzazione alzata
int metriAlzata;

if(pDoc->c_rotoloMapping.c_lastElemento < 0 )
	return;	

int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
pLayout->c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
pLayout->c_vLabel [10] = s;

s = pDoc->getCurAlzString();
pLayout->c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
pLayout->c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
pLayout->c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
pLayout->c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
pLayout->c_vLabel [15] = s;
// Total Holes A+B
s.Format("%6.0lf",va+vb+vc+vd);
pLayout->c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
pLayout->c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
pLayout->c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
pLayout->c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
pLayout->c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
pLayout->c_vLabel [21] = s;

/*
pLayout->c_vLabel[0] = pDoc->GetVPosition();
// Visualizzazione alzata
int metriAlzata;
int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
pLayout->c_vLabel [9] = s;

//--------------------
// densita D
{
CString str;
double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
str.Format("%6.0lf",densita);
pLayout->c_vLabel [2] = str;		// densita` 
if (densita > pDoc->c_difCoil.getAllarme ('D'))
	pLayout->c_vLabel[2].setBColor(RGB(255,0,0));
else
	pLayout->c_vLabel[2].setBColor(RGB(0,255,0));
}

// densita C
{
CString str;
double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
str.Format("%6.0lf",densita);
pLayout->c_vLabel [3] = str;		// densita` 
if (densita > pDoc->c_difCoil.getAllarme ('C'))
	pLayout->c_vLabel[3].setBColor(RGB(255,0,0));
else
	pLayout->c_vLabel[3].setBColor(RGB(0,255,0));
}

//--------------------
// densita B
{
CString str;
double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
str.Format("%6.0lf",densita);
pLayout->c_vLabel [4] = str;		// densita` 
if (densita > pDoc->c_difCoil.getAllarme ('B'))
	pLayout->c_vLabel[4].setBColor(RGB(255,0,0));
else
	pLayout->c_vLabel[4].setBColor(RGB(0,255,0));
}

// densita A
{
CString str;
double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
str.Format("%6.0lf",densita);
pLayout->c_vLabel [5] = str;		// densita` 
if (densita > pDoc->c_difCoil.getAllarme ('A'))
	pLayout->c_vLabel[5].setBColor(RGB(255,0,0));
else
	pLayout->c_vLabel[5].setBColor(RGB(0,255,0));
}

// Agosto 31
// Total Holes A
	
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
pLayout->c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
pLayout->c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
pLayout->c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
pLayout->c_vLabel [15] = s;
// Total Holes A+B+C+D
s.Format("%6.0lf",va+vb+vc+vd);
pLayout->c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
pLayout->c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
pLayout->c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
pLayout->c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
pLayout->c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
pLayout->c_vLabel [21] = s;
*/

}


void CDoubleRollView::drawPosDiaframma(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB)
{
// usa diaframmi
if (!pDoc->c_useDiaframmi)
	return;


CPen pen1,pen2,pen3,pen4,*oldPen;
	// red
	
	pen1.CreatePen(PS_SOLID,1,RGB(196,0,0));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft = rectB.left +pLayout->c_sizeFBanda+ (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	pen2.CreatePen(PS_SOLID,1,RGB(196,0,0));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosRight = rectB.left +pLayout->c_sizeFBanda + (1.  * posRight * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
	
	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	// non importa anche se seleziono la stessa pen due volte
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------


}



void CDoubleRollView::drawPosBobine2(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB)
{

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,1,RGB(196,196,196));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,1,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft = rectB.left + pLayout->c_sizeFBanda+(1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));


//	pDC->MoveTo(scPosLeft,rectB.top);
//	pDC->LineTo(scPosLeft,rectB.bottom);

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga  a dx strip
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

		// Debug vis pos strip
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		//---------------------------------
		// seconda riga a sinistra strip
		pDC->SelectObject(&pen1);	
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		scPosLeft = rectB.left + pLayout->c_sizeFBanda+ (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------

}



// visualizzazione posizione bobine per utente
void CDoubleRollView::drawPosBobine1(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB)
{

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx

	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	pen0.CreatePen(PS_SOLID,2,RGB(196,196,196));
	// red
	pen1.CreatePen(PS_SOLID,2,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +pLayout->c_sizeFBanda 
	int scPosLeft = rectB.left + pLayout->c_sizeFBanda+(1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

//
// disegno solo inizio strip 
// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga sinistra
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		
		// check distanza con fine strip precedente
		if (i > 0)
			{
			posLeft -= (fabs(posLeft - endPosLastStrip)/2);
			}

		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
					+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +pLayout->c_sizeFBanda 
		scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		if (i == (pDoc->c_rotoloMapping[lastElem].size()-1))
			{
			// seconda riga destra
			pDC->SelectObject(&pen1);	

			posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
			// 15-09-2004 corretto centrato +pLayout->c_sizeFBanda 
			scPosLeft = rectB.left + pLayout->c_sizeFBanda+ (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
			
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
}

void CDoubleRollView::textPosBobine2(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB)
{

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);
	
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft = rectB.left + pLayout->c_sizeFBanda+(1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga  a dx strip
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

		// riferimenti numerici
		CString sl;
		pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
	
		//---------------------------------
		// seconda riga a sinistra strip
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		scPosLeft = rectB.left + pLayout->c_sizeFBanda+ (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

		pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
 		}
	pDC->SelectObject(oldFont); 
	font.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------

}

void CDoubleRollView::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default

if (nIDEvent == c_timerId)
	{
	// redraw all area
	Invalidate(FALSE);   

    // Update Window to cause View to redraw.
	UpdateWindow();
	}

CView::OnTimer(nIDEvent);
}

BOOL CDoubleRollView::DestroyWindow() 
{
// TODO: Add your specialized code here and/or call the base class

if (c_timerId != 0)
	{
	KillTimer (c_timerId);
	c_timerId = 0;
	}
	
return CView::DestroyWindow();
}













