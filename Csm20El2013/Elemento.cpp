// Elemento.cpp: implementation of the CElemento class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include "GraphDoc.h"

#include "CFileBpe.h"
#include "Elemento.h"
#include "DAlzate.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CElemento::CElemento()
{
c_firstRising = 0;
}

CElemento::~CElemento()
{
reset();
}

void CElemento::reset()
{
c_alzate.clear();
clear();
}

void CElemento::modifyCodeLastAlzata(int tipo)
{
c_alzate.back().c_tipo = tipo;

}

void CElemento::appendAlzata(double yPos, int tipo)
{
CAlzata alzata;
alzata.c_yPos = yPos;
alzata.c_tipo = tipo;
c_alzate.push_back(alzata);

}

void CElemento::removeLastAlzata()
{
// era > 0 ma vedi baco csm20D2012 
// almeno uno rimane sempre
if (c_alzate.size() > 1)
	c_alzate.pop_back();

}

// torna numero di alzate 
// esclusi gli scarti
// conta fino a upTo 
// se upTo < 0 conta tutti
int CElemento::getNumberOfAlz(int upTo)
{
int cnt=0;
if (upTo < 0)
	upTo = c_alzate.size();
for (int i=0;i<upTo;i++)
	{
	if (c_alzate[i].c_tipo != ALZATA_SCARTO)
		cnt ++;
	}

return cnt;
}


// torna numero di alzata attuale 
// esclusi gli scarti
// conta fino a upTo 
// se upTo < 0 conta tutti
int CElemento::getCurAlzNumber(int upTo)
{
int cnt=0;
if (upTo < 0)
	upTo = c_alzate.size();
for (int i=0;i<upTo;i++)
	{
	if (c_alzate[i].c_tipo != ALZATA_SCARTO)
		cnt ++;
	}
cnt += c_firstRising;

return cnt;
}

// torna indice associato all'alzata attuale 
// indice include gli scarti
// conta fino a upTo senza scarti 
// se upTo < 0 conta tutti
int CElemento::getCurAlzIndex(int upTo)
{
int cnt=-1;
if (upTo < 0)
	upTo = c_alzate.size();
for (int i=0;i<c_alzate.size();i++)
	{
	if (c_alzate[i].c_tipo != ALZATA_SCARTO)
		cnt ++;
	if (upTo == cnt)
		return i;
	}
return cnt;
}

// sostituisce le bobine impostate per qs elemento con quelle 
// presenti in source
// serve per modificare runtime le impostazioni dei finger	
void CElemento::assignBobine(CElemento& source)
{

// swap elements of source with elemnts of this
this->swap(source);

}


//---------------------------------

CBobina::CBobina()
{
c_larghezza = 0.;
c_posizione = -1;
}

CBobina::~CBobina()
{
}

CRotolo::CRotolo()
{
// Eliminato 02-05-08 
//c_reverseBobinePosition = FALSE;
reset();
}

CRotolo::~CRotolo()
{
reset();
}

int CRotolo::getGlobalRisingId(int element,int rising)
{
if (element == -1)
	element = c_lastElemento;

int alz=0;
for (int i=0;i<=element;i++)
	{
	if (i == element)
		alz += (*this)[i].getCurAlzNumber(rising);
	else
		alz += (*this)[i].getCurAlzNumber(-1);
	}
// tolgo alzate di fine elemento  
return (alz-(element+1));
}



void CRotolo::reset()
{
clear();
c_lastElemento = -1;
}


BOOL CRotolo::save(CFileBpe *cf)
{
// save vector size
int sz = size();
cf->Write((void *)&sz,sizeof(sz));

for (int i=0;i<size();i++)
	{
	this->at(i).save(cf);
	}


return TRUE;
}

BOOL CRotolo::load(CFileBpe *cf,long ver)
{
reset();

int sz;
cf->Read((void *)&sz,sizeof(sz));

for (int i=0;i<sz;i++)
	{
	CElemento elemento;
	if (elemento.load(cf,ver))
		push_back(elemento);
	else
		return FALSE;
	}
// ricavo lastElem da indice vettore
c_lastElemento = sz-1;
return TRUE;
}

BOOL CElemento::save(CFileBpe *cf)
{
// save vector size
int sz = size();
cf->Write((void *)&sz,sizeof(sz));
for (int i=0;i<size();i++)
	{
	this->at(i).save(cf);
	}

// Alzate
sz = c_alzate.size();
cf->Write((void *)&sz,sizeof(sz));
for (int i=0;i<c_alzate.size();i++)
	{
	c_alzate.at(i).save(cf);
	}

cf->Write((void *)&c_firstRising,sizeof(c_firstRising));

return TRUE;
}

BOOL CElemento::load(CFileBpe *cf,long ver)
{
// load vector size
int sz;
cf->Read((void *)&sz,sizeof(sz));
for (int i=0;i<sz;i++)
	{
	CBobina bobina;
	if (bobina.load(cf,ver))
		{
		push_back(bobina);
		}
	else
		return FALSE;
	}

// Alzate
cf->Read((void *)&sz,sizeof(sz));
for (int i=0;i<sz;i++)
	{
	CAlzata alzata;
	if (alzata.load(cf,ver))
		c_alzate.push_back(alzata);
	else
		return FALSE;
	}

cf->Read((void *)&c_firstRising,sizeof(c_firstRising));

return TRUE;
}

BOOL CBobina::save(CFileBpe *cf)
{
cf->Write((void *)&c_larghezza,sizeof(c_larghezza));
cf->Write((void *)&c_posizione,sizeof(c_posizione));
cf->Write((void *)&c_xPos,sizeof(c_xPos));

return TRUE;
}

BOOL CBobina::load(CFileBpe *cf, long ver)
{
cf->Read((void *)&c_larghezza,sizeof(c_larghezza));
cf->Read((void *)&c_posizione,sizeof(c_posizione));
cf->Read((void *)&c_xPos,sizeof(c_xPos));


return TRUE;
}


CAlzata::CAlzata (void)
{
c_yPos = 0.;
c_tipo = ALZATA_NUOVA;
}

BOOL CAlzata::save(CFileBpe *cf)
{
cf->Write((void *)&c_tipo,sizeof(c_tipo));
cf->Write((void *)&c_yPos,sizeof(c_yPos));
return TRUE;
}

BOOL CAlzata::load(CFileBpe *cf,long ver)
{
cf->Read((void *)&c_tipo,sizeof(c_tipo));
cf->Read((void *)&c_yPos,sizeof(c_yPos));
return TRUE;
}



