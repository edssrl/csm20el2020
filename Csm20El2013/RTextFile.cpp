//------------------------------------------------------
//
//
//					RTextFile
//
// Legge ed interpreta File Testo formattati per linee
//
//------------------------------------------------------

#include "stdafx.h"
#include <afxtempl.h>


#include "RTextFile.h"



//------------------------------------------------------
//
//
//					open CFile 
//
// 
//
//------------------------------------------------------

BOOL TextFile::open (CString &name)
{
CFileException error;
BOOL retCode;

if (opened)
	return(FALSE);
nome = name;

retCode = file.Open( (LPCTSTR)nome,CFile::modeRead,&error);

if (retCode == FALSE)
	return(retCode);

opened = TRUE;

// Determino Numero campi
CString line;
readLine (line);


return TRUE;
}


//------------------------------------------------------
//
//
//					close CFile 
//
// 
//
//------------------------------------------------------

BOOL TextFile::close (void )
{

if (!opened)
	return (FALSE);

file.Close( );

opened = FALSE;  
return TRUE;
}

//------------------------------------------------------
//
//
//					read line 
//
// Legge intera linea file
//
//------------------------------------------------------

void TextFile::readLine (CString &line)
{
ASSERT (opened == TRUE);


// Clear Line
line.Empty();

char ch = 0;
int nBytes = 0;

while (ch != '\n')
	{
	nBytes = file.Read(&ch,1);
	if (nBytes == 0)
		break;
	line += ch;
	}

// calcolo numero di campi X qs riga
calcNumFields((LPCSTR) line);

}
//------------------------------------------------------
//
//
//					readField 
//
// Legge Campo numero, in linea
//
//------------------------------------------------------


BOOL TextFile::fieldNumFromLine (CString &line,int num,CString &field)
{
if (num > numField)
	return FALSE;

CString localLine;
localLine = line;

for (int i=0;i<num;i++)
	{
	field = localLine.SpanExcluding( (LPCTSTR)separator);
	localLine = localLine.Right(localLine.GetLength() - (field.GetLength()+1));
	}
 return(TRUE);
 }



//------------------------------------------------------
//
//
//					readField 
//
// Legge Campo numero, in riga identificata da key
// 3 Valori di ritorno CString, int, double
//------------------------------------------------------


BOOL TextFile::readField (CString &key,int num,CString &value,LPCSTR keyExt)
{
ASSERT (opened == TRUE);
CString line;
CString fieldValue;
CString keyExtStr;
// rewind file	CFile

file.SeekToBegin();
readLine (line);

while (line.GetLength()>0)
	{
	if (fieldNumFromLine(line,1,fieldValue))
		{
		if ((c_extKeyPosition > 0)&&
			(keyExt != NULL))
			{
			if(fieldNumFromLine(line,c_extKeyPosition,keyExtStr))
				{
				if ((fieldValue == key)&&
					(keyExtStr == keyExt))
					{ // Found
					return (fieldNumFromLine(line,num,value));
					}
				}
			}
		else
			{
			if (fieldValue == key)
				{ // Found
				return (fieldNumFromLine(line,num,value));
				}
			}
		}
	readLine (line);
	}
return FALSE;
}

// Versione int
BOOL TextFile::readField (CString &key,int num,int &value,LPCSTR keyExt)
{
CString fieldValue;
int val;

if (readField (key,num,fieldValue,keyExt))
	{// Found
	sscanf ((LPCTSTR)fieldValue,"%d",&val);
	value = val;
	return(TRUE);
	}
return FALSE;
}

// Versione double
BOOL TextFile::readField (CString &key,int num,double &value,LPCSTR keyExt)
{
CString fieldValue;
double val;

if (readField (key,num,fieldValue,keyExt))
	{// Found
	sscanf ((LPCTSTR)fieldValue,"%lf",&val);
	value = val;
	return(TRUE);
	}
return FALSE;
}

void TextFile::setExtKeyPosition(int pos)
{
if (pos <= numField)
	c_extKeyPosition = pos;
}

int TextFile::calcNumFields(LPCSTR sLine)
{
CString field;
CString line;

line = sLine;

numField = 0;
while (line.GetLength() > 0)
	{
	field = line.SpanExcluding( (LPCTSTR)separator);
	line = line.Right(line.GetLength() - (field.GetLength()+1));
	while ((line.Left(1)=="\n")||
		(line.Left(1)=="\r"))
		line = line.Right(line.GetLength() - 1);
	numField ++;
	}
return (numField);

}
