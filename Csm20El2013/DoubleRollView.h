#if !defined(AFX_DOUBLEROLLVIEW_H__60A66AFF_EB96_4547_A24C_D191CD55799A__INCLUDED_)
#define AFX_DOUBLEROLLVIEW_H__60A66AFF_EB96_4547_A24C_D191CD55799A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DoubleRollView.h : header file
//


#include "Target.h"

/////////////////////////////////////////////////////////////////////////////
// CDoubleRollView view 

class CDoubleRollView : public CView
{
protected:
	CDoubleRollView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CDoubleRollView)
ViewType vType;

// Attributes
public:
Layout  layoutTop;
Layout  layoutBottom;

// CListCtrlUp	c_listCtrl;
// CTargetBoard c_targetBoard;
// Numero di righe nuove da aggiornare su video
// settato da fillCtrl
// azzerato da draw
//int c_newRow;
//BOOL c_repaintAll;
//BOOL c_repaintLock;
// Dimensione pallini
int c_pxBRoll;
int c_timerId;
// Operations
public:
	DOC_CLASS* getTopDocument();
	DOC_CLASS* getBottomDocument();
	void update(LPARAM lHint,DOC_CLASS* pDoc,Layout* pLayout,CRect rcBounds);

	void updateLabel(DOC_CLASS* pDoc,Layout* pLayout);

	void drawPosBobine2(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,CRect rBound);
	void drawPosBobine1(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,CRect rBound);
	void drawPosDiaframma(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB);
	void textPosBobine2(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB);
	void draw(void){CClientDC dc(this);OnDraw(&dc);};
	ViewType getViewType(void) {return(vType);};
	int fillCtrl(int nmaxRow,DOC_CLASS* pDoc);
	CRect getRectTop(void);
	CRect getRectBottom(void);
	CRect getDrawRectTop(void);
	CRect getDrawRectBottom(void);
	CRect getMeterRect(void);
	void onDraw(CDC* pDC);      // overridden to draw this view
	void onDraw(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,CRect rcBounds);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDoubleRollView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual BOOL DestroyWindow();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CDoubleRollView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CDoubleRollView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOUBLEROLLVIEW_H__60A66AFF_EB96_4547_A24C_D191CD55799A__INCLUDED_)
