//-------------------------------------------------------------------
//
//
//					General Include File for RpcNet Use
//
//
//-------------------------------------------------------------------

// IDENTIFICATIVI 
#define RPCID_SERVER			0
#define RPCID_NTCSM				50
// Configurabili .ini
//#define RPCID_HC16_TOP		150
//#define RPCID_HC16_BOTTOM		151
#define RPCID_HC16CSM2	151
#define RPCID_HC16CSM3	150


// COMANDI
#define REGISTER_CLIENT 1

//Sotto Comandi
#define INI_PERIOD 0
#define END_PERIOD 1

// Tipi Allarmi
// VAL_ALARM,Tipo All,Motore
#define AL_OTTURATORE 1	// Fail Otturatore 
						//campo successivo indice motore
						// 1 M1, 2 M2, 3 entrambi
#define AL_LASERKO	2	// Laser Ko
#define AL_OVERTEMP 3
#define AL_ENCODER	4

// Comandi
#define VAL_COUNTER			101
#define VAL_PERIOD			102
#define VAL_ALARM			103
#define VAL_STATO			104
#define VAL_TEST100			105
#define VAL_STOP			106
#define VAL_SPEED			109
#define VAL_OPENFILE		111
#define VAL_CLOSEFILE		112
#define VAL_LASERKO			113
#define VAL_EXTCOUNTER		114		// Contatori su piu' pacchetti
#define VAL_TESTOFFLINE		116		 // risultato del test offline

#define VAL_DIAFRAMMA		118		// Posizione diaframma 2 byte short
#define VAL_ALZATA			119			// Comando di alzata dim 1 On 1 Off 0
#define VAL_EXTDIAFRAMMA	124		// Posizione diaframma 4 byte 2 short

#define VAL_INPUT			126		// lettura input Hw
#define VAL_COUNTER_BIGHOLE	127

#define VAL_COUNTER2AB			130	// Contatori su 1 pacchetto con posiz trasv AB
#define VAL_COUNTER_BIGHOLE2	131	// Contatori su 1 pacchetto con posiz trasv
#define VAL_COUNTER2CD			132	// Contatori su 1 pacchetto con posiz trasv CD


// Val Fattore Correz dim lineari
#define VAL_FCDL			27
//#define VAL_ALPULSE		28
// Nuovo valore dal 29-11-2002
#define VAL_ALPULSE		30
#define VAL_OUTPUT			29		// impostazioni output Hw
#define VAL_START_TESTDIAF	42		// impostazioni output Hw

#define	AUTOTEST_CMD		10
#define	VAL_RISAUTOTEST		55			//	Risultato Autotest 

#define VAL_USE_FINGERHW	60		// impostazioni finger Hw on/off
#define VAL_AL_MISALIGNED 	61		// allarme teste disallineate
#define VAL_DIST_ODDEVEN 	62		// Distanza righe pari dispari

#define VAL_ALHW_OUTPUT		65		// allarme hw nuovo per EL2020


#define VAL_COMMAND		2
#define VAL_SOGLIA		3
#define VAL_TESTSER		4
#define VAL_TESTSPEED	5
#define VAL_ALARMDIF	6

// BlackBox cmdId
#define INQ_BLACKBOX		40
#define SET_CLEARBLACKBOX	41
#define VAL_RAWBLACKBOX     130
#define VAL_BLACKBOX		131



// SubComandi (VAL_COMMAND)
#define STARTCMD	1
#define STOPCMD		2
#define TSTARTCMD	3
#define TSTOPCMD	4

#define TESTOFFLINESX	11
#define TESTOFFLINEDX	12

#define IOSTARTCMD	13
#define IOSTOPCMD	14



#define OFFCMD	 0
#define ONCMD	 1


// Stati HDE

#define HDE_STATE		0
#define HDE_WAITSTART	1
#define HDE_WAITENABLE	2
#define HDE_INSPECTION	3
#define HDE_HOLD		4
#define HDE_CHANGE		5
#define HDE_SELFTEST	9
#define HDE_TEST		10
#define HDE_TESTOFFLINE 11
#define HDE_WAITDIAPH	12
#define HDE_TESTIO		13
#define HDE_WARMUP		15
