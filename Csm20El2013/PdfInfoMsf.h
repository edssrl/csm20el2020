#if !defined(AFX_PDFINFO_H__DC6E2D11_C543_45AC_80B7_11A49E618AA9__INCLUDED_)
#define AFX_PDFINFO_H__DC6E2D11_C543_45AC_80B7_11A49E618AA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PdfInfo.h : header file
// modificato versione 3.32 28-11-18
// modificato per stampante Win10 MICROSOFT_PDF_PRINTER

#include "resource.h"


/////////////////////////////////////////////////////////////////////////////
// CPdfInfo dialog

class CPdfInfo : public CDialog
{
// Construction
public:
	CPdfInfo(CWnd* pParent = NULL);   // standard constructor
	~CPdfInfo(void);

// Dialog Data
	//{{AFX_DATA(CPdfInfo)
	enum { IDD = IDD_PDF_INFO };
	CButton	m_ctrlPdfOptions;
	CButton	m_ctrlPdfLog;
	CString	m_saveFolder;
	CString	m_pdfPrinterName;
	int		m_timeout;
	//}}AFX_DATA

// synchonization object
	CCriticalSection    c_criticalSection;	

	

	BOOL	isPdfFileReady(LPCSTR fileName,LPCSTR folder);
	CString getPdfFolder(void){return m_saveFolder;};
	CString getPdfFilePath(CString fileName, CString ddFolder);


// general functions	
	BOOL	saveToProfile(void);
	BOOL	loadFromProfile(void);
	CString getPdfPrinterName(void)
	{return(m_pdfPrinterName);};
	int getTimeout(void)
	{return(1000*m_timeout);};
	void setTopZorder(void);
	

private:
		
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPdfInfo)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPdfInfo)
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowse();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CString m_pdfVer;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PDFINFO_H__DC6E2D11_C543_45AC_80B7_11A49E618AA9__INCLUDED_)
