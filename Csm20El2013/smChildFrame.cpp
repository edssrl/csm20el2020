// smChildFrame.cpp : implementation file
//

#include "stdafx.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "RollView.h"
#include "TowerView.h"
#include "MLineView.h"


#include "dyntempl.h"

#include "Csm20El2013.h"
#include "MainFrm.h"

#include "smChildFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CsmChildFrame

IMPLEMENT_DYNCREATE(CsmChildFrame, CMDIChildWnd)

CsmChildFrame::CsmChildFrame()
{
}

CsmChildFrame::~CsmChildFrame()
{
}


BEGIN_MESSAGE_MAP(CsmChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CsmChildFrame)
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CsmChildFrame message handlers
BOOL CsmChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
cs.style &= (~WS_SYSMENU);
cs.style &= (~WS_CAPTION);
cs.style &= (~WS_THICKFRAME);
 
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CsmChildFrame diagnostics

#ifdef _DEBUG
void CsmChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CsmChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CsmChildFrame message handlers


void CsmChildFrame::OnShowWindow(BOOL bShow, UINT nStatus) 
{
CMDIChildWnd::OnShowWindow(bShow, nStatus);
// TODO: Add your message handler code here

// maximize frame
MDIMaximize();
	
}

void CsmChildFrame::OnFileOpen() 
{
// TODO: Add your command handler code here
CString szFilter (_T("CSM Files (*.csm)|*.csm|All Files (*.*)|*.*||"));

char dir [512];

// Save curDir
GetCurrentDirectory(511,dir);  

// set iniDefault dir
CProfile profile;
CString sDir;
sDir = profile.getProfileString(_T("init"),_T("RootDir"),_T("."));
SetCurrentDirectory((LPCSTR) sDir);  				

CFileDialog dialog (TRUE,_T("*.csm"),
	_T("*.csm"),OFN_HIDEREADONLY,(LPCSTR) szFilter);

if (dialog.DoModal() == IDOK)
//if (false)
	{
	// Load Data 
	CFileBpe cf;
	CFileException e;
	CString title;
	title =  dialog.GetPathName();
	CString side = getSideFromFileName(title);
	// check side
	if ((side != CSM_TOP_SIDE)&&(side != CSM_BOTTOM_SIDE)&&(side != CSM_SINGLE_SIDE))
		{
		AfxGetMainWnd()->MessageBox(_T("WRONG TYPE SIDE FILE"),_T("Error"));
		return;
		}
	
	// open
	if( !cf.Open((LPCSTR) title, CFile::modeRead | CFile::typeBinary, &e ) )
		{
		AfxGetMainWnd()->MessageBox(_T("ERROR OPEN"),_T("Error"));
		return;
		}

	// create progress control
	CProgressCtrl *progress;
	CMainFrame* pFrame;
	pFrame = (CMainFrame*) GetMDIFrame( );
	progress = pFrame->getProgressCtrl();
	pFrame->progressCreate();
	pFrame->progressSetRange(0, 8);
	pFrame->progressSetStep(2);

	CLineDoc *pDoc;
	pDoc = (CLineDoc *) GetActiveDocument( );

	// Gestione single side mode
	// Carico i single side su lato congruente, clear dati altra testa
	if (side == CSM_SINGLE_SIDE)
		{
		CLineDoc tmpDoc;
		tmpDoc.load(&cf,progress);
		// restore file pointer
		cf.SeekToBegin();
		if (tmpDoc.isTopSide())
			{
			side = CSM_TOP_SIDE;
			CLineDoc *pLocDoc;
			pLocDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
			if (pLocDoc)
				pLocDoc->loadedData = FALSE;		
			}
		else
			{
			side = CSM_BOTTOM_SIDE;
			CLineDoc *pLocDoc;
			pLocDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
			if (pLocDoc)
				pLocDoc->loadedData = FALSE;		
			}
		
		}

	if (pDoc->c_side != side)
		{// switch document 
		if (pDoc->isTopSide())
			pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
		else
			pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
		// switch view
		CMainFrame* pFrame = (CMainFrame*)GetMDIFrame( );
		if (pFrame != NULL)
			pFrame->MDINext();
		}

	pDoc->load(&cf,progress);


	cf.Close();  
	// Set Title of new data
	pDoc->SetTitle ((LPCSTR)dialog.GetFileTitle());
	pFrame->progressDestroy();
	}

// replace curdir
SetCurrentDirectory((LPCSTR) dir);  
}


CString CsmChildFrame::getSideFromFileName(CString fileName)
{

CString tmp;

tmp = fileName;

int pos = fileName.ReverseFind('\\');

// Not found cntrlslash look for slash
if (pos < 0)
	pos = fileName.ReverseFind('/');

if (pos < 0)
	// not found relative pathname
	pos = 0;
// nome senza percorso
tmp = fileName.Right(fileName.GetLength()-pos-1);
// nuovo formato coil + data
//Bobin_S02160627.csm
//      x --> 13char <--     
tmp = tmp.Mid(tmp.GetLength()-13,1);


if ((tmp == CSM_TOP_SIDE)||
	(tmp == CSM_BOTTOM_SIDE)||
	(tmp == CSM_SINGLE_SIDE))
	return tmp;


// vecchio formato
// data + coil
tmp = fileName.Right(fileName.GetLength()-pos-1);
tmp = tmp.Mid(8,1);

if ((tmp == CSM_TOP_SIDE)||
	(tmp == CSM_BOTTOM_SIDE))
	return tmp;


tmp = "";

return tmp;
}




