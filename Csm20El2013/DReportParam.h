#pragma once

#include "resource.h"

// CDReportParam dialog

class CDReportParam : public CPropertyPage
{
	DECLARE_DYNAMIC(CDReportParam)

public:
	CDReportParam();
	virtual ~CDReportParam();

// Dialog Data
	enum { IDD = IDD_REPORTPARAM };
	BOOL	m_Allarmi;
	BOOL	m_difettiPer;
	BOOL	m_difettiRand;
	BOOL	m_mappa;
	BOOL	m_alzate;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()
public:
//	CButton m_classA;
	BOOL m_classA;
	BOOL m_classB;
	BOOL m_classBig;
	BOOL m_classC;
	BOOL m_classD;
	BOOL m_graph1;
	BOOL m_graph2;
	BOOL m_graph3;
	BOOL m_graph4;
};
