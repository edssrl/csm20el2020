//------------------------------------------------------
//
//
//					RTextFile
//
// Legge ed interpreta File Testo formattati per linee
//
//------------------------------------------------------

class TextFile
{
CFile file;		// CFile object
CString nome;	// Nome File
CString separator;	// 
BOOL opened;
int numField;
// posizione dell'estensione della chiave
// Prima le righe erano identificate solo dal primo campo della riga
// ora e` possibile associare un secondo campo come estensione della chiave
int c_extKeyPosition;
void TextFile::readLine (CString &line);
BOOL fieldNumFromLine (CString &line,int num,CString &field);

public:
	int calcNumFields(LPCSTR sLine);
	void setExtKeyPosition(int pos);
TextFile (void) {numField = 0;opened = FALSE;separator = ",\r\n";
		c_extKeyPosition=-1;};
~TextFile (void) {if (opened) close();};

void addSeparator (char c){separator.Empty();separator += c;};
BOOL open (CString &name);
BOOL close (void);
// Aggiunta estensione di chiave
BOOL readField (CString &key,int field,CString &value,LPCSTR keyExt=NULL);
BOOL readField (CString &key,int field,int &value,LPCSTR keyExt=NULL);
BOOL readField (CString &key,int field,double &value,LPCSTR keyExt=NULL);
};



