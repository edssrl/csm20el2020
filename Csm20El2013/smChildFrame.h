#if !defined(AFX_SMCHILDFRAME_H__5840F628_B4DB_4DB5_8F42_C22B11BAB623__INCLUDED_)
#define AFX_SMCHILDFRAME_H__5840F628_B4DB_4DB5_8F42_C22B11BAB623__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// smChildFrame.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CsmChildFrame frame

class CsmChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CsmChildFrame)
protected:
	CsmChildFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CsmChildFrame)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

	CString getSideFromFileName(CString fileName);



		BOOL IsFrameWnd() const
		{
		// if the view begins a print preview mode, this frame returns FALSE.
		/* codice MFC interno 
		BOOL CView::DoPrintPreview(UINT nIDResource, CView* pPrintView,
				CRuntimeClass* pPreviewViewClass, CPrintPreviewState* pState)
			{
				CWnd* pMainWnd = GetParentFrame();
				if (DYNAMIC_DOWNCAST(CFrameWnd, pMainWnd) == NULL)	<--- qui entra IsFrameWnd ---
				{
					// If it is not a frame, try the main window.
					pMainWnd = AfxGetMainWnd();
				}
		------------------------------------------------------------------*/
		// this enables the preview view to be attached to the main window
	

		/*
		ASSERT(pView == NULL ||     pView->IsKindOf(RUNTIME_CLASS(CInitView))			||
									pView->IsKindOf(RUNTIME_CLASS(CRollView))			||
								    pView->IsKindOf(RUNTIME_CLASS(CTowerView))			||
								    pView->IsKindOf(RUNTIME_CLASS(CMLineView))			||
									pView->IsKindOf(RUNTIME_CLASS(CFInputOutput))		||
									pView->IsKindOf(RUNTIME_CLASS(CDoubleRollView))		||
								    pView->IsKindOf(RUNTIME_CLASS(CDoubleTowerView))	||
								    pView->IsKindOf(RUNTIME_CLASS(CDoubleMLineView))	);
		*/
		// codice precedente
		// CInitView *pView = (CInitView *)GetActiveView();
		// return (pView == NULL || ( pView->IsKindOf(RUNTIME_CLASS(CInitView)) && (!pView->c_isPreview)));

		CView *pView = GetActiveView();

		if ((pView != NULL) && pView->IsKindOf(RUNTIME_CLASS(CInitView)))
			{
			CInitView *pInitView = (CInitView *)pView;
			return(!pInitView->c_isPreview);
			}	
		return (TRUE);
	}







// Implementation
protected:
	virtual ~CsmChildFrame();

	// Generated message map functions
	//{{AFX_MSG(CsmChildFrame)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnFileOpen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMCHILDFRAME_H__5840F628_B4DB_4DB5_8F42_C22B11BAB623__INCLUDED_)
