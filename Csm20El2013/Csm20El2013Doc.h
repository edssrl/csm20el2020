// Csm20El2013Doc.h : interface of the Csm20El2013Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEMOHOLEDOC_H__92CB472B_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_)
#define AFX_DEMOHOLEDOC_H__92CB472B_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class Csm20El2013Doc : public CLineDoc
{
protected: // create from serialization only
	Csm20El2013Doc();
	DECLARE_DYNCREATE(Csm20El2013Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Csm20El2013Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~Csm20El2013Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(Csm20El2013Doc)
	afx_msg void OnOptionsCompany();
	afx_msg void OnOptionsCustomer();
	afx_msg void OnOptionsSimul();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOHOLEDOC_H__92CB472B_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_)
