#pragma once

#include "resource.h"
// CReportPrint dialog

class CDReportPrint : public CPropertyPage
{
	DECLARE_DYNAMIC(CDReportPrint)

public:
	CDReportPrint();
	virtual ~CDReportPrint();

	CLineDoc* c_pDoc;
// Dialog Data
	enum { IDD = IDD_REPORTPRINT };
	CStatic	m_ctrlStaticTo;
	CStatic	m_ctrlStaticFrom;
	CEdit	m_ctrlPosTo;
	CEdit	m_ctrlPosFrom;
	CComboBox	m_ctrlStrip;
	CComboBox	m_ctrlAlzate;
	CComboBox	m_ctrlElementi;
	int		m_repType;
	long	m_posFrom;
	long	m_posTo;
	long c_elementIndex;
	long c_alzataIndex;
	long c_stripIndex;
protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL	onOK();
	virtual BOOL OnApply( );
	afx_msg void OnReportType();
	afx_msg void OnStripReport();
	afx_msg void OnSelchangeElementi();
	afx_msg void OnSelchangeAlzate();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	DECLARE_MESSAGE_MAP()
};
