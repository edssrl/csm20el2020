// DReport.cpp : implementation file
//

#include "stdafx.h"
#include "DReport.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDReport dialog


CDReport::CDReport(CWnd* pParent /*=NULL*/)
	: CDialog(CDReport::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDReport)
	m_Allarmi = FALSE;
	m_difettiPer = FALSE;
	m_difettiRand = FALSE;
	m_mappa = FALSE;
	m_enableInt1 = FALSE;
	m_enableInt2 = FALSE;
	m_enableInt3 = FALSE;
	m_enableInt4 = FALSE;
	m_enableInt5 = FALSE;
	m_enableInt6 = FALSE;
	m_enableInt7 = FALSE;
	m_enableInt8 = FALSE;
	m_enableInt9 = FALSE;
	m_enableInt10 = FALSE;
	m_enableInt11 = FALSE;
	m_enableInt12 = FALSE;
	m_enableInt13 = FALSE;
	m_enableInt14 = FALSE;
	m_enableInt15 = FALSE;
	m_enableInt16 = FALSE;
	m_enableInt17 = FALSE;
	m_enableInt18 = FALSE;
	m_enableInt19 = FALSE;
	m_interv1 = 0;
	m_interv2 = 0;
	m_interv3 = 0;
	m_interv4 = 0;
	m_interv5 = 0;
	m_interv6 = 0;
	m_interv7 = 0;
	m_interv8 = 0;
	m_interv9 = 0;
	m_interv10 = 0;
	m_interv11 = 0;
	m_interv12 = 0;
	m_interv13 = 0;
	m_interv14 = 0;
	m_interv15 = 0;
	m_interv16 = 0;
	m_interv17 = 0;
	m_interv18 = 0;
	m_interv19 = 0;
	m_alzate = FALSE;
	//}}AFX_DATA_INIT
}


void CDReport::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDReport)
	DDX_Check(pDX, IDC_CHECK_ALLARMI1, m_Allarmi);
	DDX_Check(pDX, IDC_CHECK_DIFETTI_PER1, m_difettiPer);
	DDX_Check(pDX, IDC_CHECK_DIFETTI_RAND, m_difettiRand);
	DDX_Check(pDX, IDC_CHECK_MAPPA1, m_mappa);
	DDX_Check(pDX, IDC_ENABLE_INT1, m_enableInt1);
	DDX_Check(pDX, IDC_ENABLE_INT2, m_enableInt2);
	DDX_Check(pDX, IDC_ENABLE_INT3, m_enableInt3);
	DDX_Check(pDX, IDC_ENABLE_INT4, m_enableInt4);
	DDX_Check(pDX, IDC_ENABLE_INT5, m_enableInt5);
	DDX_Check(pDX, IDC_ENABLE_INT6, m_enableInt6);
	DDX_Check(pDX, IDC_ENABLE_INT7, m_enableInt7);
	DDX_Check(pDX, IDC_ENABLE_INT8, m_enableInt8);
	DDX_Check(pDX, IDC_ENABLE_INT9, m_enableInt9);
	DDX_Check(pDX, IDC_ENABLE_INT10, m_enableInt10);
	DDX_Check(pDX, IDC_ENABLE_INT11, m_enableInt11);
	DDX_Check(pDX, IDC_ENABLE_INT12, m_enableInt12);
	DDX_Check(pDX, IDC_ENABLE_INT13, m_enableInt13);
	DDX_Check(pDX, IDC_ENABLE_INT14, m_enableInt14);
	DDX_Check(pDX, IDC_ENABLE_INT15, m_enableInt15);
	DDX_Check(pDX, IDC_ENABLE_INT16, m_enableInt16);
	DDX_Check(pDX, IDC_ENABLE_INT17, m_enableInt17);
	DDX_Check(pDX, IDC_ENABLE_INT18, m_enableInt18);
	DDX_Check(pDX, IDC_ENABLE_INT19, m_enableInt19);
	DDX_Text(pDX, IDC_INTERV1, m_interv1);
	DDV_MinMaxInt(pDX, m_interv1, 0, 100);
	DDX_Text(pDX, IDC_INTERV2, m_interv2);
	DDV_MinMaxInt(pDX, m_interv2, 0, 100);
	DDX_Text(pDX, IDC_INTERV3, m_interv3);
	DDV_MinMaxInt(pDX, m_interv3, 0, 100);
	DDX_Text(pDX, IDC_INTERV4, m_interv4);
	DDV_MinMaxInt(pDX, m_interv4, 0, 100);
	DDX_Text(pDX, IDC_INTERV5, m_interv5);
	DDV_MinMaxInt(pDX, m_interv5, 0, 100);
	DDX_Text(pDX, IDC_INTERV6, m_interv6);
	DDV_MinMaxInt(pDX, m_interv6, 0, 100);
	DDX_Text(pDX, IDC_INTERV7, m_interv7);
	DDV_MinMaxInt(pDX, m_interv7, 0, 100);
	DDX_Text(pDX, IDC_INTERV8, m_interv8);
	DDV_MinMaxInt(pDX, m_interv8, 0, 100);
	DDX_Text(pDX, IDC_INTERV9, m_interv9);
	DDV_MinMaxInt(pDX, m_interv9, 0, 100);
	DDX_Text(pDX, IDC_INTERV10, m_interv10);
	DDV_MinMaxInt(pDX, m_interv10, 0, 100);
	DDX_Text(pDX, IDC_INTERV11, m_interv11);
	DDV_MinMaxInt(pDX, m_interv11, 0, 100);
	DDX_Text(pDX, IDC_INTERV12, m_interv12);
	DDV_MinMaxInt(pDX, m_interv12, 0, 100);
	DDX_Text(pDX, IDC_INTERV13, m_interv13);
	DDV_MinMaxInt(pDX, m_interv13, 0, 100);
	DDX_Text(pDX, IDC_INTERV14, m_interv14);
	DDV_MinMaxInt(pDX, m_interv14, 0, 100);
	DDX_Text(pDX, IDC_INTERV15, m_interv15);
	DDV_MinMaxInt(pDX, m_interv15, 0, 100);
	DDX_Text(pDX, IDC_INTERV16, m_interv16);
	DDV_MinMaxInt(pDX, m_interv16, 0, 100);
	DDX_Text(pDX, IDC_INTERV17, m_interv17);
	DDV_MinMaxInt(pDX, m_interv17, 0, 100);
	DDX_Text(pDX, IDC_INTERV18, m_interv18);
	DDV_MinMaxInt(pDX, m_interv18, 0, 100);
	DDX_Text(pDX, IDC_INTERV19, m_interv19);
	DDV_MinMaxInt(pDX, m_interv19, 0, 100);
	DDX_Check(pDX, IDC_CHECK_ALZATE, m_alzate);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDReport, CDialog)
	//{{AFX_MSG_MAP(CDReport)
	ON_BN_CLICKED(IDC_ENABLE_INT1, OnEnableInt1)
	ON_BN_CLICKED(IDC_ENABLE_INT10, OnEnableInt10)
	ON_BN_CLICKED(IDC_ENABLE_INT11, OnEnableInt11)
	ON_BN_CLICKED(IDC_ENABLE_INT12, OnEnableInt12)
	ON_BN_CLICKED(IDC_ENABLE_INT13, OnEnableInt13)
	ON_BN_CLICKED(IDC_ENABLE_INT14, OnEnableInt14)
	ON_BN_CLICKED(IDC_ENABLE_INT15, OnEnableInt15)
	ON_BN_CLICKED(IDC_ENABLE_INT16, OnEnableInt16)
	ON_BN_CLICKED(IDC_ENABLE_INT17, OnEnableInt17)
	ON_BN_CLICKED(IDC_ENABLE_INT18, OnEnableInt18)
	ON_BN_CLICKED(IDC_ENABLE_INT19, OnEnableInt19)
	ON_BN_CLICKED(IDC_ENABLE_INT2, OnEnableInt2)
	ON_BN_CLICKED(IDC_ENABLE_INT3, OnEnableInt3)
	ON_BN_CLICKED(IDC_ENABLE_INT4, OnEnableInt4)
	ON_BN_CLICKED(IDC_ENABLE_INT5, OnEnableInt5)
	ON_BN_CLICKED(IDC_ENABLE_INT6, OnEnableInt6)
	ON_BN_CLICKED(IDC_ENABLE_INT7, OnEnableInt7)
	ON_BN_CLICKED(IDC_ENABLE_INT8, OnEnableInt8)
	ON_BN_CLICKED(IDC_ENABLE_INT9, OnEnableInt9)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDReport message handlers

void CDReport::OnEnableInt1() 
{
// TODO: Add your control notification handler code here
enableIntervallo(1); 		
}

void CDReport::OnEnableInt10() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(10); 		
}

void CDReport::OnEnableInt11() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(11); 		
}

void CDReport::OnEnableInt12() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(12); 		
}

void CDReport::OnEnableInt13() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(13); 		
}

void CDReport::OnEnableInt14() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(14); 		
}

void CDReport::OnEnableInt15() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(15); 		
}

void CDReport::OnEnableInt16() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(16); 		
}


void CDReport::OnEnableInt17() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(17); 	
	
}

void CDReport::OnEnableInt18() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(18); 	
	
}

void CDReport::OnEnableInt19() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(19); 	
	
}

void CDReport::OnEnableInt2() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(2); 	
	
}


void CDReport::OnEnableInt3() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(3); 	
	
}

void CDReport::OnEnableInt4() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(4); 	
	
}

void CDReport::OnEnableInt5() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(5); 	
	
}

void CDReport::OnEnableInt6() 
{
enableIntervallo(6); 	
	// TODO: Add your control notification handler code here
	
}

void CDReport::OnEnableInt7() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(7); 	
	
}

void CDReport::OnEnableInt8() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(8); 	
	
}

void CDReport::OnEnableInt9() 
{
	// TODO: Add your control notification handler code here
enableIntervallo(9); 	
}

void CDReport::enableIntervallo(int index) 
{
UINT ItemId = -1;
BOOL enable = FALSE;
UpdateData(TRUE);

switch(index)
	{
	case 1:
		ItemId = IDC_INTERV1;
		enable = m_enableInt1;		
		break;
	case 2:
		ItemId = IDC_INTERV2;
		enable = m_enableInt2;		
		break;
	case 3:
		ItemId = IDC_INTERV3;
		enable = m_enableInt3;		
		break;
	case 4:
		ItemId = IDC_INTERV4;
		enable = m_enableInt4;		
		break;
	case 5:
		ItemId = IDC_INTERV5;
		enable = m_enableInt5;		
		break;
	case 6:
		ItemId = IDC_INTERV6;
		enable = m_enableInt6;		
		break;
	case 7:
		ItemId = IDC_INTERV7;
		enable = m_enableInt7;		
		break;
	case 8:
		ItemId = IDC_INTERV8;
		enable = m_enableInt8;		
		break;
	case 9:
		ItemId = IDC_INTERV9;
		enable = m_enableInt9;		
		break;
	case 10:
		ItemId = IDC_INTERV10;
		enable = m_enableInt10;		
		break;
	case 11:
		ItemId = IDC_INTERV11;
		enable = m_enableInt11;		
		break;
	case 12:
		ItemId = IDC_INTERV12;
		enable = m_enableInt12;		
		break;
	case 13:
		ItemId = IDC_INTERV13;
		enable = m_enableInt13;		
		break;
	case 14:
		ItemId = IDC_INTERV14;
		enable = m_enableInt14;		
		break;
	case 15:
		ItemId = IDC_INTERV15;
		enable = m_enableInt15;		
		break;
	case 16:
		ItemId = IDC_INTERV16;
		enable = m_enableInt16;		
		break;
	case 17:
		ItemId = IDC_INTERV17;
		enable = m_enableInt17;		
		break;
	case 18:
		ItemId = IDC_INTERV18;
		enable = m_enableInt18;		
		break;
	case 19:
		ItemId = IDC_INTERV19;
		enable = m_enableInt19;		
		break;
	}


if (::IsWindow (m_hWnd)&&
	(ItemId > 0)) 
	{
	CEdit* pCtrl = (CEdit*) GetDlgItem (ItemId);
	pCtrl->EnableWindow(enable);
	}


}

BOOL CDReport::OnInitDialog() 
{
CDialog::OnInitDialog();
// TODO: Add extra initialization here
UpdateData(FALSE);

for (int i = 1;i<20;i++)
	enableIntervallo(i);	

	
return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDReport::OnOK() 
{
// TODO: Add extra validation here
UpdateData(TRUE);
CDialog::OnOK();
}
