// SeqListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "SeqListCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSeqListCtrl

CSeqListCtrl::CSeqListCtrl()
{
c_seqCounter = 0;
c_maxSeqCounter = 0;

}

CSeqListCtrl::~CSeqListCtrl()
{
}


BEGIN_MESSAGE_MAP(CSeqListCtrl, gxListCtrl)
	//{{AFX_MSG_MAP(CSeqListCtrl)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSeqListCtrl message handlers

void CSeqListCtrl::onClick(int row, int col,int x, int y)
{
CString s;
s = GetItemText(row,col);

if ((c_seqCounter > 0)&&
	(s != " "))
	return;

if (row == 1)
	{
	c_seqCounter++;
	CString sVal;
	sVal.Format("%d",c_seqCounter);
	SetItemText(row,col,(LPCSTR)sVal);
	if(c_seqCounter == 1)
		{// clear all column but col
		for(int i=1;i<getNumCol();i++)
			{
			if (i != col)
				SetItemText(row,i," ");
			}
		}
	if (c_seqCounter >= c_maxSeqCounter)
		c_seqCounter = 0;
	}
	
	
	
}
