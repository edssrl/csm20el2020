// DTagliElemento.cpp : implementation file
//

#include "stdafx.h"
#include "DTagliElemento.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDTagliElemento dialog


CDTagliElemento::CDTagliElemento(CWnd* pParent /*=NULL*/)
	: CDialog(CDTagliElemento::IDD, pParent)
{

c_pElemento = NULL;
	//{{AFX_DATA_INIT(CDTagliElemento)
	m_elemento = _T("");
	m_posColtello = _T("");
	//}}AFX_DATA_INIT
}


void CDTagliElemento::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDTagliElemento)
	DDX_Control(pDX, IDC_LIST_TAGLI, m_listTagli);
	DDX_Text(pDX, IDC_ELEMENTO, m_elemento);
	DDX_Text(pDX, IDC_POS_COLTELLO, m_posColtello);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDTagliElemento, CDialog)
	//{{AFX_MSG_MAP(CDTagliElemento)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDTagliElemento message handlers

BOOL CDTagliElemento::OnInitDialog() 
{
CDialog::OnInitDialog();
	
// TODO: Add extra initialization here
// Preparazione intestazione litCtrl

ASSERT(c_pElemento != NULL);


m_listTagli.AddColumn("              ",0,IDC_NOEDITCELL);
int i;
for (i=0;i < c_pElemento->size();i++)
	{
	CString s;
	s.Format("Bobina %d",i+1);
	m_listTagli.AddColumn((LPCSTR)s,i+1,IDC_NOEDITCELL);
	}

m_listTagli.AddRow("Larghezza");
for (i=0;i < c_pElemento->size();i++)
	{
	CString s;
	s.Format("%4.0lf",(*c_pElemento)[i].c_larghezza);
	m_listTagli.AddItem(0,i+1,(LPCSTR)s);
	}

m_listTagli.AddRow("Posizione");
for (i=0;i < c_pElemento->size();i++)
	{
	CString s;
	s.Format("%d",i+1);
	m_listTagli.AddItem(1,i+1,(LPCSTR)s);
	}

m_listTagli.c_seqCounter = 0;
m_listTagli.c_maxSeqCounter = c_pElemento->size();

	
return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDTagliElemento::OnOK() 
{
// TODO: Add extra validation here
if (m_listTagli.c_seqCounter != 0)
	{
	AfxMessageBox(" ATTENZIONE Completare la SEQUENZA! ",MB_ICONERROR);
	return;
	}

// Aggiornare struttura 
for (int i=0;i<c_pElemento->size();i++)
	{
	CString s;
	s = m_listTagli.GetItemText(1,i+1);
	int order;
	order = atoi((LPCSTR)s);
	(*c_pElemento)[i].c_posizione = order;
	}
CDialog::OnOK();
}
