// modi di visualizzazione layout 
//


#ifndef LAYOUT_FILE_HEADER
#define LAYOUT_FILE_HEADER


#include "Label.h"

typedef  unsigned int Mode;

#define LINEAR		0x00000000
#define LOGARITHM	0x00000001
#define NOLABELV	0x00000002
#define NOLABELH	0x00000004
#define MSCALEV		0x00000008		// multiscala verticale
#define USE_STRING_LABELH	0x00000010
#define USE_STRING_LABELV	0x00000020

#define NOLABEL		0x00000040		// nessuna label tipo info generali
#define ONLY_TEXT_LABEL		0x00000080		// stampa solo parte bassa video con label
#define USE_STRIPRULER		0x00000100		// stampa numeri strip ruler


enum LabelId {S_CAPTION,
		S_POSITION,V_POSITION,
		S_POSITIONDAL,V_POSITIONDAL,
		S_CLIENTE,V_CLIENTE,
		S_DIMX,V_DIMX,
		S_DENSITA,V_DENSITA,
		S_FORI,V_FORI,
		S_ALARM,V_ALARM,
		S_RICETTA,V_RICETTA,
		S_ROTOLO,V_ROTOLO};


struct StringLabelH
{
CString	label;
double pos;
StringLabelH(void){clear();};
void clear(void){pos = -DBL_MAX;label = "";};

};

class Layout
{
Mode  mode;
public:


CLabel		sCaption;

// Label dati aggiuntivi
// Nome della linea, mothercoilnumber, date and time
CLabel		sLineName;
CLabel		sMothercoil;
CLabel		sDatetime;

FontInfo	fCaption;
FontInfo	fLabel;
FontInfo	fNormal;

CLabel c_sLabel [MAX_NUMLABEL];
CLabel c_vLabel [MAX_NUMLABEL];
int c_nlAfterLabel [MAX_NUMLABEL];
int c_vOrderLabel [MAX_NUMLABEL];		// ordine di disegno delle label
int c_pxSepLabel [MAX_NUMLABEL];
int c_pxSepLabelEdit;

CLabel c_cLabel [MAX_NUMCLASSI];		// label classi
//---------------
//	percentuali di 
// vista dedicate alle varie regioni dello
// schermo intorno alla zona centrale
// usate solo da CalcDrawRect
int	c_viewTopPerc;
int	c_viewBottomPerc;
int	c_viewLeftPerc;
int	c_viewRightPerc;
// flag posizione pallini classica o in alto a dx
BOOL c_useClassicPallini;

// label aggiuntive rising orizzontali
StringLabelH	c_stringLabelH[20];
void clearStringLabelH(void)
{for(int i=0;i<20;c_stringLabelH[i++].clear());};
// label aggiuntive rising verticali (compressione)
StringLabelH	c_stringLabelV[20];
void clearStringLabelV(void)
{for(int i=0;i<20;c_stringLabelV[i++].clear());};

void	setMirrorLabel(BOOL v){c_mirrorLabel = v;};
BOOL	getMirrorLabel(void){return c_mirrorLabel;};

void setDbLogo(CString logoStr)
	{if (logoStr == _T("LOGO_ELVAL"))
		c_dibLogoId = IDB_LOGO_ELVAL;
	else
		c_dibLogoId = IDB_LOGO_EMPTY;
	};

private:

CLabel sPosition;
CLabel sPositionDal;
CLabel sCliente;
CLabel sDimX;
CLabel sProduct;
CLabel sFori;
CLabel sAlarm;
CLabel sRicetta;
CLabel sRotolo;

CLabel vPosition;
CLabel vPositionDal;
CLabel vCliente;
CLabel vDimX;
CLabel vProduct;
CLabel vFori;
CLabel vAlarm;
CLabel vRicetta;
CLabel vRotolo;

double maxValY[4];
double minValY[4];
// inserito valore di allarme
double c_alValY[4];
double c_deltaY[4];

// Logo da visualizzare
int	c_dibLogoId;


CRect rcBounds;
// esclude parte centrale dal BckGr
BOOL excludeUpdateBackGr;
// esclude parte laterale label verticali dal BckGr
BOOL excludeUpdateBkgLV;

ViewType viewType;
// Background intorno
COLORREF rgbBackColor;
// background interno
COLORREF rgbDrawBackColor;
// background interno alternativo
COLORREF rgbAltDrawBackColor;




public:
int c_numSchede;

// visualizzazione solo delle bande da first a last
int c_indexFirstBanda;
int c_indexLastBanda;

// Modo visualizzazione label orizzontali partendo da ultimo a sx a zero a dx
// specifico ca
int c_mirrorLabel;
int c_mirrorDefect;
//
int c_reverseLabel;

// Numero classi
int c_numClassi;
int c_numLabelClassi;	
// indice della classe da usare
// se diverso da -1 si usa singola classe 
// di indice indexClassi
int c_indexClassi;	

int c_numCpu;		// utilizzata per calcolare numBande per col 
					// sulla base dell'impianto

// prima classe (potrebbe essere B=1 e non A=0) 	
int c_primaClasse;

// contatori dei marker per ogni lane
// visualizzati on bot
int c_countStrip[MAX_NUMSTRIP];

// flag attivazione griglia	
BOOL c_useGrid;

// dati globali usati da TargetBoard
// larghezza prima banda
int c_sizeFBanda;
// larghezza bande
int c_sizeBanda;
// bande per colonna
int c_bandePerCol;

// caso di bande dimensione diversa
// dimensioni in px
CArray <int,int> c_vectSizeBanda;
// dimensioni in mm
CArray <int,int> c_vectRealSizeBanda;
// array con schede colore di sfondo alternativo
CArray <int,int> c_bandeAltColor;

// bande per ultima colonna
int c_bandeLastCol;
// larghezza ultima banda
int c_sizeLBanda;

// prima banda indice
int c_primaBanda;
// prima banda nella scala orizzontale
int c_falsePrimaBanda;

// Unita` di misura aggiunte all'ultima label del grafico
CString c_misUnitX;
CString c_misUnitY;

// fattore moltiplicativo per calcolo label H
// serve per vis le bande in pos mm
int c_xFactor;



public:
Layout(void);

// 03-06-2008 Inserito Copy operator
Layout& operator=(Layout& source);

~Layout(void){c_bandeAltColor.RemoveAll();
	c_vectRealSizeBanda.RemoveAll();
	c_vectSizeBanda.RemoveAll();};


BOOL	useAllBande(void){return ((c_indexFirstBanda >= 0)&&
	(c_indexLastBanda > 0))?FALSE:TRUE;};

int getNumBande(void){	
		if (useAllBande())
			return  c_numSchede;
		else
			return (1 + (c_indexLastBanda - c_indexFirstBanda));};

BOOL Create(CWnd* pParentWnd);

void setExcludeBkgr(BOOL v){excludeUpdateBackGr=v;};
void setExcludeBkgLV(BOOL v){excludeUpdateBkgLV=v;};
void setAlValY(int classe,double v){c_alValY[classe-'A'] = v;};

void drawAltBackground(CDC *pDC,CRect r);

COLORREF getRgbBackColor(void) {return(rgbBackColor);};
void setRgbBackColor(COLORREF rgb) {rgbBackColor = rgb;};
COLORREF getRgbDrawBackColor(void) {return(rgbDrawBackColor);};
void setRgbDrawBackColor(COLORREF rgb) {rgbDrawBackColor = rgb;};

// max e min per classe
void setMaxY(int classe,double v)
	{maxValY[classe-'A'] = v;};
void setMinY(int classe,double v){minValY[classe-'A'] = v;};
double getMaxY(int classe)
	{return (maxValY[classe-'A']);};
double getMinY(int classe)
	{return(minValY[classe-'A']);};


void setNumSchede(int n,int f=-1,int l=-1){c_numSchede = n;c_indexFirstBanda=f;c_indexLastBanda=l;};
void setNumClassi(int n){c_numClassi = n;};
void setNumLabelClassi(int n){c_numLabelClassi = n;};
void setNumCpu(int n){c_numCpu = n;};



void setVectRealSizeBanda(CArray <int,int> &v)
		{c_vectRealSizeBanda.RemoveAll();
		for (int i=0;i<v.GetSize();i++)
			c_vectRealSizeBanda.Add(v[i]);
		};

BOOL OnEraseBkgnd(CDC* pDC,CRect &rect); 
BOOL OnEraseDrawBkgnd(CDC* pDC);

void updateDrawRegion(CDC* pdc);

void setViewType(ViewType type){viewType = type;};
void setFontCaption (int size,LPCTSTR name = "TimesNewRoman")
	{fCaption.name = name;fCaption.size = size*10;};
void setFontLabel (int size,LPCTSTR name = "TimesNewRoman")
	{fLabel.name = name;fLabel.size = size*10;};
void setFontNormal (int size,LPCTSTR name = "TimesNewRoman")
	{fNormal.name = name;fNormal.size = size*10;};

void setDrawRect(CRect &rect){rcBounds=rect;};
void CalcDrawRect (const CRect& rcBounds,CRect& rcDraw);
void Draw3DRect (CDC* pdc,const CRect& rc);
void draw (CDC* pdc);
void setCaption  (LPCTSTR s){sCaption=s;};
void setPosition (LPCTSTR s){sPosition=s;};
void setVPosition(LPCTSTR s){vPosition=s;};
void setVPosition(int v){CString s;s.Format("%6d",v);
					vPosition = s;};
int  getVPosition(void){int v;CString s(vPosition.getString());
						sscanf((LPCSTR)s,"%d",&v);
							return v;};

void setPositionDal (LPCTSTR s){sPositionDal=s;};
void setVPositionDal(LPCTSTR s){vPositionDal=s;};
void setVPositionDal(int v){CString s;s.Format("%6d",v);
						vPositionDal = s;};
int  getVPositionDal(void){int v;CString s(vPositionDal.getString());
					sscanf((LPCSTR)s,"%d",&v);
							return v;};

void setAlarm(LPCTSTR s){sAlarm=s;};
void setVAlarm(LPCTSTR s){vAlarm=s;};
void setVAlarm(int v){CString s;s.Format("%6d",v);
		vAlarm = s;};

void setCliente(LPCTSTR s){sCliente=s;};
void setVCliente(LPCTSTR s){vCliente=s;};
void setRicetta(LPCTSTR s){sRicetta=s;};
void setVRicetta(LPCTSTR s){vRicetta=s;};
void setRotolo(LPCTSTR s){sRotolo=s;};
void setVRotolo(LPCTSTR s){vRotolo=s;};

// void setLabel(LPCTSTR s){c_label.setString(s);};

void setDimX(LPCTSTR s){sDimX=s;};
void setVDimX(LPCTSTR s){vDimX=s;};
void setVDimX(int v){CString s;s.Format("%6d",v);
				vDimX = s;};
int  getVDimX (void){int v;CString s(vDimX.getString());
					sscanf((LPCSTR)s,"%d",&v);
							return v;};

void setMode(Mode m){mode = m;};


void setProduct(LPCTSTR s){sProduct=s;};
void setVProduct(LPCTSTR s){vProduct=s;};
 
void setFori(LPCTSTR s){sFori=s;};
void setVFori(LPCTSTR s){vFori=s;};

};

#endif
