// DBSet.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"

#include "Dialog.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "Csm20El2013.h"
#include "DBSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DBRicette

IMPLEMENT_DYNAMIC(DBRicette, CDaoRecordset)

DBRicette::DBRicette(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBRicette)
	m_NOME = _T("");
	m_ALLARME_A = 0.0;
	m_ALLARME_B = 0.0;
	m_ALLARME_C = 0.0;
	m_ALLARME_D = 0.0;
	m_SPESSORE_MIN = 0.0;
	m_SPESSORE_MAX = 0.0;
	m_LEGA = _T("");
	m_LENGTH_ALLARME = 0.0;
	m_SOGLIA_A = 10.0;
	m_SOGLIA_B = 30.0;
	m_SOGLIA_C = 200.0;
	m_SOGLIA_D = 500.0;
	m_SOGLIA_PER = 0.0;
	m_nFields = 14;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;

}


CString DBRicette::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBRicette::GetDefaultSQL()
{
	return _T("[RICETTE]");
}

void DBRicette::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBRicette)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Double(pFX, _T("[ALLARME_A]"), m_ALLARME_A);
	DFX_Double(pFX, _T("[ALLARME_B]"), m_ALLARME_B);
	DFX_Double(pFX, _T("[ALLARME_C]"), m_ALLARME_C);
	DFX_Double(pFX, _T("[ALLARME_D]"), m_ALLARME_D);
	DFX_Double(pFX, _T("[SPESSORE_MIN]"), m_SPESSORE_MIN);
	DFX_Double(pFX, _T("[SPESSORE_MAX]"), m_SPESSORE_MAX);
	DFX_Text(pFX, _T("[LEGA]"), m_LEGA);
	DFX_Double(pFX, _T("[LENGTH_ALLARME]"), m_LENGTH_ALLARME);
	DFX_Double(pFX, _T("[SOGLIA_A]"), m_SOGLIA_A);
	DFX_Double(pFX, _T("[SOGLIA_B]"), m_SOGLIA_B);
	DFX_Double(pFX, _T("[SOGLIA_C]"), m_SOGLIA_C);
	DFX_Double(pFX, _T("[SOGLIA_D]"), m_SOGLIA_D);
	DFX_Double(pFX, _T("[SOGLIA_PER]"), m_SOGLIA_PER);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBRicette diagnostics

#ifdef _DEBUG
void DBRicette::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBRicette::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG


void DBRicette::Close() 
{
	// TODO: Add your specialized code here and/or call the base class


CDaoRecordset::Close();

}

BOOL DBRicette::openSelectID (CString &key,BOOL closeNotFound)
{
	// TODO: Add your specialized code here and/or call the base class

// check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format("SELECT * FROM %s WHERE NOME = \'%s\'",
		GetDefaultSQL(),key);
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}


BOOL DBRicette::openSelectName (CArray <CString,CString &> &keyName)
{
	// TODO: Add your specialized code here and/or call the base class

if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format("SELECT * FROM %s ",
		GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}

for (int i=0;i<GetRecordCount();i++)
	{
	m_NOME.TrimRight();
	keyName.Add(m_NOME);
	MoveNext();
	}

Close();

return TRUE;
}



BOOL DBRicette::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// TODO: Add your specialized code here and/or call the base class
// check if db is open
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format("SELECT * FROM %s",GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	if (addOnEmpty&&CanAppend())
		{
		AddNew();
		m_NOME = _T("DEFAULT");
		m_ALLARME_A = 10.0;
		m_ALLARME_B = 10.0;
		m_ALLARME_C = 10.0;
		m_ALLARME_D = 10.0;
		m_SPESSORE_MIN = 1.0;
		m_SPESSORE_MAX = 100.0;
		m_SOGLIA_A = 15.;
		m_SOGLIA_B = 30.0;
		m_SOGLIA_C = 200.0;
		m_SOGLIA_D = 500.0;

		m_LEGA = _T("X");
		Update();
		MoveFirst();
		return TRUE;
		}
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}

BOOL DBRicette::openSelectLegaSpessore (CString &Lega,double spessore, BOOL closeNotFound)
{

if (!isDbOpen())
	return FALSE;

// TODO: Add your specialized code here and/or call the base class
// dBase already Open
CString sqlString;
//sqlString.Format("SELECT DISTINCTROW * FROM %s WHERE ((RICETTE.SPESSORE_MIN < %lf)AND(RICETTE.SPESSORE_MAX > %lf) AND (RICETTE.LEGA = \'%s\'))",
sqlString.Format("SELECT DISTINCTROW * FROM %s WHERE ((SPESSORE_MIN < %lf)AND(SPESSORE_MAX > %lf) AND (LEGA = \'%s\'))",
				 GetDefaultSQL(),spessore,spessore,Lega);

try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException *e)
	{
	CString s;
	s = "openSelectLegaSpessore: ";
	s += e->m_pErrorInfo->m_strDescription;
	AfxMessageBox(s);
	e->Delete();
	return FALSE;
	}

// Check Record Found
if (IsEOF()&&closeNotFound)
	{// Selezione Tabella Default manuale
	// Close Old query and DB
	Close();
	return FALSE;
	}
return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// DBOpzioni

IMPLEMENT_DYNAMIC(DBOpzioni, CDaoRecordset)

DBOpzioni::DBOpzioni(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBOpzioni)
	m_NOME = _T("");
	m_ALLARME_ACU = FALSE;
	m_ALLARME_DURATA = 0.0;
	m_STAMPA_AUTO = FALSE;
	m_TL_CLASSE = _T("");
	m_TL_LUNGHEZZA = 0.0;
	m_TD_DENSITA = 0.0;
	m_TD_LUNGHEZZA = 0.0;
	m_TD_SCALA = 0.0;
	m_TL_SCALA = 0.0;
	m_MAPPA100 = FALSE;
	m_STAMPA_AUTO_STRIP = FALSE;
	m_nFields = 12;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
	// m_strFilter = "NOME = DEFAULT";

}


CString DBOpzioni::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBOpzioni::GetDefaultSQL()
{
	return _T("[OPZIONI]");
}

void DBOpzioni::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBOpzioni)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Bool(pFX, _T("[ALLARME_ACU]"), m_ALLARME_ACU);
	DFX_Double(pFX, _T("[ALLARME_DURATA]"), m_ALLARME_DURATA);
	DFX_Bool(pFX, _T("[STAMPA_AUTO]"), m_STAMPA_AUTO);
	DFX_Text(pFX, _T("[TL_CLASSE]"), m_TL_CLASSE);
	DFX_Double(pFX, _T("[TL_LUNGHEZZA]"), m_TL_LUNGHEZZA);
	DFX_Double(pFX, _T("[TD_DENSITA]"), m_TD_DENSITA);
	DFX_Double(pFX, _T("[TD_LUNGHEZZA]"), m_TD_LUNGHEZZA);
	DFX_Double(pFX, _T("[TD_SCALA]"), m_TD_SCALA);
	DFX_Double(pFX, _T("[TL_SCALA]"), m_TL_SCALA);
	DFX_Bool(pFX, _T("[MAPPA100]"), m_MAPPA100);
	DFX_Bool(pFX, _T("[STAMPA_AUTO_STRIP]"), m_STAMPA_AUTO_STRIP);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBOpzioni diagnostics

#ifdef _DEBUG
void DBOpzioni::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBOpzioni::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG



void DBOpzioni::Close() 
{
	// TODO: Add your specialized code here and/or call the base class
	
CDaoRecordset::Close();

}

BOOL DBOpzioni::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// TODO: Add your specialized code here and/or call the base class
// check if db is open!
if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format("SELECT * FROM %s",GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	// Close Old query and DB
	if ((addOnEmpty)&&(CanAppend()))
		{
		AddNew();
		m_NOME = "DEFAULT";
		m_ALLARME_ACU = FALSE;
		m_ALLARME_DURATA = 10.0;
		m_STAMPA_AUTO = FALSE;
		m_TL_CLASSE = _T("A");
		m_TL_LUNGHEZZA = 100.0;
		m_TD_DENSITA = 100.0;
		m_TD_LUNGHEZZA = 100.0;
		m_TD_SCALA = 100.0;
		m_TL_SCALA = 100.0;
		m_MAPPA100 = FALSE;
		Update();
		MoveFirst();
		Close();
		return TRUE;
		}
	Close();
	return FALSE;
	}

return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// DBParametri

IMPLEMENT_DYNAMIC(DBParametri, CDaoRecordset)

DBParametri::DBParametri(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBParametri)
	m_NOME = _T("");
	m_FCDL = 0.0;
	m_L_RESIDUA_STOP = 0.0;
	m_SOGLIA_A = 0.0;
	m_SOGLIA_B = 0.0;
	m_SOGLIA_C = 0.0;
	m_SOGLIA_D = 0.0;
	m_ALARM_PULSE = 0.0;
	m_DIA_ENCODER = 0.0;
	m_ODDEVEN_DISTANCE = 0.0;
	m_nFields = 10;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;


}


CString DBParametri::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBParametri::GetDefaultSQL()
{
	return _T("[PARAMETRI]");
}

void DBParametri::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBParametri)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Double(pFX, _T("[FCDL]"), m_FCDL);
	DFX_Double(pFX, _T("[L_RESIDUA_STOP]"), m_L_RESIDUA_STOP);
	DFX_Double(pFX, _T("[SOGLIA_A]"), m_SOGLIA_A);
	DFX_Double(pFX, _T("[SOGLIA_B]"), m_SOGLIA_B);
	DFX_Double(pFX, _T("[SOGLIA_C]"), m_SOGLIA_C);
	DFX_Double(pFX, _T("[SOGLIA_D]"), m_SOGLIA_D);
	DFX_Double(pFX, _T("[ALARM_PULSE]"), m_ALARM_PULSE);
	DFX_Double(pFX, _T("[DIA_ENCODER]"), m_DIA_ENCODER);
	DFX_Double(pFX, _T("[ODDEVEN_DISTANCE]"), m_ODDEVEN_DISTANCE);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBParametri diagnostics

#ifdef _DEBUG
void DBParametri::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBParametri::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG


void DBParametri::Close() 
{
	// TODO: Add your specialized code here and/or call the base class
	
CDaoRecordset::Close();
}

BOOL DBParametri::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// TODO: Add your specialized code here and/or call the base class
// check if db is open
if (!isDbOpen())
	return FALSE;


CString sqlString;
sqlString.Format("SELECT * FROM %s",GetDefaultSQL());


try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}


// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	if (addOnEmpty && CanAppend())
		{
		AddNew();
		m_NOME = _T("DEFAULT");
		m_FCDL = 1.0;
		m_L_RESIDUA_STOP = 0.0;
		m_SOGLIA_A = 10.0;
		m_SOGLIA_B = 40.0;
		m_SOGLIA_C = 100.0;
		m_SOGLIA_D = 500.0;
		m_ALARM_PULSE = 10.;
		Update();
		MoveFirst();
		return TRUE;
		}
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}




/////////////////////////////////////////////////////////////////////////////
// DBReport

IMPLEMENT_DYNAMIC(DBReport, CDaoRecordset)

DBReport::DBReport(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(DBReport)
	m_NOME = _T("");
	m_DATI_CLIENTE = FALSE;
	m_DATA_LAVORAZIONE = FALSE;
	m_LUNGHEZZA_BOBINA = FALSE;
	m_DESCR_PERIODICI = FALSE;
	m_DESCR_RANDOM = FALSE;
	m_SOGLIE_ALLARME = FALSE;
	m_MAPPA100 = FALSE;
	m_DALMETRO = 0.0;
	m_ALMETRO = 0.0;
	m_INTERVALLO1 = 0.0;
	m_INTERVALLO2 = 0.0;
	m_INTERVALLO3 = 0.0;
	m_INTERVALLO10 = 0.0;
	m_INTERVALLO11 = 0.0;
	m_INTERVALLO12 = 0.0;
	m_INTERVALLO13 = 0.0;
	m_INTERVALLO14 = 0.0;
	m_INTERVALLO15 = 0.0;
	m_INTERVALLO16 = 0.0;
	m_INTERVALLO17 = 0.0;
	m_INTERVALLO18 = 0.0;
	m_INTERVALLO19 = 0.0;
	m_INTERVALLO4 = 0.0;
	m_INTERVALLO5 = 0.0;
	m_INTERVALLO6 = 0.0;
	m_INTERVALLO7 = 0.0;
	m_INTERVALLO8 = 0.0;
	m_INTERVALLO9 = 0.0;
	m_DESCR_ALZATE = FALSE;
	m_DESCR_GRAPH1 = FALSE;
	m_DESCR_GRAPH2 = FALSE;
	m_DESCR_GRAPH3 = FALSE;
	m_DESCR_GRAPH4 = FALSE;
	m_DESCR_CLASSA = FALSE;
	m_DESCR_CLASSB = FALSE;
	m_DESCR_CLASSC = FALSE;
	m_DESCR_CLASSD = FALSE;
	m_DESCR_CLASSBIG = FALSE;
	m_nFields = 39;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;

}


CString DBReport::GetDefaultDBName()
{
	return _T("HOLE.MDB");
}

CString DBReport::GetDefaultSQL()
{
	return _T("[REPORT]");
}

void DBReport::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(DBReport)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Bool(pFX, _T("[DATI_CLIENTE]"), m_DATI_CLIENTE);
	DFX_Bool(pFX, _T("[DATA_LAVORAZIONE]"), m_DATA_LAVORAZIONE);
	DFX_Bool(pFX, _T("[LUNGHEZZA_BOBINA]"), m_LUNGHEZZA_BOBINA);
	DFX_Bool(pFX, _T("[DESCR_PERIODICI]"), m_DESCR_PERIODICI);
	DFX_Bool(pFX, _T("[DESCR_RANDOM]"), m_DESCR_RANDOM);
	DFX_Bool(pFX, _T("[SOGLIE_ALLARME]"), m_SOGLIE_ALLARME);
	DFX_Bool(pFX, _T("[MAPPA100]"), m_MAPPA100);
	DFX_Double(pFX, _T("[DALMETRO]"), m_DALMETRO);
	DFX_Double(pFX, _T("[ALMETRO]"), m_ALMETRO);
	DFX_Double(pFX, _T("[INTERVALLO1]"), m_INTERVALLO1);
	DFX_Double(pFX, _T("[INTERVALLO2]"), m_INTERVALLO2);
	DFX_Double(pFX, _T("[INTERVALLO3]"), m_INTERVALLO3);
	DFX_Double(pFX, _T("[INTERVALLO10]"), m_INTERVALLO10);
	DFX_Double(pFX, _T("[INTERVALLO11]"), m_INTERVALLO11);
	DFX_Double(pFX, _T("[INTERVALLO12]"), m_INTERVALLO12);
	DFX_Double(pFX, _T("[INTERVALLO13]"), m_INTERVALLO13);
	DFX_Double(pFX, _T("[INTERVALLO14]"), m_INTERVALLO14);
	DFX_Double(pFX, _T("[INTERVALLO15]"), m_INTERVALLO15);
	DFX_Double(pFX, _T("[INTERVALLO16]"), m_INTERVALLO16);
	DFX_Double(pFX, _T("[INTERVALLO17]"), m_INTERVALLO17);
	DFX_Double(pFX, _T("[INTERVALLO18]"), m_INTERVALLO18);
	DFX_Double(pFX, _T("[INTERVALLO19]"), m_INTERVALLO19);
	DFX_Double(pFX, _T("[INTERVALLO4]"), m_INTERVALLO4);
	DFX_Double(pFX, _T("[INTERVALLO5]"), m_INTERVALLO5);
	DFX_Double(pFX, _T("[INTERVALLO6]"), m_INTERVALLO6);
	DFX_Double(pFX, _T("[INTERVALLO7]"), m_INTERVALLO7);
	DFX_Double(pFX, _T("[INTERVALLO8]"), m_INTERVALLO8);
	DFX_Double(pFX, _T("[INTERVALLO9]"), m_INTERVALLO9);
	DFX_Bool(pFX, _T("[DESCR_ALZATE]"), m_DESCR_ALZATE);
	DFX_Bool(pFX, _T("[DESCR_GRAPH1]"), m_DESCR_GRAPH1);
	DFX_Bool(pFX, _T("[DESCR_CLASSA]"), m_DESCR_CLASSA);
	DFX_Bool(pFX, _T("[DESCR_CLASSB]"), m_DESCR_CLASSB);
	DFX_Bool(pFX, _T("[DESCR_CLASSC]"), m_DESCR_CLASSC);
	DFX_Bool(pFX, _T("[DESCR_CLASSD]"), m_DESCR_CLASSD);
	DFX_Bool(pFX, _T("[DESCR_CLASSBIG]"), m_DESCR_CLASSBIG);
	DFX_Bool(pFX, _T("[DESCR_GRAPH2]"), m_DESCR_GRAPH2);
	DFX_Bool(pFX, _T("[DESCR_GRAPH3]"), m_DESCR_GRAPH3);
	DFX_Bool(pFX, _T("[DESCR_GRAPH4]"), m_DESCR_GRAPH4);
//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// DBReport diagnostics

#ifdef _DEBUG
void DBReport::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void DBReport::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG



void DBReport::Close() 
{
	// TODO: Add your specialized code here and/or call the base class
	
CDaoRecordset::Close();


}

BOOL DBReport::openSelectDefault (BOOL closeNotFound,BOOL addOnEmpty)
{
// TODO: Add your specialized code here and/or call the base class

return openSelect (CString(_T("DEFAULT")),closeNotFound,addOnEmpty);

}

BOOL DBReport::openSelectManual (BOOL closeNotFound,BOOL addOnEmpty)
{
// TODO: Add your specialized code here and/or call the base class

return openSelect (CString(_T("MANUAL")),closeNotFound,addOnEmpty);

}

BOOL DBReport::openSelect (CString name,BOOL closeNotFound,BOOL addOnEmpty)
{

if (!isDbOpen())
	return FALSE;

CString sqlString;
sqlString.Format("SELECT * FROM %s WHERE NOME=\'%s\'",GetDefaultSQL(),name);
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	if (addOnEmpty&&CanAppend())
		{
		AddNew();
		m_NOME = name;
		m_DATI_CLIENTE = FALSE;
		m_DATA_LAVORAZIONE = FALSE;
		m_LUNGHEZZA_BOBINA = FALSE;
		m_DESCR_PERIODICI = FALSE;
		m_DESCR_RANDOM = FALSE;
		m_DESCR_ALZATE = FALSE;
		m_SOGLIE_ALLARME = FALSE;
		m_MAPPA100 = FALSE;
		m_DALMETRO = 0.0;
		m_ALMETRO = 1000.0;
		m_INTERVALLO1 = 30.0;
		m_INTERVALLO2 = 40.0;
		m_INTERVALLO3 = 100.0;
		m_INTERVALLO4 = 0.0;
		m_INTERVALLO5 = 0.0;
		m_INTERVALLO6 = 0.0;
		m_INTERVALLO7 = 0.0;
		m_INTERVALLO8 = 0.0;
		m_INTERVALLO9 = 0.0;
		m_INTERVALLO10 = 0.0;
		m_INTERVALLO11 = 0.0;
		m_INTERVALLO12 = 0.0;
		m_INTERVALLO13 = 0.0;
		m_INTERVALLO14 = 0.0;
		m_INTERVALLO15 = 0.0;
		m_INTERVALLO16 = 0.0;
		m_INTERVALLO17 = 0.0;
		m_INTERVALLO18 = 0.0;
		m_INTERVALLO19 = 0.0;
		m_DESCR_GRAPH1 = FALSE;
		m_DESCR_GRAPH2 = FALSE;
		m_DESCR_GRAPH3 = FALSE;
		m_DESCR_GRAPH4 = FALSE;
		m_DESCR_CLASSA = FALSE;
		m_DESCR_CLASSB = FALSE;
		m_DESCR_CLASSC = FALSE;
		m_DESCR_CLASSD = FALSE;
		m_DESCR_CLASSBIG = FALSE;
		Update();
		MoveFirst();
		return TRUE;
		}
	// Close Old query and DB
	Close();
	return FALSE;
	}
return TRUE;
}
