//--------------------------------------------------------------------------
//
//			  Gestione Registry Key
//
//					F. Corradi								 21 12 96
//
//--------------------------------------------------------------------------

#include <winreg.h>


class  CRegistry
{
HKEY c_KeyRegistryRoot;
CString c_Company;
CString c_AppName;

private:
HKEY GetAppKey(void);
HKEY GetSectionKey(LPCTSTR lpszSection);

public:

CRegistry (LPCSTR company,LPCSTR app)
	{c_KeyRegistryRoot = HKEY_LOCAL_MACHINE;
	 c_Company = company;	  
	 c_AppName = app;
	 };

CRegistry (CString& company,CString& app)
	{c_KeyRegistryRoot = HKEY_LOCAL_MACHINE;
	 c_Company = company;	  
	 c_AppName = app;
	 };


UINT GetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry,int nDefault);
CString GetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry,
		LPCTSTR lpszDefault);
BOOL GetProfileBinary(LPCTSTR lpszSection, LPCTSTR lpszEntry,
	BYTE** ppData, UINT* pBytes);
BOOL WriteProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry,
	int nValue);
BOOL WriteProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry,
			LPCTSTR lpszValue);
BOOL WriteProfileBool(LPCTSTR lpszSection, LPCTSTR lpszEntry,
	BOOL data);
BOOL GetProfileBool(LPCTSTR lpszSection, LPCTSTR lpszEntry,
	BOOL data);
BOOL WriteProfileBinary(LPCTSTR lpszSection, LPCTSTR lpszEntry,
	LPBYTE pData, UINT nBytes);

};







