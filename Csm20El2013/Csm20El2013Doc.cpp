// Csm20El2013.cpp : implementation of the Csm20El2013Doc class
//

#include "stdafx.h"
#include "resource.h"


#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"

#include "DCustomer.h"
#include "DCompany.h"
// #include "DSimOption.h"

#include "Csm20El2013Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013Doc

IMPLEMENT_DYNCREATE(Csm20El2013Doc, CLineDoc)

BEGIN_MESSAGE_MAP(Csm20El2013Doc, CLineDoc)
	//{{AFX_MSG_MAP(Csm20El2013Doc)
	ON_COMMAND(ID_OPTIONS_COMPANY, OnOptionsCompany)
	ON_COMMAND(ID_OPTIONS_CUSTOMER, OnOptionsCustomer)
	ON_COMMAND(ID_OPTIONS_SIMUL, OnOptionsSimul)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013Doc construction/destruction

Csm20El2013Doc::Csm20El2013Doc()
{
	// TODO: add one-time construction code here

}

Csm20El2013Doc::~Csm20El2013Doc()
{
}

BOOL Csm20El2013Doc::OnNewDocument()
{
	if (!CLineDoc::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// Csm20El2013Doc serialization

void Csm20El2013Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013Doc diagnostics

#ifdef _DEBUG
void Csm20El2013Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void Csm20El2013Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013Doc commands



void Csm20El2013Doc::OnOptionsCompany() 
{
	// TODO: Add your command handler code here
CDCompany company;

company.DoModal();
}

void Csm20El2013Doc::OnOptionsCustomer() 
{
	// TODO: Add your command handler code here
CDCustomer customer;

customer.DoModal();
		
}

void Csm20El2013Doc::OnOptionsSimul() 
{
	// TODO: Add your command handler code here
// CDSimOption sOpt;

// sOpt.DoModal();
	
}

