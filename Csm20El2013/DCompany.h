#if !defined(AFX_DCOMPANY_H__02A312B3_D462_11D1_A6B2_00C026A019B7__INCLUDED_)
#define AFX_DCOMPANY_H__02A312B3_D462_11D1_A6B2_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DCompany.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDCompany dialog

class CDCompany : public CDialog
{
// Construction
public:
	CDCompany(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDCompany)
	enum { IDD = IDD_COMPANY };
	CString	m_name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCompany)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDCompany)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DCOMPANY_H__02A312B3_D462_11D1_A6B2_00C026A019B7__INCLUDED_)
