// Label.cpp : implementation file
//

#include "stdafx.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"

#include "DCustomer.h"
#include "DCompany.h"
// #include "DSimOption.h"

#include "Label.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLabel

CLabel::CLabel()
{
c_sValue = "SCONOSCIUTO";
//c_rect = CRect (100,100,200,200);
c_point = CPoint (10,10);
c_fontInfo.name = "Arial";
c_fontInfo.size = 100;

c_bColor = RGB (192,192,192);
c_tColor = RGB (0,0,0);

c_id = 0;
c_sBase = "0000";

c_doNotify = FALSE;
c_doSensDbclk = FALSE;
c_do3Dlook = FALSE;
c_visible = TRUE;
c_tipoIcon = NOICON;
}

CLabel::~CLabel()
{
}

BOOL CLabel::Create(CWnd* pParentWnd,UINT id)
{
LPCTSTR lpszText;

DWORD dwStyle = WS_CHILD | WS_VISIBLE | SS_NOTIFY;
c_id = id;

if (c_sValue.IsEmpty())
	lpszText = NULL;
else
	lpszText = (LPCTSTR) c_sValue;

// Default size;
CRect rect (c_point,CSize(100,100));
	
if (!CStatic::Create(lpszText,dwStyle,rect,pParentWnd,c_id))
	return FALSE;

if ((c_sValue == "SCONOSCIUTO")||(!c_visible))
	ModifyStyle(WS_VISIBLE,0);
else
	ModifyStyle(0,WS_VISIBLE);


CPaintDC dc(this); // device context for painting
rect = calcRect();

BOOL ok = SetWindowPos(NULL,c_point.x,c_point.y,rect.Size().cx+1,rect.Size().cy+1, SWP_NOMOVE | SWP_NOZORDER);
ASSERT (ok);
return TRUE;
}






BEGIN_MESSAGE_MAP(CLabel, CStatic)
	//{{AFX_MSG_MAP(CLabel)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLabel message handlers

void CLabel::setPos (CPoint p,CDC *pdc)
{

if (c_sValue == "SCONOSCIUTO")
	return;

if (GetSafeHwnd() != NULL)
	{
	// no move ?
	if (p == c_point)
		return;
	CRect r = calcRect();
	InvalidateRect (r);
	eraseRect = r;

	CClientDC dc(this);
	OnEraseBkgnd(&dc);
	c_point = p;
	BOOL ok;
	ok = SetWindowPos(NULL,c_point.x,c_point.y,r.Size().cx+1,r.Size().cy+1,SWP_NOZORDER);
	ASSERT(ok);

	updateLabel();
	}
else
	{
	c_point = p;
	if (pdc != NULL)
		draw (pdc);
	}
}


// Redraw label with new string
void CLabel::updateLabel (CString &str)
{

if (c_sValue == str)
	return;

if (GetSafeHwnd() == NULL)
	return;

CRect r = calcRect();
CRect r1 = calcRect(str);

// Same bound rect
// not erase border
if (r == r1)
	r.DeflateRect(3,3);
	
InvalidateRect (r);
eraseRect = r;

CClientDC dc(this);
OnEraseBkgnd(&dc);

setString (str);
if ((c_sValue == "SCONOSCIUTO")||(!c_visible))
	ModifyStyle(WS_VISIBLE,0);
else
	ModifyStyle(0,WS_VISIBLE);

r = calcRect();
if (r == r1)
	r.DeflateRect(3,3);

InvalidateRect (r);

BOOL ok;
ok = SetWindowPos(NULL,c_point.x,c_point.y,r.Size().cx+1,r.Size().cy+1,SWP_NOMOVE | SWP_NOZORDER);
ASSERT(ok);
}

// Redraw label with same string
void CLabel::updateLabel (CDC *pdc)
{
// 
if (GetSafeHwnd() != NULL)
	{
	CRect r = calcRect();
	r.DeflateRect(2,2);
	eraseRect = r;
	if ((c_sValue == "SCONOSCIUTO")||(!c_visible))
		ModifyStyle(WS_VISIBLE,0);
	else
		ModifyStyle(0,WS_VISIBLE);

	CClientDC dc(this);
	OnEraseBkgnd(&dc);
	InvalidateRect (r);
	}
else
	{
	if (pdc != NULL)
		draw (pdc);
	}
}


void CLabel::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

if (!c_doSensDbclk)
	return;

if (GetSafeHwnd() == NULL)
	return;

	/*
if (c_tColor != RGB(255,0,0))	
	{
	setTColor(RGB(255,0,0));
	updateLabel();
	}
else
	{
	setTColor(RGB(0,0,0));
	updateLabel();
	}
*/

/* Prova di label attiva
if (c_sValue != "Click On you crazy diamond")
	{
	setTColor(RGB(255,0,0));
	updateLabel(CString("Click On you crazy diamond"));
	}
else
	{
	setTColor(RGB(0,0,255));
	updateLabel (CString("Short Str"));
	}
*/
// CStatic::OnLButtonDblClk(nFlags, point);
if (c_doNotify)
	{
	CWnd *pwnd;
	NMHDR nmhdr;
	pwnd = GetParent();

	nmhdr.hwndFrom = GetSafeHwnd();
	nmhdr.idFrom = c_id;
	nmhdr.code = NM_DBLCLK;

	if (pwnd != NULL)
		pwnd->SendMessage(WM_NOTIFY,(WPARAM)c_id,(LPARAM)&nmhdr);
	}
}


// Disegna internamente 
void CLabel::Draw3DRect (CDC* pdc,const CRect& rectB)
{
CPen dkGrayPen,whitePen,*oPen;
CRect r;

if (rectB.top > rectB.bottom)
	return;

r = rectB;

r.right -= 1;
r.bottom -= 1;

dkGrayPen.CreatePen(PS_SOLID,0,RGB(96,96,96));
whitePen.CreatePen(PS_SOLID,0,RGB(255,255,255));


oPen = pdc->SelectObject(&dkGrayPen);
pdc->MoveTo (r.left,r.bottom);
pdc->LineTo (r.left,r.top);
pdc->MoveTo (r.left,r.top);
pdc->LineTo (r.right,r.top);

// 3D Look not for print
if (!pdc->IsPrinting())
	pdc->SelectObject(&whitePen);

pdc->MoveTo (r.right,r.top);
pdc->LineTo (r.right,r.bottom);
pdc->MoveTo (r.right,r.bottom);
pdc->LineTo (r.left,r.bottom);

pdc->SelectObject(oPen);

dkGrayPen.DeleteObject();
whitePen.DeleteObject();
}


// Calcola rettangolo circoscritto label
CRect CLabel::calcRect(CDC *ePdc) 
{
return(calcRect(c_sValue,ePdc));
}

CRect CLabel::calcRect(CString &s,CDC *ePdc) 
{
// TODO: Add your message handler code here

CDC *pdc;
if (ePdc == NULL)
	pdc = GetDC();
else
	pdc = ePdc;
CFont font,*oFont;
font.CreatePointFont (c_fontInfo.size,c_fontInfo.name,pdc);
oFont = pdc->SelectObject(&font);

// Ini Text Mode
TEXTMETRIC tm;
CSize genSize,baseSize;

pdc->GetTextMetrics(&tm);
genSize = pdc->GetTextExtent(s,s.GetLength());
baseSize = pdc->GetTextExtent(c_sBase,c_sBase.GetLength());

genSize.cx += 4;
genSize.cy += 2;
baseSize.cx += 4;
baseSize.cy += 2;

if (c_tipoIcon != NOICON)
	{// icone quadra
	genSize.cx += genSize.cy;
	baseSize.cx += baseSize.cy;
	}

//-----------------------------------
//ClientRect relativo a qs finestra
CPoint p;
if (!pdc->IsPrinting())
	{
	p = CPoint (0,0);
	}
else
	p = c_point;
CRect genRect(p,genSize);
CRect baseRect(p,baseSize);

pdc->SelectObject(oFont);

// No extern dc
if (ePdc == NULL)
	ReleaseDC(pdc);

if ((genSize.cx > baseSize.cx)||
	(genSize.cy > baseSize.cy))
	return (genRect);
else
	return (baseRect);
}




BOOL CLabel::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default

// return CStatic::OnEraseBkgnd(pDC);

CBrush ltGrayBrush,*oBrush;

ltGrayBrush.CreateSolidBrush (c_bColor);
         
ltGrayBrush.UnrealizeObject();
oBrush = pDC->SelectObject (&ltGrayBrush);    

CRect rect = calcRect(pDC);
rect.DeflateRect(1,1);
	
pDC->FillRect(rect,&ltGrayBrush);
// pDC->FillRect(eraseRect,&ltGrayBrush);

pDC->SelectObject (oBrush);    

ltGrayBrush.DeleteObject();

return TRUE;


}


void CLabel::OnPaint() 
{
CPaintDC dc(this); // device context for painting
draw(&dc);
}



void CLabel::draw (CDC *pdc)
{

if (!c_visible)
	return;

// TODO: Add your message handler code here
CRect genRect;
genRect = calcRect(pdc);

// 3d look enable ?
if (c_do3Dlook)
	Draw3DRect (pdc,genRect);

switch (c_tipoIcon)
	{
	default:
	case NOICON:
		break;
	case	IPALLINO:
		{
		CBrush brush,*oBrush;
		CRect r(genRect);
		r.right = r.left + r.Size().cy;
		r.DeflateRect(genRect.Size().cy/5,genRect.Size().cy/5);
		brush.CreateSolidBrush(c_tColor);
		oBrush = pdc->SelectObject(&brush);
		pdc->Ellipse(r);
		pdc->SelectObject(oBrush);
		brush.DeleteObject();
		}
		break;
	case	IQUADRATO:
		{
		//CBrush brush;
		CRect r(genRect);
		r.right = r.left + r.Size().cy;
		r.DeflateRect(genRect.Size().cy/5,genRect.Size().cy/5);
		//brush.CreateSolidBrush(c_tColor);
		pdc->FillSolidRect(r,c_tColor);
		//brush.DeleteObject();
		break;
		}
	}

CFont font,*oFont;
font.CreatePointFont (c_fontInfo.size,c_fontInfo.name,pdc);
oFont = pdc->SelectObject(&font);

pdc->SetTextAlign(TA_LEFT | TA_TOP);

if (pdc->IsPrinting())
	pdc->SetBkMode (TRANSPARENT);
else
	{
	pdc->SetBkMode (OPAQUE);
	pdc->SetBkColor (c_bColor);
	}

pdc->SetTextColor (c_tColor);
CPoint p(genRect.left+1,genRect.top+1);
if (c_tipoIcon != NOICON)
	{
	p.x += (genRect.Size().cy -2);
	}
pdc->ExtTextOut (p.x,p.y,0,
		NULL,c_sValue,c_sValue.GetLength(),NULL);

pdc->SelectObject(oFont);
// Do not call CStatic::OnPaint() for painting messages
}