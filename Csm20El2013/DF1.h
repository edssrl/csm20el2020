#if !defined(AFX_DF1_H__48C84B35_AEB1_49E2_93FD_6F096085571A__INCLUDED_)
#define AFX_DF1_H__48C84B35_AEB1_49E2_93FD_6F096085571A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DF1.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDF1 dialog

class CDF1 : public CDialog
{
// Construction
public:
	CDF1(CWnd* pParent = NULL);   // standard constructor
	~CDF1();					// standard destructor



BOOL	loadFromProfile(void);
BOOL	saveToProfile(void);

BOOL	c_hideMode;

CArray <CString,CString &> c_nomiRicette;


// Dialog Data
	//{{AFX_DATA(CDF1)
	enum { IDD = IDD_F1 };
	CButton	m_ctrlUseTop;
	CButton	m_ctrlUseBottom;
	CStatic	m_ctrlCoil2Label;
	CEdit	m_ctrlCoil2;
	CButton	m_ctrlMode;
	CButton	m_ctrlMode1;
	CButton	m_ctrlMode2;
	CButton	m_ctrlOk;
	CComboBox	m_ctrlSelRicetta;
	CString	m_EditSel;
	CString	m_coil;
	CString	m_alloyType;
	double	m_thickness;
	CString	m_orderNumber;
	double	m_width;
	int		m_numStrip;
	CString	m_firstRising;
	int		m_mode;
	CString	m_coil2;
	int		m_useBottomHead;
	BOOL	m_skipADef;
	CString	m_freeNote;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDF1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDF1)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnMode();
	afx_msg void OnMode1();
	afx_msg void OnMode2();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DF1_H__48C84B35_AEB1_49E2_93FD_6F096085571A__INCLUDED_)
