#pragma once

#include "DReportParam.h"
#include "DReportPrint.h"
#include "DReportSection.h"


// CReportSheet

class CDReportSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CDReportSheet)

CButton m_OKbutton;  
CButton m_Cancelbutton;   

// flag usato da onOk per sapere se le pagine acconsentono a chiudere o se lasciare aperto
BOOL	c_okClose;

public:
	CDReportSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CDReportSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	virtual ~CDReportSheet();
	void EnablePage (int nPage, BOOL bEnable = TRUE);
	void setOkClose(BOOL okClose){c_okClose = okClose;};
protected:
	  // we save the current page in TCN_SELCHANGING
       int m_nLastActive;
       // list of indexes of disabled pages
       CMap <int, int&, int, int&> m_DisabledPages;


    afx_msg void OnOKButton();  
    afx_msg void OnCancelButton(); 

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
};


