// DbRotoli.cpp : implementation file
//

#include "stdafx.h"
#include "DbRotoli.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDbRotoli

IMPLEMENT_DYNAMIC(CDbRotoli, CDaoRecordset)

CDbRotoli::CDbRotoli(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CDbRotoli)
	m_NOME = _T("");
	m_RILAVORAZIONE = 0;
	m_BOLLA = _T("");
	m_DATA_LAVORAZIONE = (DATE)0;
	m_IMPIANTO = _T("");
	m_LARGHEZZA = 0.0;
	m_LEGA = _T("");
	m_SPESSORE = 0.0;
	m_STATO = _T("");
	m_LAVORAZIONE = _T("");
	m_nFields = 10;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CDbRotoli::GetDefaultDBName()
{
	return _T("C:\\User\\hole\\Csm20El2013\\DBase\\Hole.mdb");
}

CString CDbRotoli::GetDefaultSQL()
{
	return _T("[ROTOLI]");
}

void CDbRotoli::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CDbRotoli)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[NOME]"), m_NOME);
	DFX_Long(pFX, _T("[RILAVORAZIONE]"), m_RILAVORAZIONE);
	DFX_Text(pFX, _T("[BOLLA]"), m_BOLLA);
	DFX_DateTime(pFX, _T("[DATA_LAVORAZIONE]"), m_DATA_LAVORAZIONE);
	DFX_Text(pFX, _T("[IMPIANTO]"), m_IMPIANTO);
	DFX_Double(pFX, _T("[LARGHEZZA]"), m_LARGHEZZA);
	DFX_Text(pFX, _T("[LEGA]"), m_LEGA);
	DFX_Double(pFX, _T("[SPESSORE]"), m_SPESSORE);
	DFX_Text(pFX, _T("[STATO]"), m_STATO);
	DFX_Text(pFX, _T("[LAVORAZIONE]"), m_LAVORAZIONE);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CDbRotoli diagnostics

#ifdef _DEBUG
void CDbRotoli::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CDbRotoli::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG



BOOL CDbRotoli::openSelectNome (LPCSTR nome,LPCSTR impianto,BOOL closeNotFound)
{
	// TODO: Add your specialized code here and/or call the base class

CString sqlString;
sqlString.Format("SELECT * FROM %s WHERE NOME = \'%s\' AND IMPIANTO = \'%s\' ORDER BY RILAVORAZIONE DESC",
		GetDefaultSQL(),nome,impianto);
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}
int numRecord;

numRecord = GetRecordCount();
// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}


BOOL CDbRotoli::appendRotolo (CDbRotoli *pRotolo)
{
ASSERT (pRotolo != NULL);

CString sqlString;
sqlString.Format("SELECT * FROM %s",
		GetDefaultSQL());


try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException *e)
	{
	AfxMessageBox ("Error opening RecordSet");
	e->Delete();
	return FALSE;
	}

// Perform Copy Record
if (CanAppend())
	{

	try	{AddNew();}
	catch (CDaoException *e)
		{
		AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.AddNew");
		Close();
		e->Delete();
		return FALSE;
		}
 	// Modify the Name of Ricetta
	m_NOME = pRotolo->m_NOME;
	m_LEGA = pRotolo->m_LEGA;
	m_RILAVORAZIONE = pRotolo->m_RILAVORAZIONE;
	m_LAVORAZIONE = pRotolo->m_LAVORAZIONE;
	m_IMPIANTO = pRotolo->m_IMPIANTO;
	m_BOLLA = pRotolo->m_BOLLA;
	m_STATO = pRotolo->m_STATO;
	m_SPESSORE = pRotolo->m_SPESSORE;
	m_LARGHEZZA = pRotolo->m_LARGHEZZA;
	m_DATA_LAVORAZIONE = COleDateTime::GetCurrentTime();

	try	{Update();}
	catch (CDaoException *e)
		{
		AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.Update");
		Close();
		e->Delete();
		return FALSE;
		}

	}
else
	AfxGetMainWnd()->MessageBox ("Can't Append to dbRotoli","dbRotoli.CanAppend");

Close();

return TRUE;
}