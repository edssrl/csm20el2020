// GraphView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "GraphDoc.h"

#include "DAlzate.h"
#include "DAlzataDettagli.h"
#include "DClassColor.h"

#include <AfxTempl.h>

#include "ExcelFormatLib\ExcelFormat.h"

#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"
#include "DbSet.h"
#include "DSelReportType.h"
#include "DReportSheet.h"
#include "PdfInfoMsf.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//---------------------------------------------------------
// Global function
// calcolo colori
//---------------------------------------------------------
int ClassColor [6]={0,0,0,0,0,0};

void CSM20ClassColorInvalidate (void)
{
	ClassColor[5] = 0;
}

COLORREF CSM20GetClassColor (int cl)
{

if (ClassColor[5] == 0)
	{
	CDClassColor dialog;
	dialog.loadFromProfile();
	ClassColor[0] = dialog.getRgbColorA();
	ClassColor[1] = dialog.getRgbColorB();
	ClassColor[2] = dialog.getRgbColorC();
	ClassColor[3] = dialog.getRgbColorD();
	ClassColor[4] = dialog.getRgbColorBig();
	ClassColor[5] = 1;
	}

return (ClassColor[cl%5]); 
}



//--------------------------------------------------------------
//
//  SubLine
//

// r rettangolo massimo di visualizzazione
void SubLine::setScale(CRect &rectB,LineInfo &lInfo)
{


if (numStep < lInfo.numStep)
	{
	if (gVertex != NULL)
		{
		delete [] gVertex;
		gVertex = NULL;
		}
	gVertex = new CVertex [lInfo.numStep+1];
	if (gVertex == NULL) FatalAppExit (0,"Error: polyline creation");
	}

// Save Clip region
clip = rectB;
count = lInfo.count;
numStep = lInfo.numStep;
actual = lInfo.actual;
stepScaler = lInfo.stepScaler;

//if (gBigVertex != NULL)
//	{
//	delete [] gBigVertex;
//	gBigVertex = NULL;
//	}

//gVertex = new POINT [numStep+1];
//if (gVertex == NULL) FatalAppExit (0,"Error: polyline creation");

//gBigVertex = new double [numStep+1];
//if (gBigVertex == NULL) FatalAppExit (0,"Error: gBigVertex creation");




// Trasformo da coordinate di grafico a coordinate Video
double  val;
int index;
double oldVal;
double x,y,rxr,rxl,ryb,ryt;

rxl = clip.left;
rxr = clip.right;
ryb = clip.bottom -3;
ryt = clip.top;


scale = 1 + (rxr - rxl)/1000;

for (int i=0; i<= numStep;i++)
	{

	x = rxl +((rxr - rxl)/numStep*i);

	if (i <= actual)
		index = actual - i;
	else
		index = (numStep - i) + actual;


	if (mode == LINEAR)
		{
		val = lInfo.val[index];
		val -= 	lInfo.min;
		y = ryb - ((ryb - ryt)*val/lInfo.max);
		}
	else
		{
		double MY = pow (10,log10(lInfo.max));
		double mY = pow (10,log10(lInfo.min));

		int numL = (int ) (log10(MY) - log10(mY));
		if (numL <= 0)
			numL = 1;
		int deltaY = (int)(ryb - ryt)/(numL);
		val = lInfo.val[index];
		val = log10(val/mY);
		y = (int) (val * deltaY);

		y = ryb - y;
		}


	if (y < ryt)
		y = ryt;
	gVertex[i].p.x = (int ) x;
	gVertex[i].p.y = (int ) y;
	gVertex[i].bigVertex = lInfo.valBig[index];
	//gVertex.SetAtGrow(i,CPoint(x,y));
	//gBigVertex.SetAtGrow(i,lInfo.valBig[index]);
	// gBigVertex[i] = lInfo.valBig[index];
	}

// set limit value
if (mode == LINEAR)
	{
	val = lInfo.limit;
	val -= 	lInfo.min;
	y = ryb - ((ryb - ryt)*val/lInfo.max);
	}
else
	{
	double MY = pow (10,log10(lInfo.max));
	double mY = pow (10,log10(lInfo.min));

	int numL = (int ) (log10(MY) - log10(mY));
	if (numL <= 0)
		numL = 1;
	int deltaY = (int)(ryb - ryt)/(numL);
	val = lInfo.limit;
	val = log10(val/mY);
	y = (int) (val * deltaY);
		y = ryb - y;
	}
// set limit value
limit = (int )y;

x = rxl +((rxr - rxl)/numStep) * actual;

oldVal = 	val = lInfo.val[numStep];

y = ryb - ((ryb - ryt)*(oldVal)/lInfo.max);
oldPoint.x = (int ) x;
oldPoint.y = (int ) y;

}



// r rettangolo massimo di visualizzazione
//  qui arrivano meno difetti di quelli a video, devo conservare i dati precdenti
BOOL SubLine::addNewlInfo(CRect &rectB,LineInfo &lInfo,int sizeInfo)
{


if (numStep < lInfo.numStep)
	return FALSE;

// Save Clip region
clip = rectB;
count = lInfo.count;
//count %= 
numStep = lInfo.numStep;
//actual = lInfo.actual;
stepScaler = lInfo.stepScaler;
// Trasformo da coordinate di grafico a coordinate Video
double  val;
int index;
double oldVal;
double x,y,rxr,rxl,ryb,ryt;

rxl = clip.left;
rxr = clip.right;
ryb = clip.bottom -3;
ryt = clip.top;


scale = 1 + (rxr - rxl)/1000;

// ultimo pto fine schermo?
int updateFrom = actual;
int updateTo = (actual<=lInfo.actual)?lInfo.actual:actual+1;


// gvertex esiste ed e` dimesnione numStep
for (int i=updateFrom; i<= updateTo;i++)
	{// posizioni su Punti video

	x = rxl +((rxr - rxl)/numStep*i);

	index = (updateTo - i);

	val = lInfo.val[index];
	val -= 	lInfo.min;
	y = ryb - ((ryb - ryt)*val/lInfo.max);


	if (y < ryt)
		y = ryt;
	gVertex[i].p.x = (int ) x;
	gVertex[i].p.y = (int ) y;
	gVertex[i].bigVertex = lInfo.valBig[index];;

	//gVertex.SetAtGrow(i,CPoint(x,y));
	//gBigVertex.SetAtGrow(i,lInfo.valBig[index]);
	// gBigVertex[i] = lInfo.valBig[index];
	}
// update new actual value
actual = lInfo.actual;
// set limit value
if (mode == LINEAR)
	{
	val = lInfo.limit;
	val -= 	lInfo.min;
	y = ryb - ((ryb - ryt)*val/lInfo.max);
	}
else
	{
	double MY = pow (10,log10(lInfo.max));
	double mY = pow (10,log10(lInfo.min));

	int numL = (int ) (log10(MY) - log10(mY));
	if (numL <= 0)
		numL = 1;
	int deltaY = (int)(ryb - ryt)/(numL);
	val = lInfo.limit;
	val = log10(val/mY);
	y = (int) (val * deltaY);
		y = ryb - y;
	}
// set limit value
limit = (int )y;

x = rxl +((rxr - rxl)/numStep) * actual;

oldVal = 	val = lInfo.val[sizeInfo];

y = ryb - ((ryb - ryt)*(oldVal)/lInfo.max);
oldPoint.x = (int ) x;
oldPoint.y = (int ) y;
return TRUE;
}



void SubLine::draw(CDC* pdc,BOOL update)
{
// Save Context
// pdc->SaveDC();
// Select drawing tools
CPen penLarge,pen,penLimit,*oPen,penBig;

penLarge.CreatePen(PS_SOLID,2,colorPenLarge);
pen.CreatePen(PS_SOLID,0,colorPen);
penLimit.CreatePen(PS_DOT,0,RGB(0,0,0));
penBig.CreatePen(PS_SOLID,0,colorPenBig);

// Logical to device Coord
CRect clipRect = clip;
CRgn Clip;
//ComputeClippingRectangle(pdc,&clipRect);
 
// Now if we are in print preview mode then the clipping
// rectangle needs to be adjusted before creating the
// clipping region
if (pdc->IsKindOf(RUNTIME_CLASS(CPreviewDC)))
	{
    CPreviewDC *pPrevDC = (CPreviewDC *)pdc;
 	
	pPrevDC->PrinterDPtoScreenDP(&clipRect.TopLeft());
    pPrevDC->PrinterDPtoScreenDP(&clipRect.BottomRight());
 
    // Now offset the result by the viewport origin of
    // the print preview window...
 
    CPoint ptOrg;
    ::GetViewportOrgEx(pdc->m_hDC,&ptOrg);
    clipRect += ptOrg;
    }

if (!pdc->IsPrinting())
	scale = 1;
 
// The following two function calls are the ones that
// select the clipping region into the DC. These would be
// whatever code you already have to create/select the
// clipping region

Clip.CreateRectRgnIndirect(&clipRect);
pdc->SelectClipRgn(&Clip);
int bkm = pdc->GetBkMode(); 
pdc->SetBkMode(TRANSPARENT);

oPen = pdc->SelectObject(&pen);

// linea dei limiti eliminata
//oPen = pdc->SelectObject(&penLimit);
//pdc->MoveTo (gVertex[0].x,limit);
//pdc->LineTo (gVertex[count].x,limit);
// fine 

pdc->SelectObject(&pen);

	
// Disegna Tutta La Line del Grafico
// int np = 2;
int np = 3 + (3*stepScaler);
int from = (update)? (actual-np):0;
if (from < 0)
	from =0;
//int to = (update)? (actual+np):count;
int to = (update)? actual:count;
if ((actual == 0) && (count>0))
	to = count;

if(update)
	update= 1;
for(int i=from;i<to;i++)
	{
			
	if (i == actual)
		{
		pdc->MoveTo (oldPoint);
		}
	else
		{
		pdc->MoveTo (gVertex[i].p);
		}
	pdc->LineTo (gVertex[i+1].p);

	// marker bigHole
	if(gVertex[i+1].bigVertex > 0.)
		{
		pdc->SelectObject(&penBig);
		pdc->Ellipse(gVertex[i+1].p.x-(3*scale),
				gVertex[i+1].p.y-(6*scale),
				gVertex[i+1].p.x+(3*scale),
				gVertex[i+1].p.y+(6*scale));
		pdc->SelectObject(&pen);
		}

	}

if (!pdc->IsPrinting())
	{
	pdc->SelectObject(&penLarge);

	pdc->MoveTo(gVertex[actual].p.x,clip.bottom);
	pdc->LineTo (gVertex[actual].p.x,clip.top);
	}

// Restore context
pdc->SelectObject(oPen);
penLarge.DeleteObject();
pen.DeleteObject();
penLimit.DeleteObject();
penBig.DeleteObject();


// ByFC
// Update 01-05-06
// fix BUG!!! must select null rgn before delete
pdc->SelectClipRgn(NULL);
//----------
Clip.DeleteObject();
//pdc->RestoreDC(-1);

//if (gVertex != NULL)
//	{
//	delete [] gVertex;
//	gVertex = NULL;
//	}

pdc->SetBkMode(bkm);
}

//--------------------------------------------------------------------------------------
// subrect
// r rettangolo massimo di visualizzazione
// torna posizione alta 
int SubRect::setScale(CRect &r,int offset,double max,double val)
{
// update global variable
c_max = max;
c_offset = offset;
c_val = val;
// save max rect
c_rMax = r;

if (val > 0)
	c_val = val;

double perc = val/c_max;

int delta = (int ) (perc * c_rMax.Height()); 

// int max rect
bar = r;

// offset inserito per modo TOWER
bar.bottom -= offset;
bar.top	= bar.bottom - abs(delta);
// Clipping
if (bar.top < c_rMax.top)
	bar.top = c_rMax.top;
if (bar.top > c_rMax.bottom)
	bar.top = c_rMax.bottom;

c_modified = TRUE;

return (abs(bar.top-bar.bottom));
}

// r rettangolo massimo di visualizzazione
// torna posizione alta 
int SubRect::setVal(int offset,double val)
{

if ((val == c_val)&&(offset == c_offset))
	return (abs(bar.top-bar.bottom));

c_val = val;
c_offset = offset;

double perc = c_val/c_max;

int delta = (int ) (perc * c_rMax.Height()); 

// init max rect
bar = c_rMax;

// offset inserito per modo TOWER
bar.bottom -= c_offset;
bar.top	= bar.bottom - abs(delta);
// Clipping
if (bar.top < c_rMax.top)
	bar.top = c_rMax.top;
if (bar.top > c_rMax.bottom)
	bar.top = c_rMax.bottom;

c_modified = TRUE;

return (abs(bar.top-bar.bottom));
}




void SubRect::draw(CDC* pDC)
{
// Select drawing tools
CBrush brush,br,*oBrush;
CPen penTB,penLR,*oPen;

if (!c_modified)
	return;

// Unused brush fix bug
br.CreateSolidBrush(lBrush.lbColor);
if (!brush.CreateBrushIndirect(&lBrush))
	{
	AfxMessageBox ("SW fail create BRUSH f. SubRect.draw");
	return;
	}

if (!penTB.CreatePenIndirect(&lPenTB))
	{
	AfxMessageBox ("SW fail create PEN f. SubRect.draw");
	return;
	}
if (!penLR.CreatePenIndirect(&lPenLR))
	{
	AfxMessageBox ("SW fail create PEN f. SubRect.draw");
	return;
	}


if ((oPen = pDC->SelectObject(&penTB)) == NULL)
	{
	AfxMessageBox ("SW fail Select PEN f. SubRect.draw");
	return;
	}

if ((oBrush = pDC->SelectObject(&brush)) == NULL)
 	{
	AfxMessageBox ("SW fail Select BRUSH f. SubRect.draw");
	return;
	}

// Init hatch origin
CPoint org (bar.TopLeft());
pDC->SetBrushOrg(org);

pDC->Rectangle((LPCRECT)bar);
/*---------------------------
 Testo nel rettangolo
pDC->SetTextAlign(TA_CENTER | TA_BOTTOM);
// pDC->SetBkColor(lPen.lopnColor);
pDC->SetTextColor(RGB(0,0,0));

CPoint p;
p.x = (bar.right-bar.left)/2 + bar.left;
p.y = bar.bottom - 2;
CString str;
str.Format ("%c%1d",id,pos);
pDC->ExtTextOut (p.x,p.y,ETO_CLIPPED,
		bar,str,str.GetLength(),NULL);
--------------------------------*/
  
// Draw border
// Top and bottom compresi in rectangle

// Linea Sx
if ((pDC->SelectObject(&penLR)) == NULL)
	{
	AfxMessageBox ("SW fail Select PEN f. SubRect.draw");
	return;
	}
CPoint point(bar.TopLeft());
pDC->MoveTo (point);
point.y = bar.BottomRight().y; 
pDC->LineTo (point);

// Linea Dx
point = bar.TopLeft();
point.x = bar.BottomRight().x;
pDC->MoveTo (point);
point.y = bar.BottomRight().y; 
pDC->LineTo (point);


// Restore context
pDC->SelectObject(oPen);
pDC->SelectObject(oBrush);
br.DeleteObject();
brush.DeleteObject();
penTB.DeleteObject();
penLR.DeleteObject();

c_modified = FALSE;
}




//-------------------------------------------------------

//--------------------------------------------------------------------  

void ViewDensUnit::setScale(CRect &r,double max,double val[])
// 	double valB,double valC,double valD)
{
// CRect ra,rb,rc,rd;
// split avail. rect in 4 sub rect
CRect ra ;
int Xsize,Ypos;
if (vMode == FLAT)
	{
	Xsize = (r.right - r.left)/subR.GetSize();
	}
else
	{// TOWER
	Xsize = (r.right - r.left);
	}
// |a b c d|

Ypos = 0;
// Calc Rect
ra = r;
for (int i=0;i<subR.GetSize();i++)
	{
	if (vMode == FLAT)
		{
		ra.right = ra.left;
		ra.left  = ra.right + Xsize;
		Ypos = 0;
		}
	Ypos += subR[i].setScale(ra,Ypos,max,val[i]);
	}

}

void ViewDensUnit::setVal(double val[])
// 	double valB,double valC,double valD)
{

int Ypos = 0;
for (int i=0;i<subR.GetSize();i++)
	{
	if (vMode == FLAT)
		{
		Ypos = 0;
		}
	Ypos += subR[i].setVal(Ypos,val[i]);
	}
}





/////////////////////////////////////////////////////////////////////////////
// CDensView message handlers
/////////////////////////////////////////////////////////////////////////////
// CInitView

IMPLEMENT_DYNCREATE(CInitView, CPdfPrintView)

CInitView::CInitView():
	CInit()
{

}

CInit::CInit()
{
vType = INITVIEW;

m_DESCR_ALZATE = TRUE;

m_pLocalDocument = NULL;

//onlyTextReport = FALSE;
directPrint = FALSE;
c_doPrintStrip = FALSE;
c_prStripNumber = 0;
c_prElementNumber = 0;
c_prAlzataNumber = 0;

c_isPrinting = FALSE;
c_isPreview = FALSE;
c_continuePrinting = TRUE;
c_pWaitDialog = NULL;


c_leftMarg = 20;		// margine sinistro report in mm
c_rightMarg = 10;	// margine destro report in mm
c_topMarg = 20;		// margine alto report in mm
c_bottomMarg = 15;	// margine basso report in mm
c_lastPosReport = CPoint(0,0);


c_useManualReportConfig = FALSE;
c_reportGraph[0] = FALSE;
c_reportGraph[1] = FALSE;
c_reportGraph[2] = FALSE;
c_reportGraph[3] = FALSE;
c_reportClass[0] = FALSE;
c_reportClass[1] = FALSE;
c_reportClass[2] = FALSE;
c_reportClass[3] = FALSE;
c_reportClass[4] = FALSE;



c_dalMetro=0.;
c_alMetro=0.;

c_subNastro = CRect(0,0,1,1);

c_numPageRepBobine = 0;

c_numTextPage2 = 0;
c_numTextPage3 = 0;
c_numTextPage4 = 0;
}

CInitView::~CInitView()
{
}

CInit::~CInit()
{
}

BEGIN_MESSAGE_MAP(CInitView,CPdfPrintView)
	//{{AFX_MSG_MAP(CInitView)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_WM_ERASEBKGND()
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitView drawing

CRect CInit::getDrawRect(void)
{
CRect rect (0,0,0,0);
return (rect);
}

CRect CInit::getMeterRect(void)
{
CRect rect (0,0,0,0);
return (rect);
}

void CInitView::OnDraw(CDC* pDC)
{

onDraw(pDC);

}

void CInit::onDraw(CDC* pDC)
{
// Do NOT call onDraw
// onDraw (pDC,TRUE);

DOC_CLASS* pDoc = (DOC_CLASS *) GetDoc();
// TODO: add draw code here
CRect rcBounds;
TEXTMETRIC tm;

pDC->SaveDC();

if (pDC->IsPrinting())
	{
	rcBounds =  prLimit;
	}
else
	{
	GetClientRect (rcBounds);
	}


// Ini Text Mode
pDC->GetTextMetrics(&tm);
pDC->SetBkMode (TRANSPARENT);

CFont fontCaption,fontCompany,fontNormal;
fontCaption.CreatePointFont (300,"Arial",pDC);
LOGFONT plf;
fontCaption.GetLogFont(&plf);

plf.lfQuality = 1;
plf.lfUnderline = 1;
plf.lfWeight = 10000;
plf.lfItalic = 1;
fontCaption.DeleteObject();

fontCaption.CreateFontIndirect (&plf);

//------------------------
fontCompany.CreatePointFont (200,"Arial",pDC);
LOGFONT pclf;
fontCompany.GetLogFont(&pclf);

pclf.lfQuality = 1;
pclf.lfUnderline = 1;
pclf.lfWeight = 10000;
pclf.lfItalic = 1;
fontCompany.DeleteObject();

fontCompany.CreateFontIndirect (&pclf);


fontNormal.CreatePointFont (120,"Arial",pDC);

pDC->SelectObject(&fontCaption); 

CString str;
if (pDC->IsPrinting())
	{
	str.LoadString (CSM_GRAPHVIEW_PRINTCAPTION0);
	// str ="Report di Controllo Qualita`";
	}
else
	{
	str.LoadString (CSM_GRAPHVIEW_CAPTION0);
	// str ="CSM 20";
	}

// Logo EDS
if (!pDC->IsPrinting())
	{
	HANDLE hDib = OpenDIB ("logoEds.bmp");
	if (hDib)
		{
		BITMAPINFOHEADER bi;
		DibInfo (hDib,&bi);
		StretchDibBlt (pDC->GetSafeHdc(),rcBounds.left + 30,rcBounds.top + 10, 200,100,hDib,0,0,bi.biWidth,bi.biHeight,SRCCOPY);
		FreeDib();
		}
	}



CPoint pt;
CSize genSize;
genSize = pDC->GetOutputTextExtent(str,str.GetLength());

// era TA_CENTER -> sovrappone con logo in Csm20Report
pDC->SetTextAlign(TA_LEFT | TA_TOP);

int trueLeft;
// 30 LOGO 30 Pinhole Detector
trueLeft = rcBounds.left + 30 + 200 + 30;
pt.x = trueLeft;
//pt.x = rcBounds.left + (rcBounds.right - rcBounds.left)/2;

int deltay = 25;
pt.y	= rcBounds.top + deltay;

// Logo EDS
if (!pDC->IsPrinting())
	{
	HANDLE hDib = OpenDIB ("logoEds.bmp");
	if (hDib)
		{
		BITMAPINFOHEADER bi;
		DibInfo (hDib,&bi);
		StretchDibBlt (pDC->GetSafeHdc(),rcBounds.left + 30,rcBounds.top + 10, 200,100,hDib,0,0,bi.biWidth,bi.biHeight,SRCCOPY);
		FreeDib();
		}
	}

pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);

pDC->SelectObject(&fontCompany); 
pt.y	+= genSize.cy;
pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,pDoc->getCompany(),pDoc->getCompany().GetLength(),NULL);

pDC->SetTextAlign(TA_LEFT | TA_TOP);
if (pDoc->loadedData)
	{
	pDC->SelectObject(&fontNormal); 
	pt.x = rcBounds.left + 30;
	pt.y += (genSize.cy) ;

	int alignVal = 2*(rcBounds.right - rcBounds.left)/7;
	if (pDC->IsPrinting())
		alignVal = (rcBounds.right - rcBounds.left)/2;

	//str = "COIL       : " + pDoc->getRotolo();
	str.LoadString (CSM_GRAPHVIEW_ROTOLO);
	//str += pDoc->getRotolo();
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	// extIncluded
	str += pDoc->getRotolo();
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);

	
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	pt.y += genSize.cy + genSize.cy/2;
	//str = "PRODUCT    : " + pDoc->lega;
	str.LoadString (CSM_GRAPHVIEW_LEGA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->c_lega;
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);

	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	pt.y += genSize.cy + genSize.cy/2;

	CString s;
	// str = "SPESSORE (mm)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_THICK);
	str += s;
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	s.Format (" : %5.2lf",pDoc->c_spessore);
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,s,s.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	// str = "LENGTH (m)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_LUNGHEZZA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	s.Format ("%d",(int)pDoc->getVTotalLength());
	str += s;	
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	
	pt.y += genSize.cy + genSize.cy/2;
	// str = "WIDTH (mm)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_LARGHEZZA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	s.Format (" : %5.0lf",pDoc->c_larghezza);
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,s,s.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	// str = "CUSTOMER     : " + pDoc->bolla;
	str.LoadString (CSM_GRAPHVIEW_CLIENTE);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->GetVCliente();
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	CTime t (pDoc->c_startTime);
	// str = "DATE          : " + t.Format("%d-%m-%Y  %H:%M:%S");
	CString formatRes;
	formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
	str.LoadString(CSM_GRAPHVIEW_DATE);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += t.Format(formatRes);
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);

	pt.y += genSize.cy + genSize.cy/2;
//	str = "INSPECTION CODE: " + pDoc->nomeRicetta;
	str.LoadString(CSM_GRAPHVIEW_RICETTA);
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->nomeRicetta;
	pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
		NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	}
	
c_lastPosReport = pt;
pDC->RestoreDC(-1);
}


// torna valore pos y raggiunta
int CInit::onDraw(CDC* pDC,BOOL doPrint)
{

DOC_CLASS* pDoc = (DOC_CLASS *) GetDoc();
// TODO: add draw code here
CRect rcBounds;
TEXTMETRIC tm;

pDC->SaveDC();

if (pDC->IsPrinting())
	{
	if (doPrint)
		{
		rcBounds =  prLimit;
		}
	else
		{
		rcBounds = c_printRect;
		}
	}
else
	{
	GetClientRect (rcBounds);
	}


// Ini Text Mode
pDC->GetTextMetrics(&tm);
pDC->SetBkMode (TRANSPARENT);

CFont fontCaption,fontCompany,fontNormal;
fontCaption.CreatePointFont (300,"Arial",pDC);
LOGFONT plf;
fontCaption.GetLogFont(&plf);

plf.lfQuality = 1;
plf.lfUnderline = 1;
plf.lfWeight = 10000;
plf.lfItalic = 1;
fontCaption.DeleteObject();

fontCaption.CreateFontIndirect (&plf);

//------------------------
// era 200
fontCompany.CreatePointFont (160,"Arial",pDC);
LOGFONT pclf;
fontCompany.GetLogFont(&pclf);

pclf.lfQuality = 1;
pclf.lfUnderline = 1;
pclf.lfWeight = 10000;
pclf.lfItalic = 1;
fontCompany.DeleteObject();

fontCompany.CreateFontIndirect (&pclf);

// era 120
fontNormal.CreatePointFont (110,"Arial",pDC);

pDC->SelectObject(&fontCaption); 

CString str;
if (pDC->IsPrinting())
	{
	str.LoadString (CSM_GRAPHVIEW_PRINTCAPTION0);
	// str ="Report di Controllo Qualita`";
	}
else
	{
	str.LoadString (CSM_GRAPHVIEW_CAPTION0);
	// str ="CSM 20";
	}

CPoint pt;
CSize genSize;
genSize = pDC->GetOutputTextExtent(str,str.GetLength());

pDC->SetTextAlign(TA_CENTER | TA_TOP);

pt.x = rcBounds.left + (rcBounds.right - rcBounds.left)/2;

// deltay = 50; 
int deltay = 30;
pt.y	= rcBounds.top + deltay;
if(doPrint)
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,str,str.GetLength(),NULL);

pDC->SelectObject(&fontCompany); 
pt.y	+= genSize.cy;
if(doPrint)
	pDC->ExtTextOut (pt.x,pt.y,0,
		NULL,pDoc->getCompany(),pDoc->getCompany().GetLength(),NULL);

pDC->SetTextAlign(TA_LEFT | TA_TOP);
if (pDoc->loadedData)
	{
	pDC->SelectObject(&fontNormal); 
	pt.x = rcBounds.left + 30;
	pt.y += (genSize.cy) ;

	int alignVal = 2*(rcBounds.right - rcBounds.left)/7;
	if (pDC->IsPrinting())
		alignVal = (rcBounds.right - rcBounds.left)/2;

	//str = "COIL       : " + pDoc->getRotolo();
	str.LoadString (CSM_GRAPHVIEW_ROTOLO);
	//str += pDoc->getRotolo();
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->getRotolo();
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);

	
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	pt.y += genSize.cy + genSize.cy/2;
	//str = "PRODUCT    : " + pDoc->lega;
	str.LoadString (CSM_GRAPHVIEW_LEGA);
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->c_lega;
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);

	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;

	/* disabilito vis stato
	str = "STATO" ;
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : " + pDoc->c_stato;
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);

	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	*/	


	CString s;
	s.Format ("%5.3lf",pDoc->c_spessore);
	// str = "SPESSORE (mm) ";
	str.LoadString (CSM_GRAPHVIEW_THICK);
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : " + s;
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());


	pt.y += genSize.cy + genSize.cy/2;
	// str = "LENGTH (m)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_LUNGHEZZA);
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : ";
	s.Format ("%d",(int)pDoc->getVTotalLength());
	str += s;	
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	
	pt.y += genSize.cy + genSize.cy/2;
	// str = "WIDTH (mm)   : " + s;
	str.LoadString (CSM_GRAPHVIEW_LARGHEZZA);
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : ";
	s.Format ("%5.0lf",pDoc->c_larghezza);
	str += s;	
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());
	

	pt.y += genSize.cy + genSize.cy/2;
	// str = "CUSTOMER     : " + pDoc->bolla;
	str.LoadString (CSM_GRAPHVIEW_CLIENTE);
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->c_cliente;
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;
	CTime t (pDoc->c_startTime);
	// str = "DATE          : " + t.Format("%d-%m-%Y  %H:%M:%S");
	CString formatRes;
	formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
	str.LoadString(CSM_GRAPHVIEW_DATE);
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += t.Format(formatRes);
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);

	pt.y += genSize.cy + genSize.cy/2;
//	str = "INSPECTION CODE: " + pDoc->nomeRicetta;
	str.LoadString(CSM_GRAPHVIEW_RICETTA);
	if(doPrint)
		pDC->ExtTextOut (pt.x,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	str = " : ";
	str += pDoc->nomeRicetta;
	if(doPrint)
		pDC->ExtTextOut (pt.x+alignVal,pt.y,0,
			NULL,str,str.GetLength(),NULL);
	genSize = pDC->GetOutputTextExtent(str,str.GetLength());

	pt.y += genSize.cy + genSize.cy/2;

	}
c_lastPosReport = pt;
pDC->RestoreDC(-1);
return(pt.y);
}



//---------------------------------------------------------------------------
//
//	Report Rotolo 2
//
//	Prima pagina report di coil
//

void CInit::reportRotolo2(CDC* pDC,CRect rcBounds)
{
DOC_CLASS* pDoc = (DOC_CLASS *) GetDoc();
// TODO: add draw code here
pDC->SaveDC();

// intestazione report
// provo con CPage
CPage*	ps= new CPage(rcBounds,pDC,FALSE,MM_TEXT);

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

// Intestazione
//--------------
	TABLEHEADER* pTable1 = NULL;        
	pTable1=new TABLEHEADER;  
	pTable1->SetSkip=TRUE;	// no fill 
	pTable1->PointSize=24;
	pTable1->LineSize=1;    // default shown only for demp purposes
	pTable1->NumPrintLines=1;
	pTable1->UseInches=TRUE;
	pTable1->AutoSize=FALSE;
	pTable1->Border=TRUE;
	pTable1->FillFlag=FILL_NONE;
	pTable1->NumColumns=1;
	pTable1->NumRows = 0;
	pTable1->StartRow=bTop;
	pTable1->StartCol=bLeft;
	pTable1->EndCol=bRight;
	pTable1->HeaderLines=1;
	// Intestazione: 
	pTable1->ColDesc[0].Init((bRight-bLeft),"QUALITY CONTROL REPORT COIL",FILL_NONE);
	ps->setRealPrint(TRUE);
	ps->Table(pTable1);	

	// Nome rotolo
	TABLEHEADER* pTable2 = NULL;        
	pTable2=new TABLEHEADER;  
	pTable2->SetSkip=TRUE;	// no fill 
	pTable2->PointSize=13;
	pTable2->LineSize=1;    // default shown only for demp purposes
	pTable2->NumPrintLines=1;
	pTable2->UseInches=TRUE;
	pTable2->AutoSize=TRUE;
	pTable2->Border=TRUE;
	pTable2->VLines = FALSE;	//	true draw vertical seperator lines
	pTable2->HLines = TRUE;    // ditto on horizontal lines
	pTable2->FillFlag=FILL_NONE;
	pTable2->NumColumns=2;
	pTable2->NumRows = 1;
	pTable2->StartRow=pTable1->EndRow+0.15;
	pTable2->StartCol=bLeft;
	pTable2->EndCol=bRight;
	pTable2->NoHeader=TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable2);
	// righe
	CString str;
	// strip
	str.LoadString (CSM_GRAPHVIEW_ROTOLO);
	ps->Print(pTable2,0,0,13,TEXT_LEFT,(LPCSTR)str);
	str = ": ";
	str += pDoc->getRotolo();
	ps->Print(pTable2,0,1,13,TEXT_LEFT,(LPCSTR)str);

	// Alloy Type 
	TABLEHEADER* pTable3 = NULL;        
	pTable3=new TABLEHEADER;  
	pTable3->SetSkip=TRUE;	// no fill 
	pTable3->PointSize=13;
	pTable3->LineSize=1;    // default shown only for demp purposes
	pTable3->NumPrintLines=1;
	pTable3->UseInches=TRUE;
	pTable3->AutoSize=TRUE;
	pTable3->Border=TRUE;
	pTable3->VLines = FALSE;	//	true draw vertical seperator lines
	pTable3->HLines = TRUE;    // ditto on horizontal lines
	pTable3->FillFlag=FILL_NONE;
	pTable3->NumColumns=2;
	pTable3->NumRows = 1;
	pTable3->StartRow= pTable2->EndRow+0.1;
	pTable3->StartCol=bLeft;
	pTable3->EndCol=bRight;
	pTable3->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable3);	

	// Materiale
	str.LoadString (CSM_GRAPHVIEW_LEGA);
	ps->Print(pTable3,0,0,13,TEXT_LEFT,(LPCSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (": %s",pDoc->c_lega);
	ps->Print(pTable3,0,1,13,TEXT_LEFT,(LPCSTR)str);
		
	// Thickness
	TABLEHEADER* pTable4 = NULL;        
	pTable4=new TABLEHEADER;  
	pTable4->SetSkip=TRUE;	// no fill 
	pTable4->PointSize=13;
	pTable4->LineSize=1;    // default shown only for demp purposes
	pTable4->NumPrintLines=1;
	pTable4->UseInches=TRUE;
	pTable4->AutoSize=TRUE;
	pTable4->Border=TRUE;
	pTable4->VLines = FALSE;	//	true draw vertical seperator lines
	pTable4->HLines = TRUE;    // ditto on horizontal lines
	pTable4->FillFlag=FILL_NONE;
	pTable4->NumColumns=2;
	pTable4->NumRows = 1;
	pTable4->StartRow= pTable3->EndRow+0.1;
	pTable4->StartCol=bLeft;
	pTable4->EndCol=bRight;
	pTable4->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable4);	

	// Spessore
	str.LoadString (CSM_GRAPHVIEW_THICK);
	ps->Print(pTable4,0,0,13,TEXT_LEFT,(LPCSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (": %4.2lf",pDoc->c_spessore);
	ps->Print(pTable4,0,1,13,TEXT_LEFT,(LPCSTR)str);

	// Lunghezza 
	TABLEHEADER* pTable5 = NULL;        
	pTable5=new TABLEHEADER;  
	pTable5->SetSkip=TRUE;	// no fill 
	pTable5->PointSize=13;
	pTable5->LineSize=1;    // default shown only for demp purposes
	pTable5->NumPrintLines=1;
	pTable5->UseInches=TRUE;
	pTable5->AutoSize=TRUE;
	pTable5->Border=TRUE;
	pTable5->VLines = FALSE;	//	true draw vertical seperator lines
	pTable5->HLines = TRUE;    // ditto on horizontal lines
	pTable5->FillFlag=FILL_NONE;
	pTable5->NumColumns=2;
	pTable5->NumRows = 1;
	pTable5->StartRow= pTable4->EndRow+0.1;
	pTable5->StartCol=bLeft;
	pTable5->EndCol=bRight;
	pTable5->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable5);	

	// lunghezza
	str.LoadString (CSM_GRAPHVIEW_LUNGHEZZA);
	ps->Print(pTable5,0,0,13,TEXT_LEFT,(LPCSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (": %d",(int)pDoc->getVTotalLength());
	ps->Print(pTable5,0,1,13,TEXT_LEFT,(LPCSTR)str);

	// Larghezza 
	TABLEHEADER* pTable6 = NULL;        
	pTable6=new TABLEHEADER;  
	pTable6->SetSkip=TRUE;	// no fill 
	pTable6->PointSize=13;
	pTable6->LineSize=1;    // default shown only for demp purposes
	pTable6->NumPrintLines=1;
	pTable6->UseInches=TRUE;
	pTable6->AutoSize=TRUE;
	pTable6->Border=TRUE;
	pTable6->VLines = FALSE;	//	true draw vertical seperator lines
	pTable6->HLines = TRUE;    // ditto on horizontal lines
	pTable6->FillFlag=FILL_NONE;
	pTable6->NumColumns=2;
	pTable6->NumRows = 1;
	pTable6->StartRow= pTable5->EndRow+0.1;
	pTable6->StartCol=bLeft;
	pTable6->EndCol=bRight;
	pTable6->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable6);	

	// larghezza
	str.LoadString (CSM_GRAPHVIEW_LARGHEZZA);
	ps->Print(pTable6,0,0,13,TEXT_LEFT,(LPCSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (": %4.0lf",pDoc->c_larghezza);
	ps->Print(pTable6,0,1,13,TEXT_LEFT,(LPCSTR)str);

	// Cliente 
	TABLEHEADER* pTable7 = NULL;        
	pTable7=new TABLEHEADER;  
	pTable7->SetSkip=TRUE;	// no fill 
	pTable7->PointSize=13;
	pTable7->LineSize=1;    // default shown only for demp purposes
	pTable7->NumPrintLines=1;
	pTable7->UseInches=TRUE;
	pTable7->AutoSize=TRUE;
	pTable7->Border=TRUE;
	pTable7->VLines = FALSE;	//	true draw vertical seperator lines
	pTable7->HLines = TRUE;    // ditto on horizontal lines
	pTable7->FillFlag=FILL_NONE;
	pTable7->NumColumns=2;
	pTable7->NumRows = 1;
	pTable7->StartRow= pTable6->EndRow+0.1;
	pTable7->StartCol=bLeft;
	pTable7->EndCol=bRight;
	pTable7->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable7);	

	// cliente
	str.LoadString (CSM_GRAPHVIEW_CLIENTE);
	ps->Print(pTable7,0,0,13,TEXT_LEFT,(LPCSTR)str);
	str.Format (": %s",pDoc->c_cliente);
	ps->Print(pTable7,0,1,13,TEXT_LEFT,(LPCSTR)str);


	// data ora 
	TABLEHEADER* pTable8 = NULL;        
	pTable8=new TABLEHEADER;  
	pTable8->SetSkip=TRUE;	// no fill 
	pTable8->PointSize=13;
	pTable8->LineSize=1;    // default shown only for demp purposes
	pTable8->NumPrintLines=1;
	pTable8->UseInches=TRUE;
	pTable8->AutoSize=TRUE;
	pTable8->Border=TRUE;
	pTable8->VLines = FALSE;	//	true draw vertical seperator lines
	pTable8->HLines = TRUE;    // ditto on horizontal lines
	pTable8->FillFlag=FILL_NONE;
	pTable8->NumColumns=2;
	pTable8->NumRows = 1;
	pTable8->StartRow=pTable7->EndRow+0.1;
	pTable8->StartCol=bLeft;
	pTable8->EndCol=bRight;
	pTable8->NoHeader=TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable8);
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
	// data
	CTime t (pDoc->c_startTime);
	// str = "DATE          : " + t.Format("%d-%m-%Y  %H:%M:%S");
	str.LoadString(CSM_GRAPHVIEW_DATE);
	ps->Print(pTable8,0,0,13,TEXT_LEFT,(LPCSTR)str);
	// format data
	CString formatRes;
	formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
	str = ": ";
	str += t.Format(formatRes);
	ps->Print(pTable8,0,1,13,TEXT_LEFT,(LPCSTR)str);

	

	// FreeNote 
	TABLEHEADER* pTable8a = NULL;        
	pTable8a=new TABLEHEADER;  
	pTable8a->SetSkip=TRUE;	// no fill 
	pTable8a->PointSize=14;
	pTable8a->LineSize=1;    // default shown only for demp purposes
	pTable8a->NumPrintLines=1;
	pTable8a->UseInches=TRUE;
	pTable8a->AutoSize=TRUE;
	pTable8a->Border=TRUE;
	pTable8a->VLines = FALSE;	//	true draw vertical seperator lines
	pTable8a->HLines = TRUE;    // ditto on horizontal lines
	pTable8a->FillFlag=FILL_NONE;
	pTable8a->NumColumns=2;
	pTable8a->NumRows = 1;
	pTable8a->StartRow= pTable8->EndRow+0.1;
	pTable8a->StartCol=bLeft;
	pTable8a->EndCol=bRight;
	pTable8a->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable8a);	

	// freeNote
	str.LoadString (CSM_GRAPHVIEW_FREENOTE);
	ps->Print(pTable8a,0,0,14,TEXT_LEFT,(LPCSTR)str);
	str.Format (": %s",pDoc->c_freeNote);
	ps->Print(pTable8a,0,1,14,TEXT_LEFT,(LPCSTR)str);


//-----------------------------------------------------
// sezione dettaglio difetti 

double seg[20];
int interv[20];
int numSeg = 0;
for (int i=0;i<19;i++)
	{
	if (m_INTERVALLO[i] > 0)
		{
		interv[numSeg] = (int) m_INTERVALLO[i];
		seg[numSeg++] = (pDoc->getVTotalLength() * m_INTERVALLO[i] / 100);
		}				
	}


double lastYPos = bTop;
TABLEHEADER* pTable10 = NULL;        
pTable10=new TABLEHEADER;  
pTable10->SetSkip=TRUE;	// no fill 
pTable10->PointSize=10;
pTable10->LineSize=1;    // default shown only for demp purposes
pTable10->NumPrintLines=1;
pTable10->UseInches=TRUE;
pTable10->AutoSize=TRUE;
pTable10->Border=TRUE;
pTable10->VLines = TRUE;	//	true draw vertical seperator lines
pTable10->HLines = TRUE;    // ditto on horizontal lines
pTable10->FillFlag=FILL_NONE;
pTable10->NumColumns=2;
pTable10->NumRows = 2;
pTable10->StartRow=pTable8a->EndRow+0.1;
pTable10->StartCol=bLeft;
pTable10->EndCol=bRight;
pTable10->NoHeader=TRUE;

ps->setRealPrint(TRUE);
ps->Table(pTable10);	


// recalc totali
pDoc->c_difCoil.calcTotalCode ();


CString s;
double da,db,dc,dd,rd;
s = "   ";
s += "Threshold  [um] ";
ps->Print(pTable10,0,0,9,TEXT_LEFT,(LPCSTR)s);
da = pDoc->sogliaA;
db = pDoc->sogliaB;
dc = pDoc->sogliaC;
dd = pDoc->sogliaD;
rd = pDoc->c_sogliaPer;
s.Format(" A=%3.0lf-%3.0lf B=%3.0lf-%3.0lf C=%3.0lf-%3.0lf D>%3.0lf RD=%3.0lf",
		 da,db,db,dc,dc,dd,dd,rd);
ps->Print(pTable10,0,1,9,TEXT_LEFT,(LPCSTR)s);

s = "   ";
s += "Inspection Table ";
ps->Print(pTable10,1,0,10,TEXT_LEFT,(LPCSTR)s);
s.Format(" %s ",pDoc->nomeRicetta);
ps->Print(pTable10,1,1,10,TEXT_LEFT,(LPCSTR)s);

// da qui
// calcolo numero vero di colonne
int ncol = 5; // vedi colonne fisse
ncol += (c_reportClass[0])?2:0;	// 1 difetti + 1 densita`
ncol += (c_reportClass[1])?2:0;	// 1 difetti + 1 densita`
ncol += (c_reportClass[2])?2:0;	// 1 difetti + 1 densita`
ncol += (c_reportClass[3])?2:0;	// 1 difetti + 1 densita`
ncol += (c_reportClass[4])?1:0;	// 1 difetti


// Tabella dettaglio prima pagina
TABLEHEADER* pTable12 = NULL;        
pTable12=new TABLEHEADER;  
pTable12->SetSkip=FALSE;	// no fill 
pTable12->PointSize=9;
pTable12->LineSize=1;    // default shown only for demp purposes
pTable12->NumPrintLines=1;
pTable12->UseInches=TRUE;
pTable12->AutoSize=TRUE;
pTable12->Border=TRUE;
pTable12->VLines = TRUE;	//	true draw vertical seperator lines
pTable12->HLines = TRUE;    // ditto on horizontal lines
pTable12->FillFlag=FILL_COLOR;
pTable12->NumColumns=ncol; // aggiungere colonne
pTable12->NumRows = numSeg+1+1; // aggiunta totale colonne di controllo
pTable12->StartRow= pTable10->EndRow+0.05;
pTable12->StartCol=bLeft;
pTable12->EndCol=bRight;
pTable12->HeaderLines=3;
pTable12->NoHeader = FALSE;

// index
int i = 0;
pTable12->ColDesc[i++].Text = "S";
pTable12->ColDesc[i++].Text = " Int.[m] ";
if(c_reportClass[0]) pTable12->ColDesc[i++].Text = "Cls A";
if(c_reportClass[1]) pTable12->ColDesc[i++].Text = "Cls B";
if(c_reportClass[2]) pTable12->ColDesc[i++].Text = "Cls C";
if(c_reportClass[3]) pTable12->ColDesc[i++].Text = "Cls D";
pTable12->ColDesc[i++].Text = "Cls RD";
if(c_reportClass[4]) pTable12->ColDesc[i++].Text = "Cls Big";
// intestazione ABCD
CString abcdH;
if(c_reportClass[0]) abcdH += "A";
if(c_reportClass[1]) abcdH += "B";
if(c_reportClass[2]) abcdH += "C";
if(c_reportClass[3]) abcdH += "D";
pTable12->ColDesc[i].Text = "Cls ";
pTable12->ColDesc[i++].Text += abcdH; 

if(c_reportClass[0])pTable12->ColDesc[i++].Text = "Dens. A [h/m2]";
if(c_reportClass[1])pTable12->ColDesc[i++].Text = "Dens. B [h/m2]";
if(c_reportClass[2])pTable12->ColDesc[i++].Text = "Dens. C [h/m2]";
if(c_reportClass[3])pTable12->ColDesc[i++].Text = "Dens. D [h/m2]";

pTable12->ColDesc[i].Text = "Dens.";
pTable12->ColDesc[i].Text += abcdH;
pTable12->ColDesc[i].Text += " [h/m2]";


for (i=0;i<14;i++)
	{
	// flag fill background for the col
	pTable12->ColDesc[i].FillFlag = FILL_COLOR;
	// set default background color 
	pTable12->ColDesc[i].FillColor = RGB(255,255,255);
	// set size
	if (i == 0)
		pTable12->ColDesc[i].Width = 0.2;
	if (i == 1)
		pTable12->ColDesc[i].Width = 0.8;
	}

ps->setRealPrint(TRUE);
ps->Table(pTable12);

/* solo per test
double posVect[100];
for (int i = 0;i<pDoc->c_difCoil.GetSize();i++)
	posVect[i] = pDoc->c_difCoil[i].posizione;
*/
	
BOOL laminDef=FALSE;
int vA,vB,vC,vD,vAB,vRD,vBig;
double dA,dB,dC,dD;
int tvA,tvB,tvC,tvD,tvAB,tvRD,tvBig;
double tdA,tdB,tdC,tdD;
for (int i=0;i<=(numSeg+1);i++)
	{
	// "Sezione Da Metri A Metri Classe A ClasseB Classe C Classe D Totale
	laminDef = FALSE;
	CString sItem;
	if(i == 0)
		{
		tvA = (vA = (c_reportClass[0])?(int)pDoc->c_difCoil.getTotDifetti('A',0,seg[i]):0);
		tvB = (vB = (c_reportClass[1])?(int)pDoc->c_difCoil.getTotDifetti('B',0,seg[i]):0);
		tvC = (vC = (c_reportClass[2])?(int)pDoc->c_difCoil.getTotDifetti('C',0,seg[i]):0);
		tvD = (vD = (c_reportClass[3])?(int)pDoc->c_difCoil.getTotDifetti('D',0,seg[i]):0);
		// tvAB = (vAB = (int)pDoc->c_difCoil.getTotDifetti(0,seg[i]));
		tvAB = (vAB = tvA + tvB + tvC + tvD);
		tvRD = (vRD = (int)pDoc->c_difCoil.getTotPerB(0,seg[i]));
		tvBig = (vBig = (c_reportClass[4])?(int)pDoc->c_difCoil.getTotBigHole(0,seg[i]):0);
		tdA = (dA = (c_reportClass[0])?pDoc->c_difCoil.getTotalDensity('A',0,seg[i],pDoc->c_larghezza):0);
		tdB = (dB = (c_reportClass[1])?pDoc->c_difCoil.getTotalDensity('B',0,seg[i],pDoc->c_larghezza):0);
		tdC = (dC = (c_reportClass[2])?pDoc->c_difCoil.getTotalDensity('C',0,seg[i],pDoc->c_larghezza):0);
		tdD = (dD = (c_reportClass[3])?pDoc->c_difCoil.getTotalDensity('D',0,seg[i],pDoc->c_larghezza):0);
		sItem.Format("%4.0lf - %4.0lf ",0.,seg[i]); 
		// check difetti di laminazione
		CRect subNastro;
		// in left e right ci sono invece di xLeft e xRight
		// leftCell e rightCell
		int elemento = pDoc->c_rotoloMapping.c_lastElemento;
		int numStrip = pDoc->c_rotoloMapping.at(elemento).size();
		for (int j=0;j<numStrip;j++)
			{
			CRect subNastro = getRectStrip(elemento,0,j);
			laminDef = pDoc->c_difCoil.existLamindef(0,seg[i],subNastro.left,subNastro.right);
			if (laminDef)
				break;
			}
		}
	else
		{
		if (i == numSeg)
			{
			tvA += (vA = (c_reportClass[0])?(int)pDoc->c_difCoil.getTotDifetti('A',(seg[i-1]/10)*10,pDoc->getVTotalLength()):0);
			tvB += (vB = (c_reportClass[1])?(int)pDoc->c_difCoil.getTotDifetti('B',seg[i-1],pDoc->getVTotalLength()):0);
			tvC += (vC = (c_reportClass[2])?(int)pDoc->c_difCoil.getTotDifetti('C',seg[i-1],pDoc->getVTotalLength()):0);
			tvD += (vD = (c_reportClass[3])?(int)pDoc->c_difCoil.getTotDifetti('D',seg[i-1],pDoc->getVTotalLength()):0);
			// tvAB  += (vAB = (int)pDoc->c_difCoil.getTotDifetti(seg[i-1],pDoc->getVTotalLength()));
			tvAB  += (vAB = vA + vB + vC +vD);
			tvRD  += (vRD = (int)pDoc->c_difCoil.getTotPerB(seg[i-1],pDoc->getVTotalLength()));
			tvBig += (vBig = (int)pDoc->c_difCoil.getTotBigHole(seg[i-1],pDoc->getVTotalLength()));
			tdA += (dA = (c_reportClass[0])?pDoc->c_difCoil.getTotalDensity('A',seg[i-1],pDoc->getVTotalLength(),pDoc->c_larghezza):0);
			tdB += (dB = (c_reportClass[1])?pDoc->c_difCoil.getTotalDensity('B',seg[i-1],pDoc->getVTotalLength(),pDoc->c_larghezza):0);
			tdC += (dC = (c_reportClass[2])?pDoc->c_difCoil.getTotalDensity('C',seg[i-1],pDoc->getVTotalLength(),pDoc->c_larghezza):0);
			tdD += (dD = (c_reportClass[3])?pDoc->c_difCoil.getTotalDensity('D',seg[i-1],pDoc->getVTotalLength(),pDoc->c_larghezza):0);
			sItem.Format("%4.0lf : %4.0lf ",seg[i-1],(double)pDoc->getVTotalLength());

			CRect subNastro;
			// in left e right ci sono invece di xLeft e xRight
			// leftCell e rightCell
			int elemento = pDoc->c_rotoloMapping.c_lastElemento;
			int numStrip = pDoc->c_rotoloMapping.at(elemento).size();
			for (int j=0;j<numStrip;j++)
				{
				CRect subNastro = getRectStrip(elemento,0,j);
				laminDef = pDoc->c_difCoil.existLamindef((int)seg[i-1],(int)pDoc->getVTotalLength(),subNastro.left,subNastro.right);
				if (laminDef)
					break;
				}
			}
		else
			{
			if (i == numSeg+1)
				{
				vA = tvA;
				vB = tvB;
				vC = tvC;
				vD = tvD;
				vAB = tvAB;
				vRD = tvRD;
				vBig = tvBig;
				// 01-02-2010 Ridefinito calcolo densita` complessiva !!!
				dA = (c_reportClass[0])?pDoc->c_difCoil.getTotalDensity('A',0,pDoc->getVTotalLength(),pDoc->c_larghezza):0;
				dB = (c_reportClass[1])?pDoc->c_difCoil.getTotalDensity('B',0,pDoc->getVTotalLength(),pDoc->c_larghezza):0;
				dC = (c_reportClass[2])?pDoc->c_difCoil.getTotalDensity('C',0,pDoc->getVTotalLength(),pDoc->c_larghezza):0;
				dD = (c_reportClass[3])?pDoc->c_difCoil.getTotalDensity('D',0,pDoc->getVTotalLength(),pDoc->c_larghezza):0;

				//dA = tdA/(numSeg+1);
				//dB = tdB/(numSeg+1);
				//dC = tdC/(numSeg+1);
				//dD = tdD/(numSeg+1);
				sItem.Format("%4.0lf : %4.0lf ",
					0.,(double)pDoc->getVTotalLength()); 
				}
			else
				{	
				tvA += (vA = (c_reportClass[0])?(int)pDoc->c_difCoil.getTotDifetti('A',seg[i-1],seg[i]):0);
				tvB += (vB = (c_reportClass[1])?(int)pDoc->c_difCoil.getTotDifetti('B',seg[i-1],seg[i]):0);
				tvC += (vC = (c_reportClass[2])?(int)pDoc->c_difCoil.getTotDifetti('C',seg[i-1],seg[i]):0);
				tvD += (vD = (c_reportClass[3])?(int)pDoc->c_difCoil.getTotDifetti('D',seg[i-1],seg[i]):0);
				// tvAB += (vAB = (int)pDoc->c_difCoil.getTotDifetti(seg[i-1],seg[i]));
				tvAB += (vAB = vA + vB + vC + vD);
				tvRD += (vRD = (int)pDoc->c_difCoil.getTotPerB(seg[i-1],seg[i]));
				tvBig += (vBig = (c_reportClass[3])?(int)pDoc->c_difCoil.getTotBigHole(seg[i-1],seg[i]):0);
				tdA += (dA = (c_reportClass[0])?pDoc->c_difCoil.getTotalDensity('A',seg[i-1],seg[i],pDoc->c_larghezza):0);
				tdB += (dB = (c_reportClass[1])?pDoc->c_difCoil.getTotalDensity('B',seg[i-1],seg[i],pDoc->c_larghezza):0);
				tdC += (dC = (c_reportClass[2])?pDoc->c_difCoil.getTotalDensity('C',seg[i-1],seg[i],pDoc->c_larghezza):0);
				tdD += (dD = (c_reportClass[3])?pDoc->c_difCoil.getTotalDensity('D',seg[i-1],seg[i],pDoc->c_larghezza):0);
				sItem.Format("%4.0lf : %4.0lf ",seg[i-1],seg[i]); 
				CRect subNastro;
				// in left e right ci sono invece di xLeft e xRight
				// leftCell e rightCell
				int elemento = pDoc->c_rotoloMapping.c_lastElemento;
				int numStrip = pDoc->c_rotoloMapping.at(elemento).size();
				for (int j=0;j<numStrip;j++)
					{
					CRect subNastro = getRectStrip(elemento,0,j);
					laminDef = pDoc->c_difCoil.existLamindef(seg[i-1],seg[i],subNastro.left,subNastro.right);
					if (laminDef)
						break;
					}
				}
			}
		}

	int rowPos = i;
	int colPos=0;
	CString s;
	// Numerazione sezioni
	s.Format("%d",i+1);
	if (i == numSeg+1)
		{
		pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
		// no seg
		s = "";
		}
	// coloro sfondo 
	if (laminDef)
		pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_RED);
	ps->Print(pTable12,rowPos,colPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
	colPos++;
	// intervallo di riferimento
	s = "";
	s += sItem;
	if (i == numSeg+1)
		{
		pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
		}
	else
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);

	// Class A
	if (c_reportClass[0])
		{
		sItem.Format("%d",vA);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Class B
	if (c_reportClass[1])
		{
		sItem.Format("%d",vB);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Class C
	if (c_reportClass[2])
		{
		sItem.Format("%d",vC);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Class D
	if (c_reportClass[3])
		{
		sItem.Format("%d",vD);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Class RB
	sItem.Format("%d",vRD);
	s = "";
	s += sItem;
	if (i == numSeg+1)
		{
		pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
		}
	else
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
	// Class BigHole
	if (c_reportClass[4])
		{
		sItem.Format("%d",vBig);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Class A+B+C
	sItem.Format("%d",vAB);
	s = "";
	s += sItem;
	if (i == numSeg+1)
		{
		pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
		}
	else
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
	// Densita A
	if (c_reportClass[0])
		{
		sItem.Format("%7.05lf",dA);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Densita B
	if (c_reportClass[1])
		{
		sItem.Format("%7.05lf",dB);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Densita C
	if (c_reportClass[2])
		{
		sItem.Format("%7.05lf",dC);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Densita D
	if (c_reportClass[3])
		{
		sItem.Format("%7.05lf",dD);
		s = "";
		s += sItem;
		if (i == numSeg+1)
			{
			pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
			}
		else
			ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
		}
	// Densita AB
	sItem.Format("%7.05lf",dA+dB+dC+dD);
	s = "";
	s += sItem;
	if (i == numSeg+1)
		{
		pTable12->pClsTable->FillCell(rowPos,colPos,COLOR_GRAY);
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)s);
		}
	else
		ps->Print(pTable12,rowPos,colPos++,8,TEXT_CENTER,(LPCSTR)s);
	}

//------------------------------------------------------------
// a qui

delete pTable1;
delete pTable2;
delete pTable3;
delete pTable4;
delete pTable5;
delete pTable6;
delete pTable7;
delete pTable8;
delete pTable8a;
delete pTable10;
delete pTable12;
delete ps;

pDC->RestoreDC(-1);
}


// memo last index 
// utilizzate sia da reportStrip sia da report bobine
int lastI,lastJ,lastK;
#define REPSTRIP_CLASS_X_ROW 3

long CInit::reportStrip(CDC* pDC,int page,CRect rcBounds,BOOL doPrint)
{
DOC_CLASS* pDoc = (DOC_CLASS *) GetDoc();
// TODO: add draw code here
pDC->SaveDC();


// provo con CPage
CPage*	ps= new CPage(rcBounds,pDC,FALSE,MM_TEXT);

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

if (page == 1)
	{
	// indice difetto attualmente in estrazione
	c_curExtractingDefect = 0;
	c_printedRow = 0;

	TABLEHEADER* pTable1 = NULL;        
	pTable1=new TABLEHEADER;  
	pTable1->SetSkip=TRUE;	// no fill 
	pTable1->PointSize=24;
	pTable1->LineSize=1;    // default shown only for demp purposes
	pTable1->NumPrintLines=1;
	pTable1->UseInches=TRUE;
	pTable1->AutoSize=FALSE;
	pTable1->Border=TRUE;
	pTable1->FillFlag=FILL_NONE;
	pTable1->NumColumns=1;
	pTable1->NumRows = 0;
	pTable1->StartRow=bTop;
	pTable1->StartCol=bLeft;
	pTable1->EndCol=bRight;
	pTable1->HeaderLines=1;
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
	pTable1->ColDesc[0].Init((bRight-bLeft),"QUALITY CONTROL REPORT STRIP",FILL_NONE);
	ps->setRealPrint(TRUE);
	ps->Table(pTable1);	

	TABLEHEADER* pTable2 = NULL;        
	pTable2=new TABLEHEADER;  
	pTable2->SetSkip=TRUE;	// no fill 
	pTable2->PointSize=16;
	pTable2->LineSize=1;    // default shown only for demp purposes
	pTable2->NumPrintLines=1;
	pTable2->UseInches=TRUE;
	pTable2->AutoSize=TRUE;
	pTable2->Border=TRUE;
	pTable2->VLines = FALSE;	//	true draw vertical seperator lines
	pTable2->HLines = TRUE;    // ditto on horizontal lines
	pTable2->FillFlag=FILL_NONE;
	pTable2->NumColumns=2;
	pTable2->NumRows = 1;
	pTable2->StartRow=pTable1->EndRow+0.1;
	pTable2->StartCol=bLeft;
	pTable2->EndCol=bRight;
	pTable2->NoHeader=TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable2);
	// righe
	CString str;
	// strip
	str.LoadString (CSM_GRAPHVIEW_STRIP);
	ps->Print(pTable2,0,0,16,TEXT_LEFT,(LPCSTR)str);
	str = ": ";
	str += pDoc->getStripRotolo(c_prStripNumber,c_prAlzataNumber,c_prElementNumber);
	ps->Print(pTable2,0,1,16,TEXT_LEFT,(LPCSTR)str);

	TABLEHEADER* pTable3 = NULL;        
	pTable3=new TABLEHEADER;  
	pTable3->SetSkip=TRUE;	// no fill 
	pTable3->PointSize=14;
	pTable3->LineSize=1;    // default shown only for demp purposes
	pTable3->NumPrintLines=1;
	pTable3->UseInches=TRUE;
	pTable3->AutoSize=TRUE;
	pTable3->Border=TRUE;
	pTable3->VLines = FALSE;	//	true draw vertical seperator lines
	pTable3->HLines = TRUE;    // ditto on horizontal lines
	pTable3->FillFlag=FILL_NONE;
	pTable3->NumColumns=2;
	pTable3->NumRows = 1;
	pTable3->StartRow= pTable2->EndRow+0.1;
	pTable3->StartCol=bLeft;
	pTable3->EndCol=bRight;
	pTable3->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable3);	

	// elenco celle per nastro
	// Offset sx  
	CRect subNastro;
	// in left e right ci sono invece di xLeft e xRight
	// leftCell e rightCell
	subNastro = getRectStrip(c_prElementNumber,c_prAlzataNumber,c_prStripNumber);
	double difA,difB,difC,difD,difBig,difRb;
	difA = pDoc->c_difCoil.getTotDifetti('A',subNastro.top,subNastro.bottom,
				subNastro.left,subNastro.right);
	difB = pDoc->c_difCoil.getTotDifetti('B',subNastro.top,subNastro.bottom,
				subNastro.left,subNastro.right);
	difC = pDoc->c_difCoil.getTotDifetti('C',subNastro.top,subNastro.bottom,
				subNastro.left,subNastro.right);
	difD = pDoc->c_difCoil.getTotDifetti('D',subNastro.top,subNastro.bottom,
				subNastro.left,subNastro.right);
	difBig = pDoc->c_difCoil.getTotBigHole(subNastro.top,subNastro.bottom,
				subNastro.left,subNastro.right);
	difRb = pDoc->c_difCoil.getTotPerB(subNastro.top,subNastro.bottom,
				subNastro.left,subNastro.right);
double densA,densB,densC,densD;
// ??? Posizioni relative a strip sottratto subNastro.top
double sizeBobina = pDoc->c_rotoloMapping.at(c_prElementNumber).at(c_prStripNumber).c_larghezza;

	densA = pDoc->c_difCoil.getTotalDensity('A',subNastro.top,subNastro.bottom,
							subNastro.left,subNastro.right,sizeBobina);
	densB = pDoc->c_difCoil.getTotalDensity('B',subNastro.top,subNastro.bottom,
							subNastro.left,subNastro.right,sizeBobina);
	densC = pDoc->c_difCoil.getTotalDensity('C',subNastro.top,subNastro.bottom,
							subNastro.left,subNastro.right,sizeBobina);
	densD = pDoc->c_difCoil.getTotalDensity('D',subNastro.top,subNastro.bottom,
							subNastro.left,subNastro.right,sizeBobina);

	// getTotaldensity torna -1 se area 0;
	if (densA < 0.)
		densA = 0.;
	if (densB < 0.)
		densB = 0.;
	if (densC < 0.)
		densC = 0.;
	if (densD < 0.)
		densD = 0.;

	// Riga
	// lunghezza
	str.LoadString (CSM_GRAPHVIEW_LUNGHEZZA);
	ps->Print(pTable3,0,0,14,TEXT_LEFT,(LPCSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (": %d",(int)(subNastro.bottom-subNastro.top));
	ps->Print(pTable3,0,1,14,TEXT_LEFT,(LPCSTR)str);
		
	TABLEHEADER* pTable4 = NULL;        
	pTable4=new TABLEHEADER;  
	pTable4->SetSkip=TRUE;	// no fill 
	pTable4->PointSize=14;
	pTable4->LineSize=1;    // default shown only for demp purposes
	pTable4->NumPrintLines=1;
	pTable4->UseInches=TRUE;
	pTable4->AutoSize=TRUE;
	pTable4->Border=TRUE;
	pTable4->VLines = FALSE;	//	true draw vertical seperator lines
	pTable4->HLines = TRUE;    // ditto on horizontal lines
	pTable4->FillFlag=FILL_NONE;
	pTable4->NumColumns=2;
	pTable4->NumRows = 1;
	pTable4->StartRow=pTable3->EndRow+0.1;
	pTable4->StartCol=bLeft;
	pTable4->EndCol=bRight;
	pTable4->NoHeader=TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable4);
	// data
	CTime t (pDoc->c_startTime);
	// str = "DATE          : " + t.Format("%d-%m-%Y  %H:%M:%S");
	str.LoadString(CSM_GRAPHVIEW_DATE);
	ps->Print(pTable4,0,0,14,TEXT_LEFT,(LPCSTR)str);
	// format data
	CString formatRes;
	formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
	str = ": ";
	str += t.Format(formatRes);
	ps->Print(pTable4,0,1,14,TEXT_LEFT,(LPCSTR)str);


		// Thickness
	TABLEHEADER* pTable40 = NULL;        
	pTable40=new TABLEHEADER;  
	pTable40->SetSkip=TRUE;	// no fill 
	pTable40->PointSize=14;
	pTable40->LineSize=1;    // default shown only for demp purposes
	pTable40->NumPrintLines=1;
	pTable40->UseInches=TRUE;
	pTable40->AutoSize=TRUE;
	pTable40->Border=TRUE;
	pTable40->VLines = FALSE;	//	true draw vertical seperator lines
	pTable40->HLines = TRUE;    // ditto on horizontal lines
	pTable40->FillFlag=FILL_NONE;
	pTable40->NumColumns=2;
	pTable40->NumRows = 1;
	pTable40->StartRow= pTable4->EndRow+0.1;
	pTable40->StartCol=bLeft;
	pTable40->EndCol=bRight;
	pTable40->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable40);	

	// Spessore
	str.LoadString (CSM_GRAPHVIEW_THICK);
	ps->Print(pTable40,0,0,14,TEXT_LEFT,(LPCSTR)str);
	// ??? Posizioni relative a strip sottratto subNastro.top
	str.Format (": %4.2lf",pDoc->c_spessore);
	ps->Print(pTable40,0,1,14,TEXT_LEFT,(LPCSTR)str);

	// Larghezza 
	TABLEHEADER* pTable60 = NULL;        
	pTable60=new TABLEHEADER;  
	pTable60->SetSkip=TRUE;	// no fill 
	pTable60->PointSize=14;
	pTable60->LineSize=1;    // default shown only for demp purposes
	pTable60->NumPrintLines=1;
	pTable60->UseInches=TRUE;
	pTable60->AutoSize=TRUE;
	pTable60->Border=TRUE;
	pTable60->VLines = FALSE;	//	true draw vertical seperator lines
	pTable60->HLines = TRUE;    // ditto on horizontal lines
	pTable60->FillFlag=FILL_NONE;
	pTable60->NumColumns=2;
	pTable60->NumRows = 1;
	pTable60->StartRow= pTable40->EndRow+0.1;
	pTable60->StartCol=bLeft;
	pTable60->EndCol=bRight;
	pTable60->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable60);	

	// larghezza
	str.LoadString (CSM_GRAPHVIEW_LARGHEZZA);
	ps->Print(pTable60,0,0,14,TEXT_LEFT,(LPCSTR)str);

	str.Format (": %4.0lf", sizeBobina);
	ps->Print(pTable60,0,1,14,TEXT_LEFT,(LPCSTR)str);

	// Cliente 
	TABLEHEADER* pTable70 = NULL;        
	pTable70=new TABLEHEADER;  
	pTable70->SetSkip=TRUE;	// no fill 
	pTable70->PointSize=14;
	pTable70->LineSize=1;    // default shown only for demp purposes
	pTable70->NumPrintLines=1;
	pTable70->UseInches=TRUE;
	pTable70->AutoSize=TRUE;
	pTable70->Border=TRUE;
	pTable70->VLines = FALSE;	//	true draw vertical seperator lines
	pTable70->HLines = TRUE;    // ditto on horizontal lines
	pTable70->FillFlag=FILL_NONE;
	pTable70->NumColumns=2;
	pTable70->NumRows = 1;
	pTable70->StartRow= pTable60->EndRow+0.1;
	pTable70->StartCol=bLeft;
	pTable70->EndCol=bRight;
	pTable70->NoHeader = TRUE;
	ps->setRealPrint(TRUE);
	ps->Table(pTable70);	

	// cliente
	str.LoadString (CSM_GRAPHVIEW_CLIENTE);
	ps->Print(pTable70,0,0,14,TEXT_LEFT,(LPCSTR)str);
	str.Format (": %s",pDoc->c_cliente);
	ps->Print(pTable70,0,1,14,TEXT_LEFT,(LPCSTR)str);


	//-----------------------------------------------------
	// sezione totale difetti dello strip 
	//-------------------------------------------------
	
	TABLEHEADER* pTable5 = NULL;        
	pTable5=new TABLEHEADER;  
	pTable5->SetSkip=TRUE;	// no fill 
	pTable5->PointSize=10;
	pTable5->LineSize=1;    // default shown only for demp purposes
	pTable5->NumPrintLines=1;
	pTable5->UseInches=TRUE;
	pTable5->AutoSize=TRUE;
	pTable5->Border=TRUE;
	pTable5->VLines = TRUE;	//	true draw vertical seperator lines
	pTable5->HLines = TRUE;    // ditto on horizontal lines
	pTable5->FillFlag=FILL_NONE;
	pTable5->NumColumns=2;
	pTable5->NumRows = 4;
	pTable5->StartRow=pTable70->EndRow+0.25;
	pTable5->StartCol=bLeft;
	pTable5->EndCol=bRight;
	pTable5->NoHeader=TRUE;
	ps->setRealPrint(TRUE);
	
	// prima colonna 2.5 inch
	pTable5->ColDesc[0].Width = 2.5;

	
	ps->Table(pTable5);
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda



	// Total A B A+B
	str = "Total Holes ";
	ps->Print(pTable5,0,0,10,TEXT_LEFT,(LPCSTR)str);
	str.Format(" A=%3.0lf B=%3.0lf C=%3.0lf D=%3.0lf ABCD=%3.0lf Big=%3.0lf Rd=%3.0lf",
					difA,difB,difC,difD,
					difA+difB+difC+difD,
					difBig,difRb);
	ps->Print(pTable5,0,1,9,TEXT_LEFT,(LPCSTR)str);

	// Total Density A B A+B
	str = "Total Density Class [holes/m2]";
	ps->Print(pTable5,1,0,9,TEXT_LEFT,(LPCSTR)str);
	str.Format(" A=%6.4lf B=%6.4lf C=%6.4lf D=%6.4lf  ABCD=%6.4lf",
					densA,densB,densC,densD,
					densA+densB+densC+densD);
	ps->Print(pTable5,1,1,10,TEXT_LEFT,(LPCSTR)str);

	// Threshold A B 
	str = "Threshold [um] ";
	ps->Print(pTable5,2,0,9,TEXT_LEFT,(LPCSTR)str);
	double da,db,dc,dd;;
	da = pDoc->sogliaA;
	db = pDoc->sogliaB;
	dc = pDoc->sogliaC;
	dd = pDoc->sogliaD;
	str.Format(" A=%3.0lf-%3.0lf B=%3.0lf-%3.0lf C=%3.0lf-%3.0lf D>%3.0lf",
		 da,db,db,dc,dc,dd,dd);
	ps->Print(pTable5,2,1,9,TEXT_LEFT,(LPCSTR)str);

	// Alarm Threshold A,B.Rb 
	str = "Inspection Table - Alarm Threshold";
	ps->Print(pTable5,3,0,10,TEXT_LEFT,(LPCSTR)str);
	str.Format(" %s - A=%3.0lf B=%3.0lf C=%3.0lf D=%3.0lf Rd=%3.0lf",
		pDoc->nomeRicetta,
		(double)pDoc->c_difCoil.c_sogliaAllarme[0],
		(double)pDoc->c_difCoil.c_sogliaAllarme[1],
		(double)pDoc->c_difCoil.c_sogliaAllarme[2],
		(double)pDoc->c_difCoil.c_sogliaAllarme[3],
		(double)pDoc->c_sogliaPer);
	ps->Print(pTable5,3,1,9,TEXT_LEFT,(LPCSTR)str);

	bTop = pTable5->EndRow+0.25;
	delete pTable1;
	delete pTable2;
	delete pTable3;
	delete pTable4;
	delete pTable5;
	delete pTable40;
	delete pTable60;
	delete pTable70;
	}
	
CRect subNastro;
// in left e right ci sono invece di xLeft e xRight
// leftCell e rightCell
subNastro = getRectStrip(c_prElementNumber,c_prAlzataNumber,c_prStripNumber);
	
// Ok calcolaliamo quante righe possiamo mettere
TABLEHEADER* pTable1=new TABLEHEADER;        
pTable1->PointSize=(REPSTRIP_CLASS_X_ROW<=3)?10:9;;
pTable1->LineSize=1;    // default shown only for demp purposes
pTable1->NumPrintLines=1;
pTable1->UseInches=TRUE;
pTable1->AutoSize=FALSE;
pTable1->Border=TRUE;
pTable1->FillFlag=FILL_NONE;
pTable1->NumColumns=REPSTRIP_CLASS_X_ROW*3;
pTable1->NumRows = 1;
pTable1->StartRow=0.0;
pTable1->StartCol=0.0;
pTable1->EndCol=bRight;
pTable1->HeaderLines=0;

ps->setRealPrint(FALSE);
ps->Table(pTable1);	
ps->setRealPrint(TRUE);
double sizeOneRow =pTable1->EndRow;

double hSize = bBottom - bTop;
// Numero righe per pagina ( tolgo intestazione )
int numRowXPage = (int)(hSize/sizeOneRow);
// due vanno per header
numRowXPage	-= 2;

// calcolo righe gia` stampate
int printedRow = 0;
if (c_rowPerPage.GetSize()>0)
	{// solo da pagina 2 in poi 
	for (int i=0;i<page-1;i++)
		printedRow += c_rowPerPage[i];
	}


//int rTmp = pDoc->c_difCoil.getNumClassIndex(subNastro.top,subNastro.bottom,
//			   subNastro.left,subNastro.right)/3;
//numRowXPage = min(numRowXPage,(1+rTmp)-printedRow);

// FixBug: inserito ceil per arrotondamento non troncamento!
// FixBug: aggiunta una riga ogni 3 pagine perche` in media perdiamo una cella ogni pagina.
// al piu` una riga vuota in fondo a report 
//numRowXPage = min(numRowXPage,
//			   (1+ceil((double)pDoc->c_difCoil.getNumClassIndex(subNastro.top,subNastro.bottom,
//			   subNastro.left,subNastro.right)/REPSTRIP_CLASS_X_ROW
//			   +(page-1)/REPSTRIP_CLASS_X_ROW-printedRow));
// qs e` il calcolo giusto!
int numLineDif = pDoc->c_difCoil.getNumClassIndex(subNastro.top,subNastro.bottom,
			   subNastro.left,subNastro.right);
numRowXPage = min(numRowXPage,((numLineDif / REPSTRIP_CLASS_X_ROW) + ((numLineDif % REPSTRIP_CLASS_X_ROW)?1:0) - printedRow));



if (!doPrint)
	{
	delete pTable1;
	delete ps;
	return(numRowXPage);
	}

TABLEHEADER* pTable2 = NULL;        
pTable2=new TABLEHEADER;        
pTable2->PointSize=(REPSTRIP_CLASS_X_ROW<=3)?10:9;
pTable2->LineSize=1;    // default shown only for demp purposes
pTable2->NumPrintLines=1;
pTable2->UseInches=TRUE;
pTable2->AutoSize=FALSE;
pTable2->Border=TRUE;
pTable2->FillFlag=FILL_LTGRAY;
pTable2->NumColumns=3*REPSTRIP_CLASS_X_ROW;
pTable2->NumRows = c_rowPerPage[page-1];

//pTable2->NumRows = min(c_rowPerPage[page-1],
//					   (1+(pDoc->c_difCoil.getNumIndex(subNastro.top,subNastro.bottom,
//					   subNastro.left,subNastro.right)/3)-printedRow));

pTable2->StartRow=bTop;
pTable2->StartCol=bLeft;
pTable2->EndCol=bRight;
pTable2->HeaderLines=3;
// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
for (int i=0;i<REPSTRIP_CLASS_X_ROW;i++)
	{
	double cs = (bRight-bLeft)/REPSTRIP_CLASS_X_ROW;
	// 42% col metri
	pTable2->ColDesc[0+i*3].Init(0.42*cs,"Meter",FILL_LTGRAY);
	pTable2->ColDesc[1+i*3].Init(0.29*cs,"Holes Num",FILL_NONE);
	pTable2->ColDesc[2+i*3].Init(0.29*cs,"Class",FILL_NONE);
	}

//pTable2->ColDesc[0].Init(0.9,"Meter",FILL_LTGRAY);
//pTable2->ColDesc[1].Init(0.6,"Holes Num",FILL_NONE);
//pTable2->ColDesc[2].Init(0.6,"Class",FILL_NONE);
//pTable2->ColDesc[3].Init(0.9,"Meter",FILL_LTGRAY);
//pTable2->ColDesc[4].Init(0.6,"Holes Num",FILL_NONE);
//pTable2->ColDesc[5].Init(0.6,"Class",FILL_NONE);
//pTable2->ColDesc[6].Init(0.9,"Meter",FILL_LTGRAY);
//pTable2->ColDesc[7].Init(0.6,"Holes Num",FILL_NONE);
//pTable2->ColDesc[8].Init(0.6,"Class",FILL_NONE);
//pTable2->ColDesc[9].Init(0.7,"Meter",FILL_LTGRAY);
//pTable2->ColDesc[10].Init(0.5,"Holes",FILL_NONE);
//pTable2->ColDesc[11].Init(0.5,"Class",FILL_NONE);
ps->setRealPrint(TRUE);
ps->Table(pTable2);	
// Aggiunta dettaglio difetti per ogni decaMetro difettoso indico
CString sItem;
int ColPos = 0;
int RowPos = 0;
int curRow = 0;
int curDif = 0;
// colonna base, devo riempire prima prima colonna completa poi seconda etc
int colBase = 0;
int difRowToPrint = c_rowPerPage[page-1] ; // intestazione!!
long extractedDef = 0;
for (int j=0;j<page-1;j++)
	// 21-3-14 fixBug stampa pagine singole !
	extractedDef += c_rowPerPage[j]*REPSTRIP_CLASS_X_ROW;
	// extractedDef += c_extractDefPerPage[j];

// int metriEstratti=0;
int i=0;
for (i=0;i<	pDoc->c_difCoil.GetSize();i++)
	{
	double pos;
	// Posizione a posteriori (valore 10 indica da 0 a 10)
	pos =  pDoc->c_difCoil[i].posizione; // posizione del metro dif
	// NOT trepassing end of strip
	if (pos > subNastro.bottom)
		// break; 
		continue;
	// before ? start of strip?
	if (pos < subNastro.top)
		{
		continue;
		}
	// fine pagina 11-09-07 fix bug era > ora >=
	// ora si riempie per colonne!
	//if (RowPos >= c_rowPerPage[page-1])
	//	break;

	// si ragiona per metri. se un metro si sdoppia tra due pagine manca 
	// parte restante su seconda pagina
	if ((RowPos == (c_rowPerPage[page-1]-1))
		&&
		((pTable2->NumColumns-ColPos)<REPSTRIP_CLASS_X_ROW))
		{
		int a = c_rowPerPage[page-1];
		break;
		}

//	if (i<extractedDef)
//		continue;

	// metriEstratti++;
	// nel metro ogni scheda
	//for (int j=0;j<pDoc->c_difCoil[i].GetSize();j++)
	int j = 0;
		{// Schede fuori finestra ispezione NON gestite
		// Ogni classe
		if (pDoc->c_difCoil[i].GetSize() <= 0)
			continue;
		int v = (long)pDoc->c_difCoil[i].getValBigHole(subNastro.left,subNastro.right);
		if (v > 0)
			{// Fine pagina riparto dall'inizio
			if (RowPos >= difRowToPrint)
				{
				if (colBase < REPSTRIP_CLASS_X_ROW*3)
					// ancora una colonna?
					colBase += 3;
				else
					break;
				RowPos = 0;
				}
			ColPos = colBase;	
			// 02-04-08 mancava if -> pagine tutte uguali
			// 19-03-14
			if (curDif >= (extractedDef))
				{
				// ??? Posizioni relative a strip sottratto subNastro.top
				sItem.Format ("%7.1lf",pos-subNastro.top);
				ps->Print(pTable2,RowPos,ColPos++,11,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
				/*
				if (ColPos >= pTable2->NumColumns)
					{
					RowPos++;
					ColPos = 0;
					}
				*/
				sItem.Format ("%3d",v);
				ps->Print(pTable2,RowPos,ColPos++,11,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
				/*
				if (ColPos >= pTable2->NumColumns)
					{
					RowPos++;
					ColPos = 0;
					}
				*/
				sItem.Format ("%s","Big");
				ps->Print(pTable2,RowPos,ColPos++,11,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
				/*
				if (ColPos >= pTable2->NumColumns)
					{
					RowPos++;
					ColPos = 0;
					}		
				*/
				// No info on banda
				/*
				sItem.Format ("%3d",pDoc->c_difCoil[i].ElementAt(j).banda+1);
				ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
				if (ColPos >= pTable2->NumColumns)
				*/
					{
					// c_printedRow++;
					RowPos++;
					ColPos = colBase;
					}
				}
			// 19-03-14
			curDif++;
			}	

		for (int k=0;k<pDoc->c_difCoil[i].ElementAt(j).GetSize();k++)
			{
			int v = (long)pDoc->c_difCoil[i].getValNum(subNastro.left,subNastro.right,'A'+k);
			int fontSize = 8;
			// k = classe
			if (k == 3)
				// tipo D 
				fontSize = 11;

			if (v > 0)
				{// Bande cominciano da 1 non da zero
				if (RowPos >= difRowToPrint)
					{
					if (colBase < REPSTRIP_CLASS_X_ROW*3)
						colBase += 3;
					else
						break;
					RowPos = 0;
					}
				ColPos = colBase;
				// 19-03-14
				if (curDif >= (extractedDef))
					{
					// ??? Posizioni relative a strip sottratto subNastro.top
					sItem.Format ("%7.1lf",pos-subNastro.top);
					ps->Print(pTable2,RowPos,ColPos++,fontSize,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
	//					c_printedRow++;
						RowPos++;
						ColPos = 0;
						}
					*/
					sItem.Format ("%3d",v);
					int r = (long)pDoc->c_difCoil[i].getValPerB(subNastro.left,subNastro.right);
					if (r && (k == 3))
						{
						//CString s1;
						//s1.Format("%d",r);
						//sItem += " R";
						//sItem += s1;
						sItem += "Rd";
						}
					ps->Print(pTable2,RowPos,ColPos++,fontSize,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
	//					c_printedRow++;
						RowPos++;
						ColPos = 0;
						}
					*/
	//				if (k == 3)
						sItem.Format ("%c",'A'+k);
	//				else
	//					sItem.Format ("%c",'a'+k);
					ps->Print(pTable2,RowPos,ColPos++,fontSize,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
	//					c_printedRow++;
						RowPos++;
						ColPos = 0;
						}
					*/
					// No info on banda
					/*
					sItem.Format ("%3d",pDoc->c_difCoil[i].ElementAt(j).banda+1);
					ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					if (ColPos >= pTable2->NumColumns)
					*/

						{
						//c_printedRow++;
						RowPos++;
						ColPos = colBase;
						}
					}
				// 19-03-14
				curDif++;
				}
			}
		}
	}	
//if (page <= c_extractDefPerPage.GetUpperBound())
	//c_extractDefPerPage[page-1] = metriEstratti;
// 19-03-14 sostituito i con curdif
c_extractDefPerPage[page-1] = curDif;

delete pTable1;
delete pTable2;
delete ps;
return(numRowXPage);


}


/////////////////////////////////////////////////////////////////////////////
// CInitView diagnostics

#ifdef _DEBUG
void CInitView::AssertValid() const
{
	CView::AssertValid();
}

void CInitView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CInitView message handlers

BOOL CInitView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
if (!CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
	return FALSE;

return (TRUE);
}

void CInitView::OnInitialUpdate() 
{
CPdfPrintView::OnInitialUpdate();

// set pointer pdoc to CInit
setLocalPDoc(GetDocument());

	// TODO: Add your specialized code here and/or call the base class
onInitialUpdate();
}

void CInit::onInitialUpdate() 
{
	
// Load report option
DBReport dbReport(dBase);

if (!dbReport.openSelectDefault ())
	{
	AfxGetMainWnd()->MessageBox ("Error openSelectDefault","dbReport.Open");
	dbReport.Close();
	return;
	}

//  Move Init Data from Db to Dialog
CLineDoc* pDoc = (CLineDoc* )GetDoc();

m_SOGLIE_ALLARME = dbReport.m_SOGLIE_ALLARME;
m_DATI_CLIENTE = dbReport.m_DATI_CLIENTE;
m_DATA_LAVORAZIONE = dbReport.m_DATA_LAVORAZIONE;
m_DESCR_PERIODICI = dbReport.m_DESCR_PERIODICI;
m_DESCR_RANDOM = dbReport.m_DESCR_RANDOM;
m_DESCR_ALZATE = dbReport.m_DESCR_ALZATE;
m_LUNGHEZZA_BOBINA = dbReport.m_LUNGHEZZA_BOBINA;
m_ALLARMISISTEMA = dbReport.m_MAPPA100;
//m_ALMETRO = dbReport.m_ALMETRO;
//m_DALMETRO = dbReport.m_DALMETRO;
m_DALMETRO = 0;
m_ALMETRO = pDoc->getVTotalLength();
m_INTERVALLO[0] = dbReport.m_INTERVALLO1;
m_INTERVALLO[1] = dbReport.m_INTERVALLO2;
m_INTERVALLO[2] = dbReport.m_INTERVALLO3;
m_INTERVALLO[3] = dbReport.m_INTERVALLO4;
m_INTERVALLO[4] = dbReport.m_INTERVALLO5;
m_INTERVALLO[5] = dbReport.m_INTERVALLO6;
m_INTERVALLO[6] = dbReport.m_INTERVALLO7;
m_INTERVALLO[7] = dbReport.m_INTERVALLO8;
m_INTERVALLO[8] = dbReport.m_INTERVALLO9;
m_INTERVALLO[9] = dbReport.m_INTERVALLO10;
m_INTERVALLO[10]= dbReport.m_INTERVALLO11;
m_INTERVALLO[11] = dbReport.m_INTERVALLO12;
m_INTERVALLO[12] = dbReport.m_INTERVALLO13;
m_INTERVALLO[13] = dbReport.m_INTERVALLO14;
m_INTERVALLO[14] = dbReport.m_INTERVALLO15;
m_INTERVALLO[15] = dbReport.m_INTERVALLO16;
m_INTERVALLO[16] = dbReport.m_INTERVALLO17;
m_INTERVALLO[17] = dbReport.m_INTERVALLO18;
m_INTERVALLO[18] = dbReport.m_INTERVALLO19;

// Close DbReport	
dbReport.Close();
return ;	
}

//---------------------------------------------------------
//
//			 STAMPA
//
//---------------------------------------------------------

// Sequenza
// OnPreparePrinting
// DoPreparePrinting

// Loop OnPrint
// OnEndPrinting
// Init device dependent
// prepare textPage in CPage mode
// calcola quante pagine servono
// calcolo quante righe di tabella 16 colonne 4 blocchi
int numLineAlzate = 0;
int numAlzBlock = 0;
int numElementi = 0;

int numLineDif   = 0;
int numLineAl1   = 0;
int numLineAl2   = 0;


// random
int CInit::prepareTextPageEx2(CDC* pDC,CRect r) 
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();

// indice di pagina corrente
TReportPage tPage;
c_reportVector2.RemoveAll();

// calcolo quante righe di tabella 16 colonne 4 blocchi
numLineDif = 0;

if (m_DESCR_RANDOM)
	{// use table
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
	// Aggiunta dettaglio difetti per ogni decaMetro difettoso indico
	CString sItem;
	for (int i=0;i<	pDoc->c_difCoil.GetSize();i++)
		{
		double pos;
		// pos =  (pDoc->c_difCoil.trueSizeEncod/1000.)*i;
		// Posizione a posteriori (valore 10 indica da 0 a 10)
		pos =  pDoc->c_difCoil[i].posizione; // posizione del metro dif
		// NOT trepassing total length
		if (pos > pDoc->getVTotalLength())
			//break;
			continue;
		int numItem = 0;
		// nel metro ogni scheda
		for (int j=0;j<pDoc->c_difCoil[i].GetSize();j++)
			{// Schede fuori finestra ispezione NON gestite
			// Ogni classe
			for (int k=0;k<pDoc->c_difCoil[i].ElementAt(j).GetSize();k++)
				{
				// inclusi difetti class k?
				if (!c_reportClass[k])
					continue;
				int v = pDoc->c_difCoil[i].ElementAt(j).ElementAt(k);
				if (v > 0)
					{// Bande cominciano da 1 non da zero
					numLineDif ++;
					numItem += v;
					}
				}
			// Ogni Big
			int v = (long)pDoc->c_difCoil[i].ElementAt(j).BigHoleAt();
			if (// inclusi difetti class big?
				(c_reportClass[4])&&(v > 0))
				{// Bande cominciano da 1 non da zero
				numLineDif ++;
				numItem += v;
				}				
			// Ogni PerB
			v = (long)pDoc->c_difCoil[i].ElementAt(j).PerBAt();
			if (v > 0)
				{// Bande cominciano da 1 non da zero
				numLineDif ++;
				numItem += v;
				}				
		   	}
		numItem;
		}
	}

// tre dati x ogni riga 
numLineDif = (numLineDif / 3) + ((numLineDif % 3)?1:0);


CPage*	ps= new CPage(r,pDC,FALSE,MM_TEXT);

// calcolo dimensione 1 riga
TABLEHEADER* pTable=new TABLEHEADER;        
pTable->PointSize=10;
pTable->LineSize=1;    // default shown only for demp purposes
pTable->NumPrintLines=1;
pTable->UseInches=TRUE;
pTable->AutoSize=FALSE;
pTable->Border=TRUE;
pTable->FillFlag=FILL_LTGRAY;
pTable->NumColumns=12;
pTable->NumRows = 1;
pTable->StartRow=0.0;
pTable->StartCol=0.0;
pTable->EndCol=ps->SetRightMargin(0.);
pTable->HeaderLines=0;
ps->Table(pTable);	

double dimOneRow=pTable->EndRow;
double bTop,bBottom;
// rect r in arrivo gia` internal rect
// CRect internalRect = calcPrintInternalRect(pDC,r);
CRect internalRect = r;

bTop = ps->ConvertYToInches(internalRect.top);
bBottom = ps->ConvertYToInches(internalRect.bottom);
double sizePageY = bBottom - bTop;
int lineXPage = sizePageY / dimOneRow;

// descrittore pagine difetti random
tPage.c_index = 0;
tPage.c_lineXPage = lineXPage;
int difCount = numLineDif;
while (difCount > 0)
	{
	// 3+1 per intestazione
	tPage.c_numLineHeader = 3;
	tPage.c_numLineDif = (min(difCount,tPage.getFreeLine2()));
	tPage.c_indexLineDif = numLineDif-difCount;
	difCount -= tPage.c_numLineDif;
	// pagina completa -> aggiungo a lista
	if (tPage.getFreeLine2() <= 1)
		{
		c_reportVector2.Add(tPage);
		tPage.c_index ++;
		tPage.reset();
		}
	}



if (tPage.getNumLine2() > 0)
	{
	c_reportVector2.Add(tPage);
	}

// lascio spazio per region

int nPage = c_reportVector2.GetSize();
delete pTable;
delete ps;

// Set print Num Page X text	
return (nPage);
}


// strip
int CInit::prepareTextPageEx3(CDC* pDC,CRect r) 
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();

// indice di pagina corrente
TReportPage tPage;
c_reportVector3.RemoveAll();

// calcolo quante righe di tabella 16 colonne 4 blocchi
numAlzBlock = 0;

// quante righe ogni tabella => config abcd
int nl = 5;
nl += (c_reportClass[0])?2:0;
nl += (c_reportClass[1])?2:0;
nl += (c_reportClass[2])?2:0;
nl += (c_reportClass[3])?2:0;
nl += (c_reportClass[4])?1:0;

CPage*	ps= new CPage(r,pDC,FALSE,MM_TEXT);

// calcolo dimensione 1 riga
TABLEHEADER* pTable=new TABLEHEADER;        
pTable->PointSize=7;
pTable->LineSize=1;    // default shown only for demp purposes
pTable->NumPrintLines=1;
pTable->UseInches=TRUE;
pTable->AutoSize=FALSE;
pTable->Border=TRUE;
pTable->FillFlag=FILL_LTGRAY;
pTable->NumColumns=12;
pTable->NumRows = 1;
pTable->StartRow=0.0;
pTable->StartCol=0.0;
pTable->EndCol=ps->SetRightMargin(0.);
pTable->HeaderLines=0;
ps->Table(pTable);	

double dimOneRow=pTable->EndRow;
double bTop,bBottom;
// rect r in arrivo gia` internal rect
// CRect internalRect = calcPrintInternalRect(pDC,r);
CRect internalRect = r;

bTop = ps->ConvertYToInches(internalRect.top);
bBottom = ps->ConvertYToInches(internalRect.bottom);
double sizePageY = bBottom - bTop;
int lineXPage = sizePageY / dimOneRow;

// descrittore pagine difetti random
tPage.c_index = 0;
tPage.c_lineXPage = lineXPage+1;
tPage.c_numLineHeader = 3;
// descrittore pagine alzate

if (m_DESCR_ALZATE)
	{// use table
	numElementi = pDoc->c_rotoloMapping.size();
	for (int i=0;i<numElementi;i++)
		{// max 10 strip per blocco di alzatadettagli
		int totalCol = pDoc->c_rotoloMapping[i].size();	
		int numBlock = totalCol/10;
		numBlock += (totalCol%10)?1:0;
		// trovo numero ottimale per divisione blocchi
		int colPerBlock = (int) ceil( 1.0 * totalCol / numBlock );

		// per ogni alzata alzate meno quella di chiusura
		// numAlzBlock += (pDoc->c_rotoloMapping[i].getCurAlzNumber(-1)-1)*numBlock;
		// numAlzBlock -= pDoc->c_rotoloMapping[i].c_firstRising*numBlock;
		for (int j=0;j<(pDoc->c_rotoloMapping[i].getNumberOfAlz(-1)-1);j++)
			{
			int colCounter=0;
			for (int k=0;k<numBlock;k++)
				{				
				// ok prepariamo struttura dati della singola tabella	
				tPage.c_alzPage[tPage.c_numAlzStruct].c_alzIndex = j;	// alzata corrente
				tPage.c_alzPage[tPage.c_numAlzStruct].c_elem = i;		// elemento corrente
				tPage.c_alzPage[tPage.c_numAlzStruct].c_numCol = min((totalCol-colCounter),colPerBlock);		// numero colonne di qs blocco			
				tPage.c_alzPage[tPage.c_numAlzStruct].c_numRow = nl;	// numero righe
				tPage.c_alzPage[tPage.c_numAlzStruct].c_colIndex = colCounter; // indice nelle colonne
				tPage.c_alzPage[tPage.c_numAlzStruct].c_isLastBlock = (k==(numBlock-1)?TRUE:FALSE);
				// incremento pos nelle colonne
				colCounter += tPage.c_alzPage[tPage.c_numAlzStruct].c_numCol;
				tPage.c_numAlzStruct ++;
				// almeno uno e non c'e` + posto nella pagina
				if ((tPage.c_numAlzStruct > 0)&&
					(tPage.getFreeLine3() < (nl+1)))
					{
					c_reportVector3.Add(tPage);
					tPage.c_index ++;
					tPage.reset();
					}
 				}
			}
		}
	}

if (tPage.getNumLine3() > 0)
	{
	c_reportVector3.Add(tPage);
	}

// lascio spazio per region

int nPage = c_reportVector3.GetSize();
delete pTable;
delete ps;

// Set print Num Page X text	
return (nPage);
}



int CInit::prepareTextPageEx4(CDC* pDC,CRect r) 
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();

// indice di pagina corrente
TReportPage tPage;
c_reportVector4.RemoveAll();

// calcolo quante righe di tabella 16 colonne 4 blocchi
numLineAl1 = 0;
numLineAl2 = 0;




if (m_SOGLIE_ALLARME)
	{// use region
	numLineAl1 = pDoc->densitaAlarm.getNumLine();
	}

if (m_ALLARMISISTEMA)
	{// use region
	numLineAl2 = pDoc->systemAlarm.getNumLine();
	}

CPage*	ps= new CPage(r,pDC,FALSE,MM_TEXT);

// calcolo dimensione 1 riga
TABLEHEADER* pTable=new TABLEHEADER;        
pTable->PointSize=10;
pTable->LineSize=1;    // default shown only for demp purposes
pTable->NumPrintLines=1;
pTable->UseInches=TRUE;
pTable->AutoSize=FALSE;
pTable->Border=TRUE;
pTable->FillFlag=FILL_LTGRAY;
pTable->NumColumns=12;
pTable->NumRows = 1;
pTable->StartRow=0.0;
pTable->StartCol=0.0;
pTable->EndCol=ps->SetRightMargin(0.);
pTable->HeaderLines=0;
ps->Table(pTable);	

double dimOneRow=pTable->EndRow;
double bTop,bBottom;
// rect r in arrivo gia` internal rect
// CRect internalRect = calcPrintInternalRect(pDC,r);
CRect internalRect = r;

bTop = ps->ConvertYToInches(internalRect.top);
bBottom = ps->ConvertYToInches(internalRect.bottom);
double sizePageY = bBottom - bTop;
int lineXPage = sizePageY / dimOneRow;

// descrittore pagine difetti random
tPage.c_index = 0;
tPage.c_lineXPage = lineXPage;


	
// descrittore pagine allarmi1
int al1Count = numLineAl1;
while (al1Count > 0)
	{
	//  3+1 per intestazione
	if (tPage.getFreeLine4() <= 3+1)
		{
		c_reportVector4.Add(tPage);
		tPage.c_index ++;
		tPage.reset();
		}
	// 3+1 per intestazione
	tPage.c_numLineHeader += 3;
	tPage.c_numLineAl1 = (min(al1Count,tPage.getFreeLine4()));
	tPage.c_indexLineAl1 = numLineAl1-al1Count;
	al1Count -= tPage.c_numLineAl1;
	}

// descrittore pagine allarmi2
int al2Count = numLineAl2;
while (al2Count > 0)
	{
	//  3+1 per intestazione
	if (tPage.getFreeLine4() <= 3+1)
		{
		c_reportVector4.Add(tPage);
		tPage.c_index ++;
		tPage.reset();
		}
	// 3+1 per intestazione
	tPage.c_numLineHeader += 3;
	// - 3 per intestazione
	tPage.c_numLineAl2 = (min(al2Count,tPage.getFreeLine4()));
	tPage.c_indexLineAl2 = numLineAl2-al2Count;
	al2Count -= tPage.c_numLineAl2;
	}

if (tPage.getNumLine4() > 0)
	{
	c_reportVector4.Add(tPage);
	}

// lascio spazio per region

int nPage = c_reportVector4.GetSize();
delete pTable;
delete ps;

// Set print Num Page X text	
return (nPage);
}

//---------------------------------------------------------------
//
//	stampe
//
//
//

// utilizza vettore descrittivo di pagina c_reportPage
// random
double CInit::printTextPageEx2(CPage *ps,CRect rcBounds,int pageIndex) 
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

double lastYPos = bTop;
double sizeOneRow = (bBottom-bTop)/c_reportVector2[pageIndex].c_lineXPage;
int difRowToPrint = c_reportVector2[pageIndex].c_numLineDif;
if (difRowToPrint > 0)
	{
	int difPrintedRow = c_reportVector2[pageIndex].c_indexLineDif;
	TABLEHEADER* pTable2 = NULL;        
	pTable2=new TABLEHEADER;        
	pTable2->PointSize=10;
	pTable2->LineSize=1;    // default shown only for demp purposes
	pTable2->NumPrintLines=1;
	pTable2->UseInches=TRUE;
	pTable2->AutoSize=FALSE;
	pTable2->Border=TRUE;
	pTable2->FillFlag=FILL_LTGRAY;
	pTable2->NumColumns=12;
	pTable2->NumRows = difRowToPrint;
	pTable2->StartRow=bTop;
	pTable2->StartCol=bLeft;
	pTable2->EndCol=bRight;
	pTable2->HeaderLines=3;
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
	pTable2->ColDesc[0].Init(0.7,"Meter",FILL_LTGRAY);
	pTable2->ColDesc[1].Init(0.5,"Holes Num",FILL_NONE);
	pTable2->ColDesc[2].Init(0.5,"Class",FILL_NONE);
	pTable2->ColDesc[3].Init(0.5,"Zone",FILL_NONE);
	pTable2->ColDesc[4].Init(0.7,"Meter",FILL_LTGRAY);
	pTable2->ColDesc[5].Init(0.5,"Holes Num",FILL_NONE);
	pTable2->ColDesc[6].Init(0.5,"Class",FILL_NONE);
	pTable2->ColDesc[7].Init(0.5,"Zone",FILL_NONE);
	pTable2->ColDesc[8].Init(0.7,"Meter",FILL_LTGRAY);
	pTable2->ColDesc[9].Init(0.5,"Holes Num",FILL_NONE);
	pTable2->ColDesc[10].Init(0.5,"Class",FILL_NONE);
	pTable2->ColDesc[11].Init(0.5,"Zone",FILL_NONE);
	ps->setRealPrint(TRUE);
	// use table
	ps->Table(pTable2);	
	lastYPos = pTable2->EndRow + 1. * sizeOneRow;

	CString sItem;
	int ColPos = 0;
	int RowPos = 0;
	int curDif = 0;
	// colonna base, devo riempire prima prima colonna completa poi seconda etc
	int colBase = 0;
	
	for (int i=0;i<	pDoc->c_difCoil.GetSize();i++)
		{
		double pos;
		// pos =  (pDoc->c_difCoil.trueSizeEncod/1000.)*i;
		// Posizione a posteriori (valore 10 indica da 0 a 10)
		pos =  pDoc->c_difCoil[i].posizione; // posizione del metro dif
		// NOT trepassing total length
		if (pos > pDoc->getVTotalLength())
			{
			DifMetro df = pDoc->c_difCoil[i];
			continue;
			}
		// nel metro ogni scheda
		for (int j=0;j<pDoc->c_difCoil[i].GetSize();j++)
			{// Schede fuori finestra ispezione NON gestite
			// PerB
			int v = (long)pDoc->c_difCoil[i].ElementAt(j).PerBAt();
			if (v > 0)
				{// restart row from zero
				if (RowPos >= difRowToPrint)
					{
					if (colBase < 12)
						colBase += 4;
					else
						break;
					RowPos = 0;
					}
				ColPos = colBase;
				// 02-04-08 mancava if -> pagine tutte uguali 
				if (curDif >= (difPrintedRow*3))
					{
					// ??? Posizioni assolute 
					sItem.Format ("%7.1lf",pos);
					ps->Print(pTable2,RowPos,ColPos++,9,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = 0;
						}
					*/
					sItem.Format ("%3d",v);
					ps->Print(pTable2,RowPos,ColPos++,9,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = 0;
						}
					*/
					sItem.Format ("%s","Rd");
					ps->Print(pTable2,RowPos,ColPos++,9,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = 0;
						}
					*/
					// No info on banda
					// bande partono da banda1 non zero
					int b = 1+pDoc->getRelativePosBanda(pDoc->c_difCoil[i].ElementAt(j).banda);
					sItem.Format ("%3d",b);
					ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					// if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = colBase;
						}					
					}
				curDif++;
				}
			v = (long)pDoc->c_difCoil[i].ElementAt(j).BigHoleAt();
			if ((v > 0)&&// inclusi difetti class Big?
				(c_reportClass[4]))
				{// Bande cominciano da 1 non da zero
				if (RowPos >= difRowToPrint)
					{
					if (colBase < 12)
						colBase += 4;
					else
						break;
					RowPos = 0;
					}
				ColPos = colBase;
				// 02-04-08 mancava if -> pagine tutte uguali 
				// 25-01-13 inserito >= 
				if (curDif >= (difPrintedRow*3))
					{
					// ??? Posizioni assolute 
					sItem.Format ("%7.1lf",pos);
					ps->Print(pTable2,RowPos,ColPos++,9,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = 0;
						}
					*/
					sItem.Format ("%3d",v);
					ps->Print(pTable2,RowPos,ColPos++,9,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = 0;
						}
					*/
					sItem.Format ("%s","Big");
					ps->Print(pTable2,RowPos,ColPos++,9,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					/*
					if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = 0;
						}
					*/
					// No info on banda
					int b = 1+ pDoc->getRelativePosBanda(pDoc->c_difCoil[i].ElementAt(j).banda);
					sItem.Format ("%3d",b);
					ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					// if (ColPos >= pTable2->NumColumns)
						{
						RowPos++;
						ColPos = colBase;
						}					
					}
				curDif++;
				}	
			// Ogni classe
			for (int k=0;k<pDoc->c_difCoil[i].ElementAt(j).GetSize();k++)
				{
				int v = pDoc->c_difCoil[i].ElementAt(j).ElementAt(k);
				if ((v > 0)&&	// inclusi difetti class k?
					(c_reportClass[k]))
					{// Bande cominciano da 1 non da zero
					if (RowPos >= difRowToPrint)
						{
						if (colBase < 12)
							colBase += 4;
						else
							break;
						RowPos = 0;
						}
					ColPos = colBase;
					if (curDif >= (difPrintedRow)*3)
						{
						sItem.Format ("%7.1lf",pos);
						ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
						/*if (ColPos >= 12)
							{
							RowPos++;
							ColPos = 0;
							}
						*/
						sItem.Format ("%3d",v);
						ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
						/*
						if (ColPos >= 12)
							{
							RowPos++;
							ColPos = 0;
							}
						*/
//						if (k >= 2)
//							int a = 7;
						sItem.Format ("%c",'A'+k);
						ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
						/*
						if (ColPos >= 12)
							{
							RowPos++;
							ColPos = 0;
							}
						*/
						int b = 1+ pDoc->getRelativePosBanda(pDoc->c_difCoil[i].ElementAt(j).banda);
						sItem.Format ("%3d",b);
						ps->Print(pTable2,RowPos,ColPos++,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
						
						//if (ColPos >= 12)
							{
							RowPos++;
							ColPos = colBase;
							}
						
						}
					curDif ++;
					}
				}
	   		}	
		}
	delete pTable2;
	}

return (0.0);
}

// pagina dettaglio alzate
double CInit::printTextPageEx3(CPage *ps,CRect rcBounds,int pageIndex) 
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();
double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

double lastYPos = bTop;
double sizeOneRow = (bBottom-bTop)/c_reportVector3[pageIndex].c_lineXPage;

// quante righe ogni tabella => config abcd
int nl = 5;
nl += (c_reportClass[0])?2:0;
nl += (c_reportClass[1])?2:0;
nl += (c_reportClass[2])?2:0;
nl += (c_reportClass[3])?2:0;
nl += (c_reportClass[4])?1:0;

// prepare bitmap reportClass per populatealzata dettagli
int cBit=0;
cBit |= (c_reportClass[0])?0x01:0;
cBit |= (c_reportClass[1])?0x02:0;
cBit |= (c_reportClass[2])?0x04:0;
cBit |= (c_reportClass[3])?0x08:0;
cBit |= (c_reportClass[4])?0x10:0;



int alzToPrint=c_reportVector3[pageIndex].c_numAlzStruct;
for (int k=0;k<alzToPrint;k++)
	{
	// int alzToPrint = c_reportVector3[pageIndex].c_alzPage[k].c_alzIndex;
	if (alzToPrint > 0)
		{// use table
		CString sItem;
		int RowPos = 0;
		int elem = 	c_reportVector3[pageIndex].c_alzPage[k].c_elem;
		int alzata = c_reportVector3[pageIndex].c_alzPage[k].c_alzIndex;
		int totalCols = c_reportVector3[pageIndex].c_alzPage[k].c_numCol;
		int isLastBlock = c_reportVector3[pageIndex].c_alzPage[k].c_isLastBlock;
		int firstCol = c_reportVector3[pageIndex].c_alzPage[k].c_colIndex;
		CDAlzataDettagli fintaDialog;
		AlzataDettagli alzataDettagli;
		alzataDettagli.create(pDoc->c_rotoloMapping[elem].size());
		// solo ultimo blocco ha totali in fondo
		int numCols = totalCols + (isLastBlock?2:1);

		TABLEHEADER* pTable22 = NULL;        
		pTable22=new TABLEHEADER;        
		pTable22->PointSize=8;
		pTable22->LineSize=1;    // default shown only for demp purposes
		pTable22->NumPrintLines=1;
		pTable22->UseInches=TRUE;
		pTable22->AutoSize=TRUE;
		pTable22->Border=TRUE;
		pTable22->FillFlag=FILL_LTGRAY;
		pTable22->NumColumns= numCols;
		pTable22->NumRows = nl -3;
		pTable22->StartRow=lastYPos;
		pTable22->StartCol=bLeft;
		pTable22->EndCol=bRight;
		pTable22->HeaderLines=3;
		// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
		for (int i=0;i<numCols;i++)
			{// tutte colonne dell'intestazione
			// prima
			if (i == 0)
				{
				CString s;
				//s.Format("Elem %d  Rising %d\n   Type  ",elem+1,alzata+1);
				s.Format("Elem %d  Rising %s\n",elem+1,
				// pDoc->getStringFromInt(alzata-numScarti+pDoc->c_rotoloMapping[elem].c_firstRising));
				pDoc->getStringFromInt(pDoc->c_rotoloMapping.getGlobalRisingId(elem,alzata+1)));
				pTable22->ColDesc[i].Init(0.7,s,FILL_LTGRAY);
				continue;
				}
			// ultima
			if (isLastBlock && (i == numCols-1))
				{
				CRect subNastro;
				subNastro = getRectStrip(elem,alzata,0);
				CString s;
				s.Format("Length %6d(m)",subNastro.BottomRight().y-
				subNastro.TopLeft().y);
				pTable22->ColDesc[i].Init(0.7,(LPCSTR)s,FILL_LTGRAY);
				continue;
				}
			CString s;
			//s.Format("Strip %d",i);
			// i-1 come indice perche` c'e` una colonna sx di intestazione
			double size = (bRight-bLeft-0.7-0.7)/(numCols-2);
			int dataIndex = firstCol + i - 1;
			s.Format("Strip %d",pDoc->c_rotoloMapping[elem].at(dataIndex).c_posizione);
			pTable22->ColDesc[i].Init(size,s,FILL_NONE);
			}
		ps->setRealPrint(TRUE);
		// Aggiunta dettaglio difetti per alzate
		pTable22->StartRow=lastYPos;
		ps->Table(pTable22);	
		lastYPos = pTable22->EndRow + 1 * sizeOneRow;
		fintaDialog.populateAlzataDettagli(pDoc,&alzataDettagli,elem,alzata,cBit);
		// class A?
		if(c_reportClass[0])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Holes A");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%d",alzataDettagli.c_difettiA[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				}
			// next line
			RowPos ++;
			}
		// class B?
		if(c_reportClass[1])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Holes B");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%d",alzataDettagli.c_difettiB[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				}
			// next line
			RowPos ++;
			}
		// class C?
		if(c_reportClass[2])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Holes C");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%d",alzataDettagli.c_difettiC[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				}
			// next line
			RowPos ++;
			}
		// class D?
		if(c_reportClass[3])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Holes D");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%d",alzataDettagli.c_difettiD[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				}
			// next line
			RowPos ++;
			}
		// Big Holes?
		if(c_reportClass[4])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Big Holes");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%d",alzataDettagli.c_numBigHole[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				}
			// next line
			RowPos ++;
			}
		for (int ColPos=0;ColPos<numCols;ColPos++)
			{
			// indice nella tabella dei dati di alzataDettagli
			int dataIndex = firstCol + (ColPos-1);
			// prima intestazione
			if (ColPos == 0)
				{
				sItem.Format ("RD Holes");
				ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
				}
			else
				{
				sItem.Format ("%d",alzataDettagli.c_numPerB[dataIndex]);
				ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);								
				}
			}
		RowPos ++;
		for (int ColPos=0;ColPos<numCols;ColPos++)
			{
			// indice nella tabella dei dati di alzataDettagli
			int dataIndex = firstCol + (ColPos-1);
			// prima intestazione
			if (ColPos == 0)
				{
				sItem.Format ("Total Holes");
				ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
				}
			else
				{
				sItem.Format ("%d",alzataDettagli.c_difettiTotale[dataIndex]);
				ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);								
				}
			}
		RowPos ++;
		// class A?
		if(c_reportClass[0])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Density A");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%7.05lf",alzataDettagli.c_densitaA[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);								
					}
				}
			RowPos ++;
			}
		// class B?
		if(c_reportClass[1])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Density B");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%7.05lf",alzataDettagli.c_densitaB[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);									
					}
				}
			RowPos ++;
			}
		// class C?
		if(c_reportClass[2])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Density C");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%7.05lf",alzataDettagli.c_densitaC[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);									
					}
				}
			RowPos ++;
			}
		// class D?
		if(c_reportClass[3])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Density D");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%7.05lf",alzataDettagli.c_densitaD[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				}
			RowPos ++;						
			}
		// total dens?
		if(c_reportClass[0]||
			c_reportClass[1]||
			c_reportClass[2]||
			c_reportClass[3])
			{// loop on any column
			for (int ColPos=0;ColPos<numCols;ColPos++)
				{
				// indice nella tabella dei dati di alzataDettagli
				int dataIndex = firstCol + (ColPos-1);
				// prima intestazione
				if (ColPos == 0)
					{
					sItem.Format ("Total Density");
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);
					}
				else
					{
					sItem.Format ("%7.05lf",alzataDettagli.c_densitaTotale[dataIndex]);
					ps->Print(pTable22,RowPos,ColPos,8,TEXT_BOLD|TEXT_CENTER,(LPCSTR)sItem);								
					}
				}
			}
		delete pTable22;
		alzataDettagli.Destroy();
		}
	}


return (0.0);
}



// pagina allarmi
double CInit::printTextPageEx4(CPage *ps,CRect rcBounds,int pageIndex) 
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();
double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

double lastYPos = bTop;
double sizeOneRow = (bBottom-bTop)/c_reportVector4[pageIndex].c_lineXPage;


int al1RowToPrint=c_reportVector4[pageIndex].c_numLineAl1;
if (al1RowToPrint>0)
	{// use region
	//	Spazio totale fino ad ora = paginePrec + difetti ev. pagina attuale
	int firstAl1RowToPrint = c_reportVector4[pageIndex].c_indexLineAl1;

	TABLEHEADER* pTable3 = NULL;        
	pTable3=new TABLEHEADER;  
	pTable3->SetSkip=TRUE;	// no fill 
	pTable3->PointSize=10;
	pTable3->LineSize=1;    // default shown only for demp purposes
	pTable3->NumPrintLines=1;
	pTable3->UseInches=TRUE;
	pTable3->AutoSize=FALSE;
	pTable3->Border=TRUE;
	pTable3->VLines = FALSE;	//	true draw vertical seperator lines
	pTable3->HLines = FALSE;    // ditto on horizontal lines
	pTable3->FillFlag=FILL_LTGRAY;
	pTable3->NumColumns=1;
	pTable3->NumRows = al1RowToPrint;
	pTable3->StartRow=lastYPos;
	pTable3->StartCol=bLeft;
	pTable3->EndCol=bRight;
	pTable3->HeaderLines=3;
	// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
	pTable3->ColDesc[0].Init(0.7,"Alarm Threshold",FILL_NONE);
	ps->setRealPrint(TRUE);
	ps->Table(pTable3);	

	int al1PrintedRow = 0;
	for(int i=0;i<pDoc->densitaAlarm.getNumLine();i++)
		{
		if (al1PrintedRow > al1RowToPrint)
			break;
		if (i < firstAl1RowToPrint)
			continue;
		
		CString s;
		s.Format("  [%3d] ",i+1);
		s += pDoc->densitaAlarm.c_messages[i].Left(pDoc->densitaAlarm.c_messages[i].GetLength()-2);
		ps->Print(pTable3,al1PrintedRow,0,8,TEXT_BOLD|TEXT_LEFT,(LPCSTR)s);
		al1PrintedRow++;
		}
	lastYPos = pTable3->EndRow + 1. * sizeOneRow;
	delete pTable3;
	}


int al2RowToPrint=c_reportVector4[pageIndex].c_numLineAl2;
if (al2RowToPrint>0)
	{// use region
	int firstAl2RowToPrint = c_reportVector4[pageIndex].c_indexLineAl2;

	if (al2RowToPrint > 0)
		{
		TABLEHEADER* pTable4 = NULL;        
		pTable4=new TABLEHEADER;        
		pTable4->SetSkip=TRUE;
		pTable4->PointSize=10;
		pTable4->LineSize=1;    // default shown only for demp purposes
		pTable4->NumPrintLines=1;
		pTable4->UseInches=TRUE;
		pTable4->AutoSize=FALSE;
		pTable4->Border=TRUE;
		pTable4->VLines = FALSE;	//	true draw vertical seperator lines
		pTable4->HLines = FALSE;    // ditto on horizontal lines
		pTable4->FillFlag=FILL_LTGRAY;
		pTable4->NumColumns=1;
		pTable4->NumRows = al2RowToPrint;
		pTable4->StartRow=lastYPos;
		pTable4->StartCol=bLeft;
		pTable4->EndCol=bRight;
		pTable4->HeaderLines=3;
		// Intestazione: Metro,Numero dei fori,Classe difetto,Banda
		pTable4->ColDesc[0].Init(0.7,"System alarm",FILL_NONE);
		ps->setRealPrint(TRUE);
		ps->Table(pTable4);	
		// tutti dif. + 2 intestaz. di * pagina + intestaz pagina mista
		int al2PrintedRow = 0;
		for(int i=0;i<pDoc->systemAlarm.getNumLine();i++)
			{
			if (al2PrintedRow > al2RowToPrint)
				break;
			if (i < firstAl2RowToPrint)
				continue;
			CString s;
			s.Format("  [%3d] ",i+1);
			s += pDoc->systemAlarm.c_messages[i].Left(pDoc->systemAlarm.c_messages[i].GetLength()-2);
			ps->Print(pTable4,al2PrintedRow,0,8,TEXT_BOLD|TEXT_LEFT,(LPCSTR)s);
			al2PrintedRow++;
			}
		lastYPos = pTable4->EndRow;
		delete pTable4;
		}
	}
// Set print Num Page X text	
return (0.0);
}

void CInitView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
onBeginPrinting(pDC,pInfo);
CPdfPrintView::OnBeginPrinting(pDC, pInfo);
}
void CInit::onBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
c_continuePrinting = TRUE;

}



BOOL CInitView::OnPreparePrinting(CPrintInfo* pInfo) 
{

return onPreparePrinting(pInfo);

}


BOOL CInit::onPreparePrinting(CPrintInfo* pInfo) 
{

// dBase already Open
CLineDoc* pDoc = (CLineDoc* )GetDoc();

// Create the CProgressCtrl as a child of the status bar positioned
// over the first pane.
 
CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
frame->progressCreate();
frame->progressSetRange(0, 10);
frame->progressSetStep(2);


// Load report option
DBReport dbReport(dBase);

if(c_useManualReportConfig)
	{	
	if (!dbReport.openSelectManual ())
		{
		AfxGetMainWnd()->MessageBox ("Error openSelectManual","dbReport.Open");
		dbReport.Close();
		return FALSE;
		}
	}
else
	{
	if (!dbReport.openSelectDefault ())
		{
		AfxGetMainWnd()->MessageBox ("Error openSelectDefault","dbReport.Open");
		dbReport.Close();
		return FALSE;
		}
	}
	

//  Move Init Data from Db to Dialog
m_SOGLIE_ALLARME = dbReport.m_SOGLIE_ALLARME;
m_DATI_CLIENTE = dbReport.m_DATI_CLIENTE;
m_DATA_LAVORAZIONE = dbReport.m_DATA_LAVORAZIONE;
m_DESCR_PERIODICI = dbReport.m_DESCR_PERIODICI;
m_DESCR_RANDOM = dbReport.m_DESCR_RANDOM;
m_DESCR_ALZATE = dbReport.m_DESCR_ALZATE;
m_LUNGHEZZA_BOBINA = dbReport.m_LUNGHEZZA_BOBINA;
m_ALLARMISISTEMA = dbReport.m_MAPPA100;
//m_ALMETRO = dbReport.m_ALMETRO;
//m_DALMETRO = dbReport.m_DALMETRO;
// Lunghezza fissa tutto asse
//m_DALMETRO = 0;
//m_ALMETRO = pDoc->getVTotalLength();
// ora lunghezza report configurabile da CDSelReportType
m_DALMETRO = c_dalMetro;
m_ALMETRO = c_alMetro;

//-----------------
c_reportGraph[0] = dbReport.m_DESCR_GRAPH1;
c_reportGraph[1] = dbReport.m_DESCR_GRAPH2;
c_reportGraph[2] = dbReport.m_DESCR_GRAPH3;
c_reportGraph[3] = dbReport.m_DESCR_GRAPH4;

c_reportClass[0] = dbReport.m_DESCR_CLASSA;
c_reportClass[1] = dbReport.m_DESCR_CLASSB;
c_reportClass[2] = dbReport.m_DESCR_CLASSC;
c_reportClass[3] = dbReport.m_DESCR_CLASSD;
c_reportClass[4] = dbReport.m_DESCR_CLASSBIG;
//----------------------------


m_INTERVALLO[0] = dbReport.m_INTERVALLO1;
m_INTERVALLO[1] = dbReport.m_INTERVALLO2;
m_INTERVALLO[2] = dbReport.m_INTERVALLO3;
m_INTERVALLO[3] = dbReport.m_INTERVALLO4;
m_INTERVALLO[4] = dbReport.m_INTERVALLO5;
m_INTERVALLO[5] = dbReport.m_INTERVALLO6;
m_INTERVALLO[6] = dbReport.m_INTERVALLO7;
m_INTERVALLO[7] = dbReport.m_INTERVALLO8;
m_INTERVALLO[8] = dbReport.m_INTERVALLO9;
m_INTERVALLO[9] = dbReport.m_INTERVALLO10;
m_INTERVALLO[10]= dbReport.m_INTERVALLO11;
m_INTERVALLO[11] = dbReport.m_INTERVALLO12;
m_INTERVALLO[12] = dbReport.m_INTERVALLO13;
m_INTERVALLO[13] = dbReport.m_INTERVALLO14;
m_INTERVALLO[14] = dbReport.m_INTERVALLO15;
m_INTERVALLO[15] = dbReport.m_INTERVALLO16;
m_INTERVALLO[16] = dbReport.m_INTERVALLO17;
m_INTERVALLO[17] = dbReport.m_INTERVALLO18;
m_INTERVALLO[18] = dbReport.m_INTERVALLO19;


// Close DbReport	
dbReport.Close();



CProfile profile;

c_leftMarg = profile.getProfileInt("Report","LeftMarg",20);		// margine sinistro report in mm
c_rightMarg = profile.getProfileInt("Report","RightMarg",10);	// margine destro report in mm
c_topMarg = profile.getProfileInt("Report","TopMarg",30);		// margine alto report in mm
c_bottomMarg = profile.getProfileInt("Report","BottomMarg",20);	// margine basso report in mm

c_strLogoId = profile.getProfileString("Report","Logo","LOGO_ELVAL");	// stringa per logo {LOGO_EMPTY,LOGO_ELVAL} per ora

layoutL.setDbLogo(c_strLogoId);
layoutD.setDbLogo(c_strLogoId);
layoutAD.setDbLogo(c_strLogoId);
layoutQ.setDbLogo(c_strLogoId);
c_layoutCompressABBig.setDbLogo(c_strLogoId);

// Visualizza Dialog di stampa
// If not printPreview NO Print DialogBox
pInfo->m_bDirect = directPrint; 	

// Show Progress
frame->progressStepIt();

// Abilita multicopie (Da gestire manualmente)
// pInfo->m_pPD->m_pd.Flags &= ~PD_USEDEVMODECOPIES;

// Impostazione e calcolo numero di pagine spostata 
// in OnBeginPrinting xChe` CDC modo printing -> calcoli giunsti
// Prepare Device Context
BOOL retVal = DoPreparePrinting (pInfo);
if (!retVal)
	{
	//FixBug  24-05-08 
	// se cancel su print dialog infinity loop
	c_isPrinting = FALSE;
	//--------------------------------------
	frame->progressDestroy();
	AfxMessageBox("CSM20: Printing Deleted!!",MB_ICONERROR);
	return(retVal);
	}
// Set max Number page	3 + testo
// 
CDC dc;
dc.Attach (pInfo->m_pPD->m_pd.hDC);
// save
CRect r;
r.SetRect(0, 0, 
             dc.GetDeviceCaps(HORZRES), 
             dc.GetDeviceCaps(VERTRES)) ;

r = calcPrintInternalRect(&dc,r);

int nP;
if (c_doPrintStrip)
	{
	nP = calcStripRepPage(&dc,r);
	pInfo->SetMaxPage((UINT) nP);
	dc.Detach();
	frame->progressDestroy();
	return retVal;	
	}


// setting della variabile globale per allarme difetti da laminazione
c_existLaminDef=FALSE;
int lastElem = 	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
for (int j=0;(j<pDoc->c_difCoil.GetCount())&&(!c_existLaminDef);j++)
	{// per ogni metro
	for (int i=0;(i<pDoc->c_rotoloMapping[lastElem].size())&&(!c_existLaminDef);i++)
		{// per ogni strip
		int sizeFB = c_layoutCompressABBig.c_sizeFBanda;
		//int sizeFB = 0;
		int bdx=0;
		int bsx=0;
		if (pDoc->c_rightAlignStrip)
			{// se allineato a dx larghezze crescono verso sinistra
			double xPos = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos-sizeFB;
			double xLargh = (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
			bdx = xPos/SIZE_BANDE;
			bsx = (xPos-xLargh)/SIZE_BANDE;
			}
		else
			{// se allineato a dx larghezze crescono verso destra
			double xPos = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos+sizeFB;
			double xLargh = (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
			bsx = xPos/SIZE_BANDE;
			bdx = (xPos+xLargh)/SIZE_BANDE;
			}
		c_existLaminDef = pDoc->c_difCoil[j].existLaminDef(bsx,bdx);
		}
	}


c_numTextPage2 = prepareTextPageEx2(&dc,r);
c_numTextPage3 = prepareTextPageEx3(&dc,r);
c_numTextPage4 = prepareTextPageEx4(&dc,r);

dc.Detach();


// Modifica per stampa solo report Testo
int numPage ;
int numTextPage = c_numTextPage2+c_numTextPage3+c_numTextPage4;
//if (onlyTextReport)
	//numPage = 1 + pagePr.getNumPage();
//	numPage = 1 + numTextPage + 3;
//else
	//numPage = 1 + pagePr.getNumPage() + 3 + pagePrEnd.getNumPage(); // grafici + testo

numPage = 1 + numTextPage;
for (int i=0;i<4;i++)
	numPage += (c_reportGraph[i]?1:0); // 4 grafici + testo

pInfo->SetMaxPage((UINT) numPage);
// Insert Wait Dialog Box

if (retVal)
	{
	// TODO: Add your specialized code here and/or call the base class
	
	// int code;
	CString rotolo;
	rotolo = pDoc->getRotolo();

	// Show Progress
	frame->progressStepIt();

	// Trend longitudinale * 4
	// Init layoutL Fix Attributes
	vType = MULTILINEVIEW;
	layoutL.setViewType(MULTILINEVIEW);

	// Init layoutL Fix Attributes
	CProfile profile;
	CString s;
	

	layoutL.fCaption.size =  profile.getProfileInt("MLineReport","CaptionSize",120);
	layoutL.fLabel.size = profile.getProfileInt("MLineReport","LabelSize",100);
	layoutL.fNormal.size = profile.getProfileInt("MLineReport","NormalSize",110);
	

	s = profile.getProfileString("MLineReport","NewLine","3,6");
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			layoutL.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString("MLineReport","LabelOrder","0,1,2,3,4,5,6,7,8,9");
	int k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			layoutL.c_vOrderLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString("MLineReport","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
			s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		layoutL.c_pxSepLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString("MLineReport","SCaption",
							 "TREND");
	layoutL.setCaption(s);
	layoutL.sCaption.setFontInfo(layoutL.fCaption);
	layoutL.sCaption.set3Dlook(FALSE);

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		layoutL.c_sLabel[i].setFontInfo(layoutL.fNormal);
		layoutL.c_vLabel[i].setFontInfo(layoutL.fNormal);
		layoutL.c_vLabel[i].set3Dlook(TRUE);
		layoutL.c_vLabel[i].setStringBase("999999");
		}


	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		CString sIndex;
		sIndex.Format ("SLabel%d",i);
		s = profile.getProfileString("MLineReport",sIndex,sIndex);
		if (s != sIndex)
			{
			layoutL.c_sLabel[i] = s;
			}
		}

	for (int i=0;(i<pDoc->c_difCoil.getNumClassi());i++)
		{
		CString s;
		if (c_reportClass[i])
			{
			s = TCHAR('A'+i);
			layoutL.c_cLabel[i] = s;
			layoutL.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			layoutL.c_cLabel[i].setTipoIcon(IPALLINO);
			}
		else
			{
			s = TCHAR(' ');
			layoutL.c_cLabel[i] = s;
			layoutL.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			layoutL.c_cLabel[i].setTipoIcon(NOICON);
			}

		}
	// CalcDrawRect
	layoutL.c_viewTopPerc = profile.getProfileInt("MLineReport","ViewTop%",10);
	layoutL.c_viewBottomPerc = profile.getProfileInt("MLineReport","ViewBottom%",25);
	layoutL.c_viewLeftPerc = profile.getProfileInt("MLineReport","ViewLeft%",8);
	layoutL.c_viewRightPerc = profile.getProfileInt("MLineReport","ViewRight%",5);

	// Test di congruenza valori visualizzazione
	if (m_ALMETRO > pDoc->getVTotalLength())
		m_ALMETRO = pDoc->getVTotalLength();

	if (m_DALMETRO > pDoc->getVTotalLength())
		m_DALMETRO = 0;

	if (m_DALMETRO > m_ALMETRO)
		m_DALMETRO = 0;

// aggiunte label alzate
StringLabelH strLH;
int cnt = 0;
{
for (int i=0;i<pDoc->c_rotoloMapping.size();i++)
	{
	for (int j=0;j<pDoc->c_rotoloMapping[i].c_alzate.size();j++)
		{		
		if (cnt >= 20)
			break;
		if (j == 0)
			continue;
		if ((i == pDoc->c_rotoloMapping.size()-1)&&
			(j == pDoc->c_rotoloMapping[i].c_alzate.size()-1))
			continue;
		int tipoAlzata = pDoc->c_rotoloMapping.at(i).c_alzate.at(j).c_tipo;;
		if (tipoAlzata == ALZATA_SCARTO)
			continue;
		double dPos = pDoc->c_rotoloMapping[i].c_alzate[j].c_yPos/(m_ALMETRO-m_DALMETRO);
		// dPos espresso in % dello spazio video
		strLH.pos = dPos;
		//strLH.label.Format("Ris %d",cnt+1);
		strLH.label.Format("%s",
			//pDoc->getStringFromInt(cnt+pDoc->c_rotoloMapping[i].c_firstRising));
			pDoc->getStringFromInt(pDoc->c_rotoloMapping.getGlobalRisingId(i,j+1)));
		layoutL.c_stringLabelH[cnt++] = strLH;
		}
	}
}

	layoutL.setMode (LINEAR | NOLABELH | MSCALEV | USE_STRING_LABELH);
	// fori codice
	// code = 'A'+j;
	// s.Format("%c",code); 
	

	// Set Values in string
	// scala Verticale la massima delle quattro classi
	// scale diversificate sulle quattro classi
	double dMY[4];
	for (int i=0;i<pDoc->c_difCoil.getNumClassi();i++)
		{
		dMY[i] = pDoc->c_difCoil.getMaxTrendLFromTo('A'+i,
			(int) m_DALMETRO,(int)m_ALMETRO);
		if (dMY[i] == 0.)
			dMY[i] = 1.0;
		layoutL.setMaxY('A'+i,dMY[i]*1.25);
		layoutL.setMinY('A'+i,0.);
		}

	layoutL.c_vLabel[0]  = (int )m_ALMETRO;
	layoutL.c_vLabel [1] = (int )m_DALMETRO;
//	layoutL.c_vLabel [2] = pDoc->GetVCliente();
	layoutL.c_vLabel [2] = pDoc->c_cliente;
	int delta = (int) (m_ALMETRO-m_DALMETRO);
	layoutL.c_vLabel [3] = delta;		// (int)maxValX;		// maxValX 
	layoutL.c_vLabel [4] = pDoc->getRotolo();
	layoutL.c_vLabel [5] = pDoc->c_lega;
	CTime d(pDoc->c_startTime);
	layoutL.c_vLabel [6] = d.Format( "%A, %B %d, %Y" );
	s.Format(" %6.03lf",pDoc->getAlTrend10m('A'));
	layoutL.c_vLabel [7] = s;
	s.Format(" %6.03lf",pDoc->getAlTrend10m('B'));
	layoutL.c_vLabel [8] = s;
	s.Format(" %6.03lf",pDoc->getAlTrend10m('C'));
	layoutL.c_vLabel [9] = s;
	s.Format(" %6.03lf",pDoc->getAlTrend10m('D'));
	layoutL.c_vLabel [10] = s;

	// int numClassi = ;
	layoutL.setNumClassi(pDoc->c_difCoil.getNumClassi());
	layoutL.setNumLabelClassi(pDoc->c_difCoil.getNumClassi());
	layoutL.setNumCpu(pDoc->c_numCpu);

	// Inserire i quattro codici per quattro pagine di stampa
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		// fori codice
		int code = 'A'+j;
		// check if enable
		trendLine[j].setPen (CSM20GetClassColor(j));		// era RGB (0,0,0) black
		trendLine[j].setPenLarge (CSM20GetClassColor(j));	// Black
		trendLine[j].setMode(LINEAR);	

		lineInfo[j].max = dMY[j]*1.25;
		lineInfo[j].min = 0.;
		lineInfo[j].limit = pDoc->c_difCoil.c_sogliaAllarme[j];

		// Determinazione dimensione asse X
		//int stepSize;
		double stepSize;
		stepSize = pDoc->GetTrendStep();	
		
		double deltaEnd = (pDoc->getVTotalLength() - m_ALMETRO)>0?(pDoc->getVTotalLength()- m_ALMETRO):0.;
		double deltaStart = m_DALMETRO;

		int deltaEndStep = (int)(deltaEnd/stepSize);
		int deltaStartStep = (int)(deltaStart/stepSize);
		
		int numStepX = (int)(pDoc->getVTotalLength()/stepSize);
			numStepX -= deltaEndStep;
		numStepX -= deltaStartStep;
		if (numStepX <= 0)
			numStepX = 1;

		// Numero step da disegnare
		lineInfo[j].count = (int)(((int)pDoc->getVTotalLength()/(int)stepSize < numStepX)?
					(pDoc->getVTotalLength()/(int)stepSize) : numStepX); 

//		lineInfo[j].count = (pDoc->c_difCoil.getSize(3,code) < numStep)?
//			pDoc->c_difCoil.getSize(3,code) : numStep; 
	
		lineInfo[j].numStep = numStepX;
		lineInfo[j].actual	= numStepX ;  // pDoc->c_difCoil.getSize(0,code)% numStep;
		
		// Aggiornato 27-19-1997
		lineInfo[j].newVal (lineInfo[j].numStep+1);

		// |-------|++++++++++++++|---------------|
		// 0	  Dal			  Al             meter
		//               delta		  deltaEnd
		// getValTrendL torna valori riferiti all'ultimo,lavora in metri
		// bisogna passare indici a partire da Al in poi
		// offset dal fondo
		// sostituire stepSize a 10
		for (int i=0;i<= lineInfo[j].numStep;i++)
			{
			double val = 0.;
			// 26-01-06
			// attenzione indice di partenza 
			// aggiungere deltaEndStep a i per calcolo posizione
			lineInfo[j].val[i] = pDoc->c_difCoil.getValNumLStep(code,(deltaEndStep+i)*(int)stepSize);
			}
		}	
	// Fine preparazione lineInfo
	
	// Show Progress
	frame->progressStepIt();

	/*------------------------------------------*/
	// Preparazione Densita' init colors pen and brush

	layoutD.fCaption.size = profile.getProfileInt("TowerReport","CaptionSize",120);
	layoutD.fLabel.size =  profile.getProfileInt("TowerReport","LabelSize",100);
	layoutD.fNormal.size =  profile.getProfileInt("TowerReport","NormalSize",110);


	s = profile.getProfileString("TowerReport","NewLine","3,6");
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			layoutD.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString("TowerReport","LabelOrder","0,1,2,3,4,5,6,7,8,9");
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			layoutD.c_vOrderLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString("TowerReport","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		layoutD.c_pxSepLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	// layoutD.setLabel ("label 1");
	layoutD.setViewType(TOWERVIEW);
	layoutD.setMode (LINEAR);
	s = profile.getProfileString("TowerReport","SCaption",
							 "CROSS WEB DISTRIBUTION");
	layoutD.setCaption(s);
	
	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		layoutD.c_sLabel[i].setFontInfo(layoutD.fNormal);
		layoutD.c_vLabel[i].setFontInfo(layoutD.fNormal);
		layoutD.c_vLabel[i].set3Dlook(TRUE);
		layoutD.c_vLabel[i].setStringBase("999999");
		}

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		CString sIndex;
		sIndex.Format ("SLabel%d",i);
		s = profile.getProfileString("TowerReport",sIndex,sIndex);
		if (s != sIndex)
			layoutD.c_sLabel[i] = s;
		}

	for (int i=0;(i<pDoc->c_difCoil.getNumClassi());i++)
		{
		CString s;
		if (c_reportClass[i])
			{
			s = TCHAR('A'+i);
			layoutD.c_cLabel[i] = s;
			layoutD.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			layoutD.c_cLabel[i].setTipoIcon(IPALLINO);
			}
		else
			{
			s = TCHAR(' ');
			layoutD.c_cLabel[i] = s;
			layoutD.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			layoutD.c_cLabel[i].setTipoIcon(NOICON);
			}

		}

	// CalcDrawRect
	layoutD.c_viewTopPerc = profile.getProfileInt("TowerReport","ViewTop%",10);
	layoutD.c_viewBottomPerc = profile.getProfileInt("TowerReport","ViewBottom%",25);
	layoutD.c_viewLeftPerc = profile.getProfileInt("TowerReport","ViewLeft%",8);
	layoutD.c_viewRightPerc = profile.getProfileInt("TowerReport","ViewRight%",5);

	densView.SetSize (pDoc->c_difCoil.getNumSchede()); 
	for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
		{
		densView[i].init (pDoc->c_difCoil.getNumClassi());
		densView[i].setMode (TOWER);
		for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
			{
			densView[i].setPenTB (j,CSM20GetClassColor(j));	  // Top and Bottom color
			densView[i].setPenLR (j,layoutD.getRgbBackColor());	  // Red
			switch(j%4)
				{
				case 0 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_BDIAGONAL);
					break;
				case 1 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_DIAGCROSS);
					break;
				case 2 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_FDIAGONAL);
					break;
				case 3 :
					densView[i].setBrush (j,CSM20GetClassColor(j),BS_HATCHED,HS_DIAGCROSS);
					break;

				}
			}
		}

	// Set Values in string
	// solo una vista in trend longitudinale
	double MaxY =  pDoc->c_difCoil.getMaxDensFromTo((int )m_DALMETRO,
			(int )m_ALMETRO)*1.25;
	if (MaxY == 0.) MaxY = 1.25;
	layoutD.setMaxY('A'+0,MaxY);
	layoutD.setMinY('A'+0,0.);
	// 26-01-06 sostituito almetro e dalmetro
	layoutD.c_vLabel[0]  = (int )m_ALMETRO;
	layoutD.c_vLabel [1] = (int )m_DALMETRO;
//	layoutD.c_vLabel [2] = pDoc->GetVCliente();
	layoutD.c_vLabel [2] = pDoc->c_cliente;
	layoutD.c_vLabel [3] = 0;		// maxValX 
	layoutD.c_vLabel [4] = pDoc->getRotolo();
	layoutD.c_vLabel [5] = pDoc->c_lega;
	// d contiene starttime settato da trend long piu` sopra
	layoutD.c_vLabel [6] = d.Format( "%A, %B %d, %Y" );
	s.Format(" %6.03lf",pDoc->getAlDens1Cm());
	layoutD.c_vLabel [7] = s;

	layoutD.setNumSchede(pDoc->c_difCoil.getNumSchede());
	int numClassi = pDoc->c_difCoil.getNumClassi();
	layoutD.setNumClassi(numClassi);
	layoutD.setNumLabelClassi(numClassi);
	layoutD.setNumCpu(pDoc->c_numCpu);


	// Fine Preparazione densita'
//-----------------------------------------------------------------------
	// Rapporto allarme densita`
	
	// Trend longitudinale * 4
	// Init layoutD Fix Attributes
	vType = MULTILINEVIEW;
	layoutAD.setViewType(MULTILINEVIEW);

	// Init layoutAD Fix Attributes

	layoutAD.fCaption.size =  profile.getProfileInt("AlarmReport","CaptionSize",120);
	layoutAD.fLabel.size = profile.getProfileInt("AlarmReport","LabelSize",100);
	layoutAD.fNormal.size = profile.getProfileInt("AlarmReport","NormalSize",110);
	

	s = profile.getProfileString("AlarmReport","NewLine","3,6");
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			layoutAD.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString("AlarmReport","LabelOrder","0,1,2,3,4,5,6,7,8,9");
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			layoutAD.c_vOrderLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString("AlarmReport","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
			s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		layoutAD.c_pxSepLabel [k++] = v;
		if (k>9)
			break;
		}

	s = profile.getProfileString("AlarmReport","SCaption",
							 "ALARM");
	layoutAD.setCaption(s);
	layoutAD.sCaption.setFontInfo(layoutAD.fCaption);
	layoutAD.sCaption.set3Dlook(FALSE);

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		layoutAD.c_sLabel[i].setFontInfo(layoutAD.fNormal);
		layoutAD.c_vLabel[i].setFontInfo(layoutAD.fNormal);
		layoutAD.c_vLabel[i].set3Dlook(TRUE);
		layoutAD.c_vLabel[i].setStringBase("999999");
		}


	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		CString sIndex;
		sIndex.Format ("SLabel%d",i);
		s = profile.getProfileString("AlarmReport",sIndex,sIndex);
		if (s != sIndex)
			{
			layoutAD.c_sLabel[i] = s;
			}
		}
	 
	for (int i=0;(i<pDoc->c_difCoil.getNumClassi());i++)
		{
		CString s;
		if (c_reportClass[i])
			{
			s = TCHAR('A'+i);
			layoutAD.c_cLabel[i] = s;
			layoutAD.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			layoutAD.c_cLabel[i].setTipoIcon(IPALLINO);
			}
		else
			{
			s = TCHAR(' ');
			layoutAD.c_cLabel[i] = s;
			layoutAD.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			layoutAD.c_cLabel[i].setTipoIcon(NOICON);
			}
		}

	// CalcDrawRect
	layoutAD.c_viewTopPerc = profile.getProfileInt("AlarmReport","ViewTop%",10);
	layoutAD.c_viewBottomPerc = profile.getProfileInt("AlarmReport","ViewBottom%",25);
	layoutAD.c_viewLeftPerc = profile.getProfileInt("AlarmReport","ViewLeft%",8);
	layoutAD.c_viewRightPerc = profile.getProfileInt("AlarmReport","ViewRight%",5);

// aggiunte label alzate
StringLabelH strADH;
cnt = 0;
{
for (int i=0;i<pDoc->c_rotoloMapping.size();i++)
	{
	for (int j=0;j<pDoc->c_rotoloMapping[i].c_alzate.size();j++)
		{		
		if (cnt >= 20)
			break;
		if (j == 0)
			continue;
		if ((i == pDoc->c_rotoloMapping.size()-1)&&
			(j == pDoc->c_rotoloMapping[i].c_alzate.size()-1))
			continue;
		double dPos = pDoc->c_rotoloMapping[i].c_alzate[j].c_yPos/(m_ALMETRO-m_DALMETRO);
		// dPos espresso in % dello spazio video
		strADH.pos = dPos;
		strADH.label.Format("%d",cnt+1);
		layoutAD.c_stringLabelH[cnt++] = strADH;
		}
	}
}

	layoutAD.setMode (LINEAR | NOLABELH | MSCALEV);
	// fori codice
	// code = 'A'+j;
	// s.Format("%c",code); 
	
	// Test di congruenza valori visualizzazione
	if (m_ALMETRO > pDoc->getVTotalLength())
		m_ALMETRO = pDoc->getVTotalLength();

	if (m_DALMETRO > pDoc->getVTotalLength())
		m_DALMETRO = 0;

	if (m_DALMETRO > m_ALMETRO)
		m_DALMETRO = 0;

	// Set Values in string
	dMY[0] = 1.0; // per allarmi
	// solo una vista per trend longitudinale
	layoutAD.setMaxY('A'+0,dMY[0]*1.25);
	layoutAD.setMinY('A'+0,0.);
	layoutAD.setMaxY('A'+1,dMY[0]*1.25);
	layoutAD.setMinY('A'+1,0.);
	layoutAD.setMaxY('A'+2,dMY[0]*1.25);
	layoutAD.setMinY('A'+2,0.);
	layoutAD.setMaxY('A'+3,dMY[0]*1.25);
	layoutAD.setMinY('A'+3,0.);
	layoutAD.c_vLabel[0]  = (int )m_ALMETRO;
	layoutAD.c_vLabel [1] = (int )m_DALMETRO;
//	layoutAD.c_vLabel [2] = pDoc->GetVCliente();
	layoutAD.c_vLabel [2] = pDoc->c_cliente;
	
	delta = (int) (m_ALMETRO-m_DALMETRO);
	layoutAD.c_vLabel [3] = delta;		// (int)maxValX;		// maxValX 
	layoutAD.c_vLabel [4] = pDoc->getRotolo();
	layoutAD.c_vLabel [5] = pDoc->c_lega;
	//CTime d(pDoc->startTime);
	// d tiene gia` i valori della data da trend Long
	layoutAD.c_vLabel [6] = d.Format( "%A, %B %d, %Y" );

	// int numClassi = ;
	layoutAD.setNumClassi(pDoc->c_difCoil.getNumClassi());
	layoutAD.setNumLabelClassi(pDoc->c_difCoil.getNumClassi());
	layoutAD.setNumCpu(pDoc->c_numCpu);

	// Inserire i quattro codici per quattro pagine di stampa
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		// fori codice
		int code = 'A'+j;

		trendAD[j].setPen (CSM20GetClassColor(j));		// era RGB (0,0,0) black
		trendAD[j].setPenLarge (CSM20GetClassColor(j));	// Black
		trendAD[j].setMode(LINEAR);	

		lineInfoAD[j].max = dMY[0]*1.25;
		lineInfoAD[j].min = 0.;

		// Determinazione dimensione asse X
		//int stepSize;
		double stepSize;
		stepSize = pDoc->GetTrendStep();	
		
		double deltaEnd = (pDoc->getVTotalLength() - m_ALMETRO)>0?(pDoc->getVTotalLength() - m_ALMETRO):0.;
		double deltaStart = m_DALMETRO;

		int deltaEndStep = (int)(deltaEnd/stepSize);
		int deltaStartStep = (int)(deltaStart/stepSize);
		
		int numStepX = (int)(pDoc->getVTotalLength()/stepSize);
			numStepX -= deltaEndStep;
		numStepX -= deltaStartStep;
		if (numStepX <= 0)
			numStepX = 1;

		// Numero step da disegnare
		lineInfoAD[j].count = (int)((((int)pDoc->getVTotalLength()/(int)stepSize) < numStepX)?
					(pDoc->getVTotalLength()/(int)stepSize) : numStepX); 

//		lineInfoAD[j].count = (pDoc->c_difCoil.getSize(3,code) < numStep)?
//			pDoc->c_difCoil.getSize(3,code) : numStep; 
	
		lineInfoAD[j].numStep = numStepX;
		lineInfoAD[j].actual	= numStepX ;  // pDoc->c_difCoil.getSize(0,code)% numStep;
		
		// Aggiornato 27-19-1997
		lineInfoAD[j].newVal (lineInfoAD[j].numStep+1);

		// |-------|++++++++++++++|---------------|
		// 0	  Dal			  Al             meter
		//               delta		  deltaEnd
		// getdensityLevel lavora su valori assoluti. 
		// il ssitema di disegno inverte rispetto alla lunghezza 
		// bisogna passare posizioni partire da Al indietro

		for (int i=0;i<= lineInfoAD[j].numStep;i++)
			{
			double val = 0.;
			// 26-01-06 
			// double pos = pDoc->getVTotalLength()-(double) i*stepSize;
			double pos = m_ALMETRO-(double) i*stepSize;
			val = pDoc->c_difCoil.getDensityLevel(code,pDoc->c_densityCalcLength,pos);
			if (val > pDoc->c_difCoil.getAllarme(code))
				val = 1.;
			else
				val = 0.;

			lineInfoAD[j].val[i] = val;

			//lineInfoAD[j].val[i] = pDoc->c_difCoil.getValAlarmNumLStep(code,i*(int)stepSize);
			}
		}	
	// Fine preparazione lineInfo
	
	// Show Progress
	frame->progressStepIt();
//-----------------------------------------------------------------------------------	
// Rapporto Compressione Asse

  // Show Progress
	frame->progressStepIt();

	c_layoutCompressABBig.setViewType(ROLLVIEW);
	c_layoutCompressABBig.setMode (LINEAR | NOLABELV);

	c_layoutCompressABBig.fCaption.size =  profile.getProfileInt("CompressReport","CaptionSize",120);
	c_layoutCompressABBig.fLabel.size = profile.getProfileInt("CompressReport","LabelSize",100);
	c_layoutCompressABBig.fNormal.size = profile.getProfileInt("CompressReport","NormalSize",110);
	
	// calcFirst banda and last
	// utilizzo posiz diaframma

	c_layoutCompressABBig.setNumSchede(pDoc->c_difCoil.getNumSchede(),
		(int )(((pDoc->c_trimLeft + pDoc->c_posDiaframmaSx)/SIZE_BANDE),
			(int)(pDoc->c_difCoil.getNumSchede() - ((pDoc->c_trimLeft+pDoc->c_posDiaframmaDx)/SIZE_BANDE))));	

	s = profile.getProfileString("CompressReport","NewLine","3,6");
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			c_layoutCompressABBig.c_nlAfterLabel [v] = 1;
		}

	s = profile.getProfileString("CompressReport","LabelOrder","0,1,2,3,4,5,6,7,8,9");
	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
		s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		if ((v < MAX_NUMLABEL)&&(v>=0))
			c_layoutCompressABBig.c_vOrderLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString("CompressReport","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");

	k = 0;
	while (s.GetLength() >0)
		{
		CString subS;
		subS = s.SpanExcluding(",;");
			s = s.Right(s.GetLength()-subS.GetLength()-1);
		int v;
		sscanf((LPCSTR)subS,"%d",&v);
		c_layoutCompressABBig.c_pxSepLabel [k++] = v;
		if (k>(MAX_NUMLABEL-1))
			break;
		}

	s = profile.getProfileString("CompressReport","SCaption",
							 "Compress ABCDBig");
	c_layoutCompressABBig.setCaption(s);
	c_layoutCompressABBig.sCaption.setFontInfo(c_layoutCompressABBig.fCaption);
	c_layoutCompressABBig.sCaption.set3Dlook(FALSE);

	// extra info on top right
	c_layoutCompressABBig.sLineName = pDoc->c_impianto;
	c_layoutCompressABBig.sDatetime = CTime(pDoc->c_startTime).Format("%d-%m-%Y %H:%M:%S");
	c_layoutCompressABBig.sMothercoil = pDoc->c_rotolo;

	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		c_layoutCompressABBig.c_sLabel[i].setFontInfo(c_layoutCompressABBig.fNormal);
		c_layoutCompressABBig.c_vLabel[i].setFontInfo(c_layoutCompressABBig.fNormal);
		c_layoutCompressABBig.c_vLabel[i].set3Dlook(TRUE);
		c_layoutCompressABBig.c_vLabel[i].setStringBase("999999");
		}


	for (int i=0;i<MAX_NUMLABEL;i++)
		{
		CString sIndex;
		sIndex.Format ("SLabel%d",i);
		s = profile.getProfileString("CompressReport",sIndex,sIndex);
		if (s != sIndex)
			{
			c_layoutCompressABBig.c_sLabel[i] = s;
			}
		}

	for (int i=0;(i<(pDoc->c_difCoil.getNumClassi()+1));i++)
		{
		CString s;
		if (c_reportClass[i])
			{
			s = TCHAR('A'+i);
			if (i == 4)
				s = "Big";
			c_layoutCompressABBig.c_cLabel[i] = s;
			c_layoutCompressABBig.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			c_layoutCompressABBig.c_cLabel[i].setTipoIcon(IPALLINO);
			}
		else
			{
			s = TCHAR(' ');
			if (i == 4)
				s = "";
			c_layoutCompressABBig.c_cLabel[i] = s;
			c_layoutCompressABBig.c_cLabel[i].setTColor(CSM20GetClassColor(i));
			c_layoutCompressABBig.c_cLabel[i].setTipoIcon(NOICON);
			}
		}
	// aggiunte label alzate
	StringLabelH strLV;
	// 28-11-08 fixBug clear previous rising
	c_layoutCompressABBig.clearStringLabelV();
	cnt = 0;
	{
	for (int i=0;i<pDoc->c_rotoloMapping.size();i++)
		{
		for (int j=0;j<pDoc->c_rotoloMapping[i].c_alzate.size();j++)
			{		
			if (cnt >= 20)
				break;
			if (j == 0)
				continue;
			if ((i == pDoc->c_rotoloMapping.size()-1)&&
				(j == pDoc->c_rotoloMapping[i].c_alzate.size()-1))
				continue;
			double dPos = pDoc->c_rotoloMapping[i].c_alzate[j].c_yPos/(m_ALMETRO-m_DALMETRO);
			// dPos espresso in % dello spazio video
			strLV.pos = dPos;
			strLV.label.Format("Ris%d",cnt+1);
			c_layoutCompressABBig.c_stringLabelV[cnt++] = strLV;
			}
		}
	}

	// CalcDrawRect
	c_layoutCompressABBig.c_viewTopPerc = profile.getProfileInt("CompressReport","ViewTop%",10);
	c_layoutCompressABBig.c_viewBottomPerc = profile.getProfileInt("CompressReport","ViewBottom%",25);
	c_layoutCompressABBig.c_viewLeftPerc = profile.getProfileInt("CompressReport","ViewLeft%",8);
	c_layoutCompressABBig.c_viewRightPerc = profile.getProfileInt("CompressReport","ViewRight%",5);

	
	// Test di congruenza valori visualizzazione
	if (m_ALMETRO > pDoc->c_difCoil.getMeter())
		m_ALMETRO = pDoc->c_difCoil.getMeter();

	if (m_DALMETRO > pDoc->c_difCoil.getMeter())
		m_DALMETRO = 0;

	if (m_DALMETRO > m_ALMETRO)
		m_DALMETRO = 0;

	// Set Values in string
	// Tarato su max value comunque
	if (m_ALMETRO == m_DALMETRO)
		m_ALMETRO = m_DALMETRO+1;

	c_layoutCompressABBig.c_vLabel [0] = (int )m_ALMETRO;
	c_layoutCompressABBig.c_vLabel [1] = (int )m_DALMETRO;
	c_layoutCompressABBig.c_vLabel [2] = pDoc->GetVCliente();
	
	delta = (int) (m_ALMETRO-m_DALMETRO);
	c_layoutCompressABBig.c_vLabel [3] = delta;		// (int)maxValX;		// maxValX 
	c_layoutCompressABBig.c_vLabel [4] = pDoc->getRotolo();
	c_layoutCompressABBig.c_vLabel [5] = pDoc->c_lega;
	//CTime d(pDoc->startTime);
	// d tiene gia` i valori della data da trend Long
	c_layoutCompressABBig.c_vLabel [6] = d.Format( "%A, %B %d, %Y" );

	// int numClassi = ;
	// aggiungo anche classe Big
	numClassi = pDoc->c_difCoil.getNumClassi() + 1;
	c_layoutCompressABBig.setNumClassi(numClassi);
	c_layoutCompressABBig.setNumLabelClassi(numClassi);
	c_layoutCompressABBig.c_indexClassi = 0;
	c_layoutCompressABBig.setMaxY ('A',m_DALMETRO);
	c_layoutCompressABBig.setMinY ('A',m_ALMETRO);

	// Preparazione TargetBoard
	c_targetBoardABBig.setEmpty();

	int c_pxBRoll = profile.getProfileInt("CompressReport","PxRollSize",10);

	for (int index=0;index <pDoc->c_difCoil.GetSize();index ++)
		{
		// cerco schede
		int mSize = pDoc->c_difCoil.ElementAt(index).GetSize();
		for (int scIndex=0;scIndex<mSize;scIndex ++)
			{
			// add BigHole enable => retportClass [4]
			if ((c_reportClass[4])&&
				(pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).BigHoleAt() > 0))
				{// found 
				double pos = pDoc->c_difCoil.ElementAt(index).posizione;
				if((pos >= m_DALMETRO)&&
					(pos <= m_ALMETRO))
					{					
					CTarget t;
					t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
					t.setSize (CSize(3*c_pxBRoll,5*c_pxBRoll));
					t.setColor (CSM20GetClassColor(4));
					// TRUE exclude left text
					c_targetBoardABBig.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t,TRUE);
					}
				}
			int clSize = pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).GetSize();
			for (int clIndex=(clSize-1);clIndex>=0;clIndex --)
				{// invertita priorita` di visualizzazione fori D + prioritari degli A
				// enable reportClass[clIndex]
				if ((c_reportClass[clIndex])&&
					(pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).ElementAt(clIndex) > 0))
					{
					// found 
					double pos = pDoc->c_difCoil.ElementAt(index).posizione;
					if((pos >= m_DALMETRO)&&
						(pos <= m_ALMETRO))
						{
						CTarget t;
						t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
						t.setSize (CSize(2*c_pxBRoll,3*c_pxBRoll));
						t.setColor (CSM20GetClassColor(clIndex));
						// TRUE exclude left text
						c_targetBoardABBig.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t,TRUE);
						}
					}
				}	
			}
		}
	 // Fine Rapporto Allarmi
	}

frame->progressDestroy();

return (retVal);
}

#define REP_PAGE_TRENDLONGIT	1
#define REP_PAGE_CROSSWEB		2
#define REP_PAGE_TRENDALARM		3
#define REP_PAGE_COMPRESS		4


// Routine di stampa equivalente di OnDraw
// Chiamata per ogni pagina di stampa
// pInfo.m_nCurPage [1 .. n] indica attuale pagina di stampa
void CInitView::OnPrint(CDC* pdc, CPrintInfo* pInfo) 
{
onPrint(pdc, pInfo); 
}


void CInit::onPrint(CDC* pdc, CPrintInfo* pInfo) 
{
CRect rectBorder;
// GetClientRect
CRect rcBounds = calcPrintInternalRect(pdc,pInfo->m_rectDraw,&rectBorder);

// draw border
CLineDoc* pDoc = (CLineDoc* )GetDoc();
CPen p,*op;
p.CreatePen (PS_SOLID,2,RGB(0,0,0));
op = pdc->SelectObject(&p);
pdc->MoveTo(rectBorder.left,rectBorder.top);
pdc->LineTo(rectBorder.right,rectBorder.top);
pdc->LineTo(rectBorder.right,rectBorder.bottom);
pdc->LineTo(rectBorder.left,rectBorder.bottom);
pdc->LineTo(rectBorder.left,rectBorder.top);
pdc->SelectObject (op);

prLimit = rcBounds;

int page = pInfo->m_nCurPage;

CFont fontNormal,*pold;
fontNormal.CreatePointFont (pagePr.getFontNormal()->size,pagePr.getFontNormal()->name,pdc);
pold = pdc->SelectObject(&fontNormal); 
CString s,strRes;
//s.Format ("Page %d",page);
pdc->SetTextAlign(TA_CENTER | TA_TOP);
strRes.LoadString (CSM_GRAPHVIEW_REPORTPAGINA);
s.Format (strRes,page);
CSize pagSize;
pagSize = pdc->GetTextExtent(s);
CPoint point;
// allineato al centro
point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;;
point.y = rectBorder.bottom + pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// data ora allineata a dx
CString formatRes;
formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
CTime t (pDoc->c_startTime);
s = t.Format(formatRes);
pdc->SetTextAlign(TA_RIGHT | TA_TOP);
point.x = rectBorder.right;
point.y = rectBorder.top - 2 * pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// allineato al centro
s = "File : ";
s += pDoc->getFileName();
pdc->SetTextAlign(TA_CENTER | TA_TOP);
point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;
point.y = rectBorder.top - 2 * pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// Cercafori allineato a sx
s = "Cercafori - EDS Srl";
pdc->SetTextAlign(TA_LEFT | TA_TOP);
point.x = rectBorder.left;
point.y = rectBorder.top - 2*pagSize.cy;
pdc->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

pdc->SelectObject(pold); 

// resto del report
pdc->SetTextAlign(TA_LEFT | TA_TOP);

if(c_doPrintStrip)
	{
	reportStrip(pdc,page,rcBounds,TRUE);
	return;
	}

// stampa Prima pagina
if (page == 1)
	{
	// stampa prima pagina
	reportRotolo2(pdc,rcBounds);
	return;
	}

int graphPageIndex=2;
// prima pagina grafica
if ((page == graphPageIndex)&&
	(c_reportGraph[0]))
	{
	// pagina grafica compressione
	pageGraphicPrint(pdc,REP_PAGE_COMPRESS,rcBounds);
	return;
	}
graphPageIndex += c_reportGraph[0];

// page 3
if ((page == graphPageIndex)&&
	(c_reportGraph[1]))
	{
	// pagina grafica trend longit
	pageGraphicPrint(pdc,REP_PAGE_TRENDLONGIT,rcBounds);
	return;
	}
graphPageIndex += c_reportGraph[1];

// page 4
if ((page == graphPageIndex)&&
	(c_reportGraph[2]))
	{
	// pagina grafica cross web
	pageGraphicPrint(pdc,REP_PAGE_CROSSWEB,rcBounds);
	return;
	}
graphPageIndex += c_reportGraph[2];

// page 5
if ((page == graphPageIndex)&&
	(c_reportGraph[3]))
	{
	// pagina grafica trend allarmi
	pageGraphicPrint(pdc,REP_PAGE_TRENDALARM,rcBounds);
	return;
	}
graphPageIndex += c_reportGraph[3];

// page 6
if ((c_numTextPage3)&&
	(page < graphPageIndex+c_numTextPage3))
	{
	// pagina testo alzate
	CPage*	ps= new CPage(pInfo->m_rectDraw,pdc,TRUE, MM_TEXT);
	// ps->SetPrinterMode(pdc,DMORIENT_LANDSCAPE);
	printTextPageEx3(ps,rcBounds,page-graphPageIndex);
	delete ps;	
	return;
	}
graphPageIndex += c_numTextPage3;

if ((c_numTextPage3+c_numTextPage4)&&
	(page < (graphPageIndex+c_numTextPage4)))
	{
	CPage*	ps= new CPage(pInfo->m_rectDraw,pdc,TRUE, MM_TEXT);
	printTextPageEx4(ps,rcBounds,page-(graphPageIndex));
	delete ps;	
	return;
	}
graphPageIndex += c_numTextPage4;

if ((c_numTextPage3+c_numTextPage4+c_numTextPage2)&&
	(page < (graphPageIndex+c_numTextPage2)))
	{
	CPage*	ps= new CPage(pInfo->m_rectDraw,pdc,TRUE, MM_TEXT);
	printTextPageEx2(ps,rcBounds,page-(graphPageIndex));
	delete ps;	
	return;
	}
graphPageIndex += c_numTextPage2;

if (pagePrEnd.getNumPage() > 0)
	{
	CLineDoc* pDoc = (CLineDoc* )GetDoc();
		
	// indice prima riga // era 16
	int startLine = (page - graphPageIndex - pagePr.getNumPage()) * LinePerPage;
	pagePrEnd.print (startLine,LinePerPage,rcBounds,pdc);
	return;
	}
}


void CInitView::OnFilePrint() 
{
// TODO: Add your command handler code here
CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();


if ((directPrint)||
	(c_isPreview))
	{// opzioni del report settate altrove
	if (c_isPreview)
		{
		// Clear autosave mode  
#ifndef Csm20El2013_REPORT
		// check pdfCreator Printer
		BOOL usePdfFile = FALSE;
		if (((CCSMApp*)AfxGetApp())->GetDefaultPrinter() == MICROSOFT_PDF_PRINTER)
			usePdfFile = TRUE;
		
#endif
		}
	else
		c_useManualReportConfig = FALSE;
	CPdfPrintView::OnFilePrint();	
	}
else
	{
	while (TRUE)
		{
		if (!c_isPrinting)
			{
			// CDSelReportType dialog;
			CDReportSheet reportSheet( "Printed Report" );
			CDReportSection sectionPage;
			CDReportParam	paramPage;
			CDReportPrint	printPage;

			printPage.c_pDoc = (CLineDoc* )GetDocument();

			reportSheet.AddPage(&printPage);
			reportSheet.AddPage(&paramPage);
			reportSheet.AddPage(&sectionPage);

			// load db settings
			DBReport dbReport(dBase);
			// dialog.c_pDoc = (CLineDoc* )GetDocument();
			if (dbReport.openSelectManual ())
				{
				// Move Init Data from Db to Dialog
				paramPage.m_Allarmi = dbReport.m_SOGLIE_ALLARME;
				// dialog.m_Cliente = dbReport.m_DATI_CLIENTE;
				// dialog.m_DataLavoraz = dbReport.m_DATA_LAVORAZIONE;
				paramPage.m_difettiPer = dbReport.m_DESCR_PERIODICI;
				paramPage.m_difettiRand = dbReport.m_DESCR_RANDOM;
				paramPage.m_alzate  = dbReport.m_DESCR_ALZATE;

				// dialog.m_Lunghezza = dbReport.m_LUNGHEZZA_BOBINA;
				paramPage.m_mappa = dbReport.m_MAPPA100;
				// Eliminato 
			//	dialog.m_alMetro = (int )dbReport.m_ALMETRO;
			//	dialog.m_dalMetro = (int )dbReport.m_DALMETRO;
				//-----------------
				paramPage.m_graph1 = dbReport.m_DESCR_GRAPH1;
				paramPage.m_graph2 = dbReport.m_DESCR_GRAPH2;
				paramPage.m_graph3 = dbReport.m_DESCR_GRAPH3;
				paramPage.m_graph4 = dbReport.m_DESCR_GRAPH4;

				paramPage.m_classA = dbReport.m_DESCR_CLASSA;
				paramPage.m_classB = dbReport.m_DESCR_CLASSB;
				paramPage.m_classC = dbReport.m_DESCR_CLASSC;
				paramPage.m_classD = dbReport.m_DESCR_CLASSD;
				paramPage.m_classBig = dbReport.m_DESCR_CLASSBIG;
				//----------------------------

				sectionPage.m_interv1 = (int )dbReport.m_INTERVALLO1;
				if (dbReport.m_INTERVALLO1 > 0.)
					sectionPage.m_enableInt1 = TRUE;
				sectionPage.m_interv2 = (int )dbReport.m_INTERVALLO2;
				if (dbReport.m_INTERVALLO2 > 0.)
					sectionPage.m_enableInt2 = TRUE;
				sectionPage.m_interv3 = (int )dbReport.m_INTERVALLO3;
				if (dbReport.m_INTERVALLO3 > 0.)
					sectionPage.m_enableInt3 = TRUE;
				sectionPage.m_interv4 = (int )dbReport.m_INTERVALLO4;
				if (dbReport.m_INTERVALLO4 > 0.)
					sectionPage.m_enableInt4 = TRUE;
				sectionPage.m_interv5 = (int )dbReport.m_INTERVALLO5;
				if (dbReport.m_INTERVALLO5 > 0.)
					sectionPage.m_enableInt5 = TRUE;
				sectionPage.m_interv6 = (int )dbReport.m_INTERVALLO6;
				if (dbReport.m_INTERVALLO6 > 0.)
					sectionPage.m_enableInt6 = TRUE;
				sectionPage.m_interv7 = (int )dbReport.m_INTERVALLO7;
				if (dbReport.m_INTERVALLO7 > 0.)
					sectionPage.m_enableInt7 = TRUE;
				sectionPage.m_interv8 = (int )dbReport.m_INTERVALLO8;
				if (dbReport.m_INTERVALLO8 > 0.)
					sectionPage.m_enableInt8 = TRUE;
				sectionPage.m_interv9 = (int )dbReport.m_INTERVALLO9;
				if (dbReport.m_INTERVALLO9 > 0.)
					sectionPage.m_enableInt9 = TRUE;
				sectionPage.m_interv10 = (int )dbReport.m_INTERVALLO10;
				if (dbReport.m_INTERVALLO10 > 0.)
					sectionPage.m_enableInt10 = TRUE;
				sectionPage.m_interv11 = (int )dbReport.m_INTERVALLO11;
				if (dbReport.m_INTERVALLO11 > 0.)
					sectionPage.m_enableInt11 = TRUE;
				sectionPage.m_interv12 = (int )dbReport.m_INTERVALLO12;
				if (dbReport.m_INTERVALLO12 > 0.)
					sectionPage.m_enableInt12 = TRUE;
				sectionPage.m_interv13 = (int )dbReport.m_INTERVALLO13;
				if (dbReport.m_INTERVALLO13 > 0.)
					sectionPage.m_enableInt13 = TRUE;
				sectionPage.m_interv14 = (int )dbReport.m_INTERVALLO14;
				if (dbReport.m_INTERVALLO14 > 0.)
					sectionPage.m_enableInt14 = TRUE;
				sectionPage.m_interv15 = (int )dbReport.m_INTERVALLO15;
				if (dbReport.m_INTERVALLO15 > 0.)
					sectionPage.m_enableInt15 = TRUE;
				sectionPage.m_interv16 = (int )dbReport.m_INTERVALLO16;
				if (dbReport.m_INTERVALLO16 > 0.)
					sectionPage.m_enableInt16 = TRUE;
				sectionPage.m_interv17 = (int )dbReport.m_INTERVALLO17;
				if (dbReport.m_INTERVALLO17 > 0.)
					sectionPage.m_enableInt17 = TRUE;
				sectionPage.m_interv18 = (int )dbReport.m_INTERVALLO18;
				if (dbReport.m_INTERVALLO18 > 0.)
					sectionPage.m_enableInt18 = TRUE;
				sectionPage.m_interv19 = (int )dbReport.m_INTERVALLO19;
				if (dbReport.m_INTERVALLO19 > 0.)
					sectionPage.m_enableInt19 = TRUE;

				if (reportSheet.DoModal() == IDOK)
					{
					try
						{
						dbReport.Edit();
						}
					catch (CDaoException *e)
						{
						AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
						dbReport.Close();
						return ;
						}

					dbReport.m_SOGLIE_ALLARME = paramPage.m_Allarmi;
					// dbReport.m_DATI_CLIENTE = dialog.m_Cliente;
					// dbReport.m_DATA_LAVORAZIONE = dialog.m_DataLavoraz;
					dbReport.m_DESCR_PERIODICI = paramPage.m_difettiPer;
					dbReport.m_DESCR_RANDOM = paramPage.m_difettiRand;
					dbReport.m_DESCR_ALZATE	= paramPage.m_alzate;
					// dbReport.m_LUNGHEZZA_BOBINA = dialog.m_Lunghezza;
					dbReport.m_MAPPA100 = paramPage.m_mappa;
			//		dbReport.m_ALMETRO = (double)dialog.m_alMetro;
			//		dbReport.m_DALMETRO = (double)dialog.m_dalMetro;
					//-----------------
					dbReport.m_DESCR_GRAPH1 = paramPage.m_graph1;
					dbReport.m_DESCR_GRAPH2 = paramPage.m_graph2;
					dbReport.m_DESCR_GRAPH3 = paramPage.m_graph3;
					dbReport.m_DESCR_GRAPH4 = paramPage.m_graph4;

					dbReport.m_DESCR_CLASSA = paramPage.m_classA;
					dbReport.m_DESCR_CLASSB = paramPage.m_classB;
					dbReport.m_DESCR_CLASSC = paramPage.m_classC;
					dbReport.m_DESCR_CLASSD = paramPage.m_classD;
					dbReport.m_DESCR_CLASSBIG = paramPage.m_classBig;
					//----------------------------

					if (sectionPage.m_enableInt1)
						dbReport.m_INTERVALLO1 = (double)sectionPage.m_interv1;
					else
						dbReport.m_INTERVALLO1 = 0.;
					if (sectionPage.m_enableInt2)
						dbReport.m_INTERVALLO2 = (double)sectionPage.m_interv2;
					else
						dbReport.m_INTERVALLO2 = 0.;
					if (sectionPage.m_enableInt3)
						dbReport.m_INTERVALLO3 = (double)sectionPage.m_interv3;
					else
						dbReport.m_INTERVALLO3 = 0.;
					if (sectionPage.m_enableInt4)
						dbReport.m_INTERVALLO4 = (double)sectionPage.m_interv4;
					else
						dbReport.m_INTERVALLO4 = 0.;
					if (sectionPage.m_enableInt5)
						dbReport.m_INTERVALLO5 = (double)sectionPage.m_interv5;
					else
						dbReport.m_INTERVALLO5 = 0.;
					if (sectionPage.m_enableInt6)
						dbReport.m_INTERVALLO6 = (double)sectionPage.m_interv6;
					else
						dbReport.m_INTERVALLO6 = 0.;
					if (sectionPage.m_enableInt7)
						dbReport.m_INTERVALLO7 = 
							(double)sectionPage.m_interv7;
					else
						dbReport.m_INTERVALLO7 = 0.;
					if (sectionPage.m_enableInt8)
						dbReport.m_INTERVALLO8 = 
							(double)sectionPage.m_interv8;
					else
						dbReport.m_INTERVALLO8 = 0.;
					if (sectionPage.m_enableInt9)
						dbReport.m_INTERVALLO9 = 
							(double)sectionPage.m_interv9;
					else
						dbReport.m_INTERVALLO9 = 0.;
					if (sectionPage.m_enableInt10)
						dbReport.m_INTERVALLO10 = 
							(double)sectionPage.m_interv10;
					else
						dbReport.m_INTERVALLO10 = 0.;
					if (sectionPage.m_enableInt11)
						dbReport.m_INTERVALLO11 = 
							(double)sectionPage.m_interv11;
					else
						dbReport.m_INTERVALLO11 = 0.;
					if (sectionPage.m_enableInt12)
						dbReport.m_INTERVALLO12 = 
							(double)sectionPage.m_interv12;
					else
						dbReport.m_INTERVALLO12 = 0.;
					if (sectionPage.m_enableInt13)
						dbReport.m_INTERVALLO13 = 
							(double)sectionPage.m_interv13;
					else
						dbReport.m_INTERVALLO13 = 0.;
					if (sectionPage.m_enableInt14)
						dbReport.m_INTERVALLO14 = 
							(double)sectionPage.m_interv14;
					else
						dbReport.m_INTERVALLO14 = 0.;
					if (sectionPage.m_enableInt15)
						dbReport.m_INTERVALLO15 = 
							(double)sectionPage.m_interv15;
					else
						dbReport.m_INTERVALLO15 = 0.;
					if (sectionPage.m_enableInt16)
						dbReport.m_INTERVALLO16 = 
							(double)sectionPage.m_interv16;
					else
						dbReport.m_INTERVALLO16 = 0.;
					if (sectionPage.m_enableInt17)
						dbReport.m_INTERVALLO17 = 
							(double)sectionPage.m_interv17;
					else
						dbReport.m_INTERVALLO17 = 0.;
					if (sectionPage.m_enableInt18)
						dbReport.m_INTERVALLO18 = 
							(double)sectionPage.m_interv18;
					else
						dbReport.m_INTERVALLO18 = 0.;
					if (sectionPage.m_enableInt19)
						dbReport.m_INTERVALLO19 = 
							(double)sectionPage.m_interv19;
					else
						dbReport.m_INTERVALLO19 = 0.;
					try
						{
						dbReport.Update();
						}
					catch (CDaoException *e)
						{
						AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
						dbReport.Close();
						return ;
						}
					dbReport.Close();
					}
				else
					//if (reportSheet.DoModal() != IDOK)
					{
					dbReport.Close();
					break;
					}
				}

#ifndef Csm20El2013_REPORT
			// Clear autosave mode  
			// check pdfCreator Printer
			frame->c_pdfInfo.loadFromProfile();
			BOOL usePdfFile = FALSE;
			if (((CCSMApp*)AfxGetApp())->GetDefaultPrinter() == MICROSOFT_PDF_PRINTER)
				usePdfFile = TRUE;
			//--------------------

#endif
			c_useManualReportConfig = TRUE;
			if (printPage.m_repType == 1)
				{// strip report 
				c_isPrinting = TRUE;
				c_doPrintStrip = TRUE;
				c_prElementNumber = printPage.c_elementIndex;
				c_prAlzataNumber = 	printPage.c_alzataIndex;
				c_prStripNumber = 	printPage.c_stripIndex;
				c_dalMetro = 0;
				c_alMetro = printPage.c_pDoc->getVTotalLength();
				CPdfPrintView::OnFilePrint();
				}
			else	
				{// report di rotolo
				c_doPrintStrip = FALSE;
				c_isPrinting = TRUE;
				c_dalMetro = printPage.m_posFrom;
				c_alMetro = printPage.m_posTo;
				CPdfPrintView::OnFilePrint();
				}
			}
		else
			// wait
			{	
			frame->Pump();
			}
		}
	}

#ifndef Csm20El2013_REPORT
	// check pdfCreator Printer

	BOOL usePdfFile = FALSE;
	if (((CCSMApp*)AfxGetApp())->GetDefaultPrinter() == MICROSOFT_PDF_PRINTER)
		usePdfFile = TRUE;
	
#endif
}

BOOL CInitView::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default

// Call True base class (white not gray)	
return CPdfPrintView::OnEraseBkgnd(pDC);
}


void CInitView::OnFilePrintPreview() 
{
onFilePrintPreview(); 
}

void CInit::onFilePrintPreview() 
{

// TODO: Add your command handler code here
directPrint = FALSE;
c_isPreview = TRUE;
//CDSelReportType dialog;
CDReportSheet	reportSheet( "Report Print Preview" );
CDReportSection sectionPage;
CDReportParam	paramPage;
CDReportPrint	printPage;

printPage.c_pDoc = (CLineDoc* )GetDoc();

reportSheet.AddPage(&printPage);
reportSheet.AddPage(&paramPage);
reportSheet.AddPage(&sectionPage);

// load db settings
DBReport dbReport(dBase);
// dBase already Open

while(TRUE)
	{
	if (!c_isPrinting)
		{
		if (dbReport.openSelectManual ())
			{
			// Move Init Data from Db to Dialog
			paramPage.m_Allarmi = dbReport.m_SOGLIE_ALLARME;
			// dialog.m_Cliente = dbReport.m_DATI_CLIENTE;
			// dialog.m_DataLavoraz = dbReport.m_DATA_LAVORAZIONE;
			paramPage.m_difettiPer = dbReport.m_DESCR_PERIODICI;
			paramPage.m_difettiRand = dbReport.m_DESCR_RANDOM;
			paramPage.m_alzate  = dbReport.m_DESCR_ALZATE;

			// dialog.m_Lunghezza = dbReport.m_LUNGHEZZA_BOBINA;
			paramPage.m_mappa = dbReport.m_MAPPA100;
			// Eliminato 
		//	dialog.m_alMetro = (int )dbReport.m_ALMETRO;
		//	dialog.m_dalMetro = (int )dbReport.m_DALMETRO;
			//-----------------
			paramPage.m_graph1 = dbReport.m_DESCR_GRAPH1;
			paramPage.m_graph2 = dbReport.m_DESCR_GRAPH2;
			paramPage.m_graph3 = dbReport.m_DESCR_GRAPH3;
			paramPage.m_graph4 = dbReport.m_DESCR_GRAPH4;

			paramPage.m_classA = dbReport.m_DESCR_CLASSA;
			paramPage.m_classB = dbReport.m_DESCR_CLASSB;
			paramPage.m_classC = dbReport.m_DESCR_CLASSC;
			paramPage.m_classD = dbReport.m_DESCR_CLASSD;
			paramPage.m_classBig = dbReport.m_DESCR_CLASSBIG;
			//----------------------------



			sectionPage.m_interv1 = (int )dbReport.m_INTERVALLO1;
			if (dbReport.m_INTERVALLO1 > 0.)
				sectionPage.m_enableInt1 = TRUE;
			sectionPage.m_interv2 = (int )dbReport.m_INTERVALLO2;
			if (dbReport.m_INTERVALLO2 > 0.)
				sectionPage.m_enableInt2 = TRUE;
			sectionPage.m_interv3 = (int )dbReport.m_INTERVALLO3;
			if (dbReport.m_INTERVALLO3 > 0.)
				sectionPage.m_enableInt3 = TRUE;
			sectionPage.m_interv4 = (int )dbReport.m_INTERVALLO4;
			if (dbReport.m_INTERVALLO4 > 0.)
				sectionPage.m_enableInt4 = TRUE;
			sectionPage.m_interv5 = (int )dbReport.m_INTERVALLO5;
			if (dbReport.m_INTERVALLO5 > 0.)
				sectionPage.m_enableInt5 = TRUE;
			sectionPage.m_interv6 = (int )dbReport.m_INTERVALLO6;
			if (dbReport.m_INTERVALLO6 > 0.)
				sectionPage.m_enableInt6 = TRUE;
			sectionPage.m_interv7 = (int )dbReport.m_INTERVALLO7;
			if (dbReport.m_INTERVALLO7 > 0.)
				sectionPage.m_enableInt7 = TRUE;
			sectionPage.m_interv8 = (int )dbReport.m_INTERVALLO8;
			if (dbReport.m_INTERVALLO8 > 0.)
				sectionPage.m_enableInt8 = TRUE;
			sectionPage.m_interv9 = (int )dbReport.m_INTERVALLO9;
			if (dbReport.m_INTERVALLO9 > 0.)
				sectionPage.m_enableInt9 = TRUE;
			sectionPage.m_interv10 = (int )dbReport.m_INTERVALLO10;
			if (dbReport.m_INTERVALLO10 > 0.)
				sectionPage.m_enableInt10 = TRUE;
			sectionPage.m_interv11 = (int )dbReport.m_INTERVALLO11;
			if (dbReport.m_INTERVALLO11 > 0.)
				sectionPage.m_enableInt11 = TRUE;
			sectionPage.m_interv12 = (int )dbReport.m_INTERVALLO12;
			if (dbReport.m_INTERVALLO12 > 0.)
				sectionPage.m_enableInt12 = TRUE;
			sectionPage.m_interv13 = (int )dbReport.m_INTERVALLO13;
			if (dbReport.m_INTERVALLO13 > 0.)
				sectionPage.m_enableInt13 = TRUE;
			sectionPage.m_interv14 = (int )dbReport.m_INTERVALLO14;
			if (dbReport.m_INTERVALLO14 > 0.)
				sectionPage.m_enableInt14 = TRUE;
			sectionPage.m_interv15 = (int )dbReport.m_INTERVALLO15;
			if (dbReport.m_INTERVALLO15 > 0.)
				sectionPage.m_enableInt15 = TRUE;
			sectionPage.m_interv16 = (int )dbReport.m_INTERVALLO16;
			if (dbReport.m_INTERVALLO16 > 0.)
				sectionPage.m_enableInt16 = TRUE;
			sectionPage.m_interv17 = (int )dbReport.m_INTERVALLO17;
			if (dbReport.m_INTERVALLO17 > 0.)
				sectionPage.m_enableInt17 = TRUE;
			sectionPage.m_interv18 = (int )dbReport.m_INTERVALLO18;
			if (dbReport.m_INTERVALLO18 > 0.)
				sectionPage.m_enableInt18 = TRUE;
			sectionPage.m_interv19 = (int )dbReport.m_INTERVALLO19;
			if (dbReport.m_INTERVALLO19 > 0.)
				sectionPage.m_enableInt19 = TRUE;

			if (reportSheet.DoModal() == IDOK)
				{
				try
					{
					dbReport.Edit();
					}
				catch (CDaoException *e)
					{
					AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
					dbReport.Close();
					return ;
					}

				dbReport.m_SOGLIE_ALLARME = paramPage.m_Allarmi;
				// dbReport.m_DATI_CLIENTE = dialog.m_Cliente;
				// dbReport.m_DATA_LAVORAZIONE = dialog.m_DataLavoraz;
				dbReport.m_DESCR_PERIODICI = paramPage.m_difettiPer;
				dbReport.m_DESCR_RANDOM = paramPage.m_difettiRand;
				dbReport.m_DESCR_ALZATE	= paramPage.m_alzate;
				// dbReport.m_LUNGHEZZA_BOBINA = dialog.m_Lunghezza;
				dbReport.m_MAPPA100 = paramPage.m_mappa;
		//		dbReport.m_ALMETRO = (double)dialog.m_alMetro;
		//		dbReport.m_DALMETRO = (double)dialog.m_dalMetro;
				//-----------------
				dbReport.m_DESCR_GRAPH1 = paramPage.m_graph1;
				dbReport.m_DESCR_GRAPH2 = paramPage.m_graph2;
				dbReport.m_DESCR_GRAPH3 = paramPage.m_graph3;
				dbReport.m_DESCR_GRAPH4 = paramPage.m_graph4;

				dbReport.m_DESCR_CLASSA = paramPage.m_classA;
				dbReport.m_DESCR_CLASSB = paramPage.m_classB;
				dbReport.m_DESCR_CLASSC = paramPage.m_classC;
				dbReport.m_DESCR_CLASSD = paramPage.m_classD;
				dbReport.m_DESCR_CLASSBIG = paramPage.m_classBig;
				//----------------------------

				if (sectionPage.m_enableInt1)
					dbReport.m_INTERVALLO1 = (double)sectionPage.m_interv1;
				else
					dbReport.m_INTERVALLO1 = 0.;
				if (sectionPage.m_enableInt2)
					dbReport.m_INTERVALLO2 = (double)sectionPage.m_interv2;
				else
					dbReport.m_INTERVALLO2 = 0.;
				if (sectionPage.m_enableInt3)
					dbReport.m_INTERVALLO3 = (double)sectionPage.m_interv3;
				else
					dbReport.m_INTERVALLO3 = 0.;
				if (sectionPage.m_enableInt4)
					dbReport.m_INTERVALLO4 = (double)sectionPage.m_interv4;
				else
					dbReport.m_INTERVALLO4 = 0.;
				if (sectionPage.m_enableInt5)
					dbReport.m_INTERVALLO5 = (double)sectionPage.m_interv5;
				else
					dbReport.m_INTERVALLO5 = 0.;
				if (sectionPage.m_enableInt6)
					dbReport.m_INTERVALLO6 = (double)sectionPage.m_interv6;
				else
					dbReport.m_INTERVALLO6 = 0.;
				if (sectionPage.m_enableInt7)
					dbReport.m_INTERVALLO7 = 
						(double)sectionPage.m_interv7;
				else
					dbReport.m_INTERVALLO7 = 0.;
				if (sectionPage.m_enableInt8)
					dbReport.m_INTERVALLO8 = 
						(double)sectionPage.m_interv8;
				else
					dbReport.m_INTERVALLO8 = 0.;
				if (sectionPage.m_enableInt9)
					dbReport.m_INTERVALLO9 = 
						(double)sectionPage.m_interv9;
				else
					dbReport.m_INTERVALLO9 = 0.;
				if (sectionPage.m_enableInt10)
					dbReport.m_INTERVALLO10 = 
						(double)sectionPage.m_interv10;
				else
					dbReport.m_INTERVALLO10 = 0.;
				if (sectionPage.m_enableInt11)
					dbReport.m_INTERVALLO11 = 
						(double)sectionPage.m_interv11;
				else
					dbReport.m_INTERVALLO11 = 0.;
				if (sectionPage.m_enableInt12)
					dbReport.m_INTERVALLO12 = 
						(double)sectionPage.m_interv12;
				else
					dbReport.m_INTERVALLO12 = 0.;
				if (sectionPage.m_enableInt13)
					dbReport.m_INTERVALLO13 = 
						(double)sectionPage.m_interv13;
				else
					dbReport.m_INTERVALLO13 = 0.;
				if (sectionPage.m_enableInt14)
					dbReport.m_INTERVALLO14 = 
						(double)sectionPage.m_interv14;
				else
					dbReport.m_INTERVALLO14 = 0.;
				if (sectionPage.m_enableInt15)
					dbReport.m_INTERVALLO15 = 
						(double)sectionPage.m_interv15;
				else
					dbReport.m_INTERVALLO15 = 0.;
				if (sectionPage.m_enableInt16)
					dbReport.m_INTERVALLO16 = 
						(double)sectionPage.m_interv16;
				else
					dbReport.m_INTERVALLO16 = 0.;
				if (sectionPage.m_enableInt17)
					dbReport.m_INTERVALLO17 = 
						(double)sectionPage.m_interv17;
				else
					dbReport.m_INTERVALLO17 = 0.;
				if (sectionPage.m_enableInt18)
					dbReport.m_INTERVALLO18 = 
						(double)sectionPage.m_interv18;
				else
					dbReport.m_INTERVALLO18 = 0.;
				if (sectionPage.m_enableInt19)
					dbReport.m_INTERVALLO19 = 
						(double)sectionPage.m_interv19;
				else
					dbReport.m_INTERVALLO19 = 0.;
				try
					{
					dbReport.Update();
					}
				catch (CDaoException *e)
					{
					AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
					dbReport.Close();
					return ;
					}
				dbReport.Close();
				}
			else
				//if (reportSheet.DoModal() != IDOK)
				{
				dbReport.Close();
				break;
				}

			c_useManualReportConfig = TRUE;
			if (printPage.m_repType == 1)
				{// strip report 
				c_isPrinting = TRUE;
				c_doPrintStrip = TRUE;
				c_prElementNumber = printPage.c_elementIndex;
				c_prAlzataNumber = 	printPage.c_alzataIndex;
				c_prStripNumber = 	printPage.c_stripIndex;
				//26-01-06
				c_dalMetro = 0;
				c_alMetro = printPage.c_pDoc->getVTotalLength();
				OnFilePrPview();
				}
			else	
				{// report di rotolo
				c_doPrintStrip = FALSE;
				c_isPrinting = TRUE;
				//26-01-06
				c_dalMetro = printPage.m_posFrom;
				c_alMetro = printPage.m_posTo;
				OnFilePrPview();
				}
			}
		}
	else
		{// wait
		CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
		frame->Pump();
		}
	}

directPrint = FALSE;
c_isPreview = FALSE;
}

void CInitView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
onEndPrinting(pDC,pInfo); 
}

void CInit::onEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
// TODO: Add your specialized code here and/or call the base class

// Aggiornato 27-19-1997

// Trend Longitudinale
if (lineInfo[0].val != NULL)
	{
	delete [] lineInfo[0].val;
	lineInfo[0].val=NULL;
	}

if (lineInfo[1].val != NULL)
	{
	delete [] lineInfo[1].val;
	lineInfo[1].val=NULL;
	}

if (lineInfo[2].val != NULL)
	{
	delete [] lineInfo[2].val;
	lineInfo[2].val=NULL;
	}

if (lineInfo[3].val != NULL)
	{
	delete [] lineInfo[3].val;
	lineInfo[3].val=NULL;
	}

// Allarme Densita` 
if (lineInfoAD[0].val != NULL)
	{
	delete [] lineInfoAD[0].val;
	lineInfoAD[0].val=NULL;
	}

if (lineInfoAD[1].val != NULL)
	{
	delete [] lineInfoAD[1].val;
	lineInfoAD[1].val=NULL;
	}

if (lineInfoAD[2].val != NULL)
	{
	delete [] lineInfoAD[2].val;
	lineInfoAD[2].val=NULL;
	}

if (lineInfoAD[3].val != NULL)
	{
	delete [] lineInfoAD[3].val;
	lineInfoAD[3].val=NULL;
	}

// Qualita`  
/*-------------------------------
if (lineInfoQ[0].val != NULL)
	{
	delete [] lineInfoQ[0].val;
	lineInfoQ[0].val=NULL;
	}

if (lineInfoQ[1].val != NULL)
	{
	delete [] lineInfoQ[1].val;
	lineInfoQ[1].val=NULL;
	}

if (lineInfoQ[2].val != NULL)
	{
	delete [] lineInfoQ[2].val;
	lineInfoQ[2].val=NULL;
	}

if (lineInfoQ[3].val != NULL)
	{
	delete [] lineInfoQ[3].val;
	lineInfoQ[3].val=NULL;
	}
---------------------------------*/

c_isPrinting = FALSE;

}

void CInitView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
CLineDoc* pDoc = (CLineDoc* )GetDocument();
pCmdUI->Enable(pDoc->loadedData);
	
}

void CInitView::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here

CLineDoc* pDoc = (CLineDoc* )GetDocument();
pCmdUI->Enable(pDoc->loadedData);
	
}



void CInitView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
if (pDC->IsPrinting())
	{
	CSize printSize;
	printSize.cx = pDC->GetDeviceCaps(HORZRES);
	printSize.cy = pDC->GetDeviceCaps(VERTRES);
	pDC->SetViewportExt(printSize.cx,printSize.cy);
	}

CPdfPrintView::OnPrepareDC(pDC, pInfo);
}


int  PagePrinter::print(int index,int numLine,CRect &rectB,CDC* pdc)
{
TEXTMETRIC tm;
CPoint p;

int lineCaption = 0;

if (index < 0)
	return 0;

CFont fontNormal,*pold;
CFont fontCaption;

fontNormal.CreatePointFont (fNormal.size,fNormal.name,pdc);
pold = pdc->SelectObject(&fontNormal); 
pdc->GetTextMetrics(&tm);
pdc->SetBkMode (TRANSPARENT);


p.x = rectB.left + tm.tmAveCharWidth;
p.y = rectB.top + tm.tmHeight;

if (index >= page.GetSize())
	return 0;
int i=0;
for (i=index;i<index+numLine;i++)
	{

	if (i >= page.GetSize())
		break;

	pdc->SetTextAlign(page[i].flags);
	p.y += tm.tmHeight + tm.tmExternalLeading;
	p.y += (tm.tmHeight + tm.tmExternalLeading)/6;

	int x;
	if (page[i].flags & TA_CENTER)
		{
		x = rectB.left + (rectB.right - rectB.left)/2;
 		}
	else
		x = p.x;

	pdc->ExtTextOut (x,p.y,0,
		NULL,page[i].str,page[i].str.GetLength()-2,NULL); // Skip Cr/Lf

	if (p.y > (rectB.bottom - 4*tm.tmHeight))
		break;
	
	}

// Clip rectB for other printing
rectB.top = p.y;
pdc->SelectObject(pold); 

return(i);
}


void CInit::pageGraphicPrint(CDC* pdc,int curPage,CRect &rcBounds)
{

CRect rectB;
CLineDoc* pDoc = (CLineDoc* )GetDoc();

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

switch (curPage)
	{
   	case 1:
		{
		// Disegno
		// Set base Rect
		layoutL.setDrawRect(rcBounds);
		int numClassi = layoutL.c_numClassi;
		for (int i=0;i<numClassi;i++)
			{
			double al;
			al = pDoc->getAlTrend10m('A'+i);
			if (al <= 0.)
				al = 0.;
			layoutL.setAlValY('A'+i,pDoc->getAlTrend10m('A'+i));
			}	

		layoutL.draw(pdc);
		// Trovo Finestra disegno (grigia)
		layoutL.CalcDrawRect (rcBounds,rectB);
		// Disegno	
		CRect subRectB[4];
		for (int i=0;i<numClassi;i++)
			{
			if(!c_reportClass[i])continue;
			trendLine[i].setMode (LINEAR);
			subRectB[i]=rectB;
			subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
			subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
			trendLine[i].setScale(subRectB[i],lineInfo[i]);
			trendLine[i].draw(pdc);
			}
		}
		break;
	case 2:
		{
		// Disegno
		// Set base Rect
		
		layoutD.setDrawRect(rcBounds);
		// 03-10-07
		// Disegno allarme densita` 1 cm
		double ald1cm = pDoc->getAlDens1Cm();
		layoutD.setAlValY('A'+0,ald1cm);
		//-------
		layoutD.draw(pdc);
		// Trovo Finestra disegno (grigia)
		layoutD.CalcDrawRect (rcBounds,rectB);

		// Disegno
		CLineDoc* pDoc = (CLineDoc* )GetDoc();
		CRect clientRect;
		int Xsize = (rectB.right - rectB.left)/pDoc->c_difCoil.getNumSchede();
		clientRect = rectB;

		clientRect.right = clientRect.left + Xsize;

// Disegno posizione bobine

		//-------------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	{
	// valori per disegnare asse mediano
	int leftPos,rightPos;
	CPen pen0,pen1,*oldPen;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
	oldPen = pdc->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	// double globalSize = 180 * SIZE_BANDE;
	// fixBug per sistema doppia configurazione
	// double globalSize = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left + layoutD.c_sizeFBanda+ (1.  * posLeft * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));
	// sinistra per disegnare asse mediano
	leftPos = scPosLeft;

	//pdc->MoveTo(scPosLeft,rectB.top);
	//pdc->LineTo(scPosLeft,rectB.bottom);
	// disegno solo inizio strip 
	// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	for (int i=(pDoc->c_rightAlignStrip)?pDoc->c_rotoloMapping[lastElem].size()-1:0;
			(pDoc->c_rightAlignStrip)?i>=0:i<pDoc->c_rotoloMapping[lastElem].size();
			(pDoc->c_rightAlignStrip)?i--:i++)
		{
		// prima riga sx
		pdc->SelectObject(&pen0);	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// check distanza con fine strip precedente
		if ((pDoc->c_rightAlignStrip)?(i<pDoc->c_rotoloMapping[lastElem].size()-1):(i > 0))
			{
			posLeft -= (fabs(posLeft - endPosLastStrip)/2);
			}
		int cell = posLeft/SIZE_BANDE;
		if (pDoc->c_rightAlignStrip)
			cell ++;

		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos + 
					+ (((pDoc->c_rightAlignStrip)?-1:1) * (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		if (pDoc->c_rightAlignStrip)
			// scPosLeft = rectB.right - layoutD.c_sizeFBanda  - (1.  * (globalSize-posLeft) * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));
			scPosLeft = rectB.left + layoutD.c_sizeFBanda  + (1.  * (posLeft) * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + layoutD.c_sizeFBanda  + (1.  * posLeft * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));

		pdc->MoveTo(scPosLeft,rectB.top);
		pdc->LineTo(scPosLeft,rectB.bottom);

		// prima riga
		if (((pDoc->c_rightAlignStrip)&&
			(i == (pDoc->c_rotoloMapping[lastElem].size()-1)))||
			((!pDoc->c_rightAlignStrip)&&
			(i == 0)))
			{
			CFont f,*oldF;
			f.CreatePointFont (70,"Arial",pdc);
			oldF=pdc->SelectObject(&f); 
			TEXTMETRIC tm;
			pdc->GetTextMetrics(&tm);
			CString sl;
			if (pDoc->c_rightAlignStrip)
				pdc->SetTextAlign(TA_LEFT  | TA_TOP);
			else
				pdc->SetTextAlign(TA_RIGHT | TA_TOP);
			
			int dcmode = pdc->SetBkMode (TRANSPARENT);
			sl.Format("First Cell %d",cell);
			COLORREF  oldColor;
			oldColor = pdc->GetTextColor();
			pdc->SetTextColor(RGB(255,0,0));
			pdc->TextOut(scPosLeft,rectB.bottom + tm.tmHeight/2,(LPCSTR)sl);	
			pdc->SetTextColor(oldColor);
			pdc->SelectObject(oldF); 
			}

		// ultima riga
		if( ((pDoc->c_rightAlignStrip)&&
			(i == 0))||
			((!pDoc->c_rightAlignStrip)&&
			(i == (pDoc->c_rotoloMapping[lastElem].size()-1)))
			||
			(pDoc->c_viewStripBorder))
			{
			// seconda riga
			pdc->SelectObject(&pen1);	
			posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			if(pDoc->c_rightAlignStrip)
				{
				posLeft -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
				cell = posLeft/SIZE_BANDE;
				}
			else
				{
				posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
				cell = posLeft/SIZE_BANDE;
				}

		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
			if (pDoc->c_rightAlignStrip)
				// scPosLeft = rectB.right - layoutD.c_sizeFBanda - (1.  * (globalSize-posLeft) * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));
				scPosLeft = rectB.left + layoutD.c_sizeFBanda + (1.  * (posLeft) * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));
			else	
				scPosLeft = rectB.left + layoutD.c_sizeFBanda + (1.  * posLeft * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));

			pdc->MoveTo(scPosLeft,rectB.top);
			pdc->LineTo(scPosLeft,rectB.bottom);
			if( ((pDoc->c_rightAlignStrip)&&
			(i == 0))||
			((!pDoc->c_rightAlignStrip)&&
			(i == (pDoc->c_rotoloMapping[lastElem].size()-1))))
				{
				CFont f,*oldF;
				f.CreatePointFont (70,"Arial",pdc);
				oldF=pdc->SelectObject(&f); 
				TEXTMETRIC tm;
				pdc->GetTextMetrics(&tm);
				CString sl;
				if (pDoc->c_rightAlignStrip)
					pdc->SetTextAlign(TA_RIGHT  | TA_TOP);
				else
					pdc->SetTextAlign(TA_LEFT  | TA_TOP);
				COLORREF oldColor;
				oldColor = pdc->GetTextColor();
				pdc->SetTextColor(RGB(255,0,0));
				int dcmode = pdc->SetBkMode (TRANSPARENT);
				sl.Format("Last Cell %d",cell);

				pdc->TextOut(scPosLeft,rectB.bottom + tm.tmHeight/2,(LPCSTR)sl);	
				pdc->SetTextColor(oldColor);
				pdc->SelectObject(oldF); 
				}
			}
		}
	// destra per disegnare asse mediano
	if (pDoc->c_rightAlignStrip)
		{
		leftPos = (pDoc->c_rotoloMapping[lastElem])[0].c_xPos-(pDoc->c_rotoloMapping[lastElem])[0].c_larghezza;
		rightPos = (pDoc->c_rotoloMapping[lastElem])[pDoc->c_rotoloMapping[lastElem].size()-1].c_xPos;
		}
	else
		{
		leftPos = (pDoc->c_rotoloMapping[lastElem])[0].c_xPos;
		rightPos = (pDoc->c_rotoloMapping[lastElem])[pDoc->c_rotoloMapping[lastElem].size()-1].c_xPos+(pDoc->c_rotoloMapping[lastElem])[pDoc->c_rotoloMapping[lastElem].size()-1].c_larghezza;
		}
	int scMediumPos = rectB.left + layoutD.c_sizeFBanda  + (1.  * ((leftPos+rightPos)/2) * (double)layoutD.c_sizeBanda / (layoutD.c_bandePerCol*SIZE_BANDE));

	pdc->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// end clear
	pen1.CreatePen(PS_DASH,1,RGB(0,0,0));
	oldPen = pdc->SelectObject(&pen1);
	pdc->MoveTo(scMediumPos,rectB.top);
	pdc->LineTo(scMediumPos,rectB.bottom);
	pdc->SelectObject(oldPen);
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
	
		double maxD = pDoc->c_difCoil.getMaxDensFromTo((int )m_DALMETRO,(int )m_ALMETRO);
		if (maxD == 0.)
			maxD = 1.;
		for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
			{
			double *dVal = new double [pDoc->c_difCoil.getNumClassi()];
			for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
				{
				if(c_reportClass[j])
					{
					int ferma =0;
					if (i == 40)
						ferma =1;
					// Sostituita densita` con totali per classi
					// dVal[j] = pDoc->c_difCoil.getValDensita(i,'A'+j,pDoc->GetTrendDScalaX());
					dVal[j] = pDoc->c_difCoil.getTotDifetti(i,'A'+j,(int )m_DALMETRO,
						(int )m_ALMETRO);
					}
				else
					dVal[j] = 0;
				}

			clientRect.left  = rectB.left + (int)(layoutD.c_sizeFBanda + Xsize * i);
			clientRect.right = rectB.left + (int)(layoutD.c_sizeFBanda + Xsize * (i+1));	

			densView[i].setScale(clientRect,maxD*1.25,dVal);
			densView[i].setPos(i+1);
	 		densView[i].draw(pdc);
			delete [] dVal;
			}
		}
		break;
	// Allarme Densita 3
	case 3:
		{
		// Disegno
		// Set base Rect
		layoutAD.setDrawRect(rcBounds);

		layoutAD.draw(pdc);
		// Trovo Finestra disegno (grigia)
		layoutAD.CalcDrawRect (rcBounds,rectB);
		// Disegno	
		CRect subRectB[4];
		int numClassi = layoutAD.c_numClassi;
		for (int i=0;i<numClassi;i++)
			{
			if(!c_reportClass[i])continue;
			trendAD[i].setMode (LINEAR);
			subRectB[i]=rectB;
			subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
			subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
			trendAD[i].setScale(subRectB[i],lineInfoAD[i]);
			trendAD[i].draw(pdc);
			}

		 }
		break;
	//  4
	// Compressione A,B,C,B,Big
	case 4:
		{
		// Disegno
		int c_pxBRoll = 10;
		// Set base Rect
		c_layoutCompressABBig.setDrawRect(rcBounds);
		c_layoutCompressABBig.c_useClassicPallini = FALSE;
		c_layoutCompressABBig.c_reverseLabel = (pDoc->c_rightAlignStrip);
		c_layoutCompressABBig.draw(pdc);
		// Trovo Finestra disegno (grigia)
		c_layoutCompressABBig.CalcDrawRect (rcBounds,rectB);
		// Disegno	??
		c_targetBoardABBig.setDrawAttrib((int)c_layoutCompressABBig.c_sizeFBanda,
								(int)c_layoutCompressABBig.c_sizeBanda,
								c_pxBRoll,c_layoutCompressABBig.c_bandePerCol,FALSE,FALSE);
		CRect clipRect(rectB);
		clipRect.TopLeft().x = rcBounds.TopLeft().x;
		clipRect.BottomRight().x = rcBounds.BottomRight().x;
	//	pdc->IntersectClipRect (clipRect);

		// Rettangoli Difetti di laminazione
		{
		BOOL laminationDef = FALSE;
		int lastElem;
		lastElem = pDoc->c_rotoloMapping.c_lastElemento;
		CPen penR,*oldPen;
		// Red pen
		penR.CreatePen(PS_SOLID,1,RGB(255,0,0));
		oldPen = pdc->SelectObject(&penR);
		int leftPos=0;
		int rightPos=0;
		double posLeft=0.;
		int scPosLeft=0;
		double posRight=0;
		int scPosRight=0;

		for (int j=0;j<pDoc->c_difCoil.GetCount();j++)
			{// per ogni metro
			double endPosLastStrip = 0.;
			for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
				{// per ogni strip
				// uso markercoil che ha una banda per ogni strip
				// int sizeFB = c_layoutCompressABBig.c_sizeFBanda;
				int sizeFB = 0;
				CRect r = getRectStrip(lastElem,0,i);
				int bsx = r.left;
				int bdx = r.right;

				laminationDef = pDoc->c_difCoil[j].existLaminDef(bsx,bdx);
				if (laminationDef)
					{// ok disegno rettangolo
					if (pDoc->c_rightAlignStrip)
						{// se allineato a dx larghezze crescono verso sinsitra
						posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos - (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
						endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
						}
					else
						{
						posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
						endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
									+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
						}
				// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
					scPosLeft = rectB.left+c_layoutCompressABBig.c_sizeFBanda  + 
						(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

					int scPosRight = rectB.left+c_layoutCompressABBig.c_sizeFBanda  + 
						(1.  * (posLeft + (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
					// ok ora posizione longitudinale, exception: index out of range 
					// double dPos = pDoc->c_markerCoil[j].posizione; sostituito difCoil a markerCoil 07-02-19
					double dPos = pDoc->c_difCoil[j].posizione;
					double dTop = 0;
					double dBot = pDoc->c_difCoil.getMeter();
					int scPosT = rectB.top + dPos * (rectB.bottom - rectB.top)/(dBot-dTop);
					int scPosB =	scPosT + 7*c_pxBRoll;
					scPosT -=	2*c_pxBRoll;
					// rectB.top + (dPos+dLength/2) * (rectB.bottom - rectB.top)/(dBot-dTop);

					pdc->Rectangle(scPosLeft,scPosT,scPosRight,scPosB);					
					}
				}
			}

		pdc->SelectObject(oldPen);
		penR.DeleteObject();
		}
		c_targetBoardABBig.plot(rectB,pdc,c_layoutCompressABBig.getMaxY('A'),
						c_layoutCompressABBig.getMinY('A'));

#ifdef USE_ELEMENT_COMPRESS
//-------------------------------------------------------------
// Disegno Posizione bobine allineate a sx
		CPen pen0,pen1,pen2,*oldPen;
		{
		// valori per disegnare asse mediano
		int leftPos,rightPos;
		// light grey
		//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
		pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
		// red
		// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
		pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
		pen2.CreatePen(PS_DASH,1,RGB(0,0,0));
		oldPen = pdc->SelectObject(&pen0);
		// int lastElem;
		// lastElem = pDoc->c_rotoloMapping.c_lastElemento;
		for(int lastElem=0;lastElem <= pDoc->c_rotoloMapping.c_lastElemento;lastElem++)
			{
			double posLeft;
			posLeft = pDoc->getPosColtello(lastElem);
			// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
			int scPosLeft = rectB.left +c_layoutCompressABBig.c_sizeFBanda +
				(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
			// sinistra per disegnare asse mediano
			leftPos = scPosLeft;

			// 12-02-2013 top e bottom y da ogni elemento
			double posTop = pDoc->c_rotoloMapping[lastElem].getYPos(0);
			// ultimo
			double posBot = pDoc->c_rotoloMapping[lastElem].getYPos(-1);

			posTop *= (double)(rectB.bottom-rectB.top);
			posTop /= pDoc->c_difCoil.getMeter();
			posTop += rectB.top;
			posBot *= (double)(rectB.bottom-rectB.top);
			posBot /= pDoc->c_difCoil.getMeter();
			posBot += rectB.top;
		
			int scPosTop = (int) posTop;
			int scPosBot = (int) posBot;
			//-------

			//pdc->MoveTo(scPosLeft,rectB.top);
			//pdc->LineTo(scPosLeft,rectB.bottom);
			pdc->MoveTo(scPosLeft,scPosTop);
			pdc->LineTo(scPosLeft,scPosBot);
			// disegno solo inizio strip 
			// fine strip solo per ultimo strip
			double endPosLastStrip = 0.;
			for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
				{
				// prima riga sx
				pdc->SelectObject(&pen0);	
				posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;

				// check distanza con fine strip precedente ?? a che serve 
				if ( 0)
					{
					posLeft -= (fabs(posLeft - endPosLastStrip)/2);
					}

				// memo  last end position
				endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
							+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
				// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
				scPosLeft = rectB.left+c_layoutCompressABBig.c_sizeFBanda  + 
					(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
				int scPosRight = rectB.left+c_layoutCompressABBig.c_sizeFBanda  + 
					(1.  * (posLeft + (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

				// pdc->MoveTo(scPosLeft,rectB.top);
				// pdc->LineTo(scPosLeft,rectB.bottom);
				pdc->MoveTo(scPosLeft,scPosTop);
				pdc->LineTo(scPosLeft,scPosBot);
//----------------------------------------------------------
			// Elval specific
			// riferimenti numerici in testa agli strip
			CString sl;
			int scPosText = scPosLeft + (scPosRight - scPosLeft)/2;
			pdc->SetTextAlign(TA_CENTER | TA_BOTTOM);
			int dcmode = pdc->SetBkMode (TRANSPARENT);
			sl.Format("%d",i+1);
			pdc->TextOut(scPosText,rectB.top,(LPCSTR)sl);	
			// Larghezze degli strip in fondo 
			pdc->SetTextAlign(TA_CENTER | TA_TOP);
			sl.Format("%3.0lf",(pDoc->c_rotoloMapping[lastElem])[i].c_larghezza);
			pdc->TextOut(scPosText,rectB.bottom+5,(LPCSTR)sl);	
			pdc->SetBkMode (dcmode);
// ----------------------------------------------

				if((i == (pDoc->c_rotoloMapping[lastElem].size()-1))||
					(pDoc->c_viewStripBorder))
					{
					// seconda riga
					pdc->SelectObject(&pen1);	
					posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
					posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

				// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
					scPosLeft = rectB.left +c_layoutCompressABBig.c_sizeFBanda + 
						(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda 
						/ (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

					// pdc->MoveTo(scPosLeft,rectB.top);
					// pdc->LineTo(scPosLeft,rectB.bottom);
					pdc->MoveTo(scPosLeft,scPosTop);
					pdc->LineTo(scPosLeft,scPosBot);
					}
				}

			// destra per disegnare asse mediano
			rightPos = scPosLeft;
		
			//-----------------------------
		
			// end clear
			pdc->SelectObject(&pen2);
			int mediumPos = leftPos+(rightPos-leftPos)/2;
			// pdc->MoveTo(mediumPos,rectB.top);
			// pdc->LineTo(mediumPos,rectB.bottom);
			pdc->MoveTo(mediumPos,scPosTop);
			pdc->LineTo(mediumPos,scPosBot);

			}

		pdc->SelectObject(oldPen);
		pen0.DeleteObject();
		pen1.DeleteObject();
		pen2.DeleteObject();
		// Fine disegno pos. bobine
		//---------------------------------------------------------
		}
	
#else
//-------------------------------------------------------------
// Disegno Posizione bobine allineate a sx
		{
		// valori per disegnare asse mediano
		int leftPos,rightPos;
		CPen pen0,pen1,*oldPen;
		// light grey
		//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
		pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
		// red
		// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
		pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
		oldPen = pdc->SelectObject(&pen0);
		int lastElem;
		lastElem = pDoc->c_rotoloMapping.c_lastElemento;
		double posLeft;
		//double globalSize = 180 * SIZE_BANDE;
		// fixBug per sistema doppia configurazione
		// double globalSize = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
		posLeft = pDoc->getPosColtello(lastElem);
		CMainFrame* pFrame;
		pFrame = (CMainFrame*) AfxGetMainWnd();

		//---------------------------------------------------------
		// Disegno Posizione bobine allineate a sx	
		if (pDoc->c_rightAlignStrip)	
			{
			// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
			// int scPosLeft = rectB.right - c_layoutCompressABBig.c_sizeFBanda +
			//	(1.  * (globalSize-posLeft) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
			int scPosLeft = rectB.left + c_layoutCompressABBig.c_sizeFBanda +
				(1.  * (posLeft) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
			// sinistra per disegnare asse mediano
			leftPos = scPosLeft;

			pdc->MoveTo(scPosLeft,rectB.top);
			pdc->LineTo(scPosLeft,rectB.bottom);
			// disegno solo inizio strip 
			// fine strip solo per ultimo strip

			double endPosLastStrip = 0.;
			CFont f,*oldF;
			f.CreatePointFont (70,"Arial",pdc);
			oldF=pdc->SelectObject(&f); 
			TEXTMETRIC tm;
			pdc->GetTextMetrics(&tm);

			//for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
			for (int i=pDoc->c_rotoloMapping[lastElem].size()-1;i>=0;i--)
				{
				// prima riga sx
				pdc->SelectObject(&pen0);	
				posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;

				// check distanza con fine strip precedente
				if (i < (pDoc->c_rotoloMapping[lastElem].size()-1))
					{
					posLeft -= (fabs(posLeft - endPosLastStrip)/2);
					}
				int cell = posLeft/SIZE_BANDE+1;

				// memo  last end position
				endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
							- (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
			// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
				// scPosLeft = rectB.right - c_layoutCompressABBig.c_sizeFBanda  -
				//	(1.  * (globalSize-posLeft) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
				scPosLeft = rectB.left + c_layoutCompressABBig.c_sizeFBanda  +
					(1.  * (posLeft) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
			
				
				// Primo!
				if (i==pDoc->c_rotoloMapping[lastElem].size()-1)
					{
					CFont f,*oldF;
					f.CreatePointFont (70,"Arial",pdc);
					oldF=pdc->SelectObject(&f); 
					TEXTMETRIC tm;
					pdc->GetTextMetrics(&tm);
					CString sl;
					pdc->SetTextAlign(TA_LEFT | TA_TOP);
					COLORREF  oldColor;
					oldColor = pdc->GetTextColor();
					pdc->SetTextColor(RGB(255,0,0));
					int dcmode = pdc->SetBkMode (TRANSPARENT);
					sl.Format("First Cell %d",cell);
					if (i%2)
						pdc->TextOut(scPosLeft,rectB.bottom+5 + tm.tmHeight,(LPCSTR)sl);	
					else
						pdc->TextOut(scPosLeft,rectB.bottom + 5 + 2*tm.tmHeight,(LPCSTR)sl);	
					pdc->SetTextColor(oldColor);
					pdc->SelectObject(oldF); 
					}

				posLeft -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

				//int scPosRight = rectB.right - c_layoutCompressABBig.c_sizeFBanda  - 
				//	(1.  * (globalSize-posLeft) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
				int scPosRight = rectB.left + c_layoutCompressABBig.c_sizeFBanda  + 
					(1.  * (posLeft) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

				pdc->MoveTo(scPosLeft,rectB.top);
				pdc->LineTo(scPosLeft,rectB.bottom);

	


				//----------------------------------------------------------
				// Elval specific
				// riferimenti numerici in testa agli strip
				CString sl;
				int scPosText = (scPosLeft + scPosRight)/2;
				pdc->SetTextAlign(TA_CENTER | TA_BOTTOM);
				int dcmode = pdc->SetBkMode (TRANSPARENT);
				sl.Format("%d",pDoc->c_rotoloMapping[lastElem].size()-i);
				if (i%2)
					pdc->TextOut(scPosText,rectB.top,(LPCSTR)sl);	
				else
					pdc->TextOut(scPosText,rectB.top-tm.tmHeight,(LPCSTR)sl);	
			
				// Larghezze degli strip in fondo 
				pdc->SetTextAlign(TA_CENTER | TA_TOP);
				sl.Format("%3.0lf",(pDoc->c_rotoloMapping[lastElem])[i].c_larghezza);
				if (i%2)
					pdc->TextOut(scPosText,rectB.bottom+5,(LPCSTR)sl);	
				else
					pdc->TextOut(scPosText,rectB.bottom+5+tm.tmHeight,(LPCSTR)sl);	

				pdc->SetBkMode (dcmode);
				// ----------------------------------------------
				// ultimo!
				if(i == 0)
					{
					// seconda riga
					pdc->SelectObject(&pen1);	
					posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
					posLeft -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
					int cell = posLeft/SIZE_BANDE;					
				// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
					//scPosLeft = rectB.right - c_layoutCompressABBig.c_sizeFBanda - 
					//	(1.  * (globalSize - posLeft) * (double)c_layoutCompressABBig.c_sizeBanda 
					//	/ (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
					scPosLeft = rectB.left + c_layoutCompressABBig.c_sizeFBanda + 
						(1.  * ( posLeft) * (double)c_layoutCompressABBig.c_sizeBanda 
						/ (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

					pdc->MoveTo(scPosLeft,rectB.top);
					pdc->LineTo(scPosLeft,rectB.bottom);
					
					{
					CFont f,*oldF;
					f.CreatePointFont (70,"Arial",pdc);
					oldF=pdc->SelectObject(&f); 
					TEXTMETRIC tm;
					pdc->GetTextMetrics(&tm);
					CString sl;
					COLORREF  oldColor;
					oldColor = pdc->GetTextColor();
					pdc->SetTextColor(RGB(255,0,0));
					pdc->SetTextAlign(TA_RIGHT | TA_TOP);
					int dcmode = pdc->SetBkMode (TRANSPARENT);
					sl.Format("Last Cell %d",cell);

					pdc->TextOut(scPosLeft,rectB.bottom + 5 + tm.tmHeight,(LPCSTR)sl);	
					pdc->SetTextColor(oldColor);
					pdc->SelectObject(oldF); 
					}
				
					}
				// garbage
				pdc->MoveTo(scPosRight,rectB.top);
				pdc->LineTo(scPosRight,rectB.bottom);
				}
			pdc->SelectObject(oldF); 
			f.DeleteObject();

			// destra per disegnare asse mediano
			rightPos = scPosLeft;
			pdc->SelectObject(oldPen);
			pen1.DeleteObject();
			// end clear
			pen1.CreatePen(PS_DASH,1,RGB(0,0,0));
			oldPen = pdc->SelectObject(&pen1);
			int mediumPos = leftPos - abs(rightPos-leftPos)/2;
			pdc->MoveTo(mediumPos,rectB.top);
			pdc->LineTo(mediumPos,rectB.bottom);
			}
		else
			{ // a sx
			// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
			int scPosLeft = rectB.left +c_layoutCompressABBig.c_sizeFBanda +
				(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));
			// sinistra per disegnare asse mediano
			leftPos = scPosLeft;

			pdc->MoveTo(scPosLeft,rectB.top);
			pdc->LineTo(scPosLeft,rectB.bottom);
			// disegno solo inizio strip 
			// fine strip solo per ultimo strip

			double endPosLastStrip = 0.;
			CFont f,*oldF;
			f.CreatePointFont (70,"Arial",pdc);
			oldF=pdc->SelectObject(&f); 
			TEXTMETRIC tm;
			pdc->GetTextMetrics(&tm);

			for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
				{
				// prima riga sx
				pdc->SelectObject(&pen0);	
				posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
				int cell = posLeft/SIZE_BANDE ;
				// check distanza con fine strip precedente
				if (i > 0)
					{
					posLeft -= (fabs(posLeft - endPosLastStrip)/2);
					}

				// memo  last end position
				endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
							+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
			// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
				scPosLeft = rectB.left+c_layoutCompressABBig.c_sizeFBanda  + 
					(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

				int scPosRight = rectB.left+c_layoutCompressABBig.c_sizeFBanda  + 
					(1.  * (posLeft + (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza) * (double)c_layoutCompressABBig.c_sizeBanda / (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

				pdc->MoveTo(scPosLeft,rectB.top);
				pdc->LineTo(scPosLeft,rectB.bottom);

				// Primo
				if (i==0)
					{
					CFont f,*oldF;
					f.CreatePointFont (70,"Arial",pdc);
					oldF=pdc->SelectObject(&f); 
					TEXTMETRIC tm;
					pdc->GetTextMetrics(&tm);
					CString sl;
					pdc->SetTextAlign(TA_RIGHT | TA_TOP);
					COLORREF  oldColor;
					oldColor = pdc->GetTextColor();
					pdc->SetTextColor(RGB(255,0,0));
					int dcmode = pdc->SetBkMode (TRANSPARENT);
					sl.Format("First Cell %d",cell);
					pdc->TextOut(scPosLeft,rectB.bottom + 5,(LPCSTR)sl);	
					pdc->SelectObject(oldF); 
					pdc->SetTextColor(oldColor);
					}

				//----------------------------------------------------------
				// Elval specific
				// riferimenti numerici in testa agli strip
				CString sl;
				int scPosText = (scPosLeft + scPosRight)/2;
				pdc->SetTextAlign(TA_CENTER | TA_BOTTOM);
				int dcmode = pdc->SetBkMode (TRANSPARENT);
				sl.Format("%d",i+1);
				if (i%2)
					pdc->TextOut(scPosText,rectB.top,(LPCSTR)sl);	
				else
					pdc->TextOut(scPosText,rectB.top-tm.tmHeight,(LPCSTR)sl);	
			
				// Larghezze degli strip in fondo 
				pdc->SetTextAlign(TA_CENTER | TA_TOP);
				sl.Format("%3.0lf",(pDoc->c_rotoloMapping[lastElem])[i].c_larghezza);
				if (i%2)
					pdc->TextOut(scPosText,rectB.bottom+5,(LPCSTR)sl);	
				else
					pdc->TextOut(scPosText,rectB.bottom+5+tm.tmHeight,(LPCSTR)sl);	

				pdc->SetBkMode (dcmode);
				// ----------------------------------------------
				if(i == (pDoc->c_rotoloMapping[lastElem].size()-1))
					{
					// seconda riga
					pdc->SelectObject(&pen1);	
					posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
					posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
					cell = posLeft/SIZE_BANDE;
				// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
					scPosLeft = rectB.left +c_layoutCompressABBig.c_sizeFBanda + 
						(1.  * posLeft * (double)c_layoutCompressABBig.c_sizeBanda 
						/ (c_layoutCompressABBig.c_bandePerCol*SIZE_BANDE));

					pdc->MoveTo(scPosLeft,rectB.top);
					pdc->LineTo(scPosLeft,rectB.bottom);

					{
					CFont f,*oldF;
					f.CreatePointFont (70,"Arial",pdc);
					oldF=pdc->SelectObject(&f); 
					TEXTMETRIC tm;
					pdc->GetTextMetrics(&tm);
					CString sl;
					pdc->SetTextAlign(TA_LEFT | TA_TOP);
					COLORREF  oldColor;
					oldColor = pdc->GetTextColor();
					pdc->SetTextColor(RGB(255,0,0));
					int dcmode = pdc->SetBkMode (TRANSPARENT);
					sl.Format("Last Cell %d",cell);
					if (i%2)
						pdc->TextOut(scPosLeft,rectB.bottom + 5 + tm.tmHeight,(LPCSTR)sl);	
					else
						pdc->TextOut(scPosLeft,rectB.bottom + 5 ,(LPCSTR)sl);	
					pdc->SelectObject(oldF); 
					pdc->SetTextColor(oldColor);
					}
					}
				// garbage
				pdc->MoveTo(scPosRight,rectB.top);
				pdc->LineTo(scPosRight,rectB.bottom);
				}
			pdc->SelectObject(oldF); 
			f.DeleteObject();

			// destra per disegnare asse mediano
			rightPos = scPosLeft;
			pdc->SelectObject(oldPen);
			pen1.DeleteObject();
			// end clear
			pen1.CreatePen(PS_DASH,1,RGB(0,0,0));
			oldPen = pdc->SelectObject(&pen1);
			int mediumPos = leftPos+(rightPos-leftPos)/2;
			pdc->MoveTo(mediumPos,rectB.top);
			pdc->LineTo(mediumPos,rectB.bottom);
			}
	
// disegno alzate
		int lastYPos = rectB.top;
		for (int i=0;i<pDoc->c_rotoloMapping[lastElem].c_alzate.size();i++)
			{// per ogni alzata
			double dAlz = pDoc->c_rotoloMapping[lastElem].getYPos(i);
			double dLength = (i==0)?dAlz:(dAlz-pDoc->c_rotoloMapping[lastElem].getYPos(i-1));
			double dTop = 0;
			double dBot = pDoc->c_difCoil.getMeter();
			if (dBot == 0.) dBot = 1;
			int scPosAlz = rectB.top + dAlz * (rectB.bottom - rectB.top)/(dBot-dTop);
			pdc->SelectObject(&pen1);
			pdc->SetBkMode (TRANSPARENT);
			pdc->MoveTo(rectB.left,scPosAlz);
			pdc->LineTo(rectB.right,scPosAlz);
			// label
			int scPosText = rectB.left - 20;
			CString sl;
			sl.Format("%4.0lf",dLength);
			if (i==0)
				pdc->SetTextAlign(TA_RIGHT | TA_TOP);
			else
				pdc->SetTextAlign(TA_RIGHT | TA_BASELINE);
			// ora 
			if (i > 0)
				pdc->TextOut(scPosText,scPosAlz,(LPCSTR)sl);	
			for (int j=0;j<10;j++)
				{// ruler
				int scRul = lastYPos + j * (scPosAlz-lastYPos)/10;
				pdc->SelectObject(&pen0);
				pdc->MoveTo(rectB.left,scRul);
				pdc->LineTo(rectB.left-15,scRul);
				}
			// dal lato destro nomi dei rising
			scPosText = rectB.right + 20;
			sl = pDoc->getAlzString(lastElem,i);
			if (i==0)
				pdc->SetTextAlign(TA_LEFT | TA_TOP);
			else
				pdc->SetTextAlign(TA_LEFT | TA_BASELINE);
			// ora 
			if (i > 0)
				pdc->TextOut(scPosText,scPosAlz,(LPCSTR)sl);	

			lastYPos = scPosAlz;
			}

		pdc->SelectObject(oldPen);
		pen1.DeleteObject();
		pen0.DeleteObject();
		}
		// Fine disegno pos. bobine
		//---------------------------------------------------------
#endif

		}
		break;

	}

}



int CInit::calcStripRepPage(CDC *pDc,CRect r)
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();
CRect subNastro;
int page = 0;
int nRowsPerPage=0;
// clear rowsPerPage
c_rowPerPage.RemoveAll();
c_extractDefPerPage.RemoveAll();

subNastro = getRectStrip(c_prElementNumber,c_prAlzataNumber,c_prStripNumber);
long numDef = pDoc->c_difCoil.getNumClassIndex(subNastro.top,subNastro.bottom,subNastro.left,subNastro.right);
if (numDef == 0)
	{
	// almeno 1 con 0
	c_rowPerPage.Add(nRowsPerPage);
	c_extractDefPerPage.Add(nRowsPerPage);
	return 1;	
	}

long needNumRows = numDef / 3;


if (numDef %3 > 0)
	needNumRows ++;	

	needNumRows ++;	

while (needNumRows > 0)
	{
	nRowsPerPage = reportStrip(pDc,page+1,r,FALSE);
	if (nRowsPerPage<= 0)
		break;
	c_rowPerPage.Add(nRowsPerPage);
	int v = 0;
	// preparo vettore con page posizioni a zero
	c_extractDefPerPage.Add(v);
	page ++;
	needNumRows -= 	nRowsPerPage;
	}
if (c_rowPerPage.GetSize() == 0)
	// almeno 1 con 0
	c_rowPerPage.Add(nRowsPerPage);
	
if (page == 0)
	page ++;
return page;
}


CRect CInit::calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border )
{

CRect rectB;
// GetClientRect

CSize printSize,trueSize;
// in stampa tolgo bordi
// pixel
printSize.cx = pdc->GetDeviceCaps(HORZRES);
printSize.cy = pdc->GetDeviceCaps(VERTRES);

// millimetri
trueSize.cx = pdc->GetDeviceCaps(HORZSIZE);
trueSize.cy = pdc->GetDeviceCaps(VERTSIZE);

// calcolo px per mm
CSize pixel;
pixel.cy = printSize.cy/trueSize.cy;
pixel.cx = printSize.cx/trueSize.cx;

/* Tolgo 2 cm a destra ed 1 a sx */
rcBounds.left += c_leftMarg * pixel.cx;
rcBounds.right -= c_rightMarg * pixel.cx;
/* Tolgo 3 cm in alto e 2 in basso */	
rcBounds.top += c_topMarg * pixel.cy;
rcBounds.bottom -= c_bottomMarg * pixel.cy;

CRect rect (rcBounds);

if (border != NULL)
	*border = rcBounds;

// determino interno per scrittura e grafica
// 5 millimetri in meno
rect.DeflateRect (5 * pixel.cx,5 * pixel.cy);

return(rect);
}

//------------------------------------
// Torna un rettangolo con :
// Top real Y pos
// Bottom real Y pos
// left => numero di cella a sx
// right => numero di cela a dx
//
CRect CInit::getRectStrip(int elemento,int alzata,int strip)
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();



// calcolo rettangolo relativo al nastro
CRect subNastro;

if(pDoc->c_rightAlignStrip)
	{
	// Posizione del coltello dell'elemento 
	subNastro.right = (pDoc->c_rotoloMapping.at(elemento)).at(strip).c_xPos + 9;
	// aggiungo larghezza nastro attuale
	subNastro.left = (pDoc->c_rotoloMapping.at(elemento)).at(strip).c_xPos 
					- (pDoc->c_rotoloMapping.at(elemento)).at(strip).c_larghezza - 9;
	}
else
	{
	// Posizione del coltello dell'elemento 
	subNastro.left = (pDoc->c_rotoloMapping.at(elemento)).at(strip).c_xPos - 9;
	// aggiungo larghezza nastro attuale
	subNastro.right = (pDoc->c_rotoloMapping.at(elemento)).at(strip).c_xPos + 
						(pDoc->c_rotoloMapping.at(elemento)).at(strip).c_larghezza + 9;
	}
// poszione longitudinale dipende da alzata
subNastro.top = pDoc->c_rotoloMapping.at(elemento).c_alzate.at(alzata).c_yPos;
// primo pezzo skip lrsestop 
if((alzata+1) >= pDoc->c_rotoloMapping.at(elemento).c_alzate.size())
	subNastro.bottom = pDoc->GetVPosition();
else
	subNastro.bottom = pDoc->c_rotoloMapping.at(elemento).c_alzate.at(alzata+1).c_yPos;
	

subNastro.top -= pDoc->c_lunResStop;
if (subNastro.top < 0)
	subNastro.top = 0;
subNastro.bottom -= pDoc->c_lunResStop;
if (subNastro.bottom < 0)
	subNastro.bottom = 0;
// elenco celle
int cellaSx = subNastro.left / (long)SIZE_BANDE;

int cellaDx = subNastro.right / (long)SIZE_BANDE;
// 11-02-14 il limite destro e` limite superiore, non inferiore
// quindi devo includere solo la cella precedente non anche quella succ che stara` fuori
// se cella superiore non a cavallo allora esclusa 
if (( subNastro.right % (int)SIZE_BANDE)==0)
	cellaDx--;		

// se invece la cella e` a cavallo prendo anche quella dopo
if ((subNastro.right % (long)SIZE_BANDE)>0)
	cellaDx ++;

/* Nessuna correzione di bordo 
	if ((subNastro.left % (long)SIZE_BANDE)<5)
		cellaSx --;
	if ((subNastro.right % (long)SIZE_BANDE)>22)
		cellaDx ++;
*/
if (cellaSx < 0)
	cellaSx = 0;

subNastro.left = cellaSx;
subNastro.right = cellaDx;	
return subNastro;
}


//------------------------------------
// Torna un rettangolo con :
// Top real Y pos
// Bottom real Y pos
// left => numero di cella a sx
// right => numero di cela a dx
// di tutto il coil per un certo elemento
CRect CInit::getRectCoil(int elemento)
{
CLineDoc* pDoc = (CLineDoc* )GetDoc();

// calcolo rettangolo relativo al nastro
CRect nastro;
// Posizione del coltello dell'elemento 
nastro.left = (pDoc->c_rotoloMapping.at(elemento)).at(0).c_xPos;

// aggiungo larghezza nastro attuale
nastro.right = 	(pDoc->c_rotoloMapping.at(elemento).at(pDoc->c_rotoloMapping.at(elemento).size()-1)).c_xPos
	+
	(pDoc->c_rotoloMapping.at(elemento).at(pDoc->c_rotoloMapping.at(elemento).size()-1)).c_larghezza;
 
// poszione longitudinale 
nastro.top = 0;
// primo pezzo skip lrsestop 
nastro.bottom = pDoc->getVTotalLength();

// elenco celle
int cellaSx = nastro.left / (long)SIZE_BANDE;

int cellaDx = nastro.right / (long)SIZE_BANDE;
if ((nastro.right % (long)SIZE_BANDE)>0)
	cellaDx ++;

/* Nessuna correzione di bordo 
	if ((subNastro.left % (long)SIZE_BANDE)<5)
		cellaSx --;
	if ((subNastro.right % (long)SIZE_BANDE)>22)
		cellaDx ++;
*/
if (cellaSx < 0)
	cellaSx = 0;

nastro.left = cellaSx;
nastro.right = cellaDx;	
return nastro;
}

void CInitView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
// TODO: Add your specialized code here and/or call the base class

CPdfPrintView::OnEndPrintPreview(pDC, pInfo, point, pView);

c_isPrinting = FALSE;

}


//-------------------------------------------------------------------------
//
//
//		saveSectionTxtReport ()
//		saveDefectTxtReport ()
//
//

#define INCLUDE_HOLEA_TXT
#define INCLUDE_HOLEB_TXT

#define		SEPARATOR	"\t"
#define		ENDRECORD	"\r\n"		 

CString CInitView::saveSectionTxtReport(void)
{
CString fileName;

CLineDoc* pDoc = (CLineDoc* )GetDocument();
CProfile profile;

if (!profile.getProfileBool(_T("Report"),_T("EnableTxtReport"),TRUE))
	return (_T(""));


SECURITY_ATTRIBUTES	 sa;
sa.nLength= sizeof(SECURITY_ATTRIBUTES);
sa.lpSecurityDescriptor=NULL;
sa.bInheritHandle = TRUE;

// separate component file name

fileName = pDoc->getFileName();

CString monthDir;
monthDir = pDoc->getMonthFolder();
//CTime st(pDoc->c_startTime);
//monthDir = _T("DD") + st.Format ("%m%y");

CString defDir;
defDir = profile.getProfileString("init","RootDir",".");
defDir += "\\";
defDir += monthDir;
defDir += _T("/Txt");
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/");

CString fName;
fName = defDir;

// replace name of file + Path
fName += fileName;
fName += "_S";
fileName = fName;
fileName += ".txt";
 
CFile fd;

if (!fd.Open(fileName,CFile::modeCreate | CFile::modeWrite,NULL))
	{
	CString msg;
	msg.Format("Error opening file %s", fileName);
	AfxMessageBox(msg);
	return CString("");
	}
CString s,s1;
s = "DateTime";
fd.Write(s,s.GetLength());
s1 = SEPARATOR;
fd.Write(s1,s1.GetLength());
s = "Coil";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
s = "Alloy Type";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
s ="Thickness";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
s = "Length";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
s = "Width";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
s = "Order";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
#ifdef INCLUDE_HOLEA_TXT
s = "TotalHoles A";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
#endif
//---
#ifdef INCLUDE_HOLEB_TXT
s = "TotalHoles B";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
#endif
s = "TotalHoles C";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
s = "TotalHoles D";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());
s = "TotalHoles Big";
fd.Write(s,s.GetLength());
fd.Write(s1,s1.GetLength());

for (int i=0;i<20;i++)
	{
	// Length
	s = "LengthSect";
	s1.Format("%d",i+1);
	s += s1;
	s += SEPARATOR;
	fd.Write(s,s.GetLength());
	// Holes A
#ifdef INCLUDE_HOLEA_TXT
	s = "Class A HolesSect";
	s1.Format("%d",i+1);
	s += s1;
	s += SEPARATOR;
	fd.Write(s,s.GetLength());
#endif
	// Holes B
#ifdef INCLUDE_HOLEB_TXT
	s = "Class B HolesSect";
	s1.Format("%d",i+1);
	s += s1;
	s += SEPARATOR;
	fd.Write(s,s.GetLength());
#endif
	// Holes C
	s = "Class C HolesSect";
	s1.Format("%d",i+1);
	s += s1;
	s += SEPARATOR;
	fd.Write(s,s.GetLength());
	// Holes D
	s = "Class D HolesSect";
	s1.Format("%d",i+1);
	s += s1;
	s += SEPARATOR;
	fd.Write(s,s.GetLength());
	// Holes Big
	s = "Class Big HolesSect";
	s1.Format("%d",i+1);
	s += s1;
	if (i == 19)
		s += ENDRECORD;
	else
		s += SEPARATOR;
	fd.Write(s,s.GetLength());
	}

// data record
CTime t(pDoc->c_startTime);
s = t.Format("%d-%m-%Y %H:%M:%S");
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = pDoc->getRotolo();
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = pDoc->c_lega;
s += SEPARATOR;
fd.Write(s,s.GetLength());

s.Format("%6.02lf",pDoc->c_spessore);
s += SEPARATOR;
fd.Write(s,s.GetLength());

s.Format("%d",(int)pDoc->getVTotalLength());
s += SEPARATOR;
fd.Write(s,s.GetLength());

s.Format("%6.01lf",pDoc->c_larghezza);
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = pDoc->c_cliente;
s += SEPARATOR;
fd.Write(s,s.GetLength());

//
#ifdef INCLUDE_HOLEA_TXT
s.Format("%d ",(int)pDoc->c_difCoil.getTotDifetti('A',0,(int)pDoc->getVTotalLength()));
s += SEPARATOR;
fd.Write(s,s.GetLength());
#endif
//
#ifdef INCLUDE_HOLEB_TXT
s.Format("%d ",(int)pDoc->c_difCoil.getTotDifetti('B',0,(int)pDoc->getVTotalLength()));
s += SEPARATOR;
fd.Write(s,s.GetLength());
#endif
//
s.Format("%d ",(int)pDoc->c_difCoil.getTotDifetti('C',0,(int)pDoc->getVTotalLength()));
s += SEPARATOR;
fd.Write(s,s.GetLength());
//
s.Format("%d ",(int)pDoc->c_difCoil.getTotDifetti('D',0,(int)pDoc->getVTotalLength()));
s += SEPARATOR;
fd.Write(s,s.GetLength());
//
s.Format("%d ",(int)pDoc->c_difCoil.getTotBigHole(0,(int)pDoc->getVTotalLength()));
s += SEPARATOR;
fd.Write(s,s.GetLength());


//-----------------------------------------------------
// sezione dettaglio difetti 
double seg[20];
int interv[20];
int numSeg = 0;
memset(seg,0,sizeof(seg));
memset(interv,0,sizeof(interv));
for (int i=0;i<19;i++)
	{
	if (m_INTERVALLO[i] > 0)
		{
		interv[numSeg] = (int) m_INTERVALLO[i];
		seg[numSeg++] = (pDoc->getVTotalLength() * m_INTERVALLO[i] / 100);
		}				
	}


for (int i=0;(i<20)&&(numSeg>0);i++)
	{
	CString s;
	if (i == numSeg)
		{// ultimo configurato
		// Length
		s.Format("%d ",(long)(pDoc->getVTotalLength()-seg[i-1]));
		s += SEPARATOR;
		fd.Write(s,s.GetLength());
		// Holes A
		#ifdef INCLUDE_HOLEA_TXT
		s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('A',(int)seg[i-1],(int)pDoc->getVTotalLength()));
		s += SEPARATOR;
		fd.Write(s,s.GetLength());
		#endif
		// Holes B
		#ifdef INCLUDE_HOLEB_TXT
		s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('B',(int)seg[i-1],(int)pDoc->getVTotalLength()));
		s += SEPARATOR;
		fd.Write(s,s.GetLength());
		#endif
		// Holes C
		s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('C',(int)seg[i-1],(int)pDoc->getVTotalLength()));
		s += SEPARATOR;
		fd.Write(s,s.GetLength());
		// Holes D
		s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('D',(int)seg[i-1],(int)pDoc->getVTotalLength()));
		s += SEPARATOR;
		fd.Write(s,s.GetLength());
		// Holes Big
		s.Format("%4.0lf ",pDoc->c_difCoil.getTotBigHole((int)seg[i-1],(int)pDoc->getVTotalLength()));
		if (i == 19)
			s += ENDRECORD;
		else
			s += SEPARATOR;
		fd.Write(s,s.GetLength());
		}
	else
		{// ultimo seg 19 non configurato altrimenti sarebbe = numSeg
		if (i == 19)
			{
			// Length
			s.Format("%d ",0);
			s += SEPARATOR;
			fd.Write(s,s.GetLength());
			// Holes
			s.Format("%d ",0);
			s += ENDRECORD;
			fd.Write(s,s.GetLength());
			}
		else
			{// < 19
			if (i == 0)
				{
				// Length
				s.Format("%d ",(long)(seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				// Holes A
				#ifdef INCLUDE_HOLEA_TXT
				s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('A',0,(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				#endif
				// Holes B
				#ifdef INCLUDE_HOLEB_TXT
				s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('B',0,(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				#endif
				// Holes C
				s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('C',0,(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				// Holes D
				s.Format("%4.0lf ",pDoc->c_difCoil.getTotDifetti('D',0,(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				// Holes Big
				s.Format("%4.0lf ",pDoc->c_difCoil.getTotBigHole(0,(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				}
			else
				{// << 19
				// Length
				s.Format("%d ",(long)(seg[i]-seg[i-1]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				// Holes A
				#ifdef INCLUDE_HOLEA_TXT
				s.Format("%d ",(long)pDoc->c_difCoil.getTotDifetti('A',(int)seg[i-1],(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				#endif
				// Holes B
				#ifdef INCLUDE_HOLEB_TXT
				s.Format("%d ",(long)pDoc->c_difCoil.getTotDifetti('B',(int)seg[i-1],(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				#endif
				// Holes C
				s.Format("%d ",(long)pDoc->c_difCoil.getTotDifetti('C',(int)seg[i-1],(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				// Holes D
				s.Format("%d ",(long)pDoc->c_difCoil.getTotDifetti('D',(int)seg[i-1],(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				// Holes Big
				s.Format("%4.0lf ",pDoc->c_difCoil.getTotBigHole((int)seg[i-1],(int)seg[i]));
				s += SEPARATOR;
				fd.Write(s,s.GetLength());
				}
			}
		}
	}

fd.Close();

return (fileName);
}



void CInitView::reportSave() 
{
#ifndef Csm20El2013_REPORT
	
CLineDoc* pDoc = (CLineDoc* )GetDocument();
CMainFrame *pMainFrame = (CMainFrame *)AfxGetMainWnd();

// print txt report
saveSectionTxtReport();
// xls
saveSectionXlsReport();
// 
saveDefectTxtReport();

// txt strip report
int numStrip = pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].size();
int alzataNumber = pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].c_alzate.size()-2;
for (int i=0;i<numStrip;i++)
	{
	saveDefectTxtStripReport(i,alzataNumber);
	saveDefectXlsStripReport(i,alzataNumber);
	}
// pdf Printer section
CString folder;
folder = pDoc->getMonthFolder();
//CTime st(pDoc->c_startTime);
//folder = _T("DD") + st.Format ("%m%y");

BOOL usePdfFile = FALSE;
pMainFrame->c_pdfInfo.loadFromProfile();
if (((CCSMApp*)AfxGetApp())->GetDefaultPrinter() == MICROSOFT_PDF_PRINTER)
	usePdfFile = TRUE;
//-------------------------


DBOpzioni dbOpzioni(dBase);
if (dbOpzioni.openSelectDefault ())
	{
	if (dbOpzioni.m_STAMPA_AUTO)
		{		
		CString s;
		// Lunghezza fissa tutto asse
		c_dalMetro = 0;
		c_alMetro  = pDoc->getVTotalLength();

		CString pdfName;
		CString titlePdf;
		CPdfInfo pi;
		pi.loadFromProfile();
		if (usePdfFile)
			{
			pdfName = pDoc->getFileName();
			titlePdf = pi.getPdfFilePath(pdfName,folder);
			setDocPath(titlePdf);
			s.Format("Printing Coil report file %s",pdfName);
			}
		else
			setDocPath(_T(""));
		// Stampa automatica parte solo testo
		// ora report uguali sia in automatico sia in manuale
		//onlyTextReport = TRUE;
		//onlyTextReport = FALSE;
		directPrint = TRUE; 	
		// report Completo
		c_doPrintStrip = FALSE; 	
		OnFilePrint();
		}
		directPrint = FALSE; 
		
	if (dbOpzioni.m_STAMPA_AUTO_STRIP)
		{	
		CString s;
		// Stampa automatica 
		directPrint = TRUE; 	
		// print strip		
		c_doPrintStrip = TRUE; 
		c_prElementNumber = pDoc->c_rotoloMapping.c_lastElemento;
		// -1 per arrivare ultimo indice, altro -1 per prendere indice di partenza
		c_prAlzataNumber = pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].c_alzate.size()-2;

		// loop on print for every strip
		int numStrip = pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].size();
		CString pdfName;
		CString titlePdf;
		CPdfInfo pi;
		pi.loadFromProfile();

		for (int i=0;i<numStrip&&(c_continuePrinting);i++)
			{
			c_prStripNumber = i;
			if (usePdfFile)
				{
				pdfName = pDoc->getFileStripName(i,alzataNumber);
				titlePdf = pi.getPdfFilePath(pdfName,folder);
				setDocPath(titlePdf);
				s.Format("Printing strip report file %s",pdfName);
				}
			else
				setDocPath(_T(""));
			OnFilePrint();
			}			
		//---------
		directPrint = FALSE; 	
		}
	}
dbOpzioni.Close();
#endif
}

void CInitView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

#ifndef Csm20El2013_REPORT


if(nIDEvent ==	REPORTSAVE_TIMER_ID)
	{
	CSingleLock singleLock(&c_reportSaveSem);
	if(singleLock.Lock(10)==0)		// Attempt to lock the shared resource
		return;
	
	if (singleLock.IsLocked())  // Resource has been locked
		{
		KillTimer(REPORTSAVE_TIMER_ID);
		CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
		// IMPORTANTE -> Permettiamo all'altro thread CInitView
		// di entrare
		frame->c_isReporting = TRUE;
		Sleep(20);
		reportSave();
		singleLock.Unlock();
		frame->c_isReporting = FALSE;

		}
	}
#endif
	
CView::OnTimer(nIDEvent);
}



//-------------------------------------------------------------------------
//
//
//		saveSectionExcReport ()
//		saveDefectExcReport ()
//
//

CString CInitView::saveDefectTxtReport(void)
{
CString fileName;
#ifdef  USE_SAVE_TXT
CLineDoc* pDoc = (CLineDoc* )GetDocument();
CProfile profile;

SECURITY_ATTRIBUTES	 sa;
sa.nLength= sizeof(SECURITY_ATTRIBUTES);
sa.lpSecurityDescriptor=NULL;
sa.bInheritHandle = TRUE;

// separate component file name

fileName = pDoc->getFileName();

CString monthDir;
monthDir = pDoc->getMonthFolder();
//CTime st(pDoc->c_startTime);
//monthDir = _T("DD") + st.Format ("%m%y");


CString defDir;
defDir = profile.getProfileString("init","RootDir",".");
defDir += "\\";
defDir += monthDir;
defDir += _T("/Txt");
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/");

CString fName;
fName = defDir;

// replace name of file + Path
fName += fileName;
fName += "_D";
fileName = fName;

fileName += ".txt";

CFile fd;

if (!fd.Open(fileName,CFile::modeCreate | CFile::modeWrite,NULL))
	{
	CString msg;
	msg.Format("Error opening file %s", fileName);
	AfxMessageBox(msg);
	}
CString s,s1;
s = "Position";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Total Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Class A Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Class B Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Class C Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Class D Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Class Big Holes";
s += ENDRECORD;
fd.Write(s,s.GetLength());

// Number holes interval
int index=0;
while (index < pDoc->c_difCoil.GetSize())
	{ 
	CString str;
	long nDif;
	// dovrebbero essere memorizzati solo metri difettosi
	str.Format("%4d",pDoc->c_difCoil[index].posizione);
	str += SEPARATOR;
	//
	fd.Write(str,str.GetLength());
	nDif = pDoc->c_difCoil[index].getValNum('A')+pDoc->c_difCoil[index].getValNum('B')+
		pDoc->c_difCoil[index].getValNum('C')+pDoc->c_difCoil[index].getValNum('D');
	str.Format("%4d",nDif);
	str  += SEPARATOR;
	fd.Write(str,str.GetLength());
	//
	nDif = pDoc->c_difCoil[index].getValNum('A');
	str.Format("%4d",nDif);
	str  += SEPARATOR;
	fd.Write(str,str.GetLength());
	//
	nDif = pDoc->c_difCoil[index].getValNum('B');
	str.Format("%4d",nDif);
	str  += SEPARATOR;
	fd.Write(str,str.GetLength());
	//
	nDif = pDoc->c_difCoil[index].getValNum('C');
	str.Format("%4d",nDif);
	str  += SEPARATOR;
	fd.Write(str,str.GetLength());
	//
	nDif = pDoc->c_difCoil[index].getValNum('D');
	str.Format("%4d",nDif);
	str  += SEPARATOR;
	fd.Write(str,str.GetLength());
	//
	nDif = pDoc->c_difCoil[index].getValBigHole(-1,-1);
	str.Format("%4d",nDif);
	str  += ENDRECORD;
	fd.Write(str,str.GetLength());
	//
	index ++;
	}

fd.Close();
#endif 
return (fileName);
}


CString CInitView::saveDefectTxtStripReport(int strip,int alzata)
{
CString fileName;

CLineDoc* pDoc = (CLineDoc* )GetDocument();
CProfile profile;

if (!profile.getProfileBool(_T("Report"),_T("EnableTxtReport"),TRUE))
	return (_T(""));


SECURITY_ATTRIBUTES	 sa;
sa.nLength= sizeof(SECURITY_ATTRIBUTES);
sa.lpSecurityDescriptor=NULL;
sa.bInheritHandle = TRUE;

// separate component file name

fileName = pDoc->getFileStripName(strip,alzata);

CString monthDir;
monthDir = pDoc->getMonthFolder();
//CTime st(pDoc->c_startTime);
//monthDir = _T("DD") + st.Format ("%m%y");

CString defDir;
defDir = profile.getProfileString("init","RootDir",".");
defDir += "\\";
defDir += monthDir;
// try to create also top directory otherwise we can't create the next
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/Txt");
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/");

CString fName;
fName = defDir;


// replace name of file + Path
fName += fileName;
fName += "_D";
fileName = fName;

fileName += ".txt";

CFile fd;

if (!fd.Open(fileName,CFile::modeCreate | CFile::modeWrite,NULL))
	{
	CString msg;
	msg.Format("Error opening file %s", fileName);
	AfxMessageBox(msg);
	}
CString s,s1;
s = "Position";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Total Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

#ifdef INCLUDE_HOLEA
s = "Class A Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());
#endif
#ifdef INCLUDE_HOLEB
s = "Class B Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());
#endif

s = "Class C Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Class D Holes";
s += SEPARATOR;
fd.Write(s,s.GetLength());

s = "Class Big Holes";
s += ENDRECORD;
fd.Write(s,s.GetLength());

// elenco celle per nastro
// Offset sx  
CRect subNastro;
// in left e right ci sono invece di xLeft e xRight
// leftCell e rightCell
// -1 per arrivare ultimo indice, altro -1 per prendere indice di partenza
subNastro = getRectStrip(pDoc->c_rotoloMapping.c_lastElemento,pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size()-2,strip);

// Number holes interval
int index=0;
while (index < pDoc->c_difCoil.GetSize())
	{ 
	double pos;
	// Posizione a posteriori (valore 10 indica da 0 a 10)
	pos =  pDoc->c_difCoil[index].posizione; // posizione del metro dif
	// NOT trepassing end of strip
	if (pos >= subNastro.bottom)
		break;
	// before ? start of strip?
	if (pos < subNastro.top)
		{
		index ++;
		continue;
		}

	CString str;
	long nDif,nDifA,nDifB,nDifC,nDifD,nDifBig;
	nDifA = (long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'A');
	nDifB =	(long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'B');
	nDifC =	(long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'C');
	nDifD =	(long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'D');
	nDif = nDifA + nDifB + nDifC + nDifD;
	nDifBig =	(long)pDoc->c_difCoil[index].getValBigHole(subNastro.left,subNastro.right);
	if (nDif > 0)
		{// potrebbero non esserci difetti in qs metro/strip
		double pos = pDoc->c_difCoil[index].posizione;
		str.Format ("%4d",(int)(pos-subNastro.top));
		str += SEPARATOR;
		fd.Write(str,str.GetLength());
		//
		str.Format("%4d",nDif);
		str  += SEPARATOR;
		fd.Write(str,str.GetLength());
		//
		#ifdef INCLUDE_HOLEA
		str.Format("%4d",nDifA);
		str  += SEPARATOR;
		fd.Write(str,str.GetLength());
		#endif
		//
		#ifdef INCLUDE_HOLEB
		str.Format("%4d",nDifB);
		str  += SEPARATOR;
		fd.Write(str,str.GetLength());
		#endif
		//
		str.Format("%4d",nDifC);
		str  += SEPARATOR;
		fd.Write(str,str.GetLength());
		//
		str.Format("%4d",nDifD);
		str  += SEPARATOR;
		fd.Write(str,str.GetLength());
		//
		str.Format("%4d",nDifBig);
		str  += ENDRECORD;
		fd.Write(str,str.GetLength());
		}
	index ++;
	}

fd.Close();

return (fileName);
}

#ifndef Csm20El2013_REPORT

using namespace ExcelFormat;

// inserire fori a e b nei report txt e excel
// #define INCLUDE_HOLEA
// #define INCLUDE_HOLEB


CString CInitView::saveSectionXlsReport(void)
{
CLineDoc* pDoc = (CLineDoc* )GetDocument();
CProfile profile;

if (!profile.getProfileBool(_T("Report"),_T("EnableXlsReport"),FALSE))
	return (_T(""));

SECURITY_ATTRIBUTES	 sa;
sa.nLength= sizeof(SECURITY_ATTRIBUTES);
sa.lpSecurityDescriptor=NULL;
sa.bInheritHandle = TRUE;

// separate component file name
CString fileName;
fileName = pDoc->getFileName();

CString monthDir;
monthDir = pDoc->getMonthFolder();
//CTime st(pDoc->c_startTime);
//monthDir = _T("DD") + st.Format ("%m%y");

CString defDir;
defDir = profile.getProfileString("init","RootDir",".");
defDir += "\\";
defDir += monthDir;
defDir += _T("/Xls");
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/");

CString fName;
fName = defDir;

// replace name of file + Path
fName += fileName;
fName += "_S";
fileName = fName;
fileName += ".xls";
// ok prepared file name!

BasicExcel xls;

XLSFormatManager fmt_mgr(xls);
ExcelFont font_bold;
font_bold._weight = FW_BOLD; // 700

CellFormat fmt_bold(fmt_mgr);
fmt_bold.set_font(font_bold);

// create 3 sheets and get the associated BasicExcelWorksheet pointer
xls.New(3);
BasicExcelWorksheet* sheet1 = xls.GetWorksheet(0);

sheet1->Rename("Coil Info");

int col = 0;
int row = 0;

// intestazione
BasicExcelCell* cell = sheet1->Cell(row, col);
cell->Set("DateTime");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Coil");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Alloy Type");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Thickness");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Length");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Width");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Order");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("TotalHoles A");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("TotalHoles B");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("TotalHoles C");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("TotalHoles D");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("TotalHoles Big");
cell->SetFormat(fmt_bold);
col ++;
// ok fill the sheet1 

// reset index
col = 0;
row = 1;
// data record
CString s;
CTime t(pDoc->c_startTime);
s = t.Format("%d-%m-%Y %H:%M:%S");
cell = sheet1->Cell(row, col);
cell->Set(s);
col ++;
//
s = pDoc->getRotolo();
cell = sheet1->Cell(row, col);
cell->Set(s);
col ++;
//
s = pDoc->c_lega;
cell = sheet1->Cell(row, col);
cell->Set(s);
col ++;
//
s.Format("%6.02lf",pDoc->c_spessore);
cell = sheet1->Cell(row, col);
cell->Set(s);
col ++;
//
s.Format("%d",(int)pDoc->getVTotalLength());
cell = sheet1->Cell(row, col);
cell->Set(s);
col ++;
//
s.Format("%6.01lf",pDoc->c_larghezza);
cell = sheet1->Cell(row, col);
cell->Set(s);
col ++;
//
s = pDoc->c_cliente;
cell = sheet1->Cell(row, col);
cell->Set(s);
col ++;
//
int iv = (int)pDoc->c_difCoil.getTotDifetti('A',0,(int)pDoc->getVTotalLength());
cell = sheet1->Cell(row, col);
cell->SetInteger(iv);
col ++;
//
iv = (int)pDoc->c_difCoil.getTotDifetti('B',0,(int)pDoc->getVTotalLength());
cell = sheet1->Cell(row, col);
cell->SetInteger(iv);
col ++;
//
iv = (int)pDoc->c_difCoil.getTotDifetti('C',0,(int)pDoc->getVTotalLength());
cell = sheet1->Cell(row, col);
cell->SetInteger(iv);
col ++;
//
iv = (int)pDoc->c_difCoil.getTotDifetti('D',0,(int)pDoc->getVTotalLength());
cell = sheet1->Cell(row, col);
cell->SetInteger(iv);
col ++;
//
iv = (int)pDoc->c_difCoil.getTotBigHole(0,(int)pDoc->getVTotalLength());
cell = sheet1->Cell(row, col);
cell->SetInteger(iv);
col ++;
//

// create sheet 2 and get the associated BasicExcelWorksheet pointer
BasicExcelWorksheet* sheet2 = xls.GetWorksheet(1);
sheet2->Rename("Sections");

// reset index
col = 0;
row = 0;
// intestazione
//
cell = sheet2->Cell(row, col);
cell->Set("S");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet2->Cell(row, col);
cell->Set( " Int.[m] ");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet2->Cell(row, col);
cell->Set("Cls A");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet2->Cell(row, col);
cell->Set("Cls B");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet2->Cell(row, col);
cell->Set("Cls C");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet2->Cell(row, col);
cell->Set("Cls D");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet2->Cell(row, col);
cell->Set("Cls Big");
cell->SetFormat(fmt_bold);
col ++;

//-----------------------------------------------------
// sezione dettaglio difetti 
double seg[20];
int interv[20];
int numSeg = 0;
memset(seg,0,sizeof(seg));
memset(interv,0,sizeof(interv));
for (int i=0;i<19;i++)
	{
	if (m_INTERVALLO[i] > 0)
		{
		interv[numSeg] = (int) m_INTERVALLO[i];
		seg[numSeg++] = (pDoc->getVTotalLength() * m_INTERVALLO[i] / 100);
		}				
	}



for (int i=0;(i<20)&&(numSeg>0);i++)
	{
	col = 0;
	row = 1 + i;
	CString s;
	double dv;
	// Numerazione sezioni
	iv = i+1;
	cell = sheet2->Cell(row, col);
	cell->SetInteger(iv);
	col ++;
	if (i == numSeg)
		{// ultimo configurato
		// Length
		iv = (long)(pDoc->getVTotalLength()-seg[i-1]);
		cell = sheet2->Cell(row, col);
		cell->SetInteger(iv);
		col ++;
		// Holes A
		dv = pDoc->c_difCoil.getTotDifetti('A',(int)seg[i-1],(int)pDoc->getVTotalLength());
		cell = sheet2->Cell(row, col);
		cell->SetDouble(dv);
		col ++;
		// Holes B
		dv = pDoc->c_difCoil.getTotDifetti('B',(int)seg[i-1],(int)pDoc->getVTotalLength());
		cell = sheet2->Cell(row, col);
		cell->SetDouble(dv);
		col ++;
		// Holes C
		dv = pDoc->c_difCoil.getTotDifetti('C',(int)seg[i-1],(int)pDoc->getVTotalLength());
		cell = sheet2->Cell(row, col);
		cell->SetDouble(dv);
		col ++;
		// Holes D
		dv = pDoc->c_difCoil.getTotDifetti('D',(int)seg[i-1],(int)pDoc->getVTotalLength());
		cell = sheet2->Cell(row, col);
		cell->SetDouble(dv);
		col ++;
		// Holes Big
		dv = pDoc->c_difCoil.getTotBigHole((int)seg[i-1],(int)pDoc->getVTotalLength());
		cell = sheet2->Cell(row, col);
		cell->SetDouble(dv);
		col ++;
		}
	else
		{// ultimo seg 19 non configurato altrimenti sarebbe = numSeg
		if (i == 19)
			{
			// Length
			iv = 0;
			cell = sheet2->Cell(row, col);
			cell->SetInteger(iv);
			col ++;
			// Holes
			iv =0;
			cell = sheet2->Cell(row, col);
			cell->SetInteger(iv);
			col ++;




			}
		else
			{// < 19
			if (i == 0)
				{
				// Length
				iv = (long)(seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetInteger(iv);
				col ++;
				// Holes A
				dv = pDoc->c_difCoil.getTotDifetti('A',0,(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble (dv);
				col ++;
				// Holes B
				dv = pDoc->c_difCoil.getTotDifetti('B',0,(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble (dv);
				col ++;
				// Holes C
				dv = pDoc->c_difCoil.getTotDifetti('C',0,(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble (dv);
				col ++;
				// Holes D
				dv = pDoc->c_difCoil.getTotDifetti('D',0,(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble (dv);
				col ++;
				// Holes Big
				dv = pDoc->c_difCoil.getTotBigHole(0,(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble (dv);
				col ++;
				}
			else
				{// << 19
				// Length
				iv = (long)(seg[i]-seg[i-1]);
				cell = sheet2->Cell(row, col);
				cell->SetInteger(iv);
				col ++;
				// Holes A
				dv = pDoc->c_difCoil.getTotDifetti('A',(int)seg[i-1],(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble(dv);
				col ++;
				// Holes B
				dv = pDoc->c_difCoil.getTotDifetti('B',(int)seg[i-1],(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble(dv);
				col ++;
				// Holes C
				dv = pDoc->c_difCoil.getTotDifetti('C',(int)seg[i-1],(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble(dv);
				col ++;
				// Holes D
				dv = pDoc->c_difCoil.getTotDifetti('D',(int)seg[i-1],(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble(dv);
				col ++;
				// Holes Big
				dv = pDoc->c_difCoil.getTotBigHole((int)seg[i-1],(int)seg[i]);
				cell = sheet2->Cell(row, col);
				cell->SetDouble(dv);
				col ++;
				}
			}
		}
	}

// save holes details
BasicExcelWorksheet* sheet3 = xls.GetWorksheet(2);

sheet3->Rename("Holes Details");

col = 0;
row = 0;

// intestazione
cell = sheet3->Cell(row, col);
cell->Set("Position");
cell->SetFormat(fmt_bold);
col ++;

cell = sheet3->Cell(row, col);
cell->Set("Total Holes");
cell->SetFormat(fmt_bold);
col ++;

#ifdef INCLUDE_HOLEA
// Niente A e niente B 
cell = sheet3->Cell(row, col);
cell->Set("Class A Holes");
cell->SetFormat(fmt_bold);
col ++;
#endif
#ifdef INCLUDE_HOLEB
cell = sheet3->Cell(row, col);
cell->Set("Class B Holes");
cell->SetFormat(fmt_bold);
col ++;
#endif

cell = sheet3->Cell(row, col);
cell->Set("Class C Holes");
cell->SetFormat(fmt_bold);
col ++;

cell = sheet3->Cell(row, col);
cell->Set("Class D Holes");
cell->SetFormat(fmt_bold);
col ++;

cell = sheet3->Cell(row, col);
cell->Set("Class Big Holes");
cell->SetFormat(fmt_bold);
col ++;


// Per ogni Number holes 
int index=0;
row = 0;
col = 0;
while (index < pDoc->c_difCoil.GetSize())
	{ 
	CString str;
	long nDif;
	long pos;
	// dovrebbero essere memorizzati solo metri difettosi
	pos = pDoc->c_difCoil[index].posizione;
	nDif = 
		// niente a e B
		#ifdef INCLUDE_HOLEA
			pDoc->c_difCoil[index].getValNum('A')+
		#endif
		#ifdef INCLUDE_HOLEB
			pDoc->c_difCoil[index].getValNum('B')+
		#endif
		pDoc->c_difCoil[index].getValNum('C')+pDoc->c_difCoil[index].getValNum('D')+
		pDoc->c_difCoil[index].getValBigHole(-1,-1);
	if (nDif > 0)
		{// ok ci sono fori c/d/big
		col = 0;
		row  ++;
		// posizione
		cell = sheet3->Cell(row, col);	
		cell->SetInteger(pos);
		col ++;	
		// n totale difetti
		cell = sheet3->Cell(row, col);	
		cell->SetInteger(nDif);
		col ++;	
		#ifdef INCLUDE_HOLEA
		// difetti a 
		nDif = pDoc->c_difCoil[index].getValNum('A');
		cell = sheet3->Cell(row, col);	
		cell->SetInteger(nDif);
		col ++;	
		#endif
		#ifdef INCLUDE_HOLEB
		// difetti b 
		nDif = pDoc->c_difCoil[index].getValNum('B');
		cell = sheet3->Cell(row, col);	
		cell->SetInteger(nDif);
		col ++;	
		#endif
		// difetti c 
		nDif = pDoc->c_difCoil[index].getValNum('C');
		cell = sheet3->Cell(row, col);	
		cell->SetInteger(nDif);
		col ++;	
		// difettia d 
		nDif = pDoc->c_difCoil[index].getValNum('D');
		cell = sheet3->Cell(row, col);	
		cell->SetInteger(nDif);
		col ++;	
		//
		nDif = pDoc->c_difCoil[index].getValBigHole(-1,-1);
		cell = sheet3->Cell(row, col);	
		cell->SetInteger(nDif);
		col ++;	
		}
	//
	index ++;
	}

xls.SaveAs(fileName);

return (fileName);
}


CString CInitView::saveDefectXlsStripReport(int strip,int alzata)
{
CLineDoc* pDoc = (CLineDoc* )GetDocument();
CProfile profile;

if (!profile.getProfileBool(_T("Report"),_T("EnableXlsReport"),FALSE))
	return (_T(""));


SECURITY_ATTRIBUTES	 sa;
sa.nLength= sizeof(SECURITY_ATTRIBUTES);
sa.lpSecurityDescriptor=NULL;
sa.bInheritHandle = TRUE;

// separate component file name
CString fileName;
fileName = pDoc->getFileStripName(strip,alzata);

CString monthDir;
monthDir = pDoc->getMonthFolder();
//CTime st(pDoc->c_startTime);
//monthDir = _T("DD") + st.Format ("%m%y");

CString defDir;
defDir = profile.getProfileString("init","RootDir",".");
defDir += "\\";
defDir += monthDir;
// try to create also top directory otherwise we can't create the next
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/Xls");
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/");

CString fName;
fName = defDir;

// replace name of file + Path
fName += fileName;
fName += "_D";
fileName = fName;

fileName += ".xls";

// ok prepared file name!

BasicExcel xls;

XLSFormatManager fmt_mgr(xls);
ExcelFont font_bold;
font_bold._weight = FW_BOLD; // 700

CellFormat fmt_bold(fmt_mgr);
fmt_bold.set_font(font_bold);

// create 3 sheets and get the associated BasicExcelWorksheet pointer
xls.New(1);
BasicExcelWorksheet* sheet1 = xls.GetWorksheet(0);

sheet1->Rename("Coil Info");

int col = 0;
int row = 0;

// intestazione
BasicExcelCell* cell = sheet1->Cell(row, col);
cell->Set("Position");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Total Holes");
cell->SetFormat(fmt_bold);
col ++;
//
#ifdef INCLUDE_HOLEA
cell = sheet1->Cell(row, col);
cell->Set("Class A Holes");
cell->SetFormat(fmt_bold);
col ++;
#endif
//
#ifdef INCLUDE_HOLEB
cell = sheet1->Cell(row, col);
cell->Set("Class B Holes");
cell->SetFormat(fmt_bold);
col ++;
//
#endif
cell = sheet1->Cell(row, col);
cell->Set("Class C Holes");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Class D Holes");
cell->SetFormat(fmt_bold);
col ++;
//
cell = sheet1->Cell(row, col);
cell->Set("Class Big Holes");
cell->SetFormat(fmt_bold);
col ++;
//


// elenco celle per nastro
// Offset sx  
CRect subNastro;
// in left e right ci sono invece di xLeft e xRight
// leftCell e rightCell
// -1 per arrivare ultimo indice, altro -1 per prendere indice di partenza
subNastro = getRectStrip(pDoc->c_rotoloMapping.c_lastElemento,pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size()-2,strip);

// Number holes interval
int index=0;
col = 0;
row = 0;
while (index < pDoc->c_difCoil.GetSize())
	{ 
	double pos;
	// Posizione a posteriori (valore 10 indica da 0 a 10)
	pos =  pDoc->c_difCoil[index].posizione; // posizione del metro dif
	// NOT trepassing end of strip
	if (pos >= subNastro.bottom)
		break;
	// before ? start of strip?
	if (pos < subNastro.top)
		{
		index ++;
		continue;
		}

	CString str;
	long nDif,nDifA,nDifB,nDifC,nDifD,nDifBig;
	// no dif A e B
	#ifdef INCLUDE_HOLEA
		nDifA = (long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'A');
	#else
		nDifA = 0;
	#endif
	#ifdef INCLUDE_HOLEB
		nDifB =	(long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'B');
	#else
		nDifB = 0;
	#endif
	nDifC =	(long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'C');
	nDifD =	(long)pDoc->c_difCoil[index].getValNum(subNastro.left,subNastro.right,'D');
	nDifBig =	(long)pDoc->c_difCoil[index].getValBigHole(subNastro.left,subNastro.right);
	nDif = nDifA + nDifB + nDifC + nDifD + nDifBig;
	if (nDif > 0)
		{// potrebbero non esserci difetti in qs metro/strip
		col = 0;
		row ++;
		double pos = pDoc->c_difCoil[index].posizione;
		pos -= subNastro.top;
		cell = sheet1->Cell(row, col);
		cell->SetDouble(pos);
		col ++;
		//
		cell = sheet1->Cell(row, col);
		cell->SetInteger(nDif);
		col ++;
#ifdef INCLUDE_HOLEA
		//
		cell = sheet1->Cell(row, col);
		cell->SetInteger(nDifA);
		col ++;
#endif
		
#ifdef INCLUDE_HOLEB
		//
		cell = sheet1->Cell(row, col);
		cell->SetInteger(nDifB);
		col ++;
#endif
		//
		cell = sheet1->Cell(row, col);
		cell->SetInteger(nDifC);
		col ++;
		//
		cell = sheet1->Cell(row, col);
		cell->SetInteger(nDifD);
		col ++;
		//
		cell = sheet1->Cell(row, col);
		cell->SetInteger(nDifBig);
		col ++;
		//
		}
	index ++;
	}

xls.SaveAs(fileName);

return (fileName);
}

#endif 
// #ifndef Csm20El2013_REPORT