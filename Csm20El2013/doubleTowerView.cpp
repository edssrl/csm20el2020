// TowerView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"

#include "DoubleTowerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDoubleTowerView

IMPLEMENT_DYNCREATE(CDoubleTowerView, CView)

CDoubleTowerView::CDoubleTowerView()
{
vType = TOWERVIEW;

// Init layout Fix Attributes

CProfile profile;
CString s;

// Top label size
layoutTop.fCaption.size = profile.getProfileInt("TowerLayoutTop","CaptionSize",110);
layoutTop.fLabel.size =  profile.getProfileInt("TowerLayoutTop","LabelSize",100);
layoutTop.fNormal.size =  profile.getProfileInt("TowerLayoutTop","NormalSize",70);
layoutTop.setMirrorLabel(FALSE);

// Bottom label size
layoutBottom.fCaption.size = profile.getProfileInt("TowerLayoutTop","CaptionSize",110);
layoutBottom.fLabel.size =  profile.getProfileInt("TowerLayoutTop","LabelSize",100);
layoutBottom.fNormal.size =  profile.getProfileInt("TowerLayoutTop","NormalSize",70);
layoutBottom.setMirrorLabel(FALSE);

// newline top
s = profile.getProfileString("TowerLayoutTop","NewLine","8");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutTop.c_nlAfterLabel [v] = 1;
	}

// newline bottom
s = profile.getProfileString("TowerLayoutBottom","NewLine","8");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutBottom.c_nlAfterLabel [v] = 1;
	}

// labelOrder top
s = profile.getProfileString("TowerLayoutTop","LabelOrder","0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutTop.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

// labelOrder bottom
s = profile.getProfileString("TowerLayoutBottom","LabelOrder","0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutBottom.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

// pixSepLabel top
s = profile.getProfileString("TowerLayoutTop","PixelSepLabel",
							 "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layoutTop.c_pxSepLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

// pixSepLabel Bottom
s = profile.getProfileString("TowerLayoutBottom","PixelSepLabel",
							 "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layoutBottom.c_pxSepLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

// layout.setLabel ("label 1");
layoutTop.setViewType(TOWERVIEW);
layoutTop.setMode (LINEAR);
layoutBottom.setViewType(TOWERVIEW);
layoutBottom.setMode (LINEAR);
// Passato in risorse
//s = profile.getProfileString("TowerLayout","SCaption",
//							 "CROSS WEB DISTRIBUTION");
s.LoadString(CSM_GRAPHVIEW_TOWERCAPTION);

layoutTop.setCaption(s);
layoutBottom.setCaption(s);

//for (int i=0;i<MAX_NUMLABEL;i++)
//	{
//	CString sIndex;
//	sIndex.Format ("SLabel%d",i);
//	s = profile.getProfileString("TowerLayout",sIndex,sIndex);
//	if (s != sIndex)
//		layout.c_sLabel[i] = s;
//	}

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL0);
layoutTop.c_sLabel[0] = s;
layoutBottom.c_sLabel[0] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL1);
layoutTop.c_sLabel[1] = s;
layoutBottom.c_sLabel[1] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL2);
layoutTop.c_sLabel[2] = s;
layoutBottom.c_sLabel[2] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL3);
layoutTop.c_sLabel[3] = s;
layoutBottom.c_sLabel[3] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL4);
layoutTop.c_sLabel[4] = s;
layoutBottom.c_sLabel[4] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL5);
layoutTop.c_sLabel[5] = s;
layoutBottom.c_sLabel[5] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL6);
layoutTop.c_sLabel[6] = s;
layoutBottom.c_sLabel[6] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL7);
layoutTop.c_sLabel[7] = s;
layoutBottom.c_sLabel[7] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL8);
layoutTop.c_sLabel[8] = s;
layoutBottom.c_sLabel[8] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL9);
layoutTop.c_sLabel[9] = s;
layoutBottom.c_sLabel[9] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL10);
layoutTop.c_sLabel[10] = s;
layoutBottom.c_sLabel[10] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL11);
layoutTop.c_sLabel[11] = s;
layoutBottom.c_sLabel[11] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL12);
layoutTop.c_sLabel[12] = s;
layoutBottom.c_sLabel[12] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL13);
layoutTop.c_sLabel[13] = s;
layoutBottom.c_sLabel[13] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL14);
layoutTop.c_sLabel[14] = s;
layoutBottom.c_sLabel[14] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL15);
layoutTop.c_sLabel[15] = s;
layoutBottom.c_sLabel[15] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL16);
layoutTop.c_sLabel[16] = s;
layoutBottom.c_sLabel[16] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL17);
layoutTop.c_sLabel[17] = s;
layoutBottom.c_sLabel[17] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL18);
layoutTop.c_sLabel[18] = s;
layoutBottom.c_sLabel[18] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL19);
layoutTop.c_sLabel[19] = s;
layoutBottom.c_sLabel[19] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL20);
layoutTop.c_sLabel[20] = s;
layoutBottom.c_sLabel[20] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL21);
layoutTop.c_sLabel[21] = s;
layoutBottom.c_sLabel[21] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL22);
layoutTop.c_sLabel[22] = s;
layoutBottom.c_sLabel[22] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL23);
layoutTop.c_sLabel[23] = s;
layoutBottom.c_sLabel[23] = s;

for (int i=0;(i<=4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	if (i == 4)
		s = "Big";

	layoutTop.c_cLabel[i] = s;
	layoutTop.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layoutTop.c_cLabel[i].setTipoIcon(IPALLINO);
	//
	layoutBottom.c_cLabel[i] = s;
	layoutBottom.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layoutBottom.c_cLabel[i].setTipoIcon(IPALLINO);
	}

// CalcDrawRect Top
layoutTop.c_viewTopPerc = profile.getProfileInt("TowerLayoutTop","ViewTop%",10);
layoutTop.c_viewBottomPerc = profile.getProfileInt("TowerLayoutTop","ViewBottom%",25);
layoutTop.c_viewLeftPerc = profile.getProfileInt("TowerLayoutTop","ViewLeft%",8);
layoutTop.c_viewRightPerc = profile.getProfileInt("TowerLayoutTop","ViewRight%",5);

// CalcDrawRect Bottom
layoutBottom.c_viewTopPerc = profile.getProfileInt("TowerLayoutBottom","ViewTop%",10);
layoutBottom.c_viewBottomPerc = profile.getProfileInt("TowerLayoutBottom","ViewBottom%",25);
layoutBottom.c_viewLeftPerc = profile.getProfileInt("TowerLayoutBottom","ViewLeft%",8);
layoutBottom.c_viewRightPerc = profile.getProfileInt("TowerLayoutBottom","ViewRight%",5);
	
}

CDoubleTowerView::~CDoubleTowerView()
{
}


BEGIN_MESSAGE_MAP(CDoubleTowerView, CView)
	//{{AFX_MSG_MAP(CDoubleTowerView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDoubleTowerView drawing



// Retrieve top and bottom doc
DOC_CLASS* CDoubleTowerView::getTopDocument()
{
DOC_CLASS* pDocTop = (DOC_CLASS* )GetDocument();
if (!pDocTop->isTopSide())
	{
	CDocTemplate* pDocTemplate = pDocTop->GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	while(!pDocTop->isTopSide())
		pDocTop = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	}
return (pDocTop);
}

DOC_CLASS* CDoubleTowerView::getBottomDocument()
{
DOC_CLASS* pDocBottom = (DOC_CLASS* )GetDocument();
if (pDocBottom->isTopSide())
	{
	CDocTemplate* pDocTemplate = pDocBottom->GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	while(pDocBottom->isTopSide())
		pDocBottom = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	}
return (pDocBottom);
}

// sezione posizionamento finestre
CRect CDoubleTowerView::getRectTop(void)
{
CRect rcBoundsTop;

GetClientRect (rcBoundsTop);
CPoint pt;

pt = rcBoundsTop.BottomRight();
pt.y -= rcBoundsTop.Size().cy /2;
rcBoundsTop.BottomRight() = pt;

return (rcBoundsTop);
}

CRect CDoubleTowerView::getDrawRectTop(void)
{
CRect rcBoundsTop,rect;

rcBoundsTop = getRectTop();

layoutTop.CalcDrawRect(rcBoundsTop,rect);
return (rect);
}

CRect CDoubleTowerView::getRectBottom(void)
{
CRect rcBoundsBottom,rect;

GetClientRect (rcBoundsBottom);


CPoint pt;
pt = rcBoundsBottom.TopLeft();
pt.y += rcBoundsBottom.Size().cy /2;
rcBoundsBottom.TopLeft() = pt;

return (rcBoundsBottom);
}

CRect CDoubleTowerView::getDrawRectBottom(void)
{
CRect rcBoundsBottom,rect;

rcBoundsBottom = getRectBottom();

layoutBottom.CalcDrawRect(rcBoundsBottom,rect);
return (rect);
}

void CDoubleTowerView::OnDraw(CDC* pDC)
{
CDC pdcMem;
CBitmap bitMapMem,*bitMapOld;
// rc regione totale disegnocx

CRect rectB;
CRect rc;
GetClientRect (rc);
// Set base Rect

SIZE sz;
sz.cx = rc.right - rc.left;
sz.cy = rc.bottom - rc.top;

//AtlPixelToHiMetric (&sz, &m_sizeExtent);
// store natural extent
//m_sizeNatural = m_sizeExtent;

// Create mem DC
pdcMem.CreateCompatibleDC(pDC);

// Create mem bitmap
bitMapMem.CreateCompatibleBitmap(pDC,rc.right-rc.left,
								   rc.bottom-rc.top);
// Select  mem bitmap
bitMapOld = (CBitmap*)  pdcMem.SelectObject(&bitMapMem);

//draw (di.hdcDraw,rc);
CRect rcTop,rcBottom;
rcTop = getRectTop();
layoutTop.OnEraseBkgnd(&pdcMem,rcTop);

rcBottom = getRectBottom();
layoutBottom.OnEraseBkgnd(&pdcMem,rcBottom);
// Draw on bitmap 
onDraw (&pdcMem);

//onDraw (pDC);

// replay view with bitmap
pDC->BitBlt(rc.left,rc.top,rc.right-rc.left,
			rc.bottom-rc.top,&pdcMem,0,0,SRCCOPY);

// dispose object
pdcMem.SelectObject(bitMapOld);
bitMapMem.DeleteObject();
pdcMem.DeleteDC();

}

void CDoubleTowerView::onDraw(CDC* pDC)
{
// TODO: add draw code here
// TODO: add draw code here
// Call Base Class Draw
CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);

DOC_CLASS* pDocTop = getTopDocument();
DOC_CLASS* pDocBottom = getBottomDocument();

// top space
CRect rcBoundsTop;
rcBoundsTop = getRectTop();

CRect rcBoundsBottom;
rcBoundsBottom = getRectBottom();

// Set base Rect
layoutTop.setDrawRect(rcBoundsTop);
layoutBottom.setDrawRect(rcBoundsBottom);

onDraw(pDC,pDocBottom,&layoutBottom,&towerViewBottom,rcBoundsBottom);

onDraw(pDC,pDocTop,&layoutTop,&towerViewTop,rcBoundsTop);
}


void CDoubleTowerView::onDraw(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,
							  CArray <ViewDensUnit,ViewDensUnit &> *pTowerView,CRect rcBounds)
{

// TODO: add draw code here
// Call Base Class Draw

// Call Base Class Draw
//-----------------------------------------
// Init vLabel 
// pLayout->setVFori	(pDoc->getFori());
pLayout->c_vLabel[0] = pDoc->GetVPosition();
pLayout->c_vLabel [1] = pDoc->GetTrendDVPositionDal();

// densita D
	{
	CString str;
	pLayout->setMaxY('D',pDoc->GetTrendDScalaY());
	pLayout->setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		pLayout->c_vLabel[2].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[2].setBColor(RGB(0,255,0));
	}
//

// densita C
	{
	CString str;
	pLayout->setMaxY('C',pDoc->GetTrendDScalaY());
	pLayout->setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		pLayout->c_vLabel[3].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[3].setBColor(RGB(0,255,0));
	}
//

// densita B
	{
	CString str;
	pLayout->setMaxY('B',pDoc->GetTrendDScalaY());
	pLayout->setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		pLayout->c_vLabel[4].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[4].setBColor(RGB(0,255,0));
	}
//
// densita A
	{
	CString str;
	pLayout->setMaxY('A',pDoc->GetTrendDScalaY());
	pLayout->setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		pLayout->c_vLabel[5].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[5].setBColor(RGB(0,255,0));
	}
//
pLayout->c_vLabel [6] = pDoc->getRotolo();
pLayout->c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
pLayout->c_vLabel [8] = d.Format( "%A, %B %d, %Y" );

// Visualizzazione alzata
int metriAlzata;
int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
pLayout->c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
pLayout->c_vLabel [10] = s;

s = pDoc->getCurAlzString();
pLayout->c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
pLayout->c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
pLayout->c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
pLayout->c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
pLayout->c_vLabel [15] = s;
// Total Holes A+B+C
s.Format("%6.0lf",va+vb+vc+vd);
pLayout->c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
pLayout->c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
pLayout->c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
pLayout->c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
pLayout->c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
pLayout->c_vLabel [21] = s;

//--------------------

pLayout->setNumSchede(pDoc->c_difCoil.getNumSchede());
int numClassi = pDoc->c_difCoil.getNumClassi();
pLayout->setNumClassi(numClassi);
pLayout->setNumLabelClassi(numClassi);
pLayout->setNumCpu(pDoc->c_numCpu);

pLayout->draw(pDC);

// Trovo Finestra disegno (grigia)
CRect rectB;
pLayout->CalcDrawRect(rcBounds,rectB); 

// Disegno Posizione bobine
if(pDoc->c_viewStripBorder)
	// Disegno Posizione bobine doppia riga
	drawPosBobine2(pDC,pDoc,pLayout,rectB);
else
	// Disegno Posizione bobine singola riga
	drawPosBobine1(pDC,pDoc,pLayout,rectB);

if(pDoc->c_viewStripBorderPosition)
	textPosBobine2(pDC,pDoc,pLayout,rectB);
		
// Disegno Posizione diaframmi
if (pDoc->c_useDiaframmi)
	drawPosDiaframma(pDC,pDoc,pLayout,rectB);

// Parametrizzata scala orizzontale su layout
double Xsize = 1. * pLayout->c_sizeBanda / pLayout->c_bandePerCol;

CRect clientRect;
clientRect = rectB;
clientRect.right =   clientRect.left;

CFont font,*pOldFont;
font.CreatePointFont (70,"Times New Roman",pDC);
pOldFont = pDC->SelectObject(&font); 


for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	double *dVal = new double [pDoc->c_difCoil.getNumClassi()+1]; // +1 Big
	int j;
	for (j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		// Sostituita densita` con totali per classi
		// dVal[j] = pDoc->c_difCoil.getValDensita(i,'A'+j,pDoc->GetTrendDScalaX());
		// sostituita funzione veloce che usa totali	20-01-99 
		//dVal[j] = pDoc->c_difCoil.getTotDifetti(i,'A'+j,0,
		//	(int) pDoc->c_difCoil.getMeter());
		dVal[j] = pDoc->c_difCoil.getAllDifetti(i,'A'+j);
		}
	// big 
	//dVal[j] = pDoc->c_difCoil.getTotBigHole(0,(int)pDoc->c_difCoil.getMeter(),i,i+1);
	dVal[j] = pDoc->c_difCoil.getAllBigHole(i);
	double a;
	if (dVal[1]>0)
		a = dVal[1];
	// inserito offset inizio grafico
	if (pLayout->getMirrorLabel())
		{
		clientRect.left  = rectB.right - pLayout->c_sizeFBanda - (int ) (Xsize * i);
		clientRect.right = rectB.right - pLayout->c_sizeFBanda - (int ) (Xsize * (i+1));
		}
	else
		{
		clientRect.left  = rectB.left + pLayout->c_sizeFBanda + (int ) (Xsize * i);
		clientRect.right = rectB.left + pLayout->c_sizeFBanda + (int ) (Xsize * (i+1));
		}

	if (pLayout->getMirrorLabel())
		{
		(*pTowerView)[(pDoc->c_difCoil.getNumSchede()-1)-i].setScale(clientRect,pDoc->GetTrendDScalaY(),dVal);
		(*pTowerView)[(pDoc->c_difCoil.getNumSchede()-1)-i].setPos(pDoc->c_difCoil.getNumSchede()-i);
		(*pTowerView)[(pDoc->c_difCoil.getNumSchede()-1)-i].draw(pDC);
		}
	else
		{
		(*pTowerView)[i].setScale(clientRect,pDoc->GetTrendDScalaY(),dVal);
		(*pTowerView)[i].setPos(i+1);
		(*pTowerView)[i].draw(pDC);
		}

		
	delete [] dVal;
	}

// Restore old font 
pDC->SelectObject(pOldFont); 
}



/////////////////////////////////////////////////////////////////////////////
// CDoubleTowerView diagnostics

#ifdef _DEBUG
void CDoubleTowerView::AssertValid() const
{
	CView::AssertValid();
}

void CDoubleTowerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDoubleTowerView message handlers

BOOL CDoubleTowerView::OnEraseBkgnd(CDC* pDC) 
{
return 1;
// TODO: Add your message handler code here and/or call default
CRect r;
r = getDrawRectTop();
layoutTop.OnEraseBkgnd(pDC,r);

//GetClientRect(&r);
//layout.OnEraseBkgnd(pDC,r);

r = getDrawRectBottom();
layoutBottom.OnEraseBkgnd(pDC,r);


return TRUE;	
}

BOOL CDoubleTowerView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{

// TODO: Add your specialized code here and/or call the base class
if (!CView::Create(lpszClassName, lpszWindowName, 
	dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);


// Create Top
{// only for variable scope
DOC_CLASS* pDoc = (DOC_CLASS* )getTopDocument();

towerViewTop.SetSize (pDoc->c_difCoil.getNumSchede()); 

layoutTop.Create(this);

// init colors pen and brush
// +1 Big  => 16-02-2010 No Big
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{// +1 Big
	towerViewTop[i].init (pDoc->c_difCoil.getNumClassi());  // +1 Big
	towerViewTop[i].setMode (TOWER);
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)      // +1 Big
		{
		towerViewTop[i].setPenTB (j,CSM20GetClassColor(j));	  // Top and Bottom color
		towerViewTop[i].setPenLR (j,layoutTop.getRgbBackColor());	  // Red
		towerViewTop[i].setBrush (j,CSM20GetClassColor(j));
		}
	}
}


// Create Bottom
{// only for variable scope
DOC_CLASS* pDoc = (DOC_CLASS* )getBottomDocument();

towerViewBottom.SetSize (pDoc->c_difCoil.getNumSchede()); 

layoutBottom.Create(this);

// init colors pen and brush
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	// +1 Big  => 16-02-2010 No Big
	towerViewBottom[i].init (pDoc->c_difCoil.getNumClassi()); // +1 Big
	towerViewBottom[i].setMode (TOWER);
	for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)  // +1 Big
		{
		towerViewBottom[i].setPenTB (j,CSM20GetClassColor(j));	  // Top and Bottom color
		towerViewBottom[i].setPenLR (j,layoutBottom.getRgbBackColor());	  // Red
		towerViewBottom[i].setBrush (j,CSM20GetClassColor(j));
		}
	}
}

return TRUE;
}




BOOL CDoubleTowerView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString s;
	s.Format("Got Notify Message from %d",(int)wParam);
	AfxMessageBox (s);
	return CView::OnNotify(wParam, lParam, pResult);
}

void CDoubleTowerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
DOC_CLASS* pDocTop = getTopDocument();
DOC_CLASS* pDocBottom = getBottomDocument();

if ((lHint==0) && (pHint == NULL))
	{//redraw completo
	//CRect rcBounds,rectB;
	//GetClientRect (rcBounds);
	// top
	// layoutTop.CalcDrawRect(rcBounds,rectB);
	CView::OnUpdate(pSender,lHint,pHint);	
	}
else
	{
	CRect rcBounds;
	CRect rectB;
	rcBounds = getRectTop();
	// Trovo Finestra disegno (grigia)
	layoutTop.CalcDrawRect(rcBounds,rectB); 
	// InvalidateRect(rectB);

	{
	CRect clipRect(rectB);
	CDC  *pDC;
	pDC = GetDC ();
	pDC->IntersectClipRect (clipRect);
	updateTower(pDC,pDocTop,&layoutTop,&towerViewTop,rectB);
	updateLabel(pDocTop,&layoutTop);
	updateExtra(pDC,pDocTop,&layoutTop,rectB);
	ReleaseDC(pDC);
	}

	rcBounds = getRectBottom();
	layoutBottom.CalcDrawRect(rcBounds,rectB); 
	//InvalidateRect(rectB);
	{
	CRect clipRect(rectB);
	CDC  *pDC;
	pDC = GetDC ();
	pDC->IntersectClipRect (clipRect);
	updateTower(pDC,pDocBottom,&layoutBottom,&towerViewBottom,rectB);
	updateLabel(pDocBottom,&layoutBottom);
	updateExtra(pDC,pDocBottom,&layoutBottom,rectB);
	ReleaseDC(pDC);
	}

	}	

}

void CDoubleTowerView::drawPosDiaframma(CDC *pDC,DOC_CLASS* pDoc,Layout* pLayout, CRect rectB)
{
CPen pen1,pen2,*oldPen;


// red
	
	pen1.CreatePen(PS_SOLID,1,RGB(196,0,0));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft;
	if (pLayout->getMirrorLabel())
		scPosLeft = rectB.right - pLayout->c_sizeFBanda - (1.  * posLeft * 
			(double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left +pLayout->c_sizeFBanda + (1.  * posLeft * 
			(double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));


	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	pen2.CreatePen(PS_SOLID,1,RGB(196,0,0));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight;
	if (pLayout->getMirrorLabel())
		scPosRight = rectB.right - pLayout->c_sizeFBanda - (1.  * posRight * 
			(double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
	else
		scPosRight = rectB.left +pLayout->c_sizeFBanda + (1.  * posRight * 
			(double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));


	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------

}


void CDoubleTowerView::drawPosBobine2(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB)
{

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	CPen pen1;
	// black
	pen0.CreatePen(PS_SOLID,1,RGB(0,0,0));
	// black
	pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft;
	if (pLayout->getMirrorLabel())
		scPosLeft = rectB.right - pLayout->c_sizeFBanda-(1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left + pLayout->c_sizeFBanda+(1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));


//	pDC->MoveTo(scPosLeft,rectB.top);
//	pDC->LineTo(scPosLeft,rectB.bottom);

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga  a dx strip
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		if (pLayout->getMirrorLabel())
			scPosLeft = rectB.right - pLayout->c_sizeFBanda - (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		//---------------------------------
		// seconda riga a sinistra strip
		pDC->SelectObject(&pen1);	
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		if (pLayout->getMirrorLabel())
			scPosLeft = rectB.right - pLayout->c_sizeFBanda - (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));


		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------

}



// visualizzazione posizione bobine per utente
void CDoubleTowerView::drawPosBobine1(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB)
{

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx

	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	pen0.CreatePen(PS_SOLID,2,RGB(0,0,0));
	// red
	pen1.CreatePen(PS_SOLID,2,RGB(0,0,0));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +pLayout->c_sizeFBanda 
	int scPosLeft;
	if (pLayout->getMirrorLabel())
		scPosLeft = rectB.right - pLayout->c_sizeFBanda-(1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left + pLayout->c_sizeFBanda+(1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

//
// disegno solo inizio strip 
// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga sinistra
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		
		// check distanza con fine strip precedente
		if (i > 0)
			{
			posLeft -= (fabs(posLeft - endPosLastStrip)/2);
			}

		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
					+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +pLayout->c_sizeFBanda 
		if (pLayout->getMirrorLabel())
			scPosLeft = rectB.right - pLayout->c_sizeFBanda - (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
	

		if (i == (pDoc->c_rotoloMapping[lastElem].size()-1))
			{
			// seconda riga destra
			pDC->SelectObject(&pen1);	

			posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
			// 15-09-2004 corretto centrato +pLayout->c_sizeFBanda 
			if (pLayout->getMirrorLabel())
				scPosLeft = rectB.right - pLayout->c_sizeFBanda - (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
			else	
				scPosLeft = rectB.left + pLayout->c_sizeFBanda+ (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);

			}
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
}

void CDoubleTowerView::textPosBobine2(CDC *pDC, DOC_CLASS* pDoc,Layout* pLayout,CRect rectB)
{

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);
	
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft;
	if (pLayout->getMirrorLabel())
		scPosLeft = rectB.right - pLayout->c_sizeFBanda-(1. * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
	else
		scPosLeft = rectB.left + pLayout->c_sizeFBanda+(1. * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga  a dx strip
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		if (pLayout->getMirrorLabel())
			scPosLeft = rectB.right - pLayout->c_sizeFBanda - (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));

		// riferimenti numerici
		CString sl;
		pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
	
		//---------------------------------
		// seconda riga a sinistra strip
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		if (pLayout->getMirrorLabel())
			scPosLeft = rectB.right - pLayout->c_sizeFBanda - (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
		else
			scPosLeft = rectB.left + pLayout->c_sizeFBanda + (1.  * posLeft * (double)pLayout->c_sizeBanda / (pLayout->c_bandePerCol*SIZE_BANDE));
		
		pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
 		}
	pDC->SelectObject(oldFont); 
	font.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------

}


void CDoubleTowerView::updateLabel(DOC_CLASS* pDoc,Layout* pLayout)
{

// Init vLabel 
// pLayout->setVFori	(pDoc->getFori());

pLayout->c_vLabel[0] = pDoc->GetVPosition();
pLayout->c_vLabel [1] = pDoc->GetTrendDVPositionDal();

// densita D
	{
	CString str;
	pLayout->setMaxY('D',pDoc->GetTrendDScalaY());
	pLayout->setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		pLayout->c_vLabel[2].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[2].setBColor(RGB(0,255,0));
	}

// densita C
	{
	CString str;
	pLayout->setMaxY('C',pDoc->GetTrendDScalaY());
	pLayout->setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		pLayout->c_vLabel[3].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[3].setBColor(RGB(0,255,0));
	}
//// densita B
	{
	CString str;
	pLayout->setMaxY('B',pDoc->GetTrendDScalaY());
	pLayout->setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		pLayout->c_vLabel[4].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[4].setBColor(RGB(0,255,0));
	}
//
// densita A
	{
	CString str;
	pLayout->setMaxY('A',pDoc->GetTrendDScalaY());
	pLayout->setMinY('A',0.);

	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		pLayout->c_vLabel[5].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[5].setBColor(RGB(0,255,0));
	}
//
pLayout->c_vLabel [6] = pDoc->getRotolo();
pLayout->c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
pLayout->c_vLabel [8] = d.Format( "%A, %B %d, %Y" );

// Visualizzazione alzata
int metriAlzata;
int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
pLayout->c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
pLayout->c_vLabel [10] = s;

//s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size());
// -1 indica tutti
//s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).getCurAlzNumber(-1));
s = pDoc->getCurAlzString();
pLayout->c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
pLayout->c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
pLayout->c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
pLayout->c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
pLayout->c_vLabel [15] = s;
// Total Holes A+B+C
s.Format("%6.0lf",va+vb+vc+vd);
pLayout->c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
pLayout->c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
pLayout->c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
pLayout->c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
pLayout->c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
pLayout->c_vLabel [21] = s;

}


void CDoubleTowerView::updateExtra(CDC  *pDC, DOC_CLASS* pDoc,Layout* pLayout, CRect rectB)
{

// Disegno Posizione bobine
if(pDoc->c_viewStripBorder)
	// Disegno Posizione bobine doppia riga
	drawPosBobine2(pDC,pDoc,pLayout,rectB);
else
	// Disegno Posizione bobine singola riga
	drawPosBobine1(pDC,pDoc,pLayout,rectB);

if(pDoc->c_viewStripBorderPosition)
	textPosBobine2(pDC,pDoc,pLayout,rectB);
	
// Disegno Posizione diaframmi
if (pDoc->c_useDiaframmi)
	drawPosDiaframma(pDC,pDoc,pLayout,rectB);

}


void CDoubleTowerView::updateTower(CDC  *pDC,DOC_CLASS* pDoc,Layout* pLayout, 
								   CArray <ViewDensUnit,ViewDensUnit &> *pTowerView, CRect rectB)
{
double *dVal = new double [pDoc->c_difCoil.getNumClassi()+1];	// +1 Big

for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	int j;
	for (j=0;j<pDoc->c_difCoil.getNumClassi();j++)
		{
		int a;
		dVal[j] = pDoc->c_difCoil.getAllDifetti(i,'A'+j);
		if (dVal[j] > 0)
			a=0;
		}
	// big 
	// dVal[j] = pDoc->c_difCoil.getTotBigHole(0,(int)pDoc->c_difCoil.getMeter(),i,i+1);
	dVal[j] = pDoc->c_difCoil.getAllBigHole(i);
	
	// inserito offset inizio grafico
	//clientRect.left  = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * i);
	//clientRect.right = rectB.left + layout.c_sizeFBanda + (int ) (Xsize * (i+1));

	// towerView[i].setScale(clientRect,pDoc->GetTrendDScalaY(),dVal);
	if (pLayout->getMirrorLabel())
		{
		(*pTowerView)[(pDoc->c_difCoil.getNumSchede()-1)-i].setVal(dVal);
		(*pTowerView)[(pDoc->c_difCoil.getNumSchede()-1)-i].setPos(pDoc->c_difCoil.getNumSchede()-i);
		(*pTowerView)[(pDoc->c_difCoil.getNumSchede()-1)-i].draw(pDC);
		}
	else
		{
		(*pTowerView)[i].setVal(dVal);
		(*pTowerView)[i].setPos(i+1); 
		(*pTowerView)[i].draw(pDC);	
		}
	}
delete [] dVal;

}