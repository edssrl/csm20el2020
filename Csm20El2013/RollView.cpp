// RollView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"
// #include "ctrlext.h"
// #include "ListCtrlUp.h"

#include "RollView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRollView

IMPLEMENT_DYNCREATE(CRollView, CView)

CRollView::CRollView()
{
vType = ROLLVIEW;

c_timerId = 0;

layout.setViewType(ROLLVIEW);
layout.setMode(NOLABELV | USE_STRIPRULER);

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();
//layout.c_mirrorDefect = pFrame->c_rightAlignStrip;
// layout.setMirrorLabel(pFrame->c_rightAlignStrip);

// Init layout Fix Attributes
CProfile profile;
CString s;


layout.fCaption.size = profile.getProfileInt("RollmapLayout","CaptionSize",120);
layout.fLabel.size = profile.getProfileInt("RollmapLayout","LabelSize",100);
layout.fNormal.size = profile.getProfileInt("RollmapLayout","NormalSize",110);


s = profile.getProfileString("RollmapLayout","NewLine","3,6");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString("RollmapLayout","LabelOrder","0,1,2,3,4,5,6,7,8,9");
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

s = profile.getProfileString("RollmapLayout","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layout.c_pxSepLabel [k++] = v;
	if (k>9)
		break;
	}

layout.c_pxSepLabelEdit = profile.getProfileInt("RollmapLayout","PixelSepLabelEdit",10);


//s = profile.getProfileString("RollmapLayout","SCaption",
//							 "ROLLING MAP");

s.LoadString(CSM_GRAPHVIEW_ROLLCAPTION);

layout.setCaption(s);
//for (int i=0;i<MAX_NUMLABEL;i++)
//	{
//	CString sIndex;
//	sIndex.Format ("SLabel%d",i);
//	s = profile.getProfileString("RollmapLayout",sIndex,sIndex);
//	if (s != sIndex)
//		layout.c_sLabel[i] = s;
//	}



s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL0);
layout.c_sLabel[0] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL1);
layout.c_sLabel[1] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL2);
layout.c_sLabel[2] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL3);
layout.c_sLabel[3] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL4);
layout.c_sLabel[4] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL5);
layout.c_sLabel[5] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL6);
layout.c_sLabel[6] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL7);
layout.c_sLabel[7] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL8);
layout.c_sLabel[8] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL9);
layout.c_sLabel[9] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL10);
layout.c_sLabel[10] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL11);
layout.c_sLabel[11] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL12);
layout.c_sLabel[12] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL13);
layout.c_sLabel[13] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL14);
layout.c_sLabel[14] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL15);
layout.c_sLabel[15] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL16);
layout.c_sLabel[16] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL17);
layout.c_sLabel[17] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL18);
layout.c_sLabel[18] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL19);
layout.c_sLabel[19] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL20);
layout.c_sLabel[20] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL21);
layout.c_sLabel[21] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL22);
layout.c_sLabel[22] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL23);
layout.c_sLabel[23] = s;

// ora anche Big!
// inserisco onClick su doubleClick dei pallini
// id che arriva e` 100 + classe: A=100,B=101 etc
// vedi CLabel
for (int i=0;(i<=4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	if (i == 4)
		s = "Big";
	layout.c_cLabel[i] = s;
	layout.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layout.c_cLabel[i].setTipoIcon(IPALLINO);
	layout.c_cLabel[i].setNotify(TRUE);
	layout.c_cLabel[i].setSensDbclk (TRUE);

	}


// CalcDrawRect
layout.c_viewTopPerc = profile.getProfileInt("RollmapLayout","ViewTop%",10);
layout.c_viewBottomPerc = profile.getProfileInt("RollmapLayout","ViewBottom%",25);
layout.c_viewLeftPerc = profile.getProfileInt("RollmapLayout","ViewLeft%",8);
layout.c_viewRightPerc = profile.getProfileInt("RollmapLayout","ViewRight%",5);

// dimensione pallini
c_pxBRoll = profile.getProfileInt("RollmapLayout","RollSize",12);

// Don't update internal background
layout.setRgbDrawBackColor(RGB(0,0,0));

}

CRollView::~CRollView()
{

}


BEGIN_MESSAGE_MAP(CRollView, CView)
	//{{AFX_MSG_MAP(CRollView)
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
//	ON_WM_PARENTNOTIFY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRollView drawing

CRect CRollView::getDrawRect(void)
{
CRect rcBounds,rect;

GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rect);
return (rect);
}

void CRollView::OnDraw(CDC* pDC)
{
CDC pdcMem;
CBitmap bitMapMem,*bitMapOld;
// rc regione totale disegnocx

CRect rectB;
CRect rc;
GetClientRect (rc);
// Set base Rect

SIZE sz;
sz.cx = rc.right - rc.left;
sz.cy = rc.bottom - rc.top;

//AtlPixelToHiMetric (&sz, &m_sizeExtent);
// store natural extent
//m_sizeNatural = m_sizeExtent;

// Create mem DC
pdcMem.CreateCompatibleDC(pDC);

// Create mem bitmap
bitMapMem.CreateCompatibleBitmap(pDC,rc.right-rc.left,
								   rc.bottom-rc.top);
// Select  mem bitmap
bitMapOld = (CBitmap*)  pdcMem.SelectObject(&bitMapMem);

//draw (di.hdcDraw,rc);

layout.OnEraseBkgnd(&pdcMem,rc);
// Draw on bitmap 
onDraw (&pdcMem);
//onDraw (pDC);

// replay view with bitmap
pDC->BitBlt(rc.left,rc.top,rc.right-rc.left,
			rc.bottom-rc.top,&pdcMem,0,0,SRCCOPY);

// dispose object
pdcMem.SelectObject(bitMapOld);
bitMapMem.DeleteObject();
pdcMem.DeleteDC();


}

void CRollView::onDraw(CDC* pDC)
{
// TODO: add draw code here
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
// TODO: add draw code here
// Call Base Class Draw
CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);
// Call Base Class Draw

//-----------------------------------------
// Init vLabel 
// layout.setVFori	(pDoc->getFori());
layout.c_vLabel [0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendLVPositionDal();
// densita D
	{
	CString str;
	layout.setMaxY('D',pDoc->GetTrendDScalaY());
	layout.setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		layout.c_vLabel[2].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[2].setBColor(RGB(0,255,0));
	}
//
// densita C
	{
	CString str;
	layout.setMaxY('C',pDoc->GetTrendDScalaY());
	layout.setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		layout.c_vLabel[3].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[3].setBColor(RGB(0,255,0));
	}
//

// densita B
	{
	CString str;
	layout.setMaxY('B',pDoc->GetTrendDScalaY());
	layout.setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		layout.c_vLabel[4].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[4].setBColor(RGB(0,255,0));
	}
//

// densita A
	{
	CString str;
	layout.setMaxY('A',pDoc->GetTrendDScalaY());
	layout.setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		layout.c_vLabel[5].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[5].setBColor(RGB(0,255,0));
	}
	
//-----
layout.c_vLabel [6] = pDoc->getRotolo();
layout.c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
layout.c_vLabel [8] = d.Format( "%A, %B %d, %Y" );
// Visualizzazione alzata
int metriAlzata;

if(pDoc->c_rotoloMapping.c_lastElemento < 0)
	return;	

int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
layout.c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
layout.c_vLabel [10] = s;

s = pDoc->getCurAlzString();
layout.c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
layout.c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
layout.c_vLabel [13] = s;
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
layout.c_vLabel [14] = s;
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
layout.c_vLabel [15] = s;
// Total Holes A+B+C+D
s.Format("%6.0lf",va+vb+vc+vd);
layout.c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
layout.c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
layout.c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
layout.c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
layout.c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
layout.c_vLabel [21] = s;

// numero totale strip
//s.Format("%d",pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].size());
//layout.c_vLabel [16] = s;


//--------------------

int numClassi = pDoc->c_difCoil.getNumClassi();
// aggiungo anche classe Big
layout.setNumClassi(numClassi+1);
layout.setNumLabelClassi(numClassi+1);
layout.setNumSchede(pDoc->c_difCoil.getNumSchede());
layout.setNumCpu(pDoc->c_numCpu);

layout.draw(pDC);
layout.CalcDrawRect(rcBounds,rectB); 

// Disegno Posizione bobine
if(pDoc->c_viewStripBorder)
	// Disegno Posizione bobine doppia riga
	drawPosBobine2(pDC,rectB);
else
	// Disegno Posizione bobine singola riga
	drawPosBobine1(pDC,rectB);

if(pDoc->c_viewStripBorderPosition)
	textPosBobine2(pDC,rectB);
	
// Disegno Posizione diaframmi
if (pDoc->c_useDiaframmi)
	drawPosDiaframma(pDC,rectB);

	{
	//layout.OnEraseDrawBkgnd(pDC);
	pDoc->c_targetBoard.setDrawAttrib((int)layout.c_sizeFBanda,
							(int)layout.c_sizeBanda,
							c_pxBRoll,layout.c_bandePerCol);
	CRect clipRect(rectB);
	clipRect.TopLeft().x = rcBounds.TopLeft().x;
	clipRect.BottomRight().x = rcBounds.BottomRight().x;
	pDC->IntersectClipRect (clipRect);
	pDoc->c_targetBoard.draw(rectB,pDC);
	}
// c_listCtrl.MoveWindow(rectB);

// Sincronizzo contenuto 
//	c_listCtrl.UpdateWindow();

}

/////////////////////////////////////////////////////////////////////////////
// CRollView diagnostics

#ifdef _DEBUG
void CRollView::AssertValid() const
{
	CView::AssertValid();
}

void CRollView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRollView message handlers

void CRollView::OnInitialUpdate() 
{
CView::OnInitialUpdate();

CRect gRect,dRect;
GetClientRect(gRect);


DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
	
if (pDoc != NULL)
	{	
	for (int i=0;(i<=4);i++)
		{
		if(pDoc->c_targetBoard.isExcludedDotColor(layout.c_cLabel[i].c_tColor))
			{layout.c_cLabel[i].setTColor(RGB(64,64,64));}
		}
	}


// TODO: Add your specialized code here and/or call the base class
layout.CalcDrawRect (gRect,dRect);

// if (!c_listCtrl.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT,dRect,this,0))
//	AfxMessageBox ("Error Create listCtrl");

// c_listCtrl.SetTextColor (RGB(0,0,0));

// c_listCtrl.image.Create(IDB_BITMAP_DIF1,16,1,RGB(255,255,255));	
// c_listCtrl.SetImageList(&c_listCtrl.image,TVSIL_NORMAL);

/*
CString sH ("POSITION");
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	CString s;
	s.Format (",%2d",i+1);
	sH += s;
	}
*/


c_timerId = SetTimer(11,3000,NULL);

CRect rcBounds,rectB;
GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rectB);
int nr = rectB.Size().cy / c_pxBRoll;
fillCtrl(nr);
}


// Torna numero di righe aggiunte
// Inserisce tutte le volte al piu` N righe dove N sono  quelle visualizzabili
// Default inserisce solo al piu` nMaxRow
int CRollView::fillCtrl(int nmaxRow)
{ 
int valret = 0;
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

int toIndex = pDoc->c_difCoil.GetSize();
int fromIndex = max (toIndex - nmaxRow,0);

pDoc->c_targetBoard.setEmpty();

//vettore di abilitazioni per inserimento difetti 
// in rolling map in funzione dei colori attivati/disattivati 
// con doppio click pallini a dx rollingmap
BOOL enableFromColor [5];
for (int i=0;i<5;i++)
	enableFromColor [i] = (pDoc->c_targetBoard.isColorExcluded(CSM20GetClassColor(i)))?0:1;
		
for (int index=fromIndex;index <toIndex;index ++)
	{
	BOOL found = FALSE;
	// cerco schede
	int mSize = pDoc->c_difCoil.ElementAt(index).GetSize();
	for (int scIndex=0;scIndex<mSize;scIndex ++)
		{
		// add BigHole
		if (enableFromColor [4]&&
			(pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).BigHoleAt() > 0))
			{
			// found 
			CTarget t;
			t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
			t.setSize (CSize(c_pxBRoll,c_pxBRoll));
			//t.setColor (RGB(255,255,255));
			t.setColor (CSM20GetClassColor(4));
			pDoc->c_targetBoard.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t);
			found = TRUE;
			}
		int clSize = pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).GetSize();
		// for (int clIndex=0;clIndex<clSize;clIndex ++)
		for (int clIndex=(clSize-1);clIndex>=0;clIndex --)
			{// invertita priorita` di visualizzazione fori D + prioritari degli A
			if (enableFromColor [clIndex]&&
			(pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).ElementAt(clIndex) > 0))
				{
				// found 
				CTarget t;
				t.setBanda (pDoc->c_difCoil.ElementAt(index).ElementAt(scIndex).banda);
				t.setSize (CSize(c_pxBRoll,c_pxBRoll));
				t.setColor (CSM20GetClassColor(clIndex));
				pDoc->c_targetBoard.add(pDoc->c_difCoil.ElementAt(index).posizione*1000,t);
				found = TRUE;
				}
			}
		}
	if (found)
		{
		int p1,p2;
		p1 = pDoc->c_difCoil.ElementAt(index).posizione*1000;
		p2 = pDoc->c_targetBoard.getLastPos();
		if (p1 > p2)
			valret ++;
		}
	}
if (pDoc->c_targetBoard.GetSize() > 0)
	pDoc->c_targetBoard.setLastPos(pDoc->c_targetBoard.ElementAt(pDoc->c_targetBoard.GetUpperBound()).getPos());
else
	pDoc->c_targetBoard.setLastPos(0);
return (valret);
}


BOOL CRollView::OnEraseBkgnd(CDC* pDC) 
{
// TODO: Add your message handler code here and/or call default

CRect r;
GetClientRect(&r);

// semaforo repaint all 
// vedi OnUpdate
// layout.setExcludeBkgr(!c_repaintAll);
// layout.setExcludeBkgLV(!c_repaintAll);

layout.OnEraseBkgnd(pDC,r);

return TRUE;
}



BOOL CRollView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if (!CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);


return (layout.Create(this));

}



void CRollView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
// TODO: Add your specialized code here and/or call the base class


if ((lHint==0) && (pHint == NULL))
	{//redraw completo
	CRect rcBounds,rectB;
	GetClientRect (rcBounds);
	layout.CalcDrawRect(rcBounds,rectB);
	int nr = rectB.Size().cy / c_pxBRoll;
	int newRow = fillCtrl(nr);

	CView::OnUpdate(pSender,lHint,pHint);	
	}
else
	{
	CRect rcBounds,rectB;
	GetClientRect (rcBounds);
	layout.CalcDrawRect(rcBounds,rectB);
	int nr = rectB.Size().cy / c_pxBRoll;
	int newRow = fillCtrl(nr);

	DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
	if (newRow > 0)
		{// aggiornamento nuova riga
		pDoc->c_targetBoard.setDrawAttrib((int)layout.c_sizeFBanda,(int)layout.c_sizeBanda,
								c_pxBRoll,layout.c_bandePerCol);

		CRect clipRect(rectB);
		clipRect.TopLeft().x = rcBounds.TopLeft().x;
		CDC  *pDC;
		pDC = GetDC ();
		pDC->IntersectClipRect (clipRect);
		// Disegno Posizione bobine
		if(pDoc->c_viewStripBorder)
			// Disegno Posizione bobine doppia riga
			drawPosBobine2(pDC,rectB);
		else
			// Disegno Posizione bobine singola riga
			drawPosBobine1(pDC,rectB);

		drawPosDiaframma(pDC,rectB);
		pDoc->c_targetBoard.scroll(newRow,rectB,clipRect,pDC);	
		ReleaseDC(pDC);
		}
	else
		{
		if (lHint == 2)
			{ // aggiornamento stessa riga
			pDoc->c_targetBoard.setDrawAttrib((int)layout.c_sizeFBanda,(int)layout.c_sizeBanda,
									c_pxBRoll,layout.c_bandePerCol);
			CRect clipRect(rectB);
			clipRect.TopLeft().x = rcBounds.TopLeft().x;
			CDC  *pDC;
			pDC = GetDC ();
			pDC->IntersectClipRect (clipRect);
			// Disegno Posizione bobine
			if(pDoc->c_viewStripBorder)
				// Disegno Posizione bobine doppia riga
				drawPosBobine2(pDC,rectB);
			else
				// Disegno Posizione bobine singola riga
				drawPosBobine1(pDC,rectB);
			drawPosDiaframma(pDC,rectB);
			pDoc->c_targetBoard.reDraw(pDoc->c_targetBoard.GetUpperBound(),rectB,pDC);
			ReleaseDC(pDC);
			}
		}
	layout.c_vLabel[0] = pDoc->GetVPosition();
	// Visualizzazione alzata
	int metriAlzata;
	int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
	metriAlzata = pDoc->GetVPosition() - posAlzata;
	if((pDoc->c_rotoloMapping.size() == 1)&&
		(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
		metriAlzata -= pDoc->c_lunResStop;

	CString s;
	s.Format("%d",metriAlzata);
	layout.c_vLabel [9] = s;

	// densita D
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		layout.c_vLabel[2].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[2].setBColor(RGB(0,255,0));
	}
	// densita C
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		layout.c_vLabel[3].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[3].setBColor(RGB(0,255,0));
	}
	//--------------------
	// densita B
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		layout.c_vLabel[4].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[4].setBColor(RGB(0,255,0));
	}
	// densita A
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		layout.c_vLabel[5].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[5].setBColor(RGB(0,255,0));
	}
	//--------------------

	// Agosto 31
	// Total Holes A
	
	double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",va);
	layout.c_vLabel [12] = s;
	// Total Holes B
	double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",vb);
	layout.c_vLabel [13] = s;
	// Total Holes C
	double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",vc);
	layout.c_vLabel [14] = s;
	// Total Holes D
	double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",vd);
	layout.c_vLabel [15] = s;
	// Total Holes A+B
	s.Format("%6.0lf",va+vb+vc+vd);
	layout.c_vLabel [16] = s;
	// Limit A
	s.Format("%4.0lf",pDoc->sogliaA);
	layout.c_vLabel [17] = s;
	// Limit B
	s.Format("%4.0lf",pDoc->sogliaB);
	layout.c_vLabel [18] = s;
	// Limit C
	s.Format("%4.0lf",pDoc->sogliaC);
	layout.c_vLabel [19] = s;
	// Limit D
	s.Format("%4.0lf",pDoc->sogliaD);
	layout.c_vLabel [20] = s;
	// Strip Density
	double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
	de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
	de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
	de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
	if (de < 0.)
		de = 0.;
	s.Format("%6.02lf",de);
	layout.c_vLabel [21] = s;

	// numero totale strip
//	s.Format("%d",pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].size());
//	layout.c_vLabel [16] = s;
	}


}


void CRollView::drawPosDiaframma(CDC *pDC, CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
if (!pDoc->c_useDiaframmi)
	return;


CPen pen1,pen2,pen3,pen4,*oldPen;
	// red
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
// diaframmi indipendenti da dritto o rovescio
	{
	pen1.CreatePen(PS_SOLID,1,RGB(196,0,0));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left +layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	pen2.CreatePen(PS_SOLID,1,RGB(196,0,0));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	if (pDoc->c_mode == 1)
		{// disegno diaframmi testa 2 merge SingleCoil
		pen3.CreatePen(PS_SOLID,1,RGB(196,0,0));
		pDC->SelectObject(&pen3);
		double posLeft = pDoc->c_posDiaframmaSx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		int scPosLeft = rectB.left +layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		pen4.CreatePen(PS_SOLID,1,RGB(196,0,0));
		pDC->SelectObject(&pen4);
		double posRight = pDoc->c_posDiaframmaDx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		int scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosRight,rectB.top);
		pDC->LineTo(scPosRight,rectB.bottom);
		
		pDC->SelectObject(oldPen);
		pen3.DeleteObject();
		pen4.DeleteObject();
		}
	// non importa anche se seleziono la stessa pen due volte
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------
	}

}



void CRollView::drawPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();


CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
if (pDoc->c_rightAlignStrip)	
	{
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,1,RGB(196,196,196));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,1,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	// double globalWidth = 180 * SIZE_BANDE;
	// fixBug per sistema doppia configurazione
	//double globalWidth = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
	double posRight;
	posRight = pDoc->getPosColtello(lastElem);

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda
	int scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=pDoc->c_rotoloMapping[lastElem].size()-1;i>=0;i--)
		{
		// disegno solo inizio strip 
		// fine strip solo per ultimo strip
		double endPosLastStrip = 0.;

		// prima riga a dx strip
		pDC->SelectObject(&pen0);
	 	//////

		posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// disegno strip lato dx della banda
		posRight -= SIZE_BANDE;

		// check distanza con fine strip precedente
	//	if (i < pDoc->c_rotoloMapping[lastElem].size()-1)
	//		{
	//		posRight -= (fabs(posRight - endPosLastStrip)/2);
	//		}

		// memo  last end position
	//	endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
	//				- (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));



		//////
		// posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		//---------------------------------
		// seconda riga a sx strip
		pDC->SelectObject(&pen1);	
		
		posRight -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		// disegno strip lato dx della banda
		posRight += SIZE_BANDE;

	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		// posizioni riferite a sinistra 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
else
	//---------------------------------------------------------
	// Disegno Posizione bobine allineate a sx	
	{
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,1,RGB(196,196,196));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,1,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga a dx strip
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		//---------------------------------
		// seconda riga a sx strip
		pDC->SelectObject(&pen1);	
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
}

// visualizzazione posizione bobine per utente
void CRollView::drawPosBobine1(CDC *pDC,CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
if (pDoc->c_rightAlignStrip)	
	{
//---------------------------------------------------------
// Disegno Posizione bobine allineate a dx
	// si parte dall'ultimo strip e si sottrae larghezza (03/2019 )
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,2,RGB(196,196,196));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,2,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	// double globalWidth = 180 * SIZE_BANDE;
	// fixBug per sistema doppia configurazione
	//double globalWidth = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
	double posRight;
	posRight = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft = rectB.right - layout.c_sizeFBanda - ( (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE)));

//
// disegno solo inizio strip 
// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	//for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
	for (int i=pDoc->c_rotoloMapping[lastElem].size()-1;i>=0;i--)
		{
		// prima riga sinistra
		pDC->SelectObject(&pen0);
	 	// ok ultimo strip, for al contrario
		posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		
		// check distanza con fine strip precedente
		if (i < pDoc->c_rotoloMapping[lastElem].size()-1)
			{
			posRight -= (fabs(posRight - endPosLastStrip)/2);
			}

		// disegno strip lato sx della banda
		//posRight -= SIZE_BANDE;
		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
					- (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		// if (i == (pDoc->c_rotoloMapping[lastElem].size()-1))
		if (i == 0)
			{
			// seconda riga destra
			pDC->SelectObject(&pen1);	

			posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			posRight -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
			// disegno strip lato dx della banda
			//posRight += SIZE_BANDE;
		
			// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
			// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * (posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		//break;
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
else
	{	
//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	CPen pen1;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,2,RGB(196,196,196));
	// red
	// pen1.CreatePen(PS_DASH,1,RGB(128,0,0));
	pen1.CreatePen(PS_SOLID,2,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen0);
	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
	int scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

//
// disegno solo inizio strip 
// fine strip solo per ultimo strip
	double endPosLastStrip = 0.;
	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
		// prima riga sinistra
		pDC->SelectObject(&pen0);
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		
		// check distanza con fine strip precedente
		if (i > 0)
			{
			posLeft -= (fabs(posLeft - endPosLastStrip)/2);
			}

		// memo  last end position
		endPosLastStrip = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos
					+ (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;

		// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		if (i == (pDoc->c_rotoloMapping[lastElem].size()-1))
			{
			// seconda riga destra
			pDC->SelectObject(&pen1);	

			posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
			posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
			// 15-09-2004 corretto centrato +layoutTop.c_sizeFBanda 
			scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
			
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	pen1.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
	}
}


void CRollView::textPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
 
if (pDoc->c_rightAlignStrip) 
	{
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);

	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posRight;
	// double globalWidth = 180 * SIZE_BANDE;
	// fixBug per sistema doppia configurazione
	//double globalWidth = pDoc->c_difCoil.getNumSchede() * SIZE_BANDE;
	//double rightSide =  globalWidth;
	posRight =  pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.right - layout.c_sizeLBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	pDC->TextOut(rectB.right,rectB.top,"Right");

	for (int i=pDoc->c_rotoloMapping[lastElem].size()-1;i>=0;i--)
	// for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
	 	
		posRight = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// disegno strip lato dx della banda
		posRight += SIZE_BANDE;

		//posRight /= SIZE_BANDE;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		//scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		// scPosLeft = rectB.right - layout.c_sizeFBanda -  (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));


		// riferimenti numerici
		CString sl;
		pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		sl.Format("%3.0lf",posRight/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,sl);

		//---------------------------------
		// seconda riga a sx strip
		
		posRight -= (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		//posRight /= SIZE_BANDE;
		// disegno strip lato dx della banda
		posRight -= SIZE_BANDE;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		//scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		// scPosLeft = rectB.right - layout.c_sizeFBanda - (1.  * (globalWidth-posRight) * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		sl.Format("%3.0lf",posRight/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);

		}
	pDC->SelectObject(oldFont);
	font.DeleteObject();
	}
else
	{
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);

	int lastElem;
	lastElem = pDoc->c_rotoloMapping.c_lastElemento;
	double posLeft;
	posLeft = pDoc->getPosColtello(lastElem);
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosLeft = rectB.left + layout.c_sizeFBanda+(1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	for (int i=0;i<pDoc->c_rotoloMapping[lastElem].size();i++)
		{
	 	
		posLeft = (pDoc->c_rotoloMapping[lastElem])[i].c_xPos;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		// riferimenti numerici
		CString sl;
		pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);

		//---------------------------------
		// seconda riga a sx strip
		
		posLeft += (pDoc->c_rotoloMapping[lastElem])[i].c_larghezza;
		
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->SetTextAlign(TA_RIGHT | TA_BOTTOM);
		sl.Format("%3.0lf",posLeft/SIZE_BANDE);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);
		}
	pDC->SelectObject(oldFont);
	font.DeleteObject();
	}
	// Fine disegno pos. bobine
	//---------------------------------------------------------

}



void CRollView::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default

if (nIDEvent == c_timerId)
	{
	// redraw all area
	Invalidate(FALSE);   

    // Update Window to cause View to redraw.
	UpdateWindow();
	}

CView::OnTimer(nIDEvent);
}

BOOL CRollView::DestroyWindow() 
{
// TODO: Add your specialized code here and/or call the base class

if (c_timerId != 0)
	{
	KillTimer (c_timerId);
	c_timerId = 0;
	}
	
return CView::DestroyWindow();
}

BOOL CRollView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
// TODO: Add your specialized code here and/or call the base class

// gestione double click sui pallini delle classi
// wParam = id classe pallino 100 A, 101 B, etc
// lParam = NMHDR nmhdr struttura
// nmhdr.idFrom = c_id;
// nmhdr.code = NM_DBLCLK;

NMHDR* nmhdr=NULL;
if (lParam != NULL)
	{
	DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
	nmhdr = (NMHDR*)lParam;
	if ((nmhdr->code == NM_DBLCLK)&&
		((wParam-100) >= 0) && ((wParam-100) <= 4))
		{// doubleClick su pallino
		int i = wParam-100;
		if (layout.c_cLabel[i].c_tColor != CSM20GetClassColor(i))
			{
			layout.c_cLabel[i].c_tColor = CSM20GetClassColor(i);
			pDoc->c_targetBoard.removeExcludeDotColor(CSM20GetClassColor(i));
			}
		else
			{
			pDoc->c_targetBoard.addExcludeDotColor(CSM20GetClassColor(i));
			layout.c_cLabel[i].c_tColor = RGB(64,64,64);	
			}
		// redraw all area
		pDoc->UpdateAllViews(NULL);   
		}
	}
// return CView::OnNotify(wParam, lParam, pResult);
return TRUE;
}
