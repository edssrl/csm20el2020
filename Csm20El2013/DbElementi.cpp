// DbElementi.cpp : implementation file
//

#include "stdafx.h"
#include "DbElementi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDbElementi

IMPLEMENT_DYNAMIC(CDbElementi, CDaoRecordset)

CDbElementi::CDbElementi(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CDbElementi)
	m_ROTOLO = _T("");
	m_ELEMENTO = 0;
	m_POSIZIONE = 0;
	m_LARGHEZZA = 0.0;
	m_nFields = 4;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CDbElementi::GetDefaultDBName()
{
	return _T("C:\\User\\hole\\Csm20El2013\\DBase\\Hole.mdb");
}

CString CDbElementi::GetDefaultSQL()
{
	return _T("[TAGLI]");
}

void CDbElementi::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CDbElementi)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Text(pFX, _T("[ROTOLO]"), m_ROTOLO);
	DFX_Long(pFX, _T("[ELEMENTO]"), m_ELEMENTO);
	DFX_Long(pFX, _T("[POSIZIONE]"), m_POSIZIONE);
	DFX_Double(pFX, _T("[LARGHEZZA]"), m_LARGHEZZA);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CDbElementi diagnostics

#ifdef _DEBUG
void CDbElementi::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CDbElementi::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG

BOOL CDbElementi::openSelectNome (LPCSTR nome,int elemento,BOOL closeNotFound)
{
	// TODO: Add your specialized code here and/or call the base class

CString sqlString;
sqlString.Format("SELECT * FROM %s WHERE ROTOLO = \'%s\' AND ELEMENTO = %d ORDER BY POSIZIONE",
		GetDefaultSQL(),nome,elemento);

try
	{Open (dbOpenDynaset,(LPCSTR)sqlString);} 
catch
	( CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbElementi.Open");
	e->Delete();
	return FALSE;
	}

// Check Record Found
if (IsEOF() && closeNotFound)
	{// Selezione Tabella Default manuale
	// Close Old query and DB
	Close();
	return FALSE;
	}

return TRUE;
}


BOOL CDbElementi::appendElemento (LPCSTR rotolo,int elemento,CElemento *pElemento)
{

CString sqlString;
sqlString.Format("SELECT * FROM %s",
		GetDefaultSQL());
try
	{Open (dbOpenDynaset,sqlString);} 
catch
	( CDaoException)
	{
	AfxMessageBox ("Error opening RecordSet");
	return FALSE;
	}

// Perform Copy Record
if (CanAppend())
	{
	for (int i=0;i<pElemento->size();i++)
		{
		try	{AddNew();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.AddNew");
			Close();
			return FALSE;
			}
	 	// Modify the Name of Ricetta
		m_ROTOLO = rotolo;
		m_ELEMENTO = elemento;
		m_LARGHEZZA = (*pElemento)[i].c_larghezza;
		m_POSIZIONE = (*pElemento)[i].c_posizione;

		try	{Update();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.Update");
			Close();
			return FALSE;
			}
		}
	} 
else
	AfxGetMainWnd()->MessageBox ("Can't Append to dbRotoli","dbRotoli.CanAppend");

Close();

return TRUE;
}
