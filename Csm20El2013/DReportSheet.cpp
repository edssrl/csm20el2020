// ReportSheet.cpp : implementation file
//

#include "stdafx.h"
// #include "Csm20El2013.h"
#include "MainFrm.h"
#include "Dialog.h"


#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "DAlzate.h"
#include "Csm20El2013.h"

#include "DReportSheet.h"


// CDReportSheet

IMPLEMENT_DYNAMIC(CDReportSheet, CPropertySheet)

CDReportSheet::CDReportSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
c_okClose = FALSE;
}

CDReportSheet::CDReportSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
c_okClose = FALSE;
}

CDReportSheet::~CDReportSheet()
{


}


BEGIN_MESSAGE_MAP(CDReportSheet, CPropertySheet)
	ON_BN_CLICKED(IDOK, OnOKButton)  
    ON_BN_CLICKED(IDCANCEL, OnCancelButton)  
END_MESSAGE_MAP()


// CDReportSheet message handlers


BOOL CDReportSheet::OnInitDialog()
{

CWnd* pOKButton = GetDlgItem( IDOK );  
    ASSERT( pOKButton );  
    pOKButton->ShowWindow( SW_HIDE );  
  
    CWnd* pCANCELButton = GetDlgItem( IDCANCEL );  
    ASSERT( pCANCELButton );  
    pCANCELButton->ShowWindow( SW_HIDE );  

    BOOL bResult   = CPropertySheet::OnInitDialog();  

    //Get button sizes and positions  
    CRect rect, tabrect;  
    GetDlgItem( IDOK )->GetWindowRect( rect );  
    GetTabControl()->GetWindowRect( tabrect );  
  
    ScreenToClient( rect );  
    ScreenToClient( tabrect );    
  
    //Create new OK button and set standard font  
    m_OKbutton.Create( "OK",  
                        BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | WS_TABSTOP,  
                        rect,  
                        this,  
                        IDOK );  
  
    m_OKbutton.SetFont( GetFont() );  
  
    //Get CANCEL button size and position  
    GetDlgItem( IDCANCEL )->GetWindowRect( rect );  
    ScreenToClient( rect );  
  
    //Create new SAVE button (where CANCEL was) and set standard font  
    m_Cancelbutton.Create( "Cancel",  
                            BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | WS_TABSTOP,  
                            rect,  
                            this,  
                            IDCANCEL );  
  
    m_Cancelbutton.SetFont( GetFont() );      
    UpdateData( FALSE );  

	// TODO:  Add your specialized code here
	// test disable page 1
	// this->EnablePage(1,0);


	return bResult;
}

void CDReportSheet::OnOKButton()
{
// call CPropertypage::OnOk
OnClose();
// -------- 
if (c_okClose)
	// close and return idok
	EndDialog(IDOK);


}


 void CDReportSheet::OnCancelButton()
 {
 // call CPropertypage::OnCancel
 OnClose();
 // close and return idcancel
 EndDialog(IDCANCEL); 
 }

void CDReportSheet::EnablePage (int nPage, BOOL bEnable)
   {
       // if we want to enable the page
       if (bEnable)
       {
           // remove the index from the map
           m_DisabledPages.RemoveKey (nPage);
           // take out " - Disabled" from tab label
           CTabCtrl* pTab = GetTabControl();
           ASSERT (pTab);
           TC_ITEM ti;
           char szText[100];
           ti.mask = TCIF_TEXT;
           ti.pszText = szText;
           ti.cchTextMax = 100;
           VERIFY (pTab->GetItem (nPage, &ti));
           char * pFound = strstr (szText, " - Disabled");
           if (pFound)
           {
               *pFound = '\0';
               VERIFY (pTab->SetItem (nPage, &ti));
           }
       }
       // if we want to disable it
       else
       {
           // add the index to the map
           m_DisabledPages.SetAt (nPage, nPage);
           // add " - Disabled" to tab label
           CTabCtrl* pTab = GetTabControl();
           ASSERT (pTab);
           TC_ITEM ti;
           char szText[100];
           ti.mask = TCIF_TEXT;
           ti.pszText = szText;
           ti.cchTextMax = 100;
           VERIFY (pTab->GetItem (nPage, &ti));
           strcat (szText, " - Disabled");
           VERIFY (pTab->SetItem (nPage, &ti));
       }
   }


   BOOL CDReportSheet::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
   {
       NMHDR* pnmh = (NMHDR*)lParam;
       // tab is about to change
       if (TCN_SELCHANGING == pnmh->code)
           // save the current page index
           m_nLastActive = GetActiveIndex ();
       // tab has been changed
       else if (TCN_SELCHANGE == pnmh->code)
       {
           // get the current page index
           int nCurrentPage = GetActiveIndex ();
           // if current page is in our map of disabled pages
           if (m_DisabledPages.Lookup (nCurrentPage, nCurrentPage))
           // activate the previous page
           PostMessage (PSM_SETCURSEL, m_nLastActive);
       }
       return CPropertySheet::OnNotify(wParam, lParam, pResult);
   }


   