// DaoDbCreate.cpp : implementation of the CDaoDbCreate class
//
// Fix bug Apertura senza creazione 21/09/98
// Fix bug Memory leaks -> Not deleted Exceptions *e 22/04/99
// Fix Bug eliminato AfxThrowException 19/11/99
// Inserita gestione dei commenti in def file 
//	con ; in prima colonna 20-11-99

#include "StdAfx.h"

#include "CDaoDbCreate.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDaoDbCreate


//---------------------------------------------------------------------
//		
//		load
//
//---------------------------------------------------------------------

BOOL CDaoDbCreate::load (CString &fileName)
{
CFileException error;
CFile file;
BOOL retCode;

retCode = file.Open( (LPCTSTR)fileName,CFile::modeRead,&error);

if (retCode == FALSE)
	return(retCode);

// Clear Line
CString line;

int nBytes = 0;
do
	{
	line.Empty();
	char ch = 0;
	BOOL isComment = FALSE;
	while (ch != '\n')
		{
		nBytes = file.Read(&ch,1);
			if (nBytes == 0)
				break; // EOF
		if ((ch != '\n')&&
			(ch != '\r'))
			{
			if (line.IsEmpty()&&(ch == ';'))
				isComment = TRUE;
			line += ch;
			}
		}
	if (!isComment)
		c_dbDef.Add(line);
	}
while (nBytes > 0);

// Enable Creation
c_doCreate = TRUE;

return TRUE;
}


//---------------------------------------------------------------------
//
//		
//		Open
//
//	Verifica l'esistenza dei File di data base, 
//	Apre se esiste e verifica l'esistenza di tutti i field e indici
//  Non modifica indici esistenti, ne` campi esistenti
//  AGGIUNGE SOLO INDICI O CAMPI NON PRESENTI
//	Crea db se non esiste
//
//
//---------------------------------------------------------------------

BOOL CDaoDbCreate::Open (const CString& dataBaseName)
{

if (IsOpen())
	{
	if (GetName()==dataBaseName)
		return TRUE;
	else	// Open different DB
		Close();
	}

c_dbName = dataBaseName;

if (!c_dbName.IsEmpty())
	{
	try	{CDaoDatabase::Open(c_dbName);}
	catch (CDaoException *e)
		{
		AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"db.Open");
		e->Delete();
		}
	}

// Non sono riuscito ad aprire e non posso creare
if ((!IsOpen())&&(!c_doCreate))
	{
	return(FALSE);
	}

if ((IsOpen())&&(!c_doCreate))
	{
	return(TRUE);
	}

// Aperto -> devo verificare la struttura 
// Non aperto creo nuovo 

//-------------------------------------------
//
//				Create
// Parse First string in CArray
// 
//-------------------------------------------
int lineCount = 0;
CString line;


if (!getLine(lineCount ++,line))
	return FALSE;


// Controllo versione
ParseVer pVer (line,",",":");
// 
if (!pVer.extract())
	{
	AfxGetMainWnd()->MessageBox ("Versione File .def non aggiornata");
	return FALSE;
	}

if (!getLine(lineCount ++,line))
	return FALSE;

ParseDBASE pDB (line,",",":");

// 
if (!pDB.extract())
	return FALSE;

// OK parsed
// Do Not Use DB parsed name, instead the parameter passing name
// if empty parameter name use parsed
if (c_dbName.IsEmpty())
	{
	c_dbName = pDB.nome;
	if (c_dbName.Right(4) != ".mdb")
		c_dbName += ".mdb";
	}

if (!IsOpen())
	{
	// caso di dbName empty
	try	{CDaoDatabase::Open(c_dbName);}
		catch (CDaoException *e)
		{
		CString error;
		error = e->m_pErrorInfo->m_strDescription;
		//AfxGetMainWnd()->MessageBox (error,"db.Open");
        e->Delete();
		}
	}

if (!IsOpen())
	{
	try	{Create(c_dbName,pDB.language,pDB.options);}
	catch (CDaoException *e)
		{
		AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"db.Create");
		e->Delete();
		return FALSE;
		}
	}

if (!getLine(lineCount,line))
	return FALSE;

return(CreateModifyTable(lineCount,line));

}
//---------------------------------------------------------------------
//
//		
//		CreateModifyTable
//
//	Per ogni tabella presente in .def verifica esistenza field ed index
//	Crea se non esiste, aggiorna se esitente
//	Non elimina field, index, o table. Aggiunge solo
//
//---------------------------------------------------------------------

BOOL CDaoDbCreate::CreateModifyTable (int lineCount,CString &line )
{

ParseTABLE pTB(line,",",":");

BOOL tableCreated;
while (pTB.extract())
	{
	tableCreated = FALSE;
	lineCount ++;
	CDaoTableDef table (this);
	// Try Open
	try {table.Open(pTB.nome);}
	catch (CDaoException *e0)
		{// Not exist Create
		if (e0->m_scode == E_DAO_VtoNameNotFound)
			{
			if (pTB.tbType == EXTERN_TB)
				{
				try {table.Create(pTB.nome,dbAttachExclusive,pTB.extAttr0,pTB.extAttr1);}
				//try {table.Create(pTB.nome,dbAttachExclusive);}
				catch (CDaoException *e1)
					{
					AfxGetMainWnd()->MessageBox (e1->m_pErrorInfo->m_strDescription,"table.Create");
			        e1->Delete();
					return FALSE;
					}
				//table.SetConnect(pTB.extAttr1);
				//table.SetSourceTableName(pTB.extAttr0);
				}
			if (pTB.tbType == INTERN_TB)
				{
				try {table.Create(pTB.nome);}
				catch (CDaoException *e1)
					{
					AfxGetMainWnd()->MessageBox (e1->m_pErrorInfo->m_strDescription,"table.Create");
					e1->Delete();
					return FALSE;
					}
				}
			tableCreated = TRUE;
			}
		else
			{
			AfxGetMainWnd()->MessageBox (e0->m_pErrorInfo->m_strDescription,"table.Create");
			return FALSE;
			}
		e0->Delete();
		}

	if (pTB.tbType == INTERN_TB)
		{
		//-------------------------
		// Field
		//-------------------------
		if (!getLine(lineCount,line))
			break;
		ParseFIELD pFD (line,",",":");

		while (pFD.extract())
			{
			lineCount ++;
			if (!existField (table,CString(pFD.fieldInfo.m_strName)))
				{
				try {table.CreateField(pFD.fieldInfo);}
				catch (CDaoException *e1)
					{
					CString s ("table.Create : ");
					s += pFD.fieldInfo.m_strName;
					AfxGetMainWnd()->MessageBox (e1->m_pErrorInfo->m_strDescription,"table.CreateField");
					e1->Delete();
					return FALSE;
					}
				}
			if (getLine(lineCount,line))
				pFD.update (line);
			else
				break;
			}
		//----------------------------------------
		// Index
		// BackTrace
		//----------------------------------------
		if (getLine(lineCount,line))
			{
			ParseINDEX pID (line,",",":");
			while (pID.extract())
				{
				lineCount ++;
				// IndexInfo
				if (!getLine(lineCount,line))
					break;
				ParseFIELDINDEX pIF (line,",",":");
				while (pIF.extract())
					{
					lineCount ++;
					pID.add (pIF.indexFieldInfo);
					if (!getLine(lineCount,line))
						break;
					pIF.update (line);
					}
				if (!existIndex (table,CString(pID.indexInfo.m_strName)))
					{
					try{table.CreateIndex (pID.indexInfo);}
					catch (CDaoException *e1)
						{
						AfxGetMainWnd()->MessageBox (e1->m_pErrorInfo->m_strDescription,"table.CreateIndex");
					    e1->Delete();
						return FALSE;
						}
					}
				else
					{// index exist		

					}
				if (!getLine(lineCount,line))
					break;
				pID.update (line);
				}
			}
		}
	if (tableCreated)
		{
		try{table.Append();}
		catch (CDaoException *e1)
			{
			AfxGetMainWnd()->MessageBox (e1->m_pErrorInfo->m_strDescription,"table.Append");
			e1->Delete();
			return FALSE;
			}
		}
	else
		{
		try{table.Close();}
		catch (CDaoException *e1)
			{
			AfxGetMainWnd()->MessageBox (e1->m_pErrorInfo->m_strDescription,"table.Append");
			e1->Delete();
			return FALSE;
			}
		}

	if (!getLine(lineCount,line))
		break;
	pTB.update (line);
	}

return TRUE;
}

BOOL CDaoDbCreate::existField (CDaoTableDef &table,CString &nome)
{

int numField;

try {numField = (int)table.GetFieldCount();}
catch (CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"table.Append");
	e->Delete();
	return FALSE;
	}

CDaoFieldInfo fI;
for (int i=0;i<numField;i++)
	{
	try {table.GetFieldInfo(i,fI);}
	catch (CDaoException *e)
		{
		AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"table.Append");
		e->Delete();
		return FALSE;
		}

	if (CString  (fI.m_strName) == nome)
		return TRUE;
	}
return FALSE;
}

BOOL CDaoDbCreate::existIndex (CDaoTableDef &table,CString &nome)
{

int numIndex;

try {numIndex = (int)table.GetIndexCount();}
catch (CDaoException *e)
	{
	AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"table.Append");
	e->Delete();
	return FALSE;
	}

CDaoIndexInfo fI;
for (int i=0;i<numIndex;i++)
	{
	try {table.GetIndexInfo(i,fI);}
	catch (CDaoException *e)
		{
		AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"table.Append");
        e->Delete();
		return FALSE;
		}

	if (CString  (fI.m_strName) == nome)
		return TRUE;
	}
return FALSE;
}

//------------------------------------------------------
//
//
//					parseField 
//
// Legge field identificato da num
//	line: stringa completa da analizzare 
//  num : numero del campo
//  field : valore di ritorno
//------------------------------------------------------


BOOL Parser::getFieldNum (int num,CString &field)
{

CString localLine;
localLine = line;

// Skip Key
field = localLine.SpanExcluding( (LPCTSTR)keySeparator);
localLine = localLine.Right(localLine.GetLength() - (field.GetLength()+1));

for (int i=0;i<num;i++)
	{
	if (localLine.IsEmpty()) return FALSE;
	field = localLine.SpanExcluding( (LPCTSTR)fieldSeparator);
	localLine = localLine.Right(localLine.GetLength() - (field.GetLength()+1));
	}
 return(TRUE);
 }

BOOL Parser::getFieldNum (int num,int &field)
{
CString f;

if (!getFieldNum(num,f))
	return FALSE;

sscanf ((LPCSTR)f,"%d",&field);

return TRUE;
}

BOOL Parser::getFieldNumBool (int num,BOOL &field)
{
CString f;

if (!getFieldNum(num,f))
	return FALSE;

if ((f == "TRUE")||(f == "True"))
	field = TRUE;
else
	field = FALSE;
	
return TRUE;
}

//------------------------------------------------------
//
//					parseKey 
//
// Legge chiave della stringa 
//	line: stringa completa da analizzare 
//  key : valore di ritorno
//------------------------------------------------------


BOOL Parser::getKey (CString &key)
{

CString localLine;
localLine = line;

if (localLine.IsEmpty()) return FALSE;

// Skip Key
key = localLine.SpanExcluding( (LPCTSTR)keySeparator);

if (key.IsEmpty()) return FALSE;


return(TRUE);
}


//------------------------------------------------------
//
//					parseDBASE 
//
//  Da stringa compila struttura DBaseDef 
//	 
//  
//------------------------------------------------------

ParseDBASE::ParseDBASE (CString &s,LPCSTR fs,LPCSTR ks)
						: Parser (s,fs,ks)
{
}


BOOL ParseDBASE::extract (void)
{

CString key;
CString f;

if (!getKey(key))
	return FALSE;

if (key != "DBASE")
	return FALSE;

if (!getFieldNum (1,nome))
	return FALSE;

if (!getFieldNum (2,f))
	return FALSE;

if (f != "dbLangGeneral")
	return FALSE;

language = dbLangGeneral;	// Global string ?defined???

options = 0;
int i = 3;

while (getFieldNum(i,f))
	{
	if (f == "dbEncrypt")
		options |= dbEncrypt;
	if (f == "dbVersion10")
		options |= dbVersion10;
	if (f == "dbVersion20")
		options |= dbVersion20;
	if (f == "dbVersion30")
		options |= dbVersion30;
	i++;
	}

if (i > 3)
	return TRUE;	
else
	return FALSE;
}


//------------------------------------------------------
//
//					parseTABLE 
//
//  Da stringa compila struttura DBaseDef 
//	 
//  
//------------------------------------------------------

ParseTABLE::ParseTABLE (CString &s,LPCSTR fs,LPCSTR ks)
						: Parser (s,fs,ks)
{
	tbType = NONE_TB;
}


BOOL ParseTABLE::extract (void)
{

CString key;
CString f;

if (!getKey(key))
	return FALSE;

if (key == "TABLE")
	tbType = INTERN_TB;

if (!getFieldNum (1,nome))
	return FALSE;

if (key == "EXT_TABLE")
	{
	tbType = EXTERN_TB;
	if (!getFieldNum (2,extAttr0))
		return FALSE;
	if (!getFieldNum (3,extAttr1))
		return FALSE;
	}


if (tbType == NONE_TB)
	return FALSE;


return TRUE;	
}

//------------------------------------------------------
//
//					parseFIELD::extract 
//
//  Da stringa compila struttura CDaoFieldInfo
//	 
//  
//------------------------------------------------------

ParseFIELD::ParseFIELD (CString &s,LPCSTR fs,LPCSTR ks)
						: Parser (s,fs,ks)
{
}


BOOL ParseFIELD::extract (void)
{

CString key;
CString f;

if (!getKey(key))
	return FALSE;

if (key != "FIELD")
	return FALSE;

// nome
if (!getFieldNum (1,fieldInfo.m_strName))
	return FALSE;

// tipo
if (!getFieldNum (2,f))
	return FALSE;

short type = 0;

if (f == "dbBoolean")
	type = dbBoolean;
if (f == "dbByte")
	type = dbByte;
if (f == "dbInteger")
	type = dbInteger;
if (f == "dbLong")
	type = dbLong;
if (f == "dbCurrency")
	type = dbCurrency;
if (f == "dbSingle")
	type = dbSingle;
if (f == "dbDouble")
	type = dbDouble;
if (f == "dbDate")
	type = dbDate;
if (f == "dbText")
	type = dbText;
if (f == "dbLongBinary")
	type = dbLongBinary;
if (f == "dbMemo")
	type = dbMemo;
if (f == "dbGUID")
	type = dbGUID;

fieldInfo.m_nType = type;

// size
int size;
if (!getFieldNum (3,size))
	return FALSE;
fieldInfo.m_lSize = size;

// Allow Zero Length
BOOL zeroL;
if (!getFieldNumBool (4,zeroL))
	return FALSE;

fieldInfo.m_bAllowZeroLength = zeroL;


//	Default Value
if (!getFieldNum (5,f))
	return FALSE;
if (f == "GenUniqueID")
	{
	fieldInfo.m_strDefaultValue =  "GenUniqueID( )";
	}
else
	fieldInfo.m_strDefaultValue = f;
	

int i = 6;
fieldInfo.m_lAttributes = 0;
while (getFieldNum(i,f))
	{
	if (f == "dbAutoIncrField")
		fieldInfo.m_lAttributes |= dbAutoIncrField;
	if (f == "dbDescending")
		fieldInfo.m_lAttributes |= dbDescending;
	if (f == "dbFixedField")
		fieldInfo.m_lAttributes |= dbFixedField;
	if (f == "dbAutoIncrField")
		fieldInfo.m_lAttributes |= dbAutoIncrField;
	if (f == "dbVariableField")
		fieldInfo.m_lAttributes |= dbVariableField;
	if (f == "dbUpdatableField")
		fieldInfo.m_lAttributes |= dbUpdatableField;
	i++;
	}

//	Default Value

fieldInfo.m_nOrdinalPosition = 0;    // Secondary
fieldInfo.m_bRequired = FALSE;            // Secondary
fieldInfo.m_lCollatingOrder = 0;      // Secondary
fieldInfo.m_strForeignName.Empty();    // Secondary
fieldInfo.m_strSourceField.Empty();    // Secondary
fieldInfo.m_strSourceTable.Empty();    // Secondary
fieldInfo.m_strValidationRule.Empty(); // All
fieldInfo.m_strValidationText.Empty(); // All

if (i > 5)
	return TRUE;	
else
	return FALSE;
}

//------------------------------------------------------
//
//					parseINDEX::extract 
//
//  Da stringa compila struttura CDaoIndexInfo
//	 
//  
//------------------------------------------------------

ParseINDEX::ParseINDEX (CString &s,LPCSTR fs,LPCSTR ks)
						: Parser (s,fs,ks)
{
indexInfo.m_pFieldInfos = NULL;
// indice di 
iCount = 0;
}

ParseINDEX::~ParseINDEX(void)
{
if (indexInfo.m_pFieldInfos != NULL)
	delete [] indexInfo.m_pFieldInfos;
}


BOOL ParseINDEX::extract (void)
{

CString key;
CString f;

if (!getKey(key))
	return FALSE;

if (key != "INDEX")
	return FALSE;

// nome
if (!getFieldNum (1,indexInfo.m_strName))
	return FALSE;

// numField
int numField;
if (!getFieldNum (2,numField))
	return FALSE;

indexInfo.m_nFields = numField;

// Allocate space
if (indexInfo.m_pFieldInfos != NULL)
	delete [] indexInfo.m_pFieldInfos;
	
indexInfo.m_pFieldInfos = new CDaoIndexFieldInfo [numField];

if (indexInfo.m_pFieldInfos == NULL)
	return FALSE;
iCount = 0;

// Primary
BOOL primary;
if (!getFieldNumBool (3,primary))
	return FALSE;

indexInfo.m_bPrimary = primary;

BOOL unique;
if (!getFieldNumBool (4,unique))
	return FALSE;

indexInfo.m_bUnique = unique;

return TRUE;
}


BOOL ParseINDEX::add (CDaoIndexFieldInfo &iFInfo)
{
if (indexInfo.m_pFieldInfos == NULL)
	return FALSE;

if (iCount >= indexInfo.m_nFields)
	return FALSE;

indexInfo.m_pFieldInfos[iCount ++] = iFInfo;

return TRUE;
}


//------------------------------------------------------
//
//					parseFIELDINDEX::extract 
//
//  Da stringa compila struttura CDaoIndexInfo
//	 
//  
//------------------------------------------------------

ParseFIELDINDEX::ParseFIELDINDEX (CString &s,LPCSTR fs,LPCSTR ks)
						: Parser (s,fs,ks)
{
}

BOOL ParseFIELDINDEX::extract (void)
{

CString key;
CString f;

if (!getKey(key))
	return FALSE;

if (key != "FIELDINDEX")
	return FALSE;
// nome
if (!getFieldNum (1,indexFieldInfo.m_strName))
	return FALSE;



// Descending
BOOL descending;
if (!getFieldNumBool (2,descending))
	return FALSE;

indexFieldInfo.m_bDescending = descending;

return TRUE;

}



//------------------------------------------------------
//
//					parseVer  
//
//  Da stringa compila struttura controllo versione e data .def 
//	 
//  
//------------------------------------------------------

ParseVer::ParseVer (CString &s,LPCSTR fs,LPCSTR ks)
						: Parser (s,fs,ks)
{
}


BOOL ParseVer::extract (void)
{

CString key;
CString f;

if (!getKey(key))
	return FALSE;

if (key != "VERSIONE")
	return FALSE;

if (!getFieldNum (1,versione))
	return FALSE;

if (versione != CDDBVERSIONE)
	return FALSE;

if (!getFieldNum (2,data))
	return FALSE;

return TRUE;
}
