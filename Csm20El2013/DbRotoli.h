#if !defined(AFX_DBROTOLI_H__C05C92A1_3045_11D6_AB54_00C026A019B7__INCLUDED_)
#define AFX_DBROTOLI_H__C05C92A1_3045_11D6_AB54_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbRotoli.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDbRotoli DAO recordset

class CDbRotoli : public CDaoRecordset
{
public:
	CDbRotoli(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CDbRotoli)

//---------
	BOOL openSelectNome (LPCSTR nome,LPCSTR impianto,BOOL closeNotFound);
	BOOL appendRotolo (CDbRotoli *pRotolo);


// Field/Param Data
	//{{AFX_FIELD(CDbRotoli, CDaoRecordset)
	CString	m_NOME;
	long	m_RILAVORAZIONE;
	CString	m_BOLLA;
	COleDateTime	m_DATA_LAVORAZIONE;
	CString	m_IMPIANTO;
	double	m_LARGHEZZA;
	CString	m_LEGA;
	double	m_SPESSORE;
	CString	m_STATO;
	CString	m_LAVORAZIONE;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDbRotoli)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBROTOLI_H__C05C92A1_3045_11D6_AB54_00C026A019B7__INCLUDED_)
