//-----------------------------------------------------------------
//
//				 Profile.cpp 
//
//	  Gestione profileString ed int
//-----------------------------------------------------------------

#include "stdafx.h"

// Bool Value

BOOL CProfile::getProfileBool (LPCSTR s1, LPCSTR s2, BOOL def)
{

strValue = pApp->GetProfileString(_T(s1), _T(s2), 
				_T("NODATA"));

if (strValue == "NODATA")
	{
	if (def)
		strValue = "TRUE";
	else
		strValue = "FALSE";
 
	pApp->WriteProfileString(_T(s1), _T(s2), 
				_T(strValue));
 	}

if ((strValue == "TRUE")
	||
	(strValue == "ON"))
	return TRUE;
else
	return FALSE;

}

// CString *Value
CString &CProfile::getProfileString (LPCSTR s1, LPCSTR s2, LPCSTR def)
{

strValue = pApp->GetProfileString(_T(s1), _T(s2), 
				_T("NODATA"));

if (strValue == "NODATA")
	{
 
	pApp->WriteProfileString(_T(s1), _T(s2), 
				_T(def));
 	
	strValue = def;
	}

return strValue;
}


// CString *Value
int CProfile::getProfileInt (LPCSTR s1, LPCSTR s2, int def)
{
int iVal;

iVal = pApp->GetProfileInt(_T(s1), _T(s2), 
				32767);

if (iVal == 32767)
	{
 
	pApp->WriteProfileInt(_T(s1), _T(s2), 
				def);
 	
	iVal = def;
	}

return iVal;
}

// delete Value
BOOL CProfile::deleteProfile (LPCSTR s1, LPCSTR s2)
{
 
return(pApp->WriteProfileString(_T(s1), _T(s2),NULL));
 	
}


// Write Value
BOOL CProfile::writeProfileString (LPCSTR s1, LPCSTR s2,LPCSTR s3)
{
 
return(pApp->WriteProfileString(_T(s1), _T(s2),_T(s3)));
 	
}
