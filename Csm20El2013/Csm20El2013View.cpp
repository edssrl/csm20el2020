// Csm20El2013View.cpp : implementation of the Csm20El2013View class
//

#include "stdafx.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"

#include "Csm20El2013Doc.h"
#include "Csm20El2013View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013View

IMPLEMENT_DYNCREATE(Csm20El2013View, CView)

BEGIN_MESSAGE_MAP(Csm20El2013View, CView)
	//{{AFX_MSG_MAP(Csm20El2013View)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013View construction/destruction

Csm20El2013View::Csm20El2013View()
{
	// TODO: add construction code here

}

Csm20El2013View::~Csm20El2013View()
{
}

BOOL Csm20El2013View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013View drawing

void Csm20El2013View::OnDraw(CDC* pDC)
{
	Csm20El2013Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code for native data here
}

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013View diagnostics

#ifdef _DEBUG
void Csm20El2013View::AssertValid() const
{
	CView::AssertValid();
}

void Csm20El2013View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

Csm20El2013Doc* Csm20El2013View::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(Csm20El2013Doc)));
	return (Csm20El2013Doc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Csm20El2013View message handlers
