#if !defined(AFX_DALZATE_H__A1974951_31A0_11D6_AB56_00C026A019B7__INCLUDED_)
#define AFX_DALZATE_H__A1974951_31A0_11D6_AB56_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DAlzate.h : header file
//
#define ALZATA_NUOVA    0
#define ALZATA_SCARTO   1
#define ALZATA_CONTINUA 2
#define ALZATA_ELEMENTO 3

/////////////////////////////////////////////////////////////////////////////
// CDAlzate dialog

class CDAlzate : public CDialog
{
// Construction
public:
	int c_retCode;
	CLineDoc*	c_pDocS;
	CLineDoc*	c_pDocZ;

	CDAlzate(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDAlzate)
	enum { IDD = IDD_ALZATE };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDAlzate)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDAlzate)
	afx_msg void OnAlzataContinua();
	afx_msg void OnAlzataNuova();
	afx_msg void OnAlzataScarto();
	afx_msg void OnAlzataDettagli();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DALZATE_H__A1974951_31A0_11D6_AB56_00C026A019B7__INCLUDED_)
