// DAlzate.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GraphDoc.h"

#include "DAlzate.h"
#include "DAlzatadettagli.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDAlzate dialog


CDAlzate::CDAlzate(CWnd* pParent /*=NULL*/)
	: CDialog(CDAlzate::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDAlzate)
	//}}AFX_DATA_INIT

c_pDocS = NULL;
c_pDocZ = NULL;
}


void CDAlzate::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDAlzate)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDAlzate, CDialog)
	//{{AFX_MSG_MAP(CDAlzate)
	ON_BN_CLICKED(IDC_ALZATA_CONTINUA, OnAlzataContinua)
	ON_BN_CLICKED(IDC_ALZATA_NUOVA, OnAlzataNuova)
	ON_BN_CLICKED(IDC_ALZATA_SCARTO, OnAlzataScarto)
	ON_BN_CLICKED(IDC_ALZATA_DETTAGLI, OnAlzataDettagli)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDAlzate message handlers

void CDAlzate::OnAlzataContinua() 
{
// TODO: Add your control notification handler code here
EndDialog(ALZATA_CONTINUA);
}

void CDAlzate::OnAlzataNuova() 
{
// TODO: Add your control notification handler code here
EndDialog(ALZATA_NUOVA);
}

void CDAlzate::OnAlzataScarto() 
{
	// TODO: Add your control notification handler code here
EndDialog(ALZATA_SCARTO);
}


void CDAlzate::OnAlzataDettagli() 
{
// TODO: Add your control notification handler code here

CDAlzataDettagli dialog;
dialog.c_pDocS = c_pDocS;
dialog.c_pDocZ = c_pDocZ;
dialog.DoModal();
	


}
