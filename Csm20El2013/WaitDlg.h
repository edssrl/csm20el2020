//***************************************************************************
//
// WaitDlg.h
//
//***************************************************************************

#ifndef CWAIT_DLG	
#define CWAIT_DLG

class CWaitDialog : public CDialog
{
protected:
BOOL* m_pFlag;

public:
CWaitDialog (BOOL*, LPCTSTR pszCaption = NULL, LPCTSTR pszText = NULL);
virtual ~CWaitDialog ();
virtual void OnCancel ();



static BOOL Pump (void);
BOOL goOn(void){if (m_pFlag != NULL)return(*m_pFlag);
		return TRUE;};
// simula il funzionamento modal
// testando continuamente *m_pFlag
void modal (int secToWait=-1)
	{if (secToWait == 0)
		return;
	if (secToWait > 0)
		{secToWait *= 1000; // msec
		while ((*m_pFlag)&&(secToWait > 0))
			{Pump();Sleep(100);secToWait -=100;}
		}		
	else /* all'infinito */
		while (*m_pFlag) Pump();
	};

void SetPercentComplete (int);
void SetMessageText (LPCTSTR);
void Close ();

void HideCancel (BOOL v);
void HideProgress (BOOL v);
void SetMsg1 (LPCTSTR pszText);
void SetMsg2 (LPCTSTR pszText);
void SetMsg3 (LPCTSTR pszText);
void SetMsg4 (LPCTSTR pszText);
void SetPulsText (LPCTSTR pszText);
void GetPulsText (CString &text);

};

#endif