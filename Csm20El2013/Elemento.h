// Elemento.h: interface for the CElemento class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ELEMENTO_H__7AA99B11_2D1B_11D6_AB51_00C026A019B7__INCLUDED_)
#define AFX_ELEMENTO_H__7AA99B11_2D1B_11D6_AB51_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <vector> 
using namespace std;

#include "CFileBpe.h"

class CBobina
	{
	public:
		BOOL load(CFileBpe* cf,long ver);
		BOOL save(CFileBpe *cf);
	double c_larghezza;
	double c_xPos;
	int c_posizione;
	CBobina (void);
	virtual ~CBobina();
	CBobina& operator=(const CBobina& source)
		{c_larghezza = source.c_larghezza;c_posizione=source.c_posizione;c_xPos=source.c_xPos;
	return (*this);};
	};

class CAlzata
	{
	public:
		BOOL load(CFileBpe* cf,long ver);
		BOOL save(CFileBpe* cf);
	double c_yPos;
	int c_tipo;
	CAlzata (void);
	virtual ~CAlzata(){;};
	CAlzata& operator=(const CAlzata& source)
		{c_yPos = source.c_yPos;c_tipo=source.c_tipo;
	return (*this);};
	};

class CElemento  : public vector<CBobina,allocator <CBobina> >
{
// posizione assoluta relativa al rotolo 
// delle alzate nell'elemento
// la prima viene immessa alla creazione dell'elemento
// l'ultima alla chiusura
public:
	int c_firstRising;
	
	BOOL load(CFileBpe *cf,long ver);
	BOOL save(CFileBpe *cf);
	void appendAlzata(double yPos,int tipo);
	void modifyCodeLastAlzata(int tipo);
	void removeLastAlzata();
	void assignBobine(CElemento& source);

	void reset(void);
	// torna numero di alzate
	// esclusi gli scarti
	int getNumberOfAlz(int upTo);
	// torna numero di alzata attuale 
	// esclusi gli scarti
	int getCurAlzNumber(int upTo);
	// torna indice di alzata attuale 
	int getCurAlzIndex(int upTo);

vector<CAlzata, allocator <CAlzata> > c_alzate;
	CElemento();
	virtual ~CElemento();
	CElemento& operator=(const CElemento& source)
		{assign(source.begin(),source.end());
		c_alzate.assign(source.c_alzate.begin(),
			source.c_alzate.end());
		return (*this);};
	BOOL operator==(const CElemento& source)
		{// se hanno stesso numero di strip e strip in stessa posizione
		BOOL equal = TRUE;
		// different size = different element
		if (size()!=source.size())
			return FALSE; 
		for (int i=0;i<size();i++)
			{
			if ((*this)[i].c_xPos != source[i].c_xPos)
				return FALSE;
			if ((*this)[i].c_larghezza != source[i].c_larghezza)
				return FALSE;			
			}
		return TRUE;}; 	

		// torna posizione y dell'alzata il cui indice = alzIndex
	double	getYPos(int alzIndex)
	{int alzSize = c_alzate.size();
	if (alzIndex >= alzSize)
		return -1.;
	if (alzIndex < 0)
		return c_alzate[alzSize-1].c_yPos;
	else
		return c_alzate[alzIndex].c_yPos;
	};


};


class CRotolo  : public vector<CElemento, allocator <CElemento> >
{

public:
	BOOL load(CFileBpe* cf,long ver);
	BOOL save(CFileBpe *cf);
	void reset(void);
	int c_lastElemento;
// Eliminato 02-05-08 
//	int c_reverseBobinePosition;
	CRotolo();
	virtual ~CRotolo();
	CRotolo& operator=(const CRotolo& source)
		{assign(source.begin(),source.end());
	return (*this);};
	// calcola posizione assoluta del rising
	int getGlobalRisingId(int elemento,int rising);

};


#endif // !defined(AFX_ELEMENTO_H__7AA99B11_2D1B_11D6_AB51_00C026A019B7__INCLUDED_)
