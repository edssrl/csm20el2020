// Dialog.h : header file
//

#include "FontInfo.h"


/////////////////////////////////////////////////////////////////////////////
// CDOpzioni1 dialog

#ifndef Csm20El2013IALOG
#define Csm20El2013IALOG

#include "resource.h"
#include "DAllarmi.h"

class CDOpzioni1 : public CDialog
{
// Construction
public:
	CDOpzioni1(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDOpzioni1)
	enum { IDD = IDD_OPZIONI1 };
	double	m_Lunghezza1;
	double	m_Scala1;
	double	m_Scala2;
	BOOL	m_Allarme;
	int		m_AllarmeDurata;
	BOOL	m_stampaCoilAutomatica;
	BOOL	m_stampaStripAutomatica;
	BOOL	m_useHwFinger;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDOpzioni1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
double c_minLunghezza,c_maxLunghezza;

	// Generated message map functions
	//{{AFX_MSG(CDOpzioni1)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CDParametri dialog

class CDParametri : public CDialog
{
// Construction
public:
	CDParametri(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDParametri)
	enum { IDD = IDD_PARAMETRI1 };
	double	m_LrStop;
	double	m_FCDL;
	long	m_alarmPulse;
	CString	m_strAlPulse;
	double	m_diaEncoder;
	double	m_oddEvenDistance;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDParametri)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDParametri)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	long c_maxValAlPulse;
	long c_minValAlPulse;
};
/////////////////////////////////////////////////////////////////////////////
// CDRicette dialog

class CDRicette : public CDialog
{
	BOOL allReadOnly;
	BOOL nameReadOnly;

	int c_sMinA,c_sMaxA;
	int c_sMinB,c_sMaxB;
	int c_sMinC,c_sMaxC;
	int c_sMinD,c_sMaxD;

	// Construction
public:
	void setNumClassi(int nc);
	void hideSoglie(void);
	int c_numClassi;
	CDRicette(CWnd* pParent = NULL);   // standard constructor
	void ReadOnly ();
	BOOL SetAllReadOnly (BOOL rOnly = TRUE) 
			{allReadOnly = rOnly;return(rOnly);};
	BOOL SetNameReadOnly (BOOL rOnly = TRUE) 
			{nameReadOnly = rOnly;return(rOnly);};

// Dialog Data
	//{{AFX_DATA(CDRicette)
	enum { IDD = IDD_RICETTE };
	CEdit	m_ctrlSogliaPer;
	CEdit	m_ctrlDensityLength;
	CStatic	m_textSogliaB1;
	CStatic	m_textSogliaC1;
	CStatic	m_textSogliaD1;
	CStatic	m_textSogliaA1;
	CEdit	m_ctrlSogliaD1;
	CEdit	m_ctrlSogliaC1;
	CEdit	m_ctrlSogliaB1;
	CEdit	m_ctrlSogliaA1;
	CStatic	m_textSogliaD2;
	CStatic	m_textSogliaC2;
	CStatic	m_textSogliaB2;
	CStatic	m_textSogliaA2;
	CEdit	m_EditNome;
	CEdit	m_EditSogliaD2;
	CEdit	m_EditSogliaC2;
	CEdit	m_EditSogliaB2;
	CEdit	m_EditSogliaA2;
	int		m_SogliaA2;
	int		m_SogliaB2;
	int		m_SogliaC2;
	int		m_SogliaD2;
	CString	m_Nome;
	int		m_SogliaA1;
	int		m_SogliaB1;
	int		m_SogliaC1;
	int		m_SogliaD1;
	int		m_densityLength;
	int		m_sogliaPer;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDRicette)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDRicette)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CDCopiaRicette dialog

class CDCopiaRicette : public CDialog
{
// Construction
public:
	CDCopiaRicette(CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CDCopiaRicette)
	enum { IDD = IDD_COPY_RICETTE };
	CComboBox	m_ComboSel;
	CString	m_NewRicetta;
	CString	m_ComboString;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDCopiaRicette)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDCopiaRicette)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CDSelRicette dialog

class CDSelRicette : public CDialog
{
// Construction
public:
	CDSelRicette(CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CDSelRicette)
	enum { IDD = IDD_SEL_RICETTE };
	CComboBox	m_ComboSel;
	CString	m_EditSel;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDSelRicette) 
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDSelRicette)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CDSelComPort dialog

class CDSelComPort : public CDialog
{
// Construction
public:
	CDSelComPort(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDSelComPort)
	enum { IDD = IDD_SEL_COMPORT };
	CString	m_SelComPort;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDSelComPort)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDSelComPort)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CDSoglie dialog
 
class CDSoglie : public CDialog
{
// Construction
public:
	CDSoglie(CWnd* pParent = NULL);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CDSoglie)
	enum { IDD = IDD_SOGLIE };
	CStatic	m_cStrSogliaD;
	CStatic	m_cStrSogliaC;
	CStatic	m_cStrSogliaB;
	CStatic	m_cStrSogliaA;
	CEdit	m_cSogliaD;
	CEdit	m_cSogliaC;
	CEdit	m_cSogliaB;
	CEdit	m_cSogliaA;
	int		m_SogliaA;
	int		m_SogliaB;
	int		m_SogliaC;
	int		m_SogliaD;
	CString	m_strSogliaA;
	CString	m_strSogliaB;
	CString	m_strSogliaC;
	CString	m_strSogliaD;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDSoglie)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	int c_sMinA,c_sMaxA;
	int c_sMinB,c_sMaxB;
	int c_sMinC,c_sMaxC;
	int c_sMinD,c_sMaxD;
	int c_nClassi;	// Numero classi 

	// Generated message map functions
	//{{AFX_MSG(CDSoglie)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CDAttesaStampa dialog

class CDAttesaStampa : public CDialog
{
// Construction
public:
	CDAttesaStampa(CWnd* pParent = NULL);   // standard constructor
	void centerWindow(void );

// Dialog Data
	//{{AFX_DATA(CDAttesaStampa)
	enum { IDD = IDD_ATTESA_STAMPA };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDAttesaStampa)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDAttesaStampa)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif
