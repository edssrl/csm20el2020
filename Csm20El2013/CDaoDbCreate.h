// DaoDbCreate.h : header file for the CDaoDbCreate class
// It Create a complete Dao Data Base from a file DB description
// The file is parsed with this grammar
// Capital letter are reserved words
//------------------------------------
// aggiunta gestione tabelle esterne
//------------------------------------------
// aggiunto default value in field rows

/*-----------------------------------------------

---------------------------------------
<;> comment line
<DBASE>:<name>,<language>,<d_option> [,<d_option>...]
<EXT_TABLE>:<name>,<specifier>,<specifier>
<TABLE>:<name>
<FIELD>:<name>,<type>,<size>,<allow_zero_l>,<defValue>,<field_attribute>[<field_attribute>...]
.
.
.
<INDEX>:<name>,<num_item>,<primary>,<unique>
<FIELDINDEX>:<name>,<descending>
.
.
.
.
<TABLE>:<....
<FIELD>:
..
..
..
<INDEX>:
<FIELDINDEX>
..
..

----------

<name> = <string>
<path> = <string>
<type> = <string> : {dbBoolean, dbLong, dbByte,dbInteger,dbCurrency,dbSingle,dbDate,dbDouble,dbLongBinary,dbMemo,dbGUID}
<size> = <number>
<allow_zero_len> = <bool> {TRUE, FALSE}
<defValue> = <GenUniqueID> | <string>
<field_attribute> = <string> : {dbAutoIncr, ..}
<num_item> = <number>
<primary> = <bool> : {TRUE,FALSE}
<unique> = <bool> : {TRUE,FALSE}
<descending> = <bool> : {TRUE,FALSE}
<language> = <string> : {dbLangGeneral,....}
<d_option> = <string> : {dbEncrypt,dbVersion10,...,dbVersion30}
<specifier> = <string> : {DATABASE=dBASE III;drive:\\path}


-------------------
The number of <FIELDINDEX> rows must match the <num_item> field 
in the <INDEX>.

-----------------------------------------*/
/*-----------------------------------------
1998

MODIFICABILITA` automatica del dbase

Aggiungendo una nuova tabella completa di fields e indici questa viene
automaticamente inserita nel database esistente
Aggiungendo field ad una tabella esistente questi vengono automaticamente 
inseriti nel database
Aggiungendo indici ad una tabella esistente questi vengono automaticamente 
inseriti nel database
*/

#include "StdAfx.h"

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

// #include <afxtempl.h> 


// #include "resource.h"       // main symbols

#define CDDBVERSIONE "01.02"

////////////////////////////////////////////////////////////////////////////
// CDaoDbCreate:
// See DaoDbCreate.cpp for the implementation of this class
//
//#pragma once
#ifndef CDAODBCREATE
#define CDAODBCREATE

class Parser
{
public:
CString fieldSeparator;
CString keySeparator;
CString line;
Parser (CString &s,LPCSTR fs,LPCSTR ks) 
	{line = s;fieldSeparator = fs;keySeparator = ks;};
void update (CString &s){line = s;};
BOOL getFieldNum (int num,CString &field);
BOOL getFieldNum (int num,int &field);
BOOL getFieldNumBool (int num,BOOL &field);
BOOL getKey (CString &key);
};

class ParseVer : public Parser
{
public:
// Operation
ParseVer (CString &s,LPCSTR fs,LPCSTR ks);
CString versione;
CString data;
BOOL extract (void);
// attribute
};


class ParseDBASE : public Parser
{
public:
// Operation
ParseDBASE (CString &s,LPCSTR fs,LPCSTR ks);
CString nome;
CString language;
int options;
BOOL extract (void);
// attribute
};

enum TableType {NONE_TB,INTERN_TB,EXTERN_TB};

class ParseTABLE : public Parser
{
public:
TableType tbType;
CString nome;
CString extAttr0;
CString extAttr1;

ParseTABLE (CString &s,LPCSTR fs,LPCSTR ks);
BOOL extract (void);
};



class ParseFIELD : public Parser
{
public:

CDaoFieldInfo fieldInfo;
ParseFIELD (CString &s,LPCSTR fs,LPCSTR ks);
BOOL extract (void);
};


class ParseINDEX : public Parser
{
public:
int iCount;
CDaoIndexInfo indexInfo;
ParseINDEX (CString &s,LPCSTR fs,LPCSTR ks);
~ParseINDEX(void);
BOOL extract (void);
BOOL add (CDaoIndexFieldInfo &iFInfo);
};

class ParseFIELDINDEX : public Parser
{
public:
CDaoIndexFieldInfo indexFieldInfo;
ParseFIELDINDEX (CString &s,LPCSTR fs,LPCSTR ks);
BOOL extract (void);
};



//--------------------------
//
//	Dao Db Class 
// store db and open it
//
//--------------------------
class CDaoDbCreate : public CDaoDatabase
{
CString c_fieldSeparator;
CString c_keySeparator;
CArray <CString,CString&> c_dbDef;
CString c_dbName;
BOOL c_doCreate;

public:
CDaoDbCreate (void)
	{c_doCreate = FALSE;
	c_fieldSeparator = ",";
	c_keySeparator = ":";};


// Load from Memory
BOOL load (CString *dbMemDef,int size)
	{for (int i=0;i<size;i++)
		c_dbDef.Add(dbMemDef[i]);
	c_doCreate = TRUE;
	return TRUE;
	};
// Load from File
BOOL load (CString &fileName);

BOOL getLine (int index,CString &line)
		{if (c_dbDef.GetUpperBound()>=index)
			{line = c_dbDef[index];return TRUE;}
		else
			return FALSE;
		};	

BOOL Open (const CString& dataBaseName);
BOOL CreateModifyTable (int lineCount,CString &line );
BOOL existField (CDaoTableDef &table,CString &nome);
BOOL existIndex (CDaoTableDef &table,CString &nome);

};


#endif
