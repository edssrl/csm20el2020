// ReportPrint.cpp : implementation file
//

#include "stdafx.h"
// #include "Csm20El2013.h"
#include "MainFrm.h"
#include "Dialog.h"


#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "DAlzate.h"
#include "Csm20El2013.h"
#include "DReportPrint.h"
#include "DReportSheet.h"

// CDReportPrint dialog

IMPLEMENT_DYNAMIC(CDReportPrint, CPropertyPage)

CDReportPrint::CDReportPrint()
	: CPropertyPage(CDReportPrint::IDD)
{
m_repType = -1;
m_posFrom = 0;
m_posTo = 0;

c_pDoc = NULL;
c_elementIndex = -1;
c_alzataIndex = -1;
c_stripIndex = -1;
}

CDReportPrint::~CDReportPrint()
{
}

void CDReportPrint::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_TO, m_ctrlStaticTo);
	DDX_Control(pDX, IDC_STATIC_FROM, m_ctrlStaticFrom);
	DDX_Control(pDX, IDC_POS_TO, m_ctrlPosTo);
	DDX_Control(pDX, IDC_POS_FROM, m_ctrlPosFrom);
	DDX_Control(pDX, IDC_STRIP, m_ctrlStrip);
	DDX_Control(pDX, IDC_ALZATE, m_ctrlAlzate);
	DDX_Control(pDX, IDC_ELEMENTI, m_ctrlElementi);
	DDX_Radio(pDX, IDC_REPORT_TYPE, m_repType);
	DDX_Text(pDX, IDC_POS_FROM, m_posFrom);
	DDX_Text(pDX, IDC_POS_TO, m_posTo);

}


BEGIN_MESSAGE_MAP(CDReportPrint, CPropertyPage)
	ON_BN_CLICKED(IDC_REPORT_TYPE, OnReportType)
	ON_BN_CLICKED(IDC_STRIP_REPORT, OnStripReport)
	ON_CBN_SELCHANGE(IDC_ELEMENTI, OnSelchangeElementi)
	ON_CBN_SELCHANGE(IDC_ALZATE, OnSelchangeAlzate)
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDReportPrint message handlers
BOOL CDReportPrint::OnInitDialog() 
{
CDialog::OnInitDialog();
	
// TODO: Add extra initialization here
m_repType = 0;

// load sections from Doc
int nSection = 0;
if (c_pDoc != NULL)
	{
	int nElementi = c_pDoc->c_rotoloMapping.size();
	m_posFrom = 0;
	m_posTo = c_pDoc->getVTotalLength();
	for (int i=0;i<nElementi;i++)
		{
		int nAlz = c_pDoc->c_rotoloMapping.at(i).c_alzate.size();
		if (nAlz > 0)
			{
			//int posAlzata = c_pDoc->c_rotoloMapping.at(i).c_alzate.at((c_pDoc->c_rotoloMapping.at(i).c_alzate.size())-1).c_yPos;
			//int tipoAlzata = c_pDoc->c_rotoloMapping.at(i).c_alzate.at((c_pDoc->c_rotoloMapping.at(i).c_alzate.size())-1).c_tipo;;
			int posAlzata = c_pDoc->c_rotoloMapping.at(i).c_alzate.at(0).c_yPos;
			int tipoAlzata = c_pDoc->c_rotoloMapping.at(i).c_alzate.at(0).c_tipo;;
			// era qui per visualizzare 1 invece di 0 ora a fine ciclo
			//nSection ++;
			CString s;
			s.Format("Section N. %d position %d",nSection+1,posAlzata);
			//s.Format("Section %s position %d",
			//	c_pDoc->getStringFromInt(c_pDoc->getIntFromString(c_pDoc->c_firstRising)+nSection),posAlzata);
			if (tipoAlzata == ALZATA_SCARTO)
				s += "  Reject";
			//(c_pDoc->c_rotoloMapping.at(i)).at(l).c_posizione);	
			this->m_ctrlElementi.AddString(s);
			nSection ++;
			}
		}
	}


UpdateData(FALSE);
OnReportType(); 

#ifdef PDF_TOP_ZORDER
// set pdfInfo dialog as topmost zorder
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
if (pFrame)  
	{
#ifndef Csm20El2013_REPORT
	// show pdfWnd 
	pFrame->c_pdfInfo.setTopZorder();
#endif
	}

// start timer per eventuale visualizzazione finestra pdfInfo
SetTimer(11,1000,NULL);
#endif 

return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDReportPrint::OnDestroy() 
{
KillTimer(11);
CDialog::OnDestroy();
}

BOOL CDReportPrint::OnApply()
{
CDReportSheet *psheet;
// enable tabbed dialog
// GetParent( ) and cast to CPropertySheet.
psheet = (CDReportSheet *)GetParent( ); 
BOOL okClose = onOK();
psheet->setOkClose(okClose);
return (okClose);
}

BOOL CDReportPrint::onOK() 
{
UpdateData(TRUE);


// TODO: Add extra validation here
long indexE,indexA,indexS;

if(m_repType == 0)
	{
	if ((m_posFrom > m_posTo)||
		(m_posFrom < 0)||
		(m_posTo < 0)||
		(m_posTo > c_pDoc->getVTotalLength()))
		{
		Beep(300,1000);
		return(FALSE);
		}
	return(TRUE);
	}

indexE = m_ctrlElementi.GetCurSel();
indexA = m_ctrlAlzate.GetCurSel();
indexS = m_ctrlStrip.GetCurSel();

if ((indexE != CB_ERR)&&
	(indexA != CB_ERR)&&
	(indexS != CB_ERR))
	{
	c_elementIndex = indexE;
	c_alzataIndex = c_pDoc->c_rotoloMapping.at(indexE).getCurAlzIndex(indexA);
	c_stripIndex = indexS;
	// close and return
	return (TRUE);
	}
else
	{
	AfxMessageBox ("Please Select a Element-Rising-Strip combination");
	return (FALSE);
	}

}

void CDReportPrint::OnReportType() 
{
// TODO: Add your control notification handler code here
UpdateData(TRUE);

m_ctrlElementi.EnableWindow(FALSE);
m_ctrlPosTo.EnableWindow(TRUE);
m_ctrlPosFrom.EnableWindow(TRUE);
m_ctrlStaticTo.EnableWindow(TRUE);
m_ctrlStaticFrom.EnableWindow(TRUE);

CDReportSheet *psheet;
// enable tabbed dialog
// GetParent( ) and cast to CPropertySheet.
psheet = (CDReportSheet *)GetParent( ); 
psheet->EnablePage(1,TRUE);
psheet->EnablePage(2,TRUE);


}

void CDReportPrint::OnStripReport() 
{
	// TODO: Add your control notification handler code here
m_ctrlElementi.EnableWindow(TRUE);
m_ctrlPosTo.EnableWindow(FALSE);
m_ctrlPosFrom.EnableWindow(FALSE);
m_ctrlStaticTo.EnableWindow(FALSE);
m_ctrlStaticFrom.EnableWindow(FALSE);

// disable tabbed dialog
CDReportSheet *psheet;
// enable tabbed dialog
// GetParent( ) and cast to CPropertySheet.
psheet = (CDReportSheet *)GetParent( ); 
psheet->EnablePage(1,FALSE);
psheet->EnablePage(2,FALSE);
	
}


// viene selezionato un Elemento 
void CDReportPrint::OnSelchangeElementi() 
{
// clear alzate and strip ctrl
m_ctrlAlzate.ResetContent();
m_ctrlStrip.ResetContent();

// TODO: Add your control notification handler code here
int elemento = m_ctrlElementi.GetCurSel();

// populate Alzate
if (c_pDoc != NULL)
	{
	int numAlzate = c_pDoc->c_rotoloMapping.at(elemento).c_alzate.size();
	int nSection = 0;
	for (int i=0;i<numAlzate-1;i++)
		{// ecludo ultima alzata xChe` di fine
		//int posAlzata = c_pDoc->c_rotoloMapping.at(i).c_alzate.at((c_pDoc->c_rotoloMapping.at(i).c_alzate.size())-1).c_yPos;
		//int tipoAlzata = c_pDoc->c_rotoloMapping.at(i).c_alzate.at((c_pDoc->c_rotoloMapping.at(i).c_alzate.size())-1).c_tipo;;
		int posAlzata = c_pDoc->c_rotoloMapping.at(elemento).c_alzate.at(i).c_yPos;
		int tipoAlzata = c_pDoc->c_rotoloMapping.at(elemento).c_alzate.at(i).c_tipo;;
		if (tipoAlzata != ALZATA_SCARTO)
			{
			nSection ++;
			CString s;
			//s.Format("Rising %d position %d [m]",nSection,posAlzata);
			//s.Format("Rising %s position %d [m]",c_pDoc->getStringFromInt(c_pDoc->c_rotoloMapping.at(elemento).c_firstRising+nSection-1),posAlzata);
			//int curRis = c_pDoc->c_rotoloMapping.at(elemento).c_firstRising+nSection;
			int curRis = i+1;
			s.Format("Rising %s position %d [m]",c_pDoc->getStringFromInt(c_pDoc->c_rotoloMapping.getGlobalRisingId(elemento,curRis)),posAlzata);
			//(c_pDoc->c_rotoloMapping.at(i)).at(l).c_posizione);	
			//if (tipoAlzata == ALZATA_SCARTO)
			//	s += "  SCARTO";
			this->m_ctrlAlzate.AddString(s);
			}
		}
	}

	


}

void CDReportPrint::OnSelchangeAlzate() 
{
// TODO: Add your control notification handler code here
// clear alzate and strip ctrl
m_ctrlStrip.ResetContent();

// TODO: Add your control notification handler code here
int elemento = m_ctrlElementi.GetCurSel();
int alzata =  m_ctrlAlzate.GetCurSel();

// populate Alzate
if (c_pDoc != NULL)
	{
	int numStrip;
	numStrip = c_pDoc->c_rotoloMapping.at(elemento).size();
	int nSection = 0;
	for (int i=0;i<numStrip;i++)
		{// escludo ultima alzata xChe` di fine
		int posBobina = c_pDoc->c_rotoloMapping.at(elemento).at(i).c_posizione;
		double sizeBobina = c_pDoc->c_rotoloMapping.at(elemento).at(i).c_larghezza;
		nSection ++;
		CString s;
		//s.Format("Strip %c size %5.01lf [mm]",'A'+posBobina-1,sizeBobina);
		s.Format("Strip %d size %5.01lf [mm]",posBobina,sizeBobina);
		//(c_pDoc->c_rotoloMapping.at(i)).at(l).c_posizione);	
		this->m_ctrlStrip.AddString(s);
		}
	}
	
}

void CDReportPrint::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default

#ifdef PDF_TOP_ZORDER
// set pdfInfo dialog as topmost zorder
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
if (pFrame)  
	{
#ifndef Csm20El2013_REPORT
	// show pdfWnd 
	pFrame->c_pdfInfo.setTopZorder();
#endif
	}
#endif
}

