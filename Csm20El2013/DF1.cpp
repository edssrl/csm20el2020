// DF1.cpp : implementation file
//

#include "stdafx.h"
#include "DBSet.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"
#include "DF1.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDF1 dialog


CDF1::CDF1(CWnd* pParent /*=NULL*/)
	: CDialog(CDF1::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDF1)
	m_EditSel = _T("");
	m_coil = _T("");
	m_alloyType = _T("");
	m_thickness = 0.0;
	m_orderNumber = _T("");
	m_width = 0.0;
	m_numStrip = 0;
	m_firstRising = _T("");
	m_mode = -1;
	m_coil2 = _T("");
	m_useBottomHead = 0;
	m_skipADef = FALSE;
	m_freeNote = _T("");
	//}}AFX_DATA_INIT

c_hideMode = FALSE;
}
CDF1::~CDF1()
{
c_nomiRicette.RemoveAll();
}

void CDF1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDF1)
	DDX_Control(pDX, IDC_USE_TOP_HEAD, m_ctrlUseTop);
	DDX_Control(pDX, IDC_USE_BOTTOM_HEAD, m_ctrlUseBottom);
	DDX_Control(pDX, IDC_COIL2_LABEL, m_ctrlCoil2Label);
	DDX_Control(pDX, IDC_COIL2, m_ctrlCoil2);
	DDX_Control(pDX, IDC_MODE, m_ctrlMode);
	DDX_Control(pDX, IDC_MODE1, m_ctrlMode1);
	DDX_Control(pDX, IDC_MODE2, m_ctrlMode2);
	DDX_Control(pDX, IDOK, m_ctrlOk);
	DDX_Control(pDX, IDC_COMBO_SEL_RICETTE, m_ctrlSelRicetta);
	DDX_CBString(pDX, IDC_COMBO_SEL_RICETTE, m_EditSel);
	DDX_Text(pDX, IDC_COIL, m_coil);
	DDX_Text(pDX, IDC_ALLOY_TYPE, m_alloyType);
	DDX_Text(pDX, IDC_THICKNESS, m_thickness);
	DDX_Text(pDX, IDC_ORDER_NUMBER, m_orderNumber);
	DDX_Text(pDX, IDC_WIDTH, m_width);
	DDX_Text(pDX, IDC_NUM_STRIP, m_numStrip);
	DDX_Text(pDX, IDC_FIRST_RISING, m_firstRising);
	DDX_Radio(pDX, IDC_MODE, m_mode);
	DDX_Text(pDX, IDC_COIL2, m_coil2);
	DDX_Radio(pDX, IDC_USE_TOP_HEAD, m_useBottomHead);
	DDX_Check(pDX, IDC_SKIP_A_DEF, m_skipADef);
	DDX_Text(pDX, IDC_FREE_NOTE, m_freeNote);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDF1, CDialog)
	//{{AFX_MSG_MAP(CDF1)
	ON_BN_CLICKED(IDC_MODE, OnMode)
	ON_BN_CLICKED(IDC_MODE1, OnMode1)
	ON_BN_CLICKED(IDC_MODE2, OnMode2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDF1 message handlers
BOOL	CDF1::saveToProfile(void)
{
CProfile profile;

profile.writeProfileString ("F1","Coil",m_coil);
profile.writeProfileString ("F1","Coil2",m_coil2);
profile.writeProfileString ("F1","AlloyType",m_alloyType);
profile.writeProfileString ("F1","FreeNote",m_freeNote);

CString s;
//
s.Format("%lf",m_thickness);
profile.writeProfileString ("F1","Thickness",s);

//
profile.writeProfileString ("F1","OrderNumber",m_orderNumber);

//
s.Format("%lf",m_width);
profile.writeProfileString ("F1","Width",s);
//
//
s.Format("%d",m_numStrip);
profile.writeProfileString ("F1","NumStrip",s);
//
// ricetta default
m_EditSel.TrimRight();
profile.writeProfileString ("F1","RicettaSelected",m_EditSel);

profile.writeProfileString ("F1","FirstRising",m_firstRising);

if (this->m_skipADef)
	profile.writeProfileString ("F1","SkipADef","TRUE");
else
	profile.writeProfileString ("F1","SkipADef","FALSE");

// mode obbligatorio inserirlo
//s.Format("%d",m_mode);
//profile.writeProfileString ("F1","Mode",s);

return TRUE;
}


BOOL	CDF1::loadFromProfile(void)
{
CProfile profile;

m_coil = profile.getProfileString ("F1","Coil","");
m_coil2 = profile.getProfileString ("F1","Coil2","");
m_alloyType = profile.getProfileString ("F1","AlloyType","");
m_freeNote = profile.getProfileString ("F1","FreeNote","");

CString s;
s = profile.getProfileString ("F1","Thickness","10.0");
sscanf(s,"%lf",&m_thickness);

m_orderNumber = profile.getProfileString ("F1","OrderNumber","");

//
s = profile.getProfileString ("F1","Width","1400");
sscanf(s,"%lf",&m_width);
//
m_numStrip = profile.getProfileInt ("F1","NumStrip",5);

m_firstRising = profile.getProfileString ("F1","FirstRising","A");


m_skipADef = profile.getProfileBool ("F1","SkipADef",FALSE);


// mode obbligatorio inserirlo tutte le volte
//m_mode = profile.getProfileInt ("F1","Mode",0);

// ricetta default
m_EditSel = profile.getProfileString ("F1","RicettaSelected","DEFAULT");

return TRUE;
}

BOOL CDF1::OnInitDialog() 
{
CDialog::OnInitDialog();
	
// TODO: Add extra initialization here

if (c_hideMode)
	{// offline mode
	m_ctrlMode.ShowWindow(SW_HIDE);
	m_ctrlMode1.ShowWindow(SW_HIDE);
	m_ctrlMode2.ShowWindow(SW_HIDE);
	}
else
	{// start F1 -> disable ok 
	m_ctrlOk.EnableWindow(FALSE);
	// Hide coil2 -> only if separator
	m_ctrlCoil2.ShowWindow(SW_HIDE);
	m_ctrlCoil2Label.ShowWindow(SW_HIDE);
	}

	DBRicette dbRicette(dBase);
	dbRicette.openSelectDefault();

	// TODO: Add extra initialization here
	// dBase already Open
	// m_EditSel caricata da loadFromProfile 
	while(!dbRicette.IsEOF())
		{
		 m_ctrlSelRicetta.AddString(dbRicette.m_NOME);
		 dbRicette.MoveNext();
		}
	dbRicette.Close();


if (c_nomiRicette.GetSize() > 0)
	m_EditSel = c_nomiRicette[0];
	
m_ctrlUseTop.ShowWindow(SW_HIDE);
m_ctrlUseBottom.ShowWindow(SW_HIDE);


UpdateData(FALSE);

return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDF1::OnOK() 
{
// TODO: Add extra validation here
UpdateData(TRUE);

if ((!c_hideMode) && ((m_mode < 0)||(m_mode > 2)))
	{
	AfxMessageBox("Please Select SEPARATOR, SINGLE SHEET DOUBLE COIL or SINGLE SHEET mode");
	return;
	}

CDialog::OnOK();
}

// modo separatore
void CDF1::OnMode() 
{
// TODO: Add your control notification handler code here
m_ctrlOk.EnableWindow(TRUE);
// Show coil2 -> only if separator
m_ctrlCoil2.ShowWindow(SW_SHOW);
m_ctrlCoil2Label.ShowWindow(SW_SHOW);
m_ctrlUseTop.ShowWindow(SW_HIDE);
m_ctrlUseBottom.ShowWindow(SW_HIDE);

}

// modo single sheet double coil
void CDF1::OnMode1() 
{
// TODO: Add your control notification handler code here
m_ctrlOk.EnableWindow(TRUE);
// Hide coil2 -> only if separator
m_ctrlCoil2.ShowWindow(SW_HIDE);
m_ctrlCoil2Label.ShowWindow(SW_HIDE);
m_ctrlUseTop.ShowWindow(SW_HIDE);
m_ctrlUseBottom.ShowWindow(SW_HIDE);

}

// modo single sheet single head
void CDF1::OnMode2() 
{
// TODO: Add your control notification handler code here
m_ctrlOk.EnableWindow(TRUE);
// Hide coil2 -> only if separator
m_ctrlCoil2.ShowWindow(SW_HIDE);
m_ctrlCoil2Label.ShowWindow(SW_HIDE);
m_ctrlUseTop.ShowWindow(SW_SHOW);
m_ctrlUseBottom.ShowWindow(SW_SHOW);
	
}
