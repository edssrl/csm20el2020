// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

//#undef UNICODE
//#undef _UNICODE

#include "targetver.h"
// leak detector should appear before '#include <afxwin.h>' in file stdafx.h
#include <vld.h>


#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers


#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC OLE automation classes
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <AfxTempl.h>

// Use for print preview ClipRgn
#include <afxpriv.h>
#include <math.h>

#include <afxcview.h>
#include <afxctl.h>
#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

// SingleLock Multithread
#include <afxmt.h>

#include "profile.h"

#define PI 3.14159265359


#include <afxdlgs.h>
#include <afx.h>

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#define MAX_NUMCLASSI 4+1
#define MAX_NUMLABEL 24
#define MAX_NUMSTRIP 80	// era 40

// testa separata in due hde anche se single coil
#define USE_BOTTOM

#ifdef _DEBUG
// #define _ITERATOR_DEBUG_LEVEL 0	// speedup iterator operations while debugging
#endif

#pragma warning(disable: 4018)
#pragma warning(disable: 4244)
#pragma warning(disable: 4995)
#pragma warning(disable: 4996)

#ifdef	 _CSM20_INSPECTION
#include "easyLogging++.h"

// trace only if debug defined
#define TRACE(x) LDEBUG<<x
#define TRACE0(x) TRACE(x)
#define TRACE1(x,y) (LDEBUG<<x<<y) 
#endif



