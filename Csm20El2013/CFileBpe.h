//-------------------------------------------------------------------
//
//			  CFileBpe    CFile con gestione compressore BPE
//
//
//-------------------------------------------------------------------

#ifndef CFILEBPE

#define CFILEBPE


#include "bpe32err.h"


#define BLOCKSIZE 4096      /* Maximum block size */
#define HASHSIZE  4096       /* Size of hash table */
#define MAXCHARS  150        /* Char set per block */
#define THRESHOLD 3			/* Minimum pair count */



enum BpeMode {BPE_NORMAL,BPE_COMPRESS};

class CFileBpe : public CFile
{
UINT openFlags;

BpeMode mode;
unsigned char buffer [BLOCKSIZE];
unsigned char leftCode [256];
unsigned char rightCode [256];
unsigned char left [HASHSIZE];
unsigned char right [HASHSIZE];
unsigned char count [HASHSIZE];
short size;		/* Size of current data block */
short used;

// Read Section
CArray <char ,char &> readBuffer; 
int readIndex;

// Implementation
// Compress
short lookup (unsigned char,unsigned char);
int initCompress(void);
int writeCompress(const void* buf, UINT dim);
int flushWriteCompress (void);
int flushCompress (void);

// Expand
int readCompress(void* buf, UINT dim);
int unpack(void);


public:

CFileBpe(BpeMode m = BPE_NORMAL):CFile() 
	{mode = m;
	openFlags = 0;
	readIndex = -1;
	readBuffer.SetSize(BLOCKSIZE);
	initCompress();	
	};
  
CFileBpe(HANDLE hFile,BpeMode m = BPE_NORMAL):CFile(hFile)
	{mode = m;
	openFlags = 0;
	readIndex = -1;
	readBuffer.SetSize(BLOCKSIZE);
	initCompress();	
	};
  
CFileBpe( LPCTSTR lpszFileName, UINT nOpenFlags,BpeMode m = BPE_NORMAL)
			:CFile(lpszFileName,nOpenFlags)
	{mode = m;
	openFlags = nOpenFlags;
	readIndex = -1;
	readBuffer.SetSize(BLOCKSIZE);
	initCompress();	
	};

virtual BOOL Open( LPCTSTR lpszFileName, UINT nOpenFlags, 
	CFileException* pError = NULL,BpeMode m = BPE_NORMAL )
		{mode = m;	readIndex = -1;	openFlags = nOpenFlags;
		return (CFile::Open(lpszFileName,nOpenFlags,pError));};


virtual UINT Read( void* lpBuf, UINT nCount )
		{if (mode == BPE_NORMAL) 
			{
			return (CFile::Read(lpBuf,nCount));
			}
		else 
			{return (readCompress(lpBuf,nCount));}
		};
			
virtual void Write( const void* lpBuf, UINT nCount )
  		{if (mode == BPE_NORMAL) 
			{
			CFile::Write(lpBuf,nCount); 
			}
		 else {writeCompress(lpBuf,nCount);}
		};

virtual void Close( )
	{if ((mode == BPE_COMPRESS)&&
		(openFlags & CFile::modeWrite))
		flushCompress();
	
	readIndex = -1;
	CFile::Close();
	};

virtual void Flush( )
	{if ((mode == BPE_COMPRESS)&&
		(openFlags & CFile::modeWrite))
		flushCompress();
	CFile::Flush();
	};
};


#endif