// CustStatusBar.h: interface for the CCustStatusBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CUSTSTATUSBAR_H__ACE6A711_64C9_11D2_A75C_00C026A019B7__INCLUDED_)
#define AFX_CUSTSTATUSBAR_H__ACE6A711_64C9_11D2_A75C_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CCustStatusBar : public CStatusBar  
{
public:
	CCustStatusBar();
	virtual ~CCustStatusBar();
		// Initialize our status bar.
		void Init() ;

		// Set the bitmap to display in the statusbar.
		void SetBitmap(int i) ;

		// Override the painting of the statusbar.
		virtual void DrawItem(LPDRAWITEMSTRUCT p);


  protected:
      CFont m_Font ; 			// Font for statusbar.
      int m_iCurrentDIB ;		// Index of DIB currently displayed in dialog.
//      CGL* m_pScenes[3] ;		// OpenGL rendered scenes.
//      CSimpleDIB m_DIB[3] ;	// DIBs to hold the OpenGL scenes.
};

#endif // !defined(AFX_CUSTSTATUSBAR_H__ACE6A711_64C9_11D2_A75C_00C026A019B7__INCLUDED_)
