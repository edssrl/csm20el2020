// RpcMClient.h : header file
//
//
// USO:
// Bisogna derivare CMainFrame da CRcpClient
// Implementare virtual serverRequest in CMainFrame
// RpcClient e` sviluppato per singola connessione
// Realizzato per connessioni multiple CRpcMClient 
// Inserito mittente in server request
// aggiunta gestione connessioni PIPE
// connessione con Rpcserver2011 ver >= 2.10  23/11/13
/////////////////////////////////////////////////////////////////////////////
// CRpcMClient window

#include "..\..\..\..\RpcServer2011\RpcNet.h"
#include "..\..\..\..\RpcServer2011\npipe.h"
#include <afxmt.h>
#include "System.h"

#pragma once


enum RemoteDirection  {DIR_NONE=0,DIR_RECEIVE,DIR_SEND,DIR_SEND_RECEIVE};


struct Command
{
int cmd;
int size;
void *data;
};


struct 	CommandBuffer
{
int  c_size;
char c_block[256];
};

class CRpcMClient 
{

// This code defines a list of ints.
CList<CommandBuffer,CommandBuffer> c_txBufferList;
CCriticalSection c_txBufferLock;
CList<CommandBuffer,CommandBuffer> c_rxBufferList;
CCriticalSection c_rxBufferLock;

// Construction
public:
	virtual BOOL serverRequest (int cmd,int sender,int size,void *pter);
	CRpcMClient();

	BOOL closeAndWaitPipe(void);

// Attributes
public:
// Stato sistema
BOOL c_rpcOffLine; // Funzionamento senza connessione

// gestione pipe e thread
HANDLE	c_hPipeEvent;
BOOL c_ThreadTerminated;
CWinThread* c_pipeThCom ;
static UINT pipeThreadDMCS ( LPVOID pParam);

BOOL c_serMsgReceived;	 // MainFrame override this var 
// Operations
public:
void rpcRegister(CString& wndName,int thisId,RPCTYPE type=TYPE_DMCS,int size=4);
BOOL RegisterClient (CString &nome,RPCID id,int frameSize=4,BOOL abortNotify=FALSE);
LRESULT OnServerRequest(WPARAM wParam, LPARAM lParam);
BOOL SendGeneralCommand(Command *command,RPCID remId);
BOOL SendBroadcastGeneralCommand(Command *command);
BOOL SendPipeCommand(Command *command,RPCID remId);
BOOL ReadPipeCommand(CommandBuffer *command);

void setProcServer(CString s){c_procServer=s;};
void setDirServer(CString s){c_dirServer=s;};
void addRemote(int remoteId,CString &device);
int getThisRpcId (void) {return (c_thisRpcId);};
int getType (void) {return (c_type);};

// Implementation
public:
	virtual ~CRpcMClient();

private:
	CString c_procServer;
	CString c_dirServer;
	CString c_thisWnd;
	RPCTYPE c_type;
	// tipo connessione
	BOOL c_rpcPipeMode;
	CNamedPipe	c_pipe;

	int		c_size; // per comunicazioni TYPE_DC 
	int		c_thisRpcId;
	struct Remote
		{int id;
		CString device;};
	BOOL CRpcMClient::openPipe(CString nome);

	// Remote Info
	CArray <Remote,Remote &> c_remoteRpcId; 
};

/////////////////////////////////////////////////////////////////////////////
