// GraphDoc.cpp : implementation file
//


#include "stdafx.h"

// lasciare dopo stdfx.h
#define MAIN_DOC


#include "MainFrm.h"
#include "resource.h"

#include "Endian.h"
#include "Elemento.h"


#include "GraphDoc.h"
#include "GraphView.h"
#include "RollView.h"
#include "TowerView.h"
#include "MLineView.h"

#include "WaitDlg.h"

#include "DbRotoli.h"
#include "DbElementi.h"
#include "DNumStrip.h"

#include "DStripPosition.h"
#include "DTagliElemento.h"
#include "DAlzate.h"


#include "dyntempl.h"
#include "Csm20El2013.h"

#include "DCustomer.h"
#include "DCompany.h"
// #include "DSimOption.h"

#include "DStripPosition.h"
#include "DF1.h"
#include "DTagliElemento.h"
#include "DAlzate.h"

#include "Dialog.h"
#include "RTextFile.h"
#include "DBSet.h"			// DB
#include "Endian.h"
#include "pwdlg.h"			// Password

//#ifndef Csm20El2020_REPORT
//#include "PdfInfo.h"
//#endif

#include "versione.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif






//////////////////////////////////////////////////////////////////////
//	Class Scheda

// Eliminato codice Scheda OBSOLETO!!

/////////////////////////////////////////////////////////////////////////////
// CLineDoc

// IMPLEMENT_DYNCREATE(CLineDoc, CDocument)
IMPLEMENT_DYNAMIC(CLineDoc, CDocument)

// Torna 0 per InitView 
// Torna 1 per LineView
// Torna 2 per BarView
// Torna 0 per InitView 
// Torna 1 per LineView
// Torna 1 per BarView

int CLineDoc::GetActualViewID(void)
{
POSITION pos;

pos = GetFirstViewPosition();
ASSERT (pos);

viewID = (( CDynViewDocTemplate *)GetDocTemplate())->
					GetViewID(GetNextView(pos));

return (viewID);
}


ViewType CLineDoc::GetActualViewType(void)
{
POSITION pos;
ViewType vt;

pos = GetFirstViewPosition();
ASSERT (pos);

CInitView *view;
view = (CInitView *) GetNextView(pos);

//viewType  = (( CDynViewDocTemplate *)GetDocTemplate())->
//					GetViewID(GetNextView(pos));

vt = view->getViewType();

return (vt);
}



CLineDoc::CLineDoc()
	: c_redrawAll(FALSE)
	, c_viewScroll(FALSE)
	, c_redraw(FALSE)
	, c_numLanes (1)
	, c_trimLeft  (0.)
	, c_trimRight (0.)
{

//CCSMApp* pApp = (CCSMApp*)AfxGetApp();
// Init numero schede
CProfile profile;

c_exeVer = VERSIONE;

c_difCoil.setMmBaseStepX(profile.getProfileInt(_T("Init"), _T("BaseStepX"),1)*1000); 
// BaseStepX = 1; 

int numSchede = profile.getProfileInt(_T("Init"), _T("NumeroBande"),272); 
int numSchedeLeft = profile.getProfileInt(_T("Init"), _T("NumeroBandeLeftSide"),144); 

int numClassi = profile.getProfileInt(_T("Init"), _T("NumeroClassi"),4); 
// dimensione in byte dei dati ricevuti da VAL_COUNTER 1=byte
// 2 = short , 4 == long
int sizeClassi = profile.getProfileInt(_T("Init"), _T("SizeDataClassi"),1); 

// Identificativo dell'impianto
c_impianto = profile.getProfileString(_T("Init"), _T("Impianto"),"Impianto1"); 


// c_useFingerList = profile.getProfileBool(_T("Init"), _T("UseFingerList"),FALSE);;

c_viewStripBorder = profile.getProfileBool(_T("Init"), _T("VSB"),TRUE);
c_viewStripBorderPosition = profile.getProfileBool(_T("Init"), _T("VSBP"),TRUE);

c_skipRxInAutotestMsg = profile.getProfileBool(_T("Init"), _T("SkipRxInAutotestMsg"),FALSE); ;

// configurazione indice prima scheda classe AB nell'HW
// puo` essere 0 ( normale) 1 schede ab e cd invertite
c_firstBoardClassAB = profile.getProfileInt(_T("Init"), _T("firstBoardClassAB"),0); 

c_useDiaframmi = profile.getProfileBool(_T("Init"), _T("UseDiaframmi"),TRUE); 

c_useFingerHw = profile.getProfileBool("init","UseFingerHw",TRUE);
	
// Distanza tra bordo materiale e primo coltello
// solo per fingher Hw
if (c_useFingerHw)
	c_offsetSx = profile.getProfileInt(_T("Init"), _T("DBordoColtello"),0); 
else
	c_offsetSx = 0;

// nomi bobine right to left
// Eliminato 02-05-08 
//c_rotoloMapping.c_reverseBobinePosition = profile.getProfileBool(_T("Init"), _T("ReverseBobinePos"),TRUE); 

SIZE_BANDE = (double) profile.getProfileInt(_T("Init"), _T("SizeBande"),11);

c_blankCoil = profile.getProfileInt(_T("Init"), _T("PosBlankCoil"),5);

c_limUpdateDiaf  = profile.getProfileInt(_T("Init"), _T("LimUpdateDiaf"),100);

// allinea strip su bordo destro
c_rightAlignStrip = profile.getProfileBool(_T("init"), _T("AlignStripOnRightBorder"),FALSE);


c_densityCalcLength = 1000.;

switch(sizeClassi)
	{
	case 1:
	case 2:
	case 4:
		break;
	default:
		{
		CString	resMsg;
		resMsg.LoadString(CSM_GRAPHDOC_INICLASSI);
	//	AfxMessageBox ("ERRORE: INI File -> [init] [NumeroClassi] fuori range");
		AfxMessageBox (resMsg);
		sizeClassi = 1;
		}
		break;
	}

c_sizeClassi = sizeClassi;

// inizializzo class b filterList
c_classBFilterList.SetSize(numSchede);

if ((numClassi > MAX_NUMCLASSI)||(numClassi < 0))
	{
	CString	resMsg;
	resMsg.LoadString(CSM_GRAPHDOC_INICLASSI);
//	AfxMessageBox ("ERRORE: INI File -> [init] [NumeroClassi] fuori range");
	AfxMessageBox (resMsg);
	numClassi = MAX_NUMCLASSI;
	}
 
// RemoveAll + init var 
c_difCoil.clear(numSchede,numClassi,numSchedeLeft);

// RemoveAll + init var 
CDStripPosition strip;
strip.c_rightAlignStrip = c_rightAlignStrip;
strip.loadFromProfile(FALSE);
int numMarker = strip.m_numStrip;
// i marker non sono divisi in classi
c_markerCoil.clear(numMarker,1,0);
c_numLanes = numMarker;

c_trimLeft = strip.m_trimLeft;
c_trimRight = strip.m_trimRight;

// jungfer specific
// c_firstZonePosition = 0; // distanza in mm prima zona dal diaframma

c_holeClassMarking = 1;				// 1 = class A
									// 2 = class B
									// 3 = class C
									// 4 = class D											
c_itmd = 0;							// Inspection to markers distance in cm  	
c_markersDisable = 0;			    // bitmask 1 = disable marker 0 enable
c_markersOnTime = 1;			    // the length of marker in cm
memset(c_valZoneNumberSize,0, sizeof(c_valZoneNumberSize)); // width for each zone number in mm.

// Base Page Init
TrendLScalaY = 100.;
TrendDScalaY = 100.;
TrendLScalaX = 100;
TrendDScalaX = 100;


viewID = 0;

c_fileLogPosition = -1;

systemOnLine = FALSE;
systemOnTest = FALSE;

systemLoading = FALSE;
// Idati sono stati caricati da file (no Save)
loadedData = FALSE;

diskAlarmActive = FALSE;
serAlarmActive = FALSE;
c_alarmStripActive = FALSE;
c_alarmCheckShutterLeftActiveTop = FALSE;
c_alarmCheckShutterRightActiveTop = FALSE;
c_alarmCheckShutterLeftActiveBot = FALSE;
c_alarmCheckShutterRightActiveBot = FALSE;


// Init title alarm
//CString t = "Allarme Densita'";
CString t;
t.LoadString (CSM_GRAPHDOC_ALDENS);
densitaAlarm.SetTitle(t);

// t = "Allarme Periodici";
t.LoadString (CSM_GRAPHDOC_ALPER);
// periodAlarm.SetTitle (t);

// t = "Allarme Sistema";
t.LoadString (CSM_GRAPHDOC_ALSYS);
systemAlarm.SetTitle (t);

// Allarme scheda Scollegata
serMsgReceived = FALSE;

// valore in metri tra due difetti
c_holeRate = 5;

//---------------------
// c_stato hde micro
c_statoHdeTop = -1;
c_statoHdeBot = -1;
// raddoppiati un solo doc


c_lunResStop = 0;

// alzata
c_statoAlzata = 0;

// Csm20Cf
//c_rotoloExt = 0;
// Csm20El2013
c_side = "X";

// Timer setting demo
millisec = 500;

// 
c_posDiaframmaSx = 0.;
c_posDiaframmaDx = SIZE_BANDE * numSchede;
c_posDiaframmaSx2 = 0.;
c_posDiaframmaDx2 = SIZE_BANDE * numSchede;

//---------------------------------------------------------------------------
// Elval Marker specific
// c_firstZonePosition = 0; // distanza in mm prima zona dal diaframma
c_holeClassMarking = 1;				// 1 = class A
									// 2 = class B
									// 3 = class C
									// 4 = class D											
c_itmd = 0;							// Inspection to markers distance in cm  	
c_markersDisable = 0;			    // bitmask 1 = disable marker 0 enable
c_markersOnTime = 1;			    // the length of marker in cm
memset(c_valZoneNumberSize,0, sizeof(c_valZoneNumberSize)); // width for each zone number in mm.

// qs si usa fuori ispezione
CTime time = CTime::GetCurrentTime();
// save date and time stop 
c_stopTime = time.GetTime();


c_valInput = 0;

// Offset posizione dopo continua stesso asse
c_baseContinuaRotolo = 0;

c_autoPrinted = FALSE;

c_skipHoleAInspection = FALSE;

c_firstRising = "A";
c_mode = 0;
c_numTheoryStrip = 0;

c_sideActive = 1;
}

BOOL CLineDoc::UpdateBaseDoc(void)
{
// Read Config Default Value From DBase
DBOpzioni dbOpzioni(dBase);

if (dbOpzioni.openSelectDefault ())
	{
	secAlarmSound = (int )dbOpzioni.m_ALLARME_DURATA;
	// Nessun allarme = time 0
	if (!dbOpzioni.m_ALLARME_ACU)
		secAlarmSound = 0;

	// Base Update
	vFori = dbOpzioni.m_TL_CLASSE;
	if (TrendLScalaX != (int ) dbOpzioni.m_TL_LUNGHEZZA)
		{
		TrendLScalaX = (int ) dbOpzioni.m_TL_LUNGHEZZA;
//		BaseScalaX = (int )schede.getMeter(); // Base Visualizzazione attuale posizione
		}
	TrendLScalaY = dbOpzioni.m_TL_SCALA;
	
	TrendDScalaY = dbOpzioni.m_TD_SCALA;
	TrendDScalaX = (int )dbOpzioni.m_TD_LUNGHEZZA;
		
	dbOpzioni.Close();
	}


// Read Config Default Value From DBase
DBParametri dbParametri(dBase);
if (dbParametri.openSelectDefault ())
	{
	CProfile profile;
	c_fattCorrDimLin = dbParametri.m_FCDL;
	//-------------------------------------
	// Correzione fcdl con valore rulli
	CString s = profile.getProfileString("init","DiaEncoder","300.00");
	double diaE = atof((LPCSTR)s);
	double diaV = dbParametri.m_DIA_ENCODER;
	double correz = (PI * diaV)/(PI * diaE);
	c_fattCorrDimLin *= correz;
	// -------------------------------------
	c_lunResStop = dbParametri.m_L_RESIDUA_STOP;
	/*-------------------------------
	// Agosto 2004
	// ora soglie da ricetta
	sogliaA = dbParametri.m_SOGLIA_A;
	sogliaB = dbParametri.m_SOGLIA_B;
	sogliaC = dbParametri.m_SOGLIA_C;
	sogliaD = dbParametri.m_SOGLIA_D;
	--------------------------------*/
	dbParametri.Close();
	}

// Read Config Default Value From DBase
DBRicette dbRicette(dBase);
if (dbRicette.openSelectID (nomeRicetta))
	{
	c_difCoil.c_sogliaAllarme.RemoveAll();
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_A);
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_B);
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_C);
	c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_D);
	c_densityCalcLength = dbRicette.m_LENGTH_ALLARME;
	// Agosto 2004
	// ora soglie da ricetta
	sogliaA = dbRicette.m_SOGLIA_A;
	sogliaB = dbRicette.m_SOGLIA_B;
	sogliaC = dbRicette.m_SOGLIA_C;
	sogliaD = dbRicette.m_SOGLIA_D;
	
	// Sezione Markers
	// c_firstZonePosition = dbParametri.m_FIRST_CUTTER_POS;
	// c_holeClassMarking = dbParametri.m_MARKER_CLASS;
	// c_itmd = dbParametri.m_DIST_MARKER_INSP;
	// c_markersOnTime = dbParametri.m_MARKER_SIZE;


	// soglie periodici
	c_sogliaPer = (int)dbRicette.m_SOGLIA_PER;
	//------------------------------
	dbRicette.Close();
	}
else
	{
	if (dbRicette.openSelectDefault ())
		{
		c_difCoil.c_sogliaAllarme.RemoveAll();
		c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_A);
		c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_B);
		c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_C);
		c_difCoil.c_sogliaAllarme.Add(dbRicette.m_ALLARME_D);
		c_densityCalcLength = dbRicette.m_LENGTH_ALLARME;
		// Agosto 2004
		// ora soglie da ricetta
		sogliaA = dbRicette.m_SOGLIA_A;
		sogliaB = dbRicette.m_SOGLIA_B;
		sogliaC = dbRicette.m_SOGLIA_C;
		sogliaD = dbRicette.m_SOGLIA_D;

		// soglie periodici
		c_sogliaPer = (int)dbRicette.m_SOGLIA_PER;
		//------------------------------
		dbRicette.Close();
		}
	
	}

// aggiunto secondo parametro 0, non aggiorna subito scale!
UpdateAllViews(NULL,0);
return TRUE;
}

BOOL CLineDoc::OnNewDocument()
{
if (!CDocument::OnNewDocument())
		return FALSE;

// Set Title
SetTitle ((LPCSTR)" ");

// Clear and reset data structure on new Coil
// FALSE -> coldStart
newCoil (COLD_START);

if (!UpdateBaseDoc())
	return FALSE;

return TRUE;
}

// Parametro indica warmStart
void CLineDoc::newCoil(enum StartMode warmStart,CDStripPosition* pDialog )
{
// Init numero schede
CProfile profile;
int numSchede = profile.getProfileInt(_T("init"), _T("NumeroBande"),272);
int numSchedeLeft = profile.getProfileInt(_T("init"), _T("NumeroBandeLeftSide"),144);
int numClassi = profile.getProfileInt(_T("init"), _T("NumeroClassi"),4); 
int numLaser = profile.getProfileInt(_T("init"), _T("NumeroLaser"),7); 
CString topLabel = profile.getProfileString(_T("init"), _T("TopLabel"),""); 
CString bottomLabel = profile.getProfileString(_T("init"), _T("BottomLabel"),""); 

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
ASSERT (pFrame!=NULL);

pFrame->c_stAllarmiTop.setNumLaser(0);
pFrame->c_stAllarmiBottom.setNumLaser(numLaser);

pFrame->c_stAllarmiTop.setLabel(topLabel);
pFrame->c_stAllarmiBottom.setLabel(bottomLabel);

// configura allarmi top e bot
pFrame->c_stAllarmiTop.setLaserState(AL_OFF);
pFrame->c_stAllarmiTop.setEncoderState(AL_OFF);
pFrame->c_stAllarmiTop.setOtturatore1State(AL_OFF);
pFrame->c_stAllarmiTop.setOtturatore2State(AL_OFF);
//Bottom
pFrame->c_stAllarmiBottom.setLaserState(AL_HIDE);
pFrame->c_stAllarmiBottom.setEncoderState(AL_OFF);
pFrame->c_stAllarmiBottom.setOtturatore1State(AL_OFF);
pFrame->c_stAllarmiBottom.setOtturatore2State(AL_OFF);


// if (numClassi == 2)
if (1)
// elval 2013 come 2 classi ! anche se 4, anche 2020
	{
	pFrame->c_stAllarmiTop.setNumReceiver(numSchede);
	pFrame->c_stAllarmiBottom.setNumReceiver(0);
	}
else
	// 4 classi
	{ // 
	pFrame->c_stAllarmiTop.setNumReceiver(numSchede*2);
	pFrame->c_stAllarmiBottom.setNumReceiver(numSchede*2);
	}

c_numCpu = profile.getProfileInt(_T("init"), _T("NumeroCpu"),34); 
c_numCpuLeftSide = profile.getProfileInt(_T("init"), _T("NumeroCpuLeftSide"),18); 

pFrame->c_stAllarmiTop.setNumCpu(c_numCpu);
pFrame->c_stAllarmiBottom.setNumCpu(0);
	
// Clear Allarmi in formato testo
densitaAlarm.clearMessages();
periodAlarm.clearMessages();
systemAlarm.clearMessages();

int numMarker = c_numLanes;

switch (warmStart)
	{
	case WARM_START:
		{// copy last c_lunResStop of defect
		COILTYPE dCoil;
		dCoil.clear(numSchede,numClassi,numSchedeLeft);
		dCoil.setMmBaseStepX(profile.getProfileInt(_T("Init"), _T("BaseStepX"),1)*1000);
		int totalLength = (int)getVTotalLength();
		int posMeter = (int)c_difCoil.getMeter();
		c_difCoil.dump(dCoil,totalLength,posMeter);
		c_difCoil.clear(numSchede,numClassi,numSchedeLeft);
		c_difCoil = dCoil;
		resetClassBFilterList();

		COILTYPE mCoil;
		mCoil.clear(numMarker,numClassi,numSchedeLeft);
		mCoil.setMmBaseStepX(profile.getProfileInt(_T("Init"), _T("BaseStepX"),1)*1000); 
		c_markerCoil.dump(mCoil,totalLength,posMeter);
		c_markerCoil.clear(numMarker,numClassi,numSchedeLeft);
		c_markerCoil = mCoil;


		}
		break;
	case COLD_START:
		{
		c_difCoil.clear(numSchede,numClassi,numSchedeLeft);
		resetClassBFilterList();
		c_markerCoil.clear(numMarker,numClassi,numSchedeLeft);
		}
	case CONTINUE_COIL:
		break;
	}

// flag allarme densita`
c_densityAlarmActive = FALSE;

// Aggiorno Parametri di calcolo
// c_difCoil.setLargeSize(1400);
// c_larghezza = 1400;
// Applico fattore correttivo ai 1 metri
// c_difCoil.setTrueSizeEncod(c_fattCorrDimLin * 1000.);
double corrPos;
corrPos = c_lunResStop + 0.;
c_difCoil.setMeter((int ) corrPos);
randDif.pos = ((int) corrPos);
// empty RollMap data structure
c_targetBoard.setEmpty();
c_targetBoard.setLastPos(0);

// Clear autoPrinted variable
c_autoPrinted = FALSE;

// Bobine
c_rotoloMapping.reset();

// tagli select deve impostare il primo elemento con 
// setting dei coltelli
// inserisce elemento finto con larghezza totale 1 strip solo
// quando arriva pacchetto di riconoscimento elimino elemento e inserisco
// quello corretto 
// false non modifica ma inserisce
tagliSelect(pDialog,FALSE);


// aggiorno 
// Ok aggiorno ultimo elemento lavorato
c_rotoloMapping.c_lastElemento += 1;

// inizializzo maschera per identificare strip
fillStripList();

if (c_tmpFingerList.GetSize() == 0)
	// set fingerList CArray size as numSchede
	c_tmpFingerList.SetSize(c_difCoil.getNumSchede());

// creazione fingerList vuota  
if (c_fingerList.GetSize() == 0)
	fillFingerList(c_fingerList);

if (c_tmpFingerList2.GetSize() == 0)
	c_tmpFingerList2.SetSize(c_difCoil.getNumSchede());

if (c_fingerList2.GetSize() == 0)
	fillFingerList(c_fingerList2);
	
// aggiungo alzata di partenza se resettato metraggio OK 0
// altrimenti alzata continuazione
c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).c_firstRising = getIntFromString(c_firstRising);

}



CLineDoc::~CLineDoc()
{
c_fingerList.RemoveAll();
c_fingerList2.RemoveAll();
c_tmpFingerList.RemoveAll();
c_tmpFingerList2.RemoveAll();
c_classBFilterList.RemoveAll();
}


BEGIN_MESSAGE_MAP(CLineDoc, CDocument)
	//{{AFX_MSG_MAP(CLineDoc)				 
	ON_COMMAND(ID_TESTGRAFICO, OnTestgrafico)
	ON_COMMAND(ID_FILE_START, OnFileStart)
	ON_COMMAND(ID_FILE_STOP, OnFileStop)
	ON_UPDATE_COMMAND_UI(ID_FILE_START, OnUpdateFileStart)
	ON_UPDATE_COMMAND_UI(ID_FILE_STOP, OnUpdateFileStop)
	ON_COMMAND(ID_FILE_TSTART, OnFileTstart)
	ON_UPDATE_COMMAND_UI(ID_FILE_TSTART, OnUpdateFileTstart)
	ON_COMMAND(ID_FILE_ALZATA, OnFileAlzata)
	ON_UPDATE_COMMAND_UI(ID_FILE_ALZATA, OnUpdateFileAlzata)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLineDoc diagnostics

#ifdef _DEBUG
void CLineDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLineDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLineDoc serialization

/*
CLineDoc&  CLineDoc::operator=(CLineDoc& source)
{

// Dati Lavorazione
// Save rotolo
c_rotolo = source.c_rotolo;
// Save rotoloExt
c_rotolo2 = source.c_rotolo2;

// save title
// c_fileName = source.c_fileName;

// Save lega
c_lega = source.c_lega;

// Save ricetta
nomeRicetta = source.nomeRicetta; 

// Save stato
c_statoAlzata = source.c_statoAlzata; // insert EOS

// Save cliente
c_cliente = source.c_cliente; // insert EOS

// Save startTime
c_startTime = source.c_startTime;

// nome asse precedente x stampa online ripetuta ultimo asse
// CString c_lastFileName;


/ Attributi specifici jungfer
c_address = source..c_address;
c_city = source.c_city;
c_nation = source.c_nation;
c_notes = source.c_notes;
c_numLanes = source.c_numLanes;
c_operator = source.c_operator;
c_spessore2 = source.c_spessore2;
c_telefax = source.c_telefax;
c_telephone = source.c_telephone;
c_zip = source.c_zip;
c_order = source.c_order;
*


// save spessore larghezza
c_spessore = source.c_spessore;
c_larghezza = source.c_larghezza;

// Dati System Dependent
c_fattCorrDimLin = source.c_fattCorrDimLin;
c_lunResStop = source.c_lunResStop;

// Save Difetti
c_difCoil = source.c_difCoil;

// Save Difetti
c_markerCoil = source.c_markerCoil;


// Save Allarmi
densitaAlarm = source.densitaAlarm;

periodAlarm = source.periodAlarm;

systemAlarm = source.systemAlarm;

return (*this);
}

*/

BOOL CLineDoc::save(CFileBpe *cf,CProgressCtrl *progress)
{
char idFileType[] = FILEDOCTYPE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
// Save ID
cf->Write((void *)idFileType,sizeof(idFileType));

// Ver 02 = numero schede variabili (14-11-97)
int ver = FILEDOCVER; 
// Save ID
cf->Write((void *)&ver,sizeof(int));

// Save c_exeVer
cf->Write((void *)&c_exeVer,sizeof(c_exeVer));

// Dati Lavorazione
// Save c_rotolo
int size = c_rotolo.GetLength() + 1; // insert EOS
char c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_rotolo),size -1);
cf->Write((void *)&c,sizeof (c));

// Save c_rotolo2
size = c_rotolo2.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_rotolo2),size -1);
cf->Write((void *)&c,sizeof (c));

// Save side
size = c_side.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_side),size -1);
cf->Write((void *)&c,sizeof (c));

// Save c_rotoloRil
cf->Write((void *)&c_rotoloRil,sizeof(c_rotoloRil));

// Save c_firstRising
size = c_firstRising.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_firstRising),size -1);
cf->Write((void *)&c,sizeof (c));

// Save c_lega
size = c_lega.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_lega),size -1);
cf->Write((void *)&c,sizeof (c));

// Save ricetta
size = nomeRicetta.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)nomeRicetta),size -1);
cf->Write((void *)&c,sizeof (c));

// Save c_stato - c_cliente
size = c_cliente.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_cliente),size -1);
cf->Write((void *)&c,sizeof (c));


// Save c_bolla -> diventata c_densita
size = c_densita.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_densita),size -1);
cf->Write((void *)&c,sizeof (c));

// Save impianto
size = c_impianto.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_impianto),size -1);
cf->Write((void *)&c,sizeof (c));

// Save freeNote
size = c_freeNote.GetLength() + 1; // insert EOS
c = '\0';
cf->Write((void *)&size,sizeof(int));
cf->Write((void *)((LPCSTR)c_freeNote),size -1);
cf->Write((void *)&c,sizeof (c));

// Save c_startTime
cf->Write((void *)&c_startTime,sizeof(c_startTime));


// save c_spessore c_larghezza
cf->Write((void *)&c_spessore,sizeof(c_spessore));
cf->Write((void *)&c_larghezza,sizeof(c_larghezza));

// save c_offsetSx c_posDiaframma
cf->Write((void *)&c_offsetSx,sizeof(c_offsetSx));
cf->Write((void *)&c_posDiaframma,sizeof(c_posDiaframma));
cf->Write((void *)&c_posDiaframmaSx,sizeof(c_posDiaframmaSx));
cf->Write((void *)&c_posDiaframmaDx,sizeof(c_posDiaframmaDx));
cf->Write((void *)&c_trimLeft,sizeof(c_trimLeft));
cf->Write((void *)&c_trimRight,sizeof(c_trimRight));

// Dati System Dependent
cf->Write((void *)&c_fattCorrDimLin,sizeof(c_fattCorrDimLin));
cf->Write((void *)&c_lunResStop,sizeof(c_lunResStop));

// modo di acquisizione 
cf->Write((void *)&c_mode,sizeof(c_mode));

// Dati System Dependent soglie di riconoscimento
cf->Write((void *)&sogliaA,sizeof(sogliaA));
cf->Write((void *)&sogliaB,sizeof(sogliaB));
cf->Write((void *)&sogliaC,sizeof(sogliaC));
cf->Write((void *)&sogliaD,sizeof(sogliaD));
cf->Write((void *)&c_sogliaPer,sizeof(c_sogliaPer));
cf->Write((void *)&c_densityCalcLength,sizeof(c_densityCalcLength));

// Ver.0.72 aggiunto save impostazione .ini c_rightAlignStrip per aprire entrambi tipi di file 
// destri e sinistri a partire da 
// #define FILEDOCVER	7 
cf->Write((void *)&c_rightAlignStrip,sizeof(c_rightAlignStrip));

// Ver.1.03 aggiunto save filterParam FILEDOCVER 8
c_filterParam.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();

// Save Difetti
c_difCoil.save(cf);
// Save Difetti
c_markerCoil.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();

// Save Allarmi
densitaAlarm.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();
periodAlarm.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();
systemAlarm.save(cf);

if (progress != NULL)
	progress->StepIt();
// save settore bobine e nastri
c_rotoloMapping.save(cf);

// c_valZoneNumberSize[MAX_NUMSTRIP]
cf->Write((void *)c_valZoneNumberSize,sizeof(int) * MAX_NUMSTRIP);

// save report 100 metri
// Update Progress
//if (progress != NULL)
//	progress->StepIt();
//dif100.save(cf);

// Update Progress
if (progress != NULL)
	progress->StepIt();
return TRUE;
}

BOOL CLineDoc::load(CFileBpe *cf,CProgressCtrl *progress)
{
// Tentativo dati caricati da file
loadedData = FALSE;

this;

char idFileType[30];

// Update Progress
if (progress != NULL)
	progress->StepIt();

// Load ID
cf->Read((void *)idFileType,sizeof(FILEDOCTYPE));
if (strcmp(idFileType,FILEDOCTYPE))
	{
	CString s;
	s.Format ("Load: Wrong File Format");
	AfxGetMainWnd()->MessageBox(s,"Error");
	return (FALSE);
	}

int ver; 
// Load Ver
cf->Read((void *)&ver,sizeof(int));
if (ver > FILEDOCVER)
	{
	CString s;
	s.Format ("Load: Wrong File Version (found %d, need %d",ver,FILEDOCVER);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return (FALSE);
	}


if (ver >= 2)
	cf->Read((void *)&c_exeVer,sizeof(c_exeVer));
else
	c_exeVer = VERSIONE;

	
// Load	c_rotolo
int size;
cf->Read((void *)&size,sizeof(int));
LPTSTR pc;
pc = c_rotolo.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}
cf->Read((void *)pc,size);
c_rotolo.ReleaseBuffer();
//------------ End load c_rotolo

// Load	c_rotolo2
cf->Read((void *)&size,sizeof(int));
pc = c_rotolo2.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}
cf->Read((void *)pc,size);
c_rotolo2.ReleaseBuffer();
//------------ End load c_rotolo

// Load	c_rotoloExt
cf->Read((void *)&size,sizeof(int));
pc = c_side.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}
cf->Read((void *)pc,size);
c_side.ReleaseBuffer();
//------------ End load c_rotoloExt

// Load	c_rotoloRil
cf->Read((void *)&c_rotoloRil,sizeof(c_rotoloRil));
//------------ End load c_rotoloRil


// Load c_firstRising	
cf->Read((void *)&size,sizeof(int));
pc = c_firstRising.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}
cf->Read((void *)pc,size);
c_firstRising.ReleaseBuffer();
//------------ End load c_firstRising

// Load	c_lega
cf->Read((void *)&size,sizeof(int));
pc = c_lega.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}

cf->Read((void *)pc,size);
c_lega.ReleaseBuffer();
//------------------- End load c_lega

// Load	nomeRicetta
cf->Read((void *)&size,sizeof(int));
pc = nomeRicetta.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}

cf->Read((void *)pc,size);
nomeRicetta.ReleaseBuffer();
//------------------- End load nomeRicetta


// Load	c_stato diventato cliente
cf->Read((void *)&size,sizeof(int));
pc = c_cliente.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}

cf->Read((void *)pc,size);
c_cliente.ReleaseBuffer();
//------------------- End load c_stato

// Load	bolla -> densita
cf->Read((void *)&size,sizeof(int));
pc = c_densita.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}

cf->Read((void *)pc,size);
c_densita.ReleaseBuffer();
//------------------- End load bolla

// Load	impianto
cf->Read((void *)&size,sizeof(int));
pc = c_impianto.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}
cf->Read((void *)pc,size);
c_impianto.ReleaseBuffer();
//------------------- End load impianto


// Load	freeNote
cf->Read((void *)&size,sizeof(int));
pc = c_freeNote.GetBuffer(size+1);
if (pc == NULL)
	{
	CString s;
	s.Format ("Load File Alloc Failed on size %d",size);
	AfxGetMainWnd()->MessageBox(s,"Error");
	return FALSE;
	}
cf->Read((void *)pc,size);
c_freeNote.ReleaseBuffer();
//------------------- End load freeNote



// Load c_startTime
cf->Read((void *)&c_startTime,sizeof(c_startTime));

// Load c_spessore e c_larghezza
cf->Read((void *)&c_spessore,sizeof(c_spessore));
cf->Read((void *)&c_larghezza,sizeof(c_larghezza));

// save c_offsetSx c_posDiaframma
cf->Read((void *)&c_offsetSx,sizeof(c_offsetSx));
cf->Read((void *)&c_posDiaframma,sizeof(c_posDiaframma));

if (ver >= 4)
	{
	cf->Read((void *)&c_posDiaframmaSx,sizeof(c_posDiaframmaSx));
	cf->Read((void *)&c_posDiaframmaDx,sizeof(c_posDiaframmaDx));
	cf->Read((void *)&this->c_trimLeft,sizeof(c_trimLeft));
	cf->Read((void *)&this->c_trimRight,sizeof(c_trimRight));
	}
else
	{
	c_posDiaframmaSx = 30;
	c_posDiaframmaDx = 10;
	c_trimLeft = 10;
	c_trimRight = 10;
	}

// Dati System Dependent
cf->Read((void *)&c_fattCorrDimLin,sizeof(c_fattCorrDimLin));
cf->Read((void *)&c_lunResStop,sizeof(c_lunResStop));
cf->Read((void *)&c_mode,sizeof(c_mode));

// Dati System Dependent soglie di riconoscimento
cf->Read((void *)&sogliaA,sizeof(sogliaA));
cf->Read((void *)&sogliaB,sizeof(sogliaB));
cf->Read((void *)&sogliaC,sizeof(sogliaC));
cf->Read((void *)&sogliaD,sizeof(sogliaD));	

if (ver >= 2)
	{
	cf->Read((void *)&c_sogliaPer,sizeof(c_sogliaPer));	
	cf->Read((void *)&c_densityCalcLength,sizeof(c_densityCalcLength));
	}
else
	{
	c_sogliaPer = -1;
	c_densityCalcLength = 1000;
	}
// Ver.0.72 aggiunto save impostazione .ini c_rightAlignStrip per aprire entrambi tipi di file 
// destri e sinistri a partire da 
// #define FILEDOCVER	7 
if (ver >= 7)
	{
	cf->Read((void *)&c_rightAlignStrip,sizeof(c_rightAlignStrip));
	}
else
	{
	// carica da .ini allinea strip su bordo destro
	CProfile profile;
	c_rightAlignStrip = profile.getProfileBool(_T("init"), _T("AlignStripOnRightBorder"),FALSE);
	}	

if (ver >= 8)
	c_filterParam.load(cf,ver);
else
	c_filterParam.clear();

// Update Progress
if (progress != NULL)
	progress->StepIt();

// Load schede
if (c_difCoil.load(cf,ver)== FALSE)
//if (schede.load(cf)== FALSE)
	return(FALSE);	// Fail

// Load marker
if (c_markerCoil.load(cf,ver)== FALSE)
	return(FALSE);	// Fail



// Update Progress
if (progress != NULL)
	progress->StepIt();
// Load Allarmi
if (densitaAlarm.load(cf) == FALSE)
	return FALSE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
if (periodAlarm.load(cf) == FALSE)
	return FALSE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
if (systemAlarm.load(cf) == FALSE)
	return FALSE;

// Update Progress
if (progress != NULL)
	progress->StepIt();
// load settore bobine e nastri
if (!c_rotoloMapping.load(cf,ver))
	return FALSE;
// load report 100 metri
// Scommentare 
//if (dif100.load(cf,ver) == FALSE)
//	return FALSE;

if (ver >= 5)
// c_valZoneNumberSize[MAX_NUMSTRIP]
	cf->Read((void *)c_valZoneNumberSize,sizeof(int) * MAX_NUMSTRIP);

// Update Progress
if (progress != NULL)
	progress->StepIt();
// Idati sono stati caricati da file
loadedData = TRUE;
UpdateAllViews(NULL);
return TRUE;
}


void CLineDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLineDoc commands

int cn = 0;

void CLineDoc::OnTestgrafico() 
{
#ifndef Csm20El2013_REPORT

// TODO: Add your command handler code here

if (!systemOnLine)
	return;

AfxGetMainWnd()->SetFocus();


CLineDoc *pDocTop = ((CCSMApp*)AfxGetApp())->GetDocumentTop();
if (pDocTop)
	{
	pDocTop->HoleGenerator();
	pDocTop->MarkerGenerator();
	}
CLineDoc *pDocBot = ((CCSMApp*)AfxGetApp())->GetDocumentBottom();
if (pDocBot)
	{
	pDocBot->HoleGenerator();
	pDocBot->MarkerGenerator();
	}
#endif
return;

}

void CLineDoc::MarkerGenerator(void) 
{
int defPres = 0;

if (!systemOnLine)
	return;

if (randMarker.pos < 0)
	{
	randMarker.pos = rand() % c_holeRate;
	randMarker.scheda = rand() % c_markerCoil.getNumSchede();
	//randMarker.scheda = 1;
	// randMarker.classe =  rand() % c_markerCoil.getNumClassi();
	randMarker.classe = 0;
	}
else
	{
	DifMetro difMetro;
	int pos = (int ) c_markerCoil.getMeter();
	pos += (int)c_markerCoil.getMeterBaseStepX();
	if (pos >= randMarker.pos)
		{
		DifBanda difBanda1;
		for (int j=0;j<c_markerCoil.getNumClassi();j++)
			{
			if ((j == randMarker.classe)&&
				(j == c_holeClassMarking))
				{
				defPres = 1;
				int v;
				v = 1;
				difBanda1.Add(v,j);
				}
			else
				{
				int v;
				v = 0;
				difBanda1.Add(v,j);
				}
			}
		difMetro.clear();
		for (int i=0;i<c_markerCoil.getNumSchede();i++)
			{
			if (i == randMarker.scheda)
				{
				// if (i < 2)
					{
					defPres += 1; 
					difBanda1.setBanda(i);
					difMetro.addModify(difBanda1);
					}
				}
			}					
		difMetro.posizione = pos; 
		
		randMarker.pos+= c_holeRate;
		randMarker.scheda = rand() % c_markerCoil.getNumSchede();
		randMarker.scheda = 1;
		randMarker.classe = rand() % c_markerCoil.getNumClassi();
		// randMarker.classe = 0;
		}
	if (defPres >= 1)
		{
		difMetro.calcTotalCode(TRUE,0x1f);
		c_markerCoil.Add(difMetro,0x1f);
		}
	c_markerCoil.setMeter(pos);
	// Chiamare una funzione per il calcolo delle regioni 
	// da aggiornare

	// 1 solo aggiornamento
	// 0 repaint all
	// vedere funzioni OnUpdate
	// di CView
	UpdateAllViews(NULL,1);
	}

return;
}



// celle
int stripPosList[4][6]={{20,80,160,190,-1,-1},{20,80,100,130,-1,-1},
						{66,77,110,141,161,181},{66,77,110,171,-1,-1}
						};
int stripPosList2[4][6]={{80,160,-1,-1,-1,-1},{82,130,-1,-1,-1,-1},
						{66,77,110,141,161,181},{66,77,110,171,-1,-1}
						};
int stripIndex = 0;

// mm
int simDiaframmaSx [] = {0,140};
int simDiaframmaDx [] = {0,140};

void CLineDoc::HoleGenerator(void) 
{
BOOL defPres = FALSE;
BOOL def252Present;
// set fingerList CArray size as numSchede


if (!systemOnLine)
	return;

def252Present = FALSE;


if (randDif.pos < 0)
	{
	randDif.pos = rand() % c_holeRate;
	randDif.scheda = rand() % c_difCoil.getNumSchede();
	// randDif.scheda = 10;
	randDif.classe = rand() % c_difCoil.getNumClassi();
	randDif.big = 7;
	}
else
	{
	// simulazione pacchetto doppio
	DifMetro difMetro1,difMetro2;
	int pos = (int ) c_difCoil.getMeter();
	pos += (int)c_difCoil.getMeterBaseStepX();
	if (pos >= randDif.pos)
		{
		DifBanda difBanda1;
		for (int j=0;j<c_difCoil.getNumClassi();j++)
			{
			// j == 0 classe A
			// inserito uso skip
			if (j == 0)
				{
				if (c_skipHoleAInspection)
					{
					defPres = FALSE;
					int v;
					v = 0;
					difBanda1.Add(v,j);
					continue;
					}
				}

			if (j == randDif.classe)
				{
				defPres = TRUE;
				int v;
				v = 60;
				difBanda1.Add(v,j);

				}
			else
				{
				int v;
				v =3;
				difBanda1.AddBigHole(randDif.big);
				difBanda1.Add(v,j);
				}
			}
		
		difMetro1.clear();
		difMetro2.clear();
		
		//if (pos < 50)
			{
	//		c_posDiaframmaDx = 1. * SIZE_BANDE * c_difCoil.getNumSchede() - (double ) simDiaframmaDx [0];
	//		c_posDiaframmaSx = simDiaframmaSx [0];
			}
		//if (pos >= 50)
		//	{
		//	c_posDiaframmaDx = 1. * SIZE_BANDE * c_difCoil.getNumSchede() - (double ) simDiaframmaDx [1];
		//	c_posDiaframmaSx = simDiaframmaSx [1];
		//	}
		
		if (pos <= 100)
			stripIndex = 0;
		if (pos > 100)
			stripIndex = 1;
		if (pos > 200)
			stripIndex = 2;
		if (pos > 300)
			stripIndex = 3;


		for (int i=0;i<c_difCoil.getNumSchede();i++)
			{
			// disable fingerList 
			int vAClass = 252;
			int vAClass2 = 252;
			if ((stripPosList[stripIndex][0] >=0)&&
				(stripPosList[stripIndex][1] >=0)&&
				((i >= stripPosList[stripIndex][0])&&(i < stripPosList[stripIndex][1])))
				vAClass = 0;
			if ((stripPosList[stripIndex][2] >=0)&&
				(stripPosList[stripIndex][3] >=0)&&
				((i >= stripPosList[stripIndex][2])&&(i < stripPosList[stripIndex][3])))
				vAClass = 0;
			if ((stripPosList[stripIndex][4] >=0)&&
				(stripPosList[stripIndex][5] >=0)&&
				((i >= stripPosList[stripIndex][4])&&(i < stripPosList[stripIndex][5])))
				vAClass = 0;
			// secondo coil
			if ((stripPosList2[stripIndex][0] >=0)&&
				(stripPosList2[stripIndex][1] >=0)&&
				((i >= stripPosList2[stripIndex][0])&&(i < stripPosList2[stripIndex][1])))
				vAClass2 = 0;
			if ((stripPosList2[stripIndex][2] >=0)&&
				(stripPosList2[stripIndex][3] >=0)&&
				((i >= stripPosList2[stripIndex][2])&&(i < stripPosList2[stripIndex][3])))
				vAClass2 = 0;
			if ((stripPosList2[stripIndex][4] >=0)&&
				(stripPosList2[stripIndex][5] >=0)&&
				((i >= stripPosList2[stripIndex][4])&&(i < stripPosList2[stripIndex][5])))
				vAClass2 = 0;

			BOOL outDiaframma = c_useDiaframmi && ((i <= (c_posDiaframmaSx/SIZE_BANDE)) || 
						(i >= ((c_posDiaframmaDx/SIZE_BANDE)-1)));

			if(!c_useFingerHw)
				{
				if ((vAClass == 252)||outDiaframma)
					{// banda sotto finger
					c_tmpFingerList[i]=1;
					def252Present = TRUE;
					}
				else
					{// banda libera
					c_tmpFingerList[i]=0;
					}		

				if (vAClass2 == 252)
					{// banda sotto finger
					c_tmpFingerList2[i]=1;
					def252Present = TRUE;
					}
				else
					{// banda libera
					c_tmpFingerList2[i]=0;
					}
				
				if ((c_mode == 1) &&
					(c_fingerList  [i] == 1)&&
					(c_fingerList2 [i] == 1))
					continue;

				if ((c_mode == 0)&&
					(c_fingerList[i]==1))
					continue;

				}
			if (i == randDif.scheda)
				{
				difBanda1.setBanda(i);
				if (i < 112)
					{
					difMetro1.addModify(difBanda1);
					difMetro2.addModify(difBanda1);
					}
				else
					difMetro2.addModify(difBanda1);
				}
			}	
		
		difMetro1.posizione = pos; 
		difMetro2.posizione = pos; 
		
		// randDif.pos+= rand() % c_holeRate;
		randDif.pos +=  c_holeRate;
		randDif.scheda = rand() % (min(c_difCoil.getNumSchede(),160));
		//randDif.scheda = 10;
		randDif.classe = rand() % c_difCoil.getNumClassi();
		randDif.classe = 0;
		randDif.big = ((rand() % 10) == 0);
		}

	if (defPres)
		{
		// pre calcolo  totali per classe 
		difMetro1.calcTotalCode(TRUE,0x1f);
		c_difCoil.Add(difMetro1,0x1f);
		difMetro2.calcTotalCode(TRUE,0x1f);
		c_difCoil.setBaseSplitSchede(112);
		c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro2,0x1f);
		}
	c_difCoil.setMeter(pos);
	// Chiamare una funzione per il calcolo delle regioni 
	// da aggiornare

	// 1 solo aggiornamento
	// 0 repaint all
	// vedere funzioni OnUpdate
	// di CView
	// check if visible
	{
	CMainFrame *mFrame = (CMainFrame *)AfxGetMainWnd();
	POSITION pos = GetFirstViewPosition();
	CView* pView = GetNextView(pos);
	CFrameWnd* wFrame = (CFrameWnd*) pView->GetParent();

	if (wFrame == mFrame->MDIGetActive(NULL))
		{
		UpdateAllViews(NULL,1);
		}
	}
	}
updateDifAlarm();

if (def252Present)
	{
	c_fingerList.Copy(c_tmpFingerList);
	c_fingerList2.Copy(c_tmpFingerList2);
	tagliSelect(TRUE,FALSE);	
	}

return;
}



void CLineDoc::OnViewAggiorna() 
{
	// TODO: Add your command handler code here
UpdateBaseDoc();	
UpdateAllViews(NULL);
}

//------------------------------------
//
//	Gestione Messagi tipo c_stato HDE
//
//------------------------------------
 
void CLineDoc::gestValStatoHde (PVOID p,DWORD size,
							BOOL fromTop) // Gestisce arrivo VAL_STATO
{

// Received Msg from serial
serMsgReceived = TRUE;

if (size != 1)
	{// errore dimensione 
	return;
	}

int nuovoStato;
nuovoStato = (int ) *((unsigned char *) p);


BOOL genMsg = FALSE;
if ((nuovoStato == HDE_HOLD)&&
	( getStatoHde(fromTop) == HDE_INSPECTION)&&
	systemOnLine)
	{
	if (!systemOnLine)
	return;

	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_HOLDENTER);
	// allarmeSistema.Format("Pausa IN\r\n");
	allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	// eliminata vis finestra su pause 
	// genMsg = TRUE;
	}

if ((nuovoStato == HDE_INSPECTION)&&
	(getStatoHde(fromTop) == HDE_HOLD)&&
	systemOnLine)
	{
	CString allarmeSistema;
	CString s;
	s.LoadString(CSM_GRAPHDOC_HOLDEXIT);
	// allarmeSistema.Format("Pausa OUT\r\n");
	allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
	systemAlarm.appendMsg(allarmeSistema);
	// eliminata vis finestra su pause 	
	// genMsg = TRUE;
	}
int st = getStatoHde(fromTop);
// Gestione test teste in ispezione
if (systemOnLine&&
	(
	((nuovoStato == HDE_INSPECTION)&&
	(getStatoHde(fromTop) == HDE_INSPECTION))
	// && ((c_mode == 0)||(c_mode == 1)))
	||

	((c_mode == 2) && (nuovoStato == HDE_WAITSTART) && 
	(getStatoHde(fromTop) == HDE_INSPECTION))||
	
	((c_mode == 2) && (nuovoStato == HDE_WARMUP) && 
	(getStatoHde(fromTop) == HDE_INSPECTION))
	))
	{// siamo in inserzione da almeno 5 sec tutte e due le macchine devono essere partite
	if (getStatoHde(!fromTop) != getStatoHde(fromTop))
//		((getStatoHde(!fromTop) == HDE_WAITSTART)||
//		(getStatoHde(!fromTop) == HDE_WAITENABLE)||
//		(getStatoHde(!fromTop) == HDE_WARMUP)))
		// altra testa offline
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_MISALIGNED_HEAD);
		// s = "Misaligned HEAD start";
		allarmeSistema.Format ("m. %d: %s\r\n",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		sendAlMisaligned (1);
		// send bottom
		// doc->sendAlMisaligned (1);
		// Disable Micro
		sendStartStop (FALSE,FALSE);
		if (c_mode == 0)
			{
			// Both Process
			FileSaveStop(TRUE); 
			}
		else
			{// modo 1-singleCoil e 2-singleSheet
			// only first Process
			FileSaveStop(FALSE); 
			}
		genMsg = TRUE;
		}
	}

if (genMsg)
	systemAlarm.visualizza(AfxGetMainWnd(),secAlarmSound);
	
setStatoHde(nuovoStato,fromTop);
}

//------------------------------------
//
//	Gestione Messagi tipo ValExtCounter
// Estesi per essere divisi su piu pacchetti
// Ora sono 4 pacchetti:
// offset 0 -> difetti A	(1 byte x cella )
// offset 1 -> difetti B	(1 byte x cella )
// offset 2 -> difetti C	(4 bit x cella )
// offset 3 -> difetti D	(4 bit x cella )
// d-m-c-s-o1-o2-m1,m2,m3,m4 
// formato pacchetti dmcs + 2 byte offset + 4 byte metro
//------------------------------------

// nel caso alternato arrivano due pacchetti da due teste 
// che vanno nello stesso doc, bisogna fare il merge delle bande
 

void CLineDoc::gestValExtCounter(PVOID p, DWORD size,BOOL fromBottom)
{
// Offset nel vettore difetti di qs gruppo di dati
short offset;
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

// Received Msg from serial
serMsgReceived = TRUE;

// Only system Enabled
if (!systemOnLine)
	return;

unsigned char  *cDest;
unsigned short *sDest;
unsigned long  *lDest;
cDest = (unsigned char *) p; 

// impostazione offset
short sOff;
sOff = (*((short *)cDest));
_swab ((char *)&sOff,(char *)&offset,2);

cDest += sizeof(short);

long sPos,dPos;
sPos = *((long *)cDest); 
swaplong (&sPos,&dPos,1);

cDest += sizeof(long);

// incremento pointer
sDest = (unsigned short *) cDest;
lDest = (unsigned long *)  cDest;

int nCl = min (c_difCoil.getNumClassi(),4);
int numField = (size-sizeof(long)-2)/c_sizeClassi; // erano campi long ora sono parametrizzati

if (offset > 1)
	// C e D solo 4 bit
	numField *= 2;
// 
// size classi sempre 1
// 

// Formato su 1 mt
// Offset,Posiz,ValA[0],ValA[1].ValA[2]...., ValA[6] .....;
//
// Nuovo protocollo 4 pacchetti 1xA 1xB 1xC 1xD

// Calcolo posizione corretta da lResStop
double corrPos;
corrPos = c_lunResStop + dPos;
corrPos += c_baseContinuaRotolo;
if (dPos < 0)
	{
	if (offset == 0)
		// difetti a
		corrPos = c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX();
	else
		corrPos = c_difCoil.getMeter();
	}


int lastIndex;

lastIndex = min (c_difCoil.getNumSchede(),numField);
	
DifMetro difMetro;
DifBanda difBanda;
DifMetro difMetroMarker;
DifBanda difBandaMarker;
BOOL bandaDefPres = FALSE;
BOOL metroDefPres = FALSE;
// flag received packet with stripMarker
BOOL dif252Found = FALSE;

// Create new metro or update existing
#ifndef _DEBUG
if (((corrPos==0) || (c_difCoil.getMeter()==(int)corrPos))&&
	(c_difCoil.GetSize()>0)&&
	(c_difCoil.GetAt(c_difCoil.GetUpperBound()).posizione == (int)corrPos))
#else
if ((dPos <0)&&(offset != 0)&&(c_difCoil.GetSize()>0))
#endif
	{
	difMetro = c_difCoil.GetAt(c_difCoil.GetUpperBound());
	difMetro.clearBandaMod();
	difMetroMarker = c_markerCoil.GetAt(c_difCoil.GetUpperBound());
	difMetroMarker.clearBandaMod();
	}
// non esiste + il concetto di halfHead
// prepariamo intervalli di strip


// Aggiornamento finger con pos diaframmi anche con metri senza difetti
if ((numField == 0)&&
	(!c_useFingerHw))
	{	
	FingerList* pTmpFingerList;
	FingerList* pFingerList;
	double tmpPosDiaframmaSx;
	double tmpPosDiaframmaDx;
	BOOL outDiaframma=FALSE;
	if ((c_mode ==1) && fromBottom)
		{
		pTmpFingerList = &c_tmpFingerList2;
		pFingerList = &c_fingerList2;
		tmpPosDiaframmaSx = c_posDiaframmaSx2;
		tmpPosDiaframmaDx = c_posDiaframmaDx2;
		}
	else
		{// modo 0-separatore e 2-singleSheet
		pTmpFingerList = &c_tmpFingerList;
		pFingerList = &c_fingerList;
		tmpPosDiaframmaSx = c_posDiaframmaSx;
		tmpPosDiaframmaDx = c_posDiaframmaDx;
		}

	// quando arriva pacchetto vuoto non e` possibile
	// ricavare posizione mezza testa onLine.
	// uso 52 * c_sizeclassi  
	// 52 224/2 inizio seconda mezza testa
	// int virtualFirstIndex = (offset==0)? 0 : (halfHead/(c_sizeClassi*nCl)); 
	//int virtualLastIndex =  (offset==0)? (halfHead/(c_sizeClassi*nCl)) : c_difCoil.getNumSchede();
		
	int virtualFirstIndex = 0; 
	int virtualLastIndex =  c_difCoil.getNumSchede();
	
	for (int i=virtualFirstIndex;i<virtualLastIndex;i++)
		{
		if (i > pTmpFingerList->GetSize())
			continue;	

		// modo single coil finger spessore 0 No separazione
		// perche` and con altro strip non presenta discontinuita`
		// 13-10-2005 
		// era > e < stretto +-2
		// inserita possibilita` di lavorare senza diaframmi
		outDiaframma = c_useDiaframmi && 
						((i <= ((tmpPosDiaframmaSx/SIZE_BANDE)-1)) || 
						 (i >= ((tmpPosDiaframmaDx/SIZE_BANDE)+2)));

		if (outDiaframma)
			{// banda sotto finger
			dif252Found = TRUE;
			pTmpFingerList->SetAt(i,1);
			}
		else
			{// banda libera
			pTmpFingerList->SetAt(i,0);
			}
		}
	}

// Codice eseguito solo se esistono difetti
// per ogni byte nel pacchetto 
// per offset 2 e 3 lastIndex = numfield = numero campi = 2*numbyte
for (int scIndex = 0;scIndex<lastIndex;scIndex ++)
	{
	difBanda.clear();
	difBandaMarker.clear();
// Csm20El2013 porting
	long trueIndex = scIndex;	// scIndex sempre = numero bande

	if (trueIndex >= c_difCoil.getNumSchede())
		break;
	// uso diaframmi anche con finger Hw
	BOOL outDiaframma=FALSE;
#ifdef MASK_DIAF
	if ((c_mode ==1) && fromBottom)
		{
		// inserita possibilita` di lavorare senza diaframmi
		outDiaframma   = c_useDiaframmi &&
					((trueIndex <= ((c_posDiaframmaSx2/SIZE_BANDE)-1))|| 
					 (trueIndex >= ((c_posDiaframmaDx2/SIZE_BANDE)+0)));
		}
	else
		{// modo 0-separatore e 2-singleSheet
		// inserita possibilita` di lavorare senza diaframmi
		outDiaframma = c_useDiaframmi &&
				((trueIndex <= ((c_posDiaframmaSx/SIZE_BANDE)-1))|| 
				 (trueIndex >= ((c_posDiaframmaDx/SIZE_BANDE)+0)));
		}
#endif
	int markerIndex[2];
	// una cella puo` stare in 2 strip
	markerIndex[0] = -1;
	markerIndex[1] = -1;
	int mi = 0;
	// for (int i=0;(i<c_stripList.GetSize()) && (mi < 2);i++)
	
	for (int i=(c_rightAlignStrip?c_stripList.GetSize()-1:0);
		(c_rightAlignStrip?i>=0:(i<c_stripList.GetSize())) && (mi < 2);
		c_rightAlignStrip?i--:i++)
		{
		if (c_stripList[i].ElementAt(trueIndex) == 1)
			{// found
			markerIndex[mi++] = i;
			if (mi > 1)
			  break;
			}
		}

	if (markerIndex[0] >= 0)
		{// preparo marker Index 0
		difBandaMarker = difMetroMarker.getDifBanda(markerIndex[0]);
		difBandaMarker.clearMod();
		difBandaMarker.setBanda(markerIndex[0]);
		difBandaMarker.setNumClassi(nCl);
		}

	difBanda = difMetro.getDifBanda(trueIndex);
	difBanda.clearMod();
	difBanda.setBanda(trueIndex);
	difBanda.setNumClassi(nCl);

	bandaDefPres = FALSE;
	int vAClass=0;

	int v=0;
	if (offset <=1)
		v = (int ) *cDest;
	else
		// indici pari + significativi!
		v = (int ) (0x0f & ((*cDest) >> ((1-(scIndex%2))*4)));
	// j == 0 classe A
	// inserito uso skip
	if (offset == 0)
		{
		if (c_skipHoleAInspection)
			{
			// clear A def
			if (v != 252) v = 0;
			}
		vAClass = v;
		}
	// se 252 numero difetti classe A allora 
	// banda sotto finger memo numDif classe A
	if ((v)&&(v != 252))	
		bandaDefPres = TRUE;
	
	// Periodici
	if (offset == 3)
		{
		// difetti tipo D
		if (v > c_sogliaPer)
			{
			// non + necessaria consequenzialita`
			//if (c_classBFilterList[scIndex])
				difBanda.SetPerB(1);
			c_classBFilterList[scIndex] = 1;
			}
		else
			c_classBFilterList[scIndex] = 0;
		}

	if (v != 252)
		{
		difBanda.Set(offset,v);
		}
	else
		{
		difBanda.Set(offset,0);
		difBandaMarker.Set(offset,0);
		}

	FingerList* pTmpFingerList=NULL;
	FingerList* pFingerList=NULL;
	if (!c_useFingerHw)
		{
		BOOL outDiaframma=FALSE;
		if ((c_mode ==1) && fromBottom)
			{
			pTmpFingerList = &c_tmpFingerList2;
			pFingerList    = &c_fingerList2;
			// inserita possibilita` di lavorare senza diaframmi
			outDiaframma   = c_useDiaframmi &&
						((trueIndex <= (((c_posDiaframmaSx2+c_trimLeft)/SIZE_BANDE)-1))|| 
						 (trueIndex >= (((c_posDiaframmaDx2-c_trimRight)/SIZE_BANDE)+0)));
			}
		else
			{// modo 0-separatore e 2-singleSheet
			pTmpFingerList = &c_tmpFingerList;
			pFingerList = &c_fingerList;
			// inserita possibilita` di lavorare senza diaframmi
			outDiaframma = c_useDiaframmi &&
					((trueIndex <= (((c_posDiaframmaSx+c_trimLeft)/SIZE_BANDE)-1))|| 
					 (trueIndex >= (((c_posDiaframmaDx-c_trimRight)/SIZE_BANDE)+0)));
			}
		if ((vAClass == 252)||outDiaframma)
			{// banda sotto finger
			dif252Found = TRUE;
			pTmpFingerList->SetAt(trueIndex,1);
			}
		else
			{// banda libera
			pTmpFingerList->SetAt(trueIndex,0);
			}
		// Fine settings finger automatici
		}
	if (bandaDefPres)
		{
		// check se banda sotto finger
		// disable fingerList 
		//if ((!c_useFingerHw)&&
		//	(c_useFingerList))
		if (!c_useFingerHw)
			{
			if (pFingerList->GetAt(trueIndex)==0)
				{// == 0 cella visibile
				metroDefPres = TRUE;
				// difMetro.Add(difBanda);
				difMetro.addModify(difBanda);
				difMetroMarker.addModify(difBandaMarker);
				if (markerIndex[1] >= 0)
					{// preparo marker Index 0
					difBandaMarker = difMetroMarker.getDifBanda(markerIndex[1]);
					difBandaMarker.clearMod();
					difBandaMarker.setBanda(markerIndex[1]);
					difBandaMarker.setNumClassi(nCl);
					difBandaMarker.Set(offset,v);
					difMetroMarker.addModify(difBandaMarker);
					}
				}
			}
		else
			{
			if(!outDiaframma)
				{// check se sotto diaframma anche per finger Hw
				metroDefPres = TRUE;
				// difMetro.Add(difBanda);
				difMetro.addModify(difBanda);
				difMetroMarker.addModify(difBandaMarker);
				if (markerIndex[1] >= 0)
					{// preparo marker Index 1 E INSERISCO
					difBandaMarker = difMetroMarker.getDifBanda(markerIndex[1]);
					difBandaMarker.clearMod();
					difBandaMarker.setBanda(markerIndex[1]);
					difBandaMarker.setNumClassi(nCl);
					difBandaMarker.Set(offset,v);
					difMetroMarker.addModify(difBandaMarker);
					}
				}
			}
		}
	// per pacchetti 3 e 4 1/2 byte alla volta
	if ((offset < 2)||
		(scIndex % 2))
		cDest ++; // al piu` 4 Classi
	}


if (metroDefPres)
	{
	if (dPos >= 0)
		{
		difMetro.posizione = (int ) corrPos;
		difMetroMarker.posizione = (int ) corrPos;
		}
	else
		{// incremento automatico, 
		//	lresStop tenuto conto in inizializzazione
		// if (offset == 0)
			difMetro.posizione = (int)c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX();
			difMetroMarker.posizione = (int)c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX();
		}
	// CSM20D POrting skip difetti m < 20
	if (difMetro.posizione > 20)
		{
		// precalcolo totali per classe (true inizializzo counter)
		difMetro.calcTotalCode(TRUE,(0x01<<offset));
		difMetroMarker.calcTotalCode(TRUE,(0x01<<offset));
		if ((c_difCoil.GetSize() == 0)||
			((c_difCoil.GetAt(c_difCoil.GetUpperBound())).posizione != difMetro.posizione))
			{
			c_difCoil.Add(difMetro,(0x01<<offset));
			c_viewScroll = TRUE;
			c_markerCoil.Add(difMetroMarker,(0x01<<offset));
			}
		else
			{
			// calcolo poszione prima banda con totali da aggiornare  
			long baseIndex = 0;
			c_difCoil.setBaseSplitSchede(baseIndex);
			c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro,(0x01<<offset));
			c_markerCoil.SetAt(c_markerCoil.GetUpperBound(),difMetroMarker,(0x01<<offset));
			c_viewScroll = FALSE;
			}
		}
	} 
	
// Update Metraggio
if (dPos >= 0)
	{
	// Salto di metraggio ridisegno tutto
	int deltaP;
	deltaP = (int) corrPos - (int)c_difCoil.getMeter();
	deltaP -= (int)c_difCoil.getMeterBaseStepX();
	if (deltaP > 0)
		c_redrawAll = TRUE;
	c_difCoil.setMeter((int) corrPos);
	c_markerCoil.setMeter((int) corrPos);
	}
else
	{
	// incremento automatico, 
	//	lresStop tenuto conto in inizializzazione
	if (offset == 0)
		{
		c_difCoil.setMeter(c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX());
		c_markerCoil.setMeter(c_markerCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX());	
		}
	}

// Aggiornamento View
if(dif252Found)
	{// received strip marker
	if ((c_mode ==1) && fromBottom)
		{
		c_fingerList2.Copy(c_tmpFingerList2);
		}
	else
		{
		c_fingerList.Copy(c_tmpFingerList);
		c_redrawAll |= tagliSelect(TRUE,FALSE);
		}
	}

#ifndef _DEBUG
if (offset >= 3)
#endif
	c_redraw = TRUE;

}


// Gestione nuova modalita` di pacchetto formato:
// posTrasv,B,A posTrasv,B,A
// posTrasv,C,D posTrasv,C,D
// posTrasv,0,Big posTrasv,0,Big

// maschera sui diaframmi + trimLeft e trimRight
#define MASK_DIAF

// mask contiene 
// UPDATE_MASK_C|UPDATE_MASK_D
// oppure 
// UPDATE_MASK_A|UPDATE_MASK_B
// oppure 
// UPDATE_MASK_BIG

void CLineDoc::gestValCounter2(PVOID p, DWORD size,BOOL fromBottom,int mask)
{
// Received Msg from serial
serMsgReceived = TRUE;
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

// Only system Enabled
if (!systemOnLine)
	return;

unsigned char  *cDest;

cDest = (unsigned char *) p; 

long sPos,dPos;
sPos = *((long *)cDest); 
swaplong (&sPos,&dPos,1);

cDest += sizeof(long);

///Values
//long	progressive meter  counter
//unsigned char [0]	numero della prima cella 	con fori di tipo A, B, A e B 		range 1-208
//unsigned char [1]	I 4 bit pi� significativi = num fori di classe B 	range 0-15
//	I 4 bit meno significativi = num fori di classe A 	range 0-15
//Esempio 3 fori B e 7 fori A =  0011 0111  	
//unsigned char [2]	numero della seconda cella 	con fori di tipo A, B, A e B 	range 1-208
//unsigned char [3]	I 4 bit pi� significativi = num fori di classe B 	range 0-15
//	I 4 bit meno significativi = num fori di classe A 	range 0-15

// se from Bottom al numero di cella si somma c_difCoil.getNumSchedeLeft()

// Il pacchetto ora e` composto da:
// ... (long), char1,char2, char1,char2
// dove char 1 e` sempre posizione trasversale e char 2 due nibble 

int nCl = min (c_difCoil.getNumClassi(),4);
// erano campi long ora sono parametrizzati
int numField;

// numero di campi (coppie posTra-contatori) :
// numField = (size-sizeof(long)-2)/c_sizeClassi; 
numField = (size-sizeof(long)); 

// gestite solo sizeclassi == 1


// Formato su 1 mt
// PosizLong,posTra,ValA[0]+ValB[0],ValA[1],ValB[1],....., ValD[6] .....;
//

// Calcolo posizione corretta da lResStop
double corrPos;
corrPos = c_lunResStop + dPos;
	corrPos += c_baseContinuaRotolo;
	
DifMetro difMetro; // vuoto
DifBanda difBanda;
DifMetro difMetroMarker;
DifBanda difBandaMarker;

BOOL bandaDefPres = FALSE;
BOOL metroDefPres = FALSE;
// flag received packet with stripMarker
BOOL dif252Found = FALSE;

if (corrPos > c_difCoil.getMeter())
	{
	c_bitMaskPck = 0;
	}

// Create new metro or update existing
int existIndex = -1;
if (((corrPos==0) || (c_difCoil.getMeter()==(int)corrPos))&&
	(c_difCoil.GetSize()>0)&&
	(c_difCoil.GetAt(c_difCoil.GetUpperBound()).posizione == (int)corrPos))
	{
	difMetro = c_difCoil.GetAt(c_difCoil.GetUpperBound());
	difMetro.clearBandaMod();
	difMetroMarker = c_markerCoil.GetAt(c_difCoil.GetUpperBound());
	difMetroMarker.clearBandaMod();

	if (c_bitMaskPck & mask)
		{
#ifndef Csm20CA_REPORT
		// log
		//CString logStr;
		//logStr.Format("Posizione ripetuta A-B atteso %d - arrivato %d",
		//	(int)c_difCoil.getMeter(),(int)corrPos);
		//c_pLog->LogString(LOG_WARNING,logStr);
#endif
		}
	}
else
	{
	if (corrPos < c_difCoil.getMeter())
		{//metro gia arrivato
		int i = c_difCoil.GetUpperBound();
		while(i>= 0)
			{
			if (c_difCoil[i].posizione == (int)corrPos)
				{
				difMetro = c_difCoil.GetAt(c_difCoil.GetUpperBound());
				existIndex = i;
				break;
				}
			i--;
			}
#ifndef Csm20CA_REPORT
		// log
		//CString logStr;
		//logStr.Format("Posizione errata A-B atteso %d - arrivato %d",
		//	(int)c_difCoil.getMeter(),(int)corrPos);
		//c_pLog->LogString(LOG_WARNING,logStr);
#endif
		}
	}

c_bitMaskPck |= mask;

int baseClass = 0;

if (mask == (UPDATE_MASK_C | UPDATE_MASK_D))
	baseClass = 2;

if (mask == UPDATE_MASK_BIG)
	baseClass = 4;

int lastIndex = numField/2;

// Codice eseguito solo se esistono difetti
// ciclo da fare su size/2
for (int scIndex = 0;scIndex<lastIndex;scIndex ++)
	{
	// CSM20A posiz trasversale in pacchetto porting
	long trueIndex = *cDest;
	// se da bottom seconda parte foglio
	if (fromBottom)
		trueIndex += c_difCoil.getNumSchedeLeft();
	//-----------------------------------
	if (trueIndex >= c_difCoil.getNumSchede())
		break;

	int markerIndex[2];
	markerIndex[0] = -1;
	markerIndex[1] = -1;
	int mi = 0;
	for (int i=0;(i<c_stripList.GetSize()) && (mi < 2);i++)
		{
		if (c_stripList[i].ElementAt(trueIndex) == 1)
			{// found
			//if (pFrame->c_rightAlignStrip)
			//	markerIndex[mi++] = c_stripList.GetSize()-1-i;
			//else
				markerIndex[mi++] = i;
			if (mi > 1)
			  break;
			}
		}
	if (markerIndex[0] >= 0)
		{// preparo marker Index 0
		difBandaMarker = difMetroMarker.getDifBanda(markerIndex[0]);
		difBandaMarker.clearMod();
		difBandaMarker.setBanda(markerIndex[0]);
		difBandaMarker.setNumClassi(nCl);
		}


	difBanda = difMetro.getDifBanda(trueIndex);
	difBanda.clearMod();
	difBanda.setBanda(trueIndex);
	difBanda.setNumClassi(nCl);



	// uso diaframmi anche con finger Hw
	BOOL outDiaframma=FALSE;
#ifdef MASK_DIAF
	if ((c_mode ==1) && fromBottom)
		{
		// inserita possibilita` di lavorare senza diaframmi
		outDiaframma   = c_useDiaframmi &&
					((trueIndex <= (((c_posDiaframmaSx2+c_trimLeft)/SIZE_BANDE)-1))|| 
					 (trueIndex >= (((c_posDiaframmaDx2-c_trimRight)/SIZE_BANDE)+0)));
		}
	else
		{// modo 0-separatore e 2-singleSheet
		// inserita possibilita` di lavorare senza diaframmi
		outDiaframma = c_useDiaframmi &&
				((trueIndex <= (((c_posDiaframmaSx+c_trimLeft)/SIZE_BANDE)-1))|| 
				 (trueIndex >= (((c_posDiaframmaDx-c_trimRight)/SIZE_BANDE)+0)));
		}
#endif
	bandaDefPres = FALSE;
	int vAClass=0;
	// solo prime due classi
	for (int j=0;j<2;j++)
		{
		int v=0;
		int offset = baseClass + j;
		// nibble
		if (j==0)
			v = (int) ((*(cDest+1)) & 0x0f);
		else
			v = (int) ((*(cDest+1)>>4) &0x0f);

		// inserito uso skip
		if ((j+baseClass) == 0)
			{
			if (c_skipHoleAInspection)
				{
				// clear A def
				if (v != 252) v = 0;
				}
			vAClass = v;
			}
		// se 252 numero difetti classe A allora 
		// banda sotto finger memo numDif classe A
		if ((v)&&(v != 252))	
			bandaDefPres = TRUE;

		// Periodici
		if (offset == 3)
			{
			// difetti tipo D
			if (v > c_sogliaPer)
				{
				// non + necessaria consequenzialita`
				//if (c_classBFilterList[scIndex])
					difBanda.SetPerB(1);
				c_classBFilterList[scIndex] = 1;
				}
			else
				c_classBFilterList[scIndex] = 0;
			}

		if (baseClass < 4)
			{
			if (v != 252)
				{
				difBanda.Set(offset,v);
				difBandaMarker.Set(offset,v);		
				}
			else
				{
				difBanda.Set(offset,0);
				difBandaMarker.Set(offset,0);
				}
			}
		else
			{
			if (v) {difBanda.AddBigHole(v);difBandaMarker.AddBigHole(v);};
			}
		}
	// l'offset di posizione trasversale e` impostato da 224/2 == 112
	// long trueIndex = scIndex+offset*halfHead/(c_sizeClassi*nCl);
	// difBanda.setBanda(trueIndex);
	
	FingerList* pTmpFingerList=NULL;
	FingerList* pFingerList=NULL;
	if ((!c_useFingerHw)||
		(c_mode == 1))
		{
		BOOL outDiaframma=FALSE;
		if ((c_mode ==1) && fromBottom)
			{
			pTmpFingerList = &c_tmpFingerList2;
			pFingerList    = &c_fingerList2;
			// inserita possibilita` di lavorare senza diaframmi
			outDiaframma   = c_useDiaframmi &&
						((trueIndex < (((c_posDiaframmaSx2+c_trimLeft)/SIZE_BANDE)-1))|| 
						 (trueIndex > (((c_posDiaframmaDx2+c_trimRight)/SIZE_BANDE)+1)));
			}
		else
			{// modo 0-separatore e 2-singleSheet
			pTmpFingerList = &c_tmpFingerList;
			pFingerList = &c_fingerList;
			// inserita possibilita` di lavorare senza diaframmi
			outDiaframma = c_useDiaframmi &&
					((trueIndex <= (((c_posDiaframmaSx+c_trimLeft)/SIZE_BANDE)-1))|| 
					 (trueIndex >= (((c_posDiaframmaDx-c_trimRight)/SIZE_BANDE)+1)));
			}
		if (!c_useFingerHw)
			{
			if ((vAClass == 252)||outDiaframma)
				{// banda sotto finger
				dif252Found = TRUE;
				pTmpFingerList->SetAt(trueIndex,1);
				}
			else
				{// banda libera
				pTmpFingerList->SetAt(trueIndex,0);
				}
			}
		// Fine settings finger automatici
		}
	if (bandaDefPres)
		{
		// check se banda sotto finger
		// disable fingerList 
		//if ((!c_useFingerHw)&&
		//	(c_useFingerList))
		// modo alternato usa fingerlist
		if ((!c_useFingerHw)||
			(c_mode == 1))
			{
			if (pFingerList->GetAt(trueIndex)==0)
				{// == 0 cella visibile
				metroDefPres = TRUE;
				difMetro.addModify(difBanda);
				difMetroMarker.addModify(difBandaMarker);
				if (markerIndex[1] >= 0)
					{// preparo marker Index 0
					//DifBanda difBandaMarker2 = difMetroMarker.getDifBanda(markerIndex[1]);
					//difBandaMarker2.clearMod();
					// replico stessa difBanda ma con markerIndex diverso
					difBandaMarker.setBanda(markerIndex[1]);
					//difBandaMarker2.setNumClassi(nCl);
					//difBandaMarker2.Set(,);
					difMetroMarker.addModify(difBandaMarker);
					}
				}
			}
		else
			{
			if (!outDiaframma)
				{
				metroDefPres = TRUE;
				difMetro.addModify(difBanda);
				difMetroMarker.addModify(difBandaMarker);
				if (markerIndex[1] >= 0)
					{// preparo marker Index 1 E INSERISCO
//					difBandaMarker = difMetroMarker.getDifBanda(markerIndex[1]);
//					difBandaMarker.clearMod();
					// replico stessa difBanda ma con markerIndex diverso
					difBandaMarker.setBanda(markerIndex[1]);
//					difBandaMarker.setNumClassi(nCl);
//					difBandaMarker.Set(offset,v);
					difMetroMarker.addModify(difBandaMarker);
					}

				}
			}
		}
	cDest += 2; // al piu` 4 Classi
	}

BOOL viewScroll = FALSE;
if (metroDefPres)
	{
	if (dPos >= 0)
		{
		difMetro.posizione = (int ) corrPos;
		difMetroMarker.posizione = (int ) corrPos;
		}
	else    
		{// incremento automatico, 
		//	lresStop tenuto conto in inizializzazione
		difMetro.posizione = (int)c_difCoil.getStripMeter() + (int)c_difCoil.getMeterBaseStepX();
		difMetroMarker.posizione = (int)c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX();
		}
	// CSM20D POrting skip difetti m < 20
	if (difMetro.posizione > c_blankCoil)
		{
		// precalcolo totali per classe (true inizializzo counter)
		difMetro.calcTotalCode(TRUE,mask);
		difMetroMarker.calcTotalCode(TRUE,mask);
		if ((c_difCoil.GetSize() == 0)||
			((c_difCoil.GetAt(c_difCoil.GetUpperBound())).posizione != difMetro.posizione))
			{
			c_difCoil.Add(difMetro,mask);
			c_viewScroll = TRUE;
			c_markerCoil.Add(difMetroMarker,mask);
			}
		else
			{
			// calcolo poszione prima banda con totali da aggiornare  
			long baseIndex = 0;
			c_difCoil.setBaseSplitSchede(baseIndex);
			c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro,mask);
			c_markerCoil.SetAt(c_markerCoil.GetUpperBound(),difMetroMarker,mask);
			c_viewScroll = FALSE;
			}
		}
	}

/*
		if (
			(c_difCoil.GetSize() == 0)||
			((c_difCoil.GetAt(c_difCoil.GetUpperBound())).posizione != difMetro.posizione))
			{
			//difMetro.calcTotalCode(TRUE);
			difMetro.calcTotalCode(TRUE,mask);
			difMetroMarker.calcTotalCode(TRUE,mask);
			if (existIndex >=0)
				{
				long baseIndex = 0;
				c_difCoil.setBaseSplitSchede(baseIndex);
				// c_difCoil.SetAt(existIndex,difMetro,UPDATE_MASK_A|UPDATE_MASK_B);
				c_difCoil.SetAt(existIndex,difMetro,mask);
				viewScroll = FALSE;	
				}
			else
				{
				// precalcolo totali per classe (true inizializzo counter)
				// c_difCoil.Add(difMetro,UPDATE_MASK_A|UPDATE_MASK_B);
				c_difCoil.Add(difMetro,mask);
				viewScroll = TRUE;
				}
			}
		else
			{
			// precalcolo totali per classe, ricalcolo tutto
			// difMetro.calcTotalCode(TRUE,UPDATE_MASK_A|UPDATE_MASK_B);
			difMetro.calcTotalCode(TRUE,mask);
			// calcolo poszione prima banda con totali da aggiornare  
			long baseIndex = 0;
			c_difCoil.setBaseSplitSchede(baseIndex);
			if (existIndex >=0)
				{
				// c_difCoil.SetAt(existIndex,difMetro,UPDATE_MASK_A|UPDATE_MASK_B);
				c_difCoil.SetAt(existIndex,difMetro,mask);
				viewScroll = FALSE;	
				}
			else
				{
				// TRUE updateTotali
				// c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro,UPDATE_MASK_A|UPDATE_MASK_B);
				c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro,mask);
				viewScroll = FALSE;
				}
			}
		}
	} 
*/


// Update Metraggio
BOOL redrawAll = FALSE;
if (dPos >= 0)
	{
	// Salto di metraggio ridisegno tutto
	int deltaP;
	deltaP = (int) corrPos - (int)c_difCoil.getMeter();
	deltaP -= (int)c_difCoil.getMeterBaseStepX();
	if (deltaP > 5)
		{
#ifndef Csm20CA_REPORT
		// log
		// CString logStr;
		// logStr.Format("Salto metraggio atteso %d - arrivato %d",
		//	(int)c_difCoil.getMeter()+1,(int)corrPos);
		// c_pLog->LogString(LOG_WARNING,logStr);
#endif
		redrawAll = TRUE;
		}

	c_difCoil.setMeter((int) corrPos);
	c_markerCoil.setMeter((int) corrPos);
	}
else
	{
	// incremento automatico, 
	//	lresStop tenuto conto in inizializzazione
	c_difCoil.setMeter(c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX());
	c_markerCoil.setMeter(c_markerCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX());	
	redrawAll = TRUE;
	}

// Aggiornamento View
// if(dif252Found)
if (!c_useFingerHw)
	{// received strip marker
	if ((c_mode ==1) && fromBottom)
		{
		c_fingerList2.Copy(c_tmpFingerList2);
		}
	else
		{
		c_fingerList.Copy(c_tmpFingerList);
		c_redrawAll |= tagliSelect(TRUE,FALSE);
		}
	}

// Aggiorno Visualizzazione
CRect r;
// 1 significa solo aggiornamento
// 0 repaint all


#ifndef _DEBUG
if (baseClass >= 4)
#endif
	c_redraw = TRUE;

}


// updateViews, non viene + fatta sincrona con la comunicazione ma temporizzata su 500 msec
int CLineDoc::updateView(void)
{

// update solo dopo D
if (!c_redraw)
	return TRUE;

// Aggiorno Visualizzazione
CRect r;
// 1 significa solo aggiornamento
// 0 repaint all

// check if visible
CMainFrame *mFrame = (CMainFrame *)AfxGetMainWnd();
POSITION pos = GetFirstViewPosition();
CView* pView = GetNextView(pos);
CFrameWnd* wFrame = (CFrameWnd*) pView->GetParent();

if (wFrame == mFrame->MDIGetActive(NULL))
	{
	if (c_redrawAll)
		UpdateAllViews (NULL,0);
	else
		{
		// ridisegno solo su difetto D
		if (c_viewScroll)
			UpdateAllViews (NULL,1); // scroll 
		else
			UpdateAllViews (NULL,2); // redraw
		}
	}
	
updateDifAlarm();
// clear updateFlag
c_redrawAll = 0;
c_viewScroll = 0;
c_redraw = 0;

return TRUE;
}


//------------------------------------
//
//	Gestione Messagi tipo ValCounterBigHole
//
//------------------------------------

void CLineDoc::gestValCounterBigHole(PVOID p, DWORD size)
{
// Received Msg from serial
serMsgReceived = TRUE;

// Only system Enabled
if (!systemOnLine)
	return;


unsigned char  *cDest;
unsigned short *sDest;
unsigned long  *lDest;
cDest = (unsigned char *) p; 

long sPos,dPos;
sPos = *((long *)cDest); 
swaplong (&sPos,&dPos,1);

cDest += sizeof(long);

// incremento pointer
sDest = (unsigned short *) cDest;
lDest = (unsigned long *)  cDest;

int numField = 2*(size-sizeof(long)); // sono campi halfchar


// Formato su 1 mt
// Posiz,ValBh[0],ValBh[1],....., ValBh[6] .....;
//
// Riceve qualunque numero di schede 


// Fill dati Schede 

// Calcolo posizione corretta da lResStop
double corrPos;
corrPos = c_lunResStop + dPos;
corrPos += c_baseContinuaRotolo;

DifMetro difMetro;
DifBanda difBanda;
BOOL bandaDefPres = FALSE;
BOOL metroDefPres = FALSE;

// test se arrivato bigHole e mancano a,b,c,d precedenti
if(c_difCoil.GetSize()<=0)
	{// arrivato pacchetto orfano!
	TRACE("gestvalCounterBigHole arrivato pacchetto orfano su difCoilVuoto ");
	return;
	}
	
if	(c_difCoil.GetAt(c_difCoil.GetUpperBound()).posizione < (int)corrPos)
	{// arrivato pacchetto orfano!
	TRACE("gestvalCounterBigHole arrivato pacchetto orfano ");
	return;
	}

// Create new metro or update existing
if (
	((corrPos==0) || (c_difCoil.getMeter() == (int)corrPos))&&
	(c_difCoil.GetSize()>0)&&
	(c_difCoil.GetAt(c_difCoil.GetUpperBound()).posizione == (int)corrPos))
	difMetro = c_difCoil.GetAt(c_difCoil.GetUpperBound());

int lastIndex = numField;

for (int scIndex = 0;scIndex<lastIndex;scIndex ++)
	{
	difBanda.clear();
	long trueIndex = scIndex;
	// uso diaframmi anche con finger Hw
	BOOL outDiaframma=FALSE;
	// modo 0-separatore e 2-singleSheet
	// inserita possibilita` di lavorare senza diaframmi
	outDiaframma = c_useDiaframmi &&
			((trueIndex <= ((c_posDiaframmaSx/SIZE_BANDE)-1))|| 
			 (trueIndex >= ((c_posDiaframmaDx/SIZE_BANDE)+0)));
	
	difBanda = difMetro.getDifBanda(trueIndex);
	difBanda.setBanda(trueIndex);

	bandaDefPres = FALSE;

	int j = scIndex/2;
	int v=0;
	// pari 
	v = (int ) ((scIndex%2)==0)?(cDest[j]>>4):cDest[j];
	v &= 0x0f;
	if (v)	
		bandaDefPres = TRUE;

	difBanda.AddBigHole (v);	

	// l'offset di posizione trasversale e` 0
	if (bandaDefPres)
		{
		// check se banda sotto finger
		if (c_useFingerHw)
			{// == 0 cella visibile
			if (!outDiaframma)
				{
				metroDefPres = TRUE;
				difMetro.addModify(difBanda);
				}
			}
		else
			{
			if(c_fingerList[trueIndex]==0)
				{// == 0 cella visibile
				metroDefPres = TRUE;
				difMetro.addModify(difBanda);
				}
			}
		}
	}


if (metroDefPres)
	{
	if (dPos >= 0)
		{
		difMetro.posizione = (int ) corrPos;
		}
	else
		{// incremento automatico, 
		//	lresStop tenuto conto in inizializzazione
		difMetro.posizione = (int)c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX();
		}
		// skip difetti m < 20
	if (difMetro.posizione > 20)
		{
		// precalcolo totali per classe (true inizializzo counter)
		difMetro.calcTotalCode(TRUE,UPDATE_MASK_BIG);
		if ((c_difCoil.GetSize() == 0)||
			((c_difCoil.GetAt(c_difCoil.GetUpperBound())).posizione != difMetro.posizione))
			{
			c_difCoil.Add(difMetro,UPDATE_MASK_BIG);
			c_viewScroll = TRUE;
			}
		else
			{
			// precalcolo totali per classe, ricalcolo tutto
			// calcolo poszione prima banda con totali da aggiornare  
			long baseIndex = 0;
			c_difCoil.setBaseSplitSchede(baseIndex);
			c_difCoil.SetAt(c_difCoil.GetUpperBound(),difMetro,UPDATE_MASK_BIG);
			c_viewScroll = FALSE;
			}
		}
	}
	

if (dPos >= 0)
	{
	// Salto di metraggio ridisegno tutto
	int deltaP;
	deltaP = (int) corrPos - (int)c_difCoil.getMeter();
	deltaP -= (int)c_difCoil.getMeterBaseStepX();
	if (deltaP > 0)
		c_redrawAll = TRUE;
	c_difCoil.setMeter((int) corrPos);
	}
else
	{
	// incremento automatico, 
	//	lresStop tenuto conto in inizializzazione
		c_difCoil.setMeter(c_difCoil.getMeter() + (int)c_difCoil.getMeterBaseStepX());
	}


}







// ----------------------------------------
// Aggiornamento allarmi soglie difetti
void CLineDoc::updateDifAlarm() 
{

CString allarme,s;
BOOL alarm = FALSE;
// densita A
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('A',c_densityCalcLength);
	str.Format("%6.0lf",densita);
	if (densita > c_difCoil.getAllarme ('A'))
		{
		// 
		if (!densitaAlarm.c_statusDensClassA)
			{
			// 
			densitaAlarm.c_statusDensClassA=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFA);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassA)
			{
			// 
			densitaAlarm.c_statusDensClassA=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFA);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}

// densita B
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('B',c_densityCalcLength);
	str.Format("%6.0lf",densita);
	if (densita > c_difCoil.getAllarme ('B'))
		{
		// 
		if (!densitaAlarm.c_statusDensClassB)
			{
			// 
			densitaAlarm.c_statusDensClassB=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFB);
			// allarme.Format("Superata soglia difetti tipo A (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassB)
			{
			// 
			densitaAlarm.c_statusDensClassB=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFB);
			// allarme.Format("Superata soglia difetti tipo B (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}
//


// densita C
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('C',c_densityCalcLength);
	str.Format("%6.0lf",densita);
	if (densita > c_difCoil.getAllarme ('C'))
		{
		// 
		if (!densitaAlarm.c_statusDensClassC)
			{
			// 
			densitaAlarm.c_statusDensClassC=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFC);
			// allarme.Format("Superata soglia difetti tipo C (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassC)
			{
			// 
			densitaAlarm.c_statusDensClassC=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFC);
			// allarme.Format("Superata soglia difetti tipo C (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}
//

// densita D
	{
	CString str;
	double densita=c_difCoil.getDensityLevel('D',c_densityCalcLength);
	str.Format("%6.0lf",densita);
	if (densita > c_difCoil.getAllarme ('D'))
		{
		// 
		if (!densitaAlarm.c_statusDensClassD)
			{
			// 
			densitaAlarm.c_statusDensClassD=TRUE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_SOGLIADIFD);
			// allarme.Format("Superata soglia difetti tipo D (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	else
		{
		if (densitaAlarm.c_statusDensClassD)
			{
			// 
			densitaAlarm.c_statusDensClassD=FALSE;
			//
			CString resMsg;
			resMsg.LoadString (CSM_GRAPHDOC_NO_SOGLIADIFD);
			// allarme.Format("Superata soglia difetti tipo D (%8.2lf) al m. %d\r\n",trendA,
			allarme.Format(resMsg,densita,
				(int)c_difCoil.getMeter());
			densitaAlarm.appendMsg(allarme);
			alarm = TRUE;			
			}
		}
	}
//

if ((densitaAlarm.c_statusDensClassA)||
	(densitaAlarm.c_statusDensClassB)||
	(densitaAlarm.c_statusDensClassC)||
	(densitaAlarm.c_statusDensClassD))
	c_densityAlarmActive = TRUE;


if (alarm)
	{
// Invio micro allarme ( non serve + )
//	sendAlarmDif ();
// Allarme Densita non visualizza +
//	densitaAlarm.visualizza(AfxGetMainWnd(),secAlarmSound);
	}

}


//------------------------------------
//
//	Gestione Messagi tipo ValPeriod
//
//------------------------------------


void CLineDoc::gestValPeriod (PVOID p,DWORD size) // Gestisce arrivo VAL_PERIOD
{
if (!systemOnLine)
	return;

}

//------------------------------------
//
//	Gestione Messagi tipo ValAlarm
//	 Allarmi di sistema
// aggiunto 1 byte per gestire fino a 32 CPU
//------------------------------------

void CLineDoc::gestValAlarm (PVOID p,DWORD size,BOOL fromTop) 
{
// Received Msg from serial
serMsgReceived = TRUE;


BOOL alActive = FALSE;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

unsigned char *upc;
upc = (unsigned char *)p;

int al;

// Allarme singolo laser rotto
al = (int) upc[0] & 0x01;
if (fromTop)
	{
	//if (al && (al != pFrame->c_stAllarmiTop.laser)&&
	//	systemOnLine)
		if ((pFrame->c_stAllarmiTop.updateLaserState(al))&& al)
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_LASERNOK);
		// allarmeSistema.Format("Laser NON funzionante\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	}
else
	{
	//if (al && (al != pFrame->c_stAllarmiBottom.laser)&&
	//	systemOnLine)
	if ((pFrame->c_stAllarmiBottom.updateLaserState(al)) && al)
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_LASERNOK);
		// allarmeSistema.Format("Laser NON funzionante\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	}

	
// Encoder
al = (upc[0] & 0x02)?1:0;
if (fromTop)
	{
	//if (al && (al != pFrame->c_stAllarmiTop.encoder)&&
	//	systemOnLine)
	if ((pFrame->c_stAllarmiTop.updateEncoderState(al)) && al)
		{
		CString allarmeSistema;
		CString s,s1;
		s = _T("Cpu Left "); 
		s1.LoadString(CSM_GRAPHDOC_ENCODERNOK);
		s += s1;
		// allarmeSistema.Format("Encoder NON funzionante\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	// pFrame->c_stAllarmiTop.encoder = al;
	}
else
	{
	//if (al && (al != pFrame->c_stAllarmiBottom.encoder)&&
	//	systemOnLine)
	if ((pFrame->c_stAllarmiBottom.updateEncoderState(al)) && al)
		{
		CString allarmeSistema;
		CString s,s1;
		s = _T("Cpu Right ");
		s1.LoadString(CSM_GRAPHDOC_ENCODERNOK);
		s += s1;
		// allarmeSistema.Format("Encoder NON funzionante\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	//pFrame->c_stAllarmiBottom.encoder = al;
	}
	
// otturatore1
al = (upc[0] & 0x04)?1:0;
if (fromTop)
	{
	//if (al && (al != pFrame->c_stAllarmiTop.otturatore1)&&
	//	systemOnLine)
	if ((pFrame->c_stAllarmiTop.updateOtturatore1State(al)) && al)
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_OTTURA1NOK);
		// allarmeSistema.Format("Otturatore1 NON in posizione\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	//pFrame->c_stAllarmiTop.otturatore1 = al;
	}
else
	{
	//if (al && (al != pFrame->c_stAllarmiBottom.otturatore1)&&
	//	systemOnLine)
	if ((pFrame->c_stAllarmiBottom.updateOtturatore1State(al)) && al)
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_OTTURA1NOK);
		// allarmeSistema.Format("Otturatore1 NON in posizione\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	//pFrame->c_stAllarmiBottom.otturatore1 = al;
	}

// Otturatore2
al = (upc[0] & 0x08)?1:0;
if (fromTop)
	{
	//if (al && (al != pFrame->c_stAllarmiTop.otturatore2)&&
	//	systemOnLine)
	if ((pFrame->c_stAllarmiTop.updateOtturatore2State(al)) && al)
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_OTTURA2NOK);
		// allarmeSistema.Format("Otturatore2 NON in posizione\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	// pFrame->c_stAllarmiTop.otturatore2 = al;
	}
else
	{
	//if (al && (al != pFrame->c_stAllarmiBottom.otturatore2)&&
	//	systemOnLine)
	if ((pFrame->c_stAllarmiBottom.updateOtturatore2State(al)) && al)
		{
		CString allarmeSistema;
		CString s;
		s.LoadString(CSM_GRAPHDOC_OTTURA2NOK);
		// allarmeSistema.Format("Otturatore2 NON in posizione\r\n");
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	// pFrame->c_stAllarmiBottom.otturatore2 = al;
	}

// CPU
int numCpu = this->c_numCpu;	
int numCpuLeft = c_numCpuLeftSide;	
int numCpuRight = c_numCpu - c_numCpuLeftSide;

// skip 1 byte
unsigned char mask;
upc += 1;
// blk[5]-blk[6]
// CPU 16-32 = 16 bit
if (fromTop)
	{// invariata numerazione
	if (numCpuLeft > 16)
		{
		for (int i=0;i< min((pFrame->c_stAllarmiTop.alNumCpu-16),2);i++)
			{  
			if ((i%8) == 0)
				mask = 0x01;
			al = (upc[i/8] & mask)?1:0;
			//if (al && (al != pFrame->c_stAllarmiTop.cpu[i+16])&&
			//	systemOnLine)
			if (al && (al != pFrame->c_stAllarmiTop.cpu[i+16]))
				{
				CString allarmeSistema;
				CString s,s1;
				s.LoadString(CSM_GRAPHDOC_CPUNOK);
				// allarmeSistema.Format("CPU %d NON funzionante\r\n",i);
				s1.Format (s,i+1+16);
				allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s1);
				systemAlarm.appendMsg(allarmeSistema);
				alActive = TRUE;
				}
			pFrame->c_stAllarmiTop.cpu[i+16] = al;
			mask = mask << 1;
			}
		}
	}
else
	{// parte destra devo aggiungere alla numerazione la parte sinistra
	if (numCpuRight > 16)
		{
		for (int i=0;i< min((numCpuRight-16),16);i++)
			{  
			if ((i%8) == 0)
				mask = 0x01;
			al = (upc[i/8] & mask)?1:0;
			//if (al && (al != pFrame->c_stAllarmiBottom.cpu[i+16])&&
			//	systemOnLine)
			if (al && (al != pFrame->c_stAllarmiTop.cpu[i+16+numCpuLeft]))
				{
				CString allarmeSistema;
				CString s,s1;
				s.LoadString(CSM_GRAPHDOC_CPUNOK);
				// allarmeSistema.Format("CPU %d NON funzionante\r\n",i);
				s1.Format (s,i+1+16+18);	// aggiunto parte sx = 18 cpu
				allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s1);
				systemAlarm.appendMsg(allarmeSistema);
				alActive = TRUE;
				}
			pFrame->c_stAllarmiBottom.cpu[i+16+numCpuLeft] = al;
			mask = mask << 1;
			}
		}
	}


// skip 1+1 byte  
//if ((fromTop && (numCpu > 16))||
//	((!fromTop)&&(numCpu > 16)))
if (numCpu > 16)
	upc += 2;

// skip 1 byte ora 32 CPU 
// upc += 1;
// blk[7]-blk[8] = 16 bit
// CPU 0-15
if (fromTop)
	{// queste sono le effettive prime 16 cpu 
	for (int i=0;i< min(numCpuLeft,16);i++)	// 2 byte per le CPU
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		//if (al && (al != pFrame->c_stAllarmiTop.cpu[i])&&
		//	systemOnLine)
		if (al && (al != pFrame->c_stAllarmiTop.cpu[i]))
			{
			CString allarmeSistema;
			CString s,s1;
			s.LoadString(CSM_GRAPHDOC_CPUNOK);
			// allarmeSistema.Format("CPU %d NON funzionante\r\n",i);
			s1.Format (s,i+1);
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s1);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		pFrame->c_stAllarmiTop.cpu[i] = al;
		mask = mask << 1;
		}
	}
else
	{// qs pck contiene seconda parte cpu che vanno su barra top, da 19 a 26
	for (int i=0;i< min(numCpuRight,16);i++)	// 2 byte per le CPU
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		//if (al && (al != pFrame->c_stAllarmiBottom.cpu[i])&&
		//	systemOnLine)
		if (al && (al != pFrame->c_stAllarmiTop.cpu[i+numCpuLeft]))
			{
			CString allarmeSistema;
			CString s,s1;
			s.LoadString(CSM_GRAPHDOC_CPUNOK);
			// allarmeSistema.Format("CPU %d NON funzionante\r\n",i);
			s1.Format (s,i+1+18);	// aggiunto parte sx = 18 cpu
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s1);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		pFrame->c_stAllarmiTop.cpu[i+numCpuLeft] = al;
		mask = mask << 1;
		}
	}
// skip cpu byte 
// int nCpu = (pFrame->c_stAllarmiTop.alNumCpu<=16)?2:4;
// formato fisso con due+due byte per le CPU
int nCpu = 2;
upc += nCpu;

//-----------------------------
// ricevitori
// c_firstBoardClassAB = 2;
int numReceiver = c_difCoil.getNumSchede();	
int numReceiverLeft = c_difCoil.getNumSchedeLeft();	
int numReceiverRight = numReceiver - numReceiverLeft;

if (fromTop)
	{// ricevitori sulla riga sopra
	for (int i=0;i< min(numReceiverLeft,(int )(size-2-nCpu)*8);i++)
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		// 11-03-05
		int index=i;
		if (c_difCoil.getNumClassi() > 2)
			{// quattro classi
			if (c_firstBoardClassAB == 0)
				{// caso normale
				// 0-0 1-2 2-1 3-3
				// 0=0+0 1=1+1 2=2-1 3=3+0
				int bitMap [4] = {0,1,-1,0};
				index = i + bitMap[i%4];
				}
			if (c_firstBoardClassAB == 1)
				{//caso invertito
				// 0-1 1-3 2-0 3-2
				// 0=0+1 1=1+2 2=2-2 3=3-1
				int bitMap [4] = {1,2,-2,-1};
				index = i + bitMap[i%4];	
				}
			if (c_firstBoardClassAB == 2)
				{//caso 
				// 0-1 2-3 2-2 3-3
				// 0=0+1 1=1-1 2=2+1 3=3-1
				int bitMap [4] = {1,-1,1,-1};
				index = i + bitMap[i%4];	
				}
			}
		if (al && (al != pFrame->c_stAllarmiTop.receiver[index])&&
			systemOnLine)
			{
			CString allarmeSistema;
			//  CString cls;
			CString fmt;
			fmt.LoadString(CSM_GRAPHDOC_RICNOK);
			CString s;
			// Cella %d Rx %d
			s.Format(fmt,i+1,i/2+1);
			// Par Cella,Receiver 
			/*
			switch (c_difCoil.getNumClassi())
				{
				case 2:
					cls = "A/B";
					s.Format(fmt,i+1,cls,i/2+1);
					break;
				case 4:
				default:
					if (i%2 == 0)
						cls = "A/B";
					else
						cls = "C/D";
					s.Format(fmt,i/2+1,cls,i/2+1);
				}
			*/
			// allarmeSistema.Format("RICEVITORE Classi %s Numero %d NON funzionante\r\n",cls,i/2);
			// Modificato in 
			// allarmeSistema.Format("CELLA %d Classi %s Rx %d NON funzionante\r\n",cls,i/2);
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		// 11-03-05
		//pFrame->c_stAllarmi.receiver[i] = al;
		pFrame->c_stAllarmiTop.receiver[index] = al;

		mask = mask << 1;
  	    }
/*
	{
	for (int i=0;i< min(pFrame->c_stAllarmiTop.alNumReceiver,(int )(size-2-nCpu)*8);i++)
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		// 11-03-05
		int index=i;
		if (c_difCoil.getNumClassi() > 2)
			{// quattro classi
			if (c_firstBoardClassAB == 0)
				{// caso normale
				// 0-0 1-2 2-1 3-3
				// 0=0+0 1=1+1 2=2-1 3=3+0
				int bitMap [4] = {0,1,-1,0};
				index = i + bitMap[i%4];
				}
			if (c_firstBoardClassAB == 1)
				{//caso invertito
				// 0-1 1-3 2-0 3-2
				// 0=0+1 1=1+2 2=2-2 3=3-1
				int bitMap [4] = {1,2,-2,-1};
				index = i + bitMap[i%4];	
				}
			if (c_firstBoardClassAB == 2)
				{//caso 
				// 0-0 1-1 2-2 3-3
				// 0=0+1 1=1+2 2=2-2 3=3-1
				int bitMap [4] = {0,0,1,1};
				index = i + bitMap[i%4];	
				}
			}

		//if (al && (al != pFrame->c_stAllarmiTop.receiver[index])&&
		//	systemOnLine)
		if (al && (al != pFrame->c_stAllarmiTop.receiver[index]))
			{
			CString allarmeSistema;
			CString cls;
			CString fmt;
			// skip rx indication in msg 
			if(c_skipRxInAutotestMsg)
				fmt.LoadString(CSM_GRAPHDOC_RICNOK2);
			else
				// normal case
				fmt.LoadString(CSM_GRAPHDOC_RICNOK);
			//--
			CString s;
			// Par Cella,Classi,Receiver 
			switch (c_difCoil.getNumClassi())
				{
				case 2:
					cls = "A/B";
					// skip rx indication in msg 
					if(c_skipRxInAutotestMsg)
						s.Format(fmt,i+1,cls);
					else
						s.Format(fmt,i+1,cls,i/2+1);
					break;
				case 4:
				default:
					{
					if (i%2 == 0)
						cls = "A/B";
					else
						cls = "C/D";
					// skip rx indication in msg 
					if(c_skipRxInAutotestMsg)
						s.Format(fmt,i/2+1,cls);
					else
						{
						//s.Format(fmt,i/2+1,cls,i/2+1);
	//---------------------------------------------------------
	// Tabelle assegnazione bit 
	// caso c_firstBoardClassAB == 0
	// bit 0,cella 1,rx 1, AB  rx = cella +0
	// bit 1,cella 1,rx 2, CD  rx = cella +1					
	// bit 2,cella 2,rx 1, AB  rx = cella +0 -1
	// bit 3,cella 2,rx 2, CD  rx = cella +1 -1

	// caso c_firstBoardClassAB == 1
	// bit 0,cella 1,rx 2, AB  rx = cella +1
	// bit 1,cella 1,rx 1, CD  rx = cella +0					
	// bit 2,cella 2,rx 2, AB  rx = cella +1 -1
	// bit 3,cella 2,rx 1, CD  rx = cella +0 -1
						// uso variabile rx per modificare anche visualizzazione grafica 
						int cella = i/2+1;
						int rx = 0;
						if (c_firstBoardClassAB == 0)
							rx = cella + ((i%2==0)?0:1) - ((i%4>=2)?1:0);
						else
							rx = cella + ((i%2==0)?1:0) - ((i%4>=2)?1:0);					
						s.Format(fmt,cella,cls,rx);
						}
					}
				}
			// allarmeSistema.Format("RICEVITORE Classi %s Numero %d NON funzionante\r\n",cls,i/2);
			// Modificato in CSM_GRAPHDOC_RICNOK
			// allarmeSistema.Format("CELLA %d Classi %s Rx %d NON funzionante\r\n",cls,i/2);
			// Modificato in CSM_GRAPHDOC_RICNOK2
			// allarmeSistema.Format("CELLA %d Classi %s NON funzionante\r\n",cls);
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		// 11-03-05
		//pFrame->c_stAllarmi.receiver[i] = al;
		pFrame->c_stAllarmiTop.receiver[index] = al;

		mask = mask << 1;
		}
	if (alActive)
		{
		systemAlarm.visualizza(pFrame,secAlarmSound);
		}
	}
*/
	}
else
	{// ricevitori sulla riga sotto 
	for (int i=0;i< min(numReceiverRight,(int )(size-2-nCpu)*8);i++)
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		// 11-03-05
		int index=i;
		if (c_difCoil.getNumClassi() > 2)
			{// quattro classi
			if (c_firstBoardClassAB == 0)
				{// caso normale
				// 0-0 1-2 2-1 3-3
				// 0=0+0 1=1+1 2=2-1 3=3+0
				int bitMap [4] = {0,1,-1,0};
				index = i + bitMap[i%4];
				}
			if (c_firstBoardClassAB == 1)
				{//caso invertito
				// 0-1 1-3 2-0 3-2
				// 0=0+1 1=1+2 2=2-2 3=3-1
				int bitMap [4] = {1,2,-2,-1};
				index = i + bitMap[i%4];	
				}
			if (c_firstBoardClassAB == 2)
				{//caso 
				// 0-1 2-3 2-2 3-3
				// 0=0+1 1=1-1 2=2+1 3=3-1
				int bitMap [4] = {1,-1,1,-1};
				index = i + bitMap[i%4];	
				}
			}

		//if (al && (al != pFrame->c_stAllarmiBottom.receiver[index])&&
		//	systemOnLine)
		if (al && (al != pFrame->c_stAllarmiTop.receiver[index+numReceiverLeft]))
			{
			CString allarmeSistema;
			// CString cls;
			CString fmt;
			// skip rx indication in msg 
			if(c_skipRxInAutotestMsg)
				fmt.LoadString(CSM_GRAPHDOC_RICNOK2);
			else
				// normal case
				fmt.LoadString(CSM_GRAPHDOC_RICNOK);
			//--
			CString s;
			// Cella %d Rx %d
			s.Format(fmt,i+1+numReceiverLeft,i/2+1+numReceiverLeft/2); // diviso 2 anche i ricevitori left
			// Par Cella,Classi,Receiver 
			/*
			switch (c_difCoil.getNumClassi())
				{
				case 1:
					{
					//int cell = i+modulo*16;
					int cell = i/2+1;
					cls = _T("A");// niente classe
					// 4 celle per ricevitore
					s.Format(fmt,cell+1,cell/4+1);
					}
					break;
				case 2:
					cls = "A/B";
					// skip rx indication in msg 
					if(c_skipRxInAutotestMsg)
						s.Format(fmt,i+1,cls);
					else
						s.Format(fmt,i+1,cls,i/2+1);
					break;
				case 4:
				default:
					{
					if (i%2 == 0)
						cls = "A/B";
					else
						cls = "C/D";
					// skip rx indication in msg 
					if(c_skipRxInAutotestMsg)
						s.Format(fmt,i/2+1,cls);
					else
						{
						//s.Format(fmt,i/2+1,cls,i/2+1);
	//---------------------------------------------------------
	// Tabelle assegnazione bit 
	// caso c_firstBoardClassAB == 0
	// bit 0,cella 1,rx 1, AB  rx = cella +0
	// bit 1,cella 1,rx 2, CD  rx = cella +1					
	// bit 2,cella 2,rx 1, AB  rx = cella +0 -1
	// bit 3,cella 2,rx 2, CD  rx = cella +1 -1

	// caso c_firstBoardClassAB == 1
	// bit 0,cella 1,rx 2, AB  rx = cella +1
	// bit 1,cella 1,rx 1, CD  rx = cella +0					
	// bit 2,cella 2,rx 2, AB  rx = cella +1 -1
	// bit 3,cella 2,rx 1, CD  rx = cella +0 -1
						// uso variabile rx per modificare anche visualizzazione grafica 
						int cella = i/2+1;
						int rx = 0;
						if (c_firstBoardClassAB == 0)
							rx = cella + ((i%2==0)?0:1) - ((i%4>=2)?1:0);
						else
							rx = cella + ((i%2==0)?1:0) - ((i%4>=2)?1:0);					
						s.Format(fmt,cella,cls,rx);
						}
					}
				}
			*/
			// allarmeSistema.Format("RICEVITORE Classi %s Numero %d NON funzionante\r\n",cls,i/2);
			// Modificato in CSM_GRAPHDOC_RICNOK
			// allarmeSistema.Format("CELLA %d Classi %s Rx %d NON funzionante\r\n",cls,i/2);
			// Modificato in CSM_GRAPHDOC_RICNOK2
			// allarmeSistema.Format("CELLA %d Classi %s NON funzionante\r\n",cls);
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		// 11-03-05
		//pFrame->c_stAllarmi.receiver[i] = al;
		pFrame->c_stAllarmiTop.receiver[index+numReceiverLeft] = al;
		mask = mask << 1;
		}
/*
	{
	for (int i=0;i< min(pFrame->c_stAllarmiBottom.alNumReceiver,(int )(size-2-nCpu)*8);i++)
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		if (al && (al != pFrame->c_stAllarmiBottom.receiver[i])&&
			systemOnLine)
			{
			CString allarmeSistema;
			CString cls;
			CString fmt;
			fmt.LoadString(CSM_GRAPHDOC_RICNOK);
			CString s;
			// Par Cella,Classi,Receiver 
			switch (c_difCoil.getNumClassi())
				{
				case 2:
					cls = "A/B";
					s.Format(fmt,i+1,cls,i/2+1);
					break;
				case 4:
				default:
					if (i%2 == 0)
						cls = "A/B";
					else
						cls = "C/D";
					s.Format(fmt,i/2+1,cls,i/2+1);
				}
			// allarmeSistema.Format("RICEVITORE Classi %s Numero %d NON funzionante\r\n",cls,i/2);
			// Modificato in 
			// allarmeSistema.Format("CELLA %d Classi %s Rx %d NON funzionante\r\n",cls,i/2);
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		pFrame->c_stAllarmiBottom.receiver[i] = al;
		mask = mask << 1;
		}
	}*/
	if (alActive)
		{
		systemAlarm.visualizza(pFrame,secAlarmSound);
		}
	}


}

//--------------------------------------
//
//	Gestione Messagi tipo ValRisAutotest
//		 Allarmi laser rotti
//			non utilizzato
//--------------------------------------

// ok fatto
void CLineDoc::gestValRisAutotest(PVOID p,DWORD size)
{// fatto
//!!!!! non utilizzato !!!!!!!
return;
// formato 1 int per il modulo ( 1-10) + n-1 ( 16 )valori per celle 
// Received Msg from serial
serMsgReceived = TRUE;

BOOL alActive = FALSE;
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

unsigned int *upc;
upc = (unsigned int *)p;

// modulo 1-10 
int modulo = *upc;
// partiamo da 0
modulo --; 
if (modulo < 0)
	modulo = 0;

// avanti pointer, skip numero modulo
upc++;

for (int i=0;i<(min(16,size-1)) ;i++)
	{//allarmi in logica negata aggiunto NOT !
	if (i+modulo*16 >=  pFrame->c_stAllarmiTop.alNumReceiver)
		break;
	int al = !upc[i];
	if (al && (al != pFrame->c_stAllarmiTop.receiver[i+modulo*16])&&
		(!systemOnLine))
		{
		CString allarmeSistema;
		CString cls;
			CString fmt;
			fmt.LoadString(CSM_GRAPHDOC_RICNOK);
			CString s;
			// Par Cella,Classi,Receiver 
			switch (c_difCoil.getNumClassi())
				{
				case 1:
					{
					int cell = i+modulo*16;
					cls = _T("A");// niente classe
					// 4 celle per ricevitore
					s.Format(fmt,cell+1,cell/4+1);
					}
					break;
				case 2:
					cls = _T("A/B");
					// s.Format(fmt,modulo+1,cls,i+1+modulo*16);
					s.Format(fmt,modulo+1,i+1+modulo*16);
					break;
				case 4:
				default:
					if (i%2 == 0)
						cls = _T("A/B");
					else
						cls = _T("C/D");
					s.Format(fmt,modulo+1,i+1+modulo*16);
				}
			// allarmeSistema.Format(_T("RICEVITORE Classi %s Numero %d NON funzionante\r\n"),cls,i/2);
			// Modificato in 
			// allarmeSistema.Format(_T("CELLA %d Classi %s Rx %d NON funzionante\r\n"),cls,i/2);
			allarmeSistema.Format (_T("m. %d: %s"),(int)c_difCoil.getMeter(),s);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		
		pFrame->c_stAllarmiTop.receiver[i+modulo*16] = (pFrame->c_stAllarmiTop.receiver[i+modulo*16] == AL_OFF)?al:
							pFrame->c_stAllarmiTop.receiver[i+modulo*16] || al;
  	    }



/*

int al;
// dimensione variabile
for (int i=0;i< min(pFrame->c_stAllarmi.alNumLaser,(int )(size -1));i++)
	{  
	al = upc[i];
	// allarme conosciuto
	if (al && (al != pFrame->c_stAllarmi.laserKO[i+(modulo*16)])&&
		systemOnLine)
		{
		CString allarmeSistema;
		CString fmt;
		fmt.LoadString(CSM_GRAPHDOC_LASNOK);
		CString s;
		s.Format(fmt,(i+1)+modulo*16);
		// allarmeSistema.Format("LASER %d NON funzionante\r\n",i);
		allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
		systemAlarm.appendMsg(allarmeSistema);
		alActive = TRUE;
		}
	pFrame->c_stAllarmi.laserKO[i+(modulo*16)] = al;
	}

if (alActive)
	{
	systemAlarm.visualizza(pFrame,secAlarmSound);
	}
*/
	
}


//--------------------------------------
//
//	Gestione Messagi tipo ValLaser
//				   Allarmi laser rotti
//--------------------------------------


void CLineDoc::gestValLaser(PVOID p,DWORD size,bool fromTop)
{
// Received Msg from serial
serMsgReceived = TRUE;

BOOL alActive = FALSE;
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

unsigned char *upc;
upc = (unsigned char *)p;

int al;
unsigned char mask;

/* pacchetto arriva solo da top 
if (fromTop)
	{// arriva solo da top, ma disegnamo nella riga sotto
	// dimensione variabile
	for (int i=0;i< min(pFrame->c_stAllarmiTop.alNumLaser,(int )(size)*8);i++)
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		// allarme conosciuto
		//if (al && (al != pFrame->c_stAllarmiTop.laserKO[i])&&
		//	systemOnLine)
		if (al && (al != pFrame->c_stAllarmiTop.laserKO[i]))
			{
			CString allarmeSistema;
			CString fmt;
			fmt.LoadString(CSM_GRAPHDOC_LASNOK);
			CString s;
			s.Format(fmt,i+1);
			// allarmeSistema.Format("LASER %d NON funzionante\r\n",i);
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		pFrame->c_stAllarmiTop.laserKO[i] = al;
		mask = mask << 1;
		}
	if (alActive)
		{
		systemAlarm.visualizza(pFrame,secAlarmSound);
		}
	}
else
*/
// pacchetto arriva solo da top/master ma disegna sulla riga sotto 
if (fromTop)
	{
	// dimensione variabile
	for (int i=0;i< min(pFrame->c_stAllarmiBottom.alNumLaser,(int )(size)*8);i++)
		{  
		if ((i%8) == 0)
			mask = 0x01;
		al = (upc[i/8] & mask)?1:0;
		// allarme conosciuto
		//if (al && (al != pFrame->c_stAllarmiBottom.laserKO[i])&&
		//	systemOnLine)
		if (al && (al != pFrame->c_stAllarmiBottom.laserKO[i]))
			{
			CString allarmeSistema;
			CString fmt;
			fmt.LoadString(CSM_GRAPHDOC_LASNOK);
			CString s;
			s.Format(fmt,i+1);
			// allarmeSistema.Format("LASER %d NON funzionante\r\n",i);
			allarmeSistema.Format ("m. %d: %s",(int)c_difCoil.getMeter(),s);
			systemAlarm.appendMsg(allarmeSistema);
			alActive = TRUE;
			}
		pFrame->c_stAllarmiBottom.laserKO[i] = al;
		mask = mask << 1;
		}
	if (alActive)
		{
		systemAlarm.visualizza(pFrame,secAlarmSound);
		}
	}


}

//------------------------------------
//
//	Invia Allarme difetti
//			broadcast	   
//------------------------------------


void CLineDoc::sendAlarmDif (void)
{
#ifndef Csm20El2013_REPORT
struct Command command;

command.cmd = VAL_ALARMDIF;
command.size = 0;
command.data = NULL;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->SendBroadcastGeneralCommand(&command);
//if(isTopSide())
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
//else
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);

//pFrame->SendGeneralCommand(&command);

#endif
}


//------------------------------------
//
//	Invia start o stop message
//				   
// comandi ora broadcast 
//------------------------------------


void CLineDoc::sendStartStop (BOOL start,BOOL test)
{

#ifndef Csm20El2013_REPORT
struct Command command;
char tipo;

command.cmd = VAL_COMMAND;

if (start)
	{
	if (test)
		tipo = TSTARTCMD;
	else
		tipo = STARTCMD;
	}
else
	tipo = STOPCMD;

command.size = sizeof (tipo);
command.data = (void *) &tipo;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

//if (tipo == STOPCMD)
	pFrame->SendBroadcastGeneralCommand(&command);
/*
else
	{
	if(isTopSide())
		pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
	else
		pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);
	}
*/
#endif
}


//------------------------------------
//
//	Invia Input-Output start o stop message
//				   solo master
//------------------------------------


void CLineDoc::sendIOStartStop (BOOL start)
{
#ifndef Csm20El2013_REPORT
struct Command command;
char tipo;

command.cmd = VAL_COMMAND;

if (start)
	tipo = IOSTARTCMD;
else
	tipo = IOSTOPCMD;

command.size = sizeof (tipo);
command.data = (void *) &tipo;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

//if(isTopSide())
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
//else
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);
#endif
}

//------------------------------------
//
//	Invia Valore soglie
//				   broadcast
//------------------------------------


void CLineDoc::sendValSoglie (void)
{
#ifndef Csm20El2013_REPORT
struct Command command;
short sFrom[4],sTo[4];

command.cmd = VAL_SOGLIA;

sFrom[0] = (short) sogliaA;
sFrom[1] = (short) sogliaB;
sFrom[2] = (short) sogliaC;
sFrom[3] = (short) sogliaD;

// Swap data (short format)
_swab ((char *)sFrom,(char *)sTo,4*sizeof(short));


command.size = 4 * sizeof (short);
command.data = (void *) &sTo;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

pFrame->SendBroadcastGeneralCommand(&command);

/* 
 if(isTopSide())
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
else
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);

//pFrame->SendGeneralCommand(&command);
*/

#endif
}

//------------------------------------------------
//
//	Invia Valore fattore di correzione dim lin
//				   broadcast
//------------------------------------------------


void CLineDoc::sendValFcdl (void)
{
#ifndef Csm20El2013_REPORT

struct Command command;
short sFrom,sTo;

command.cmd = VAL_FCDL;

sFrom = (short) (c_fattCorrDimLin * 1000.);

// Swap data (short format)
_swab ((char *)&sFrom,(char *)&sTo,1*sizeof(short));

command.size = 1 * sizeof (short);
command.data = (void *) &sTo;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->SendBroadcastGeneralCommand(&command);

/*
if(isTopSide())
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
else
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);

//pFrame->SendGeneralCommand(&command);
*/

#endif
}

//------------------------------------------------
//
//	Invia impostazione finger HW
//				   solo top
//------------------------------------------------


void CLineDoc::sendUseFingerHw (void)
{
#ifndef Csm20El2013_REPORT
struct Command command;
unsigned char cv;

command.cmd = VAL_USE_FINGERHW;

cv = c_useFingerHw;

command.size = 1;
command.data = (void *) &cv;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
//pFrame->SendBroadcastGeneralCommand(&command);
//if(isTopSide())
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
//else
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);
#endif
}


//------------------------------------------------
//
//	Invia impostazione finger HW
//				   broadcast
//------------------------------------------------


void CLineDoc::sendValDistOddEven (void)
{
#ifndef Csm20El2013_REPORT
CProfile profile;
struct Command command;
unsigned char cv;

command.cmd = VAL_DIST_ODDEVEN;

DBParametri dbParametri (dBase);
if (dbParametri.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	cv = dbParametri.m_ODDEVEN_DISTANCE;
	}
else
	cv = 0;
dbParametri.Close();

command.size = 1;
command.data = (void *) &cv;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->SendBroadcastGeneralCommand(&command);

//if(isTopSide())
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
//else
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);

#endif
}


//------------------------------------------------
//
//	Invia Start test diaframma
//				   solo top
//------------------------------------------------


void CLineDoc::sendStartTestDiaf (BOOL left)
{
#ifndef Csm20El2013_REPORT
struct Command command;
unsigned char cv[2];

command.cmd = VAL_START_TESTDIAF;

if (left)
	{
	cv[0] = 1;	
	cv[1] = 0;
	}
else
	{
	cv[0] = 0;
	cv[1] = 1;
	}

command.size = 2;
command.data = (void *) cv;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
//pFrame->SendBroadcastGeneralCommand(&command);
//if(isTopSide())
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
//else
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);

#endif
}

///------------------------------------------------
//
//	Invia Valore durata impulso di allarme
//				   
//------------------------------------------------


void CLineDoc::sendValAlPulse (void)
{
#ifndef Csm20El2013_REPORT
struct Command command;
short sFrom,sTo;

command.cmd = VAL_ALPULSE;

DBParametri dbPar(dBase);
 if (dbPar.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	sFrom = (short) dbPar.m_ALARM_PULSE;

	// Swap data (short format)
	_swab ((char *)&sFrom,(char *)&sTo,1*sizeof(short));

	command.size = 1 * sizeof (short);
	command.data = (void *) &sTo;

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
if(isTopSide())
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
else
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);

//	pFrame->SendBroadcastGeneralCommand(&command);
//	pFrame->SendGeneralCommand(&command);
	dbPar.Close();
	}
#endif
}



///------------------------------------------------
//
//	Invia Valore durata impulso di allarme
//				   solo per top
//------------------------------------------------


void CLineDoc::sendValHwAlarm (bool alarm)
{
#ifndef Csm20El2013_REPORT
struct Command command;
char val;

val = alarm;
command.cmd = VAL_ALHW_OUTPUT;

command.size = 1 * sizeof (char);
command.data = (void *) &val;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
#endif
}



///------------------------------------------------
//
//	Invia Valore output
//				   solo top
//------------------------------------------------


void CLineDoc::sendValOutput (unsigned short v)
{
#ifndef Csm20El2013_REPORT
struct Command command;
short sFrom,sTo;

command.cmd = VAL_OUTPUT;

// Move Init Data from Db to Dialog
sFrom = (short) v;

// Swap data (short format)
_swab ((char *)&sFrom,(char *)&sTo,1*sizeof(short));

command.size = 1 * sizeof (short);
command.data = (void *) &sTo;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
//if(isTopSide())
	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
//else
//	pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);
#endif
}


//------------------------------------
//
//	Invia Input-Output start o stop message
//			broadcast	   
//------------------------------------


void CLineDoc::sendAlMisaligned (BOOL alarm)
{
#ifndef Csm20El2013_REPORT
struct Command command;
char al;

command.cmd = VAL_AL_MISALIGNED;

if (alarm)
	al = 1;
else
	al = 0;

command.size = sizeof (al);
command.data = (void *) &al;

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

//if (alarm)
	pFrame->SendBroadcastGeneralCommand(&command);
//else
//	{
//	if(isTopSide())
//		pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_TOP);
//	else
//		pFrame->SendGeneralCommand(&command,pFrame->RPCID_HC16_BOTTOM);
//	}
#endif
}

//------------------------------------
//
//	Invia comando autotest 
//				   
//------------------------------------


void CLineDoc::sendAutotest (void)
{
#ifndef Csm20El2013_REPORT
// non utilizzato
return;

char al;
// clear stato autotest 
CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->c_stAllarmiTop.clearReceiver();	
// clear allarmi sistema testo
systemAlarm.clearMessages();

struct Command command;
command.cmd = VAL_COMMAND;

al = AUTOTEST_CMD;

command.size = sizeof (al);
command.data = (void *) &al;

pFrame->SendPipeCommand(&command,pFrame->RPCID_HC16_TOP);

#endif

}

//------------------------------------
//
//	Gestione Messaggi watch dog seriale
//				   
//------------------------------------

void CLineDoc::gestValTestSer (PVOID p,DWORD size) // Gestisce arrivo VAL_PERIOD
{

// Received Msg from serial
serMsgReceived = TRUE;

if (!systemOnLine)
	return;

}

//------------------------------------
//
//	Gestione Messagi Stop Acquisizione 
//				   
//------------------------------------

void CLineDoc::gestValStop (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_STOP

// Received Msg from serial
serMsgReceived = TRUE;

if (!systemOnLine)
	return;

// close both processes!!! 
// only this related process,
// era FALSE
if ((c_mode == 0)||(c_mode == 2)||
	((c_mode == 1)&&(isTopSide())))
	FileSaveStop(TRUE);
	
}

//------------------------------------
//
//	Gestione Messagi File Close 
//				   
//------------------------------------

void CLineDoc::gestValClose (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_FILECLOSE


// Received Msg from serial
serMsgReceived = TRUE;

// disabilitato FileSave xChe` doppie stampe automatiche
return;

if (!systemOnLine)
	return;

FileSave();
	
}

//------------------------------------
//
//	Gestione Messagi File Open 
//	( cambio asse al volo )			   
//------------------------------------

void CLineDoc::gestValOpen (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_FILEOPEN
// Received Msg from serial
serMsgReceived = TRUE;

if (!systemOnLine)
	return;

// No test 
//fileStart(FALSE);

// Display empty data
UpdateAllViews(NULL);
	
}

//------------------------------------
//
//	Gestione Messaggi Alzata 
//				   
//------------------------------------

void CLineDoc::gestValAlzata (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_ALZATA
// Received Msg from serial
#ifndef Csm20El2013_REPORT
serMsgReceived = TRUE;

return;
if (!systemOnLine)
	return;

if (size != 1)
	{// errore dimensione 
	return;
	}

int nuovoValAlzata;
nuovoValAlzata = (int ) *((unsigned char *) p);

if ((c_statoAlzata != nuovoValAlzata)&&
	(nuovoValAlzata == 1))
	{// alzata ON
	CDAlzate dialog;
	dialog.DoModal();
	switch(dialog.c_retCode)
		{
		case ALZATA_NUOVA:
			// Nessuna modifica alzata precedente
			// aggiungo alzata 
			c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
			break;
		case ALZATA_CONTINUA:
			break;
		case ALZATA_SCARTO:
			// alzata passata era SCARTO modifico il codice  
			c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).modifyCodeLastAlzata(ALZATA_SCARTO);
			// aggiungo alzata di partenza
			c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_SCARTO);
			break;
		case ALZATA_ELEMENTO:
			// Nuovo elelento
			CDTagliElemento dialog;
			dialog.m_posColtello.Format("%d",getPosColtello(c_rotoloMapping.c_lastElemento+1));
			dialog.c_pElemento = &c_rotoloMapping[c_rotoloMapping.c_lastElemento+1];
			dialog.m_elemento.Format("Elemento %d",c_rotoloMapping.c_lastElemento+2);
			if (dialog.DoModal() == IDCANCEL)
				return;
			//
			// Ok aggiorno ultimo elemento lavorato
			c_rotoloMapping.c_lastElemento += 1;
			// aggiungo alzata di partenza
			c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
			// 
			//------------------------------------------------
			// aggiornamento dbase elementi
			CDbElementi dbElementi(dBase);
	
			if (!dbElementi.openSelectNome((LPCSTR)c_rotolo,c_rotoloMapping.c_lastElemento,TRUE))
				{// non ancora memorizzati
				dbElementi.appendElemento((LPCSTR)c_rotolo,
					c_rotoloMapping.c_lastElemento,
					&c_rotoloMapping[c_rotoloMapping.c_lastElemento]);
				}
			break;
		}
		
	}
c_statoAlzata = nuovoValAlzata;
// Display empty data
UpdateAllViews(NULL);
#endif
}


//------------------------------------
//
//	Gestione Messagi Diaframma 
//				   
//------------------------------------

void CLineDoc::gestValDiaframma (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_DIAFRAMMA
// Received Msg from serial
serMsgReceived = TRUE;

// posizione del diaframma attivo anche fuori ispezione
// per test IO
//if (!systemOnLine)
//	return;


if (size != 2)
	{// errore dimensione 
	return;
	}

unsigned short from = *((unsigned short *) p);
unsigned short to;
_swab((char *)&from,(char *)&to,2);


c_posDiaframma = (double ) to;
}

//------------------------------------
//
//	Gestione Messagi stato Hw Input 
//				   
//------------------------------------

void CLineDoc::gestValInput (PVOID p,DWORD size) 
{// Gestisce arrivo VAL_DIAFRAMMA
// Received Msg from serial
serMsgReceived = TRUE;


if (size != 2)
	{// errore dimensione 
	return;
	}

unsigned short from = *((unsigned short *) p);
unsigned short to;
_swab((char *)&from,(char *)&to,2);


c_valInput = to;

}

//------------------------------------
//
//	Gestione Messagi Diaframma 
//				   
// no trim sui diaframmi!
//------------------------------------

void CLineDoc::gestValExtDiaframma (PVOID p,DWORD size,BOOL fromBottom) 
{// Gestisce arrivo VAL_DIAFRAMMA
// Received Msg from serial
serMsgReceived = TRUE;

if (size != 4)
	{// errore dimensione 
	return;
	}

unsigned short* sp;

sp = (unsigned short *) p;
unsigned short from = *sp;
unsigned short to;
_swab((char *)&from,(char *)&to,2);
double tmpSxPos;
tmpSxPos = (double ) to;

sp ++;
from = *sp;
_swab((char *)&from,(char *)&to,2);
double tmpDxPos;
tmpDxPos = 1. * SIZE_BANDE * c_difCoil.getNumSchede() - (double )to;
double relTmpDxPos = (double ) to;

BOOL update = FALSE;
if (!fromBottom)
{
if (tmpSxPos != c_posDiaframmaSx)
	update = TRUE;

if (tmpDxPos != c_posDiaframmaDx)
	update = TRUE;
}
// 10 secondi di delay dopo chiusura ispezione
CTime time = CTime::GetCurrentTime();
CProfile profile;
int checkLeft = profile.getProfileInt ("Shutter","RestPositionLeft",-15);
int checkRight = profile.getProfileInt ("Shutter","RestPositionRight",-15);
int checkDelta = profile.getProfileInt ("Shutter","RestPositionDelta",1);

if(fromBottom)
{
if ((!systemOnLine)&&
	((time.GetTime()-c_stopTime) > 60))
	{// check value of left and right shutter calibration

	if ((tmpSxPos<(checkLeft-checkDelta))||
		((tmpSxPos>(checkLeft+checkDelta))))
		{
		if (!c_alarmCheckShutterLeftActiveBot)
			{
			CString allarmeSistema;
			CString strRes;
			allarmeSistema.Format("Warning: SHUTTER LEFT NEED CALIBRATION!\r\n");
			systemAlarm.appendMsg(allarmeSistema);
			c_alarmCheckShutterLeftActiveBot = TRUE;
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			systemAlarm.visualizza(pFrame,secAlarmSound);			
			pFrame->c_stAllarmiBottom.updateOtturatore1State(AL_ON);
			}
		}
	else
		{
		if (c_alarmCheckShutterLeftActiveBot)
			{
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			pFrame->c_stAllarmiBottom.updateOtturatore1State(AL_OFF);
			}
		}
	if ((relTmpDxPos<(checkRight-checkDelta))||
		((relTmpDxPos>(checkRight+checkDelta))))
		{
		if (!c_alarmCheckShutterRightActiveBot)
			{
			CString allarmeSistema;
			CString strRes;
			allarmeSistema.Format("Warning: SHUTTER RIGHT NEED CALIBRATION!\r\n");
			systemAlarm.appendMsg(allarmeSistema);
			c_alarmCheckShutterRightActiveBot = TRUE;
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			systemAlarm.visualizza(pFrame,secAlarmSound);
			pFrame->c_stAllarmiBottom.updateOtturatore2State(AL_ON);
			}
		}
	else
		{
		if (c_alarmCheckShutterRightActiveBot)
			{
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			pFrame->c_stAllarmiBottom.updateOtturatore2State(AL_OFF);
			}
		}
	return;
	}
else
	{// in ispezione
	// clear flag alarm
	c_alarmCheckShutterLeftActiveBot = FALSE;
	c_alarmCheckShutterRightActiveBot = FALSE;
	}
return;
}
else
{// fromTop
if ((!systemOnLine)&&
	((time.GetTime()-c_stopTime) > 60))
	{// check value of left and right shutter calibration

	if ((tmpSxPos<(checkLeft-checkDelta))||
		((tmpSxPos>(checkLeft+checkDelta))))
		{
		if (!c_alarmCheckShutterLeftActiveTop)
			{
			CString allarmeSistema;
			CString strRes;
			allarmeSistema.Format("Warning: SHUTTER LEFT NEED CALIBRATION!\r\n");
			systemAlarm.appendMsg(allarmeSistema);
			c_alarmCheckShutterLeftActiveTop = TRUE;
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			systemAlarm.visualizza(pFrame,secAlarmSound);
			pFrame->c_stAllarmiTop.updateOtturatore1State(AL_ON);
			}
		}
	else
		{
		if (c_alarmCheckShutterLeftActiveTop)
			{
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			pFrame->c_stAllarmiTop.updateOtturatore1State(AL_OFF);
			}
		}
	if ((relTmpDxPos<(checkRight-checkDelta))||
		((relTmpDxPos>(checkRight+checkDelta))))
		{
		if (!c_alarmCheckShutterRightActiveTop)
			{
			CString allarmeSistema;
			CString strRes;
			allarmeSistema.Format("Warning: SHUTTER RIGHT NEED CALIBRATION!\r\n");
			systemAlarm.appendMsg(allarmeSistema);
			c_alarmCheckShutterRightActiveTop = TRUE;
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			systemAlarm.visualizza(pFrame,secAlarmSound);
			pFrame->c_stAllarmiTop.updateOtturatore2State(AL_ON);
			}
		}
	else
		{
		if (c_alarmCheckShutterRightActiveTop)
			{
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			pFrame->c_stAllarmiTop.updateOtturatore2State(AL_OFF);
			}
		}
	return;
	}
else
	{// in ispezione
	// clear flag alarm
	c_alarmCheckShutterLeftActiveTop = FALSE;
	c_alarmCheckShutterRightActiveTop = FALSE;
	}
}

if ((!systemOnLine)||
	(systemOnLine&&(this->c_difCoil.getMeter() > c_limUpdateDiaf)))
	return;

if ((c_mode == 1) && fromBottom)
	{
	c_posDiaframmaDx2 = tmpDxPos;
	c_posDiaframmaSx2 = tmpSxPos;
	}
else
	{// modo 0-separatore e 2-singleSheet
	c_posDiaframmaDx = tmpDxPos;
	c_posDiaframmaSx = tmpSxPos;
	}

if (update)
	{
	tagliSelect((CDStripPosition*) NULL,TRUE);
	// update larghezza coil, include anche trimLeft e right
	c_larghezza = (c_posDiaframmaDx - c_posDiaframmaSx)-c_trimLeft-c_trimRight;
	c_difCoil.setLargeSize(c_larghezza);
	fillStripList();
	}

// redraw all 
if (update)
	UpdateAllViews (NULL,0);
}

//------------------------------------------------
// Inizio Lavorazione
//------------------------------------------------

void CLineDoc::OnUpdateFileStart(CCmdUI* pCmdUI) 
{

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();

if (frame->c_isReporting)
	pCmdUI->Enable(FALSE);
else
	pCmdUI->Enable(!systemOnLine);


}

static int mode =0;

// OnFileStart Csm20El2013 con uso file AS400
void CLineDoc::OnFileStart() 
{
#ifndef Csm20El2013_REPORT

// Check su warmUp
CLineDoc *pDoc;
pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentTop();

if(pDoc->getStatoHde(true) == HDE_WARMUP)
	{
	AfxMessageBox("Waiting Warmup,\n Please repeat Start",MB_ICONEXCLAMATION);
	return;
	}

// pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentBottom();
if(pDoc->getStatoHde(false) == HDE_WARMUP)
	{
	AfxMessageBox("Waiting Warmup,\n Please repeat Start",MB_ICONEXCLAMATION);
	return;
	}

// parte sempre prima top
if (!isTopSide())
	{
	CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
	frame->MDINext();
	frame->Pump();
	CDocTemplate* pDocTemplate = GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	CLineDoc *pDoc;
	pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	// stesso?
	if (pDoc->c_side == c_side)
		pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	pDoc->fileStart(FALSE);
	return;
	}
fileStart(FALSE);

#endif
}

// Gestione File start per Test
void CLineDoc::OnFileTstart() 
{
// test mode
fileStart(TRUE);
}



void CLineDoc::fileStart(BOOL testMode) 
{
#ifndef Csm20El2013_REPORT
CString namerotolo,namerotoloExt;
CProfile profile;
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

if (systemOnLine)
	return;

// TODO: Add your command handler code here
// Init Dialog Member Variable

CDocTemplate* pDocTemplate = GetDocTemplate();
POSITION docPos = pDocTemplate->GetFirstDocPosition( );
CLineDoc *pDocBot;
pDocBot = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
// pDocBot = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
if (pDocBot->c_side == c_side)
	pDocBot = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);

CString s;

BOOL continueCoil=0;
if (loadedData || ((c_sideActive==2)&&pDocBot->loadedData))
	{
	//if (AfxMessageBox("Do you want to continue previous coil?",MB_YESNO | MB_DEFBUTTON2)==IDYES)
	CString s;
	s.LoadString(CSM_GRAPHDOC_ASK_CONTINUECOIL);
	if (AfxMessageBox(s,MB_YESNO | MB_DEFBUTTON2)==IDYES)
		{
		continueCoil = 1;
		s.LoadString(CSM_GRAPHDOC_ASK_CONTINUESTRIP);
		// s = "Continue previous strip?";
		// 11-05-2010 eliminata removeLastAlzata
		// c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).removeLastAlzata();
//		if (AfxMessageBox(s,MB_YESNO | MB_DEFBUTTON2)==IDYES)
			{// 27-10-2010 rinserita removelastalzata
			c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).removeLastAlzata();
			}
		/*
		else
			{
			continueCoil = 2;
			// alzata passata era SCARTO modifico il codice  
			c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).modifyCodeLastAlzata(ALZATA_SCARTO);
			// aggiungo alzata nuova
			c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
			}
		*/
		}
	}


if (!continueCoil)
	{
	// Init diaframmi con valori di F1
	c_useFingerHw = profile.getProfileBool("init","UseFingerHw",TRUE);	
	
	// allinea strip su bordo destro, carico da .ini perche` avrei potuto aprire file esterno per report e poi 
	// mettermi a fare ispezione
	c_rightAlignStrip = profile.getProfileBool(_T("init"), _T("AlignStripOnRightBorder"),FALSE);

	if (c_useFingerHw)
		{
		// TODO: Add your command handler code here
		CDStripPosition dialog;
		dialog.c_rightAlignStrip = c_rightAlignStrip;
		// dialog.c_offsetSx = c_offsetSx;
		dialog.loadFromProfile(TRUE);
		dialog.loadFromResource();
		// substitute coil name
		if (testMode)
			dialog.m_coil = "TEST";

		if (dialog.DoModal() != IDOK)
			return;


		// if test !save
		if (!testMode)
			dialog.saveToProfile();
		// load data from dialog
		c_spessore = dialog.m_thickness;
		c_cliente = dialog.m_orderNumber;
		
		
		c_lega = dialog.m_alloyType;
		c_firstRising = dialog.m_firstRising;
		c_mode = dialog.m_mode;
		mode = dialog.m_mode;
		// 1 top, 2 bottom
		c_sideActive = 1;
		// c_sideActive = dialog.m_useBottomHead + 1;
		c_rotolo = dialog.m_coil;
		c_rotolo2 = dialog.m_coil2;
		
		//------------ trim left e right -----------------
		if (c_rightAlignStrip)	
			{// invertiti trim left e right
			c_trimRight = dialog.m_trimLeft;
			c_trimLeft = dialog.m_trimRight;
			}
		else
			{
			c_trimLeft = dialog.m_trimLeft;
			c_trimRight = dialog.m_trimRight;
			}

		// larghezza impostata da shutter
		c_larghezza = (c_posDiaframmaDx - c_posDiaframmaSx)-c_trimLeft-c_trimRight;
		
		// skip A def? 
		c_skipHoleAInspection = dialog.m_skipADef;

		c_numTheoryStrip = dialog.m_numStrip;
		nomeRicetta = dialog.m_EditSel;
		nomeRicetta.TrimRight();
		c_freeNote = dialog.m_freeNote;
		c_freeNote.TrimRight();
		UpdateBaseDoc();

		c_numLanes = c_numTheoryStrip;
		// i marker non sono divisi in classi
		c_markerCoil.clear(c_numLanes,1,0);

		CArray <int,int> stripA;
		dialog.fillStripSizeArray(stripA);
		for (int i=0;i<stripA.GetCount();i++)
			{
			c_valZoneNumberSize[i] = stripA[i];
			}



		// set larghezza per calcolo densita`
		c_difCoil.setLargeSize(c_larghezza);

		CTime time = CTime::GetCurrentTime();
		// save date and time start 
		c_startTime = time.GetTime();
		// Composizione nomi dei File
		namerotolo = getFileName();
		SetTitle ((LPCSTR)namerotolo);

		// init all data structure in new coil job
		// cold start call to tagliSelect che carica da .ini
		// fingerList
		newCoil (COLD_START,&dialog);
		}
	else
		{
		CDF1 dialog;
		dialog.loadFromProfile();
	//	dialog.loadFromResource();
		// substitute coil name
		if (testMode)
			dialog.m_coil = "TEST";

		if (dialog.DoModal() != IDOK)
			return;
		// if test !save
		if (!testMode)
			dialog.saveToProfile();
		// load data from dialog
		c_spessore = dialog.m_thickness;
		c_cliente = dialog.m_orderNumber;
		c_larghezza = dialog.m_width;
		c_lega = dialog.m_alloyType;
		c_firstRising = dialog.m_firstRising;
		c_mode = dialog.m_mode;
		mode = dialog.m_mode;
		// 1 top, 2 bottom
		// 1 top, 2 bottom
		c_sideActive = 1;
		// c_sideActive = dialog.m_useBottomHead + 1;
		c_rotolo = dialog.m_coil;
		c_rotolo2 = dialog.m_coil2;
		
		// skip A def? 
		c_skipHoleAInspection = dialog.m_skipADef;

		c_numTheoryStrip = dialog.m_numStrip;
		nomeRicetta = dialog.m_EditSel;
		nomeRicetta.TrimRight();
		c_freeNote = dialog.m_freeNote;
		c_freeNote.TrimRight();
		UpdateBaseDoc();

		// set larghezza per calcolo densita`
		c_difCoil.setLargeSize(c_larghezza);

		CTime time = CTime::GetCurrentTime();
		// save date and time start 
		c_startTime = time.GetTime();
		// Composizione nomi dei File
		namerotolo = getFileName();
		SetTitle ((LPCSTR)namerotolo);

		// init all data structure in new coil job
		// cold start call to tagliSelect che carica da .ini
		// fingerList
		newCoil (COLD_START);
		}
	}

UpdateBaseDoc();

// set offset as previous meter 
// 0 -> new Coil
// last value if continue
c_baseContinuaRotolo = c_difCoil.getMeter();	

if (testMode)
	systemOnTest = TRUE;
else
	systemOnTest = FALSE;
	

// Indico append su file di Log
c_fileLogPosition = -1;
// Wait at least TOT sec
serMsgReceived = TRUE;
	
// Init Alarm flag
diskAlarmActive = FALSE;
serAlarmActive = FALSE;
c_alarmStripActive = FALSE;
c_alarmCheckShutterLeftActiveTop = FALSE;
c_alarmCheckShutterRightActiveTop = FALSE;
c_alarmCheckShutterLeftActiveBot = FALSE;
c_alarmCheckShutterRightActiveBot = FALSE;

// c_posDiaframmaSx = 200;
// Update Diaframmi con pos iniziali F1
if (!continueCoil)
	{// se continua rotolo non devo inizializzare posDiaf
	if (c_useFingerHw)
		{// F1 ha senso solo con fingher Hw e non continua rotolo
		int lastElem = c_rotoloMapping.c_lastElemento;
		// 
		if (c_rightAlignStrip)
			{// se allineato a dx larghezze crescono verso sinsitra
			c_posDiaframmaSx2 = (c_rotoloMapping[lastElem])[0].c_xPos -
						(c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_larghezza;
			c_posDiaframmaSx  = (c_rotoloMapping[lastElem])[0].c_xPos-
						(c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_larghezza;

			c_posDiaframmaDx2 = (c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_xPos;
			c_posDiaframmaDx  = (c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_xPos;
			}
		else
			{
			c_posDiaframmaSx2 = (c_rotoloMapping[lastElem])[0].c_xPos;
			c_posDiaframmaSx  = (c_rotoloMapping[lastElem])[0].c_xPos;

			c_posDiaframmaDx2 = (c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_xPos+
							(c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_larghezza;
			c_posDiaframmaDx  = (c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_xPos+
				(c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_larghezza;
			}
	// 
		}
	else
		{// init diaframmi
		c_posDiaframmaSx = 0.;
		c_posDiaframmaDx = SIZE_BANDE * c_difCoil.getNumSchede();
		c_posDiaframmaSx2 = 0.;
		c_posDiaframmaDx2 = SIZE_BANDE * c_difCoil.getNumSchede();
		}
	}

if ((c_mode == 2)&&(c_sideActive == 2))
	{// selezionata testa 2
	CDocTemplate* pDocTemplate = GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	CLineDoc *pDoc;
	pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	if (pDoc->c_side == c_side)
		pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	// Send Micro flag use finger Hw
	pDoc->sendUseFingerHw();
	pDoc->sendValDistOddEven();
	pDoc->SlaveStart(this,testMode,continueCoil);	
	return;
	}
// Send Micro Valori soglie
sendValSoglie ();

// Send Micro flag use finger Hw
sendUseFingerHw();
sendValDistOddEven();

// Send Micro Valori FCDL
sendValFcdl ();
// Send Micro Valori Durata impulso allarme
sendValAlPulse ();

// Start Sistema
systemOnLine = TRUE;
// Enable Micro, False-> No test
sendStartStop (TRUE,testMode);

// create second frame
//CDocTemplate* pDocTemplate = GetDocTemplate();
//POSITION docPos = pDocTemplate->GetFirstDocPosition( );
docPos = pDocTemplate->GetFirstDocPosition( );
CLineDoc *pDoc;
pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
if (pDoc != NULL)
	{
	if(c_mode ==0)
		{// Separatore
		// parte sempre con Bottom visualizzato 
		if (isTopSide())
			// qs  e' top prima
			{
			POSITION pos = GetFirstViewPosition();
			CView* pView = GetNextView(pos);
			CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();
			((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,5,TRUE);	
			}
		if (pDoc->c_side != c_side)
			{// OK DIFFERENT
			pDoc->SlaveStart(this,testMode,continueCoil);		
			}
		else
			{// same get next
			pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
			pDoc->SlaveStart(this,testMode,continueCoil);		
			}
		if (!isTopSide())
			// qs e' Bottom dopo
			{
			POSITION pos = GetFirstViewPosition();
			CView* pView = GetNextView(pos);
			CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();
			((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,5,TRUE);	
			}
		}
	else
		{// Alternato
		// modo 1-singleCoil e 2-singleSheet
		POSITION pos = GetFirstViewPosition();
		CView* pView = GetNextView(pos);
		CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();
		((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,1,TRUE);
		// entrambi i documenti set c_mode
		if (pDoc->c_side == c_side)
			pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
		pDoc->c_mode = c_mode;
		
		// clear altri dati su seconda testa
		pDoc->loadedData = FALSE;
		pDoc->systemAlarm.clearMessages();

		loadedData = FALSE;

		pDoc->c_rotolo = "";
		pDoc->SetTitle ("");
	
		if(c_mode == 1)
			{
			// singleSheet double coil
			pDoc->UpdateBaseDoc();
			// Send Micro Valori soglie
			pDoc->sendValSoglie ();
			// Send Micro Valori FCDL
			pDoc->sendValFcdl ();
			// Send Micro Valori Durata impulso allarme
			pDoc->sendValAlPulse ();	
			// Send Micro flag use finger Hw
			pDoc->sendUseFingerHw ();
			pDoc->sendValDistOddEven();

			// Enable Micro, False-> No test
			pDoc->sendStartStop (TRUE,testMode);
			}
		}
	}
//----
#endif
}



// SlaveStart Csm20El2013 come seconda frame 
// copia stessa struttura dati
void CLineDoc::SlaveStart(CLineDoc* pDoc,BOOL testMode,int continueCoil) 
{
CProfile profile;

if (systemOnLine)
	return;


// TODO: Add your command handler code here
// Init Dialog Member Variable
// era != 0 deve essere == 1
// fixbug 27-10-10
if (continueCoil == 1)
	c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).removeLastAlzata();

if (continueCoil==2)
	{
	// alzata passata era SCARTO modifico il codice  
	c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).modifyCodeLastAlzata(ALZATA_SCARTO);
	// aggiungo alzata nuova
	c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
	}

if ((!loadedData)||
	(!continueCoil))
	{
	c_useFingerHw = profile.getProfileBool("init","UseFingerHw",TRUE);	
	c_spessore = pDoc->c_spessore;
	c_cliente = pDoc->c_cliente;
	c_larghezza = pDoc->c_larghezza;
	c_rotolo = pDoc->c_rotolo;
	c_rotolo2 = pDoc->c_rotolo2;
	c_lega = pDoc->c_lega;
	c_firstRising = pDoc->c_firstRising;
	c_mode = pDoc->c_mode;
	c_numTheoryStrip = pDoc->c_numTheoryStrip;
	// 1 top, 2 bottom
	c_sideActive = pDoc->c_sideActive;

	// skip A def? 
	c_skipHoleAInspection = pDoc->c_skipHoleAInspection;


	nomeRicetta = pDoc->nomeRicetta;
	c_freeNote = pDoc->c_freeNote;
	UpdateBaseDoc();
	
	// set larghezza per calcolo densita`
	c_difCoil.setLargeSize(c_larghezza);

	// save date and time start 
	c_startTime = pDoc->c_startTime;
	newCoil(COLD_START);
	}

UpdateBaseDoc();

// set offset as previous meter 
// 0 -> new Coil
// last value if continue
c_baseContinuaRotolo = c_difCoil.getMeter();	

// Composizione nomi dei File

// Indico append su file di Log
c_fileLogPosition = -1;
// Wait at least TOT sec
serMsgReceived = TRUE;
	
// Init Alarm flag
diskAlarmActive = FALSE;
serAlarmActive = FALSE;
c_alarmStripActive = FALSE;
c_alarmCheckShutterLeftActiveTop = FALSE;
c_alarmCheckShutterRightActiveTop = FALSE;
c_alarmCheckShutterLeftActiveBot = FALSE;
c_alarmCheckShutterRightActiveBot = FALSE;

// Update Diaframmi con pos iniziali F1
if (!continueCoil)
	{
	if (c_useFingerHw)
		{// F1 ha senso solo con fingher Hw
		int lastElem = c_rotoloMapping.c_lastElemento;
		c_posDiaframmaSx2 = (c_rotoloMapping[lastElem])[0].c_xPos;
		c_posDiaframmaSx  = (c_rotoloMapping[lastElem])[0].c_xPos;
		// 
		c_posDiaframmaDx2 = (c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_xPos+
						(c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_larghezza;
		c_posDiaframmaDx  = (c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_xPos+
			(c_rotoloMapping[lastElem])[c_rotoloMapping[lastElem].size()-1].c_larghezza;
		// 
		}
	else
		{// init diaframmi
		c_posDiaframmaSx = 0.;
		c_posDiaframmaDx = SIZE_BANDE * c_difCoil.getNumSchede();
		c_posDiaframmaSx2 = 0.;
		c_posDiaframmaDx2 = SIZE_BANDE * c_difCoil.getNumSchede();
		}
	}


CString title = getFileName();
SetTitle ((LPCSTR)title);

// Send Micro Valori soglie
sendValSoglie ();

// Send Micro flag use finger Hw
sendUseFingerHw();
sendValDistOddEven();

// Send Micro Valori FCDL
sendValFcdl ();
// Send Micro Valori Durata impulso allarme
sendValAlPulse ();

// Start Sistema
	
// Modify View Visualizza vista di default
POSITION pos = GetFirstViewPosition();
CView* pView = GetNextView(pos);
CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();

if (c_mode == 0)
	((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,5,TRUE);
else
	((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,1,TRUE);

systemOnLine = TRUE;
// Enable Micro, False-> No test
sendStartStop (TRUE,testMode);

}




void CLineDoc::OnUpdateFileTstart(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
pCmdUI->Enable(!systemOnLine);
	
}





// Eliminata funzione di calcolo larghezze delle singole bande

//-----------------------------------------------
//	Codice comune a OnFileStart
//	e gestFileOpen
//
//
//
// Parametro TRUE per warmStart

/*
void CLineDoc::FileStart(BOOL warmStart ) 
{

// Laminazione sottile non gestito
return;
CTime time = CTime::GetCurrentTime();

// save date and time start 
c_startTime = time.GetTime();
	
// Set Title
CString title,tFormatRes;
tFormatRes.LoadString(CSM_GRAPHDOC_DATEFORMAT);
title = time.Format("%Y-%m-%d");
title += "-";
CString l;
l = c_lega.Left(5);
if (l.GetLength() > 0)
	{
	title += l;
	title += "-";
	}
CString baseFileRotolo;
baseFileRotolo = title;

// save c_rotoloExt
c_rotoloRil = getLastRotoloRil(baseFileRotolo);
c_rotoloRil ++;

title += getRotolo();
title += c_side;

SetTitle ((LPCSTR)title);

// init all data structure in new coil job
newCoil (warmStart);

}
*/

//-----------------------------------------------
// Stop sistema

void CLineDoc::OnUpdateFileStop(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here

// pCmdUI->Enable(systemOnLine&&(!systemLoading));
pCmdUI->Enable(systemOnLine);
	
}


// Comando di chiusura
void CLineDoc::OnFileStop() 
{
// TODO: Add your command handler code here

if (!systemOnLine)
	return;

if (!systemOnTest)
	{
	if (!CPasswordDlg::CheckPassword())
		{
		return;
		}
	}


CString msgRes;
msgRes.LoadString (CSM_GRAPHDOC_STOPINSP);
// if (AfxMessageBox("STOP INSPECTION\nPress OK to STOP INSPECTION\nPress CANCEL to CONTINUE",MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
if (AfxMessageBox(msgRes,MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
	{
	// Sistema in linea
	systemOnLine = TRUE;
	return;
	}
// Disable Micro
sendStartStop (FALSE,FALSE);
if (c_mode == 0)
	{
	// Both Process
	FileSaveStop(TRUE); 
	}
else
	{// modo 1-singleCoil e 2-singleSheet
	// only first Process
	FileSaveStop(FALSE); 
	}
	
}


// close current inspection
void CLineDoc::FileSaveStop(BOOL bothProcess) 
{
// TODO: Add your command handler code here
#ifndef Csm20El2013_REPORT

if (!systemOnLine)
	return;

// Sistema Fuori linea
systemOnLine = FALSE;
// Codice comune con il test
systemOnTest = FALSE;

CTime time = CTime::GetCurrentTime();
// save date and time stop
c_stopTime = time.GetTime();

// aggiungo alzata di fine
c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);


// get second frame and stop it
if (bothProcess)
	{
	CLineDoc *pDoc = NULL;

	if (this->isTopSide())
		pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
	else	
		pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();

	pDoc->SlaveSaveStop(this);		
	Sleep(1000);
	}

// Save Data
FileSave();
Sleep(1000);

// Videata iniziale
POSITION pos = GetFirstViewPosition();
CView* pView = GetNextView(pos);
CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();

((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,0,TRUE);	

#endif
}


// close alternate inspection
void CLineDoc::SlaveSaveStop(CLineDoc* pDoc) 
{
// TODO: Add your command handler code here

if (!systemOnLine)
	return;

// Sistema Fuori linea
systemOnLine = FALSE;
// Codice comune con il test
systemOnTest = FALSE;

CTime time = CTime::GetCurrentTime();
// save date and time stop 
c_stopTime = time.GetTime();

// aggiungo alzata di fine
c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);

// Disable Micro
sendStartStop (FALSE,FALSE);

// Save Data
FileSave();

// Videata iniziale
POSITION pos = GetFirstViewPosition();
CView* pView = GetNextView(pos);
CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();

((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,0,TRUE);
	
}

void CLineDoc::FileSave(void) 
{

#ifndef Csm20El2013_REPORT

CProfile profile;

// Idati sono stati caricati RealTime
loadedData = TRUE;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
frame->progressCreate();
frame->progressSetRange(0, 8);
frame->progressSetStep(2);
CProgressCtrl *progress;
progress = frame->getProgressCtrl();


// Save Data  only for new Data
CFileBpe cf;
CFileException e;
CString title;
title = getMonthFolder();
//CTime st(c_startTime);
//title = _T("DD") + st.Format ("%m%y");

SECURITY_ATTRIBUTES	 sa;

sa.nLength= sizeof(SECURITY_ATTRIBUTES);
sa.lpSecurityDescriptor=NULL;
sa.bInheritHandle = TRUE;

// Show Progress
frame->progressStepIt();

CString defDir;
defDir = profile.getProfileString("init","RootDir",".");
defDir += "\\";
defDir += title;
::CreateDirectory((LPCTSTR)defDir,&sa);
defDir += _T("/");
defDir += GetTitle() + _T(".csm");
if(!cf.Open((LPCTSTR) defDir, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, &e ) )
	{
	CString s;
	s.Format("File could not be opened %d",e.m_cause);
	AfxMessageBox(s);
	}
else
	{
	save(&cf,progress);
	CDocTemplate* pDocTemplate = GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	CLineDoc *pDoc;
	pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	if (pDoc != NULL)
		{
		save(&cf,progress);
		}

	cf.Close();
	}
  
// Show Progress
frame->progressStepIt();

CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
ASSERT_VALID(pFrame);
ASSERT(pFrame->IsFrameWnd());
//pFrame = pFrame->GetActiveFrame();

//pApp->pDocTemplate->ReplaceView(pFrame,0,TRUE);
POSITION pos = GetFirstViewPosition();
CView* pView = GetNextView(pos);
CFrameWnd* pChildFrame = (CFrameWnd*) pView->GetParent();

((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pChildFrame,0,TRUE);

Sleep(50);
pFrame->Pump();

CInitView *view;
POSITION wPos;
wPos = GetFirstViewPosition();
view = (CInitView *) GetNextView(wPos);


if(isTopSide())
	view->launchReportSave(1000);
else
	view->launchReportSave(800);
	

frame->progressStepIt();
// Close Progress
frame->progressDestroy();

return;



#endif 

}



void CLineDoc::OnViewPeriodici() 
{

}

void CLineDoc::OnViewTrend() 
{
// TODO: Add your command handler code here

}
void CLineDoc::OnViewSystem() 
{
	
}


void CLineDoc::OnUpdateViewPeriodici(CCmdUI* pCmdUI) 
{

// TODO: Add your command update UI handler code here
pCmdUI->Enable(loadedData || systemOnLine);
// pCmdUI->SetCheck(periodAlarm.isActive ());
	
}

void CLineDoc::OnUpdateViewTrend(CCmdUI* pCmdUI) 
{

// TODO: Add your command update UI handler code here

pCmdUI->Enable(loadedData || systemOnLine);
// pCmdUI->SetCheck(densitaAlarm.isActive ());
	
}


void CLineDoc::OnUpdateViewSystem(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
pCmdUI->Enable(loadedData || systemOnLine);
// pCmdUI->SetCheck(systemAlarm.isActive ());
	
}

void CLineDoc::OnCloseDocument() 
{
	// TODO: Add your specialized code here and/or call the base class
CDocument::OnCloseDocument();
}

BOOL CLineDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	// TODO: Add your specialized code here and/or call the base class
if (systemOnLine)
	{	
	CString msgRes;
	msgRes.LoadString (CSM_GRAPHDOC_EXITSTOPINSP);
//	if (AfxMessageBox("WARNING !!! The System is running\nPress OK to STOP the inspection\nPress CANCEL to continue the inspection",MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
	if (AfxMessageBox(msgRes,MB_OKCANCEL|MB_DEFBUTTON2|MB_ICONEXCLAMATION) == IDCANCEL)
		return FALSE;
	OnFileStop();
	// se ancora online = premuto cancel
	return (!systemOnLine);
	}
	
return CDocument::CanCloseFrame(pFrame);
}



// Una volta formato il nome base del c_rotolo 
// Viene cercato il file con stesso nome ed estensione piu` 
// alta 
//			xxxxxxxxx_5.csm
// queste indicano le rilavorazioni

int CLineDoc::getLastRotoloRil(CString &baseRotolo)
{

CTime st(c_startTime);
CString dir = _T("DD") + st.Format ("%m%Y");

char curDir [256];
if (!GetCurrentDirectory (255,curDir))
	return (0);

if (!SetCurrentDirectory(dir))
	return (0);	// directory !exist

CFileFind fileSearch;
CString searchString;
searchString = baseRotolo;
// add ext wildCard
searchString += "_*.csm";

int rilavorazione = 0;

if (fileSearch.FindFile (searchString))
	{
	BOOL go = TRUE;
	while (go)
		{
		go = fileSearch.FindNextFile();
		if (!fileSearch.IsDirectory())
			{
			CString fileName;
			fileName = fileSearch.GetFileName();
			// Estraggo estensione numerica
			CString sr (fileName.Right(4));
			sr.MakeUpper();
			if (sr == ".CSM")
				{
				fileName = fileName.Left(fileName.GetLength()-4);
				int i = 0;
				while (isdigit(fileName[fileName.GetLength()-i-1]))
					{
					i++;
					if (i >= fileName.GetLength())
						break;
					}
				if (i>0)
					{
					int vExt;
					sscanf(fileName.Right(i),"%d",&vExt);
					if (vExt > rilavorazione)
						rilavorazione = vExt;
					}
				}
			}
		}
	}
fileSearch.Close();

SetCurrentDirectory(curDir);

return (rilavorazione);
}

// estrazione e visualizzazione tagli per bobine 
BOOL CLineDoc::tagliSelect(BOOL changeLast,BOOL forceAdd)
{

// inserisce alzata con strip ricavati da c_fingerList 
	
// almeno una	
//   ORA TUTTA qs PARTE e` automatica

CElemento elemento;
double pos,size;
double startBobina;
pos = 0;
startBobina = 0;
long index = 1;
BOOL fingerActive = FALSE;

FingerList tmpFingerList;

tmpFingerList.Copy(c_fingerList);

bool vf[300];
bool vf1[300];
bool vf2[300];

if (c_mode == 1)
	{// merge delle due fingerList
	int maxIndex = (min(tmpFingerList.GetSize(),c_fingerList2.GetSize()));
	for (int i=0;i<maxIndex;i++)
		// and
		{
		vf1[i] = tmpFingerList[i];
		vf2[i] = c_fingerList2[i];

// 13-10-05 -------------------
// testa1   111111000011110000111111 
// testa2   111111111100001111000011 
// Facendo solo and non vedo divisioni tra strip con finger size 0	
// inserito xor tra celle contigue stessa testa
// 27-04-10 Modificato per evitare di stringere gli strip
		if(i < (maxIndex -1))
			tmpFingerList[i] = (tmpFingerList[i] && c_fingerList2[i])||
								(
								(!(tmpFingerList[i+1] && c_fingerList2[i]+1)) // ! nuovo !
								&&
								((tmpFingerList[i] ^ tmpFingerList[i+1])||
								(c_fingerList2[i] ^ c_fingerList2[i+1]))
								);
//			tmpFingerList[i] = (tmpFingerList[i] && c_fingerList2[i])||
//							((tmpFingerList[i] ^ tmpFingerList[i+1])||
//							 (c_fingerList2[i] ^ c_fingerList2[i+1]));

		else

			tmpFingerList[i] = (tmpFingerList[i] && c_fingerList2[i]);

		vf[i] = tmpFingerList[i];
		}

	}

for (int i=0;i<tmpFingerList.GetSize();i++)
	{
	if (i == 0)
		{
		pos = c_offsetSx; // posizione rispetto al riferimento
		size = 0.;
		}
	if (tmpFingerList[i] == 0)
		{
		if (fingerActive)
			{// fine finger inizia bobina
			fingerActive = FALSE;
			size = SIZE_BANDE;
			startBobina = pos ;
			// se uso diaframmi -> clipping sul diaframma
			double leftDiaf = min(c_posDiaframmaSx,c_posDiaframmaSx2);
			if (c_useDiaframmi && (startBobina < leftDiaf))
				startBobina = leftDiaf;
			}
		else
			{// continua larghezza bobina 
			size += SIZE_BANDE;
			}
		}
	else
		{// fingerList[i] == 1 
		if (fingerActive)
			{// continua finger
			;
			}
		else
			{// inizia nuovo finger 
			if (size > 0.)
				{// chiudo bobina
				CBobina bobina;
				bobina.c_larghezza = size; // 
				bobina.c_xPos = startBobina;
				// se uso diaframmi -> clipping sul diaframma
				double rightDiaf = max(c_posDiaframmaDx,c_posDiaframmaDx2);
				if (c_useDiaframmi && (bobina.c_xPos+bobina.c_larghezza > rightDiaf))
					bobina.c_larghezza = rightDiaf - bobina.c_xPos;
				bobina.c_posizione = index ++;
				elemento.push_back(bobina);
				}
			// inizia nuovo finger
			fingerActive = TRUE;
			size = 0.;
			}	
		}
	pos += SIZE_BANDE;	
	}

if ((fingerActive == FALSE)&&
	(size > 0.))
	{// esisteva strip in formazione che non e` stato chiuso
	// caso di diaframma dx all'estrema destra
	// chiudo bobina
	CBobina bobina;
	bobina.c_larghezza = size; // 
	bobina.c_xPos = startBobina;
	// clipping sul diaframma se in uso
	double rightDiaf = max(c_posDiaframmaDx,c_posDiaframmaDx2);
	if (c_useDiaframmi && (bobina.c_xPos+bobina.c_larghezza > rightDiaf))
		bobina.c_larghezza = rightDiaf - bobina.c_xPos;
	bobina.c_posizione = index ++;
	elemento.push_back(bobina);
	}
	
	
if (elemento.size() == 0)
	{	
	CBobina bobina;
	bobina.c_larghezza = size+ SIZE_BANDE/2; // 
	bobina.c_xPos = startBobina;
	bobina.c_posizione = index ++;
	elemento.push_back(bobina);
	}

// Eliminato 02-05-08 
//if (c_rotoloMapping.c_reverseBobinePosition)
//	{
//	for (int i=0;i<elemento.size();i++)
//		{
//		// reverse posizione
//		elemento[i].c_posizione = elemento.size() - i;
//		}
//	}

if (c_difCoil.getMeter()> (200. + c_baseContinuaRotolo))
	{
	if (elemento.size() != 	c_numTheoryStrip)
		{// discrepanza tra numero strip teorici e rilevati
		if (!c_alarmStripActive)
			{
			CString allarmeSistema;
			CString strRes;
			//strRes.LoadString(CSM_MAINFRM_SPDISK);
			allarmeSistema.Format("Pos: %3.0lf Strip Number detected (%d) Differs from Strip Number Typed (%d)\r\n",
				c_difCoil.getMeter(),elemento.size(),c_numTheoryStrip);

			systemAlarm.appendMsg(allarmeSistema);
			c_alarmStripActive = TRUE;
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			systemAlarm.visualizza(pFrame,secAlarmSound);
			}
		}
	else
		{
		if (c_alarmStripActive)
			{
			CString allarmeSistema;
			CString strRes;
			//strRes.LoadString(CSM_MAINFRM_SPDISK);
			allarmeSistema.Format("Pos: %3.0lf Strip Number detected Equal to Strip Number Typed\r\n",
				c_difCoil.getMeter());

			systemAlarm.appendMsg(allarmeSistema);
			c_alarmStripActive = FALSE;
			CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
			systemAlarm.visualizza(pFrame,secAlarmSound);
			}
		}
	}

// confronto tra vecchi e nuovo elemento
if ((!forceAdd)&&
	(c_rotoloMapping.size()>0)&&(c_rotoloMapping.back() == elemento))
	return FALSE;

if (changeLast)
	{
	if(c_rotoloMapping.size()>0)
		{
		// remove last elemento
		c_rotoloMapping.back().assignBobine(elemento);
		}
	}
else
	c_rotoloMapping.push_back(elemento);

return TRUE;
}

// estrazione e visualizzazione tagli per bobine 
BOOL CLineDoc::tagliSelect(CDStripPosition* pDialog,BOOL changeLast)
{

// Le dimensioni delle strip e dei finger sono memorizzati nella stringa 
// CDStripPosition::c_stripListRow1
CDStripPosition dlg;
dlg.c_rightAlignStrip = c_rightAlignStrip;

// almeno una	
if (pDialog == NULL)
	{
	dlg.loadFromProfile(FALSE);
	dlg.loadFromResource();
	pDialog = &dlg;
	}
CString str = pDialog->c_stripListRow1;
/*
if (pFrame->c_rightAlignStrip)	
	{// e` stata girata per visualizzarla dx-sx ora rigiriamo di nuovo
	// reverse strip from left to right
	CString stripListRow1;
	int commaPos;
	stripListRow1.LoadString(CSM_STRIPLISTSIZE);
	if (str.GetAt(str.GetLength()-1) == ',')
		str = str.Left(str.GetLength()-1);
	while((commaPos = str.ReverseFind(','))>=0)
		{
		stripListRow1 += str.Right(str.GetLength()-commaPos);
		str = str.Left(commaPos);
		}
	str = stripListRow1;
	}

*/

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	

CString subStr;

CElemento elemento;

double pos,size;
pos = 0;
long index = 1;
int i = 0;
while((subStr = str.SpanExcluding(",")) != "")
	{
	if (i == 0)
		{// pos = dialog.m_firstCutter + LEFTCUTTER_REFERENCE; // intestazione riga
		// pos = pDialog->m_firstCutter - c_offsetSx; // posizione rispetto al riferimento 
		// pos = 0;
		if (c_rightAlignStrip)	
			{
			pos =  c_posDiaframmaDx - pDialog->m_trimLeft;
			}
		else
			{
			pos = pDialog->m_trimLeft + c_posDiaframmaSx;
			}
		}
	// 
	else
		{
		sscanf(subStr,"%lf",&size);
		// if (i % 2)
		// ora solo bobine, niente finger
			{// ok aggiungiamo vero 
			if (c_rightAlignStrip)	
				{
				CBobina bobina;
				bobina.c_larghezza = size; // pos - lastPos;
				bobina.c_xPos = pos;
				bobina.c_posizione = index ++;
				elemento.insert(elemento.begin(),bobina);
				pos -= size;
				}
			else
				{
				CBobina bobina;
				bobina.c_larghezza = size; // pos - lastPos;
				bobina.c_xPos = pos;
				bobina.c_posizione = index ++;
				elemento.push_back(bobina);
				pos += size;
				}
			}
		}
	str = str.Right(str.GetLength() - (subStr.GetLength() + 1));
	i++;
	}

if (c_rightAlignStrip)	
	{// flip list
	for (int i=0;i<elemento.size();i++)
		{
		//elemento.at(i).c_posizione = i+1;		
		}
	}

int nBob = elemento.size();

// aggiunto changeLast
if (changeLast)
	{
	if(c_rotoloMapping.size()>0)
		{
		// remove last elemento
		c_rotoloMapping.back().assignBobine(elemento);
		}
	}
else
	c_rotoloMapping.push_back(elemento);

return TRUE;

}


double CLineDoc::calcLarghezzaNastri(int elemento)
{
double larghTot = 0.;
for (int i=0;i<c_rotoloMapping.at(elemento).size();i++)
	{
	larghTot += c_rotoloMapping.at(elemento).at(i).c_larghezza;
	}
return (larghTot);
}


BOOL CLineDoc::checkLarghezza()
{
double larghTot = calcLarghezzaNastri(c_rotoloMapping.c_lastElemento+1);

CString s;
s.Format("    LARGHEZZA ROTOLO      \n VERIFICARE che sia MAGGIORE od UGUALE a %4.0lf (mm)",larghTot);

if (AfxMessageBox((LPCSTR)s,MB_ICONQUESTION | MB_YESNO) == IDYES)
	return TRUE;

return FALSE;
}



void CLineDoc::OnFileAlzata() 
{
#ifndef Csm20El2013_REPORT

CLineDoc* pDoc;

if(c_mode == 0)
	{
	if (isTopSide())
		pDoc = ((CCSMApp*)AfxGetApp())->GetDocumentBottom();
	else
		pDoc = ((CCSMApp*)AfxGetApp())->GetDocumentTop();
	}
else
	// modo 1-singleCoil e 2-singleSheet
	pDoc = NULL;

// disable signal from plant
// FixBug: 12-11-07 
// Eliminato blocco della ricezione-> assi con lunghezze differenti 
// systemOnLine = FALSE;

BOOL canBeNewElem = FALSE;
// TODO: Add your command handler code here


CDAlzataDettagli dialog;
dialog.c_pDocS = ((CCSMApp*)AfxGetApp())->GetDocumentBottom();
dialog.c_pDocZ = ((CCSMApp*)AfxGetApp())->GetDocumentTop();


// Use PdfCreator section
CString folder;
folder = getMonthFolder();
//CTime st(c_startTime);
//folder = _T("DD") + st.Format ("%m%y");


// pdf Printer section
BOOL usePdfFile = FALSE;
CMainFrame* pMainFrame;
pMainFrame = (CMainFrame*) AfxGetMainWnd();

pMainFrame->c_pdfInfo.loadFromProfile();
if (((CCSMApp*)AfxGetApp())->GetDefaultPrinter() == MICROSOFT_PDF_PRINTER)
	usePdfFile = TRUE;
//-------------------------

int retCode= dialog.DoModal();
switch(retCode)
	{
	case ALZATA_NUOVA:
		{// alzata passata era nuova OK nessuna modifica 
		resetClassBFilterList();

		CMDIChildWnd* childWin = pMainFrame->MDIGetActive();
		// aggiungo alzata nuova
		c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
		if (pDoc != NULL)
			pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).appendAlzata(pDoc->c_difCoil.getMeter(),ALZATA_NUOVA);
		canBeNewElem = TRUE;
		// gestire stampe degli strip
		DBOpzioni dbOpzioni(dBase);
		if (dbOpzioni.openSelectDefault ())
			{
			// switch view
			{// this
				
			POSITION pos = GetFirstViewPosition();
			CView* pView = GetNextView(pos);
			int curViewId = ((CDynViewDocTemplate*) GetDocTemplate( ))->GetViewID(pView);
			CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();
			((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,0,TRUE);

			CInitView *view;
			POSITION wPos;
			wPos = GetFirstViewPosition();
			view = (CInitView *) GetNextView(wPos);

			// txt strip report
			int numStrip = c_rotoloMapping[c_rotoloMapping.c_lastElemento].size();
			int alzataNumber = c_rotoloMapping[c_rotoloMapping.c_lastElemento].c_alzate.size()-2;
			for (int i=0;i<numStrip;i++)
				{
				view->saveDefectTxtStripReport(i,alzataNumber);
				view->saveDefectXlsStripReport(i,alzataNumber);
				}
			// 

			if (dbOpzioni.m_STAMPA_AUTO_STRIP)
				{
				// Stampa automatica parte solo testo
//				view->onlyTextReport = TRUE;
				view->directPrint = TRUE; 	
				// print strip		
				view->c_doPrintStrip = TRUE; 
				view->c_prElementNumber = c_rotoloMapping.c_lastElemento;
				// -1 per arrivare ultimo indice, altro -1 per prendere indice di partenza
				view->c_prAlzataNumber = c_rotoloMapping[c_rotoloMapping.c_lastElemento].c_alzate.size()-2;

				// loop on print for every strip
				int numStrip = c_rotoloMapping[c_rotoloMapping.c_lastElemento].size();
				CString pdfName;
				CString titlePdf;
				for (int i=0;i<numStrip;i++)
					{
					// pdf Printer section
					if (usePdfFile)
						{
						pdfName = getFileStripName(i,alzataNumber);
						titlePdf = pMainFrame->c_pdfInfo.getPdfFilePath(pdfName,folder);
						view->setDocPath(titlePdf);
						}
					else
						view->setDocPath(_T(""));
					//--
					view->c_prStripNumber = i;
					view->OnFilePrint();
				//---------
					}
				// report Completo
				view->c_doPrintStrip = FALSE; 	
//				view->onlyTextReport = FALSE;
				view->directPrint = FALSE; 	
				}
			// flag allarme densita`
			// clearAllarmi densita`
			c_densityAlarmActive = FALSE;
			((CDynViewDocTemplate*) GetDocTemplate( ))->ReplaceView(pFrame,curViewId,TRUE);
			}

			if (pDoc != NULL)
				{// other document
				POSITION pos = pDoc->GetFirstViewPosition();
				CView* pView = pDoc->GetNextView(pos);
				int curViewId = ((CDynViewDocTemplate*) pDoc->GetDocTemplate( ))->GetViewID(pView);
				CFrameWnd* pFrame = (CFrameWnd*) pView->GetParent();
				((CDynViewDocTemplate*) pDoc->GetDocTemplate( ))->ReplaceView(pFrame,0,TRUE);

				CInitView *view;
				POSITION wPos;
				wPos = pDoc->GetFirstViewPosition();
				view = (CInitView *) pDoc->GetNextView(wPos);
				// txt strip report
				int numStrip = pDoc->c_rotoloMapping[c_rotoloMapping.c_lastElemento].size();
				int alzataNumber = pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].c_alzate.size()-2;
				CString pdfName;
				CString titlePdf;
				for (int i=0;i<numStrip;i++)
					{
					view->saveDefectTxtStripReport(i,alzataNumber);
					view->saveDefectXlsStripReport(i,alzataNumber);
					} 

				if (dbOpzioni.m_STAMPA_AUTO_STRIP)
					{
					// Stampa automatica parte solo testo
//					view->onlyTextReport = TRUE;
					view->directPrint = TRUE; 	
					// print strip		
					view->c_doPrintStrip = TRUE; 
					view->c_prElementNumber = pDoc->c_rotoloMapping.c_lastElemento;
					// -1 per arrivare ultimo indice, altro -1 per prendere indice di partenza
					view->c_prAlzataNumber = pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].c_alzate.size()-2;
					// loop on print for every strip
					int numStrip = pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].size();
					for (int i=0;i<numStrip;i++)
						{
						// pdf Printer section
						if (usePdfFile)
							{
							pdfName = pDoc->getFileStripName(i,alzataNumber);
							titlePdf = pMainFrame->c_pdfInfo.getPdfFilePath(pdfName,folder);
							view->setDocPath(titlePdf);
							}
						else
							view->setDocPath(_T(""));
						//--
						view->c_prStripNumber = i;
   						view->OnFilePrint();
						// pdf Printer section
							//----	
						}
					// report Completo
					view->c_doPrintStrip = FALSE; 	
//					view->onlyTextReport = FALSE;
					view->directPrint = FALSE; 	
					}
				// flag allarme densita`
				pDoc->c_densityAlarmActive = FALSE;
				((CDynViewDocTemplate*) pDoc->GetDocTemplate( ))->ReplaceView(pFrame,curViewId,TRUE);
				// torno sulla pagina corrente
				//pMainFrame->MDINext();
				pMainFrame->MDIActivate(childWin);

				}
			}
			
		dbOpzioni.Close();
		}
		//
		break;
	case ALZATA_CONTINUA:
		break;
	case ALZATA_SCARTO:
		// alzata passata era SCARTO modifico il codice  
		resetClassBFilterList();
		c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).modifyCodeLastAlzata(ALZATA_SCARTO);
		if (pDoc != NULL)
			pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).modifyCodeLastAlzata(ALZATA_SCARTO);
		// aggiungo alzata nuova
		c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
		if (pDoc != NULL)
			pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).appendAlzata(pDoc->c_difCoil.getMeter(),ALZATA_NUOVA);
		canBeNewElem = TRUE;
		break;
	}

/* Determinazione nuovi elementi in automatico non manuale */
#ifdef USE_NEW_ELEMENT
if (canBeNewElem)
	{// un elemento ogni nuova alzata!!!
	if(c_useFingerHw)
		{// due versioni diverse di tagliSel
		tagliSelect((CDStripPosition*) NULL,FALSE);
		if (pDoc != NULL)
			pDoc->tagliSelect((CDStripPosition*)NULL,FALSE);
		}
	else	
		{
		tagliSelect(FALSE,TRUE);
		if (pDoc != NULL)
			pDoc->tagliSelect(FALSE,TRUE);
		}
	c_rotoloMapping.c_lastElemento += 1;
	if (pDoc != NULL)
		pDoc->c_rotoloMapping.c_lastElemento += 1;
	// Ok aggiorno ultimo elemento lavorato
	// aggiungo alzata di partenza
	c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).appendAlzata(c_difCoil.getMeter(),ALZATA_NUOVA);
	if (pDoc != NULL)
		pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).appendAlzata(pDoc->c_difCoil.getMeter(),ALZATA_NUOVA);

	//c_rotoloMapping.at(c_rotoloMapping.c_lastElemento).c_firstRising = 
	//	c_rotoloMapping.at(c_rotoloMapping.c_lastElemento-1).getCurAlzNumber(-1);
	//if (pDoc != NULL)
	//	pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_firstRising = 
	//	pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento-1).getCurAlzNumber(-1);

	// init all data structure in new coil job
	if(!c_useFingerHw)
		{
		fillFingerList(c_fingerList);
		if (pDoc != NULL)
			pDoc->fillFingerList(pDoc->c_fingerList);
		}

	CString s;
	//s.Format("Modificare Posizioni dei finger?");
	
	s.LoadString (ID_GRAPHDOC_MODIFYFINGER);
	if (AfxMessageBox((LPCSTR)s,MB_ICONQUESTION | MB_YESNO |MB_DEFBUTTON2 )==IDYES)
		{
		// Nuovo elemento
		CDNumStrip dialogStrip;
		dialogStrip.m_numStrip = c_numTheoryStrip;
		// bloccare edit dei campi identif coil
		if (dialogStrip.DoModal() == IDOK)
			{
			c_numTheoryStrip = dialogStrip.m_numStrip;
			if (pDoc != NULL)
				pDoc->c_numTheoryStrip = dialogStrip.m_numStrip;
			}
		}

	}
#endif

// enable signal from plant
// 12-11-07 FixBug V.
// systemOnLine = TRUE;

#endif
UpdateAllViews(NULL);
}


// Larghezza Totale 27 *  NumeroBande 
// prima
// Dist Bordo Sx = (LargTot - Sommatoria nastri) / 2
// Ora si allinea a sx
int CLineDoc::getPosColtello(int elemento)
{
int v;

// v = ( 27 * c_difCoil.getNumSchede() - calcLarghezzaNastri(elemento))/2;
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
if (c_rightAlignStrip)	
	{
	int posLast = c_rotoloMapping.at(elemento).size()-1;
	v = c_rotoloMapping.at(elemento).at(posLast).c_xPos;
	//v = c_difCoil.getNumSchede() * SIZE_BANDE - (c_rotoloMapping.at(elemento).at(0).c_xPos+c_rotoloMapping.at(elemento).at(0).c_larghezza);
	}
else
	v = c_rotoloMapping.at(elemento).at(0).c_xPos;
return (v);
}

void CLineDoc::OnUpdateFileAlzata(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(systemOnLine);
}


// Genero vettore di dimensione numSchede con tutti i 
// valori a zero cioe` bande tutte abilitate
BOOL	CLineDoc::fillFingerList(FingerList& fingerList)
{
int lastElem;
lastElem = c_rotoloMapping.c_lastElemento;

// save exception if .ini empty
int sz = c_rotoloMapping[lastElem].size();
if (sz == 0)
	return FALSE;

// In realta` StripPos
FingerPos* pFingerPos;
pFingerPos = new FingerPos[c_rotoloMapping[lastElem].size()+1];
fingerList.RemoveAll();

// per ogni strip o bobina ho memorizzato posizione x e dimensione
// 
int i;
for (i=0;i<=c_rotoloMapping[lastElem].size();i++)
	{
	if (i == 0)
		{// primo
		pFingerPos[0].xLeft = 0.;
		pFingerPos[0].xRight = (c_rotoloMapping[lastElem])[0].c_xPos + XCELL_PLUS;
		// -------------------------------------
		}
	else
		{
		if (i == c_rotoloMapping[lastElem].size())
			{// ultimo
			pFingerPos[i].xLeft = (c_rotoloMapping[lastElem])[i-1].c_xPos
			 						+ (c_rotoloMapping[lastElem])[i-1].c_larghezza - XCELL_PLUS;
			pFingerPos[i].xRight = SIZE_BANDE * c_difCoil.getNumSchede();	
			}
		else
			{// finger intermedi: 
			pFingerPos[i].xLeft = (c_rotoloMapping[lastElem])[i-1].c_xPos
									+ (c_rotoloMapping[lastElem])[i-1].c_larghezza - XCELL_PLUS;
			pFingerPos[i].xRight = (c_rotoloMapping[lastElem])[i].c_xPos + XCELL_PLUS;
			}
		}
	}

// OK compilato tabella finger
for (i = 0;i < c_difCoil.getNumSchede();i++)
	{// set vettore BOOL 
	fingerList.Add (0);
	for (int j=0;j<c_rotoloMapping[lastElem].size()+1;j++)
		{// ogni cella confrontata con tutti i finger
		// confronto con left va fatto con bordo dx della cella che ha dim non zero ( entra prima con bordo destro )
		// confronto con right va fatto con bordo sx della cella che ha dim non zero (esce dopo con bordo sx )
		if (((SIZE_BANDE * (i+1)) > pFingerPos[j].xLeft) && 
			((SIZE_BANDE * i) < pFingerPos[j].xRight))
			{
			// prendo in considerazione solo primo ed ultimo 
			// xChe` uso solo diaframmi
			//if ((j==0)||
			//	(j==c_rotoloMapping[lastElem].size()))
			fingerList[i] = 1;
			// ok prossima cella
			break;
			}
		}
	}
// fixBug c'era una costante 200 max num celle qs release supporta 224
int v [300];
for (int i=0;i<fingerList.GetSize();i++)
	v[i] = fingerList[i];

delete [] pFingerPos;
return TRUE;
}


// Elval specific
// maschera per identificazione bande dentro strip
BOOL	CLineDoc::fillStripList(void)
{
int lastElem;
lastElem = c_rotoloMapping.c_lastElemento;

if (lastElem < 0)
	return FALSE;

// save exception if .ini empty
int sz = c_rotoloMapping[lastElem].size();
if (sz <= 0)
	return FALSE;

for (int i=0;i<c_stripList.GetSize();i++)
	c_stripList[i].RemoveAll();

c_stripList.RemoveAll();

// per ogni strip o bobina ho memorizzato posizione x e dimensione
// 
// inizializzo c_stripList con numero bobine
c_stripList.SetSize(c_rotoloMapping[lastElem].size());

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

// compiliamo tabella stripList
for (int j=0;j<c_rotoloMapping[lastElem].size();j++)
	{
	//---------------------------------------------------------
	if (c_rightAlignStrip)	
		{
		// int jj = c_rotoloMapping[lastElem].size()-1-j;
		int jj = j;
		c_stripList[jj].SetSize(c_difCoil.getNumSchede());
		for (int i = 0;i < c_difCoil.getNumSchede();i++)
		// for (int i = (c_difCoil.getNumSchede()-1);i >= 0;i--)
			{// set vettore BOOL 
			// allargato di 9 mm per contenere celle a sx o dx
			// X---------------------X--------------------x---------
			//                   X-----X                 X-----X
			double p1=(c_rotoloMapping[lastElem])[j].c_xPos-(c_rotoloMapping[lastElem])[j].c_larghezza-9;
		
			double p2=(c_rotoloMapping[lastElem])[j].c_xPos+9;

			if (((SIZE_BANDE * (i+1)) >= p1) && 
				((SIZE_BANDE * i) <= p2))
				{
				c_stripList[jj].ElementAt(i) = 1;
				// ok prossima cella
				}
			else
				c_stripList[jj].ElementAt(i) = 0;
			}
		}
	else
		{
		// set size cell
		c_stripList[j].SetSize(c_difCoil.getNumSchede());
		for (int i = 0;i < c_difCoil.getNumSchede();i++)
			{// set vettore BOOL 
			// allargato di 9 mm per contenere celle a sx o dx
			// X---------------------X--------------------x---------
			//                   X-----X                 X-----X
			double p1=(c_rotoloMapping[lastElem])[j].c_xPos+(c_rotoloMapping[lastElem])[j].c_larghezza+9;
		
			double p2=(c_rotoloMapping[lastElem])[j].c_xPos-9;

			if (((SIZE_BANDE * (i+1)) <= p1) && 
				((SIZE_BANDE * i) >= p2))
				{
				c_stripList[j].ElementAt(i) = 1;
				// ok prossima cella
				}
			else
				c_stripList[j].ElementAt(i) = 0;
			}
		}
	}




return TRUE;

}


