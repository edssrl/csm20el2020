/*
Module : CHOOSEDIRDLG.H
Purpose: Interface for an MFC class to get a directory/folder
Created: PJN / 26-11-1997
History: None

Copyright (c) 1997 - 1999 by PJ Naughter.  
All rights reserved.

*/


////////////////////////////////// Macros ///////////////////////////

#ifndef __CHOOSEDIRDLG_H__
#define __CHOOSEDIRDLG_H__


/////////////////////////// Classes /////////////////////////////////

class CChooseDirDlg : public CObject
{
public:
//Get a directory
  BOOL GetDirectory(CString& sDir, CWnd* pWndParent, BOOL bOldStyleDialog, const CString& sTitle);

protected:
  static int CALLBACK SetSelProc(HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData);
};



#endif //_CHOOSEDIRDLG_H__