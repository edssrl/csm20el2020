// CG: This header was added by the Password Dialog Component

// pwdlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg dialog

class CPasswordDlg : public CDialog
{
// Construction
public:
	CPasswordDlg(CWnd* pParent = NULL);   // standard constructor
	static BOOL CheckPassword();
	static BOOL ClearPassword();

// Dialog Data
	//{{AFX_DATA(CPasswordDlg)
	enum { IDD = GC_IDD_PASSWORD };
	CString	m_strPassword;
	CString	m_Message;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPasswordDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void Encrypt(LPCSTR lpsz1, CString& strOut);
	void SetPrompt();

	BOOL m_bConfirmMode;
	CString m_strConfirming;

	// Generated message map functions
	//{{AFX_MSG(CPasswordDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString m_BackDoor;
};
