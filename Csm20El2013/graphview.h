// GraphView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBaseGraphView view
#pragma once

// Funzione di calcolo colori per Classi
COLORREF CSM20GetClassColor (int cl);
void CSM20ClassColorInvalidate (void);

#define DOC_CLASS CLineDoc


#include "FontInfo.h"
#include "Label.h"
#include "PrintLibSrc\CPage.h"
#include "PrintLibSrc\Dib.h"
#include "WaitDlg.h"
#include "Layout.h"
#include "PdfPrintView.h"


//Nome Server Window
#define CSM_REPORT_CLASSNAME "Csm20El2013Report"
#define CSM_CLASSNAME "Csm20El2013"


#ifndef _GRAPH_VIEW
#define _GRAPH_VIEW

struct LinePrinter
{
UINT flags;   // Flag Di stampa 
CString str;  // Valore da stampare
};

class PagePrinter
{
FontInfo fNormal;
FontInfo fCaption;
CArray <LinePrinter,LinePrinter&> page;
int nPage;
public:
PagePrinter(void){fCaption.name = "Arial";
	fCaption.size = 100;
	fNormal.name = "Arial";
	fNormal.size = 100;
	nPage = 0;
	};

BOOL add(CString &s,UINT f)
	{
	LinePrinter lp;
	lp.flags = f;
	lp.str = s;
	page.Add(lp);
	return TRUE;
	};
void clear (void) {page.RemoveAll();nPage=0;};
int print(int index,int numLine,CRect &rectB,CDC* pdc);
void setNumPage(int n){nPage = n;};
int  getNumPage(void){return(nPage);};
FontInfo *getFontNormal(void) {return(&fNormal);};
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CLineView view

// Aggiornato 27-10-97
// Aggiornato 27-10-09
struct LineInfo
{
double max;
double min;
double limit;
double *val;		// valori da disegnare
double *valBig;		// valori Big
int c_size;
int count;			// numero valori presenti vettore
int numStep;		// numero passi totali
int actual;
int stepScaler;
LineInfo (void) {stepScaler=1;c_size=0;val=NULL;valBig=NULL;min=0.;limit = 0.; max=0.;count = 0;};
~LineInfo (void) {if (val != NULL) {delete [] val; val = NULL;}
				if (valBig != NULL) {delete [] valBig; valBig = NULL;}};
// Safe allocation // fixBug falsi big in densityalarm
void newVal (int size,BOOL erase=TRUE)
{if ((val == NULL)||(valBig==NULL)||
		(size > c_size))
		{
		int allocSize = size;
		if (allocSize < 10000)
			allocSize = 10000;
		c_size = allocSize;
		if (val != NULL) {delete [] val; val = NULL;}
			val = new double [allocSize];
		if (valBig != NULL) {delete [] valBig; valBig = NULL;}
			valBig = new double [allocSize];
		}
if(erase)
{
	memset( val, 0, size*(sizeof(double)));
	memset( valBig, 0, size*(sizeof(double)));};
}
};


typedef CArray <CPoint,CPoint&> CPointArray;

struct CVertex
{
CPoint p;
double bigVertex;
};

class SubLine
{
Mode mode;
int limit;
// LPPOINT gVertex; // Lista Punti Polyline Disegno
//CPointArray gVertex;
//CArray <double,double&> gBigVertex;
// double*	gBigVertex;
CVertex*	gVertex;
int scale;	// da usare per disegnare sua carta da dimensioni video
int count;
int actual;
int numStep;
int stepScaler;
CPoint oldPoint;
COLORREF colorPen;
COLORREF colorPenLarge;
COLORREF colorPenBig;
CRect clip;
public:
SubLine(void) { stepScaler = 1;
				numStep =0;
				count = 0;
				actual = 0;
				gVertex = NULL; 
				//gVertex.SetSize(10000);
				//gBigVertex.SetSize(10000);
				//gBigVertex=NULL; 
				mode = LINEAR;
				setPen(RGB(0,0,0));setPenLarge(RGB(0,0,0));setPenBig(RGB(0,0,0));
				scale=1;};
~SubLine(void) {if (gVertex != NULL) delete [] gVertex;};
		//gVertex.RemoveAll();
		//gBigVertex.RemoveAll();};
		//if (gBigVertex != NULL) delete [] gBigVertex;};

void setPen (COLORREF  rgb)
	{colorPen = rgb;};
void setPenLarge (COLORREF  rgb)
	{colorPenLarge = rgb;};
void setPenBig (COLORREF  rgb)
	{colorPenBig = rgb;};

void setMode(Mode m){mode = m;};
// calcola dimensione rettangolo relativo a valore 
// da visualizzare,valore della scala, dimensione in della
// finestra di visualizzazione. 
void setScale(CRect &rectB,LineInfo &lInfo);
void draw(CDC* pdc,BOOL update=FALSE);
BOOL IsEmpty (void) {//return(gVertex.GetSize()==0);
				if (gVertex == NULL) return TRUE;
					return FALSE;};
int getActual(void){return(actual);};
int getCount(void){return(count);};
BOOL addNewlInfo(CRect &rectB,LineInfo &lInfo,int sizeInfo);
};




/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CDensView view


class SubRect
{
CRect bar;
LOGBRUSH lBrush;								  
LOGPEN  lPenTB;		// Pen top bottom
LOGPEN  lPenLR;		// Pen left right

char id;
int pos;

// inseriti offset, max e rect 4 speed purposes
double c_max;
CRect  c_rMax;
double c_val;
int		c_offset;
BOOL	c_modified;

public:
void setId (char c){id = c;};
void setPos (int v){pos = v;};
void setPen (COLORREF  rgb,UINT lopnWidth=0,UINT lopnStyle=PS_SOLID)
{
setPenTB (rgb,lopnWidth,lopnStyle);
setPenLR (rgb,lopnWidth,lopnStyle);
};
void setPenTB (COLORREF  rgb,UINT lopnWidth=0,UINT lopnStyle=PS_SOLID)
{
lPenTB.lopnStyle = lopnStyle;

lPenTB.lopnWidth.x = lopnWidth;
lPenTB.lopnWidth.y = 0;	// Not Used

lPenTB.lopnColor = rgb;
};
void setPenLR (COLORREF  rgb,UINT lopnWidth=0,UINT lopnStyle=PS_SOLID)
{
lPenLR.lopnStyle = lopnStyle;

lPenLR.lopnWidth.x = lopnWidth;
lPenLR.lopnWidth.y = 0;	// Not Used

lPenLR.lopnColor = rgb;
};

void setBrush (COLORREF lbColor,UINT lbStyle = BS_SOLID ,LONG lbHatch = HS_BDIAGONAL) 
{
lBrush.lbStyle = lbStyle;
lBrush.lbColor = lbColor;
lBrush.lbHatch = lbHatch;
};

// calcola dimensione rettangolo relativo a valore 
// da visualizzare, valore della scala, dimensione in della
// finestra di visualizzazione.
// torna top  
int setScale(CRect &r,int offset,double max,double val);
int setVal(int offset,double val);
void draw(CDC* pDC);
};


enum ViewDensMode {FLAT,TOWER};

class ViewDensUnit
{

ViewDensMode vMode;
CArray <SubRect,SubRect &> subR;


// SubRect subA;
// SubRect subB;
// SubRect subC;
// SubRect subD;
public:
ViewDensUnit(int n){init(n);};
ViewDensUnit(void){init(2);};
void init(int n){subR.SetSize(n);vMode = FLAT;
	for (int i=0;i<subR.GetSize();i++)
		subR[i].setId(('A'+i));};
void setMode (ViewDensMode m){vMode = m;};

void setPos(int v){for(int i=0;i<subR.GetSize();i++) subR[i].setPos(v);};
void setScale(CRect &r,double max,double val[]); 
void setVal(double val[]);


//	double valB,double valC,double valD);
void draw(CDC* pDC)
	{for(int i=0;i<subR.GetSize();i++)subR[i].draw(pDC);};
void setPen (int id,COLORREF  rgb){ASSERT(id < subR.GetSize());subR[id].setPen(rgb);};
void setPenTB (int id,COLORREF  rgb){ASSERT(id < subR.GetSize());subR[id].setPenTB(rgb);};
void setPenLR (int id,COLORREF  rgb){ASSERT(id < subR.GetSize());subR[id].setPenLR(rgb);};

void setBrush (int id,COLORREF lbColor,UINT lbStyle = BS_SOLID ,LONG lbHatch = 0)
	{ASSERT(id<subR.GetSize());subR[id].setBrush(lbColor,lbStyle,lbHatch);}; 
};



/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CInitView view
struct PageAlzStruct
{// struttura per ogni tabella di alzatadettagli 
int c_elem;
int c_alzIndex;
int c_colIndex;
int c_numCol;
int c_numRow;
int c_isLastBlock;
PageAlzStruct(void){clear();};
void clear(void)
{c_elem=c_alzIndex=c_colIndex=0;c_numCol=0;c_numRow=0;c_isLastBlock=0;};
int getNumLine(void)
{return c_numRow;};
};

struct TReportPage
{
TReportPage(){c_index=0;c_lineXPage=0;reset();};
void reset(void)
{c_numLineDif=c_indexLineDif=c_numLineHeader=c_numAlzStruct=0;
c_numLineAl1=c_numLineAl2=c_indexLineAl2=0;
for (int i=0;i<10;i++)c_alzPage[i].clear();
};
//BYFC 
int getNumLine2(void){return(c_numLineHeader+c_numLineDif);};
int getNumLine3(void){int nr = c_numLineHeader;
			nr = 0;
			for(int i=0;i<c_numAlzStruct;i++)
				nr += c_alzPage[i].getNumLine();
			return nr;};
int getNumLine4(void){return(c_numLineHeader+c_numLineAl1+c_numLineAl2);};
int getFreeLine2(void){return(c_lineXPage-getNumLine2());};
int getFreeLine3(void){return(c_lineXPage-getNumLine3());};
int getFreeLine4(void){return(c_lineXPage-getNumLine4());};
int c_index;			// numero di pagina
int c_numLineHeader;	// numero righe per i difetti random
int c_numLineDif;		// numero righe per i difetti random
int c_indexLineDif;		// indice prima riga difetti random
PageAlzStruct	c_alzPage[10];	// descrittore alzate
int	c_numAlzStruct;		//
int c_numLineAl1;		//
int	c_indexLineAl1;		//
int c_numLineAl2;		//
int	c_indexLineAl2;		//
int c_lineXPage;
};


#define REPORTSAVE_TIMER_ID		118

#ifdef MAIN_CSM20
CSemaphore	c_reportSaveSem;	
#else

extern CSemaphore	c_reportSaveSem;	
#endif



class CInit
{
public:

	CInit(); 
	

protected:
	// protected constructor used by dynamic creation

	ViewType vType;

CArray <TReportPage, TReportPage &>c_reportVector2;
CArray <TReportPage, TReportPage &>c_reportVector3;
CArray <TReportPage, TReportPage &>c_reportVector4;

// stringa da profile per logo (LOGO_ELVAL, LOGO_EMPTY ) per ora
CString c_strLogoId;

CDocument*	m_pLocalDocument;

// Attributes
public:

	void setLocalPDoc(CDocument* pdoc)
	{m_pLocalDocument=pdoc;}; 

	CDocument*	GetDoc(){return m_pLocalDocument;};

CSize c_printSize;
CRect c_printRect;

int c_leftMarg;		// margine sinistro report in mm
int c_rightMarg;	// margine destro report in mm
int c_topMarg;		// margine alto report in mm
int c_bottomMarg;	// margine basso report in mm

// Tipi di grafici in stampa
// Densita` trasversale
CArray <ViewDensUnit, ViewDensUnit &>densView;

//
Layout  layoutD;
// Compressioni d'asse
Layout c_layoutCompressABBig ;

CTargetBoard	c_targetBoardABBig;


// Trend longitudinale
LineInfo lineInfo [4];
SubLine trendLine [4];
Layout  layoutL ;

// Allarme Densita`
LineInfo lineInfoAD [4];
SubLine trendAD [4];
Layout  layoutAD;

// Indice Qualita`
LineInfo lineInfoQ [4];
SubLine trendQ [4];
Layout  layoutQ ;

// Rect Print
CRect prLimit;

// Limiti di asse da comprendere nel report
CRect c_subNastro;

// CDAttesaStampa waitPrint;

// Stampa formato testo
PagePrinter pagePr;
PagePrinter pagePrEnd;	 // Testo fine Report
//BOOL onlyTextReport;	 // Stampa report solo testo
BOOL directPrint;		// Stampa diretta senza dialog 

// stampa report di strip
BOOL	c_doPrintStrip;
// identificazione strip
long	c_prStripNumber;
long	c_prElementNumber;
long	c_prAlzataNumber;
//-------------------------
// righe per pagina dei report in formato tabellare
CArray <int,int &> c_rowPerPage;
CArray <int,int &> c_extractDefPerPage;

// indice difetto attualmente in estrazione
// gli strip vanno solo in automatico allora non si deve gestire avanti
// ed indietro di preview -> + facile
long c_curExtractingDefect;
// serve per non stampare parte di tabella vuota
long c_printedRow;
// flag per sapere se siamo in ciclo di stampa o preview
BOOL c_isPrinting;
// flag per sapere se siamo in ciclo di preview
BOOL c_isPreview;
// flag per sapere se continuare a stampare
BOOL c_continuePrinting;
// pointer CWaitDialog per CWaitDialog::pump
CWaitDialog* c_pWaitDialog;

// boolean difetti di laminazione
BOOL c_existLaminDef;

int c_numTextPage2;		// numero di pagine testo 
int c_numTextPage3;		// numero di pagine testo 
int c_numTextPage4;		// numero di pagine testo 

//-------------------
// configuratore report
BOOL	c_useManualReportConfig;
BOOL	c_reportGraph[4];
BOOL	c_reportClass[5];
//--------------------------
int LinePerPage;
int ColumnPerPage;		 // Dettaglio difetti numero di colonne 1-4

int c_numPageRepBobine;
// Report option
BOOL	m_DATI_CLIENTE;
BOOL	m_DATA_LAVORAZIONE;
BOOL	m_LUNGHEZZA_BOBINA;
BOOL	m_DESCR_PERIODICI;
BOOL	m_DESCR_RANDOM;
BOOL	m_DESCR_ALZATE;
BOOL	m_SOGLIE_ALLARME;
BOOL	m_ALLARMISISTEMA;
double	m_DALMETRO;
double	m_ALMETRO;
double	m_INTERVALLO[20];

//---------------------------
// duplicazione m_DALMETRO
// duplicazione m_ALMETRO
// fatta per aggiungere impostazione da dialog
// selReportType e non modificare troppo codice esistente
double c_dalMetro;
double c_alMetro;

// Operations
public:
	//void draw(void){CClientDC dc(this);OnDraw(&dc);};
	int onDraw(CDC* pDC,BOOL doPrint);

	ViewType getViewType(void) {return(vType);};
	CRect getDrawRect(void);
	CRect getMeterRect(void); //empty rect
	void reportRotolo2(CDC* pDc,CRect rcBounds);
	long reportStrip(CDC* pDC,int page,CRect r,BOOL doPrint);
	int calcStripRepPage(CDC *pDc,CRect r);

	CRect getRectStrip(int elemento,int alzata,int strip);
	CRect getRectCoil(int elemento);

	virtual BOOL DoPreparePrinting (CPrintInfo* pInfo)=0;
	virtual void GetClientRect(LPRECT rcBounds)=0;
	virtual void OnFilePrPview(void)=0;

//---------------------
int  prepareTextPageEx2(CDC* pdc,CRect r); 
int  prepareTextPageEx3(CDC* pdc,CRect r); 
int  prepareTextPageEx4(CDC* pdc,CRect r); 

double printTextPageEx2(CPage* ps,CRect rcBounds,int pageIndex); 
double printTextPageEx3(CPage* ps,CRect rcBounds,int pageIndex); 
double printTextPageEx4(CPage* ps,CRect rcBounds,int pageIndex); 
void pageGraphicPrint(CDC* pdc,int curPage,CRect &rcBounds);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInitView)
	public:
	// virtual void onPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void onInitialUpdate();
//	protected:
	virtual void onDraw(CDC* pDC);      // overridden to draw this view
	virtual void onPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void onEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void onBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL onPreparePrinting(CPrintInfo* pInfo);
	virtual void onFilePrintPreview();
	//virtual void onEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	//}}AFX_VIRTUAL

// Implementation

	virtual ~CInit();
protected:

	// Generated message map functions
public:
	CRect calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border=NULL);
	CPoint c_lastPosReport;
	
};

class CInitView : public CPdfPrintView, public CInit
{
public:

	CInitView(); 


	void draw(void){CClientDC dc(this);OnDraw(&dc);};

protected:
	          // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CInitView)

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInitView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	virtual void OnInitialUpdate();
//	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	//}}AFX_VIRTUAL

// Implementation
public:
	void launchReportSave(int deltaT=-1)
	{if(deltaT <= 0)
		SetTimer(REPORTSAVE_TIMER_ID,50+rand()%50,NULL);
	 else
		SetTimer(REPORTSAVE_TIMER_ID,deltaT,NULL);

	};		

	CString saveSectionTxtReport(void);
	CString saveDefectTxtReport(void);
	CString saveDefectTxtStripReport(int stripNumber,int alzata);
	CString saveSectionXlsReport(void);
	CString saveDefectXlsStripReport(int strip,int alzata);
	// spostato qui report save dopo F2 e fileSave, 
	// attivato da timer
	void reportSave(); 



	virtual ~CInitView();
protected:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
public:
	CRect calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border=NULL);
	CPoint c_lastPosReport;
	//{{AFX_MSG(CInitView)
	afx_msg void OnFilePrint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnFilePrintPreview();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	virtual BOOL DoPreparePrinting (CPrintInfo* pInfo)
		{return CPdfPrintView::DoPreparePrinting(pInfo);};
	virtual void GetClientRect(LPRECT rcBounds)
		{return CPdfPrintView::GetClientRect(rcBounds);};
	virtual void OnFilePrPview(void)
		{return CPdfPrintView::OnFilePrintPreview();};
};

#endif
/////////////////////////////////////////////////////////////////////////////
