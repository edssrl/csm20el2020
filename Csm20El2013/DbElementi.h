#if !defined(AFX_DBELEMENTI_H__C05C92A2_3045_11D6_AB54_00C026A019B7__INCLUDED_)
#define AFX_DBELEMENTI_H__C05C92A2_3045_11D6_AB54_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DbElementi.h : header file
//

#include "Elemento.h"

/////////////////////////////////////////////////////////////////////////////
// CDbElementi DAO recordset

class CDbElementi : public CDaoRecordset
{
public:
	CDbElementi(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CDbElementi)


//-----------------------
	BOOL openSelectNome (LPCSTR nome,int elemento,BOOL closeNotFound);
	BOOL appendElemento (LPCSTR rotolo,int elemento,CElemento *pElemento);


// Field/Param Data
	//{{AFX_FIELD(CDbElementi, CDaoRecordset)
	CString	m_ROTOLO;
	long	m_ELEMENTO;
	long	m_POSIZIONE;
	double	m_LARGHEZZA;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDbElementi)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBELEMENTI_H__C05C92A2_3045_11D6_AB54_00C026A019B7__INCLUDED_)
