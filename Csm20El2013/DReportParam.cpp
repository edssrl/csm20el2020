// ReportParam.cpp : implementation file
//

#include "stdafx.h"
// #include "Csm20El2013.h"

#include "DReportParam.h"
#include "afxdialogex.h"


// CReportParam dialog

IMPLEMENT_DYNAMIC(CDReportParam, CPropertyPage)

CDReportParam::CDReportParam()
	: CPropertyPage(CDReportParam::IDD)
{
	m_Allarmi = FALSE;
	m_difettiPer = FALSE;
	m_difettiRand = FALSE;
	m_mappa = FALSE;
	m_alzate = FALSE;

}

CDReportParam::~CDReportParam()
{
}

void CDReportParam::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_ALLARMI1, m_Allarmi);
	DDX_Check(pDX, IDC_CHECK_DIFETTI_PER1, m_difettiPer);
	DDX_Check(pDX, IDC_CHECK_DIFETTI_RAND, m_difettiRand);
	DDX_Check(pDX, IDC_CHECK_MAPPA1, m_mappa);
	DDX_Check(pDX, IDC_CHECK_ALZATE, m_alzate);

	//  DDX_Control(pDX, IDC_CHECK_CLASSA, m_classA);
	DDX_Check(pDX, IDC_CHECK_CLASSA, m_classA);
	DDX_Check(pDX, IDC_CHECK_CLASSB, m_classB);
	DDX_Check(pDX, IDC_CHECK_CLASSBIG, m_classBig);
	DDX_Check(pDX, IDC_CHECK_CLASSC, m_classC);
	DDX_Check(pDX, IDC_CHECK_CLASSD, m_classD);
	DDX_Check(pDX, IDC_CHECK_GRAPH1, m_graph1);
	DDX_Check(pDX, IDC_CHECK_GRAPH2, m_graph2);
	DDX_Check(pDX, IDC_CHECK_GRAPH3, m_graph3);
	DDX_Check(pDX, IDC_CHECK_GRAPH4, m_graph4);
}


BEGIN_MESSAGE_MAP(CDReportParam, CPropertyPage)
END_MESSAGE_MAP()


BOOL CDReportParam::OnInitDialog() 
{
CDialog::OnInitDialog();
// TODO: Add extra initialization here

return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// CDReportParam message handlers
void CDReportParam::OnOK() 
{
// TODO: Add extra validation here
UpdateData(TRUE);
CDialog::OnOK();
}
