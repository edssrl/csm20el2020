// FInputOutput.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "GraphDoc.h"

#include "DAlzate.h"
#include "DAlzataDettagli.h"

#include <AfxTempl.h>


#include "GraphView.h"
#include "dyntempl.h"

#include "Csm20El2013.h"
#include "FInputOutput.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFInputOutput

IMPLEMENT_DYNCREATE(CFInputOutput, CFormView)

CFInputOutput::CFInputOutput()
	: CFormView(CFInputOutput::IDD)
{
	//{{AFX_DATA_INIT(CFInputOutput)
	m_out0 = FALSE;
	m_out1 = FALSE;
	m_out2 = FALSE;
	m_out3 = FALSE;
	m_out7 = FALSE;
	m_in0 = FALSE;
	m_in2 = FALSE;
	m_in3 = FALSE;
	m_posDiafDx = 0;
	m_posDiafSx = 0;
	m_speedEncoder = 0;
	m_in1 = FALSE;
	m_in4 = FALSE;
	m_in5 = FALSE;
	m_in6 = FALSE;
	m_in7 = FALSE;
	m_out4 = FALSE;
	m_out5 = FALSE;
	m_out6 = FALSE;
	//}}AFX_DATA_INIT



}

CFInputOutput::~CFInputOutput()
{
}

void CFInputOutput::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFInputOutput)
	DDX_Control(pDX, IDC_OUT6, m_ctrlOut6);
	DDX_Control(pDX, IDC_OUT5, m_ctrlOut5);
	DDX_Control(pDX, IDC_OUT4, m_ctrlOut4);
	DDX_Control(pDX, IDC_IN7, m_ctrlIn7);
	DDX_Control(pDX, IDC_IN6, m_ctrlIn6);
	DDX_Control(pDX, IDC_IN5, m_ctrlIn5);
	DDX_Control(pDX, IDC_IN4, m_ctrlIn4);
	DDX_Control(pDX, IDC_IN1, m_ctrlIn1);
	DDX_Control(pDX, IDC_START_RIGHT_DIAPH, m_startRightButton);
	DDX_Control(pDX, IDC_START_LEFT_DIAPH, m_startLeftButton);
	DDX_Control(pDX, IDC_LABEL_LR_TEST, m_ctrlLabelLRTest);
	DDX_Control(pDX, IDC_POSIZ_DIAPH_SX, m_ctrlPosDiaphSx);
	DDX_Control(pDX, IDC_POSIZ_DIAPH_DX, m_ctrlPosDiaphDx);
	DDX_Control(pDX, IDC_SPEED_ENCODER, m_ctrlSpeedEncoder);
	DDX_Control(pDX, IDC_LABEL_SPEED_ENCODER, m_ctrlLabelSpeedEncoder);
	DDX_Control(pDX, IDC_LABEL_POSIZ_DIAPH_DX, m_ctrlLabelPosDiaphDx);
	DDX_Control(pDX, IDC_LABEL_POSIZ_DIAPH_SX, m_ctrlLabelPosDiaphSx);
	DDX_Control(pDX, IDC_OUT7, m_ctrlOut7);
	DDX_Control(pDX, IDC_OUT3, m_ctrlOut3);
	DDX_Control(pDX, IDC_OUT2, m_ctrlOut2);
	DDX_Control(pDX, IDC_OUT1, m_ctrlOut1);
	DDX_Control(pDX, IDC_OUT0, m_ctrlOut0);
	DDX_Control(pDX, IDC_IN3, m_ctrlIn3);
	DDX_Control(pDX, IDC_IN2, m_ctrlIn2);
	DDX_Control(pDX, IDC_IN0, m_ctrlIn0);
	DDX_Check(pDX, IDC_OUT0, m_out0);
	DDX_Check(pDX, IDC_OUT1, m_out1);
	DDX_Check(pDX, IDC_OUT2, m_out2);
	DDX_Check(pDX, IDC_OUT3, m_out3);
	DDX_Check(pDX, IDC_OUT7, m_out7);
	DDX_Check(pDX, IDC_IN0, m_in0);
	DDX_Check(pDX, IDC_IN2, m_in2);
	DDX_Check(pDX, IDC_IN3, m_in3);
	DDX_Text(pDX, IDC_POSIZ_DIAPH_DX, m_posDiafDx);
	DDX_Text(pDX, IDC_POSIZ_DIAPH_SX, m_posDiafSx);
	DDX_Text(pDX, IDC_SPEED_ENCODER, m_speedEncoder);
	DDX_Check(pDX, IDC_IN1, m_in1);
	DDX_Check(pDX, IDC_IN4, m_in4);
	DDX_Check(pDX, IDC_IN5, m_in5);
	DDX_Check(pDX, IDC_IN6, m_in6);
	DDX_Check(pDX, IDC_IN7, m_in7);
	DDX_Check(pDX, IDC_OUT4, m_out4);
	DDX_Check(pDX, IDC_OUT5, m_out5);
	DDX_Check(pDX, IDC_OUT6, m_out6);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFInputOutput, CFormView)
	//{{AFX_MSG_MAP(CFInputOutput)
	ON_BN_CLICKED(IDC_UPDATE, OnUpdate)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_START_LEFT_DIAPH, OnStartLeftDiaph)
	ON_BN_CLICKED(IDC_START_RIGHT_DIAPH, OnStartRightDiaph)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFInputOutput diagnostics

#ifdef _DEBUG
void CFInputOutput::AssertValid() const
{
	CFormView::AssertValid();
}

void CFInputOutput::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG




CLineDoc* CFInputOutput::GetDocument(void)
{
CCSMApp* pApp = (CCSMApp*) AfxGetApp();

return pApp->GetDocumentTop();
}


// Attenzione usare solo in onInitialUpdate
void CFInputOutput::labelFromProfile(void)
{
CString s;
CProfile profile;

//  Ingressi
s = profile.getProfileString("FormIO","In0","Abilitazione");
if (s == "")
	{
	m_ctrlIn0.ShowWindow(SW_HIDE);
	}
m_ctrlIn0.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","In1","Cambio");
if (s == "")
	{
	m_ctrlIn1.ShowWindow(SW_HIDE);
	}
m_ctrlIn1.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","In2","Cambio");
if (s == "")
	{
	m_ctrlIn2.ShowWindow(SW_HIDE);
	}
m_ctrlIn2.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","In3","Pausa");
if (s == "")
	{
	m_ctrlIn3.ShowWindow(SW_HIDE);
	}
m_ctrlIn3.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","In4","Pausa");
if (s == "")
	{
	m_ctrlIn4.ShowWindow(SW_HIDE);
	}
m_ctrlIn4.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","In5","Pausa");
if (s == "")
	{
	m_ctrlIn5.ShowWindow(SW_HIDE);
	}
m_ctrlIn5.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","In6","Pausa");
if (s == "")
	{
	m_ctrlIn6.ShowWindow(SW_HIDE);
	}
m_ctrlIn6.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","In7","Pausa");
if (s == "")
	{
	m_ctrlIn7.ShowWindow(SW_HIDE);
	}
m_ctrlIn7.SetWindowText((LPCSTR)s);

//  Uscite
s = profile.getProfileString("FormIO","Out0","Allarme foro");
if (s == "")
	{
	m_ctrlOut0.ShowWindow(SW_HIDE);
	}
m_ctrlOut0.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","Out1","Allarme sistema");
if (s == "")
	{
	m_ctrlOut1.ShowWindow(SW_HIDE);
	}
m_ctrlOut1.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","Out2","Laser");
if (s == "")
	{
	m_ctrlOut2.ShowWindow(SW_HIDE);
	}
m_ctrlOut2.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","Out3","Led Ispezione");
if (s == "")
	{
	m_ctrlOut3.ShowWindow(SW_HIDE);
	}
m_ctrlOut3.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","Out4","Led Ispezione");
if (s == "")
	{
	m_ctrlOut4.ShowWindow(SW_HIDE);
	}
m_ctrlOut4.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","Out5","Led Ispezione");
if (s == "")
	{
	m_ctrlOut5.ShowWindow(SW_HIDE);
	}
m_ctrlOut5.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","Out6","Led Ispezione");
if (s == "")
	{
	m_ctrlOut6.ShowWindow(SW_HIDE);
	}
m_ctrlOut6.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","Out7","Soffio Aria");
if (s == "")
	{
	m_ctrlOut7.ShowWindow(SW_HIDE);
	}
m_ctrlOut7.SetWindowText((LPCSTR)s);
// 

// Diaframmi
s = profile.getProfileString("FormIO","PosDiafSx","Posizione Diaframma Sinistro");
if (s == "")
	{
	m_ctrlLabelPosDiaphSx.ShowWindow(SW_HIDE);
	m_ctrlPosDiaphSx.ShowWindow(SW_HIDE);
	}
m_ctrlLabelPosDiaphSx.SetWindowText((LPCSTR)s);
// 
s = profile.getProfileString("FormIO","PosDiafDx","Posizione Diaframma Destro");
if (s == "")
	{
	m_ctrlLabelPosDiaphDx.ShowWindow(SW_HIDE);
	m_ctrlPosDiaphDx.ShowWindow(SW_HIDE);
	}
m_ctrlLabelPosDiaphDx.SetWindowText((LPCSTR)s);
 
// SpeedEncoder 
s = profile.getProfileString("FormIO","SpeedEncoder","Velocita` Encoder");
if (s == "")
	{
	m_ctrlLabelSpeedEncoder.ShowWindow(SW_HIDE);
	m_ctrlSpeedEncoder.ShowWindow(SW_HIDE);
	}
m_ctrlLabelSpeedEncoder.SetWindowText((LPCSTR)s);
// 
// SpeedEncoder 
s = profile.getProfileString("FormIO","LeftRightTest","Test Diaframma");
if (s == "")
	{
	m_ctrlLabelLRTest.ShowWindow(SW_HIDE);
	m_startLeftButton.ShowWindow(SW_HIDE);
	m_startRightButton.ShowWindow(SW_HIDE);
	}
m_ctrlLabelLRTest.SetWindowText((LPCSTR)s);
// 

}



/////////////////////////////////////////////////////////////////////////////
// CFInputOutput message handlers

void CFInputOutput::OnUpdate() 
{
UpdateData(TRUE);
	
// TODO: Add your control notification handler code here
CLineDoc* pDoc = (CLineDoc*)GetDocument();

unsigned short v;

v = ((m_out7 & 0x01)<<7) | ((m_out6 & 0x01)<<6) | ((m_out5 & 0x01)<<5) | ((m_out4 & 0x01)<<4) | 
		((m_out3 & 0x01)<<3) | ((m_out2 & 0x01)<<2) |	((m_out1 & 0x01)<<1) | (m_out0 & 0x01);  

pDoc->sendValOutput(v); 

}

void CFInputOutput::OnInitialUpdate() 
{
CFormView::OnInitialUpdate();
	
CProfile profile;
BOOL useProfileLabel = profile.getProfileBool("FormIO","UseProfileLabel",FALSE);

if (useProfileLabel)
	{
	labelFromProfile();
	}

// TODO: Add your specialized code here and/or call the base class
// send switch mode to hde

CLineDoc* pDoc = (CLineDoc*)GetDocument();

if (!pDoc->isTopSide())
	{
	CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
	frame->MDINext();
	frame->Pump();
	CDocTemplate* pDocTemplate = pDoc->GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	pDoc = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	}

if (!pDoc->isTopSide())
	AfxMessageBox ("Warning wrong side!!!");

if (pDoc->getStatoHde(true) != HDE_WAITSTART)
	{
	CString str;
	str.LoadString(CSM_FIO_NO_WAIT_START);
	// AfxMessageBox (" Stato sistema HDE diverso da WAIT_START");
	AfxMessageBox (str);
	}
else
	{
	// send start IO mode cmd
	pDoc->sendIOStartStop (TRUE);
	}

// timer 1 sec di aggiornamento stato ingressi
if ((c_timerId=SetTimer(rand(),1000,NULL))== 0)
	{
	AfxGetMainWnd()->MessageBox ("Cannot Create AlarmTimer","SetTimer");
	}


}

void CFInputOutput::OnDestroy() 
{
CLineDoc* pDoc = (CLineDoc*)GetDocument();

if (pDoc != NULL)
	// send start IO mode cmd
	pDoc->sendIOStartStop (FALSE);

// destroy timer	
KillTimer(c_timerId);

CFormView::OnDestroy();
// TODO: Add your message handler code here	
}

void CFInputOutput::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default
CFormView::OnTimer(nIDEvent);
CLineDoc* pDoc = (CLineDoc*)GetDocument();


UpdateData(TRUE);

m_in0 =  pDoc->c_valInput & 0x01;
m_in1 = (pDoc->c_valInput & 0x02)>>1;
m_in2 = (pDoc->c_valInput & 0x04)>>2;
m_in3 = (pDoc->c_valInput & 0x08)>>3;
m_in4 = (pDoc->c_valInput & 0x10)>>4;
m_in5 = (pDoc->c_valInput & 0x20)>>5;
m_in6 = (pDoc->c_valInput & 0x40)>>6;
m_in7 = (pDoc->c_valInput & 0x40)>>7;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
m_speedEncoder = frame->c_valEncoder;

m_posDiafDx = pDoc->c_posDiaframmaDx;
m_posDiafSx = pDoc->c_posDiaframmaSx;

UpdateData(FALSE);
}

void CFInputOutput::OnStartLeftDiaph() 
{
// TODO: Add your control notification handler code here

CLineDoc* pDoc = (CLineDoc*)GetDocument();


// true = left
pDoc->sendStartTestDiaf(TRUE);	

}

void CFInputOutput::OnStartRightDiaph() 
{
// TODO: Add your control notification handler code here

CLineDoc* pDoc = (CLineDoc*)GetDocument();


// true = left
pDoc->sendStartTestDiaf(FALSE);	

}
