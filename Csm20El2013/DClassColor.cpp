// DClassColor.cpp : implementation file
//

#include "stdafx.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "Csm20El2013.h"
#include "DClassColor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDClassColor dialog


CDClassColor::CDClassColor(CWnd* pParent /*=NULL*/)
	: CDialog(CDClassColor::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDClassColor)
	m_colorA = _T("");
	m_colorB = _T("");
	m_colorBig = _T("");
	m_colorC = _T("");
	m_colorD = _T("");
	//}}AFX_DATA_INIT
}


void CDClassColor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDClassColor)
	DDX_Text(pDX, IDC_COLOR_A, m_colorA);
	DDX_Text(pDX, IDC_COLOR_B, m_colorB);
	DDX_Text(pDX, IDC_COLOR_BIG, m_colorBig);
	DDX_Text(pDX, IDC_COLOR_C, m_colorC);
	DDX_Text(pDX, IDC_COLOR_D, m_colorD);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDClassColor, CDialog)
	//{{AFX_MSG_MAP(CDClassColor)
	ON_BN_CLICKED(IDC_BUTTON_COLORA, OnButtonColora)
	ON_BN_CLICKED(IDC_BUTTON_COLORB, OnButtonColorb)
	ON_BN_CLICKED(IDC_BUTTON_COLORBIG, OnButtonColorbig)
	ON_BN_CLICKED(IDC_BUTTON_COLORC, OnButtonColorc)
	ON_BN_CLICKED(IDC_BUTTON_COLORD, OnButtonColord)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDClassColor message handlers

BOOL	CDClassColor::loadFromProfile(void)
{
CProfile profile;

m_colorA = profile.getProfileString ("Color","ClassColorA","0x00ff00");
m_colorB = profile.getProfileString ("Color","ClassColorB","0x0000ff");
m_colorC = profile.getProfileString ("Color","ClassColorC","0xff0000");
m_colorD = profile.getProfileString ("Color","ClassColorD","0x800000");
m_colorBig = profile.getProfileString ("Color","ClassColorBig","0x000080");

return TRUE;
}


BOOL	CDClassColor::saveToProfile(void)
{
CProfile profile;

profile.writeProfileString ("Color","ClassColorA",m_colorA);
profile.writeProfileString ("Color","ClassColorB",m_colorB);
profile.writeProfileString ("Color","ClassColorC",m_colorC);
profile.writeProfileString ("Color","ClassColorD",m_colorD);
profile.writeProfileString ("Color","ClassColorBig",m_colorBig);

return TRUE;
}


void CDClassColor::OnButtonColora() 
{
// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorA);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorA = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);

}

void CDClassColor::OnButtonColorb() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorB);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorB = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

void CDClassColor::OnButtonColorbig() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorBig);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorBig = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

void CDClassColor::OnButtonColorc() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorC);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorC = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

void CDClassColor::OnButtonColord() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

CColorDialog dlg;
dlg.m_cc.Flags |= CC_FULLOPEN;
dlg.m_cc.Flags |= CC_RGBINIT;
dlg.m_cc.rgbResult = Hex2Rgb(m_colorD);
//dlg.m_cc.rgbResult = RGB(0,155,0);
if (dlg.DoModal()==IDOK)
	m_colorD = Rgb2Hex(dlg.m_cc.rgbResult); 

UpdateData(FALSE);
	
}

CString CDClassColor::Rgb2Hex(COLORREF color)
{
CString rgbS;
rgbS.Format("0x%02x%02x%02x",
			GetRValue(color),GetGValue(color),GetBValue(color));
return rgbS;
}

COLORREF CDClassColor::Hex2Rgb(CString color)
{
int r,g,b;
// skip 0x
color = color.Right(color.GetLength()-2);

// get red
r = Hex2Int(color.Left(2));

// get green
color = color.Right(color.GetLength()-2);
g = Hex2Int(color.Left(2));
	
// get blue
color = color.Right(color.GetLength()-2);
b = Hex2Int(color.Left(2));

return (RGB(r,g,b));
}

int CDClassColor::Hex2Int(CString strHex)
{
int v=0;
int expo = 1;
while(strHex.GetLength() > 0)
	{
	char ch;
	ch = strHex[strHex.GetLength()-1];
	ch = toupper(ch);
	// check if letter
	if (ch >= 'A')
		{
		ch -= 'A';
		ch += 10;
		}
	else
		ch -= '0';
	if ((ch < 0)||(ch > 15))
		return -1;
	v += (ch * expo);
	expo *= 16;
	strHex = strHex.Left(strHex.GetLength()-1);
	}
return v;
}
