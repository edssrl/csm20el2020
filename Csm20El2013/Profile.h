//-----------------------------------------------------------------
//
//				 Profile.h
//
//	  Gestione profileString ed int
//-----------------------------------------------------------------

#ifndef _CPROFILE_H_
#define _CPROFILE_H_


class CProfile
{
CString strValue;
CWinApp* pApp;
public:
CProfile (CWinApp* pA) {pApp = pA;};
CProfile (void ) {pApp = AfxGetApp();};

BOOL getProfileBool (LPCSTR s1, LPCSTR s2, BOOL def);
CString &getProfileString (LPCSTR s1, LPCSTR s2, LPCSTR def);
int getProfileInt (LPCSTR s1, LPCSTR s2, int def);
BOOL deleteProfile (LPCSTR s1, LPCSTR s2);
BOOL writeProfileString (LPCSTR s1, LPCSTR s2, LPCSTR s3);

};

#endif
