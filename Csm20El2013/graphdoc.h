// GraphDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBaseDoc document

#include "FontInfo.h"
#include "Dialog.h"
#include "Target.h"
#include "Elemento.h"	// Added by ClassView
#include "DStripPosition.h"


#include "float.h"		// 

#ifndef  _GRAPH_DOC
#define  _GRAPH_DOC

#define CSM_SINGLE_SIDE	"0"		// non era 
#define CSM_TOP_SIDE	"1"		// era "Z"
#define CSM_BOTTOM_SIDE	"2"		// era "S"

// #define FILEDOCTYPE	"Csm20El2020"	
// #define FILEDOCVER	1

#define FILEDOCTYPE	"Csm20El2013"	
#define FILEDOCVER	9

// dimensione dei ricevitori CSM20D era 10
//#define SIZE_BANDE	11.
#ifdef MAIN_DOC
double SIZE_BANDE = 10.;
#else
extern double SIZE_BANDE;
#endif

#define  THISAPP CCSMApp
enum ViewType {UNKVIEW,INITVIEW,LINEVIEW,DENSVIEW,TOWERVIEW,ROLLVIEW,MULTILINEVIEW};

#define NMAXCLASSI 4

#define LEFTCUTTER_REFERENCE 0.

#define COILTYPE DifCoil

// bandina di sicurezza 
// #define XCELL_PLUS	6.
// ora vogliamo fori sul bordo siano in due strip
#define XCELL_PLUS	0

// in realta` non sono fingerPos ma 
// StripPos
struct FingerPos
{
double  xLeft;
double	xRight;
};

struct FilterParam
	{
	int		c_filterTimes;		// contatore filtraggi ripetuti
	int		c_filterType;		// tipo ultimo filtraggio
	BOOL	c_clearOutside;		// zone esterne azzerate
	double	c_limitA;			// limite Filtro A
	double	c_limitB;			// limite Filtro B
	double	c_limitC;			// limite Filtro C
	double	c_limitD;			// limite Filtro D
	double	c_limitBig;			// limite Filtro Big
	FilterParam(void){clear();};
	void clear(void){c_filterTimes=c_filterType=c_clearOutside=0;
	c_limitA=c_limitB=c_limitC=c_limitD=c_limitBig=0;};
	BOOL save(CFileBpe *cf)
		{cf->Write((void *)&c_filterTimes,sizeof(int));
		cf->Write((void *)&c_filterType,sizeof(int));
		cf->Write((void *)&c_clearOutside,sizeof(int));
		cf->Write((void *)&c_limitA,sizeof(double));
		cf->Write((void *)&c_limitB,sizeof(double));
		cf->Write((void *)&c_limitC,sizeof(double));
		cf->Write((void *)&c_limitD,sizeof(double));
		cf->Write((void *)&c_limitBig,sizeof(double));return(TRUE);};
	BOOL load(CFileBpe *cf,int ver)
		{cf->Read((void *)&c_filterTimes,sizeof(int));
		cf->Read((void *)&c_filterType,sizeof(int));
		cf->Read((void *)&c_clearOutside,sizeof(int));
		cf->Read((void *)&c_limitA,sizeof(double));
		cf->Read((void *)&c_limitB,sizeof(double));
		cf->Read((void *)&c_limitC,sizeof(double));
		cf->Read((void *)&c_limitD,sizeof(double));
		cf->Read((void *)&c_limitBig,sizeof(double));return(TRUE);};


	};

// spostato codice in difcoil.h
#include "DifCoil.h"



/////////////////////////////////////////////////////////////////////////////
// CLineDoc document
struct RandDif 
	{
	int pos;
	int scheda;
	int classe;
	int big;
	RandDif (void){big = 0;pos = -1;scheda = -1;classe=-1;};
	};


enum  StartMode {COLD_START=0,WARM_START,CONTINUE_COIL};

typedef  CArray <int,int > FingerList;
typedef  CArray < FingerList, FingerList > StripList;



class CLineDoc : public CDocument
{
// protected:
public:
	CLineDoc();           // protected constructor used by dynamic creation
	// DECLARE_DYNCREATE(CLineDoc)
	DECLARE_DYNAMIC(CLineDoc)

//	CLineDoc&  operator=(CLineDoc& source);
// Attributes
public:
CTargetBoard c_targetBoard;
CTargetBoard c_markerBoard;
int 	c_numLanes;


// versione del programma eseguibile che ha effettuato acquis
double c_exeVer;

// vettore n fingerList dove n e` il numero di strip
// utilizzato in gestValCounter per sapere se un difetto e` dentro uno strip
StripList c_stripList;

// Lista dei celle proibite xChe` coincidenti con taglio-finger
FingerList c_fingerList;
// BOOL		c_useFingerList;
// Lista di transito per gestire strip automatici dif=252
FingerList c_tmpFingerList;
// lista di celle che hanno superato la soglia per difetti tipo B
ClassBFilterList c_classBFilterList;

// liste per gestire modalita` alternata 
// i difetti della seconda testa vengono gestiti dal doc TOP 
// ma con lista separata
FingerList c_fingerList2;
FingerList c_tmpFingerList2;
BOOL c_useFingerHw; 
//DifCoil c_difCoil;
COILTYPE  c_difCoil;
COILTYPE  c_markerCoil;

// struttura di appoggio per simulazione fori
// spostata dentro CLineDoc per gestire + documenti
RandDif		randDif;
RandDif		randMarker;
// GroupSchede schede;
// Report 100 metri
// DifRep100  dif100;

// AS400 File Param
public:

// Filter variable
FilterParam	c_filterParam;

// configurazione messaggio allarme su schede/ricevitori autotest
BOOL c_skipRxInAutotestMsg;
// configurazione indice prima scheda classe AB nell'HW
// puo` essere 0 ( normale) 1 schede ab e cd invertite
int	c_firstBoardClassAB;

BOOL c_skipHoleAInspection;

// identificativo del lato del rotolo top o bottom
// rotoloExt scambiato con cSide
CString	c_side;
// lato attivo in modo single sheet
BOOL	c_sideActive;

CString c_rotolo;		// Nome compatibile as400
CString c_rotolo2;		// Separatore due nomi di rotolo diversi
// 0 indica prima lavorazione
// da 1 in poi sono le rilavorazioni
int c_rotoloRil;			// Csm20El2013 estensione rilavorazione

long c_fileLogPosition;	// Ultima scrittura in file log

//c_bolla diventata c_densita 
CString c_densita;
CString c_lega;
double c_spessore;
CString c_cliente;
double c_larghezza;
// nota libera per operatori
CString	c_freeNote;

// orientamento strip Right/left bool;
int	c_rightAlignStrip;


// offset a sx posizione primo coltello
// tra bordo materiale e primo coltello
double c_offsetSx;
double c_posDiaframma; 
double c_posColtello; 
// aggiunti trim Left e right
double c_trimLeft;
double c_trimRight;
// blank inizio coil primi xx metri ispezionati ( era 20 mt )
int c_blankCoil;
int c_limUpdateDiaf;	// diaframma aggiornato solo nei primi limUpdateDiaf
//---------------
time_t c_startTime;		// istante inizio coil
time_t c_stopTime;		// istante fine coil
// 

// comando nuovo per disegno
double c_posDiaframmaSx;
double c_posDiaframmaDx;
// gestione diaframmi testa 2 modalita` singleCoil
double c_posDiaframmaSx2;
double c_posDiaframmaDx2;
BOOL	c_useDiaframmi;


// opzioni di visualizzazione stripLine
BOOL	c_viewStripBorder;
BOOL	c_viewStripBorderPosition;


// base di calcolo densita`  
double c_densityCalcLength;

//---------------
CString c_firstRising;
int		c_mode;
int		c_numTheoryStrip;
BOOL	c_alarmStripActive;
BOOL	c_alarmCheckShutterLeftActiveTop;
BOOL	c_alarmCheckShutterRightActiveTop;
BOOL	c_alarmCheckShutterLeftActiveBot;
BOOL	c_alarmCheckShutterRightActiveBot;

// Bool indica avvenuta stampa in automatico
BOOL c_autoPrinted;		// already auto printed
BOOL c_densityAlarmActive;

// dimensione in byte dei valori delle classi
// ricevuti da VAL_COUNTER
int c_sizeClassi;

// Numero Cpu utilizzate serve a Layout per disegnare scala orizzontale
int c_numCpu;
int c_numCpuLeftSide;
unsigned short c_valInput;
// Ricetta
public:
CString	nomeRicetta;
double	sogliaA;
double	sogliaB;
double	sogliaC;
double	sogliaD;

// soglia periodici
int		c_sogliaPer;

// Parametri specifici marker
// int		c_firstZonePosition; // distanza in mm prima zona dal diaframma
int		c_holeClassMarking;				// 1 = class A
										// 2 = class B
										// 3 = class C
										// 4 = class D											
int 	c_itmd;							// Inspection to markers distance in cm  	
int		c_markersDisable;			    // bitmask 1 = disable marker 0 enable
int		c_markersOnTime;			    // the length of marker in cm
int		c_valZoneNumberSize[MAX_NUMSTRIP]; // width for each zone number in mm.



// utilizzato in gestvalHole2
int		c_bitMaskPck;


// Configurazione Allarmi
int secAlarmSound;

CDAllarmi densitaAlarm;
CDAllarmi periodAlarm;
CDAllarmi systemAlarm;

BOOL diskAlarmActive;
BOOL serAlarmActive;

BOOL serMsgReceived;
// Parametri

protected:
// stato HDE
int c_statoHdeTop;		// stato del micro ricevuto e visualizzato
int c_statoHdeBot;	// stato del micro ricevuto e visualizzato

public:
double	c_fattCorrDimLin;		// Centimetro reale
double	c_lunResStop;			// Dimensione residua allo stop

// Stati sistema
// Indica Sistema attivo
BOOL systemOnLine;
BOOL systemOnTest;

// Indica sistema trasferisce 100 m
BOOL systemLoading;
// I dati sono stati caricati 
BOOL loadedData;

//------------------------
// Layout
double TrendLScalaY;
double TrendDScalaY;
int TrendLScalaX;

// valore in metri di ogni step asse X
// 10 metri primi sistemi
// 1 metro sistema circuit foil
// int BaseStepX;
int TrendDScalaX;

// identifica ID vista attiva (serve per CDynViewDocTempl)
int viewID;

// Nome fabbrica
CString c_company;

// Operations
public:

	int getStatoHde(bool fromTop=true){return (fromTop?c_statoHdeTop:c_statoHdeBot);};
	void setStatoHde(int st,bool fromTop=true){if(fromTop)c_statoHdeTop=st;else c_statoHdeBot=st;};


CString& getCompany(void) {return(c_company);};

// generazione difetti random 
void HoleGenerator(void); 
// generazione marker random 
void MarkerGenerator(void); 
// Timer setting demo
int millisec;
// valore in metri tra due difetti
int c_holeRate;

// Aggiornamento allarmi soglie difetti
void updateDifAlarm(); // usata da holeGeneartor e gestValCounter
//

// clear c_classBFilterList
void resetClassBFilterList()
	{c_classBFilterList.SetSize(c_difCoil.getNumSchede());
		for(int i=0;i<c_classBFilterList.GetSize();i++)
			c_classBFilterList[i] = 0;};			

void setSide(CString s) // imposta lato di ispezione, aggiorna anche titoli fin. allarme
	{c_side = s;
	s = systemAlarm.GetTitle();
	s += " ";s += c_side;
	systemAlarm.SetTitle(s);
	s = densitaAlarm.GetTitle();
	s += " ";s += c_side;
	densitaAlarm.SetTitle(s);
	};

BOOL isTopSide(void) 
	{return (c_side == CSM_TOP_SIDE);};

	//	
// Calcolo dinamico bande visualizzate
BOOL setSizeBande(void);
// -----------------

// torna valore prima banda sinistra
// ottenuta dalla posizione diafLeft 
int getRelativePosBanda(int absBanda)
{

int rBanda = (int ) absBanda - (int)((c_trimLeft+c_posDiaframmaSx)/SIZE_BANDE);

return rBanda;
}

// torna valore relativo al bordo 
// ottenuta dalla posizione diafLeft 
double getRelativePos(double pos)
{

double rPos =  pos + (c_trimLeft+c_posDiaframmaSx);

return rPos;
}


//----------------------------
double getAlDens1Cm(void)
{
// Sab = sogliaA + sogliaB
// Sric = superficie densita` in ricetta
// Srif = superficie riferimento = lungh tot * dim cella = lTot * 0.01
// proporzione 
// Sab : Sric = X : Srif
// X = (Sab * Srif)/Sric
if (c_densityCalcLength == 0.)
	return DBL_MIN;
double sogliaAllarme = 0.;
for(int i=0;i<c_difCoil.getNumClassi();i++)
	sogliaAllarme += c_difCoil.c_sogliaAllarme[i];

// sizebande in mm
return ((sogliaAllarme) * (c_difCoil.getMeter() * (SIZE_BANDE/1000.)) / 
		c_densityCalcLength);
}
//----------------------------
double getAlTrend10m(int classe)
{
// Sx = sogliaX   dove x e` classe 
// Larg = larghezza
// S10m = Sx * Largh 10/sup-ricetta 	
// X = (Sab * Srif)/Sric
double soglia;
switch(classe)
	{default:
	case 'A':
	case 0 : soglia = c_difCoil.c_sogliaAllarme[0];break;
	case 'B':
	case 1 : soglia = c_difCoil.c_sogliaAllarme[1];break;	
	case 'C':
	case 2 : soglia = c_difCoil.c_sogliaAllarme[2];break;
	case 'D':
	case 3 : soglia = c_difCoil.c_sogliaAllarme[3];break;
	}
// getLargeSize torna valore mm
return (soglia * 10. * (c_difCoil.getLargeSize() * 0.001)/ c_densityCalcLength);
}



// combina nome rotolo con estensione
CString getRotolo(void)
	{CString s1;
	if ((c_mode == 0)&&(!isTopSide()))
		s1 = c_rotolo2;
	else
		s1 = c_rotolo;
	// s1 += "_";
	s1 += c_firstRising;
	s1 += "_";
	if (c_mode == 0)
		s1 += c_side;
	else
		// modo 1-singleCoil e 2-singleSheet
		s1 += '0';
	return (s1);};

// combina nome rotolo con strip estensione
CString getStripRotolo(int stripNumber, int alzata=-1,int element=-1)
	{
	if (element < 0)
		element = c_rotoloMapping.c_lastElemento;
	CString s1,s;
	s1 = getRotolo();
	s1 += "_";
	// s.Format("%02d",stripNumber+1);
	s.Format("%02d",c_rotoloMapping[element].at(stripNumber).c_posizione);
	s1 += s;
	long subCoil=0;
//	for (int i=0;i<=element;i++)
//		subCoil += c_rotoloMapping[i].getCurAlzNumber(alzata);
	subCoil = c_rotoloMapping.getGlobalRisingId(element,alzata);
	s1 += getStringFromInt(subCoil+1);
	return (s1);};

// trasforma numero in stringa con base numerazione 25
// A = 1 Z = 25  ZA = 26 ZB = 27 etc
CString getStringFromInt(int v)
{
CString s;
while (v > 25)
	{
	s += "Z";
	v -= 25;
	}

s += TCHAR('A' + v);
return s;
}

int getIntFromString(CString s)
{
int v=0;
s.MakeUpper();
for (int i=0;i<s.GetLength();i++)
	{
	v += (s[i] - 'A');
	}

return v;
}

BOOL fillFingerList(FingerList& fingerList);
// maschera per identificazione bande dentro strip
BOOL fillStripList(void);

long getCurAlzNumber()
{long alz=0;
long elemento = c_rotoloMapping.c_lastElemento;
alz = getAlzNumber(elemento,-1);
return(alz);};

CString getCurAlzString(void)
{
return(getStringFromInt(getCurAlzNumber()));
};

CString getAlzString(int elem,int ris)
{
return(getStringFromInt(getAlzNumber(elem,ris)));
}

long getAlzNumber(int elemento,int ris)
{long alz;
if ((elemento < 0 )||
	(elemento > c_rotoloMapping.c_lastElemento))
	return -1;
alz = c_rotoloMapping.getGlobalRisingId(elemento,ris);
return(alz);};

#define USE_YYYYMM
CString getMonthFolder()
{

CString monthDir;
CTime st(c_startTime);
#ifdef USE_YYYYMM
monthDir = _T("DD") + st.Format ("%Y%m");
#else
monthDir = _T("DD") + st.Format ("%m%y");
#endif

return monthDir;
}

// modificato per ELVAL
CString getFileName(void)
{
CString s1;
CTime t(c_startTime);
s1 = getRotolo();
s1 += t.Format("%d%H%M%S");
return (s1);};

CString getFileStripName(int stripNumber,int alzata = -1)
{
CString s1;
CTime t(c_startTime);
s1 = getStripRotolo(stripNumber,alzata);
s1 += t.Format("%d%H%M%S");
return (s1);};

ViewType GetActualViewType(void);
int GetActualViewID(void);
int GetViewID(CView* pView);
// Chiamata da CDocTemplate per abilitare cambio viste
BOOL isChangeViewEnabled(int id ){
	
	if ((id == ID_VIEW_AS_BEGIN)||
		(id == ID_VIEW_AS_END))
		// prima e ultima view init e IO offLine
		return(!systemOnLine);
	else
		return(systemOnLine);
	};

// init memory at start every coil
void newCoil(StartMode warmStart,CDStripPosition* pDialog=NULL);
void SlaveStart(CLineDoc* pDoc,BOOL testMode,int continueCoil); 
void SlaveSaveStop(CLineDoc* pDoc); 
void fileStart(BOOL testMode); 


CString vFori;
int GetForiCod(void) {if (vFori == "A") return 'A';
					if (vFori == "B") return 'B';
					if (vFori == "C") return 'C';
					if (vFori == "D") return 'D';
					return('A');
					};
CString &getFori(void){return(vFori);};
// Insert Values
void SetTrendLScalaY (double v){TrendLScalaY = v;UpdateAllViews(NULL);};
void SetTrendLScalaX (int v){TrendLScalaX = v;UpdateAllViews(NULL);};
void SetTrendDScalaY (double v){TrendDScalaY = v;UpdateAllViews(NULL);};
void SetTrendDScalaX (int v){TrendDScalaX = v;UpdateAllViews(NULL);};

// Available For Views
double GetTrendLScalaY (void){ return(TrendLScalaY);};
int GetTrendLScalaX (void){ return(TrendLScalaX);};
double GetTrendDScalaY (void){ return(TrendDScalaY);};
int GetTrendDScalaX (void){ return(TrendDScalaX);};
// Dimensione in metri di ogni step

// double GetTrendStep (void){ return(c_fattCorrDimLin * BaseStepX);};
double GetTrendStep (void){ return(c_difCoil.getMeterBaseStepX());};

// Operations
public:
// modificata struttura memo difetti 
// da schede a DifCoil
void GetVPosition(CString &str)
	{str.Format ("%4.0lf",c_difCoil.getMeter());};
int GetVPosition(void )
	{return ((int ) c_difCoil.getMeter());};

int getVTotalLength(void )
	{int v = (int ) (c_difCoil.getMeter() - c_lunResStop);
	return (v>0?v:0);};


int GetTrendLVPositionDal(void )
	{int v;
	v = (int )(c_difCoil.getMeter() - TrendLScalaX);
	v = v>0 ? v : 0;
	return v;};

int GetTrendDVPositionDal(void )
	{int v;
	v = (int )(c_difCoil.getMeter()-TrendDScalaX);
	v = v>0 ? v : 0;
	return v;}; 


void GetVPositionDal(CString &str,ViewType type)
 	{int v;
	switch (type)
		{case DENSVIEW:
			v = GetTrendDVPositionDal(); 
			break;
		default:
		case LINEVIEW :
			v = GetTrendLVPositionDal();
			break;
		}
	str.Format ("%d",v);};

void GetVPositionDal(CString &str)
	{
	GetVPositionDal(str,GetActualViewType());
	};

CString &GetVCliente(void)
	{return (c_cliente);};

// estrae dalla directory estensione numerica dei file 
// esistenti indicano rilavorazioni
int getLastRotoloRil(CString &baseRotolo);

// Modificati valori di configurazione visualizzazione
BOOL UpdateBaseDoc(void);

// Chiusura acquisizione
void FileSaveStop(BOOL bothProcess); 
// salvataggio dati
void FileSave(void);
// inizializzazione comuni a Start e Cambio Asse 
// void FileStart(BOOL warmStart); 

// OBSOLETA!!!
// Selezione rotolo
// BOOL rotoloSelect(); 



// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLineDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual void OnCloseDocument();
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	long c_baseContinuaRotolo;
	int getPosColtello(int elemento);
	double calcLarghezzaNastri(int elemento);

	BOOL checkLarghezza();
	int c_statoAlzata;
	CRotolo c_rotoloMapping;
	// se change = false aggiunge elemento altrimenti aggiorna 
	// bobine lastElemento
	BOOL tagliSelect(BOOL changeLast,BOOL forceAdd);
	BOOL tagliSelect(CDStripPosition* pDialog,BOOL changeLast);

	// ricerca rotolo in archivio
//	BOOL searchRotolo(LPCSTR idRotolo);
	CString c_impianto;
	CString c_lavorazione;

	void gestValCounterBigHole(PVOID p, DWORD size);
	void gestValExtCounter(PVOID p, DWORD size,BOOL fromBottom=1);
	// 3 byte 
	// pacchetto long pos, char cella, nibble A nibble B, 
	// gestisce tutti i tipi anche bighole
	void gestValCounter2(PVOID p, DWORD size,BOOL fromBottom,int mask=0);
	// gestione allarmi laser rotti
	void gestValLaser(PVOID p,DWORD size,bool fromTop=1);

	// Gestione sistema micro (receive)
	void gestValPeriod (PVOID p,DWORD size); // Gestisce arrivo VAL_PERIOD
	void gestValAlarm (PVOID p,DWORD size,BOOL fromTop=1); // Gestisce arrivo VAL_ALARM
	void gestValTestSer (PVOID p,DWORD size); // Gestisce arrivo VAL_PERIOD
	void gestValStatoHde (PVOID p,DWORD size,BOOL fromTop=1); // Gestisce arrivo VAL_STATO
	void gestValStop(PVOID p,DWORD size);
	void gestValClose (PVOID p,DWORD size); 
	void gestValOpen (PVOID p,DWORD size); 
	void gestValAlzata (PVOID p,DWORD size); 
	void gestValDiaframma (PVOID p,DWORD size); 
	void gestValExtDiaframma (PVOID p,DWORD size,BOOL fromBottom=1); 
	void gestValRisAutotest(PVOID p,DWORD size);
	void gestValInput (PVOID p,DWORD size); 

	// Gestione sistema micro (send)
	void sendStartStop (BOOL start,BOOL test);
	void sendAlarmDif (void);
	void sendValSoglie (void);
	void sendValFcdl (void);
	void sendValAlPulse (void);
	void sendValOutput (unsigned short v);
	void sendIOStartStop (BOOL start);
	void sendStartTestDiaf (BOOL left);
	void sendUseFingerHw (void);
	void sendAlMisaligned (BOOL alarm);
	void sendValDistOddEven (void);
	void sendAutotest (void);
	void sendValHwAlarm (bool alarm);				// solo per top, replica stato allarme della bot

	BOOL save (CFileBpe *cf,CProgressCtrl *progress=NULL);
	BOOL load (CFileBpe *cf,CProgressCtrl *progress=NULL);

	virtual ~CLineDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CLineDoc)
	afx_msg void OnTestgrafico();
	afx_msg void OnViewAggiorna();
	afx_msg void OnFileStart();
	afx_msg void OnFileStop();
	afx_msg void OnViewPeriodici();
	afx_msg void OnViewTrend();
	afx_msg void OnUpdateViewPeriodici(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewTrend(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileStart(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileStop(CCmdUI* pCmdUI);
	afx_msg void OnViewSystem();
	afx_msg void OnUpdateViewSystem(CCmdUI* pCmdUI);
	afx_msg void OnFileTstart();
	afx_msg void OnUpdateFileTstart(CCmdUI* pCmdUI);
	afx_msg void OnFileAlzata();
	afx_msg void OnUpdateFileAlzata(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	// updateViews, non viene + fatta sincrona con la comunicazione ma temporizzata su 500 msec
	int updateView(void);
	// Variabile di stato settata da gestValExtCounter e gestValBigHole per sincronizzrsi con updateView
	BOOL c_redrawAll;
	BOOL c_viewScroll;
	BOOL c_redraw;
};

#endif
