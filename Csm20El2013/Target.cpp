//------------------------------------------------------------
//
//
//
//					Target.h
//
//
//
//
//-------------------------------------------------------------


#include "stdafx.h"
#include "Target.h"



CTarget::CTarget (void )
{
banda = 0;
color = RGB (255,255,255);
size = CSize(10,10);
}


void CTarget::draw(CPoint p,CDC	*pdc)	
{
CBrush brush (color);
CBrush *pOldBrush;
pOldBrush = pdc->SelectObject(&brush);

CPen pen (PS_SOLID,1,color);
CPen *pOldPen;
pOldPen = pdc->SelectObject(&pen);
p.y -= size.cy;
CRect r (p,size);
r.DeflateRect(1,1);
pdc->Ellipse(r);

pdc->SelectObject(pOldPen);
pdc->SelectObject(pOldBrush);

}

COLORREF	CTarget::setColor(COLORREF rgb)
{
COLORREF c = color;
color = rgb;

return(c);
}


CSize	CTarget::setSize(CSize &s)
{
CSize oldSize;
oldSize = size;
size = s;
return(oldSize);
}


//----------------------------------------------------------
//
//		CTargetRow
//
//----------------------------------------------------------


CTargetRow::CTargetRow (CTargetRow& source)
{
pos = source.pos;
state = source.state;
c_excludeYText = source.c_excludeYText;
for (int i=0;i<source.GetSize();i++)
	{
	Add (source.GetAt(i));
	}
}

CTargetRow &CTargetRow::operator=(CTargetRow& source)
{
RemoveAll();
pos = source.pos;
state = source.state;
c_excludeYText = source.c_excludeYText;
for (int i=0;i<source.GetSize();i++)
	{
	Add (source.GetAt(i));
	}
return (*this);
}


void CTargetRow::draw(CPoint p,CPoint pr,CDC *pdc,int pxFC,
					  int pxC,int nbC,int pxR,int mirror,ColorArray *pColorArray,BOOL useColorArray)	
{

CString label;
double dPos = pos;
dPos /= 1000.;

if(!c_excludeYText)
	{
	label.Format("%4.2lf",dPos);
	pdc->TextOut(p.x - 12,p.y,label);
	pdc->MoveTo(p.x -10,p.y-pxR/2);
	pdc->LineTo(p.x,p.y-pxR/2);
	}

int tOldBanda = -1;

switch (mirror)
	{
	case 1:
		{
		for (int i=0;i<GetSize();i++)
		{
		CPoint tPos (pr);
		int b = ElementAt(i).getBanda();
		if (b == tOldBanda)
			{
			tOldBanda = b;
			continue;
			}
		tOldBanda = b;
		if (nbC == 1)
			{// una sola banda per colonna
			tPos.x -= pxFC + (b)*pxC + (pxC+ElementAt(i).getSize().cx)/2;
			}
		else
			{// piu` di una banda per colonna
			int col = (b)/nbC; // b-1
			// vado alla colonna
			tPos.x -= pxFC + col*pxC;
			// interno alla colonna
			col = (b)%nbC;  // b-1
			tPos.x -= col*pxC/nbC;

			int deltaX = (pxC/nbC+ElementAt(i).getSize().cx)/2;
			if (deltaX > 0)
				tPos.x -= deltaX;
			}
		BOOL draw = TRUE;
		if (useColorArray)
			{for (int j=0; j< pColorArray->GetSize();j++)
				{
				if (ElementAt(i).getColor() == pColorArray->ElementAt(j))
					{
					draw = FALSE;
					break;
					}
				}
			}
		if (draw)
			ElementAt(i).draw(tPos,pdc);
		}
	}
	break;
case 0:
case 2:
	{// no mirror
	for (int i= (mirror)?GetSize()-1:0;
		(mirror)?i>=0:i<GetSize();(mirror)?i--:i++)
		{
		CPoint tPos (p);
		int b = ElementAt(i).getBanda();
		if (b == tOldBanda)
			{
			tOldBanda = b;
			continue;
			}
		tOldBanda = b;
		if (nbC == 1)
			{// una sola banda per colonna
			tPos.x += pxFC + (b)*pxC + (pxC-ElementAt(i).getSize().cx)/2;
			}
		else
			{// piu` di una banda per colonna
			int col = (b)/nbC; // b-1
			// vado alla colonna
			tPos.x += pxFC + col*pxC;
			// interno alla colonna
			col = (b)%nbC;  // b-1
			tPos.x += col*pxC/nbC;

			int deltaX = (pxC/nbC-ElementAt(i).getSize().cx)/2;
			if (deltaX > 0)
				tPos.x += deltaX;
			}
		BOOL draw = TRUE;
		if (useColorArray)
			{
			for (int j=0; j< pColorArray->GetSize();j++)
				{
					if (ElementAt(i).getColor() == pColorArray->ElementAt(j))
					{
					draw = FALSE;
					break;
					}
				}
			}
		if (draw)
			ElementAt(i).draw(tPos,pdc);
		}
	}
	}	
}

// funzione di disegno con bande tutte diverse
void CTargetRow::drawEx(CPoint p,CPoint pR,CDC	*pdc,int pxFC,int nbC,int pxR,
						CArray <int,int> &stripSize,int mirror,BOOL useGrid,ColorArray *pColorArray,BOOL useColorArray )	
{

CString label;
double dPos = pos;
dPos /= 1000.;
label.Format("%4.2lf",dPos);
pdc->TextOut(p.x - 12,p.y,label);
pdc->MoveTo(p.x -10,p.y-pxR/2);
pdc->LineTo(p.x,p.y-pxR/2);

CPen penGrid,*oldPen;
penGrid.CreatePen(PS_SOLID,2,RGB(128,128,128));


int tOldBanda = -1;
switch (mirror)	
	{
	case 1:
		{
	for (int i=0;i<GetSize();i++)
		{
		CPoint tPos (pR);
		int b = ElementAt(i).getBanda();
		if (b == tOldBanda)
			{
			tOldBanda = b;
			continue;
			}
		tOldBanda = b;
		// una sola banda per colonna
		int currentPosX = 0;
		for (int j=0;j<b;j++)
			currentPosX += abs(stripSize[j]);
		tPos.x -= (pxFC + currentPosX + (abs(stripSize[b])+ElementAt(i).getSize().cx)/2);

		BOOL draw = TRUE;
		if (useColorArray)
			{
			for (int j=0; j< pColorArray->GetSize();j++)
				{
					if (ElementAt(i).getColor() == pColorArray->ElementAt(j))
					{
					draw = FALSE;
					break;
					}
				}
			}
		if (draw)
			ElementAt(i).draw(tPos,pdc);
		}

	int currentPosX=pxFC;
	for (int i=0;i<stripSize.GetSize();i++)
		{
		CRect r;
		r.right = pR.x - currentPosX;
		r.left = r.right - abs(stripSize[i]);
		r.top = p.y;
		r.bottom = p.y-pxR;
		if (stripSize[i]<0)
			{
			drawAltBackground(pdc,r);		
			}

		if (useGrid)
			{
			currentPosX += abs(stripSize[i]);
			oldPen = pdc->SelectObject(&penGrid); 
			pdc->MoveTo(pR.x-(currentPosX),p.y);
			pdc->LineTo(pR.x-(currentPosX),p.y-pxR);
			pdc->SelectObject(oldPen); 
			}
		// aggiornamento sfondo

			}
		}
	break;
case 0:
case 2:
	{
	//for (int i= (mirror)?GetSize()-1:0;
	//	(mirror)?i>=0:i<GetSize();
	//	(mirror)?i--:i++)
	for (int i= 0;i<GetSize();i++)
		{
		CPoint tPos (p);
		int b = ElementAt(i).getBanda();
		if (b == tOldBanda)
			{
			tOldBanda = b;
			continue;
			}
		tOldBanda = b;
		// una sola banda per colonna
		int currentPosX = 0;
		if (mirror)
			{
			for (int j=0;j<(b);j++)
				currentPosX += (abs(stripSize[j]));
				//currentPosX += (abs(stripSize[stripSize.GetSize()-j-1]));

			// tPos.x += pxFC + currentPosX + (abs(stripSize[stripSize.GetSize()-b-1])-ElementAt(i).getSize().cx)/2;
			tPos.x += pxFC + currentPosX + (abs(stripSize[b])-ElementAt(i).getSize().cx)/2;
			}
		else
			{
			for (int j=0;j<(b);j++)
				currentPosX += abs(stripSize[(mirror)?stripSize.GetSize()-j-1:j]);
			tPos.x += pxFC + currentPosX + (abs(stripSize[b])-ElementAt(i).getSize().cx)/2;
			}

		BOOL draw = TRUE;
		if (useColorArray)
			{
			for (int j=0; j< pColorArray->GetSize();j++)
				{
					if (ElementAt(i).getColor() == pColorArray->ElementAt(j))
					{
					draw = FALSE;
					break;
					}
				}
			}
		if (draw)
			ElementAt(i).draw(tPos,pdc);
		}
	int currentPosX=pxFC;

	for (int i= (mirror)?stripSize.GetSize()-1:0;
		(mirror)?i>=0:i<stripSize.GetSize();(mirror)?i--:i++)
		{
		CRect r;
		r.left = p.x + currentPosX;
		r.right = r.left + abs(stripSize[i]);
		r.top = p.y;
		r.bottom = p.y-pxR;
		if (stripSize[i]<0)
			{
			drawAltBackground(pdc,r);		
			}

		if (useGrid)
			{
			currentPosX += abs(stripSize[i]);
			oldPen = pdc->SelectObject(&penGrid); 
			pdc->MoveTo(p.x+(currentPosX),p.y);
			pdc->LineTo(p.x+(currentPosX),p.y-pxR);
			pdc->SelectObject(oldPen); 
			}
		}
	}
	break;
	}
penGrid.DeleteObject();

}

void CTargetRow::drawAltBackground(CDC *pDC,CRect r)
{
// background settori alternativi
// bande tutte uguali
CBrush brushAlt,*oBrush;
brushAlt.CreateSolidBrush (drawAltBkColor);
brushAlt.UnrealizeObject();
oBrush = pDC->SelectObject (&brushAlt);
pDC->FillRect(&r,&brushAlt);
pDC->SelectObject (oBrush);    

brushAlt.DeleteObject();
}



//----------------------------------------------------------
//
//		CTargetBoard
//
//----------------------------------------------------------


// Add Target to board
// if !exist row create

void CTargetBoard::add (int pos,CTarget &t,BOOL excludeLeftText)
{

// Search back 
// e` piu` probabile che debba appendere in fondo che in cima
//
int i=0;
for (i=GetUpperBound();i>=0;i--)
	{
	if (ElementAt(i).getPos() == pos)
		// found
		{
		ElementAt(i).Add(t);
		break;
		}
	}
if (i < 0)
	{// new Row
	CTargetRow row;
	row.excludeYText(excludeLeftText);
	row.setPos(pos);
	row.Add(t);
	Add (row);
	}
}


void CTargetBoard::draw(CRect r,CDC	*pdc)	
{
int nr = r.Size().cy / pxPerRow;
CFont fnt,*oFnt;

fnt.CreateFont(-1*pxPerRow,0,0,0,FW_NORMAL,0,0,0,DEFAULT_CHARSET,OUT_TT_PRECIS,
			   CLIP_CHARACTER_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,
			   (LPCTSTR) font.name);
oFnt = pdc->SelectObject(&fnt); 
pdc->SetTextAlign(TA_RIGHT | TA_BOTTOM);
pdc->SetBkMode (OPAQUE);
pdc->SetBkColor (textBkColor);


for (int i=GetUpperBound(),j=0;j<GetSize()&&(i>=0);i--,j++)
	{// divido in righe
	CPoint p  (r.TopLeft().x,r.BottomRight().y-j*pxPerRow);	
	CPoint pR (r.BottomRight().x,r.BottomRight().y-j*pxPerRow);
	if (p.y > r.TopLeft().y)
		{
		ElementAt(i).setScope(CTINSCOPE);
		if (c_stripSize.GetSize() == 0)
			ElementAt(i).draw(p,pR,pdc,pxFirstCol,pxPerCol,nBandePerCol,pxPerRow,c_mirror,&c_excludeDotColor,
						c_useColorArray);
		else
			{
			ElementAt(i).drawEx(p,pR,pdc,pxFirstCol,nBandePerCol,pxPerRow,c_stripSize,c_mirror,c_useGrid,
				&c_excludeDotColor,c_useColorArray);
			}
		}
	else
		ElementAt(i).setScope(CTOUTSCOPE);

	}
pdc->SelectObject(oFnt); 

}

// Ridisegna solo riga index
void CTargetBoard::reDraw(int index,CRect r,CDC	*pdc)	
{
int nr = r.Size().cy / pxPerRow;
CFont fnt,*oFnt;

fnt.CreateFont(-1*pxPerRow,0,0,0,FW_NORMAL,0,0,0,DEFAULT_CHARSET,OUT_TT_PRECIS,
			   CLIP_CHARACTER_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,
			   (LPCTSTR) font.name);
oFnt = pdc->SelectObject(&fnt); 
pdc->SetTextAlign(TA_RIGHT | TA_BOTTOM);
pdc->SetBkMode (OPAQUE);
pdc->SetBkColor (textBkColor);


if ((index <= GetUpperBound())&&
	(index >= 0))
	{// divido in righe
	CPoint p (r.TopLeft().x,r.BottomRight().y-(GetUpperBound()-index)*pxPerRow);
	CPoint pR (r.BottomRight().x,r.BottomRight().y-(GetUpperBound()-index)*pxPerRow);
	if (p.y > r.TopLeft().y)
		{
		ElementAt(index).setScope(CTINSCOPE);
		// ElementAt(index).draw(p,pdc,pxFirstCol,pxPerCol,nBandePerCol,pxPerRow);
		if (c_stripSize.GetSize() == 0)
			ElementAt(index).draw(p,pR,pdc,pxFirstCol,pxPerCol,nBandePerCol,pxPerRow,c_mirror,
				&c_excludeDotColor,c_useColorArray);
		else
			{
			ElementAt(index).drawEx(p,pR,pdc,pxFirstCol,nBandePerCol,pxPerRow,c_stripSize,c_mirror,c_useGrid,
				&c_excludeDotColor,c_useColorArray);
			}
		}
	else
		ElementAt(index).setScope(CTOUTSCOPE);

	}
pdc->SelectObject(oFnt); 

}

void CTargetBoard::scroll(int sr,CRect rInt,CRect rClip,CDC *pdc)	
{
// scroll rectangle
BOOL v;
CRect rect (rClip);
rect.top += sr * pxPerRow;
v = pdc->ScrollDC(0,-1* sr * pxPerRow, &rect,&rClip,
		NULL,NULL);

// calc Redraw rect
CRect scRect(rInt);
scRect.left -= 1;
scRect.top += (scRect.Size().cy - sr * pxPerRow);

CRect clRect(rClip);
clRect.top += (clRect.Size().cy - sr * pxPerRow);

// parte interna nera
CBrush brushInt,brushExt;
brushInt.CreateSolidBrush (drawBkColor);
brushInt.UnrealizeObject();
pdc->FillRect(&scRect,&brushInt);
// parte esterna grigia
brushExt.CreateSolidBrush (textBkColor);
brushExt.UnrealizeObject();
CRgn rgnExt,rgnInt,rgnAnd;
rgnExt.CreateRectRgnIndirect(&clRect);
rgnInt.CreateRectRgnIndirect(&scRect);
rgnAnd.CreateRectRgnIndirect(&scRect); 
rgnAnd.CombineRgn(&rgnExt,&rgnInt,RGN_DIFF);
  
pdc->FillRgn(&rgnAnd,&brushExt);


   
rgnExt.DeleteObject();
rgnAnd.DeleteObject();
rgnInt.DeleteObject();
brushExt.DeleteObject();
brushInt.DeleteObject();


int nr = rInt.Size().cy / pxPerRow;
CFont fnt,*oFnt;
fnt.CreateFont(-1*pxPerRow,0,0,0,FW_NORMAL,0,0,0,DEFAULT_CHARSET,OUT_TT_PRECIS,
			   CLIP_CHARACTER_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,
			   (LPCTSTR) font.name);
oFnt = pdc->SelectObject(&fnt); 
pdc->SetTextAlign(TA_RIGHT | TA_BOTTOM);
pdc->SetBkMode (OPAQUE);
pdc->SetBkColor (textBkColor);

for (int i=GetUpperBound(),j=0;j<GetSize()&&(i>=0);i--,j++)
	{// divido in righe
	CPoint p (rInt.TopLeft().x,rInt.BottomRight().y-j*pxPerRow);
	CPoint pR (rInt.BottomRight().x,rInt.BottomRight().y-j*pxPerRow);
	if ((ElementAt(i).getScope() == CTNEW)&&
		(p.y > rInt.TopLeft().y))
		{
		// ElementAt(i).draw(p,pdc,pxFirstCol,pxPerCol,nBandePerCol,pxPerRow);
		if (c_stripSize.GetSize() == 0)
			ElementAt(i).draw(p,pR,pdc,pxFirstCol,pxPerCol,nBandePerCol,pxPerRow,c_mirror,&c_excludeDotColor,c_useColorArray);
		else
			{
			ElementAt(i).drawEx(p,pR,pdc,pxFirstCol,nBandePerCol,pxPerRow,c_stripSize,c_mirror,c_useGrid,&c_excludeDotColor,c_useColorArray);
			}
		ElementAt(i).setScope(CTINSCOPE);
		}
	}
pdc->SelectObject(oFnt); 
}

//-----------------------------------------------------------------------
//
//		plot disegna in modalita` visualizzazione reale dell'asse 
//					tipo compressione
//
//-----------------------------------------------------------------------

 
void CTargetBoard::plot(CRect r,CDC	*pdc,double posTop,double posBottom)	
{
CFont fnt,*oFnt;

fnt.CreateFont(-1*pxPerRow,0,0,0,FW_NORMAL,0,0,0,DEFAULT_CHARSET,OUT_TT_PRECIS,
			   CLIP_CHARACTER_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH,
			   (LPCTSTR) font.name);
oFnt = pdc->SelectObject(&fnt); 
pdc->SetTextAlign(TA_RIGHT | TA_BOTTOM);
pdc->SetBkMode (OPAQUE);
pdc->SetBkColor (textBkColor);

double yK = (double)(r.BottomRight().y-r.TopLeft().y)/fabs(posBottom - posTop);

for (int i=GetUpperBound(),j=0;j<GetSize()&&(i>=0);i--,j++)
	{// divido in righe
	CPoint p (r.TopLeft().x,2*pxPerRow+r.TopLeft().y+(int)(yK*(ElementAt(i).getPos()/1000.-posTop)));
	CPoint pR (r.BottomRight().x,r.BottomRight().y-j*pxPerRow);
	if (p.y > r.TopLeft().y)
		{
		ElementAt(i).setScope(CTINSCOPE);
		ElementAt(i).draw(p,pR,pdc,pxFirstCol,pxPerCol,nBandePerCol,pxPerRow,FALSE,&c_excludeDotColor,c_useColorArray);
		}
	else
		ElementAt(i).setScope(CTOUTSCOPE);

	}
pdc->SelectObject(oFnt); 

}

