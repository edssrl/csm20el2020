//----------------------------------------------------------
//
//			Gestione stato allami
//
//----------------------------------------------------------

#define AL_NMAX_CPU	 64
#define AL_NMAX_RECEIVER 300
#define AL_NMAX_LASER 300

#define AL_HIDE	-2	// allarme non visibile
#define AL_OFF	-1	// allarme non definito
#define AL_OK	0	// allarme non attivo
#define AL_ON	1	// allarme attivo	

// Ogni ricevitore e` doppio con due bit
// 4 classi
// quelli pari A/B Ch1
// quelli dispari C/D Ch1
// 2 classi
// quelli pari A/B Ch1
// quelli dispari A/B Ch2 


struct StatoAllarmi
	{
	private:
	int laser;
	int encoder;
	int otturatore1;
	int otturatore2;

	public:
	int alNumCpu;
	int alNumReceiver;
	int alNumLaser;
	int cpu [AL_NMAX_CPU];
	int receiver [AL_NMAX_RECEIVER];
	int laserKO [AL_NMAX_LASER];
	CString c_label;
	StatoAllarmi (void)
		{
		setInitState();			
		};
	void setInitState (void)
		{
		alNumCpu = 1;
		alNumReceiver = 10;
		alNumLaser = 10;
		// blocchetti grandi
		laser = AL_OFF;
		encoder = AL_OFF;
		otturatore1 = AL_OFF;
		otturatore2 = AL_OFF;
		c_label = "UNK";
		for (int i=0;i<AL_NMAX_CPU;i++) cpu[i] = AL_OFF;
		for (int i=0;i<AL_NMAX_RECEIVER;i++) 
			receiver[i] = AL_OFF;
		for (int i=0;i<AL_NMAX_LASER;i++) 
			laserKO[i] = AL_OFF;
		};
	void setNumLaser(int n){if (n<=AL_NMAX_LASER) alNumLaser=n;};
	void setLabel(CString s){c_label = s;};
	void setNumCpu(int n){if (n<=AL_NMAX_CPU) alNumCpu=n;};
	void setNumReceiver(int n){if (n<=AL_NMAX_RECEIVER) alNumReceiver=n;};
	void draw (CDC *pdc,CRect rect,BOOL topSide);
	void clearReceiver(void){memset(receiver,AL_OFF,sizeof(receiver));};
	int updateLaserState(int st){return ((laser!= AL_HIDE)&&(laser != st))?(laser=st):0;};
	int updateEncoderState(int st){return ((encoder!= AL_HIDE)&&(encoder != st))?(encoder=st):0;};
	int updateOtturatore1State(int st){return ((otturatore1!= AL_HIDE)&&(otturatore1 != st))?(otturatore1=st):0;};
	int updateOtturatore2State(int st){return ((otturatore2!= AL_HIDE)&&(otturatore2 != st))?(otturatore2=st):0;};
	int getLaserState(void){return laser;};
	int getEncoderState(void){return encoder;};
	int getOtturatore1State(void){return otturatore1;};
	int getOtturatore2State(void){return otturatore2;};
	int setLaserState(int st){return (laser=st);};
	int setEncoderState(int st){return (encoder=st);};
	int setOtturatore1State(int st){return (otturatore1=st);};
	int setOtturatore2State(int st){return (otturatore2=st);};
	bool isAlarmActive(bool fullCheck){
	// blocchetti grandi
	if (fullCheck)
		{
		if (laser == AL_ON) return true;
		if (otturatore1 == AL_ON) return true;
		if (otturatore2 == AL_ON) return true;
		for (int i=0;i<alNumReceiver;i++) 
			if (receiver[i] == AL_ON) return true;
		}
	if (encoder == AL_ON) return true;
	for (int i=0;i<alNumCpu ;i++) 
		if (cpu[i] == AL_ON) return true;

	// Laser esclusi
	//for (int i=0;i<alNumLaser;i++) 
	//	if (laserKO[i] == AL_ON) return true;
	return false;
	};

	};

 