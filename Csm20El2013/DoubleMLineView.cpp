// Multi Line View : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"

#include "DoubleMLineView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Modificato update, speedUp visualization 


#define MAX_X_VIEW		5000.0

/////////////////////////////////////////////////////////////////////////////
// CLineView

IMPLEMENT_DYNCREATE(CDoubleMLineView, CView)

CDoubleMLineView::CDoubleMLineView()
{

lastActualMeterTop =  0;
lastActualMeterBot =  0;
c_numStepX=1;
c_stepScaler=1;


vType = MULTILINEVIEW;
layoutTop.setViewType(MULTILINEVIEW);
layoutBottom.setViewType(MULTILINEVIEW);

// Init layout Fix Attributes
CProfile profile;
CString s;

layoutTop.fCaption.size =  profile.getProfileInt("MLineLayoutTop","CaptionSize",110);
layoutTop.fLabel.size = profile.getProfileInt("MLineLayoutTop","LabelSize",100);
layoutTop.fNormal.size = profile.getProfileInt("MLineLayoutTop","NormalSize",70);

layoutBottom.fCaption.size =  profile.getProfileInt("MLineLayoutBottom","CaptionSize",110);
layoutBottom.fLabel.size = profile.getProfileInt("MLineLayoutBottom","LabelSize",100);
layoutBottom.fNormal.size = profile.getProfileInt("MLineLayoutBottom","NormalSize",70);

// Top
s = profile.getProfileString("MLineLayoutTop","NewLine","8");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutTop.c_nlAfterLabel [v] = 1;
	}
// Bottom
s = profile.getProfileString("MLineLayoutBottom","NewLine","8");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutBottom.c_nlAfterLabel [v] = 1;
	}

// Top
s = profile.getProfileString("MLineLayoutTop","LabelOrder","0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutTop.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}
// Bottom
s = profile.getProfileString("MLineLayoutBottom","LabelOrder","0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layoutBottom.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

// Top
s = profile.getProfileString("MLineLayoutTop","PixelSepLabel",
							 "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layoutTop.c_pxSepLabel [k++] = v;
	if (k>9)
		break;
	}
// Bottom
s = profile.getProfileString("MLineLayoutBottom","PixelSepLabel",
							 "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layoutBottom.c_pxSepLabel [k++] = v;
	if (k>9)
		break;
	}


//s = profile.getProfileString("MLineLayout","SCaption",
//							 "TREND");
s.LoadString(CSM_GRAPHVIEW_MLINECAPTION);
layoutTop.setCaption(s);
layoutBottom.setCaption(s);

// label nel .ini
//for (int i=0;i<MAX_NUMLABEL;i++)
//	{
//	CString sIndex;
//	sIndex.Format ("SLabel%d",i);
//	s = profile.getProfileString("MLineLayout",sIndex,sIndex);
//	if (s != sIndex)
//		{
//		layoutTop.c_sLabel[i] = s;
//		}
//	}

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL0);
layoutTop.c_sLabel[0] = s;
layoutBottom.c_sLabel[0] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL1);
layoutTop.c_sLabel[1] = s;
layoutBottom.c_sLabel[1] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL2);
layoutTop.c_sLabel[2] = s;
layoutBottom.c_sLabel[2] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL3);
layoutTop.c_sLabel[3] = s;
layoutBottom.c_sLabel[3] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL4);
layoutTop.c_sLabel[4] = s;
layoutBottom.c_sLabel[4] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL5);
layoutTop.c_sLabel[5] = s;
layoutBottom.c_sLabel[5] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL6);
layoutTop.c_sLabel[6] = s;
layoutBottom.c_sLabel[6] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL7);
layoutTop.c_sLabel[7] = s;
layoutBottom.c_sLabel[7] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL8);
layoutTop.c_sLabel[8] = s;
layoutBottom.c_sLabel[8] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL9);
layoutTop.c_sLabel[9] = s;
layoutBottom.c_sLabel[9] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL10);
layoutTop.c_sLabel[10] = s;
layoutBottom.c_sLabel[10] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL11);
layoutTop.c_sLabel[11] = s;
layoutBottom.c_sLabel[11] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL12);
layoutTop.c_sLabel[12] = s;
layoutBottom.c_sLabel[12] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL13);
layoutTop.c_sLabel[13] = s;
layoutBottom.c_sLabel[13] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL14);
layoutTop.c_sLabel[14] = s;
layoutBottom.c_sLabel[14] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL15);
layoutTop.c_sLabel[15] = s;
layoutBottom.c_sLabel[15] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL16);
layoutTop.c_sLabel[16] = s;
layoutBottom.c_sLabel[16] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL17);
layoutTop.c_sLabel[17] = s;
layoutBottom.c_sLabel[17] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL18);
layoutTop.c_sLabel[18] = s;
layoutBottom.c_sLabel[18] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL19);
layoutTop.c_sLabel[19] = s;
layoutBottom.c_sLabel[19] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL20);
layoutTop.c_sLabel[20] = s;
layoutBottom.c_sLabel[20] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL21);
layoutTop.c_sLabel[21] = s;
layoutBottom.c_sLabel[21] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL22);
layoutTop.c_sLabel[22] = s;
layoutBottom.c_sLabel[22] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL23);
layoutTop.c_sLabel[23] = s;
layoutBottom.c_sLabel[23] = s;

for (int i=0;(i<=4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	if (i == 4)
		s = "Big";
	// top
	layoutTop.c_cLabel[i] = s;
	layoutTop.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layoutTop.c_cLabel[i].setTipoIcon(IPALLINO);
	// bottom
	layoutBottom.c_cLabel[i] = s;
	layoutBottom.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layoutBottom.c_cLabel[i].setTipoIcon(IPALLINO);
	}



// Top CalcDrawRect
layoutTop.c_viewTopPerc = profile.getProfileInt("MLineLayoutTop","ViewTop%",10);
layoutTop.c_viewBottomPerc = profile.getProfileInt("MLineLayoutTop","ViewBottom%",25);
layoutTop.c_viewLeftPerc = profile.getProfileInt("MLineLayoutTop","ViewLeft%",8);
layoutTop.c_viewRightPerc = profile.getProfileInt("MLineLayoutTop","ViewRight%",5);
// Bottom
layoutBottom.c_viewTopPerc = profile.getProfileInt("MLineLayoutBottom","ViewTop%",10);
layoutBottom.c_viewBottomPerc = profile.getProfileInt("MLineLayoutBottom","ViewBottom%",25);
layoutBottom.c_viewLeftPerc = profile.getProfileInt("MLineLayoutBottom","ViewLeft%",8);
layoutBottom.c_viewRightPerc = profile.getProfileInt("MLineLayoutBottom","ViewRight%",5);

StringLabelH strLH;
strLH.pos = 100;
strLH.label = "Ris1";
layoutTop.c_stringLabelH[0] = strLH;
layoutBottom.c_stringLabelH[0] = strLH;

strLH.pos = 300;
strLH.label = "Ris2";
layoutTop.c_stringLabelH[1] = strLH;
layoutBottom.c_stringLabelH[1] = strLH;

strLH.pos = 500;
strLH.label = "Ris3";
layoutTop.c_stringLabelH[2] = strLH;
layoutBottom.c_stringLabelH[2] = strLH;

layoutTop.setMode (LINEAR | NOLABELH | MSCALEV | USE_STRING_LABELH);
layoutBottom.setMode (LINEAR | NOLABELH | MSCALEV | USE_STRING_LABELH);
}

CDoubleMLineView::~CDoubleMLineView()
{
}


BEGIN_MESSAGE_MAP(CDoubleMLineView, CView)
	//{{AFX_MSG_MAP(CDoubleMLineView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDoubleMLineView drawing



// Retrieve top and bottom doc
DOC_CLASS* CDoubleMLineView::getTopDocument()
{
DOC_CLASS* pDocTop = (DOC_CLASS* )GetDocument();
if (!pDocTop->isTopSide())
	{
	CDocTemplate* pDocTemplate = pDocTop->GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	while(!pDocTop->isTopSide())
		pDocTop = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	}
return (pDocTop);
}

DOC_CLASS* CDoubleMLineView::getBottomDocument()
{
DOC_CLASS* pDocBottom = (DOC_CLASS* )GetDocument();
if (pDocBottom->isTopSide())
	{
	CDocTemplate* pDocTemplate = pDocBottom->GetDocTemplate();
	POSITION docPos = pDocTemplate->GetFirstDocPosition( );
	while(pDocBottom->isTopSide())
		pDocBottom = (CLineDoc *) pDocTemplate->GetNextDoc(docPos);
	}
return (pDocBottom);
}

// sezione posizionamento finestre
CRect CDoubleMLineView::getRectTop(void)
{
CRect rcBoundsTop;

GetClientRect (rcBoundsTop);
CPoint pt;

pt = rcBoundsTop.BottomRight();
pt.y -= rcBoundsTop.Size().cy /2;
rcBoundsTop.BottomRight() = pt;

return (rcBoundsTop);
}

CRect CDoubleMLineView::getDrawRectTop(void)
{
CRect rcBoundsTop,rect;

rcBoundsTop = getRectTop();

layoutTop.CalcDrawRect(rcBoundsTop,rect);
return (rect);
}

CRect CDoubleMLineView::getRectBottom(void)
{
CRect rcBoundsBottom,rect;

GetClientRect (rcBoundsBottom);


CPoint pt;
pt = rcBoundsBottom.TopLeft();
pt.y += rcBoundsBottom.Size().cy /2;
rcBoundsBottom.TopLeft() = pt;

return (rcBoundsBottom);
}

CRect CDoubleMLineView::getDrawRectBottom(void)
{
CRect rcBoundsBottom,rect;

rcBoundsBottom = getRectBottom();

layoutBottom.CalcDrawRect(rcBoundsBottom,rect);
return (rect);
}



/////////////////////////////////////////////////////////////////////////////
// CDoubleMLineView diagnostics

#ifdef _DEBUG
void CDoubleMLineView::AssertValid() const
{
	CView::AssertValid();
}

void CDoubleMLineView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDoubleMLineView message handlers

BOOL CDoubleMLineView::OnEraseBkgnd(CDC* pDC) 
{
// TODO: Add your message handler code here and/or call default
return TRUE;
// TODO: Add your message handler code here and/or call default
CRect r;
r = getDrawRectTop();
layoutTop.OnEraseBkgnd(pDC,r);

//GetClientRect(&r);
//layout.OnEraseBkgnd(pDC,r);

r = getDrawRectBottom();
layoutBottom.OnEraseBkgnd(pDC,r);


return TRUE;	
}

BOOL CDoubleMLineView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{

// TODO: Add your specialized code here and/or call the base class
if (!CView::Create(lpszClassName, lpszWindowName, 
	dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);

{//Top
DOC_CLASS* pDoc = (DOC_CLASS* )getTopDocument();

layoutTop.Create(this);
layoutTop.setNumClassi(pDoc->c_difCoil.getNumClassi());

for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
	{
	trendLineTop[j].setPen(CSM20GetClassColor(j));
	trendLineTop[j].setPenBig(CSM20GetClassColor(pDoc->c_difCoil.getNumClassi()));
	}

}
{//Bottom
DOC_CLASS* pDoc = (DOC_CLASS* )getBottomDocument();

layoutBottom.Create(this);
layoutBottom.setNumClassi(pDoc->c_difCoil.getNumClassi());

for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
	{
	trendLineBottom[j].setPen(CSM20GetClassColor(j));
	trendLineBottom[j].setPenBig(CSM20GetClassColor(pDoc->c_difCoil.getNumClassi()));
	}
}

return TRUE;
}



BOOL CDoubleMLineView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString s;
	s.Format("Got Notify Message from %d",(int)wParam);
	AfxMessageBox (s);
	return CView::OnNotify(wParam, lParam, pResult);
}


void CDoubleMLineView::OnDraw(CDC* pDC)
{
CDC pdcMem;
CBitmap bitMapMem,*bitMapOld;
// rc regione totale disegnocx

CRect rectB;
CRect rc;
GetClientRect (rc);
// Set base Rect

SIZE sz;
sz.cx = rc.right - rc.left;
sz.cy = rc.bottom - rc.top;

//AtlPixelToHiMetric (&sz, &m_sizeExtent);
// store natural extent
//m_sizeNatural = m_sizeExtent;

// Create mem DC
pdcMem.CreateCompatibleDC(pDC);

// Create mem bitmap
bitMapMem.CreateCompatibleBitmap(pDC,rc.right-rc.left,
								   rc.bottom-rc.top);
// Select  mem bitmap
bitMapOld = (CBitmap*)  pdcMem.SelectObject(&bitMapMem);

//draw (di.hdcDraw,rc);
CRect rcTop,rcBottom;
rcTop = getRectTop();
layoutTop.OnEraseBkgnd(&pdcMem,rcTop);

rcBottom = getRectBottom();
layoutBottom.OnEraseBkgnd(&pdcMem,rcBottom);
// Draw on bitmap 
onDraw (&pdcMem);

//onDraw (pDC);

// replay view with bitmap
pDC->BitBlt(rc.left,rc.top,rc.right-rc.left,
			rc.bottom-rc.top,&pdcMem,0,0,SRCCOPY);

// dispose object
pdcMem.SelectObject(bitMapOld);
bitMapMem.DeleteObject();
pdcMem.DeleteDC();

}

void CDoubleMLineView::onDraw(CDC* pDC)
{
// TODO: add draw code here
// TODO: add draw code here
// Call Base Class Draw
CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);

DOC_CLASS* pDocTop = getTopDocument();
DOC_CLASS* pDocBottom = getBottomDocument();

// top space
CRect rcBoundsTop;
rcBoundsTop = getRectTop();

CRect rcBoundsBottom;
rcBoundsBottom = getRectBottom();

// Set base Rect
layoutTop.setDrawRect(rcBoundsTop);
layoutBottom.setDrawRect(rcBoundsBottom);


onDraw(pDC,pDocTop,&layoutTop,trendLineTop,rcBoundsTop,lineInfoTop);

onDraw(pDC,pDocBottom,&layoutBottom,trendLineBottom,rcBoundsBottom,lineInfoBottom);

lastActualMeterTop = pDocTop->c_difCoil.getMeter();

lastActualMeterBot = pDocBottom->c_difCoil.getMeter();

}



void CDoubleMLineView::onDraw(CDC* pDC,DOC_CLASS* pDoc,Layout* pLayout,
							  SubLine *pTrendLine,CRect rcBounds,LineInfo* pLineInfo)
{

double maxValY = pDoc->GetTrendLScalaY();
double maxValX = pDoc->GetTrendLScalaX();

// init gloval var c_numStepX
// numero Passi divisione Disegno
calcNumStepX(pDoc);

updateLabel(pDoc,pLayout);

/*-----------------
// Call Base Class Draw
pLayout->c_vLabel[0] = pDoc->GetVPosition();
pLayout->c_vLabel [1] = pDoc->GetTrendLVPositionDal();
// densita D
	{
	CString str;
	pLayout->setMaxY('D'+0,pDoc->GetTrendLScalaY());
	pLayout->setMinY('D'+0,0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		pLayout->c_vLabel[2].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[2].setBColor(RGB(0,255,0));
	}
//
// densita C
	{
	CString str;
	pLayout->setMaxY('C',pDoc->GetTrendLScalaY());
	pLayout->setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		pLayout->c_vLabel[3].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[3].setBColor(RGB(0,255,0));
	}
//-----
// densita B
	{
	CString str;
	pLayout->setMaxY('B'+0,pDoc->GetTrendLScalaY());
	pLayout->setMinY('B'+0,0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		pLayout->c_vLabel[4].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[4].setBColor(RGB(0,255,0));
	}
//
// densita A
	{
	CString str;
	pLayout->setMaxY('A',pDoc->GetTrendLScalaY());
	pLayout->setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		pLayout->c_vLabel[5].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[5].setBColor(RGB(0,255,0));
	}
//-----
pLayout->c_vLabel [6] = pDoc->getRotolo();
pLayout->c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
pLayout->c_vLabel [8] = d.Format( "%A, %B %d, %Y" );
// Visualizzazione alzata
int metriAlzata;
int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
pLayout->c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
pLayout->c_vLabel [10] = s;

//s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size());
// -1 indica tutti 
// s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).getCurAlzNumber(-1));
s = pDoc->getCurAlzString();
pLayout->c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
pLayout->c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
pLayout->c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
pLayout->c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
pLayout->c_vLabel [15] = s;
// Total Holes A+B
s.Format("%6.0lf",va+vb+vc+vd);
pLayout->c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
pLayout->c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
pLayout->c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
pLayout->c_vLabel [19] = s;
/// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
pLayout->c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
pLayout->c_vLabel [21] = s;

---------------------------------------*/

//--------------------

int numClassi = pDoc->c_difCoil.getNumClassi();
// aggiungo anche classe Big ?
pLayout->setNumClassi(numClassi);
pLayout->setNumLabelClassi(numClassi+1);

CRect rectB;
// Trovo Finestra disegno (grigia)
pLayout->CalcDrawRect(rcBounds,rectB); 
pLayout->draw(pDC);


// init gloval var c_numStepX
calcNumStepX(pDoc);
int viewTrendStep = pDoc->GetTrendStep()* c_stepScaler;


int code = pDoc->GetForiCod();

for (int i=0;i<numClassi;i++)
	{
	pLineInfo[i].max = pDoc->GetTrendLScalaY();
	pLineInfo[i].min = 0.;
	pLineInfo[i].limit = pDoc->c_difCoil.c_sogliaAllarme[i];
	int locCount = (int)pDoc->c_difCoil.getMeter()/viewTrendStep;
	pLineInfo[i].count = (int)(locCount < c_numStepX)?locCount:c_numStepX; 
	pLineInfo[i].numStep = c_numStepX;
	pLineInfo[i].stepScaler = c_stepScaler;
	pLineInfo[i].actual	= (int) (pDoc->c_difCoil.getMeter()/viewTrendStep)% c_numStepX;
	// Aggiornato 1997 alloca memoria
	pLineInfo[i].newVal (pLineInfo[i].numStep+1);
	}

for (int j=0;j<= c_numStepX;j++)
	{
	// trovo indice metro dif 
	int idx = pDoc->c_difCoil.getMeterIdxLStep(j*viewTrendStep);
	for (int i=0;i<numClassi;i++)
		{
		if (idx < 0)
			{
			pLineInfo[i].val[j] = 0;
			pLineInfo[i].valBig[j] = 0;
			}
		else
			{
			pLineInfo[i].val[j] = pDoc->c_difCoil[idx].getValNum('A'+i);
			// lineInfo[i].val[j] = pDoc->c_difCoil.getValNumLStep('A'+i,j*viewTrendStep);
			if (i == pDoc->c_difCoil.getNumClassi()-1)
				{
				pLineInfo[i].valBig[j] = pDoc->c_difCoil[idx].getTotalBig();
				}
			else
				pLineInfo[i].valBig[j] = 0;
			}
		}
	}

CRect subRectB[4];
for (int i=0;i<numClassi;i++)
	{
	pTrendLine[i].setMode (LINEAR);
	subRectB[i]=rectB;
	subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
	subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
	pTrendLine[i].setScale(subRectB[i],pLineInfo[i]);
	pTrendLine[i].draw(pDC,FALSE);
	}
}



void CDoubleMLineView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
Invalidate(TRUE);	
}

void CDoubleMLineView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
// TODO: Add your specialized code here and/or call the base class
if ((lHint==0) && (pHint == NULL))
	CView::OnUpdate(pSender,lHint,pHint);	
else
	{
	// Top Update
	CDC  *pDC;
	pDC = GetDC ();
		{
		DOC_CLASS* pDoc = getBottomDocument();

		// 04-03-2010 fix Bug non settati top e bottom rett. disegno in update 
		// righe cursore non cancellate!
		CRect rcBoundsBottom;
		rcBoundsBottom = getRectBottom();
		layoutBottom.setDrawRect(rcBoundsBottom);


		static CRect rcInvalid;
		CRect rectB;
		int oldStepX,actualStepX;

		int maxValX = pDoc->GetTrendLScalaX();
		// update global variable c_numStepX
		calcNumStepX(pDoc);
		actualStepX = ((int)pDoc->c_difCoil.getMeter()/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene

		// oldStepX = ((int)lastActualMeterBot/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene
		// oldstepX e` quello prima del precedente xche` potrebbe essere stato aggiornato da 
		// pacchetto arrivato con altra classe
		oldStepX = ((int)(lastActualMeterBot-pDoc->GetTrendStep())/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene

		// Spazio Di Disegno
		// Trovo Finestra disegno (grigia)
		rectB = getDrawRectBottom();
		if ((oldStepX<0)||(actualStepX <= 0)||(actualStepX == c_numStepX))
			{// estremi del quadro
			rcInvalid = rectB;
			rcInvalid.left -= 1;
			rcInvalid.right += 1;
			}
		else
			{
			double rxr,rxl;
			rxl = rectB.left;
			rxr = rectB.right;

			rcInvalid.left = (int ) floor(rxl +((rxr - rxl)/c_numStepX *oldStepX))-1;
			rcInvalid.right = (int )ceil(rxl +((rxr - rxl)/c_numStepX *actualStepX));

			rcInvalid.top	= rectB.top;
			rcInvalid.bottom = rectB.bottom;
			}
		pDC->IntersectClipRect (rcInvalid);
		layoutBottom.OnEraseDrawBkgnd(pDC);
		layoutBottom.updateDrawRegion(pDC);
		updateTrend(pDC,pDoc,trendLineBottom,rectB,lineInfoBottom);
		updateLabel(pDoc,&layoutBottom);
		lastActualMeterBot = pDoc->c_difCoil.getMeter();
		// InvalidateRect(rcInvalid);
		}


		{// Bottom Update
		DOC_CLASS* pDoc = getTopDocument();
		// top space

		// 04-03-2010 fix Bug non settati top e bottom rett. disegno in update 
		// righe cursore non cancellate!
		CRect rcBoundsTop;
		rcBoundsTop = getRectTop();
		layoutTop.setDrawRect(rcBoundsTop);

		static CRect rcInvalid;
		CRect rectB;
		int oldStepX,actualStepX;

		int maxValX = pDoc->GetTrendLScalaX();
		// update global variable c_numStepX
		calcNumStepX(pDoc);
		actualStepX = ((int)pDoc->c_difCoil.getMeter()/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene

		// oldStepX = ((int)lastActualMeterTop/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene
		// oldstepX e` quello prima del precedente xche` potrebbe essere stato aggiornato da 
		// pacchetto arrivato con altra classe
		oldStepX = ((int)(lastActualMeterTop-pDoc->GetTrendStep())/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene


		// Spazio Di Disegno
		// Trovo Finestra disegno (grigia)
		rectB = getDrawRectTop();
		// estremi del quadro
		if ((oldStepX<0)||(actualStepX <= 0)||(actualStepX == c_numStepX))
			{
			rcInvalid = rectB;
			rcInvalid.left -= 1;
			rcInvalid.right += 1;
			}
		else
			{
			double rxr,rxl;
			rxl = rectB.left;
			rxr = rectB.right;

			rcInvalid.left = (int ) floor(rxl +((rxr - rxl)/c_numStepX *oldStepX))-1;
			rcInvalid.right = (int ) ceil(rxl +((rxr - rxl)/c_numStepX *actualStepX));

			rcInvalid.top	= rectB.top;
			rcInvalid.bottom = rectB.bottom;
			}
		pDC->IntersectClipRect (rcInvalid);
		layoutTop.OnEraseDrawBkgnd(pDC);
		layoutTop.updateDrawRegion(pDC);
		updateTrend(pDC,pDoc,trendLineTop,rectB,lineInfoTop);
		updateLabel(pDoc,&layoutTop);
		lastActualMeterTop = pDoc->c_difCoil.getMeter();
		// InvalidateRect(rcInvalid);
		}
	ReleaseDC(pDC);
	}
}


void CDoubleMLineView::updateLabel(DOC_CLASS* pDoc,Layout* pLayout)
{

pLayout->c_vLabel [0] = pDoc->GetVPosition();
pLayout->c_vLabel [1] = pDoc->GetTrendLVPositionDal();
// densita D Top
	{
	CString str;
	pLayout->setMaxY('D',pDoc->GetTrendLScalaY());
	pLayout->setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		pLayout->c_vLabel[2].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[2].setBColor(RGB(0,255,0));
	}
//

// densita C Top
	{
	CString str;
	pLayout->setMaxY('C',pDoc->GetTrendLScalaY());
	pLayout->setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		pLayout->c_vLabel[3].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[3].setBColor(RGB(0,255,0));
	}
// densita B Top
	{
	CString str;
	pLayout->setMaxY('B',pDoc->GetTrendLScalaY());
	pLayout->setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		pLayout->c_vLabel[4].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[4].setBColor(RGB(0,255,0));
	}
//

// densita A Top
	{
	CString str;
	pLayout->setMaxY('A',pDoc->GetTrendLScalaY());
	pLayout->setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	pLayout->c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		pLayout->c_vLabel[5].setBColor(RGB(255,0,0));
	else
		pLayout->c_vLabel[5].setBColor(RGB(0,255,0));
	}

//-----
pLayout->c_vLabel [6] = pDoc->getRotolo();
pLayout->c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
pLayout->c_vLabel [8] = d.Format( "%d-%m-%Y" );
// Visualizzazione alzata
int metriAlzata;

if(pDoc->c_rotoloMapping.c_lastElemento < 0 )
	return;	

int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
pLayout->c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
pLayout->c_vLabel [10] = s;

s = pDoc->getCurAlzString();
pLayout->c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
pLayout->c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
pLayout->c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
pLayout->c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
pLayout->c_vLabel [15] = s;
// Total Holes A+B
s.Format("%6.0lf",va+vb+vc+vd);
pLayout->c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
pLayout->c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
pLayout->c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
pLayout->c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
pLayout->c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
pLayout->c_vLabel [21] = s;

}


void CDoubleMLineView::updateTrend(CDC *pDC,DOC_CLASS* pDoc,SubLine *pTrendLine,CRect rectB,
									LineInfo* pLineInfo)
{

double maxValY = pDoc->GetTrendLScalaY();
double maxValX = pDoc->GetTrendLScalaX();
int code = pDoc->GetForiCod();
int numClassi = pDoc->c_difCoil.getNumClassi();

// update global variable
calcNumStepX(pDoc);

int viewTrendStep = (int)pDoc->GetTrendStep()*c_stepScaler;

int sizeInfo= 0;

for (int i=0;i<numClassi;i++)
	{
	pLineInfo[i].max = pDoc->GetTrendLScalaY();
	pLineInfo[i].min = 0.;
	pLineInfo[i].limit = pDoc->c_difCoil.c_sogliaAllarme[i];

	pLineInfo[i].count = (int)((((int)pDoc->c_difCoil.getMeter()/viewTrendStep) < c_numStepX)?
						(pDoc->c_difCoil.getMeter()/viewTrendStep) : c_numStepX); 
	pLineInfo[i].numStep = c_numStepX;
	pLineInfo[i].stepScaler = c_stepScaler;
	pLineInfo[i].actual	= (int) (pDoc->c_difCoil.getMeter()/viewTrendStep)% c_numStepX;
	// Aggiornato 1997 alloca memoria

	if (pDoc->isTopSide())
		sizeInfo= 1+(pDoc->c_difCoil.getMeter()-lastActualMeterTop)/viewTrendStep;
	else
		sizeInfo= 1+(pDoc->c_difCoil.getMeter()-lastActualMeterBot)/viewTrendStep;

	if (sizeInfo < 0)
		return;

	pLineInfo[i].newVal (sizeInfo+1);
	}

for (int j=0;j<= sizeInfo;j++)
	{
	// trovo indice metro dif 
	int idx = pDoc->c_difCoil.getMeterIdxLStep(j*viewTrendStep);
	for (int i=0;i<numClassi;i++)
		{
		if (idx < 0)
			{
			pLineInfo[i].val[j] = 0;
			pLineInfo[i].valBig[j] = 0;
			}
		else
			{
			pLineInfo[i].val[j] = pDoc->c_difCoil[idx].getValNum('A'+i);
			// lineInfo[i].val[j] = pDoc->c_difCoil.getValNumLStep('A'+i,j*viewTrendStep);
			if (i == (pDoc->c_difCoil.getNumClassi()-1))
				{
				pLineInfo[i].valBig[j] = pDoc->c_difCoil[idx].getTotalBig();
				}
			else
				pLineInfo[i].valBig[j] = 0;
			}
		}
	}

CRect subRectB[4];
for (int i=0;i<numClassi;i++)
	{
	pTrendLine[i].setMode (LINEAR);
	subRectB[i]=rectB;
	subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
	subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
		// test se cambiata dimensione vis
	if(pTrendLine[i].addNewlInfo(subRectB[i],pLineInfo[i],sizeInfo))
		pTrendLine[i].draw(pDC,TRUE);
	else
		Invalidate();		
	}

}

void CDoubleMLineView::calcNumStepX (CLineDoc* pDoc)
{
	int maxValX = pDoc->GetTrendLScalaX();
	c_numStepX = (int)((double)maxValX / pDoc->GetTrendStep());	// numero Passi divisione Disegno
	// parametrizzato numStepX al maxvalx, massimo MAX_X_VIEW pt di risoluzione non ha senso
	// visualizzarne di piu`
	c_stepScaler = (int) ((double)maxValX/(MAX_X_VIEW*pDoc->GetTrendStep()));
	if (c_stepScaler <= 0)
		c_stepScaler = 1;
	c_numStepX = c_numStepX/c_stepScaler;

	if (c_numStepX <= 0)
		c_numStepX = 1;

return;
}



