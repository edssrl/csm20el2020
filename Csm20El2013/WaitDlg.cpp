//***************************************************************************
//
// WaitDlg.cpp
//
//***************************************************************************

#include "stdafx.h"
#include "resource.h"
// #include "WaitRsc.h"
#include "WaitDlg.h"


/////////////////////////////////////////////////////////////////////////////
// CWaitDialog class implementation


CWaitDialog::CWaitDialog (BOOL* pFlag, LPCTSTR pszCaption, LPCTSTR pszText) :
CDialog ()
{
m_pFlag = pFlag;

//
// Disable the main window and create the dialog.
//

AfxGetMainWnd ()->EnableWindow (FALSE);
Create (IDD_WAITDIALOG);

//
// Initialize the dialog caption and the static text control.
//
CString s;
//SetWindowText ((pszCaption == NULL) ? "Attendere" : pszCaption);
s.LoadString(IDWAITDLG_ATTENDERE);
SetWindowText ((pszCaption == NULL) ? (LPCTSTR)s : pszCaption);
CStatic* pCtrl = (CStatic*) GetDlgItem (IDC_MSGCTRL);
// pCtrl->SetWindowText ((pszText == NULL) ? "Premi Stop per terminare " 
// "l'operazione" : pszText); 
s.LoadString(IDWAITDLG_PRESSTOP);
pCtrl->SetWindowText ((pszText == NULL) ? s : pszText); 

// clear message
SetMsg1(_T(""));
SetMsg2(_T(""));
SetMsg3(_T(""));
SetMsg4(_T(""));

// hide progress ctrl
HideProgress(TRUE);

//
// Display the dialog.
//
ShowWindow (SW_SHOW);
}


CWaitDialog::~CWaitDialog ()
{
Close ();
}


void CWaitDialog::OnCancel ()
{
CString pText;
GetPulsText(pText);

if (pText == "Stop")
	{
	CString string;
	//string.Format ("Interruzione Operazione\n Per un corretto funzionamento\n l'operazione dovra` essere ripetuta\n  INTERROMPERE ?");
	string.LoadString(IDWAITDLG_BREAK);
	if (AfxMessageBox((LPCSTR)string,MB_YESNO) == IDNO)
		{
		return;
		}
	}
*m_pFlag = FALSE;
Close ();
}


BOOL CWaitDialog::Pump (void)
{
MSG msg;
//
// Retrieve and dispatch any waiting messages.
//
while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)) 
	{
	if (!AfxGetApp ()->PumpMessage ()) {
		::PostQuitMessage (0);
	return FALSE;
	}
}

//
// Simulate the framework's idle processing mechanism.
//
LONG lIdle = 0;
while (AfxGetApp ()->OnIdle (lIdle++));
return TRUE;
}


void CWaitDialog::SetPercentComplete (int nPercent)
{
if (::IsWindow (m_hWnd)) 
	{
	if (nPercent < 0)
		nPercent = 0;
	else 
		if (nPercent > 100)
			nPercent = 100;
	CProgressCtrl* pCtrl = 
		(CProgressCtrl*) GetDlgItem (IDC_PROGRESSCTRL);
	pCtrl->SetPos (nPercent);
	}
}


void CWaitDialog::SetMessageText (LPCTSTR pszText)
{
if (::IsWindow (m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDC_MSGCTRL);
	pCtrl->SetWindowText (pszText);
	}
}

void CWaitDialog::SetMsg1 (LPCTSTR pszText)
{
if (::IsWindow (m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDC_MSG1);
	pCtrl->SetWindowText (pszText);
	}
}

void CWaitDialog::SetMsg2 (LPCTSTR pszText)
{
if (::IsWindow (m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDC_MSG2);
	pCtrl->SetWindowText (pszText);
	}
}

void CWaitDialog::SetMsg3 (LPCTSTR pszText)
{
if (::IsWindow (m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDC_MSG3);
	pCtrl->SetWindowText (pszText);
	}
}

void CWaitDialog::SetMsg4 (LPCTSTR pszText)
{
if (::IsWindow (m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDC_MSG4);
	pCtrl->SetWindowText (pszText);
	}
}

void CWaitDialog::SetPulsText (LPCTSTR pszText)
{
if (::IsWindow (m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDCANCEL);
	pCtrl->SetWindowText (pszText);
	}
}

void CWaitDialog::GetPulsText (CString &text)
{
text = "";
if (::IsWindow (m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDCANCEL);
	pCtrl->GetWindowText (text);
	}
}


void CWaitDialog::HideProgress (BOOL v)
{
if (::IsWindow (m_hWnd)) 
	{
 
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDC_PROGRESSCTRL);
	if (v)
		pCtrl->ShowWindow (SW_HIDE);
	else
		pCtrl->ShowWindow (SW_SHOW);
	}
}

void CWaitDialog::HideCancel (BOOL v)
{
if (::IsWindow (m_hWnd)) 
	{
 
	CStatic* pCtrl = (CStatic*) GetDlgItem (IDCANCEL);
	if (v)
		pCtrl->ShowWindow (SW_HIDE);
	else
		pCtrl->ShowWindow (SW_SHOW);
	}
}

void CWaitDialog::Close ()
{
if (::IsWindow (m_hWnd)) 
	{
	CWnd *wnd = AfxGetMainWnd();
	wnd->EnableWindow(TRUE);
	DestroyWindow ();
	}
}

