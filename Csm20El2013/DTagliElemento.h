#if !defined(AFX_DTAGLIELEMENTO_H__77AA5C20_2DEA_11D6_AB52_00C026A019B7__INCLUDED_)
#define AFX_DTAGLIELEMENTO_H__77AA5C20_2DEA_11D6_AB52_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DTagliElemento.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDTagliElemento dialog

#include "resource.h"
#include "SeqListCtrl.h"
#include "Elemento.h"

class CDTagliElemento : public CDialog
{
// Construction
public:
	CDTagliElemento(CWnd* pParent = NULL);   // standard constructor

	CElemento* c_pElemento;

// Dialog Data
	//{{AFX_DATA(CDTagliElemento)
	enum { IDD = IDD_TAGLI_ELEMENTO };
	CSeqListCtrl	m_listTagli;
	CString	m_elemento;
	CString	m_posColtello;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDTagliElemento)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDTagliElemento)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DTAGLIELEMENTO_H__77AA5C20_2DEA_11D6_AB52_00C026A019B7__INCLUDED_)
