#if !defined(AFX_SEQLISTCTRL_H__77AA5C22_2DEA_11D6_AB52_00C026A019B7__INCLUDED_)
#define AFX_SEQLISTCTRL_H__77AA5C22_2DEA_11D6_AB52_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SeqListCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSeqListCtrl window

#include "..\..\..\..\..\Library\UniCodeListCtrl\ListCtrl.h"
//#include "C:\User2\ListCtrl\ListCtrl.h"
class CSeqListCtrl : public gxListCtrl
{
// Construction
public:
	CSeqListCtrl();

// Attributes
public:

// Operations
public:
	virtual void onClick(int row, int col,int x, int y);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSeqListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	int c_maxSeqCounter;
	int c_seqCounter;
	virtual ~CSeqListCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSeqListCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEQLISTCTRL_H__77AA5C22_2DEA_11D6_AB52_00C026A019B7__INCLUDED_)
