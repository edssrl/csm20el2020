// DStripPosition.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "DBSet.h"
#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"
#include "MainFrm.h"

//#include "Csm20El2013.h"
#include "DStripPosition.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDStripPosition dialog


CDStripPosition::CDStripPosition(CWnd* pParent /*=NULL*/)
	: CDialog(CDStripPosition::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDStripPosition)
	m_EditSel = _T("");
	//  m_numStrip = 0;
	m_orderNumber = _T("");
	m_alloyType = _T("");
	m_coil = _T("");
	m_thickness = 0.0;
	//	m_cutSize = 0.0;
	// 	m_firstCutter = 0.0;
	m_width = 0.0;
	// modo singola testa
	m_mode = 2;
	// modo testa top
	//  m_useBottomHead = 0;
	m_coil2 = _T("");
	m_skipADef = FALSE;
	m_firstRising = _T("");
	m_freeNote = _T("");
	//}}AFX_DATA_INIT

	// c_offsetSx = 0.;
	c_hideMode = FALSE;
	c_fingerHwMode = TRUE;

	m_trimLeft = 0.0;
	m_trimRight = 0.0;
	m_stripWidth = 0.0;
	m_msg1 = _T("");
	m_numStrip = 0;
	// orientamento strip Right/left bool;
	c_rightAlignStrip = FALSE;

	m_labelTrimLeft = _T("");
	m_labelTrimRight = _T("");
}


void CDStripPosition::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDStripPosition)
	DDX_Control(pDX, IDOK, m_ctrlOk);
	//  DDX_Control(pDX, IDC_USE_TOP_HEAD, m_ctrlUseTop);
	//  DDX_Control(pDX, IDC_USE_BOTTOM_HEAD, m_ctrlUseBottom);
	//  DDX_Control(pDX, IDC_COIL2_LABEL, m_ctrlCoil2Label);
	//  DDX_Control(pDX, IDC_COIL2, m_ctrlCoil2);
	DDX_Control(pDX, IDC_COMBO_SEL_RICETTE, m_ctrlSelRicetta);
	DDX_CBString(pDX, IDC_COMBO_SEL_RICETTE, m_EditSel);
	DDX_Control(pDX, IDC_STRIP_LIST2, m_stripList2);
	DDX_Control(pDX, IDC_STRIP_LIST1, m_stripList1);
	//  DDX_Text(pDX, IDC_NUM_STRIP, m_numStrip);
	DDX_Text(pDX, IDC_ORDER_NUMBER, m_orderNumber);
	DDX_Text(pDX, IDC_ALLOY_TYPE, m_alloyType);
	DDX_Text(pDX, IDC_COIL, m_coil);
	DDX_Text(pDX, IDC_THICKNESS, m_thickness);
	// 	DDX_Text(pDX, IDC_CUT_SIZE, m_cutSize);
	//	DDX_Text(pDX, IDC_FIRST_CUTTER, m_firstCutter);
	DDX_Text(pDX, IDC_WIDTH, m_width);
	// DDX_Radio(pDX, IDC_MODE, m_mode);
	// DDX_Control(pDX, IDC_MODE, m_ctrlMode);
	// DDX_Control(pDX, IDC_MODE1, m_ctrlMode1);
	// DDX_Control(pDX, IDC_MODE2, m_ctrlMode2);
	//  DDX_Radio(pDX, IDC_USE_TOP_HEAD, m_useBottomHead);
	// DDX_Text(pDX, IDC_COIL2, m_coil2);
	DDX_Check(pDX, IDC_SKIP_A_DEF, m_skipADef);
	DDX_Text(pDX, IDC_FIRST_RISING, m_firstRising);
	DDX_Text(pDX, IDC_FREE_NOTE, m_freeNote);
	DDX_Control(pDX, IDC_RULER_STRIP, m_stripRuler);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_ONE_STRIP_SIZE, m_ctrlUpdateStripSize);
	DDX_Control(pDX, IDC_STRIP_WIDTH, m_ctrlStripWidth);
	DDX_Text(pDX, IDC_TRIM_LEFT, m_trimLeft);
	DDX_Text(pDX, IDC_TRIM_RIGHT, m_trimRight);
	DDX_Text(pDX, IDC_STRIP_WIDTH, m_stripWidth);
	DDX_Check(pDX, IDC_ONE_STRIP_SIZE, m_oneStripSize);
	DDX_Text(pDX, IDC_MSG, m_msg1);
	DDX_Text(pDX, IDC_NUM_STRIP, m_numStrip);
	DDV_MinMaxInt(pDX, m_numStrip, 1, 65);
	DDX_Text(pDX, IDC_LABEL_TRIM_LEFT, m_labelTrimLeft);
	DDX_Text(pDX, IDC_LABEL_TRIM_RIGHT, m_labelTrimRight);
}


BEGIN_MESSAGE_MAP(CDStripPosition, CDialog)
	//{{AFX_MSG_MAP(CDStripPosition)
	ON_BN_CLICKED(IDC_UPDATE1, OnUpdate1)
	ON_BN_CLICKED(IDC_UPDATE2, OnUpdate2)
	ON_EN_CHANGE(IDC_NUM_STRIP, OnChangeNumStrip)
	//ON_EN_CHANGE(IDC_FIRST_CUTTER, OnChangeFirstCutter)
	//ON_EN_CHANGE(IDC_CUT_SIZE, OnChangeCutSize)
	// ON_BN_CLICKED(IDC_MODE, OnMode)
	// ON_BN_CLICKED(IDC_MODE1, OnMode1)
	// ON_BN_CLICKED(IDC_MODE2, OnMode2)
	// ON_BN_CLICKED(IDC_USE_BOTTOM_HEAD, OnUseBottomHead)
	// ON_BN_CLICKED(IDC_USE_TOP_HEAD, OnUseTopHead)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDStripPosition message handlers

BOOL	CDStripPosition::saveToProfile(void)
{
CProfile profile;

profile.writeProfileString ("F1","Coil",m_coil);
// profile.writeProfileString ("F1","Coil2",m_coil2);
profile.writeProfileString ("F1","AlloyType",m_alloyType);
profile.writeProfileString ("F1","FreeNote",m_freeNote);
CString s;
//
s.Format("%lf",m_thickness);
profile.writeProfileString ("F1","Thickness",s);

//
profile.writeProfileString ("F1","OrderNumber",m_orderNumber);

//
s.Format("%lf",m_width);
profile.writeProfileString ("F1","Width",s);
//
//
s.Format("%d",m_numStrip);
profile.writeProfileString ("F1","NumStrip",s);
//
// ricetta default
m_EditSel.TrimRight();
profile.writeProfileString ("F1","RicettaSelected",m_EditSel);

profile.writeProfileString ("F1","FirstRising",m_firstRising);

if (this->m_skipADef)
	profile.writeProfileString ("F1","SkipADef","TRUE");
else
	profile.writeProfileString ("F1","SkipADef","FALSE");

// mode obbligatorio inserirlo
//s.Format("%d",m_mode);
//profile.writeProfileString ("F1","Mode",s);
//------------------------------------------------

// new variable
s.Format("%4.0lf",this->m_trimLeft);
profile.writeProfileString ("F1","TrimLeft",s);
s.Format("%4.0lf",this->m_trimRight);
profile.writeProfileString ("F1","TrimRight",s);
s.Format("%4.0lf",this->m_stripWidth);
profile.writeProfileString ("F1","StripWidth",s);
if(m_oneStripSize)
	profile.writeProfileString ("F1","ChangeStripSize","TRUE");
else
	profile.writeProfileString ("F1","ChangeStripSize","FALSE");


// stripSettings 
// s.Format("%lf",m_cutSize);
// profile.writeProfileString ("F1","CutSize",s);
//
// s.Format("%lf",m_firstCutter);
// profile.writeProfileString ("F1","FirstCutter",s);

// inserire save to profile di tutta la tabella
c_stripListHeader.TrimRight();
profile.writeProfileString ("F1","StripListHeader",c_stripListHeader);
c_stripListRow1.TrimRight();
profile.writeProfileString ("F1","StripListRow1",c_stripListRow1);
c_fingerListHeader.TrimRight();
profile.writeProfileString ("F1","FingerListHeader",c_fingerListHeader);
c_fingerListRow1.TrimRight();
profile.writeProfileString ("F1","FingerListRow1",c_fingerListRow1);

return TRUE;
}


BOOL	CDStripPosition::loadFromProfile(BOOL useFileInterface)
{
CProfile profile;

m_coil = profile.getProfileString ("F1","Coil","");
//m_coil2 = profile.getProfileString ("F1","Coil2","");
m_alloyType = profile.getProfileString ("F1","AlloyType","");
m_freeNote = profile.getProfileString ("F1","FreeNote","");

CString s;
s = profile.getProfileString ("F1","Thickness","10.0");
sscanf(s,"%lf",&m_thickness);

m_orderNumber = profile.getProfileString ("F1","OrderNumber","");

//
s = profile.getProfileString ("F1","Width","1400");
sscanf(s,"%lf",&m_width);
//
m_numStrip = profile.getProfileInt ("F1","NumStrip",5);

m_firstRising = profile.getProfileString ("F1","FirstRising","A");


m_skipADef = profile.getProfileBool ("F1","SkipADef",FALSE);


// mode obbligatorio inserirlo tutte le volte
//m_mode = profile.getProfileInt ("F1","Mode",0);

// ricetta default
m_EditSel = profile.getProfileString ("F1","RicettaSelected","DEFAULT");

// new variable
m_trimLeft = profile.getProfileInt ("F1","TrimLeft",0);
m_trimRight = profile.getProfileInt ("F1","TrimRight",0);
m_stripWidth = profile.getProfileInt ("F1","StripWidth",0);
m_oneStripSize = profile.getProfileBool ("F1","ChangeStripSize",FALSE);

// StripSettings specific
// s = profile.getProfileString ("F1","CutSize","5");
// sscanf(s,"%lf",&m_cutSize);
//
// s = profile.getProfileString ("F1","FirstCutter","0");
// sscanf(s,"%lf",&m_firstCutter);

// stripList load
// intestazioni passate su risorse x internazionalizzazione
c_stripListHeader = profile.getProfileString("F1","StripListHeader"," CUTTER    ");
c_stripListRow1   = profile.getProfileString("F1","StripListRow1","   Size   ");
c_fingerListHeader = profile.getProfileString("F1","FingerListHeader"," FINGER ");
c_fingerListRow1   = profile.getProfileString("F1","FingerListRow1"," Position ");

// provo a sovrascrivere con textFile
CString configFile;

configFile = profile.getProfileString("F1","FileInterface","");

if (useFileInterface)
	{
	if ((configFile != "")&&
		loadFromFile(configFile))
		{
		m_msg1 = "Loaded data from file ";
		m_msg1 += configFile;
		return TRUE;
		}
	else
		{
		m_msg1 = "Error: can't Load data from file ";
		m_msg1 += configFile;
		}
	}
else
	{// no error
	m_msg1 = "";	
	}

// altrimenti leggo fa profile come al solito
return TRUE;
}

BOOL	CDStripPosition::loadFromResource(void)
{
// c_stripListHeader = " CUTTER    ";
c_stripListHeaderInit.LoadString(CSM_STRIPLISTHEADER);
// c_stripListSize   = "   Size   ";
c_stripListSize.LoadString(CSM_STRIPLISTSIZE);
//c_fingerListHeader = " FINGER ";
c_fingerListHeaderInit.LoadString(CSM_FINGERLISTHEADER);
//c_fingerListRow1 = " Position ";
c_fingerListPosition.LoadString(CSM_FINGERLISTPOSITION);
//c_fingerListHeader = " Finger %d";
c_fingerListHeaderNumber.LoadString(CSM_FINGERLISTHEADERNUMBER);
// c_stripListHeaderNumber = " Strip %d";
c_stripListHeaderNumber.LoadString(CSM_STRIPLISTHEADERNUMBER);

return 1;
}

// il max teorico e` 
#define TOTAL_WIDTH 800.
#define REAL_FACTOR 10

BOOL CDStripPosition::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	// Fill sel ricetta
	// TODO: Add extra initialization here
	DBRicette dbRicette(dBase);
	
	dbRicette.openSelectDefault();

	// TODO: Add extra initialization here
	// dBase already Open
	// m_EditSel caricata da loadFromProfile 
	while(!dbRicette.IsEOF())
		{
		 m_ctrlSelRicetta.AddString(dbRicette.m_NOME);
		 dbRicette.MoveNext();
		}
	dbRicette.Close();
	UpdateData(FALSE);

// disabilitato modo alternato
if (c_fingerHwMode)
	{
	// m_ctrlMode1.EnableWindow(FALSE);
	}


	// impostare larghezza rotolo in cm
	// 140 * 10 
	CRect r;
	m_stripRuler.GetClientRect(&r); 
	// non includo dist bordo sx
	//m_stripRuler.SetMargin( (TOTAL_WIDTH+c_offsetSx)/10 );
	m_stripRuler.SetMargin( TOTAL_WIDTH/REAL_FACTOR);
	// m_stripRuler.SetPosColor2((long)m_firstCutter-c_offsetSx);
	m_stripRuler.SetRealFactor(REAL_FACTOR);
	m_stripRuler.SetBackGroundColor( RGB( 200 , 200 , 255 ) );
	m_stripRuler.SetSeperatorSize( 4 );
	//m_stripRuler.SetMilimeterPixel( r.Size().cx/((TOTAL_WIDTH+c_offsetSx)/10) );
	m_stripRuler.SetMilimeterPixel( r.Size().cx/(TOTAL_WIDTH/REAL_FACTOR) );
	// center in window
	long v = ((r.Size().cx - m_stripRuler.GetMargin()*m_stripRuler.GetMilimeterPixel())/2);
	m_stripRuler.SetScrollPos(- v);
	
//	m_stripRuler.AddSeperator( 0   , 1 );
//	m_stripRuler.AddSeperator( 20  , 2 );
//	m_stripRuler.AddSeperator( 35  , 3 , SEPTYPE_NOPOSCHANGE );
//	m_stripRuler.AddSeperator( 56  , 4 , 0 , NULL , RGB( 0 , 0 , 0 ) , RGB( 255 , 0 , 0 ) , 20 , 56 );
//	m_stripRuler.AddSeperator( 130 , 5 );

	m_stripRuler.SetStyle( RWSTYLE_HORZ | RWSTYLE_BOTTOMALIGN );
	

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();
// Strip List 
// add first column
CString str,subStr;
str = c_stripListHeader;
int i = 0;

m_stripList1.setFont(_T("MS Sans Serif"), 70);

while((subStr = str.SpanExcluding(",")) != "")
	{
	if (i == 0)
		m_stripList1.AddColumn(subStr,i,IDC_NOEDITCELL);
	else
		m_stripList1.AddColumn(subStr,i,IDC_NUMBERCELL);
	str = str.Right(str.GetLength()-(subStr.GetLength()+1));
	i++;
	}

// add First row
i = 0;
str = c_stripListRow1;

while((subStr = str.SpanExcluding(",")) != "")
	{
	if (i == 0)
		m_stripList1.AddRow(subStr);
	else
		{
		double v;
		sscanf(subStr,"%lf",&v);
		// 01-2014 aggiunto format 
		m_stripList1.AddItem(0,i,v,_T("%4.1lf"));
		}
	str = str.Right(str.GetLength()-(subStr.GetLength()+1));
	i++;
	}
m_stripList1.setReflectMode(TRUE);
// resize column
m_stripList1.AdjustColumnWidths();


// Finger List 
// add first column
if (m_stripList2.IsWindowVisible())
	{
	CString str,subStr;
	str = c_fingerListHeader;
	int i = 0;

	while((subStr = str.SpanExcluding(",")) != "")
		{
		if (i == 0)
			m_stripList2.AddColumn(subStr,i,IDC_NOEDITCELL);
		else
			m_stripList2.AddColumn(subStr,i,IDC_NUMBERCELL);
		str = str.Right(str.GetLength()-(subStr.GetLength()+1));
		i++;
		}

	// add First row
	i = 0;
	str = c_fingerListRow1;

	while((subStr = str.SpanExcluding(",")) != "")
		{
		if (i == 0)
			m_stripList2.AddRow(subStr);
		else
			{
			double v;
			sscanf(subStr,"%lf",&v);
			// 01-2014 aggiunto format 
			m_stripList2.AddItem(0,i,v,_T("%4.1lf"));
			}
		str = str.Right(str.GetLength()-(subStr.GetLength()+1));
		i++;
		}
	m_stripList2.setReflectMode(TRUE);
	// resize column
	m_stripList2.AdjustColumnWidths();
	UpdateRulerFromList(); 
	}


// ----------------------

if (c_hideMode)
	{// offline mode
//	m_ctrlMode.ShowWindow(SW_HIDE);
//	m_ctrlMode1.ShowWindow(SW_HIDE);
//	m_ctrlMode2.ShowWindow(SW_HIDE);
	}
else
	{// start F1 -> disable ok 
	m_ctrlOk.EnableWindow(FALSE);
	// Hide coil2 -> only if separator
//	m_ctrlCoil2.ShowWindow(SW_HIDE);
//	m_ctrlCoil2Label.ShowWindow(SW_HIDE);
	}
	
//m_ctrlUseTop.ShowWindow(SW_HIDE);
//m_ctrlUseBottom.ShowWindow(SW_HIDE);

if (c_rightAlignStrip)
	{
	m_labelTrimLeft = "Trimming Right";
	m_labelTrimRight = "Trimming Left";
	}
else
	{
	m_labelTrimLeft = "Trimming Left";
	m_labelTrimRight = "Trimming Right";
	}


UpdateData(FALSE);

return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDStripPosition::UpdateRulerFromList() 
{
CHeaderCtrl* pCtrl = m_stripList2.GetHeaderCtrl();
ASSERT(pCtrl != NULL);

// clear sep
m_stripRuler.DeleteAllSeperator();
CRect r;
m_stripRuler.GetClientRect(&r); 
//

// era /10
m_stripRuler.SetMargin(TOTAL_WIDTH / REAL_FACTOR);
//m_stripRuler.SetMargin((TOTAL_WIDTH+c_offsetSx)/10 );
// m_stripRuler.SetPosColor2((long)m_firstCutter-c_offsetSx);
// m_stripRuler.SetPosColor2((long)c_offsetSx);
// era 10
m_stripRuler.SetRealFactor(REAL_FACTOR);
m_stripRuler.SetBackGroundColor( RGB( 200 , 200 , 255 ) );
m_stripRuler.SetSeperatorSize( 4 );
// m_stripRuler.SetMilimeterPixel( r.Size().cx/((TOTAL_WIDTH+c_offsetSx)/10) );
m_stripRuler.SetMilimeterPixel( r.Size().cx/(TOTAL_WIDTH/REAL_FACTOR) );
// center in window
long v = ((r.Size().cx - m_stripRuler.GetMargin()*m_stripRuler.GetMilimeterPixel())/2);
m_stripRuler.SetScrollPos(- v);

int numHItem = pCtrl->GetItemCount();
for (int i=1;i<numHItem;i++)
	{
	double pos = m_stripList2.getItemDouble(0,i);
	pos /= m_stripRuler.GetRealFactor();

//	if (i == 1)
		{// Primo
		m_stripRuler.AddSeperator(pos, i-1, 
			SEPTYPE_NOPOSAFTER,NULL , 
					RGB( 0 , 0 , 0 ) , RGB( 255 , 0, 0));
		}
/*
		else
		if (i == (numHItem - 1))
			{// Ultimo
			if (i%2 == 0)
				m_stripRuler.AddSeperator(v/m_stripRuler.GetRealFactor(), i-1 , 
					SEPTYPE_NOPOSBEFORE , NULL , 
					RGB( 0 , 0 , 0 ) , RGB( 255 , 0, 0 )  );
			else
				m_stripRuler.AddSeperator(v/m_stripRuler.GetRealFactor() , i-1, 
						SEPTYPE_NOPOSBEFORE );
			}
		else
			{// Intermedi
			if (i%2 == 0)
				m_stripRuler.AddSeperator(v/m_stripRuler.GetRealFactor(), i-1 , 
					SEPTYPE_NOPOSBEFORE | SEPTYPE_NOPOSAFTER , NULL , 
					RGB( 0 , 0 , 0 ) , RGB( 255 , 0, 0 )  );
			else
				m_stripRuler.AddSeperator(v/m_stripRuler.GetRealFactor() , i-1, 
						SEPTYPE_NOPOSBEFORE | SEPTYPE_NOPOSAFTER);
			}
*/

	}



}


// Update 1 prepara i campi size precompilati solo per cio` che riguarda 
// fingerSize
void CDStripPosition::OnUpdate1() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

if (m_numStrip > 0)
	{
	// Set Ruler param
	CRect r;

	if (m_ctrlUpdateStripSize.GetCheck())
		{// vechio update
		// clear strip list
		
		if (m_stripRuler.IsWindowVisible())
			m_stripRuler.DeleteAllSeperator();
		if (m_stripList2.IsWindowVisible())
			{
			m_stripList2.DeleteAllItems();
			m_stripList2.removeHeader();
			}

		m_stripList1.DeleteAllItems();
		m_stripList1.removeHeader();
		// add first column
		// m_stripList1.AddColumn("    CUTTER    ",0,IDC_NOEDITCELL);
		m_stripList1.AddColumn(c_stripListHeaderInit,0,IDC_NOEDITCELL);
		// add First row
		// m_stripList1.AddRow("   Size   ");
		m_stripList1.AddRow(c_stripListSize);
		//
		double stripSz;
		double fingerSz=0;

		// dimensione separatore 

		stripSz = this->m_stripWidth;
	
	//	fingerSz = this->m_cutSize;
	
		// aggiungi primo coltello a listCtrl
		// m_stripList1.AddColumn(" Strip 1",1,IDC_NUMBERCELL);
		CString s;
		//s.Format(c_stripListHeaderNumber,1);
		//m_stripList1.AddColumn(s,1,IDC_NUMBERCELL);
		// 01-2014 aggiunto format 	
		// m_stripList1.AddItem(0,1,stripSz,_T("%4.1lf"));

		// coltelli intermedi
		for (int i=0;i<m_numStrip;i++)
			{
			CString s;
			// aggiungi finger a listCtrl
			// s.Format(" Finger %d ",i+1);
			// s.Format(c_fingerListHeaderNumber,i+1);
			// m_stripList1.AddColumn(s,2*i+2,IDC_NUMBERCELL);
			// m_stripList1.AddItem(0,2*i+2,fingerSz);

			// aggiungi strip a listCtrl
			// s.Format(" Strip %d ",i+2);
			// if (c_rightAlignStrip)
			if (0)
				s.Format(c_stripListHeaderNumber,(m_numStrip - i));
			else
				s.Format(c_stripListHeaderNumber,i+1);

			// era 2*i+3 ora senza finger i+2
			m_stripList1.AddColumn(s,i+1,IDC_NUMBERCELL);
			// 01-2014 aggiunto format 
			m_stripList1.AddItem(0,i+1,stripSz,_T("%4.1lf"));
			}
	
		// resize column
		m_stripList1.AdjustColumnWidths();
		//
		}
	else
		{// aggiorniamo solo numero di strip 
		 // tre casi == < o > di quelli esistenti
		if (m_numStrip > m_stripList1.getNumCol()-1)
			{// aggiungo strip
			for (int i=m_stripList1.getNumCol()-1;i<m_numStrip;i++)
				{
				CString s;
				// aggiungi strip a listCtrl
				// s.Format(" Strip %d ",i+2);
			//f (c_rightAlignStrip)
				if (0)
					{
					s.Format(c_stripListHeaderNumber, i + 1);
					int idx = (i-m_stripList1.getNumCol()+2);
					m_stripList1.AddColumn(s,idx,IDC_NUMBERCELL);
					// 01-2014 aggiunto format 
					double stripSz = this->m_stripWidth;
					m_stripList1.AddItem(0,idx,stripSz,_T("%4.1lf"));
					}
				else
					{
					s.Format(c_stripListHeaderNumber,i+1);
					// era 2*i+3 ora senza finger i+2
					m_stripList1.AddColumn(s,i+1,IDC_NUMBERCELL);
					// 01-2014 aggiunto format 
					double stripSz = this->m_stripWidth;
					m_stripList1.AddItem(0,i+1,stripSz,_T("%4.1lf"));
					}
				}
			}
		else
			{
			if (m_numStrip < m_stripList1.getNumCol())
				{// tolgo strip
				for (int i=m_stripList1.getNumCol()-1;i>m_numStrip;i--)
					{
					// rimuovi strip a listCtrl
					m_stripList1.DeleteColumn(i);
					}
				}
			// altrimenti e` uguale e non faccio nulla
			}
		}
	// resize column
	m_stripList1.AdjustColumnWidths();
	}
UpdateData(FALSE);
Invalidate();		
}

void CDStripPosition::OnUpdate2() 
{
	// TODO: Add your control notification handler code here
UpdateData(TRUE);

m_stripRuler.DeleteAllSeperator();
if (m_numStrip > 0)
	{
	// Set Ruler param
	CRect r;

	// clear strip list
	m_stripList2.DeleteAllItems();
	m_stripList2.removeHeader();
	// add first column
	// m_stripList2.AddColumn("    FINGER    ",0,IDC_NOEDITCELL);
	m_stripList2.AddColumn(c_fingerListHeaderInit,0,IDC_NOEDITCELL);
	// add First row
	// m_stripList2.AddRow(" Position ");
	m_stripList2.AddRow(c_fingerListPosition);
	//
	
	// primo coltello
	double shutterSize = 0.; // overlapp?
	// primo coltello
	// double fingerPos = this->m_firstCutter - c_offsetSx + m_cutSize/2;
	// double fingerPos = m_firstCutter - c_offsetSx + 5 - 8.5;
	double fingerPos = m_trimLeft;

	CHeaderCtrl* pCtrl = m_stripList1.GetHeaderCtrl();
	ASSERT(pCtrl != NULL);
	int numField = pCtrl->GetItemCount();
	// for (int i=0;i<numField-2;i++)
	//Ora anche diaframmi sx e dx
	for (int i=0;i<numField;i++)
		{
		CString s;
		double stripSz;
		if (i==0)
			{// shutter OK
			s.Format(c_fingerListHeaderNumber,i/2+1);
			m_stripList2.AddColumn("Shutter",i/2+1,IDC_NUMBERCELL);
			m_stripList2.AddItem(0,i/2+1,fingerPos);
			// tolgo shutterSize solo prima volta e preparo pos successiva
			//fingerPos -= 5;
			//fingerPos += 8.5;
			
			// fingerPos += stripSz;
			// fingerPos += m_cutSize/2.;
			//fingerPos -= 25.0/2.;
			}
		else
			{
			if (i==(numField-1))
				{// ultimo shutter
				stripSz    = m_stripList1.getItemDouble(0,i);
				fingerPos += stripSz;
				// fingerPos -= m_cutSize/2.;
				//fingerPos += 25.0/2.;
				//fingerPos -= 5;
				//fingerPos += 8.5;
				//
				// era i/2+12
				s.Format(c_fingerListHeaderNumber,i+1);
				// era i/2+2
				m_stripList2.AddColumn("Shutter  ",i+1,IDC_NUMBERCELL);
				// era i/2+2
				m_stripList2.AddItem(0,i+1,fingerPos);
				}
			else
				{// coltelli
				stripSz = m_stripList1.getItemDouble(0,i+1);
				// if (i%2 == 0)
				if (i > 0)	
					{
					// aggiungi finger
					// s.Format(" Finger %d ",i/2+1);
					stripSz = m_stripList1.getItemDouble(0,i+1);
					fingerPos += stripSz;
					// era i/2
					s.Format(c_fingerListHeaderNumber,i);
					// era i/2+1
					m_stripList2.AddColumn(s,i+1,IDC_NUMBERCELL);
					double realFingerPos;
					realFingerPos = fingerPos;
					// era i/2+1
					m_stripList2.AddItem(0,i+1,realFingerPos);
					
					}
				else
					{// dispari => strip da non vis 
					stripSz = m_stripList1.getItemDouble(0,i);
					fingerPos += stripSz;
					}

				}
			}
		}
	// resize column
	m_stripList1.AdjustColumnWidths();
	// Update Ruler
	UpdateRulerFromList();
	//
	UpdateData(FALSE);
	}
Invalidate();	
}




// notify: arriva msg da ruler
BOOL CDStripPosition::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{

// Utilizzato per abilitare/disabilitare pulsanti
BOOL globalMode;

globalMode = 1;

// globalMode = (m_mode == 0) || (m_mode == 1) || ((m_mode == 2)&&(m_useBottomHead >= 0));

// OnOk

if (m_stripList2.IsWindowVisible())
	{
	if((m_stripList2.getNumCol() > 0)&&
	((c_hideMode)||(globalMode)))
		m_ctrlOk.EnableWindow(TRUE);
	else
		m_ctrlOk.EnableWindow(FALSE);
	}
else
	m_ctrlOk.EnableWindow(TRUE);
		
	
// TODO: Add your specialized code here and/or call the base class
/*
#define  NM_RULER_NOTIFICATIONMESSAGE   0x1112
#define  NMSUB_RULER_SEPERATORCHANGE    0x0001
#define  NMSUB_RULER_SEPERATORCHANGING  0x0002
*/
return CDialog::OnNotify(wParam, lParam, pResult);

// non gestiti update automatici da ruler a tabella

gxListCtrl *pListCtrl = (gxListCtrl *)GetDlgItem( IDC_STRIP_LIST2 );
if( pListCtrl == NULL || !IsWindow( pListCtrl->m_hWnd ) )
   return FALSE;

CString str;
CString strType;

CRulerWnd::RULERWNDNOTIFY_INFO *pRulerNM = (CRulerWnd::RULERWNDNOTIFY_INFO *)lParam;
if( pRulerNM != NULL && pRulerNM->hdr.code == NM_RULER_NOTIFICATIONMESSAGE ) 
	{
	int id = pRulerNM->iSepID;

	switch( pRulerNM->nSubMessage ) 
		{
		case NMSUB_RULER_SEPERATORCHANGE :
        case NMSUB_RULER_SEPERATORCHANGING :
			CString s;
			s.Format("%d",pRulerNM->iNewPos);
			m_stripList1.SetItemText(0,id+1,s);
			break;
		}; //switch

	}
else
	{
	LV_DISPINFO* dispInfo;
	dispInfo =(LV_DISPINFO*) lParam;

	if ((dispInfo->hdr.code == LVN_ENDLABELEDIT)&&
		(dispInfo->item.iSubItem > 0))
		{// ok
		long v;
		sscanf((LPCSTR)dispInfo->item.pszText,"%d",&v);
		if (!m_stripRuler.SetRealPos( dispInfo->item.iSubItem-1,v))
			{
			// invalid position!!
			Beep(300,400);
			}
		m_stripRuler.Invalidate();
		}
	}
		
		
return CDialog::OnNotify(wParam, lParam, pResult);
}



void CDStripPosition::OnOK() 
{
// TODO: Add extra validation here
UpdateData(TRUE);
TCHAR tc;

tc = m_firstRising.GetAt(0);

if (_istalpha(tc) == 0)
	{
	AfxMessageBox(_T("Error: On Field First Rising only capital letters are allowed!"),MB_ICONERROR );
	return;
	}

// inserire save to profile di tutta la tabella
CHeaderCtrl* pCtrl = m_stripList1.GetHeaderCtrl();
ASSERT(pCtrl != NULL);
int numHItem = pCtrl->GetItemCount();
CString separator;
c_stripListHeader = "";
separator = ",";
HDITEM hdi;
TCHAR  lpBuffer[256];
hdi.mask = HDI_TEXT;
hdi.pszText = lpBuffer;
hdi.cchTextMax = 256;
for (int i=0;i<numHItem;i++)
	{
	pCtrl->GetItem(i, &hdi);   
	c_stripListHeader += hdi.pszText;
	if (i < (numHItem-1))
		c_stripListHeader += separator;
	}

// Campi tabella => una sola riga
c_stripListRow1 = "";
int i = 0;
for (int j=0;j<numHItem;j++)
	{
	c_stripListRow1 += m_stripList1.GetItemText(i,j);
	if (i < (numHItem-1))
		c_stripListRow1 += separator;
	}

if (m_stripList2.IsWindowVisible())
	{
	// finger list 2
	pCtrl = m_stripList2.GetHeaderCtrl();
	ASSERT(pCtrl != NULL);
	numHItem = pCtrl->GetItemCount();
	c_fingerListHeader = "";
	separator = ",";


	hdi.mask = HDI_TEXT;
	hdi.pszText = lpBuffer;
	hdi.cchTextMax = 256;


	for (i=0;i<numHItem;i++)
		{
		pCtrl->GetItem(i, &hdi);   
		c_fingerListHeader += hdi.pszText;
		if (i < (numHItem-1))
			c_fingerListHeader += separator;
		}

	// Campi tabella => una sola riga
	c_fingerListRow1 = "";
	i = 0;
	for (int j=0;j<numHItem;j++)
		{
		if (j == 0)
			{
			c_fingerListRow1 += m_stripList2.GetItemText(i,j);
			}
		else
			{
			double v;
			CString s; 
			v = m_stripList2.getItemDouble(i,j);
			// aggiungo 25-(fingerSize/2)
			// v += (25 - m_cutSize/2);
			// 23-02-05 eliminato spostamento a sx di 25 mm 
			// valore indicato e` direttamente asse centrale del finger

			s.Format("%4.1lf",v);
			c_fingerListRow1 += s;
			}
		if (i < (numHItem-1))
			c_fingerListRow1 += separator;
		}
	}
// fine finger list

CDialog::OnOK();
}


// disabilita OK se num strip cambia
void CDStripPosition::OnChangeNumStrip() 
{
m_ctrlOk.EnableWindow(FALSE);
}
// come sopra
void CDStripPosition::OnChangeFirstCutter() 
{
m_ctrlOk.EnableWindow(FALSE);
}
// come sopra
void CDStripPosition::OnChangeCutSize() 
{
m_ctrlOk.EnableWindow(FALSE);
}



void CDStripPosition::OnMode() 
{
// TODO: Add your control notification handler code here
if(m_stripList2.getNumCol() > 1)
	m_ctrlOk.EnableWindow(TRUE);
else
	m_ctrlOk.EnableWindow(FALSE);

// Show coil2 -> only if separator
// m_ctrlCoil2.ShowWindow(SW_SHOW);
// m_ctrlCoil2Label.ShowWindow(SW_SHOW);
// m_ctrlUseTop.ShowWindow(SW_HIDE);
// m_ctrlUseBottom.ShowWindow(SW_HIDE);
	
}

void CDStripPosition::OnMode1() 
{
// TODO: Add your control notification handler code here
if(m_stripList2.getNumCol() > 1)
	m_ctrlOk.EnableWindow(TRUE);
else
	m_ctrlOk.EnableWindow(FALSE);
// Hide coil2 -> only if separator
// m_ctrlCoil2.ShowWindow(SW_HIDE);
// m_ctrlCoil2Label.ShowWindow(SW_HIDE);
// m_ctrlUseTop.ShowWindow(SW_HIDE);
// m_ctrlUseBottom.ShowWindow(SW_HIDE);
	
}

void CDStripPosition::OnMode2() 
{
// TODO: Add your control notification handler code here

//if((m_stripList2.getNumCol() > 1)&&
//	(m_useBottomHead >= 0))
//   m_ctrlOk.EnableWindow(TRUE);
// else
	m_ctrlOk.EnableWindow(FALSE);
// Hide coil2 -> only if separator
// m_ctrlCoil2.ShowWindow(SW_HIDE);
// m_ctrlCoil2Label.ShowWindow(SW_HIDE);
// m_ctrlUseTop.ShowWindow(SW_SHOW);
// m_ctrlUseBottom.ShowWindow(SW_SHOW);
	
}

void CDStripPosition::OnUseBottomHead() 
{
// TODO: Add your control notification handler code here
// m_useBottomHead = 0;	
if(m_stripList2.getNumCol() > 1)
   m_ctrlOk.EnableWindow(TRUE);

}

void CDStripPosition::OnUseTopHead() 
{
	// TODO: Add your control notification handler code here
// m_useBottomHead = 1;	
if(m_stripList2.getNumCol() > 1)
   m_ctrlOk.EnableWindow(TRUE);
}

BOOL CDStripPosition::fillStripSizeArray(CArray <int,int> &stripSizeArray)
{
CString s,s1;

s = c_stripListRow1;
int pos = 0;
int cnt = 0;
while ((pos=s.Find(',',0))>=0)
	{
	s1 = s.Left(pos);
	s = s.Right(s.GetLength()-(pos+1));
	// reverse header
	
	if (cnt > 0)
		{
		if (c_rightAlignStrip)
			stripSizeArray.InsertAt(0,atoi(s1));
		else
			stripSizeArray.Add(atoi(s1));
		}
	cnt++;
	}
return TRUE;
}

// carica valori F1 da file, 
// formato : coilname,ordernumber,thickness,alloyType, trimLeft, trimRight, strip1,strip2, ... , etc
BOOL CDStripPosition::loadFromFile(CString fileName)
{
CFile f;
CString stripListHeader;
CString stripListRow1;
CString fingerListHeader;
CString fingerListRow1;
stripListHeader.LoadString(CSM_STRIPLISTHEADER);
stripListRow1.LoadString(CSM_STRIPLISTSIZE);
fingerListHeader.LoadString(CSM_FINGERLISTHEADER);
fingerListRow1.LoadString(CSM_FINGERLISTPOSITION);

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

// primo shutter a zero
fingerListHeader += "Shutter";
fingerListRow1   += "0.0";
int cnt = 0;

if (f.Open(fileName,CFile::modeRead,NULL,NULL))
	{// ok aperto
	// leggo fino in fondo al file
	TCHAR buffer [2048];
	int nch = f.Read(buffer,2048);
	if (nch > 0)
		{// qualche cosa da leggere
		buffer[nch] = '\0';
		CString s (buffer);
		CString s1;
		int pos = 0;
		double fpos = 0.;
		while ((pos=s.Find(',',0))>=0)
			{
			s1 = s.Left(pos);
			s = s.Right(s.GetLength()-(pos+1));
			switch(cnt)
				{
				// coilname
				case 0:	m_coil = s1; break;
				// orderNumber
				case 1:	m_orderNumber = s1; break;
				// thickness
				case 2:	m_thickness = _ttof(s1); break;
				// alloytype
				case 3:	m_alloyType = s1; break;
				// trimLeft
				case 4:	m_trimLeft = _ttof(s1); break;
				// trimRight
				case 5:	m_trimRight = _ttof(s1); break;
				// da qui in poi strip width
				default:
					{// ok clear strip 
					stripListHeader += ",";
					CString s2,s3;
					s2.LoadString(CSM_STRIPLISTHEADERNUMBER);
					s3.Format(s2,cnt-5);
					stripListHeader += s3;
					stripListRow1 += ",";
					stripListRow1 += s1;
					// ok finger
					double dv = atof((LPCSTR)s1);
					fingerListHeader += ",";
					s2.LoadString(CSM_FINGERLISTHEADERNUMBER);
					s3.Format(s2,cnt-5);
					fingerListHeader += s3;
					fingerListRow1 += ",";
					// scrivo posizione finger
					// aumento con larghezza strip attuale
					fpos += dv;
					s2.Format("%3.01lf",fpos);
					fingerListRow1 += s2;					
					}
					break;
				}
			cnt++;
			}		
		c_stripListHeader = stripListHeader;
		// torno indietro di 1 sui finger
		c_stripListRow1 = stripListRow1;
		// virgola compresa!
		int lastComma = fingerListHeader.ReverseFind(',');
		fingerListHeader = fingerListHeader.Left(lastComma+1);
		fingerListHeader += "Shutter";
		// scrivo posizione finger
		//CString s2;
		//s2.Format(",%3.01lf",fpos);
		//fingerListRow1 += s2;
		c_fingerListHeader = fingerListHeader;
		c_fingerListRow1 = fingerListRow1;
		// m_numStrip = cnt - 5;
		m_numStrip = cnt - 6;		// secondo me e` -6 c'era un baco boh! 15-3-16
		}
	// reverse header
	/*
	if (c_rightAlignStrip)
		{// devo farlo in fondo xche altrimenti non so quanti strip ci sono!
		stripListHeader.LoadString(CSM_STRIPLISTHEADER);
		CString s2,s3;
		s2.LoadString(CSM_STRIPLISTHEADERNUMBER);
		for (int i=m_numStrip;i>0;i--)
			{
			stripListHeader += ",";
			s3.Format(s2,i);
			stripListHeader += s3;
			}
		c_stripListHeader = stripListHeader;
		// reverse strip from left to right
		int commaPos;
		stripListRow1.LoadString(CSM_STRIPLISTSIZE);
		while((commaPos = c_stripListRow1.ReverseFind(','))>=0)
			{
			stripListRow1 += c_stripListRow1.Right(c_stripListRow1.GetLength()-commaPos);
			c_stripListRow1 = c_stripListRow1.Left(commaPos);
			}
		c_stripListRow1 = stripListRow1;
		// reverse fingerlist
		fingerListRow1.LoadString(CSM_FINGERLISTPOSITION);
		while((commaPos = c_fingerListRow1.ReverseFind(','))>=0)
			{
			fingerListRow1 += c_fingerListRow1.Right(c_fingerListRow1.GetLength()-commaPos);
			c_fingerListRow1 = c_fingerListRow1.Left(commaPos);
			}
		c_fingerListRow1 = fingerListRow1;
	
		}
	*/
	return TRUE;
	}

return FALSE;
}
