//-----------------------------------------------------------
//
//		CMapCoil
//
//------------------

class CMapCoil : public CMap <int,int,DifMetro,DifMetro &>
{
double meter;			// posizione attuale
// double trueSizeEncod;	// Dimensione corretta tra due interrupt
double largeSize;		// larghezza singole bande mm
int numeroClassi;
int numeroSchede;
int baseStepX;			// in mm

CArray <QuattroClassi,QuattroClassi &> c_totali;

public:
CArray <double,double &> c_sogliaAllarme;

public:

	int GetSize(void){int n = GetCount();return(n);};

int Add (DifMetro &dMetro)
	{
	for (int i=0;i<numeroSchede;i++)
		{
		if (numeroClassi > 0)
			c_totali[i].totalA += dMetro.getValNum(i,'A');
		if (numeroClassi > 1)
			c_totali[i].totalB += dMetro.getValNum(i,'B');
		if (numeroClassi > 2)
			c_totali[i].totalC += dMetro.getValNum(i,'C');
		if (numeroClassi > 3)
			c_totali[i].totalD += dMetro.getValNum(i,'D');
		}
	CMap<int,int,DifMetro,DifMetro &>::SetAt(dMetro.posizione,dMetro);
	return (0);
	};
int getMeterBaseStepX(void) {return(baseStepX/1000);};
void setMmBaseStepX(int millimetri) {baseStepX = millimetri;};

CMapCoil(void )
	{meter = 0.;// Init Schede
	largeSize = 1500.; // Default 1.5 mt
	numeroClassi = 4;
	// m_nGrowBy = 1000;	// cresce di 1000 metri per volta
	baseStepX = 1000;	// step ogni metro default
	};
~CMapCoil (void)
		{
		/*
		POSITION pos;
		DifMetro pDM;
		int key;
		pos = GetStartPosition();
		while (pos != NULL)
			{
			GetNextAssoc(pos,key,pDM);
			
			pDM->RemoveAll();
			}
		*/
		RemoveAll();
		c_sogliaAllarme.RemoveAll();
		c_totali.RemoveAll();
		};

// Copy operator
CMapCoil& operator = (CMapCoil& source)
{
clear();
meter = source.meter;			// posizione attuale
largeSize = source.largeSize;		// larghezza singole bande mm
numeroClassi = source.numeroClassi;
numeroSchede = source.numeroSchede;
baseStepX = source.baseStepX;
c_totali.RemoveAll();
RemoveAll();

for (int i=0;i<source.c_totali.GetSize();i++)
	{
	c_totali.Add (source.c_totali.ElementAt(i));
	}

POSITION pos;
DifMetro pDM;
int key;
pos = source.GetStartPosition();
while (pos != NULL)
	{
	source.GetNextAssoc(pos,key,pDM);
	SetAt(pDM.posizione,pDM);
	}
return (*this);
};


void clear(int numSchede,int nClassi){
			numeroClassi = nClassi;
			numeroSchede = numSchede;
			clear();};
void clear(void )
			{
			RemoveAll();
			meter = 0.;
			c_totali.RemoveAll();
			c_totali.SetSize(numeroSchede);
			};
// Implementazione delle interfacce di GroupSchede 
public:

double getMeter (){return(meter);};
double getAllarme (int code){if ((code - 'A') < c_sogliaAllarme.GetSize()) return(c_sogliaAllarme[code-'A']);
			return (0.);};
int getNumSchede (void){return(numeroSchede);};
int getNumClassi (void){return(numeroClassi);};


void setMeter (double m){meter = m;};
void setLargeSize (int mm){largeSize = (double ) mm;};
//void setTrueSizeEncod (double dmm){trueSizeEncod = dmm;};

double getIncMeter (void){return(1.);};

// torna numero difetti della classe code
// disp metri dalla posizione attuale 
double getValAlarmNumLStep(int code,int disp = 0)
	{double v = getValNumAt(code,(int) (meter-disp));
	if (v >= getAllarme(code))
		return (1.); // era v
	return (0.);};

double getValNumLStep(int code,int disp = 0)
	{
	return(getValNumAt(code,(int) (meter-disp)));};

	

// somma valori di tutte le schede per quel codice
double getValNumAt(int code,int pos)
	{
	double val=0.;
	DifMetro pDM;
	if (Lookup(pos,pDM))
		{
		val = pDM.getValNum(code);
		}
	return(val);};

	
// Somma tutti i difetti di code fra metro from e metro To
// appartenenti a scheda
double getAllDifetti(int scheda,int code)
{
double val=0.;
// Nessun elemnto nella mappa
if (GetCount() == 0)
	return (val);
if (scheda < c_totali.GetSize())
	{
	switch(code)
		{
		case 'A' : val = (double)c_totali[scheda].totalA;
				break;
		case 'B' : val = (double)c_totali[scheda].totalB;
				break;
		case 'C' : val = (double)c_totali[scheda].totalC;
				break;
		case 'D' : val = (double)c_totali[scheda].totalD;
				break;
		}
	}

return (val);
}


// Somma tutti i difetti di code fra metro from e metro To
// appartenenti a scheda
double getTotDifetti(int scheda,int code,int from,int to)
{
double val=0.;
// Nessun elemento nell'array
if (GetCount() == 0)
	return (val);
DifMetro dM;
for (int key=from;key <= to;key++)
	{
	if (Lookup(key,dM))
		{
		val += dM.getValNum(scheda,code);
		}
	}
return (val);
}

// Somma tutti i difetti di code fra metro from e metro To
// di qualunque scheda
double getTotDifetti(int code,int from,int to)
{
double val=0.;
// Nessun elemnto nell'array
if (GetCount() == 0)
	return (val);
DifMetro dM;
for (int key=from;key <= to;key++)
	{
	if (Lookup(key,dM))
		{
		val += dM.getValNum(code);
		}
	}
return (val);
}

// Somma tutti i difetti presenti nel metro index
/*
double getTotDifetti(int index)
{
double val=0.;
// Nessun elemento nell'array
if (GetSize() == 0)
	return (val);

for (int k=0;k<ElementAt(index).GetSize();k++)
	val += ElementAt(index).getValNum('A'+k);
return (val);
}

*/

// Somma tutti i difetti fra metro from e metro To
// di qualunque scheda e tutti i code
double getTotDifetti(int from,int to)
{
double val=0.;
// Nessun elemnto nell'array
if (GetCount() == 0)
	return (val);
DifMetro dM;
for (int key=from;key <= to;key++)
	{
	if (Lookup(key,dM))
		{
		for (int j=0;j<getNumClassi();j++)
			val += dM.getValNum('A'+j);
		}
	}
return (val);
}

// valore massimo per qualunque scheda
double getMaxDensFromTo(int from ,int to)
	{
	double Mval=0.;
	// Nessun elemnto nell'array
	if (GetCount() == 0)
		return (Mval);
	DifMetro dM;
	for (int k=0;k<getNumSchede();k++)
		{
		double dv = 0.;
		for (int key=from;key <= to;key++)
			{
			if (Lookup(key,dM))
				{
				dv += dM.getValNumInScheda(k);
				}
			}
		Mval = max(dv,Mval);
		}
	return (Mval);
	};

// Valore max trendl tra metro from e metro to
double getMaxTrendLFromTo(int code,int from,int to)
	{
	double Mval=0.;
	// Nessun elemnto nell'array
	if (GetCount() == 0)
		return (Mval);
	DifMetro dM;
	for (int key=from;key <= to;key++)
		{
		if (Lookup(key,dM))
			{
			Mval = max(dM.getValNum(code),Mval);
			}
		}
	return (Mval);
	};

// Valore min trendl tra metro from e metro to
double getMinTrendLFromTo(int code,int from,int to)
	{
	double mval=0.;
	// Nessun elemnto nell'array
	if (GetCount() == 0)
		return (mval);
	DifMetro dM;
	for (int key=from;key <= to;key++)
		{
		if (Lookup(key,dM))
			{
			mval = min(dM.getValNum(code),mval);
			}
		}
	return (mval);
	};

double getValRelativeTLStep(int code,int disp = 0)
	{double val;
	val = getValNumLStep(code,disp);
	val = val / getValMeanTL(code);
	return (val);
	};

// Indici su getMaxTrend
double getMaxRelativeTL(int code,int from,int to)
	{double val;
	val = getMaxTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};

double getMinRelativeTL(int code,int from,int to)
	{double val;
	val = getMinTrendLFromTo(code,from,to);
	val = val / getValMeanTL(code);
	return (val);
	};


double getValMeanTL(int code)
{
double val=0.;
// Nessun elemnto nell'array
if (GetCount() == 0)
	return (val);
POSITION pos;
DifMetro pDM;
int key;
pos = GetStartPosition();
while (pos != NULL)
	{
	GetNextAssoc(pos,key,pDM);
	val += pDM.getValNum(code);
	}

// trovo tutti i difetti di tipo code
// li divido per la lunghezza del supporto
val /= getMeter(); 
return (val);
};
//--------------------------------------------------
// riversa in dCoil i difetti dei metri da from a to
// torna numero di metri difettosi
int dump (CMapCoil &dCoil,int from,int to)
{
if (GetCount() <= 0)
	return 0;
int n=0;

DifMetro dM;
for (int key=from;key <= to;key++)
	{
	if (Lookup(key,dM))
		{
		// ricalcolo posizione
		dM.posizione = (int ) ( (to - from) + dM.posizione - getMeter());
		if (dM.posizione < 0)
			dM.posizione = 0;
		dCoil.SetAt(dM.posizione,dM);
		n ++;
		}
	}
return (n);};


//----------------------------
// Save and load
BOOL save(CFile *cf)
	{
	// Free Unused memory
	//FreeExtra();
	cf->Write((void *)&meter,sizeof(meter));
	//cf->Write((void *)&trueSizeEncod,sizeof(trueSizeEncod));
	cf->Write((void *)&largeSize,sizeof(largeSize));
	cf->Write((void *)&numeroClassi,sizeof(numeroClassi));
	cf->Write((void *)&numeroSchede,sizeof(numeroSchede));
	cf->Write((void *)&baseStepX,sizeof(baseStepX));
	int nDifMetri;
	nDifMetri = GetCount();
	cf->Write((void *)&nDifMetri,sizeof(nDifMetri));
	POSITION pos;
	DifMetro pDM;
	int key;
	pos = GetStartPosition();
	while (pos != NULL)
		{
		GetNextAssoc(pos,key,pDM);
		if (!pDM.save(cf)) return FALSE;
		}
	int n;
	n = c_sogliaAllarme.GetSize();
	cf->Write((void *)&n,sizeof(n));
	for (int i=0;i<n;i++)
		{
		double d;
		d = c_sogliaAllarme[i];
		cf->Write((void *)&d,sizeof(d));
		}
	return TRUE;};

BOOL load(CFile *cf,int ver)
	{
	cf->Read((void *)&meter,sizeof(meter));
	// cf->Read((void *)&trueSizeEncod,sizeof(trueSizeEncod));
	cf->Read((void *)&largeSize,sizeof(largeSize));
	cf->Read((void *)&numeroClassi,sizeof(numeroClassi));
	cf->Read((void *)&numeroSchede,sizeof(numeroSchede));
	cf->Read((void *)&baseStepX,sizeof(baseStepX));
	int nDifMetri;
	cf->Read((void *)&nDifMetri,sizeof(nDifMetri));
	// SetSize(nDifMetri);
	DifMetro pDM;
	for (int i=0;i<nDifMetri;i++)
		{
		if (!pDM.load(cf,ver)) return FALSE;
		SetAt(pDM.posizione,pDM);
		}
	int nSAlarm;
	cf->Read((void *)&nSAlarm,sizeof(nSAlarm));
	c_sogliaAllarme.SetSize(nSAlarm);
	for (i=0;i<nSAlarm;i++)
		{
		double d;
		cf->Read((void *)&d,sizeof(d));
		c_sogliaAllarme[i] = d;
		}
	return TRUE;};

};
