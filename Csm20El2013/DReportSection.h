#pragma once

#include "resource.h"
// CReportSection dialog

class CDReportSection : public CPropertyPage
{
	DECLARE_DYNAMIC(CDReportSection)

public:
	CDReportSection();
	virtual ~CDReportSection();

// Dialog Data
	enum { IDD = IDD_REPORTSECTION };
	BOOL	m_enableInt1;
	BOOL	m_enableInt2;
	BOOL	m_enableInt3;
	BOOL	m_enableInt4;
	BOOL	m_enableInt5;
	BOOL	m_enableInt6;
	BOOL	m_enableInt7;
	BOOL	m_enableInt8;
	BOOL	m_enableInt9;
	BOOL	m_enableInt10;
	BOOL	m_enableInt11;
	BOOL	m_enableInt12;
	BOOL	m_enableInt13;
	BOOL	m_enableInt14;
	BOOL	m_enableInt15;
	BOOL	m_enableInt16;
	BOOL	m_enableInt17;
	BOOL	m_enableInt18;
	BOOL	m_enableInt19;
	int		m_interv1;
	int		m_interv2;
	int		m_interv3;
	int		m_interv4;
	int		m_interv5;
	int		m_interv6;
	int		m_interv7;
	int		m_interv8;
	int		m_interv9;
	int		m_interv10;
	int		m_interv11;
	int		m_interv12;
	int		m_interv13;
	int		m_interv14;
	int		m_interv15;
	int		m_interv16;
	int		m_interv17;
	int		m_interv18;
	int		m_interv19;

protected:
	void enableIntervallo(int index); 

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnEnableInt1();
	afx_msg void OnEnableInt10();
	afx_msg void OnEnableInt11();
	afx_msg void OnEnableInt12();
	afx_msg void OnEnableInt13();
	afx_msg void OnEnableInt14();
	afx_msg void OnEnableInt15();
	afx_msg void OnEnableInt16();
	afx_msg void OnEnableInt17();
	afx_msg void OnEnableInt18();
	afx_msg void OnEnableInt19();
	afx_msg void OnEnableInt2();
	afx_msg void OnEnableInt3();
	afx_msg void OnEnableInt4();
	afx_msg void OnEnableInt5();
	afx_msg void OnEnableInt6();
	afx_msg void OnEnableInt7();
	afx_msg void OnEnableInt8();
	afx_msg void OnEnableInt9();
 	virtual BOOL OnInitDialog();
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()
};
