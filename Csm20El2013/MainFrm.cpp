// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"

#include "Dialog.h"			// Dialog
#include "DReport.h"		// Dialog
#include "DF1.h"
#include "DStripPosition.h"

// #include "PdfInfo.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "LogOff.h"

#include "DAlzataDettagli.h"
#include "DClassColor.h"


#include "Csm20El2013.h"

#include "DBSet.h"			// DB
#include "pwdlg.h"			// Password

// #include "RTextFile.h"

#include "DReportSheet.h"

#include <dos.h>
#include <direct.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

//IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)
IMPLEMENT_DYNAMIC(CMainFrame,  CMDIFrameWnd)

//BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_COMMAND_EX(CG_ID_VIEW_CALARMBAR, OnBarCheck)
	ON_UPDATE_COMMAND_UI(CG_ID_VIEW_CALARMBAR, OnUpdateControlBarMenu)
	ON_UPDATE_COMMAND_UI(IDC_LASER,       OnUpdateAlLaser)
	ON_UPDATE_COMMAND_UI(IDC_ENCODER,     OnUpdateAlEncoder)
	ON_UPDATE_COMMAND_UI(IDC_OTTURATORE1, OnUpdateAlOtturatore1)
	ON_UPDATE_COMMAND_UI(IDC_OTTURATORE2, OnUpdateAlOtturatore2)
	ON_UPDATE_COMMAND_UI(IDC_CPURECEIVER, OnUpdateAlCpuReceiver)

	ON_UPDATE_COMMAND_UI(ID_INDICATOR_DATE, OnUpdateDate)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_TIME, OnUpdateTime)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_STATUS_TOP, OnUpdateStatusTop)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_STATUS_BOT, OnUpdateStatusBot)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_METER, OnUpdateMeter)

	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_CONFIGURA_OPZIONI, OnConfiguraOpzioni)
	ON_COMMAND(ID_CONFIGURA_REPORT, OnConfiguraReport)
	ON_COMMAND(ID_RICETTE_COPIA, OnRicetteCopia)
	ON_COMMAND(ID_RICETTE_ELIMINA, OnRicetteElimina)
	ON_COMMAND(ID_RICETTE_MODIFICA, OnRicetteModifica)
	ON_COMMAND(ID_RICETTE_NUOVA, OnRicetteNuova)
	ON_COMMAND(ID_RICETTE_VISUALIZZA, OnRicetteVisualizza)
	ON_COMMAND(ID_CONFIGURA_PARAMETRI, OnConfiguraParametri)
	ON_COMMAND(ID_FILE_MODIFICAPASSWORD, OnFileModificapassword)
	ON_MESSAGE(WM_COPYDATA, OnServerRequest)
	ON_COMMAND(ID_FILE_COMPORT, OnFileComport)
	ON_WM_TIMER()
	ON_COMMAND(ID_CONFIGURA_SOGLIE, OnConfiguraSoglie)
	ON_COMMAND(ID_VIEW_REPAINT, OnViewRepaint)
	ON_COMMAND(ID_VIEW_SYSTEM, OnViewSystem)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SYSTEM, OnUpdateViewSystem)
	ON_COMMAND(ID_VIEW_TREND, OnViewTrend)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TREND, OnUpdateViewTrend)
	ON_UPDATE_COMMAND_UI(ID_FILE_OPEN, OnUpdateFileOpen)
	ON_UPDATE_COMMAND_UI(ID_CONFIGURA_PARAMETRI, OnUpdateConfiguraParametri)
	// ON_COMMAND(ID_VISUALIZZA_BOT_TOP, OnVisualizzaBotTop)
	ON_COMMAND(ID_CONFIGURA_AVVIO_F1, OnConfiguraAvvioF1)
	ON_COMMAND(ID_VIEW_DATA_BYCLASS, OnViewDataByclass)
	ON_UPDATE_COMMAND_UI(ID_VIEW_DATA_BYCLASS, OnUpdateViewDataByclass)
	// ON_UPDATE_COMMAND_UI(ID_VISUALIZZA_BOT_TOP, OnUpdateVisualizzaBotTop)
	ON_UPDATE_COMMAND_UI(ID_SINGLE_COIL_MODE, OnUpdateSingleCoilMode)
	ON_COMMAND(ID_CONFIGURA_LOCALIZZAZIONE, OnConfiguraLocalizzazione)
	ON_COMMAND(ID_CONFIGURA_PDF, OnConfiguraPdf)
	ON_COMMAND(ID_VIEW_KREPORT, OnViewKreport)
	ON_COMMAND(ID_CONFIGURA_COLORI, OnConfiguraColori)
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_SYSCOMMAND()
	ON_WM_NCLBUTTONDBLCLK()
	ON_WM_WINDOWPOSCHANGING()

END_MESSAGE_MAP()


static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_PROGRESS,
	ID_INDICATOR_LABEL_TOP,
	ID_INDICATOR_STATUS_TOP,	// visualizzazione stato hc16
	ID_INDICATOR_LABEL_BOT,
	ID_INDICATOR_STATUS_BOT,	// visualizzazione stato hc16
	ID_INDICATOR_METER		// Visualizzazione posizione attuale
	// ID_INDICATOR_DATE,
	// ID_INDICATOR_TIME
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	
// TODO: add member initialization code here
progressBusy = FALSE;
alarmTimerId = 0;
statusTimerId = 0;	
rxPipeTimerId = 0;
rxPipeTimerMsec =0;
c_lineSpeed = 0.;
c_doSendSoglie = FALSE;	// invia o no valori soglie insieme allo stato
c_valEncoder = 23;

RPCID_HC16_TOP=150;
RPCID_HC16_BOTTOM=151;

c_isReporting = FALSE;

}

CMainFrame::~CMainFrame()
{

}


CProgressCtrl* CMainFrame::getProgressCtrl(void)
	{
	if (progressBusy)
		return (&wndProgress);
	return (NULL);
	};
BOOL CMainFrame::progressCreate(void)
	{CRect rc;
	if (progressBusy)
		{
		progressDestroy();
		}
	m_wndStatusBar.GetItemRect (1, &rc);
	rc.top += 1;
	rc.bottom -=1;
	rc.left += 1;
	rc.right -=1;
	VERIFY (wndProgress.Create(WS_CHILD | WS_VISIBLE, rc,
    	     &m_wndStatusBar, 1));
	progressBusy = TRUE;
	return (TRUE);
	}
void CMainFrame::progressDestroy(void)
	{
	if (progressBusy)
		wndProgress.DestroyWindow();
	progressBusy = FALSE;
	}
void CMainFrame::progressSetRange(int from,int to)
	{if (progressBusy)
		wndProgress.SetRange(from, to);};
void CMainFrame::progressSetStep(int s)
	{if (progressBusy)
		wndProgress.SetStep(s);};
void CMainFrame::progressStepIt(void)
 	{if (progressBusy)
		wndProgress.StepIt();};




int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
CProfile profile;


	//if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.Create(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar top\n");
		return -1;      // fail to create
	}


	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

/*
	m_wndStatusBar.Init();
	UINT nID, nStyle;
    int cxWidth;

    m_wndStatusBar.GetPaneInfo(2, nID, nStyle, cxWidth);
    m_wndStatusBar.SetPaneInfo(2, nID, nStyle | SBT_OWNERDRAW | SBPS_POPOUT,
           cxWidth);
*/
	
	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
				CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
		//		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

c_disableSystemMenu=profile.getProfileBool(_T("init"),_T("DisableSystemMenu"),FALSE);
if (c_disableSystemMenu)
	{
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
		{
		pSysMenu->DeleteMenu(SC_CLOSE,MF_BYCOMMAND);	
		pSysMenu->DeleteMenu(SC_MOVE,MF_BYCOMMAND);	 
		pSysMenu->DeleteMenu(SC_ICON,MF_BYCOMMAND);	
		pSysMenu->DeleteMenu(SC_SIZE,MF_BYCOMMAND);	
		// pSysMenu->DeleteMenu(SC_ZOOM,MF_BYCOMMAND);	
		pSysMenu->DeleteMenu(SC_RESTORE,MF_BYCOMMAND);	
		}

	}



	// Check and start pdfCreator
c_pdfInfo.loadFromProfile();
// spostato connect su ogni stampa
//if (((CCSMApp*)AfxGetApp())->GetDefaultPrinter() == c_pdfInfo.getPdfPrinterName())
//	c_pdfInfo.pdfComConnect();

// Init Server Process
CString strValue;
	strValue = profile.getProfileString(_T("DataBase"), _T("Definition"), 
				_T("DbHole.def"));

	// Init DataBase Object And Open It
	dBase->load(strValue);
	strValue = profile.getProfileString(_T("DataBase"), _T("Name"), 
				_T("HOLE.MDB"));

	try{dBase->Open(strValue);}
	catch (CDaoException *e)
		{
		AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dBase.Open");
		return -1;
		}

	// invia o no valori soglie insieme allo stato
	 c_doSendSoglie = profile.getProfileBool(_T("Timer"), _T("SendSoglie"),FALSE);



	// CG: The following block was inserted by 'Status Bar' component.
	{
		// Find out the size of the static variable 'indicators' defined
		// by AppWizard and copy it
		int nOrigSize = sizeof(indicators) / sizeof(UINT);

		UINT* pIndicators = new UINT[nOrigSize + 2];
		memcpy(pIndicators, indicators, sizeof(indicators));

		// Call the Status Bar Component's status bar creation function
		if (!InitStatusBar(pIndicators, nOrigSize, 60))
		{
			TRACE0("Failed to initialize Status Bar\n");
			return -1;
		}
		delete[] pIndicators;
	}



// Init addressId in rpcClient
// Add remote system
strValue = profile.getProfileString(_T("init"), _T("TopCommDevice"),_T("COM1")); 
addRemote(RPCID_HC16_TOP,strValue);	

#ifdef USE_BOTTOM
strValue = profile.getProfileString(_T("init"), _T("BottomCommDevice"),_T("COM2")); 
addRemote(RPCID_HC16_BOTTOM,strValue);
RPCID_HC16_BOTTOM = profile.getProfileInt(_T("init"), _T("BottomCommAddress"),151);		
#endif

// address configurabili
RPCID_HC16_TOP = profile.getProfileInt(_T("init"), _T("TopCommAddress"),150);



// Register this and remote system
char name [81];
::GetClassName(GetSafeHwnd(),name,30);
strValue = name;
rpcRegister(strValue,RPCID_NTCSM);


// Create alarm timer
int intervalTimer;
// default 30 sec
intervalTimer = profile.getProfileInt(_T("Timer"), _T("AlarmSec"),30); 
// Disable Timer if interval == 0
if (intervalTimer > 0)
	{
	intervalTimer *= 1000; // Valore da secondi in millisec
	if ((alarmTimerId=SetTimer(rand(),intervalTimer,NULL))== 0)
		{
		AfxGetMainWnd()->MessageBox ("Cannot Create AlarmTimer","SetTimer");
		return(-1);
		}
	}


// default 20 sec.
intervalTimer = profile.getProfileInt(_T("Timer"), _T("StatusSec"),20); 
// Disable Timer if interval == 0
if (intervalTimer > 0)
	{
	intervalTimer *= 1000; // Valore da secondi in millisec
	if ((statusTimerId=SetTimer(rand(),intervalTimer,NULL))== 0)
		{
		AfxGetMainWnd()->MessageBox ("Cannot Create StatusTimer","SetTimer");
		return(-1);
		}
	}

// default 20 sec.
rxPipeTimerMsec = profile.getProfileInt(_T("Timer"), _T("RxPipeMsec"),50); 
// Disable Timer if interval == 0
if (intervalTimer > 0)
	{
	if ((rxPipeTimerId=SetTimer(rand(),rxPipeTimerMsec,NULL))== 0)
		{
		AfxGetMainWnd()->MessageBox ("Cannot Create rxPipeTimerId","SetTimer");
		return(-1);
		}
	}



	// TODO: Add a menu item that will toggle the visibility of the
	// dialog bar named "CAlarmBar":
	//   1. In ResourceView, open the menu resource that is used by
	//      the CMainFrame class
	//   2. Select the View submenu
	//   3. Double-click on the blank item at the bottom of the submenu
	//   4. Assign the new item an ID: CG_ID_VIEW_CALARMBAR
	//   5. Assign the item a Caption: CAlarmBar

	// TODO: Change the value of CG_ID_VIEW_CALARMBAR to an appropriate value:
	//   1. Open the file resource.h
	// CG: The following block was inserted by the 'Dialog Bar' component
	{
		// Initialize dialog bar m_wndCAlarmBar Top
		if (!m_wndCAlarmBarTop.Create(this, CG_IDD_CALARMBAR,
			CBRS_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_HIDE_INPLACE,
			CG_ID_VIEW_CALARMBAR))
		{
			TRACE0("Failed to create dialog bar m_wndCAlarmBar top \n");
			return -1;		// fail to create
		}

		m_wndCAlarmBarTop.EnableDocking(CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM);
		EnableDocking(CBRS_ALIGN_ANY);
		DockControlBar(&m_wndCAlarmBarTop);

	}

#ifdef USE_BOTTOM
	{
		// Initialize dialog bar m_wndCAlarmBar Bottom
		if (!m_wndCAlarmBarBottom.Create(this, CG_IDD_CALARMBAR,
			CBRS_TOP | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_HIDE_INPLACE,
			CG_ID_VIEW_CALARMBAR))
		{
			TRACE0("Failed to create dialog bar m_wndCAlarmBar bot \n");
			return -1;		// fail to create
		}

		m_wndCAlarmBarBottom.EnableDocking(CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM);
		EnableDocking(CBRS_ALIGN_ANY);
		DockControlBar(&m_wndCAlarmBarBottom);

	}
#endif

return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
static LPCSTR className = NULL;

if( !CMDIFrameWnd::PreCreateWindow(cs))
	return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs


if (className == NULL) 
	{
    // One-time class registration
    // The only purpose is to make the class name something meaningful
    // instead of "Afx:0x4d:27:32:hup1hup:hike!"
    WNDCLASS wndcls;
	::GetClassInfo(AfxGetInstanceHandle(), cs.lpszClass, &wndcls);
  	wndcls.lpszClassName = CSM_CLASSNAME;
    wndcls.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    VERIFY(AfxRegisterClass(&wndcls));
    className= CSM_CLASSNAME;
    }
cs.lpszClass = className;
return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
//	CFrameWnd::AssertValid();
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
//	CFrameWnd::Dump(dc);
}

#endif //_DEBUG



BOOL CMainFrame::Pump (void)
{
MSG msg;
//
// Retrieve and dispatch any waiting messages.
//
while (::PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE)) 
	{
	if (!AfxGetApp ()->PumpMessage ()) 
		{
		::PostQuitMessage (0);
		return FALSE;
		}
	}

//
// Simulate the framework's idle processing mechanism.
//
LONG lIdle = 0;
while (AfxGetApp ()->OnIdle (lIdle++))
	Sleep (500);
return TRUE;
}




/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnConfiguraOpzioni() 
{
DBOpzioni dbOpzioni(dBase);
CDOpzioni1 dialog;
CProfile profile;

// dBase already Open
if (dbOpzioni.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_Allarme = dbOpzioni.m_ALLARME_ACU;
	dialog.m_AllarmeDurata = (int )dbOpzioni.m_ALLARME_DURATA;
	dialog.m_stampaCoilAutomatica = dbOpzioni.m_STAMPA_AUTO;
	dialog.m_stampaStripAutomatica = dbOpzioni.m_STAMPA_AUTO_STRIP;
	// dialog.m_Mappa = dbOpzioni.m_MAPPA100;
	// dialog.m_Classe = dbOpzioni.m_TL_CLASSE;

	dialog.m_useHwFinger = profile.getProfileBool("init","UseFingerHw",TRUE);	

	dialog.m_Lunghezza1 = dbOpzioni.m_TL_LUNGHEZZA;
	dialog.m_Scala1 = dbOpzioni.m_TL_SCALA;


// 	dialog.m_Densita = dbOpzioni.m_TD_DENSITA;
//	dialog.m_Lunghezza2 = dbOpzioni.m_TD_LUNGHEZZA;
	dialog.m_Scala2 = dbOpzioni.m_TD_SCALA;
	
	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbOpzioni.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbOpzioni.Edit");
			dbOpzioni.Close();
			return ;
			}
		
	 	dbOpzioni.m_ALLARME_ACU = dialog.m_Allarme;
		dbOpzioni.m_ALLARME_DURATA = (double) dialog.m_AllarmeDurata;
		dbOpzioni.m_STAMPA_AUTO = dialog.m_stampaCoilAutomatica;
		dbOpzioni.m_STAMPA_AUTO_STRIP = dialog.m_stampaStripAutomatica;
		//dbOpzioni.m_MAPPA100 = dialog.m_Mappa;
	
		//dbOpzioni.m_TL_CLASSE = dialog.m_Classe;
		dbOpzioni.m_TL_LUNGHEZZA = dialog.m_Lunghezza1;
		dbOpzioni.m_TL_SCALA = dialog.m_Scala1;

		// dbOpzioni.m_TD_DENSITA = dialog.m_Densita;

		// dbOpzioni.m_TD_LUNGHEZZA = dialog.m_Lunghezza2;
		dbOpzioni.m_TD_SCALA = dialog.m_Scala2;
		try
			{
			dbOpzioni.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbOpzioni.Update");
			dbOpzioni.Close();
			return ;
			}
		dbOpzioni.Close();

// ----------------------------------
// Gestione cambio modalita`
		CString s;
		if (dialog.m_useHwFinger)
			s = "TRUE";
		else
			s = "FALSE";

		profile.writeProfileString("init","UseFingerHw",s);	
		profile.writeProfileString("init","UseDiaframmi",s);	
		
//--------------------------------------

		CLineDoc *pDoc;
		pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentTop();
		pDoc->UpdateBaseDoc();
		if (!pDoc->systemOnLine)
			{
			pDoc->c_useFingerHw = dialog.m_useHwFinger;
			if (!dialog.m_useHwFinger)
				{
				pDoc->c_offsetSx = 0;
				pDoc->c_useDiaframmi = FALSE;
				}
			else
				{
				pDoc->c_offsetSx = profile.getProfileInt(_T("Init"), _T("DBordoColtello"),0); 
				pDoc->c_useDiaframmi = TRUE;
				}
			}
		pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentBottom();
		pDoc->UpdateBaseDoc();
		if (!pDoc->systemOnLine)
			{
			pDoc->c_useFingerHw = dialog.m_useHwFinger;
			if (!dialog.m_useHwFinger)
				{
				pDoc->c_offsetSx = 0;
				pDoc->c_useDiaframmi = FALSE;
				}
			else
				{
				pDoc->c_offsetSx = profile.getProfileInt(_T("Init"), _T("DBordoColtello"),0); 
				pDoc->c_useDiaframmi = TRUE;
				}
			}
		}
	}


// Aggiorno View
// if (!PostMessage(WM_COMMAND,ID_VIEW_AGGIORNA))
//	AfxGetMainWnd()->MessageBox ("Fail ","Post Message");

}

void CMainFrame::OnConfiguraReport() 
{
// create tab dialog
CDReportSheet   reportDlg( "Report Configure Dialog" ); // 		
CDReportSection*    pSectionPage	= new CDReportSection;  
CDReportParam*		pParamPage		= new CDReportParam;  
	  
// enable close OK on sheet
reportDlg.setOkClose(TRUE);

ASSERT( pSectionPage );  
ASSERT( pParamPage );    
	  
reportDlg.AddPage( pSectionPage ); 
reportDlg.AddPage( pParamPage ); 


// TODO: Add your command handler code here
DBReport dbReport(dBase);
// dBase already Open

if (dbReport.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	pParamPage->m_Allarmi = dbReport.m_SOGLIE_ALLARME;
	// dialog.m_Cliente = dbReport.m_DATI_CLIENTE;
	// dialog.m_DataLavoraz = dbReport.m_DATA_LAVORAZIONE;
	pParamPage->m_difettiPer = dbReport.m_DESCR_PERIODICI;
	pParamPage->m_difettiRand = dbReport.m_DESCR_RANDOM;
	pParamPage->m_alzate  = dbReport.m_DESCR_ALZATE;

	// dialog.m_Lunghezza = dbReport.m_LUNGHEZZA_BOBINA;
	pParamPage->m_mappa = dbReport.m_MAPPA100;
	//-----------------
	pParamPage->m_graph1 = dbReport.m_DESCR_GRAPH1;
	pParamPage->m_graph2 = dbReport.m_DESCR_GRAPH2;
	pParamPage->m_graph3 = dbReport.m_DESCR_GRAPH3;
	pParamPage->m_graph4 = dbReport.m_DESCR_GRAPH4;

	pParamPage->m_classA = dbReport.m_DESCR_CLASSA;
	pParamPage->m_classB = dbReport.m_DESCR_CLASSB;
	pParamPage->m_classC = dbReport.m_DESCR_CLASSC;
	pParamPage->m_classD = dbReport.m_DESCR_CLASSD;
	pParamPage->m_classBig = dbReport.m_DESCR_CLASSBIG;
	//----------------------------
	// Eliminato 
//	dialog.m_alMetro = (int )dbReport.m_ALMETRO;
//	dialog.m_dalMetro = (int )dbReport.m_DALMETRO;

	pSectionPage->m_interv1 = (int )dbReport.m_INTERVALLO1;
	if (dbReport.m_INTERVALLO1 > 0.)
		pSectionPage->m_enableInt1 = TRUE;
	pSectionPage->m_interv2 = (int )dbReport.m_INTERVALLO2;
	if (dbReport.m_INTERVALLO2 > 0.)
		pSectionPage->m_enableInt2 = TRUE;
	pSectionPage->m_interv3 = (int )dbReport.m_INTERVALLO3;
	if (dbReport.m_INTERVALLO3 > 0.)
		pSectionPage->m_enableInt3 = TRUE;
	pSectionPage->m_interv4 = (int )dbReport.m_INTERVALLO4;
	if (dbReport.m_INTERVALLO4 > 0.)
		pSectionPage->m_enableInt4 = TRUE;
	pSectionPage->m_interv5 = (int )dbReport.m_INTERVALLO5;
	if (dbReport.m_INTERVALLO5 > 0.)
		pSectionPage->m_enableInt5 = TRUE;
	pSectionPage->m_interv6 = (int )dbReport.m_INTERVALLO6;
	if (dbReport.m_INTERVALLO6 > 0.)
		pSectionPage->m_enableInt6 = TRUE;
	pSectionPage->m_interv7 = (int )dbReport.m_INTERVALLO7;
	if (dbReport.m_INTERVALLO7 > 0.)
		pSectionPage->m_enableInt7 = TRUE;
	pSectionPage->m_interv8 = (int )dbReport.m_INTERVALLO8;
	if (dbReport.m_INTERVALLO8 > 0.)
		pSectionPage->m_enableInt8 = TRUE;
	pSectionPage->m_interv9 = (int )dbReport.m_INTERVALLO9;
	if (dbReport.m_INTERVALLO9 > 0.)
		pSectionPage->m_enableInt9 = TRUE;
	pSectionPage->m_interv10 = (int )dbReport.m_INTERVALLO10;
	if (dbReport.m_INTERVALLO10 > 0.)
		pSectionPage->m_enableInt10 = TRUE;
	pSectionPage->m_interv11 = (int )dbReport.m_INTERVALLO11;
	if (dbReport.m_INTERVALLO11 > 0.)
		pSectionPage->m_enableInt11 = TRUE;
	pSectionPage->m_interv12 = (int )dbReport.m_INTERVALLO12;
	if (dbReport.m_INTERVALLO12 > 0.)
		pSectionPage->m_enableInt12 = TRUE;
	pSectionPage->m_interv13 = (int )dbReport.m_INTERVALLO13;
	if (dbReport.m_INTERVALLO13 > 0.)
		pSectionPage->m_enableInt13 = TRUE;
	pSectionPage->m_interv14 = (int )dbReport.m_INTERVALLO14;
	if (dbReport.m_INTERVALLO14 > 0.)
		pSectionPage->m_enableInt14 = TRUE;
	pSectionPage->m_interv15 = (int )dbReport.m_INTERVALLO15;
	if (dbReport.m_INTERVALLO15 > 0.)
		pSectionPage->m_enableInt15 = TRUE;
	pSectionPage->m_interv16 = (int )dbReport.m_INTERVALLO16;
	if (dbReport.m_INTERVALLO16 > 0.)
		pSectionPage->m_enableInt16 = TRUE;
	pSectionPage->m_interv17 = (int )dbReport.m_INTERVALLO17;
	if (dbReport.m_INTERVALLO17 > 0.)
		pSectionPage->m_enableInt17 = TRUE;
	pSectionPage->m_interv18 = (int )dbReport.m_INTERVALLO18;
	if (dbReport.m_INTERVALLO18 > 0.)
		pSectionPage->m_enableInt18 = TRUE;
	pSectionPage->m_interv19 = (int )dbReport.m_INTERVALLO19;
	if (dbReport.m_INTERVALLO19 > 0.)
		pSectionPage->m_enableInt19 = TRUE;



	if (reportDlg.DoModal() == IDOK)
		{
		try
			{
			dbReport.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
			dbReport.Close();
			return ;
			}

		dbReport.m_SOGLIE_ALLARME = pParamPage->m_Allarmi;
		// dbReport.m_DATI_CLIENTE = dialog.m_Cliente;
		// dbReport.m_DATA_LAVORAZIONE = dialog.m_DataLavoraz;
		dbReport.m_DESCR_PERIODICI = pParamPage->m_difettiPer;
		dbReport.m_DESCR_RANDOM = pParamPage->m_difettiRand;
		dbReport.m_DESCR_ALZATE	= pParamPage->m_alzate;
		// dbReport.m_LUNGHEZZA_BOBINA = dialog.m_Lunghezza;
		dbReport.m_MAPPA100 = pParamPage->m_mappa;
//		dbReport.m_ALMETRO = (double)dialog.m_alMetro;
//		dbReport.m_DALMETRO = (double)dialog.m_dalMetro;
		//-----------------
		dbReport.m_DESCR_GRAPH1 = pParamPage->m_graph1;
		dbReport.m_DESCR_GRAPH2 = pParamPage->m_graph2;
		dbReport.m_DESCR_GRAPH3 = pParamPage->m_graph3;
		dbReport.m_DESCR_GRAPH4 = pParamPage->m_graph4;

		dbReport.m_DESCR_CLASSA = pParamPage->m_classA;
		dbReport.m_DESCR_CLASSB = pParamPage->m_classB;
		dbReport.m_DESCR_CLASSC = pParamPage->m_classC;
		dbReport.m_DESCR_CLASSD = pParamPage->m_classD;
		dbReport.m_DESCR_CLASSBIG = pParamPage->m_classBig;
		//----------------------------


		if (pSectionPage->m_enableInt1)
			dbReport.m_INTERVALLO1 = (double)pSectionPage->m_interv1;
		else
			dbReport.m_INTERVALLO1 = 0.;
		if (pSectionPage->m_enableInt2)
			dbReport.m_INTERVALLO2 = (double)pSectionPage->m_interv2;
		else
			dbReport.m_INTERVALLO2 = 0.;
		if (pSectionPage->m_enableInt3)
			dbReport.m_INTERVALLO3 = (double)pSectionPage->m_interv3;
		else
			dbReport.m_INTERVALLO3 = 0.;
		if (pSectionPage->m_enableInt4)
			dbReport.m_INTERVALLO4 = (double)pSectionPage->m_interv4;
		else
			dbReport.m_INTERVALLO4 = 0.;
		if (pSectionPage->m_enableInt5)
			dbReport.m_INTERVALLO5 = (double)pSectionPage->m_interv5;
		else
			dbReport.m_INTERVALLO5 = 0.;
		if (pSectionPage->m_enableInt6)
			dbReport.m_INTERVALLO6 = (double)pSectionPage->m_interv6;
		else
			dbReport.m_INTERVALLO6 = 0.;
		if (pSectionPage->m_enableInt7)
			dbReport.m_INTERVALLO7 = 
				(double)pSectionPage->m_interv7;
		else
			dbReport.m_INTERVALLO7 = 0.;
		if (pSectionPage->m_enableInt8)
			dbReport.m_INTERVALLO8 = 
				(double)pSectionPage->m_interv8;
		else
			dbReport.m_INTERVALLO8 = 0.;
		if (pSectionPage->m_enableInt9)
			dbReport.m_INTERVALLO9 = 
				(double)pSectionPage->m_interv9;
		else
			dbReport.m_INTERVALLO9 = 0.;
		if (pSectionPage->m_enableInt10)
			dbReport.m_INTERVALLO10 = 
				(double)pSectionPage->m_interv10;
		else
			dbReport.m_INTERVALLO10 = 0.;
		if (pSectionPage->m_enableInt11)
			dbReport.m_INTERVALLO11 = 
				(double)pSectionPage->m_interv11;
		else
			dbReport.m_INTERVALLO11 = 0.;
		if (pSectionPage->m_enableInt12)
			dbReport.m_INTERVALLO12 = 
				(double)pSectionPage->m_interv12;
		else
			dbReport.m_INTERVALLO12 = 0.;
		if (pSectionPage->m_enableInt13)
			dbReport.m_INTERVALLO13 = 
				(double)pSectionPage->m_interv13;
		else
			dbReport.m_INTERVALLO13 = 0.;
		if (pSectionPage->m_enableInt14)
			dbReport.m_INTERVALLO14 = 
				(double)pSectionPage->m_interv14;
		else
			dbReport.m_INTERVALLO14 = 0.;
		if (pSectionPage->m_enableInt15)
			dbReport.m_INTERVALLO15 = 
				(double)pSectionPage->m_interv15;
		else
			dbReport.m_INTERVALLO15 = 0.;
		if (pSectionPage->m_enableInt16)
			dbReport.m_INTERVALLO16 = 
				(double)pSectionPage->m_interv16;
		else
			dbReport.m_INTERVALLO16 = 0.;
		if (pSectionPage->m_enableInt17)
			dbReport.m_INTERVALLO17 = 
				(double)pSectionPage->m_interv17;
		else
			dbReport.m_INTERVALLO17 = 0.;
		if (pSectionPage->m_enableInt18)
			dbReport.m_INTERVALLO18 = 
				(double)pSectionPage->m_interv18;
		else
			dbReport.m_INTERVALLO18 = 0.;
		if (pSectionPage->m_enableInt19)
			dbReport.m_INTERVALLO19 = 
				(double)pSectionPage->m_interv19;
		else
			dbReport.m_INTERVALLO19 = 0.;
		try
			{
			dbReport.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbReport.Open");
			dbReport.Close();
			return ;
			}

		}
	}
dbReport.Close();

delete pSectionPage;
delete pParamPage;

}

void CMainFrame::OnUpdateConfiguraParametri(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();
// Enable only in mode !OnLine
pCmdUI->Enable(!pDoc->systemOnLine);
}

void CMainFrame::OnConfiguraParametri() 
{
// TODO: Add your command handler code here
DBParametri dbParametri (dBase);
CDParametri dialog;

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;

 if (dbParametri.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_FCDL = dbParametri.m_FCDL;
	dialog.m_LrStop = dbParametri.m_L_RESIDUA_STOP;
	dialog.m_alarmPulse = (long)dbParametri.m_ALARM_PULSE;
	dialog.m_diaEncoder = dbParametri.m_DIA_ENCODER;
	dialog.m_oddEvenDistance = dbParametri.m_ODDEVEN_DISTANCE;
	
	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbParametri.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbParametri.Open");
			dbParametri.Close();
			return ;
			}

		dbParametri.m_FCDL = dialog.m_FCDL;
		dbParametri.m_L_RESIDUA_STOP = dialog.m_LrStop;
		dbParametri.m_ALARM_PULSE = dialog.m_alarmPulse;
		dbParametri.m_DIA_ENCODER = dialog.m_diaEncoder;
		dbParametri.m_ODDEVEN_DISTANCE = dialog.m_oddEvenDistance;

		try
			{
			dbParametri.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbParametri.Open");
			dbParametri.Close();
			return ;
			}

		}
	}
dbParametri.Close();



}

//----------------------------------------------------------------------------------------
//
//	  Handler RICETTE
//
//----------------------------------------------------------------------------------------

void CMainFrame::OnRicetteCopia() 
{
// TODO: Add your command handler code here
// Seleziona Ricetta
CDCopiaRicette dialog;

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;


if (dialog.DoModal() == IDOK)
	{
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);
	DBRicette dbRicetteNew(dBase);

	// TODO: Add extra initialization here
	// dBase already Open
	// FALSE -> NotClose if EOF
	if (!dbRicette.openSelectID (dialog.m_ComboString,FALSE))
		{// Messaggi interni dbRicette
		return;
		}	
	if (!dbRicetteNew.openSelectID (dialog.m_ComboString,FALSE))
		{// Messaggi interni dbRicette
		dbRicette.Close();
		return;
		}	


	// Perform Copy Record
	if (dbRicetteNew.CanAppend())
		{
		try	{dbRicetteNew.AddNew();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.AddNew");
			dbRicette.Close();
			dbRicetteNew.Close();
			return;
			}
   		// Modify the Name of Ricetta
		dbRicetteNew.m_NOME = dialog.m_NewRicetta;
		dbRicetteNew.m_LEGA = dbRicette.m_LEGA;
		dbRicetteNew.m_ALLARME_A = dbRicette.m_ALLARME_A;
		dbRicetteNew.m_ALLARME_B = dbRicette.m_ALLARME_B;
		dbRicetteNew.m_ALLARME_C = dbRicette.m_ALLARME_C;
		dbRicetteNew.m_ALLARME_D = dbRicette.m_ALLARME_D;
		dbRicetteNew.m_SPESSORE_MAX = dbRicette.m_SPESSORE_MAX;
		dbRicetteNew.m_SPESSORE_MIN = dbRicette.m_SPESSORE_MIN;
		dbRicetteNew.m_LENGTH_ALLARME = dbRicette.m_LENGTH_ALLARME;
		dbRicetteNew.m_SOGLIA_A = dbRicette.m_SOGLIA_A;
		dbRicetteNew.m_SOGLIA_B = dbRicette.m_SOGLIA_B;
		dbRicetteNew.m_SOGLIA_C = dbRicette.m_SOGLIA_C;
		dbRicetteNew.m_SOGLIA_D = dbRicette.m_SOGLIA_D;
	
		try	{dbRicetteNew.Update();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.Update");
			dbRicetteNew.Close();
			dbRicette.Close();
			return;
			}
		}
	else
		AfxGetMainWnd()->MessageBox ("Can't Append to dbRicette","dbRicette.CanAppend");

	dbRicette.Close();
	dbRicetteNew.Close();

	CString strRes1,strRes2;
	strRes1.LoadString(CSM_MAINFRM_RICETTACOPIA);
	strRes2.LoadString(CSM_MAINFRM_HRICETTACOPIA);
	//AfxGetMainWnd()->MessageBox ("Creata Nuova Ricetta","Copia Ricetta");
	AfxGetMainWnd()->MessageBox (strRes1,strRes2);

	return;
	}
return;	
}

void CMainFrame::OnRicetteElimina() 
{
CDSelRicette dialog;

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;


if (dialog.DoModal() == IDOK)
	{
	CString str1,str2;

	//str.Format ("Cancellazione ricetta %s\nPremi OK per Cancellare\nPremi ANNULLA per NON Cancellare",dialog.m_EditSel); 
	str1.LoadString(CSM_MAINFRM_OKRICETTACANCELLA);
	str2.Format (str1,dialog.m_EditSel);
	if (AfxMessageBox(str2,MB_OKCANCEL) == IDCANCEL)
		return;
		
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);
	
	// TODO: Add extra initialization here
	// dBase already Open
	if (!dbRicette.openSelectID (dialog.m_EditSel,FALSE))
		{// Messaggi interni dbRicette
		return;
		}

	// Perform Delete Record
	if (dbRicette.CanUpdate())
		{
		try	{dbRicette.Delete();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.Delete");
			dbRicette.Close();
			return;
			}
		}
	else
		AfxGetMainWnd()->MessageBox ("Can't Delete record in dbRicette","dbRicette.CanUpdate");

	dbRicette.Close();

	CString strRes1,strRes2;
	strRes1.LoadString(CSM_MAINFRM_RICETTAELIMINA);
	strRes2.LoadString(CSM_MAINFRM_HRICETTAELIMINA);

	AfxGetMainWnd()->MessageBox (strRes1,strRes2);

	return;
	}
return;	
	
}

void CMainFrame::OnRicetteModifica() 
{
// TODO: Add your command handler code here
CDSelRicette dialog;

// CG: This code was inserted by the Password Dialog Component
//if (!CPasswordDlg::CheckPassword())
//	return;


if (dialog.DoModal() == IDOK)
	{
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);
	
	if (!dbRicette.openSelectID (dialog.m_EditSel,FALSE))
		{// Messaggi interni dbRicette
		return;
		}


	CDRicette dialogR;
	// Init Dialog Member Variable
	dialogR.m_Nome = dbRicette.m_NOME;

//	dialogR.m_Lega = dbRicette.m_LEGA;
	dialogR.m_SogliaA2 = (int )dbRicette.m_ALLARME_A;
	dialogR.m_SogliaB2 = (int )dbRicette.m_ALLARME_B;
	dialogR.m_SogliaC2 = (int )dbRicette.m_ALLARME_C;
	dialogR.m_SogliaD2 = (int )dbRicette.m_ALLARME_D;
	// dimensione fori in micron
	dialogR.m_SogliaA1 = dbRicette.m_SOGLIA_A;
	dialogR.m_SogliaB1 = dbRicette.m_SOGLIA_B;
	dialogR.m_SogliaC1 = dbRicette.m_SOGLIA_C;
	dialogR.m_SogliaD1 = dbRicette.m_SOGLIA_D;
	
//	dialogR.m_SpessoreMax = dbRicette.m_SPESSORE_MAX;
//	dialogR.m_SpessoreMin = dbRicette.m_SPESSORE_MIN;
	dialogR.m_densityLength = dbRicette.m_LENGTH_ALLARME;
	dialogR.m_sogliaPer = (int)dbRicette.m_SOGLIA_PER;

	dialogR.SetNameReadOnly();
	CLineDoc *pDoc;
	pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentTop();

	dialogR.setNumClassi(pDoc->c_difCoil.getNumClassi());

	if (dialogR.DoModal() == IDOK)
		{
		// Perform Copy Record
		if (dbRicette.CanUpdate())
			{
			try	{dbRicette.Edit();}
			catch (CDaoException *e)
				{
				AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.Edit");
				dbRicette.Close();
				return;
				}
   			// Modify the Name of Ricetta
//			dbRicette.m_LEGA = dialogR.m_Lega;
			dbRicette.m_ALLARME_A = (double )dialogR.m_SogliaA2;
			dbRicette.m_ALLARME_B = (double )dialogR.m_SogliaB2;
			dbRicette.m_ALLARME_C = (double )dialogR.m_SogliaC2;
			dbRicette.m_ALLARME_D = (double )dialogR.m_SogliaD2;
//			dbRicette.m_SPESSORE_MAX = (double )dialogR.m_SpessoreMax;
//			dbRicette.m_SPESSORE_MIN = (double )dialogR.m_SpessoreMin;
			dbRicette.m_LENGTH_ALLARME = (double)dialogR.m_densityLength;
			dbRicette.m_SOGLIA_PER = (double)dialogR.m_sogliaPer;

			// dimensione fori in micron
			dbRicette.m_SOGLIA_A = dialogR.m_SogliaA1;
			dbRicette.m_SOGLIA_B = dialogR.m_SogliaB1;
			dbRicette.m_SOGLIA_C = dialogR.m_SogliaC1;
			dbRicette.m_SOGLIA_D = dialogR.m_SogliaD1;

			try	{dbRicette.Update();}
			catch (CDaoException *e)
				{
				AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.Update");
				dbRicette.Close();
				return;
				}
			}
		else
			AfxGetMainWnd()->MessageBox ("Can't Update dbRicette","dbRicette.CanUpdate");

		dbRicette.Close();

		CString strRes1,strRes2;
		strRes1.LoadString(CSM_MAINFRM_RICETTAMODIFICA);
		strRes2.LoadString(CSM_MAINFRM_HRICETTAMODIFICA);
		//AfxGetMainWnd()->MessageBox ("Modificata Ricetta","Modifica Ricetta");
		AfxGetMainWnd()->MessageBox (strRes1,strRes2);
		}
	}	
}



void CMainFrame::OnRicetteNuova() 
{
// TODO: Add your command handler code here
DBRicette dbRicette(dBase);

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;

if (!dbRicette.openSelectDefault ())
		{// Messaggi interni dbRicette
		return;
		}
 

CDRicette dialogR;
// Init Dialog Member Variable
dialogR.m_Nome = "";

//dialogR.m_Lega = dbRicette.m_LEGA;
dialogR.m_SogliaA2 = (int )dbRicette.m_ALLARME_A;
dialogR.m_SogliaB2 = (int )dbRicette.m_ALLARME_B;
dialogR.m_SogliaC2 = (int )dbRicette.m_ALLARME_C;
dialogR.m_SogliaD2 = (int )dbRicette.m_ALLARME_D;
//dialogR.m_SpessoreMax = dbRicette.m_SPESSORE_MAX;
//dialogR.m_SpessoreMin = dbRicette.m_SPESSORE_MIN;
dialogR.m_densityLength = (int)dbRicette.m_LENGTH_ALLARME;
dialogR.m_sogliaPer = (int)dbRicette.m_SOGLIA_PER;

// dimensione fori in micron
dialogR.m_SogliaA1 = dbRicette.m_SOGLIA_A;
dialogR.m_SogliaB1 = dbRicette.m_SOGLIA_B;
dialogR.m_SogliaC1 = dbRicette.m_SOGLIA_C;
dialogR.m_SogliaD1 = dbRicette.m_SOGLIA_D;


CLineDoc *pDoc;
pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentTop();

dialogR.setNumClassi(pDoc->c_difCoil.getNumClassi());

if (dialogR.DoModal() == IDOK)
	{
	// Perform Copy Record
	if (dbRicette.CanAppend())
		{
		try	{dbRicette.AddNew();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.AddNew");
			dbRicette.Close();
			return;
			}
 
		// Modify the Name of Ricetta
		dbRicette.m_NOME = dialogR.m_Nome;

//		dbRicette.m_LEGA = dialogR.m_Lega;
		dbRicette.m_ALLARME_A = (double )dialogR.m_SogliaA2;
		dbRicette.m_ALLARME_B = (double )dialogR.m_SogliaB2;
		dbRicette.m_ALLARME_C = (double )dialogR.m_SogliaC2;
		dbRicette.m_ALLARME_D = (double )dialogR.m_SogliaD2;
//		dbRicette.m_SPESSORE_MAX = (double )dialogR.m_SpessoreMax;
//		dbRicette.m_SPESSORE_MIN = (double )dialogR.m_SpessoreMin;
		dbRicette.m_LENGTH_ALLARME = (double)(dialogR.m_densityLength);
		dbRicette.m_SOGLIA_PER = dialogR.m_sogliaPer;

		// dimensione fori in micron
		dbRicette.m_SOGLIA_A = dialogR.m_SogliaA1;
		dbRicette.m_SOGLIA_B = dialogR.m_SogliaB1;
		dbRicette.m_SOGLIA_C = dialogR.m_SogliaC1;
		dbRicette.m_SOGLIA_D = dialogR.m_SogliaD1;

		try	{dbRicette.Update();}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbRicette.Update");
			dbRicette.Close();
			return;
			}
		}
	else
		AfxGetMainWnd()->MessageBox ("Can't Append to dbRicette","dbRicette.CanAppend");

	CString strRes1,strRes2;
	strRes1.LoadString(CSM_MAINFRM_RICETTANUOVA);
	strRes2.LoadString(CSM_MAINFRM_HRICETTANUOVA);
	//xGetMainWnd()->MessageBox ("Inserita Ricetta","Nuova Ricetta");

	AfxGetMainWnd()->MessageBox (strRes1,strRes2);
	}
dbRicette.Close();
}	

	


void CMainFrame::OnRicetteVisualizza() 
{
// TODO: Add your command handler code here
CDSelRicette dialog;

if (dialog.DoModal() == IDOK)
	{
	// m_ComboString contiene selezione
	DBRicette dbRicette(dBase);

	if (!dbRicette.openSelectID (dialog.m_EditSel,FALSE))
		{// Messaggi interni dbRicette
		return;
		}


	CDRicette dialogR;
	// Init Dialog Member Variable
	dialogR.m_Nome = dbRicette.m_NOME;

//	dialogR.m_Lega = dbRicette.m_LEGA;

	dialogR.m_SogliaA2 = (int )dbRicette.m_ALLARME_A;
	dialogR.m_SogliaB2 = (int )dbRicette.m_ALLARME_B;
	dialogR.m_SogliaC2 = (int )dbRicette.m_ALLARME_C;
	dialogR.m_SogliaD2 = (int )dbRicette.m_ALLARME_D;
//	dialogR.m_SpessoreMax = dbRicette.m_SPESSORE_MAX;
//	dialogR.m_SpessoreMin = dbRicette.m_SPESSORE_MIN;
	dialogR.m_densityLength = (int)(dbRicette.m_LENGTH_ALLARME);
	dialogR.m_sogliaPer = (int)dbRicette.m_SOGLIA_PER;

	// dimensione fori in micron
	dialogR.m_SogliaA1 = dbRicette.m_SOGLIA_A;
	dialogR.m_SogliaB1 = dbRicette.m_SOGLIA_B;
	dialogR.m_SogliaC1 = dbRicette.m_SOGLIA_C;
	dialogR.m_SogliaD1 = dbRicette.m_SOGLIA_D;

	dialogR.SetAllReadOnly();
	dialogR.SetNameReadOnly();
	
	CLineDoc *pDoc;
	pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentTop();

	dialogR.setNumClassi(pDoc->c_difCoil.getNumClassi());

	if (dialogR.DoModal() == IDOK)
		{
		}
	dbRicette.Close();
	}		
}

//----------------------------------------------------------------------------------------
//
//	  Handler SOGLIE
//
//----------------------------------------------------------------------------------------


void CMainFrame::OnConfiguraSoglie() 
{
	// TODO: Add your command handler code here
	// TODO: Add your command handler code here
DBParametri dbParametri (dBase);
CDSoglie dialog;


// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;

 if (dbParametri.openSelectDefault ())
	{
	// Move Init Data from Db to Dialog
	dialog.m_SogliaA = (int )dbParametri.m_SOGLIA_A;
	dialog.m_SogliaB = (int )dbParametri.m_SOGLIA_B;
	dialog.m_SogliaC = (int )dbParametri.m_SOGLIA_C;
	dialog.m_SogliaD = (int )dbParametri.m_SOGLIA_D;

	
	if (dialog.DoModal() == IDOK)
		{
		try
			{
			dbParametri.Edit();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbParametri.Open");
			dbParametri.Close();
			return ;
			}
  		
		dbParametri.m_SOGLIA_A = dialog.m_SogliaA;
		dbParametri.m_SOGLIA_B = dialog.m_SogliaB;
		dbParametri.m_SOGLIA_C = dialog.m_SogliaC;
		dbParametri.m_SOGLIA_D = dialog.m_SogliaD;

		try
			{
			dbParametri.Update();
			}
		catch (CDaoException *e)
			{
			AfxGetMainWnd()->MessageBox (e->m_pErrorInfo->m_strDescription,"dbParametri.Update");
			dbParametri.Close();
			return ;
			}

		}
	}
dbParametri.Close();
}


void CMainFrame::OnFileModificapassword() 
{
// TODO: Add your command handler code here
// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::ClearPassword())
	return;
	
}

void CMainFrame::OnUpdateDate(CCmdUI* pCmdUI)
{
	// CG: This function was inserted by 'Status Bar' component.

	// Get current date and format it
	CTime time = CTime::GetCurrentTime();
	CString tFormatRes;
	tFormatRes.LoadString (CSM_MAINFRM_DATEFORMAT);
	//CString strDate = time.Format(_T("%a, %d, %b %Y "));
	CString strDate = time.Format(tFormatRes);

	// BLOCK: compute the width of the date string
	CSize size;
	{
		HGDIOBJ hOldFont = NULL;
		HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
		CClientDC dc(NULL);
		if (hFont != NULL) 
			hOldFont = dc.SelectObject(hFont);
		size = dc.GetTextExtent(strDate);
		if (hOldFont != NULL) 
			dc.SelectObject(hOldFont);
	}

	// Update the pane to reflect the current date
	UINT nID, nStyle;
	int nWidth;
	m_wndStatusBar.GetPaneInfo(m_nDatePaneNo, nID, nStyle, nWidth);
	m_wndStatusBar.SetPaneInfo(m_nDatePaneNo, nID, nStyle, size.cx*1.5);

	pCmdUI->SetText(strDate);
	pCmdUI->Enable(TRUE);

}

void CMainFrame::OnUpdateTime(CCmdUI* pCmdUI)
{
	// CG: This function was inserted by 'Status Bar' component.

	// Get current date and format it
	CString strTime; 
	CTime time = CTime::GetCurrentTime();
	if (time.GetTime() >= 0)
		{	
		CString tFormatRes;
		tFormatRes.LoadString (CSM_MAINFRM_TIMEFORMAT);
		//strTime = time.Format(_T("%I:%M %S "));
		strTime = time.Format(_T(tFormatRes));
		strTime = (time.GetHour() < 12 ? _T("") : _T(""))+ strTime +(time.GetHour() < 12 ? _T("AM "):_T("PM "));
		

		// BLOCK: compute the width of the date string
		CSize size;
		{
		HGDIOBJ hOldFont = NULL;
		HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
		CClientDC dc(NULL);
		if (hFont != NULL) 
			hOldFont = dc.SelectObject(hFont);
		size = dc.GetTextExtent(strTime);
		if (hOldFont != NULL) 
			dc.SelectObject(hOldFont);
		}

	// Update the pane to reflect the current time
	UINT nID, nStyle;
	int nWidth;
	m_wndStatusBar.GetPaneInfo(m_nTimePaneNo, nID, nStyle, nWidth);
	m_wndStatusBar.SetPaneInfo(m_nTimePaneNo, nID, nStyle, size.cx*1.5);
	pCmdUI->SetText(strTime);
	pCmdUI->Enable(TRUE);
	}
}

BOOL CMainFrame::InitStatusBar(UINT *pIndicators, int nSize, int nSeconds)
{
	// CG: This function was inserted by 'Status Bar' component.
	

	// Create an index for the DATE pane
	m_nDatePaneNo = nSize++;
	pIndicators[m_nDatePaneNo] = ID_INDICATOR_DATE;
	// Create an index for the TIME pane
	m_nTimePaneNo = nSize++;
	nSeconds = 1;
	pIndicators[m_nTimePaneNo] = ID_INDICATOR_TIME;

	// TODO: Select an appropriate time interval for updating
	// the status bar when idling.
	m_wndStatusBar.SetTimer(0x1000, nSeconds * 1000, NULL);

	return m_wndStatusBar.SetIndicators(pIndicators, nSize);

}


LRESULT CMainFrame::OnServerRequest(WPARAM wParam, LPARAM lParam)
{
AfxGetMainWnd()->SetFocus();

return (CRpcMClient::OnServerRequest(wParam,lParam));
}



// Modify Com Port Settings

void CMainFrame::OnFileComport() 
{
// TODO: Add your command handler code here

// CG: This code was inserted by the Password Dialog Component
if (!CPasswordDlg::CheckPassword())
	return;

CWinApp* pApp = AfxGetApp();

CDSelComPort dialog;
dialog.m_SelComPort	= pApp->GetProfileString(_T("init"), _T("commDevice"),_T("COM1")); 

if (dialog.DoModal() == IDOK)
	{
	pApp->WriteProfileString(_T("init"), _T("commDevice"),dialog.m_SelComPort); 
	}

	
}



void CMainFrame::OnUpdateFileOpen(CCmdUI* pCmdUI) 
{

CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();
if (pDoc)
	// Enable only in mode !OnLine
	pCmdUI->Enable(!pDoc->systemOnLine);

}


void CMainFrame::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default


// Test if Doc Variable serMsgReceived True otherways Alarm
CLineDoc *pDoc;
pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentTop();
if (pDoc == NULL)	
	return;

if (nIDEvent == rxPipeTimerId)
	{
	int updateView=0;
	KillTimer(rxPipeTimerId);
	CommandBuffer b;
	while (ReadPipeCommand(&b))
		{// get pck
		HRpcInfo rpcInfo(TYPE_DMCS);
		rpcInfo.setParamByte(b.c_block [0],b.c_block [1],b.c_block [2],b.c_block [3]);
		updateView = serverRequest (rpcInfo.getCmd(),
				rpcInfo.getSender(),
				(DWORD)rpcInfo.getSize(),
				(PVOID)&b.c_block[4]);
		}
	if (updateView == 2)
		pDoc->updateView();
	SetTimer(rxPipeTimerId,rxPipeTimerMsec,NULL);
	}

// Test allarmi seriale e disco pieno
// solo a sistema online altrimenti sporca file 
// caricato per visualizzazione
if ((nIDEvent == alarmTimerId)&&
		(pDoc->systemOnLine))
	{
	CString strFreeDiskSpace;
	BOOL alActive = FALSE;
	// Fill disk free information
		// Fill disk free information
	FARPROC pGetDiskFreeSpaceEx = GetProcAddress( GetModuleHandle("kernel32.dll"),
                         "GetDiskFreeSpaceExA");
    ULARGE_INTEGER i64FreeBytesToCaller;
    ULARGE_INTEGER i64TotalBytes;
    ULARGE_INTEGER i64FreeBytes;

	if (pGetDiskFreeSpaceEx)
		{    
		BOOL fResult = GetDiskFreeSpaceEx ("C:",
                (PULARGE_INTEGER) &i64FreeBytesToCaller,
                (PULARGE_INTEGER) &i64TotalBytes,
                (PULARGE_INTEGER) &i64FreeBytes);

		__int64 freeByte,totalByte;
		freeByte = i64FreeBytes.QuadPart;
		totalByte = i64TotalBytes.QuadPart;
		
		freeByte /= (1024*1024);
		long dFreeMegaByte = (DWORD)freeByte;
		long diskMin;
		CProfile profile;
		diskMin = profile.getProfileInt(_T("init"), _T("MinFreeDisk"),1000); 
		if ((dFreeMegaByte < diskMin)&&
			(!pDoc->diskAlarmActive))
			{
			CString allarmeSistema;
			CString strRes;
			strRes.LoadString(CSM_MAINFRM_SPDISK);
			// allarmeSistema.Format("Spazio su disco = %d(Mbytes)\r\nProvvedere alla Cancellazione\r\n",dFree);
			allarmeSistema.Format(strRes,dFreeMegaByte);

			pDoc->systemAlarm.appendMsg(allarmeSistema);
			pDoc->diskAlarmActive = TRUE;
			alActive = TRUE;
			}
		}
	else
		strFreeDiskSpace.LoadString(CG_IDS_DISK_SPACE_UNAVAIL);
	
	if ((!pDoc->serMsgReceived)&&
		(pDoc->systemOnLine)&&
		(!pDoc->serAlarmActive))
		{
		// Alarm
		// Allarmi di sistema
		CString allarmeSistema;
		// allarmeSistema.Format("Collegamento seriale interrotto\r\n");
		allarmeSistema.LoadString(CSM_MAINFRM_SERIALNOK);
		pDoc->systemAlarm.appendMsg(allarmeSistema);
		pDoc->serAlarmActive = TRUE;
		alActive = TRUE;
		}
	if (alActive)
		pDoc->systemAlarm.visualizza(this,pDoc->secAlarmSound);
	pDoc->serMsgReceived = FALSE;	
	}

// Invio messaggio programma attivo
if (nIDEvent == statusTimerId)
	{
	sendStatus (pDoc->systemOnLine);
	// invio valori delle soglie
	if (c_doSendSoglie)
		{
		Sleep (50);
		pDoc->sendValSoglie();
		}
	bool alHde = c_stAllarmiBottom.isAlarmActive(false) | c_stAllarmiTop.isAlarmActive(false);
	pDoc->sendValHwAlarm(alHde);
	}

// Test if Doc Variable serMsgReceived True otherways Alarm
pDoc = ((CCSMApp *)AfxGetApp())->GetDocumentBottom();

// disable bottom system update
pDoc = NULL;
//----------------------------
if (pDoc == NULL)	
	return;

// Test allarmi seriale e disco pieno
// solo a sistema online altrimenti sporca file 
// caricato per visualizzazione
if ((nIDEvent == alarmTimerId)&&
		(pDoc->systemOnLine))
	{
	CString strFreeDiskSpace;
	BOOL alActive = FALSE;
	// Fill disk free information
    ULARGE_INTEGER i64FreeBytesToCaller;
    ULARGE_INTEGER i64TotalBytes;
    ULARGE_INTEGER i64FreeBytes;

	BOOL fResult = GetDiskFreeSpaceEx ("C:",
                (PULARGE_INTEGER) &i64FreeBytesToCaller,
                (PULARGE_INTEGER) &i64TotalBytes,
                (PULARGE_INTEGER) &i64FreeBytes);

		__int64 freeByte,totalByte;
		freeByte = i64FreeBytes.QuadPart;
		totalByte = i64TotalBytes.QuadPart;
		
		freeByte /= (1024*1024);
		long dFreeMegaByte = (DWORD)freeByte;
		long diskMin;
		diskMin = AfxGetApp()->GetProfileInt(_T("init"), _T("MinFreeDisk"),10000); 
		if ((dFreeMegaByte < diskMin)&&
			(!pDoc->diskAlarmActive))
			{
			CString allarmeSistema;
			CString strRes;
			strRes.LoadString(CSM_MAINFRM_SPDISK);
			// allarmeSistema.Format("Spazio su disco = %d(Mbytes)\r\nProvvedere alla Cancellazione\r\n",dFree);
			allarmeSistema.Format(strRes,dFreeMegaByte);

			pDoc->systemAlarm.appendMsg(allarmeSistema);
			pDoc->diskAlarmActive = TRUE;
			alActive = TRUE;
			}
	else
		strFreeDiskSpace.LoadString(CG_IDS_DISK_SPACE_UNAVAIL);
	
	if ((!pDoc->serMsgReceived)&&
		(pDoc->systemOnLine)&&
		(!pDoc->serAlarmActive))
		{
		// Alarm
		// Allarmi di sistema
		CString allarmeSistema;
		// allarmeSistema.Format("Collegamento seriale interrotto\r\n");
		allarmeSistema.LoadString(CSM_MAINFRM_SERIALNOK);
		pDoc->systemAlarm.appendMsg(allarmeSistema);
		pDoc->serAlarmActive = TRUE;
		alActive = TRUE;
		}
	if (alActive)
		pDoc->systemAlarm.visualizza(this,pDoc->secAlarmSound);
	pDoc->serMsgReceived = FALSE;	
	}

// Invio messaggio programma attivo
if (nIDEvent == statusTimerId)
	{
	sendStatus (pDoc->systemOnLine);
	// invio valori delle soglie
	if (c_doSendSoglie)
		{
		Sleep (50);
		pDoc->sendValSoglie();
		}
	}

CFrameWnd::OnTimer(nIDEvent);
}


static BOOL onDestroyFlag = FALSE;

BOOL CMainFrame::DestroyWindow() 
{
	// TODO: Add your specialized code here and/or call the base class
if (alarmTimerId != 0)
	KillTimer(alarmTimerId);
if (statusTimerId != 0)
	KillTimer(statusTimerId);
	
m_wndStatusBar.KillTimer(0x1000);

if (dBase != NULL)
	{
	if (dBase->IsOpen())
		dBase->Close();
	delete dBase;
	dBase = NULL;
	AfxDaoTerm( );
	}

CProfile profile;
BOOL logOff;
logOff = profile.getProfileBool("init","LogOffOnExit",FALSE);

if ((!onDestroyFlag)&&
	(logOff))
	{
	// logOff no shutdown, no reboot
	onDestroyFlag = TRUE;
	LogOff(FALSE,FALSE);
	return FALSE;
	}
else
	return CFrameWnd::DestroyWindow();
}


//------------------------------------
//
//	Invia status message
//			broadcast	   
//------------------------------------


void CMainFrame::sendStatus (BOOL stato)
{
struct Command command;
char tipo;

command.cmd = VAL_TESTSER;

if (stato)
	tipo = ONCMD;
else
	tipo = OFFCMD;

command.size = sizeof (tipo);
command.data = (void *) &tipo;

// RpcClient e' base class mainframe
SendBroadcastGeneralCommand(&command);
//SendGeneralCommand(&command);

}

// Secondo parametro mittente
// valore di ritorno utilizzato per aggiornamento view su pacchetti
// extCounter e big hole
BOOL CMainFrame::serverRequest(int cmd,int sender,int size,void *data)
// BOOL CMainFrame::serverRequest(int cmd,int size,void *data)
{
int vret = 0;
// Notify alarm
c_serMsgReceived = TRUE;

bool fromTop = (sender == RPCID_HC16_TOP);

CLineDoc *doc=NULL,*doc1=NULL,*doc2=NULL;
doc1 = ((CCSMApp *)AfxGetApp())->GetDocumentTop();
doc2 = ((CCSMApp *)AfxGetApp())->GetDocumentBottom();
#ifdef MODE_SEPARATORE
// modo single sheet testa 2 disabilitata
if ((doc1->c_mode == 2) && 
	(doc1->c_sideActive == 1) && 
	(sender == RPCID_HC16_BOTTOM))
	return vret;

// FixBug copiaIncolla Doc1-Doc2 invece di ... vedi sotto 
if ((doc1->c_mode == 2) && 
	(doc1->c_sideActive == 2) && 
	(sender == RPCID_HC16_TOP))
	return vret;

if (sender == RPCID_HC16_TOP)
	doc = doc1;

if (((doc1->c_mode == 0) && (sender == RPCID_HC16_BOTTOM))||
	((doc1->c_mode == 2) && (doc1->c_sideActive==2) 
	&& (sender == RPCID_HC16_BOTTOM)))
	doc = doc2;
else
	doc = doc1;
#else
	doc = doc1;
#endif
// a parte i casi speciali sopra doc e` sempre doc1 

if (doc == NULL)
	return vret;

if (size > 242)
	return vret;

if ((size > 0)&&
	(data == NULL))
	return vret;

vret = 1;
switch (cmd)
	{
	case 0 :
		break;
	case VAL_COUNTER2AB :
		doc->gestValCounter2(data,size,(sender == RPCID_HC16_BOTTOM),UPDATE_MASK_A|UPDATE_MASK_B);
		vret = 2;
		break;
	case VAL_COUNTER2CD :
		// from Top
		doc->gestValCounter2(data,size,(sender == RPCID_HC16_BOTTOM),UPDATE_MASK_C|UPDATE_MASK_D);
		vret = 2;
		break;
	case VAL_COUNTER_BIGHOLE2 :
		// doc->gestValCounterBigHole2(data,size,(sender == RPCID_HC16_BOTTOM),UPDATE_MASK_BIG);
		doc->gestValCounter2(data,size,(sender == RPCID_HC16_BOTTOM),UPDATE_MASK_BIG);
		vret = 2;
		break;
	case VAL_EXTCOUNTER :
		// from Top
		doc->gestValExtCounter(data,size,(sender == RPCID_HC16_BOTTOM));
		vret = 2;
		break;
	case VAL_COUNTER_BIGHOLE :
		doc->gestValCounterBigHole(data,size);
		vret = 2;
		break;
	case VAL_PERIOD :
		doc->gestValPeriod(data,size);
		break;
	case VAL_ALARM :
		// anche in modo single laser su testa 2 
#ifdef MODE_SEPARATORE
		if ((doc1->c_mode == 1) && (sender == RPCID_HC16_BOTTOM))
			doc2->gestValAlarm(data,size);
		else
#endif			
			doc->gestValAlarm(data,size,fromTop);	
		break;
	case VAL_LASERKO :
		// anche in modo single laser su testa 2 
#ifdef MODE_SEPARATORE
		if ((doc1->c_mode == 1) && (sender == RPCID_HC16_BOTTOM))
			doc2->gestValLaser(data,size);
		else
#endif	
			doc->gestValLaser(data,size);
		break;
	case VAL_STATO :
#ifdef MODE_SEPARATORE
		if (sender == RPCID_HC16_TOP)
			doc1->gestValStatoHde(data,size,doc2);
		else
			doc2->gestValStatoHde(data,size,doc1);
#endif
			doc->gestValStatoHde(data,size,fromTop);
		break;
	case VAL_STOP :
		doc->gestValStop(data,size);
		break;
	case VAL_OPENFILE :
		doc->gestValOpen(data,size);
		break;
	case VAL_ALZATA :
		doc->gestValAlzata(data,size);
		break;
	case VAL_DIAFRAMMA :
		doc->gestValDiaframma(data,size);
		break;
	case VAL_EXTDIAFRAMMA :
		doc->gestValExtDiaframma(data,size,(sender == RPCID_HC16_BOTTOM));
		break;
	case VAL_CLOSEFILE :
		doc->gestValClose(data,size);
		break;
	case PACKET_ABORTED :
		{
		CString allarmeSistema;
		allarmeSistema.LoadString(CSM_MAINFRM_EGCOMUNICAZ);
		// allarmeSistema.Format("ERRORE GENERALE Comunicazioni\r\n");
		doc->systemAlarm.appendMsg(allarmeSistema);
		}
		break;
	case VAL_INPUT :
		doc->gestValInput(data,size);
		break;
	case VAL_SPEED :
		// solo da testa top
		if (fromTop)
			gestValLineSpeed(data,size);
		break;
//	case VAL_RISAUTOTEST :
//		doc->gestValRisAutotest(data,size);
//		break;
	default :
		{
		CString s;
		s.Format("DEFAULT COMMAND = %d ( 0x%x )\n",cmd,cmd);
		doc->systemAlarm.appendMsg(s);
		vret = 0;
		}
		break;
	}

return vret;
}



void CMainFrame::OnViewRepaint() 
{
// TODO: Add your command handler code here
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();
if (pDoc)
	pDoc->UpdateAllViews(NULL);	
}

//----------------------------------------------------------------------
//		Visualizzazione CDialogBar Allarmi

void CMainFrame::OnUpdateAlLaser(CCmdUI* pCmdUI)
{
	// CG: This function was inserted by 'Status Bar' component.

if (::IsWindow (m_wndCAlarmBarTop.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarTop.GetDlgItem (IDC_LASER);
	ASSERT(pCtrl->GetStyle() & SS_BITMAP);
	if (c_stAllarmiTop.getLaserState() != AL_HIDE)
		{
		CBitmap bm;
		switch (c_stAllarmiTop.getLaserState())
			{
			default:
			case AL_OFF :
				if (!bm.LoadBitmap (IDB_LASER_OFF))
					AfxMessageBox("Can't Load bitmap!");
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_LASER_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_LASER_FAULT);
				break;
			}
		pCtrl->SetBitmap ((HBITMAP)bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);
	}

if (::IsWindow (m_wndCAlarmBarBottom.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarBottom.GetDlgItem (IDC_LASER);
	ASSERT(pCtrl->GetStyle() & SS_BITMAP);
	if (c_stAllarmiBottom.getLaserState() != AL_HIDE)
		{
		CBitmap bm;
		switch (c_stAllarmiBottom.getLaserState())
			{
			default:
			case AL_OFF :
				if (!bm.LoadBitmap (IDB_LASER_OFF))
					AfxMessageBox("Can't Load bitmap!");
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_LASER_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_LASER_FAULT);
				break;
			}
		pCtrl->SetBitmap ((HBITMAP)bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);
	}

}

void CMainFrame::OnUpdateAlEncoder(CCmdUI* pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow (m_wndCAlarmBarTop.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarTop.GetDlgItem (IDC_ENCODER);
	if (c_stAllarmiTop.getEncoderState() != AL_HIDE)
		{
		CBitmap bm;
			switch (c_stAllarmiTop.getEncoderState())
			{
			default:
			case AL_OFF :
				bm.LoadBitmap (IDB_ENCODER_OFF);
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_ENCODER_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_ENCODER_FAULT);
				break;
			}
		pCtrl->SetBitmap ((HBITMAP)bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);
	}
if (::IsWindow (m_wndCAlarmBarBottom.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarBottom.GetDlgItem (IDC_ENCODER);
	if (c_stAllarmiBottom.getEncoderState() != AL_HIDE)
		{
		CBitmap bm;
			switch (c_stAllarmiBottom.getEncoderState())
			{
			default:
			case AL_OFF :
				bm.LoadBitmap (IDB_ENCODER_OFF);
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_ENCODER_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_ENCODER_FAULT);
				break;
			}
		pCtrl->SetBitmap ((HBITMAP)bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);
	}
}

void CMainFrame::OnUpdateAlOtturatore1(CCmdUI* pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow (m_wndCAlarmBarTop.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarTop.GetDlgItem (IDC_OTTURATORE1);
	if (c_stAllarmiTop.getOtturatore1State() != AL_HIDE)
		{
		CBitmap bm;
			switch (c_stAllarmiTop.getOtturatore1State())
			{
			default:
			case AL_OFF :
				if(!bm.LoadBitmap (IDB_OTTURATORE_OFF))
					AfxMessageBox("Can't Load bitmap!");
				
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_OTTURATORE_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_OTTURATORE_FAULT);
				break;
			}
		pCtrl->SetBitmap ((HBITMAP)bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);	
	}
if (::IsWindow (m_wndCAlarmBarBottom.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarBottom.GetDlgItem (IDC_OTTURATORE1);
	if (c_stAllarmiBottom.getOtturatore1State() != AL_HIDE)
		{
		CBitmap bm;
		switch (c_stAllarmiBottom.getOtturatore1State())
			{
			default:
			case AL_OFF :
				if(!bm.LoadBitmap (IDB_OTTURATORE_OFF))
					AfxMessageBox("Can't Load bitmap!");			
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_OTTURATORE_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_OTTURATORE_FAULT);
				break;
			}
		pCtrl->SetBitmap ((HBITMAP)bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);	
	}
}

void CMainFrame::OnUpdateAlOtturatore2(CCmdUI* pCmdUI)
{
// CG: This function was inserted by 'Status Bar' component.
if (::IsWindow (m_wndCAlarmBarTop.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarTop.GetDlgItem (IDC_OTTURATORE2);
	if (c_stAllarmiTop.getOtturatore2State() != AL_HIDE)
		{
		CBitmap bm;
			switch (c_stAllarmiTop.getOtturatore2State())
			{
			default:
			case AL_OFF :
				bm.LoadBitmap (IDB_OTTURATORE_OFF);
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_OTTURATORE_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_OTTURATORE_FAULT);
				break;
			}
		pCtrl->SetBitmap (bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);	
	}	
if (::IsWindow (m_wndCAlarmBarBottom.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarBottom.GetDlgItem (IDC_OTTURATORE2);
	CBitmap bm;
	if (c_stAllarmiBottom.getOtturatore2State() != AL_HIDE)
		{
		switch (c_stAllarmiBottom.getOtturatore2State())
			{
			default:
			case AL_OFF :
				bm.LoadBitmap (IDB_OTTURATORE_OFF);
				break;
			case AL_OK :
				bm.LoadBitmap (IDB_OTTURATORE_ON);
				break;
			case AL_ON :
				bm.LoadBitmap (IDB_OTTURATORE_FAULT);
				break;
			}
		pCtrl->SetBitmap (bm);
		bm.DeleteObject();
		}
	else
		pCtrl->ShowWindow(SW_HIDE);	
	}

}

void CMainFrame::OnUpdateAlCpuReceiver(CCmdUI* pCmdUI)
{
if (::IsWindow (m_wndCAlarmBarTop.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarTop.GetDlgItem (IDC_CPURECEIVER);
	CDC *pdc; // select current bitmap into a compatible CDC
    pdc = pCtrl->GetDC ();
	ASSERT (pdc != NULL);
	if (pdc != NULL)
		{
		CRect rect;
		CLineDoc *pDoc;
		pCtrl->GetClientRect(&rect);
		BOOL isOnTopZorder=FALSE;
		pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();
		if (pDoc != NULL)
			{
			if (pDoc->isTopSide())
				isOnTopZorder=TRUE;
			if ((pDoc->c_mode == 0)&&
				(pDoc->systemOnLine))
				isOnTopZorder=TRUE;

			}
		c_stAllarmiTop.draw(pdc,rect,isOnTopZorder);
		pCtrl->ReleaseDC(pdc);
		}
	}
if (::IsWindow (m_wndCAlarmBarBottom.m_hWnd)) 
	{
	CStatic* pCtrl = (CStatic*) m_wndCAlarmBarBottom.GetDlgItem (IDC_CPURECEIVER);
	CDC *pdc; // select current bitmap into a compatible CDC
    pdc = pCtrl->GetDC ();
	ASSERT (pdc != NULL);
	if (pdc != NULL)
		{
		CRect rect;
		CLineDoc *pDoc;
		pCtrl->GetClientRect(&rect);
		BOOL isOnTopZorder=FALSE;
		pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();
		if (pDoc != NULL)
			{
			if (!pDoc->isTopSide())
				isOnTopZorder=TRUE;
			if ((pDoc->c_mode == 0)&&
				(pDoc->systemOnLine))
				isOnTopZorder=TRUE;
			}

		c_stAllarmiBottom.draw(pdc,rect,isOnTopZorder);
		pCtrl->ReleaseDC(pdc);
		}
	}

}

//------------------------------------------------
//
//	Visualizzazione dello statoHDE in status Bar
//
void CMainFrame::OnUpdateStatusTop(CCmdUI* pCmdUI)
{
CString sTop;

CLineDoc *pDocTop=NULL;
pDocTop = ((CCSMApp *)AfxGetApp())->GetDocumentTop();
if (pDocTop == NULL)
	return;
switch (pDocTop->getStatoHde(true))
	{
	default :
	case HDE_STATE:
		// s = " HDE STATE ";
		sTop.LoadString(CSM_MAINFRM_HDESTDEFAULT);
		break;
	case HDE_WAITSTART:
		//s = " WAIT START ";
		sTop.LoadString(CSM_MAINFRM_HDESTWAITSTART);
		break;
	case HDE_WAITENABLE:
		//s = " WAIT ENABLE ";
		sTop.LoadString(CSM_MAINFRM_HDESTWAITENABLE);
		break;
	case HDE_INSPECTION:
		//s = " INSPECTION ";
		sTop.LoadString(CSM_MAINFRM_HDESTINSPECTION);
		break;
	case HDE_HOLD:
		//s = " HOLD ";
		sTop.LoadString(CSM_MAINFRM_HDESTHOLD);
		break;
	case HDE_CHANGE:
		//s = " CHANGE ";
		sTop.LoadString(CSM_MAINFRM_HDESTCHANGE);
		break;
	case HDE_SELFTEST:
		//s = " SELF TEST ";
		sTop.LoadString(CSM_MAINFRM_HDESTSELFTEST);
		break;
	case HDE_TEST:
		//s = " TEST ";
		sTop.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_WAITDIAPH:
		sTop = " ATTESA DIAFRAMMA ";
		//s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_TESTIO:
		sTop = " TEST I/O  ";
		//s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_WARMUP:
		sTop = " WARMUP  ";
		//s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	}


CSize size;
	{
	HGDIOBJ hOldFont = NULL;
	HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
	CClientDC dc(NULL);
	if (hFont != NULL) 
		hOldFont = dc.SelectObject(hFont);
	size = dc.GetTextExtent(sTop);
	if (hOldFont != NULL) 
		dc.SelectObject(hOldFont);
	}

// docTop, aggiunto label ora id 3
// Update the pane to reflect the current state
UINT nID, nStyle;
int nWidth;
// label
m_wndStatusBar.GetPaneInfo(2, nID, nStyle, nWidth);
nStyle |= SBPS_POPOUT; //  | SBT_OWNERDRAW ;
nWidth = max(nWidth,30);
m_wndStatusBar.SetPaneInfo(2, nID, nStyle, nWidth);
// ora stato
m_wndStatusBar.GetPaneInfo(3, nID, nStyle, nWidth);
// nStyle |= SBPS_POPOUT; //  | SBT_OWNERDRAW ;
nWidth = max(nWidth,2*size.cx);
m_wndStatusBar.SetPaneInfo(3, nID, nStyle, nWidth);

pCmdUI->SetText(sTop);
pCmdUI->Enable(TRUE);
}


//------------------------------------------------
//
//	Visualizzazione dello statoHDE in status Bar
//
void CMainFrame::OnUpdateStatusBot(CCmdUI* pCmdUI)
{
CString sBot;

CLineDoc *pDocTop=NULL;
pDocTop = ((CCSMApp *)AfxGetApp())->GetDocumentTop();
if (pDocTop == NULL)
	return;
//CLineDoc *pDocBot=NULL;
//pDocBot = ((CCSMApp *)AfxGetApp())->GetDocumentBottom();

//if (pDocBot == NULL)
//	return;

switch (pDocTop->getStatoHde(false))
	{
	default :
	case HDE_STATE:
		// s = " HDE STATE ";
		sBot.LoadString(CSM_MAINFRM_HDESTDEFAULT);
		break;
	case HDE_WAITSTART:
		//s = " WAIT START ";
		sBot.LoadString(CSM_MAINFRM_HDESTWAITSTART);
		break;
	case HDE_WAITENABLE:
		//s = " WAIT ENABLE ";
		sBot.LoadString(CSM_MAINFRM_HDESTWAITENABLE);
		break;
	case HDE_INSPECTION:
		//s = " INSPECTION ";
		sBot.LoadString(CSM_MAINFRM_HDESTINSPECTION);
		break;
	case HDE_HOLD:
		//s = " HOLD ";
		sBot.LoadString(CSM_MAINFRM_HDESTHOLD);
		break;
	case HDE_CHANGE:
		//s = " CHANGE ";
		sBot.LoadString(CSM_MAINFRM_HDESTCHANGE);
		break;
	case HDE_SELFTEST:
		//s = " SELF TEST ";
		sBot.LoadString(CSM_MAINFRM_HDESTSELFTEST);
		break;
	case HDE_TEST:
		//s = " TEST ";
		sBot.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_WAITDIAPH:
		sBot = " ATTESA DIAFRAMMA ";
		//s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_TESTIO:
		sBot = " TEST I/O  ";
		//s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	case HDE_WARMUP:
		sBot = " WARMUP  ";
		//s.LoadString(CSM_MAINFRM_HDESTTEST);
		break;
	}

CSize size;
	{
	HGDIOBJ hOldFont = NULL;
	HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
	CClientDC dc(NULL);
	if (hFont != NULL) 
		hOldFont = dc.SelectObject(hFont);
	size = dc.GetTextExtent(sBot);
	if (hOldFont != NULL) 
		dc.SelectObject(hOldFont);
	}

// docTop
// Update the pane to reflect the current state
UINT nID, nStyle;
int nWidth;
m_wndStatusBar.GetPaneInfo(4, nID, nStyle, nWidth);
nStyle |= SBPS_POPOUT; //  | SBT_OWNERDRAW ;
nWidth = max(nWidth,30);
m_wndStatusBar.SetPaneInfo(4, nID, nStyle, nWidth);

m_wndStatusBar.GetPaneInfo(5, nID, nStyle, nWidth);
// nStyle |= SBPS_POPOUT; //  | SBT_OWNERDRAW ;
nWidth = max(nWidth,2*size.cx);
m_wndStatusBar.SetPaneInfo(5, nID, nStyle, nWidth);

pCmdUI->SetText(sBot);
pCmdUI->Enable(TRUE);
}

// visualizza VEOCITA` barra di stato
void CMainFrame::OnUpdateMeter(CCmdUI* pCmdUI)
{

CString speed; 
speed.Format (" %06.2lf m/min ",c_lineSpeed);

CSize size;
	{
	HGDIOBJ hOldFont = NULL;
	HFONT hFont = (HFONT)m_wndStatusBar.SendMessage(WM_GETFONT);
	CClientDC dc(NULL);
	if (hFont != NULL) 
		hOldFont = dc.SelectObject(hFont);
	size = dc.GetTextExtent(speed);
	if (hOldFont != NULL) 
		dc.SelectObject(hOldFont);
	}

	// Update the pane to reflect the current state
UINT nID, nStyle;
int nWidth;
m_wndStatusBar.GetPaneInfo(6, nID, nStyle, nWidth);
// nStyle |= SBPS_POPOUT; //  | SBT_OWNERDRAW ;
nWidth = max(nWidth,2*size.cx);
m_wndStatusBar.SetPaneInfo(6, nID, nStyle,nWidth);

pCmdUI->SetText(speed);
pCmdUI->Enable(TRUE);
}


//----------------------------------------------
//		Finestre di allarmi
//----------------------------------------------
// attiva visualizzazione allarmi di sistema
//
void CMainFrame::OnViewSystem() 
{
// TODO: Add your command handler code here

CLineDoc *pDoc;
// pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();
pDoc = (CLineDoc *) (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();

if (pDoc == NULL)
	return;

BOOL visible = (pDoc->systemAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

if (visible)
	{// se deve nascondere nasconde entrambi
	pDoc->systemAlarm.DestroyWindow();
	CLineDoc *pTopDoc;
		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
	if (pTopDoc == NULL)
		return;

	if (pTopDoc == pDoc)
		// altro
		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();

	pTopDoc->systemAlarm.DestroyWindow();
	}
else
	{
	pDoc->systemAlarm.visualizza(this);
// F7 solo testa top
//	CLineDoc *pTopDoc;
//		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
//	if (pTopDoc == NULL)
//		return;

//	if (pTopDoc == pDoc)
		// altro
//		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
//	pTopDoc->systemAlarm.visualizza(this);
	}

}

void CMainFrame::OnUpdateViewSystem(CCmdUI* pCmdUI) 
{

CLineDoc *pDoc;
pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();

if (pDoc == NULL)
	return;

BOOL enable = (pDoc->systemAlarm.getNumLine() > 0)?TRUE:FALSE;	

pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
if (pDoc == NULL)
	return;

enable |= (pDoc->systemAlarm.getNumLine() > 0)?TRUE:FALSE;	


pCmdUI->Enable(enable);	


BOOL visible = (pDoc->systemAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

pCmdUI->SetCheck(visible);	
}

void CMainFrame::OnViewTrend() 
{
	// TODO: Add your command handler code here
CLineDoc *pDoc;

pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();
if (pDoc == NULL)
	return;



BOOL visible = (pDoc->densitaAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

if (visible)
	{
	pDoc->densitaAlarm.DestroyWindow();
	CLineDoc *pTopDoc;
		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
	if (pTopDoc == NULL)
		return;

	if (pTopDoc == pDoc)
		// altro
		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();

	pTopDoc->densitaAlarm.DestroyWindow();
	}
else
	{
	pDoc->densitaAlarm.visualizza(this);
// Solo testa top
//	CLineDoc *pTopDoc;
//		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
//	if (pTopDoc == NULL)
//		return;

//	if (pTopDoc == pDoc)
//		// altro
//		pTopDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
//	pTopDoc->densitaAlarm.visualizza(this);
	}
	
}

void CMainFrame::OnUpdateViewTrend(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
CLineDoc *pDoc;
pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();

if (pDoc == NULL)
	return;

BOOL enable = (pDoc->densitaAlarm.getNumLine() > 0)?TRUE:FALSE;	

pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();
if (pDoc == NULL)
	return;

enable |= (pDoc->densitaAlarm.getNumLine() > 0)?TRUE:FALSE;	


pCmdUI->Enable(enable);	


BOOL visible = (pDoc->densitaAlarm.GetSafeHwnd() == NULL)?FALSE:TRUE;	

pCmdUI->SetCheck(visible);	
	
}


//------------------------------------
//
//	Gestione Messaggi velocita` di linea
//			   
//------------------------------------

void CMainFrame::gestValLineSpeed (PVOID p,DWORD size) // Gestisce arrivo VAL_SPEED
{
// Received Msg 
c_serMsgReceived = TRUE;

// size 2
short sFrom,sTo,*sP;

if (size != 2)
	return;

sP = (short *) p;
sFrom = (short) *sP;

// Swap data (short format)
_swab ((char *)&sFrom,(char *)&sTo,1*sizeof(short));

/*
CLineDoc *pDoc;
pDoc = (CLineDoc *) GetActiveFrame( )->GetActiveDocument();

if (pDoc != NULL)
	{
	}
*/

// memo raw val encoder
c_valEncoder = sTo;

double speed;

if (sTo <= 0)
	{
	speed = 0.;
	}
else
	{
	// cm ogni 5 sec
	speed = (double) sTo;

	speed *= 12.;	 // cm in min	(era 6. per cm ogni 10 sec)

	// speed *= pDoc->c_fattCorrDimLin; // cm veri al min
	speed  /= 100.; // in metri al minuto
	}

c_lineSpeed = speed;
}


void CMainFrame::OnVisualizzaBotTop() 
{
// TODO: Add your command handler code here

// MDINext();

}

void CMainFrame::OnUpdateVisualizzaBotTop(CCmdUI* pCmdUI) 
{
BOOL enable;
#ifdef USE_BOTTOM_SEP
// TODO: Add your command update UI handler code here
CLineDoc *pDocTop,*pDocBot;
if (AfxGetApp() == NULL)
	return;
pDocTop = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentTop();
pDocBot = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();

if (pDocBot == NULL)
	return;

if (pDocTop == NULL)
	return;


// modo 1-singleCoil e 2-singleSheet
//enable  = (!(pDocTop->c_mode && pDocBot->c_mode));
//enable |= (pDocTop->c_sideActive==1)?(!pDocTop->systemOnLine):0;
//enable |= (pDocTop->c_sideActive==2)?(!pDocBot->systemOnLine):0;

enable = !((pDocTop->systemOnLine)||(pDocBot->systemOnLine));
pCmdUI->Enable(enable);	
#else
// enable = (pDocBot->systemOnLine);
// enable = TRUE; // usato per trend longit xche` F8 markerview/
// pCmdUI->Enable(enable);	
#endif	
}

void CMainFrame::OnConfiguraAvvioF1() 
{
CLineDoc *pDoc;
pDoc = (CLineDoc *)((CCSMApp *)AfxGetApp())->GetDocumentBottom();


if (pDoc->c_useFingerHw)
	{
	// TODO: Add your command handler code here
	CDStripPosition dialog;
	// dialog.c_offsetSx = pDoc->c_offsetSx;
	dialog.c_rightAlignStrip = pDoc->c_rightAlignStrip;
	dialog.c_hideMode = TRUE;
	dialog.loadFromProfile(FALSE);
	dialog.loadFromResource();
	if (dialog.DoModal() != IDOK)
		return;
	dialog.saveToProfile();
	}
else
	{
	// TODO: Add your command handler code here
	CDF1 dialog;
	dialog.c_hideMode = TRUE;

	dialog.loadFromProfile();
	//dialog.loadFromResource();
	if (dialog.DoModal() != IDOK)
		return;
	dialog.saveToProfile();
	}
}



void CMainFrame::OnViewDataByclass() 
{
// TODO: Add your command handler code here
CLineDoc* pDocTop;
CLineDoc* pDocBottom;
BOOL enable = FALSE;

pDocBottom = ((CCSMApp*)AfxGetApp())->GetDocumentBottom();

pDocTop = ((CCSMApp*)AfxGetApp())->GetDocumentTop();

c_alzataDettagli.c_pDocS = pDocBottom;
c_alzataDettagli.c_pDocZ = pDocTop;
// c_alzataDettagli.DoModal();


c_alzataDettagli.visualizza(this);	
}

void CMainFrame::OnUpdateViewDataByclass(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
CLineDoc* pDocTop;
CLineDoc* pDocBottom;
BOOL enable = FALSE;

pDocBottom = ((CCSMApp*)AfxGetApp())->GetDocumentBottom();

pDocTop = ((CCSMApp*)AfxGetApp())->GetDocumentTop();

if ((pDocBottom != NULL)&&
	(pDocTop != NULL))
	{// significativo solo online perche` mostra ultima sezione
	enable = pDocTop->systemOnLine || pDocBottom->systemOnLine;
	}


pCmdUI->Enable(enable);	

}






void CMainFrame::OnUpdateSingleCoilMode(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here

CLineDoc* pDocTop;
CLineDoc* pDocBottom;
BOOL enable = FALSE;

pDocBottom = ((CCSMApp*)AfxGetApp())->GetDocumentBottom();

pDocTop = ((CCSMApp*)AfxGetApp())->GetDocumentTop();

if ((pDocBottom != NULL)&&
	(pDocTop != NULL))
	{// significativo solo online perche` mostra ultima sezione
	enable = pDocTop->c_mode || pDocBottom->c_mode;
	}

pCmdUI->Enable(enable);	
	

}



void CMainFrame::OnConfiguraLocalizzazione() 
{
// TODO: Add your command handler code here
CProfile profile;
CString s;
	
if(profile.getProfileBool("init","EnableUseItalianLangDll",FALSE))
	{
	CString s;
	//s = "Utilizzare Lingua Italiana?";
	s.LoadString(SMSERVICE_ITALIAN_LANG);

	if (AfxMessageBox((LPCSTR)s,MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
		profile.writeProfileString("init","LangDll","NONE");
		CString s;
		//s = "Per cambiare la lingua chiudere e riavviare il programma";
		s.LoadString(SMSERVICE_RESTART_LANG);

		AfxMessageBox((LPCSTR)s,MB_ICONEXCLAMATION);
		return;
		}
	}	

// TODO: Add your command handler code here
CString szFilter ("dll Files (*.dll)|*.dll|All Files (*.*)|*.*||");

CFileDialog dialog (TRUE,(LPCSTR) "*.dll",
	(LPCSTR) "*.dll",0,(LPCSTR) szFilter);

SetCurrentDirectory(".");

if (dialog.DoModal() == IDOK)
	{
	CString dllName;
	dllName = dialog.GetPathName();
	profile.writeProfileString("init","LangDll",dllName);
	
	CString s;
	//s = "Per cambiare la lingua chiudere e riavviare il programma";
	s.LoadString(SMSERVICE_RESTART_LANG);

	AfxMessageBox((LPCSTR)s,MB_ICONEXCLAMATION);
	}
	
}




void CMainFrame::OnConfiguraPdf() 
{
// TODO: Add your command handler code here

#ifndef Csm20El2013_REPORT


c_pdfInfo.DoModal();	

#endif

}

void CMainFrame::OnViewKreport() 
{
// TODO: Add your command handler code here

	
CProfile profile;
CString procReport;
CString dirReport;

procReport = profile.getProfileString(_T("SERVER"), _T("ReportProg"), 
				"CSM20El2013Report.exe");
dirReport = profile.getProfileString(_T("SERVER"), _T("ReportDir"), 
				"./");
	

CWnd *report = CWnd::FindWindow(CSM_REPORT_CLASSNAME,NULL);
if (!report)
	{
	PROCESS_INFORMATION ProcessInformation;
	STARTUPINFO StartUpInfo;
	
	::GetStartupInfo (&StartUpInfo);
	BOOL fSuccess = CreateProcess((LPCSTR)procReport,NULL,
				NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,
				NULL,(LPCSTR)dirReport,
				&StartUpInfo,&ProcessInformation); 

	if (fSuccess) 
		{
		// Wait Creation Window RpcServer
		int res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
		if (res != 0)
			{
			res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
			if (res != 0)
				{
				CString s;
				s = "Fail Wait Idle Report " + procReport;
				AfxMessageBox (s);
				}
			}
	 	CloseHandle(ProcessInformation.hThread);
		CloseHandle(ProcessInformation.hProcess);
		CWnd *report = CWnd::FindWindow(CSM_REPORT_CLASSNAME,NULL);
		if(report)
			{
			report->ShowWindow(SW_SHOWMAXIMIZED);
			report->SetFocus( );
			report->SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0,
			  SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
			}

		}
	else
		{
		CString s;
		s = "Unable to Create Process " + procReport;
		AfxMessageBox (s);
		return;
		}
	}
else
	{// already open set zOrder TopMost
	
	report->ShowWindow(SW_SHOWMAXIMIZED);
	report->SetFocus( );
	report->SetWindowPos(&CWnd::wndTop, 0, 0, 0, 0,
      SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
	
	}


}


void CMainFrame::OnConfiguraColori() 
{
	
CDClassColor dialog;

dialog.loadFromProfile();

if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	CSM20ClassColorInvalidate();
	}
}


void CMainFrame::OnClose()
{
// TODO: Add your message handler code here and/or call default

	// TODO: Add your message handler code here and/or call default
CCSMApp *pApp;
pApp = (CCSMApp*) AfxGetApp();

if (pApp->GetDocumentTop()->CanCloseFrame(this))	
	CMDIFrameWnd::OnClose();

// 	__super::OnClose();
}




// disable alt+F4
void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
// TODO: Add your message handler code here and/or call default

if (IsWindowVisible()&&c_disableSystemMenu)
	{
	if((nID & 0xFFF0) == SC_CLOSE)
		{
		// do nothing - just return
 		return;
		}
	if((nID & 0xFFF0) == SC_MAXIMIZE)
		{
		// do nothing - just return
 		return;
		}
	if((nID & 0xFFF0) == SC_MINIMIZE)
		{
		// do nothing - just return
 		return;
		}
	if((nID & 0xFFF0) == SC_RESTORE)
		{
		// do nothing - just return
 		return;
		}
if((nID & 0xFFF0) == SC_SIZE)
		{
		// do nothing - just return
 		return;
		}
	}

__super::OnSysCommand(nID, lParam);
}

// disable resize with dblclick on top window bar
void CMainFrame::OnNcLButtonDblClk(UINT nHitTest, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

//	__super::OnNcLButtonDblClk(nHitTest, point);
}

// disable move/resize on single click top bar
void CMainFrame::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{

if (IsWindowVisible()&&c_disableSystemMenu)
	{
	lpwndpos->flags |= SWP_NOMOVE; 
	lpwndpos->flags |= SWP_NOSIZE; 
	lpwndpos->flags |= SWP_NOREPOSITION;
	}
__super::OnWindowPosChanging(lpwndpos);

	// TODO: Add your message handler code here
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	return __super::PreTranslateMessage(pMsg);
}
