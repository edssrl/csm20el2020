#if !defined(AFX_DNUMSTRIP_H__60827C01_2D77_426F_B620_AC1BA24BF397__INCLUDED_)
#define AFX_DNUMSTRIP_H__60827C01_2D77_426F_B620_AC1BA24BF397__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DNumStrip.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDNumStrip dialog

class CDNumStrip : public CDialog
{
// Construction
public:
	CDNumStrip(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDNumStrip)
	enum { IDD = IDD_NUMSTRIP };
	int		m_numStrip;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDNumStrip)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDNumStrip)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DNUMSTRIP_H__60827C01_2D77_426F_B620_AC1BA24BF397__INCLUDED_)
