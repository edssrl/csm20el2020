// Dialog.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"

#include "MainFrm.h"
#include "Dialog.h"

#include "Profile.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "Csm20El2013.h"

#include "DBSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDOpzioni1 dialog


CDOpzioni1::CDOpzioni1(CWnd* pParent /*=NULL*/)
	: CDialog(CDOpzioni1::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDOpzioni1)
	m_Lunghezza1 = 0.0;
	m_Scala1 = 0.0;
	m_Scala2 = 0.0;
	m_Allarme = FALSE;
	m_AllarmeDurata = 0;
	m_stampaCoilAutomatica = FALSE;
	m_stampaStripAutomatica = FALSE;
	m_useHwFinger = FALSE;
	//}}AFX_DATA_INIT

CProfile profile;
c_minLunghezza = (double) profile.getProfileInt("Opzioni","TLMinLunghezza",10);
c_maxLunghezza = (double) profile.getProfileInt("Opzioni","TLMaxLunghezza",1000);
}


void CDOpzioni1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDOpzioni1)
	DDX_Text(pDX, IDC_EDIT_LUNGHEZZA1, m_Lunghezza1);
	DDX_Text(pDX, IDC_EDIT_SCALA1, m_Scala1);
	DDX_Text(pDX, IDC_EDIT_SCALA2, m_Scala2);
//	DDX_Check(pDX, IDC_ALLARME_ACUSTICO2, m_Allarme);
//	DDX_Text(pDX, IDC_EDIT_ALLARMEACU, m_AllarmeDurata);
//	DDV_MinMaxInt(pDX, m_AllarmeDurata, 0, 59);
	DDX_Check(pDX, IDC_STAMPA_AUTOMATICA, m_stampaCoilAutomatica);
	DDX_Check(pDX, IDC_STAMPA_AUTOMATICA2, m_stampaStripAutomatica);
//	DDX_Check(pDX, IDC_USE_FINGER_HW, m_useHwFinger);
	//}}AFX_DATA_MAP

	DDX_Text(pDX, IDC_EDIT_LUNGHEZZA1, m_Lunghezza1);
	DDV_MinMaxDouble(pDX, m_Lunghezza1, c_minLunghezza, c_maxLunghezza);
}


BEGIN_MESSAGE_MAP(CDOpzioni1, CDialog)
	//{{AFX_MSG_MAP(CDOpzioni1)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDOpzioni1 message handlers
/////////////////////////////////////////////////////////////////////////////
// CDParametri dialog


CDParametri::CDParametri(CWnd* pParent /*=NULL*/)
	: CDialog(CDParametri::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDParametri)
	m_LrStop = 0.0;
	m_FCDL = 0.0;
	m_alarmPulse = 0;
	m_strAlPulse = _T("");
	m_diaEncoder = 0.0;
	c_minValAlPulse = 10;
	c_maxValAlPulse = 500;
	m_oddEvenDistance = 0.0;
	//}}AFX_DATA_INIT
}


void CDParametri::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDParametri)
	DDX_Text(pDX, IDC_EDIT_LRSTOP1, m_LrStop);
	DDX_Text(pDX, IDC_EDIT_FCDL1, m_FCDL);
	DDX_Text(pDX, IDC_EDIT_ALARMPULSE, m_alarmPulse);
	DDX_Text(pDX, IDC_STATIC_ALPULSE, m_strAlPulse);
	DDX_Text(pDX, IDC_EDIT_DIAM_ENCODER, m_diaEncoder);
	DDX_Text(pDX, IDC_EDIT_DIST_ODDEVEN_CELL, m_oddEvenDistance);
	//}}AFX_DATA_MAP
	DDV_MinMaxLong(pDX, m_alarmPulse, c_minValAlPulse, c_maxValAlPulse);
}


BEGIN_MESSAGE_MAP(CDParametri, CDialog)
	//{{AFX_MSG_MAP(CDParametri)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()




// CDParametri1 message handlers

BOOL CDParametri::OnInitDialog() 
{
CDialog::OnInitDialog();
	
// TODO: Add extra initialization here
	
CProfile profile;

c_minValAlPulse = profile.getProfileInt("Parametri","MinValAlPulse",10);
c_maxValAlPulse = profile.getProfileInt("Parametri","MaxValAlPulse",500);

m_strAlPulse.Format("(%d - %d msec)",c_minValAlPulse,c_maxValAlPulse);

UpdateData(FALSE);

return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}







/////////////////////////////////////////////////////////////////////////////
// CDRicette dialog


CDRicette::CDRicette(CWnd* pParent /*=NULL*/)
	: CDialog(CDRicette::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDRicette)
	m_SogliaA2 = 0;
	m_SogliaB2 = 0;
	m_SogliaC2 = 0;
	m_SogliaD2 = 0;
	m_Nome = _T("");
	m_SogliaA1 = 0;
	m_SogliaB1 = 0;
	m_SogliaC1 = 0;
	m_SogliaD1 = 0;
	m_densityLength = 0;
	m_sogliaPer = 0;
	//}}AFX_DATA_INIT
allReadOnly = FALSE;
nameReadOnly = FALSE;
c_numClassi = 4;

CProfile profile;
c_sMinA = profile.getProfileInt("Soglie","Min_A",10);
c_sMaxA = profile.getProfileInt("Soglie","Max_A",25);
c_sMinB = profile.getProfileInt("Soglie","Min_B",26);
c_sMaxB = profile.getProfileInt("Soglie","Max_B",99);
c_sMinC = profile.getProfileInt("Soglie","Min_C",100);
c_sMaxC = profile.getProfileInt("Soglie","Max_C",399);
c_sMinD = profile.getProfileInt("Soglie","Min_D",400);
c_sMaxD = profile.getProfileInt("Soglie","Max_D",800);

}


void CDRicette::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDRicette)
	DDX_Control(pDX, IDC_EDIT_SOGLIA_PER, m_ctrlSogliaPer);
	DDX_Control(pDX, IDC_DENSITY_LENGTH, m_ctrlDensityLength);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_B12, m_textSogliaB1);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_C1, m_textSogliaC1);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_D1, m_textSogliaD1);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_A1, m_textSogliaA1);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_D1, m_ctrlSogliaD1);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_C1, m_ctrlSogliaC1);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_B1, m_ctrlSogliaB1);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_A1, m_ctrlSogliaA1);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_D2, m_textSogliaD2);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_C2, m_textSogliaC2);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_B2, m_textSogliaB2);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_A2, m_textSogliaA2);
	DDX_Control(pDX, IDC_EDIT_NOME, m_EditNome);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_D2, m_EditSogliaD2);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_C2, m_EditSogliaC2);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_B2, m_EditSogliaB2);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_A2, m_EditSogliaA2);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_A2, m_SogliaA2);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_B2, m_SogliaB2);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_C2, m_SogliaC2);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_D2, m_SogliaD2);
	DDX_Text(pDX, IDC_EDIT_NOME, m_Nome);
	DDX_Text(pDX, IDC_DENSITY_LENGTH, m_densityLength);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_PER, m_sogliaPer);
	//}}AFX_DATA_MAP

	switch (c_numClassi)
		{
		case 4:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_D1, m_SogliaD1);
			DDV_MinMaxInt(pDX, m_SogliaD1, c_sMinD, c_sMaxD);
		case 3:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_C1, m_SogliaC1);
			DDV_MinMaxInt(pDX, m_SogliaC1, c_sMinC, c_sMaxC);
		case 2:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_B1, m_SogliaB1);
			DDV_MinMaxInt(pDX, m_SogliaB1, c_sMinB, c_sMaxB);
		case 1:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_A1, m_SogliaA1);
			DDV_MinMaxInt(pDX, m_SogliaA1, c_sMinA, c_sMaxA);
		default:
			break;
		}



}


BEGIN_MESSAGE_MAP(CDRicette, CDialog)
	//{{AFX_MSG_MAP(CDRicette)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDRicette::hideSoglie()
{

switch (c_numClassi)
	{
	case 0:
		m_ctrlSogliaA1.ShowWindow(SW_HIDE);
		m_textSogliaA1.ShowWindow(SW_HIDE);
		m_EditSogliaA2.ShowWindow(SW_HIDE);
		m_textSogliaA2.ShowWindow(SW_HIDE);
	case 1:
		m_ctrlSogliaB1.ShowWindow(SW_HIDE);
		m_textSogliaB1.ShowWindow(SW_HIDE);
		m_EditSogliaB2.ShowWindow(SW_HIDE);
		m_textSogliaB2.ShowWindow(SW_HIDE);
	case 2:
		m_textSogliaC1.ShowWindow(SW_HIDE);
		m_ctrlSogliaC1.ShowWindow(SW_HIDE);
		m_textSogliaC2.ShowWindow(SW_HIDE);
		m_EditSogliaC2.ShowWindow(SW_HIDE);
	case 3:
		m_textSogliaD1.ShowWindow(SW_HIDE);
		m_ctrlSogliaD1.ShowWindow(SW_HIDE);
		m_textSogliaD2.ShowWindow(SW_HIDE);
		m_EditSogliaD2.ShowWindow(SW_HIDE);
	case 4:
	default:
		break;
	}

}

void CDRicette::setNumClassi(int nc)
{

c_numClassi = nc;

}


void CDRicette::ReadOnly ()
{

//m_EditSpessoreMin.SetReadOnly (allReadOnly);
//m_EditSpessoreMax.SetReadOnly (allReadOnly);
m_EditSogliaD2.SetReadOnly (allReadOnly);
m_EditSogliaC2.SetReadOnly (allReadOnly);
m_EditSogliaB2.SetReadOnly (allReadOnly);
m_EditSogliaA2.SetReadOnly (allReadOnly);

m_ctrlSogliaD1.SetReadOnly (allReadOnly);
m_ctrlSogliaC1.SetReadOnly (allReadOnly);
m_ctrlSogliaB1.SetReadOnly (allReadOnly);
m_ctrlSogliaA1.SetReadOnly (allReadOnly);

m_ctrlSogliaPer.SetReadOnly (allReadOnly);
m_ctrlDensityLength.SetReadOnly (allReadOnly);

// qs prima era radioButton
//m_ctrlDensityLength2.EnableWindow (!allReadOnly);
//m_ctrlDensityLength3.EnableWindow (!allReadOnly);
//m_ctrlDensityLength4.EnableWindow (!allReadOnly);
//m_ctrlDensityLength5.EnableWindow (!allReadOnly);

//m_ctrlLega.SetReadOnly (allReadOnly);

m_EditNome.SetReadOnly (nameReadOnly);

}

/////////////////////////////////////////////////////////////////////////////
// CDRicette message handlers
/////////////////////////////////////////////////////////////////////////////
// CDCopiaRicette dialog


CDCopiaRicette::CDCopiaRicette(CWnd* pParent /*=NULL*/)
	: CDialog(CDCopiaRicette::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDCopiaRicette)
	m_NewRicetta = _T("");
	m_ComboString = _T("");
	//}}AFX_DATA_INIT
}


void CDCopiaRicette::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDCopiaRicette)
	DDX_Control(pDX, IDC_COMBO_SEL_RICETTE, m_ComboSel);
	DDX_Text(pDX, IDC_EDIT_NEW_RICETTA, m_NewRicetta);
	DDX_CBString(pDX, IDC_COMBO_SEL_RICETTE, m_ComboString);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDCopiaRicette, CDialog)
	//{{AFX_MSG_MAP(CDCopiaRicette)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDCopiaRicette message handlers
/////////////////////////////////////////////////////////////////////////////
// CDSelRicette dialog


CDSelRicette::CDSelRicette(CWnd* pParent /*=NULL*/)
	: CDialog(CDSelRicette::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDSelRicette)
	m_EditSel = _T("");
	//}}AFX_DATA_INIT
}


void CDSelRicette::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDSelRicette)
	DDX_Control(pDX, IDC_COMBO_SEL_RICETTE, m_ComboSel);
	DDX_CBString(pDX, IDC_COMBO_SEL_RICETTE, m_EditSel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDSelRicette, CDialog)
	//{{AFX_MSG_MAP(CDSelRicette)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDSelRicette message handlers



BOOL CDCopiaRicette::OnInitDialog() 
{

CDialog::OnInitDialog();

DBRicette dbRicette (dBase);

if (dbRicette.openSelectDefault())
	{
	m_ComboString = dbRicette.m_NOME;
	while(!dbRicette.IsEOF())
		{
		 m_ComboSel.AddString(dbRicette.m_NOME);
		 dbRicette.MoveNext();
		}
	dbRicette.Close();
	UpdateData(FALSE);	
	return TRUE;
	}

return FALSE;  // return TRUE unless you set the focus to a control
           // EXCEPTION: OCX Property Pages should return FALSE
}



BOOL CDSelRicette::OnInitDialog() 
{
CDialog::OnInitDialog();
	
// TODO: Add extra initialization here
DBRicette dbRicette(dBase);
	
dbRicette.openSelectDefault();

// TODO: Add extra initialization here
// dBase already Open
m_EditSel = dbRicette.m_NOME;
while(!dbRicette.IsEOF())
	{
	 m_ComboSel.AddString(dbRicette.m_NOME);
	 dbRicette.MoveNext();
 	}

dbRicette.Close();
UpdateData(FALSE);
	
return TRUE;  // return TRUE unless you set the focus to a control
             // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CDRicette::OnInitDialog() 
{
CDialog::OnInitDialog();
// TODO: Add extra initialization here
CString s,s1;
m_textSogliaA1.GetWindowText(s);
s1.Format("(%2d-%2d [um])",c_sMinA,c_sMaxA);
s += s1;
m_textSogliaA1.SetWindowText(s);
  
m_textSogliaB1.GetWindowText(s);
s1.Format("(%2d-%2d [um])",c_sMinB,c_sMaxB);
s += s1;
m_textSogliaB1.SetWindowText(s);

m_textSogliaC1.GetWindowText(s);
s1.Format("(%2d-%2d [um])",c_sMinC,c_sMaxC);
s += s1;
m_textSogliaC1.SetWindowText(s);

m_textSogliaD1.GetWindowText(s);
s1.Format("(%2d-%2d [um])",c_sMinD,c_sMaxD);
s += s1;
m_textSogliaD1.SetWindowText(s);

ReadOnly();
hideSoglie();

	
return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}
/////////////////////////////////////////////////////////////////////////////
// CDSelComPort dialog


CDSelComPort::CDSelComPort(CWnd* pParent /*=NULL*/)
	: CDialog(CDSelComPort::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDSelComPort)
	m_SelComPort = _T("");
	//}}AFX_DATA_INIT
}


void CDSelComPort::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDSelComPort)
	DDX_CBString(pDX, IDC_COMBO_SEL_COMPORT, m_SelComPort);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDSelComPort, CDialog)
	//{{AFX_MSG_MAP(CDSelComPort)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDSelFileAS400 dialog



/////////////////////////////////////////////////////////////////////////////
// CDSoglie dialog


CDSoglie::CDSoglie(CWnd* pParent /*=NULL*/)
	: CDialog(CDSoglie::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDSoglie)
	m_SogliaA = 0;
	m_SogliaB = 0;
	m_SogliaC = 0;
	m_SogliaD = 0;
	m_strSogliaA = _T("");
	m_strSogliaB = _T("");
	m_strSogliaC = _T("");
	m_strSogliaD = _T("");
	//}}AFX_DATA_INIT

CProfile profile;
c_sMinA = profile.getProfileInt("Soglie","Min_A",10);
c_sMaxA = profile.getProfileInt("Soglie","Max_A",25);
c_sMinB = profile.getProfileInt("Soglie","Min_B",26);
c_sMaxB = profile.getProfileInt("Soglie","Max_B",99);
c_sMinC = profile.getProfileInt("Soglie","Min_C",100);
c_sMaxC = profile.getProfileInt("Soglie","Max_C",399);
c_sMinD = profile.getProfileInt("Soglie","Min_D",400);
c_sMaxD = profile.getProfileInt("Soglie","Max_D",800);

c_nClassi = profile.getProfileInt("init","NumeroClassi",4);



CString str;
str.LoadString(CSM_CDSOGLIE_A);
m_strSogliaA.Format(str,c_sMinA,c_sMaxA);

str.LoadString(CSM_CDSOGLIE_B);
m_strSogliaB.Format(str,c_sMinB,c_sMaxB);

str.LoadString(CSM_CDSOGLIE_C);
m_strSogliaC.Format(str,c_sMinC,c_sMaxC);

str.LoadString(CSM_CDSOGLIE_D);
m_strSogliaD.Format(str,c_sMinD,c_sMaxD);


}


void CDSoglie::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDSoglie)
	DDX_Control(pDX, IDC_STATIC_SOGLIA_D1, m_cStrSogliaD);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_C1, m_cStrSogliaC);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_B12, m_cStrSogliaB);
	DDX_Control(pDX, IDC_STATIC_SOGLIA_A1, m_cStrSogliaA);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_D1, m_cSogliaD);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_C1, m_cSogliaC);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_B1, m_cSogliaB);
	DDX_Control(pDX, IDC_EDIT_SOGLIA_A1, m_cSogliaA);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_A1, m_SogliaA);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_B1, m_SogliaB);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_C1, m_SogliaC);
	DDX_Text(pDX, IDC_EDIT_SOGLIA_D1, m_SogliaD);
	DDX_Text(pDX, IDC_STATIC_SOGLIA_A1, m_strSogliaA);
	DDX_Text(pDX, IDC_STATIC_SOGLIA_B12, m_strSogliaB);
	DDX_Text(pDX, IDC_STATIC_SOGLIA_C1, m_strSogliaC);
	DDX_Text(pDX, IDC_STATIC_SOGLIA_D1, m_strSogliaD);
	//}}AFX_DATA_MAP

	switch (c_nClassi)
		{
		case 4:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_D1, m_SogliaD);
			DDV_MinMaxInt(pDX, m_SogliaD, c_sMinD, c_sMaxD);
		case 3:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_C1, m_SogliaC);
			DDV_MinMaxInt(pDX, m_SogliaC, c_sMinC, c_sMaxC);
		case 2:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_B1, m_SogliaB);
			DDV_MinMaxInt(pDX, m_SogliaB, c_sMinB, c_sMaxB);
		case 1:
			DDX_Text(pDX, IDC_EDIT_SOGLIA_A1, m_SogliaA);
			DDV_MinMaxInt(pDX, m_SogliaA, c_sMinA, c_sMaxA);
		default:
			break;
		}

	

}


BEGIN_MESSAGE_MAP(CDSoglie, CDialog)
	//{{AFX_MSG_MAP(CDSoglie)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDSoglie message handlers


BOOL CDSoglie::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

// Agosto 2004 soglie gestite da ricette
//switch (c_nClassi)
switch (0)
	{
	case 0:
		m_cSogliaA.EnableWindow(FALSE);
		m_cStrSogliaA.EnableWindow(FALSE);
	case 1:
		m_cSogliaB.EnableWindow(FALSE);
		m_cStrSogliaB.EnableWindow(FALSE);
	case 2:
		m_cSogliaC.EnableWindow(FALSE);
		m_cStrSogliaC.EnableWindow(FALSE);
	case 3:
		m_cStrSogliaD.EnableWindow(FALSE);
		m_cSogliaD.EnableWindow(FALSE);
	case 4:
	default:
		break;
	}

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}




/////////////////////////////////////////////////////////////////////////////
// CDAttesaStampa dialog


CDAttesaStampa::CDAttesaStampa(CWnd* pParent /*=NULL*/)
	: CDialog(CDAttesaStampa::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDAttesaStampa)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDAttesaStampa::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDAttesaStampa)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDAttesaStampa, CDialog)
	//{{AFX_MSG_MAP(CDAttesaStampa)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDAttesaStampa message handlers

void CDAttesaStampa::centerWindow(void )
{// Random Move

// Move Window
CRect rect,rectParent;
// Parent Client Rect
GetParent()->GetClientRect(&rectParent);
// this WRect
GetWindowRect (&rect);

int deltax= ((rectParent.right - rectParent.left)-(rect.right - rect.left))/2; 
int deltay = ((rectParent.top - rectParent.bottom)-(rect.top - rect.bottom))/2; 


MoveWindow (deltax,deltay,abs(rect.right - rect.left),
	  abs(rect.top - rect.bottom),TRUE);

}



