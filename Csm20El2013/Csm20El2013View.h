// Csm20El2013View.h : interface of the Csm20El2013View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEMOHOLEVIEW_H__92CB472D_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_)
#define AFX_DEMOHOLEVIEW_H__92CB472D_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class Csm20El2013View : public CView
{
protected: // create from serialization only
	Csm20El2013View();
	DECLARE_DYNCREATE(Csm20El2013View)

// Attributes
public:
	Csm20El2013Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Csm20El2013View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~Csm20El2013View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(Csm20El2013View)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in Csm20El2013View.cpp
inline Csm20El2013Doc* Csm20El2013View::GetDocument()
   { return (Csm20El2013Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOHOLEVIEW_H__92CB472D_CAFD_11D1_A6AC_00C026A019B7__INCLUDED_)
