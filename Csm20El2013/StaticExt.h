#if !defined(AFX_STATICEXT_H__5F1C9D51_DCC5_11D5_AB0F_00C026A019B7__INCLUDED_)
#define AFX_STATICEXT_H__5F1C9D51_DCC5_11D5_AB0F_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StaticExt.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStaticExt window


#include <vector>
using namespace std;

// Classe per visualizzare testi con qualunque Font e
// qualunque colore

struct ColorFontChars
	{
	CString fontName;
	int fontSize;
	COLORREF fontColor;
	COLORREF backColor;
	CString fontSubString;
	ColorFontChars() {fontSubString = "";
		fontName = "Arial";fontSize = 100; 
		fontColor = RGB(0,0,0);
		backColor = RGB(192,192,192);};
	
	};

// visualizza in sequenza tutte le stringhe, con i relativi attributi,
// presenti in colorFontChars

class CStaticExt : public CStatic
{
// Construction
public:
	CStaticExt();

// Attributes
public:
	vector <ColorFontChars> c_stringVector;
// Operations
public:
	int addString(CString s,CString fontName, int fontSize, 
		COLORREF color, COLORREF backColor);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStaticExt)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStaticExt();

	// Generated message map functions
protected:
	//{{AFX_MSG(CStaticExt)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STATICEXT_H__5F1C9D51_DCC5_11D5_AB0F_00C026A019B7__INCLUDED_)
