// RollView.cpp : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"
#include "Layout.h"
// #include "ctrlext.h"
// #include "ListCtrlUp.h"

#include "MarkerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMarkerView

IMPLEMENT_DYNCREATE(CMarkerView, CView)

CMarkerView::CMarkerView()
{
vType = ROLLVIEW;

layout.setViewType(ROLLVIEW);
layout.setMode(NOLABELV | USE_STRIPRULER);
// layout.c_useGrid = TRUE;

// Init layout Fix Attributes
CProfile profile;
CString s;


layout.fCaption.size = profile.getProfileInt("MarkermapLayout","CaptionSize",120);
layout.fLabel.size = profile.getProfileInt("MarkermapLayout","LabelSize",100);
layout.fNormal.size = profile.getProfileInt("MarkermapLayout","NormalSize",110);


s = profile.getProfileString("MarkermapLayout","NewLine","3,6");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString("MarkermapLayout","LabelOrder","0,1,2,3,4,5,6,7,8,9");
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

s = profile.getProfileString("MarkermapLayout","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layout.c_pxSepLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

//s = profile.getProfileString("MarkermapLayout","SCaption",
//							 "MARKER MAP");

s.LoadString(CSM_GRAPHVIEW_MARKERCAPTION);

layout.setCaption(s);

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL0);
layout.c_sLabel[0] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL1);
layout.c_sLabel[1] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL2);
layout.c_sLabel[2] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL3);
layout.c_sLabel[3] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL4);
layout.c_sLabel[4] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL5);
layout.c_sLabel[5] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL6);
layout.c_sLabel[6] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL7);
layout.c_sLabel[7] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL8);
layout.c_sLabel[8] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL9);
layout.c_sLabel[9] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL10);
layout.c_sLabel[10] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL11);
layout.c_sLabel[11] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL12);
layout.c_sLabel[12] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL13);
layout.c_sLabel[13] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL14);
layout.c_sLabel[14] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL15);
layout.c_sLabel[15] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL16);
layout.c_sLabel[16] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL17);
layout.c_sLabel[17] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL18);
layout.c_sLabel[18] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL19);
layout.c_sLabel[19] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL20);
layout.c_sLabel[20] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL21);
layout.c_sLabel[21] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL22);
layout.c_sLabel[22] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL23);
layout.c_sLabel[23] = s;


// ora anche Big!
for (int i=0;(i<=4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	if (i == 4)
		s = "Big";
	layout.c_cLabel[i] = s;
	layout.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layout.c_cLabel[i].setTipoIcon(IPALLINO);
	}

// CalcDrawRect
layout.c_viewTopPerc = profile.getProfileInt("MarkermapLayout","ViewTop%",10);
layout.c_viewBottomPerc = profile.getProfileInt("MarkermapLayout","ViewBottom%",25);
layout.c_viewLeftPerc = profile.getProfileInt("MarkermapLayout","ViewLeft%",8);
layout.c_viewRightPerc = profile.getProfileInt("MarkermapLayout","ViewRight%",5);

// dimensione pallini
c_pxBRoll = profile.getProfileInt("MarkermapLayout","RollSize",12);

// Don't update internal background
layout.setRgbDrawBackColor(RGB(0,0,0));

// mirror mode 1 a dx
// layout.c_mirrorLabel = TRUE;
// layout.c_mirrorDefect = FALSE;
}

CMarkerView::~CMarkerView()
{
}


BEGIN_MESSAGE_MAP(CMarkerView, CView)
	//{{AFX_MSG_MAP(CMarkerView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMarkerView drawing

CRect CMarkerView::getDrawRect(void)
{
CRect rcBounds,rect;

GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rect);
return (rect);
}



void CMarkerView::OnDraw(CDC* pDC)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();
// TODO: add draw code here
// Call Base Class Draw
CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);
// Call Base Class Draw


// init mark counter
{
int nMaxStrip = min(pDoc->c_markerCoil.getNumSchede(),MAX_NUMSTRIP);

for (int i=0;i<nMaxStrip;i++)
	{

	// solo per test altrimenti inverto due volte!!!
//	if (((pDoc->c_stripDisable>>i)&0x01)==1)
//		layout.c_countStrip[i]	= -1;
//	else
		{
		//layout.c_countStrip[i] = (int)pDoc->c_markerCoil.getTotDifetti (((pFrame->c_rightAlignStrip)?nMaxStrip-i-1:i),'A',0,pDoc->c_markerCoil.getMeter());
		layout.c_countStrip[i] = (int)pDoc->c_markerCoil.getTotDifetti  (i,'A',0,pDoc->c_markerCoil.getMeter());
		layout.c_countStrip[i] += (int)pDoc->c_markerCoil.getTotDifetti (i,'B',0,pDoc->c_markerCoil.getMeter());
		layout.c_countStrip[i] += (int)pDoc->c_markerCoil.getTotDifetti (i,'C',0,pDoc->c_markerCoil.getMeter());
		layout.c_countStrip[i] += (int)pDoc->c_markerCoil.getTotDifetti (i,'D',0,pDoc->c_markerCoil.getMeter());
		}

	}
}

/*--------------------------------------------------------
no colori alternati nelle bande ( erano bande disabilitate )
for (int i=0;i<min(pDoc->c_markerCoil.getNumSchede(),32);i++)
	{
//	if (((pDoc->c_markerDisable>>i)&0x01)==1)
//		layout.c_bandeAltColor.Add(i);
	}
*/
//-----------------------------------------
// Init vLabel 
// layout.setVFori	(pDoc->getFori());
layout.c_vLabel [0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendLVPositionDal();
// densita D
	{
	CString str;
	layout.setMaxY('D',pDoc->GetTrendDScalaY());
	layout.setMinY('D',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		layout.c_vLabel[2].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[2].setBColor(RGB(0,255,0));
	}
//
// densita C
	{
	CString str;
	layout.setMaxY('C',pDoc->GetTrendDScalaY());
	layout.setMinY('C',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		layout.c_vLabel[3].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[3].setBColor(RGB(0,255,0));
	}
//

// densita B
	{
	CString str;
	layout.setMaxY('B',pDoc->GetTrendDScalaY());
	layout.setMinY('B',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		layout.c_vLabel[4].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[4].setBColor(RGB(0,255,0));
	}
//

// densita A
	{
	CString str;
	layout.setMaxY('A',pDoc->GetTrendDScalaY());
	layout.setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		layout.c_vLabel[5].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[5].setBColor(RGB(0,255,0));
	}
	
//-----
layout.c_vLabel [6] = pDoc->getRotolo();
layout.c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
layout.c_vLabel [8] = d.Format( "%A, %B %d, %Y" );
// Visualizzazione alzata
int metriAlzata;

if(pDoc->c_rotoloMapping.c_lastElemento < 0)
	return;	

int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
layout.c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
layout.c_vLabel [10] = s;

s = pDoc->getCurAlzString();
layout.c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
layout.c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
layout.c_vLabel [13] = s;
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
layout.c_vLabel [14] = s;
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
layout.c_vLabel [15] = s;
// Total Holes A+B+C+D
s.Format("%6.0lf",va+vb+vc+vd);
layout.c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
layout.c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
layout.c_vLabel [18] = s;
// Limit C
s.Format("%4.0lf",pDoc->sogliaC);
layout.c_vLabel [19] = s;
// Limit D
s.Format("%4.0lf",pDoc->sogliaD);
layout.c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
layout.c_vLabel [21] = s;

// numero totale strip
//s.Format("%d",pDoc->c_rotoloMapping[pDoc->c_rotoloMapping.c_lastElemento].size());
//layout.c_vLabel [16] = s;


// layout.c_vectRealSizeBanda.Add(100);
// layout.c_vectRealSizeBanda.Add(200);
// layout.c_vectRealSizeBanda.Add(300);


//--------------------
int numClassi = pDoc->c_markerCoil.getNumClassi();
layout.setNumClassi(numClassi);

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// sostituire con numero marker
layout.setNumSchede(pDoc->c_markerCoil.getNumSchede());
// layout.setNumSchede(pDoc->c_difCoil.getNumSchede());

// int numClassi = pDoc->c_difCoil.getNumClassi();
// aggiungo anche classe Big
//layout.setNumClassi(numClassi+1);
//layout.setNumLabelClassi(numClassi+1);
//layout.setNumSchede(pDoc->c_difCoil.getNumSchede());
//layout.setNumCpu(pDoc->c_numCpu);

layout.draw(pDC);
layout.CalcDrawRect(rcBounds,rectB); 

// Disegno Posizione bobine
// if(pDoc->c_viewStripBorder)
	// Disegno Posizione bobine doppia riga
//	drawPosBobine1(pDC,rectB);
// else
	// Disegno Posizione bobine singola riga
	drawPosBobine1(pDC,rectB);

// if(pDoc->c_viewStripBorderPosition)
    	textPosBobine2(pDC,rectB);
	
// Disegno Posizione diaframmi
// no diaframmi
// if (pDoc->c_useDiaframmi)
//	 drawPosDiaframma(pDC,rectB);

	{
	//layout.OnEraseDrawBkgnd(pDC);
	//pDoc->c_markerBoard.setDrawAttrib((int)layout.c_sizeFBanda,
	//						(int)layout.c_sizeBanda,
//							c_pxBRoll,layout.c_bandePerCol);
	pDoc->c_markerBoard.setDrawAttrib((int)layout.c_sizeFBanda,
				c_pxBRoll,1,layout.c_vectSizeBanda,layout.c_reverseLabel?2:0,layout.c_useGrid);
	
	CRect clipRect(rectB);
	clipRect.TopLeft().x = rcBounds.TopLeft().x;
	clipRect.BottomRight().x = rcBounds.BottomRight().x;
	pDC->IntersectClipRect (clipRect);
	pDoc->c_markerBoard.draw(rectB,pDC);
	}


/*
// TODO: add draw code here
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
// TODO: add draw code here
// Call Base Class Draw
CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);
// Call Base Class Draw
//-- FC --
// qui impostare max per ogni classe
layout.setMaxY('A',pDoc->GetTrendDScalaY());
layout.setMinY('A',0.);


// init mark counter
{
int nMaxStrip = min(pDoc->c_markerCoil.getNumSchede(),7);
for (int i=0;i<nMaxStrip;i++)
	{

	// solo per test altrimenti inverto due volte!!!
//	if (((pDoc->c_stripDisable>>i)&0x01)==1)
//		layout.c_countStrip[i]	= -1;
//	else
		{
		layout.c_countStrip[i] = (int)pDoc->c_markerCoil.getTotDifetti (i,'A',0,pDoc->c_markerCoil.getMeter());
		layout.c_countStrip[i] += (int)pDoc->c_markerCoil.getTotDifetti (i,'B',0,pDoc->c_markerCoil.getMeter());
		layout.c_countStrip[i] += (int)pDoc->c_markerCoil.getTotDifetti (i,'C',0,pDoc->c_markerCoil.getMeter());
		layout.c_countStrip[i] += (int)pDoc->c_markerCoil.getTotDifetti (i,'D',0,pDoc->c_markerCoil.getMeter());
		}

	}
}

//-----------------------------------------
// Init vLabel 
// layout.setVFori	(pDoc->getFori());
layout.c_vLabel[0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendLVPositionDal();
layout.c_vLabel [2] = pDoc->GetVCliente();
layout.c_vLabel [3] = 0;		// maxValX 
layout.c_vLabel [4] = pDoc->getRotolo();
layout.c_vLabel [5] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
layout.c_vLabel [6] = d.Format( "%A, %B %d, %Y" );

// numero totale marker
int totMarker = pDoc->c_markerCoil.getTotDifetti(0,pDoc->c_markerCoil.getMeter());
CString s;
s.Format("%d",totMarker);
layout.c_vLabel[7] = s;

int numClassi = pDoc->c_markerCoil.getNumClassi();
layout.setNumClassi(numClassi);

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// sostituire con numero marker
layout.setNumSchede(pDoc->c_markerCoil.getNumSchede());

layout.c_bandeAltColor.RemoveAll();

for (int i=0;i<min(pDoc->c_markerCoil.getNumSchede(),32);i++)
	{
	if (((pDoc->c_markerDisable>>i)&0x01)==1)
		layout.c_bandeAltColor.Add(i);
	}


layout.draw(pDC);
layout.CalcDrawRect(rcBounds,rectB); 


	{
	//layout.OnEraseDrawBkgnd(pDC);
	//pDoc->c_markerBoard.setDrawAttrib((int)layout.c_sizeFBanda,
	//						(int)layout.c_sizeBanda,
	//						c_pxBRoll,layout.c_bandePerCol);
	
	
	pDoc->c_stripBoard.setDrawAttrib((int)layout.c_sizeFBanda,
				c_pxBRoll,1,layout.c_vectSizeBanda,layout.c_mirrorDefect,layout.c_useGrid);
	
	
	CRect clipRect(rectB);
	clipRect.TopLeft().x = rcBounds.TopLeft().x;
	clipRect.BottomRight().x = rcBounds.BottomRight().x;
	pDC->IntersectClipRect (clipRect);
	pDoc->c_markerBoard.draw(rectB,pDC);
	}
// c_listCtrl.MoveWindow(rectB);

// Sincronizzo contenuto 
//	c_listCtrl.UpdateWindow();
*/

}

/////////////////////////////////////////////////////////////////////////////
// CMarkerView diagnostics

#ifdef _DEBUG
void CMarkerView::AssertValid() const
{
	CView::AssertValid();
}

void CMarkerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMarkerView message handlers

void CMarkerView::OnInitialUpdate() 
{
CView::OnInitialUpdate();

CRect gRect,dRect;
GetClientRect(gRect);


DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();
 layout.c_reverseLabel = pDoc->c_rightAlignStrip;
// layout.c_mirrorDefect = pFrame->c_rightAlignStrip;

// TODO: Add your specialized code here and/or call the base class
layout.CalcDrawRect (gRect,dRect);

layout.setNumSchede(pDoc->c_markerCoil.getNumSchede());

CArray <int,int> v;

for (int i=0;i<layout.c_numSchede;i++)
	v.Add(pDoc->c_valZoneNumberSize[i]);

layout.setVectRealSizeBanda(v);


// if (!c_listCtrl.Create (WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT,dRect,this,0))
//	AfxMessageBox ("Error Create listCtrl");

// c_listCtrl.SetTextColor (RGB(0,0,0));

// c_listCtrl.image.Create(IDB_BITMAP_DIF1,16,1,RGB(255,255,255));	
// c_listCtrl.SetImageList(&c_listCtrl.image,TVSIL_NORMAL);

/*
CString sH ("POSITION");
for (int i=0;i<pDoc->c_difCoil.getNumSchede();i++)
	{
	CString s;
	s.Format (",%2d",i+1);
	sH += s;
	}
*/


CRect rcBounds,rectB;
GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rectB);
int nr = rectB.Size().cy / c_pxBRoll;
fillCtrl(nr);
}


// Torna numero di righe aggiunte
// Inserisce tutte le volte al piu` N righe dove N sono  quelle visualizzabili
// Default inserisce solo al piu` nMaxRow
int CMarkerView::fillCtrl(int nmaxRow)
{ 
int valret = 0;
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

int toIndex = pDoc->c_markerCoil.GetSize();
int fromIndex = max (toIndex - nmaxRow,0);
pDoc->c_markerBoard.setEmpty();
for (int index=fromIndex;index <toIndex;index ++)
	{
	BOOL found = FALSE;
	// cerco schede
	int mSize = pDoc->c_markerCoil.ElementAt(index).GetSize();
	for (int scIndex=0;scIndex<mSize;scIndex ++)
		{
		int clSize = pDoc->c_markerCoil.ElementAt(index).ElementAt(scIndex).GetSize();
		//for (int clIndex=0;clIndex<clSize;clIndex ++)
		// default colore difetti + grossi
		for (int clIndex=(clSize-1);clIndex>=0;clIndex--)
			{
			if (pDoc->c_markerCoil.ElementAt(index).ElementAt(scIndex).ElementAt(clIndex) > 0)
				{
				// found 
				CTarget t;
				if (pDoc->c_rightAlignStrip)
					{
					t.setBanda (pDoc->c_markerCoil.ElementAt(index).ElementAt(scIndex).banda);
					}
				else
					{
					t.setBanda (pDoc->c_markerCoil.ElementAt(index).ElementAt(scIndex).banda);
					}

				t.setSize (CSize(c_pxBRoll,c_pxBRoll));
				t.setColor (CSM20GetClassColor(clIndex));
				// t.setColor (RGB(127,255,255));
				pDoc->c_markerBoard.add(pDoc->c_markerCoil.ElementAt(index).posizione*1000,t);
				found = TRUE;
				}
			}
		}
	if (found)
		{
		int p1,p2;
		p1 = pDoc->c_markerCoil.ElementAt(index).posizione*1000;
		p2 = pDoc->c_markerBoard.getLastPos();
		if (p1 > p2)
			valret ++;
		}
	}
if (pDoc->c_markerBoard.GetSize() > 0)
	pDoc->c_markerBoard.setLastPos(pDoc->c_markerBoard.ElementAt(pDoc->c_markerBoard.GetUpperBound()).getPos());
else
	pDoc->c_markerBoard.setLastPos(0);
return (valret);
}


BOOL CMarkerView::OnEraseBkgnd(CDC* pDC) 
{
// TODO: Add your message handler code here and/or call default

CRect r;
GetClientRect(&r);

// semaforo repaint all 
// vedi OnUpdate
// layout.setExcludeBkgr(!c_repaintAll);
// layout.setExcludeBkgLV(!c_repaintAll);

layout.OnEraseBkgnd(pDC,r);

return TRUE;
}



BOOL CMarkerView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if (!CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);


return (layout.Create(this));

}



void CMarkerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
if ((lHint==0) && (pHint == NULL))
	{
	CRect rcBounds,rectB;
	GetClientRect (rcBounds);
	layout.CalcDrawRect(rcBounds,rectB);
	int nr = rectB.Size().cy / c_pxBRoll;
	int newRow = fillCtrl(nr);

	CView::OnUpdate(pSender,lHint,pHint);	
	}
else
	{
	CRect rcBounds,rectB;
	GetClientRect (rcBounds);
	layout.CalcDrawRect(rcBounds,rectB);
	int nr = rectB.Size().cy / c_pxBRoll;
	int newRow = fillCtrl(nr);

	DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

	if (newRow > 0)
		{
//		pDoc->c_markerBoard.setDrawAttrib((int)layout.c_sizeFBanda,(int)layout.c_sizeBanda,
//								c_pxBRoll,layout.c_bandePerCol);


		pDoc->c_markerBoard.setDrawAttrib((int)layout.c_sizeFBanda,
				c_pxBRoll,1,layout.c_vectSizeBanda,layout.c_reverseLabel?2:0,layout.c_useGrid);


		CRect clipRect(rectB);
		clipRect.TopLeft().x = rcBounds.TopLeft().x;
		CDC  *pDC;
		pDC = GetDC ();
		pDC->IntersectClipRect (clipRect);
		// Disegno Posizione bobine singola riga
		drawPosBobine1(pDC,rectB);
		pDoc->c_markerBoard.scroll(newRow,rectB,clipRect,pDC);
		ReleaseDC(pDC);
		}
	else
		{
		if (lHint == 2)
			{ // aggiornamento stessa riga
	//		pDoc->c_markerBoard.setDrawAttrib((int)layout.c_sizeFBanda,(int)layout.c_sizeBanda,
	//								c_pxBRoll,layout.c_bandePerCol);


			pDoc->c_markerBoard.setDrawAttrib((int)layout.c_sizeFBanda,
					c_pxBRoll,1,layout.c_vectSizeBanda,layout.c_reverseLabel?2:0,layout.c_useGrid);
			CRect clipRect(rectB);
			clipRect.TopLeft().x = rcBounds.TopLeft().x;
			CDC  *pDC;
			pDC = GetDC ();
			pDC->IntersectClipRect (clipRect);
			// Disegno Posizione bobine singola riga
			drawPosBobine1(pDC,rectB);
			pDoc->c_markerBoard.reDraw(pDoc->c_markerBoard.GetUpperBound(),rectB,pDC);
			ReleaseDC(pDC);
			}
		}
	// numero totale marker
	// int totMarker = pDoc->c_markerCoil.getTotDifetti(0,pDoc->c_markerCoil.getMeter());
	// CString s;
	// s.Format("%d",totMarker);
	// layout.c_vLabel[7] = s; // non e` 7!
	
	// init mark counter
	{
	for (int i=0;i<min(pDoc->c_markerCoil.getNumSchede(),7);i++)
		{
		int cntMark = (int)pDoc->c_markerCoil.getTotDifetti (i,'A',0,pDoc->c_markerCoil.getMeter());
		cntMark += (int)pDoc->c_markerCoil.getTotDifetti (i,'B',0,pDoc->c_markerCoil.getMeter());
		cntMark += (int)pDoc->c_markerCoil.getTotDifetti (i,'C',0,pDoc->c_markerCoil.getMeter());
		cntMark += (int)pDoc->c_markerCoil.getTotDifetti (i,'D',0,pDoc->c_markerCoil.getMeter());
		
		if (cntMark != layout.c_countStrip[i])
			{
			Invalidate(NULL);	
			}
		
		}
	layout.c_vLabel[0] = pDoc->GetVPosition();
	// Visualizzazione alzata
	int metriAlzata;
	int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
	metriAlzata = pDoc->GetVPosition() - posAlzata;
	if((pDoc->c_rotoloMapping.size() == 1)&&
		(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
		metriAlzata -= pDoc->c_lunResStop;

	CString s;
	s.Format("%d",metriAlzata);
	layout.c_vLabel [9] = s;

	// densita D
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		layout.c_vLabel[2].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[2].setBColor(RGB(0,255,0));
	}
	// densita C
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		layout.c_vLabel[3].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[3].setBColor(RGB(0,255,0));
	}
	//--------------------
	// densita B
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		layout.c_vLabel[4].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[4].setBColor(RGB(0,255,0));
	}
	// densita A
	{
	CString str;
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		layout.c_vLabel[5].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[5].setBColor(RGB(0,255,0));
	}
	//--------------------

	// Agosto 31
	// Total Holes A
	
	double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",va);
	layout.c_vLabel [12] = s;
	// Total Holes B
	double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",vb);
	layout.c_vLabel [13] = s;
	// Total Holes C
	double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",vc);
	layout.c_vLabel [14] = s;
	// Total Holes D
	double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
	s.Format("%6.0lf",vd);
	layout.c_vLabel [15] = s;
	// Total Holes A+B
	s.Format("%6.0lf",va+vb+vc+vd);
	layout.c_vLabel [16] = s;
	// Limit A
	s.Format("%4.0lf",pDoc->sogliaA);
	layout.c_vLabel [17] = s;
	// Limit B
	s.Format("%4.0lf",pDoc->sogliaB);
	layout.c_vLabel [18] = s;
	// Limit C
	s.Format("%4.0lf",pDoc->sogliaC);
	layout.c_vLabel [19] = s;
	// Limit D
	s.Format("%4.0lf",pDoc->sogliaD);
	layout.c_vLabel [20] = s;
	// Strip Density
	double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
	de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
	de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
	de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
	if (de < 0.)
		de = 0.;
	s.Format("%6.02lf",de);
	layout.c_vLabel [21] = s;
	}
	}
}

void CMarkerView::drawPosDiaframma(CDC *pDC, CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
if (!pDoc->c_useDiaframmi)
	return;



CPen pen1,pen2,pen3,pen4,*oldPen;
	// red
	
	pen1.CreatePen(PS_SOLID,1,RGB(196,0,0));
	oldPen = pDC->SelectObject(&pen1);
	double posLeft = pDoc->c_posDiaframmaSx;
	// 19-12-2013, posizione diaframmi su bande larghezza differente  
	// int scPosLeft = rectB.left + layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	int scPosLeft = rectB.left + layout.c_sizeFBanda + (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

	pDC->MoveTo(scPosLeft,rectB.top);
	pDC->LineTo(scPosLeft,rectB.bottom);

	pen2.CreatePen(PS_SOLID,1,RGB(196,0,0));
	pDC->SelectObject(&pen2);
	double posRight = pDoc->c_posDiaframmaDx;
	// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
	int scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
	pDC->MoveTo(scPosRight,rectB.top);
	pDC->LineTo(scPosRight,rectB.bottom);
	
	if (pDoc->c_mode == 1)
		{// disegno diaframmi testa 2 merge SingleCoil
		pen3.CreatePen(PS_SOLID,1,RGB(196,0,0));
		pDC->SelectObject(&pen3);
		double posLeft = pDoc->c_posDiaframmaSx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		int scPosLeft = rectB.left +layout.c_sizeFBanda+ (1.  * posLeft * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));

		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);

		pen4.CreatePen(PS_SOLID,1,RGB(196,0,0));
		pDC->SelectObject(&pen4);
		double posRight = pDoc->c_posDiaframmaDx2;
		// 15-09-2004 corretto centrato +layout.c_sizeFBanda 
		int scPosRight = rectB.left +layout.c_sizeFBanda + (1.  * posRight * (double)layout.c_sizeBanda / (layout.c_bandePerCol*SIZE_BANDE));
	
		pDC->MoveTo(scPosRight,rectB.top);
		pDC->LineTo(scPosRight,rectB.bottom);
		
		pDC->SelectObject(oldPen);
		pen3.DeleteObject();
		pen4.DeleteObject();
		}
	// non importa anche se seleziono la stessa pen due volte
	pDC->SelectObject(oldPen);
	pen1.DeleteObject();
	pen2.DeleteObject();
	// Fine disegno pos. Diaframma
	//---------------------------------------------------------


}



void CMarkerView::drawPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
int scPosLeft;

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,2,RGB(196,196,196));
	// prima riga sinistra
	oldPen = pDC->SelectObject(&pen0);
	
	scPosLeft = rectB.left + layout.c_sizeFBanda;
	for (int i=0;i<layout.c_vectSizeBanda.GetSize();i++)
		{ 	
		scPosLeft += layout.c_vectSizeBanda.ElementAt(i);
		pDC->MoveTo(scPosLeft,rectB.top);
		pDC->LineTo(scPosLeft,rectB.bottom);
		}

	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
}



// visualizzazione posizione bobine per utente
void CMarkerView::drawPosBobine1(CDC *pDC,CRect rectB)
{

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

int scPosLeft;

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx
	CPen pen0,*oldPen;
	// light grey
	//pen0.CreatePen(PS_DASH,1,RGB(196,196,196));
	pen0.CreatePen(PS_SOLID,2,RGB(196,196,196));
	oldPen = pDC->SelectObject(&pen0);
	
	CMainFrame* pFrame;
	pFrame = (CMainFrame*) AfxGetMainWnd();
	//layout.c_reverseLabel = pFrame->c_rightAlignStrip;

	if (pDoc->c_rightAlignStrip)
		{
		scPosLeft = rectB.left + layout.c_sizeFBanda;
		//for (int i=layout.c_vectSizeBanda.GetSize()-1;i>=0;i--)
		for (int i=0;i<layout.c_vectSizeBanda.GetSize();i++)
			{
			// prima riga sinistra
			scPosLeft += layout.c_vectSizeBanda.ElementAt(i);
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		}
	else
		{
		scPosLeft = rectB.left + layout.c_sizeFBanda;
		for (int i=0;i<layout.c_vectSizeBanda.GetSize();i++)
			{
			// prima riga sinistra
			scPosLeft += layout.c_vectSizeBanda.ElementAt(i);
			pDC->MoveTo(scPosLeft,rectB.top);
			pDC->LineTo(scPosLeft,rectB.bottom);
			}
		}
	pDC->SelectObject(oldPen);
	pen0.DeleteObject();
	// Fine disegno pos. bobine
	//---------------------------------------------------------
}


void CMarkerView::textPosBobine2(CDC *pDC, CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();
return;

CMainFrame* pFrame;
pFrame = (CMainFrame*) AfxGetMainWnd();

//---------------------------------------------------------
// Disegno Posizione bobine allineate a sx	
	CFont font,*oldFont; 
	font.CreatePointFont (90,"Arial",pDC);
	oldFont = pDC->SelectObject(&font);
	pDC->SetBkMode (TRANSPARENT);

	int scPosLeft = rectB.left + layout.c_sizeFBanda;
	for (int i=0;i<layout.c_vectSizeBanda.GetSize();i++)
		{
		// prima riga sinistra
		
		// solo 1/2 larghezza
			scPosLeft += layout.c_vectSizeBanda.ElementAt(i)/2;
		if (i > 0)	
			{// aggiungo anche meta` banda precedente
			scPosLeft += layout.c_vectSizeBanda.ElementAt(i-1)/2;
			}

		// riferimenti numerici
		CString sl;
		pDC->SetTextAlign(TA_LEFT | TA_BOTTOM);
		// reverse header
		if (pDoc->c_rightAlignStrip)
			sl.Format("%d",layout.c_vectSizeBanda.GetSize()-i);
		else
			sl.Format("%d",i);
		pDC->TextOut(scPosLeft,rectB.top,(LPCSTR)sl);	
		}

pDC->SelectObject(oldFont);
font.DeleteObject();

	// Fine disegno pos. bobine
	//---------------------------------------------------------

}
