//-------------------------------------------------------------------------
//
//
//			Layout.cpp
//
//

#include "stdafx.h"
#include "FontInfo.h"
#include "GraphDoc.h"

#include "Layout.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

Layout::Layout(void)
{
sCaption = "SCONOSCIUTO";

sLineName = "SCONOSCIUTO";
sMothercoil = "SCONOSCIUTO";
sDatetime = "SCONOSCIUTO";

fCaption.name = "Arial";
fLabel.name = "Arial";
fNormal.name = "Arial";

fCaption.size = 120;
fLabel.size = 100;
fNormal.size = 110;

int i;
for (i=0;i<MAX_NUMLABEL;i++)
	{
	c_sLabel[i] = "SCONOSCIUTO";
	c_vLabel[i] = "SCONOSCIUTO";
	c_nlAfterLabel [i] = -1;
	c_vOrderLabel [i] = -1;
	c_pxSepLabel [i] = 10; // 10 pixel separazione
	}

for (i=0;i<MAX_NUMCLASSI;i++)
	{
	c_cLabel[i] = "SCONOSCIUTO";
	}

c_pxSepLabelEdit = 10;

for (i=0;i<sizeof(maxValY)/sizeof(double);i++)
	{
	maxValY[i] = 100.;
	minValY[i] = 0.;
	c_alValY[i] = DBL_MIN;
	}

c_numSchede = 7;
// da a
c_indexFirstBanda = -1;
c_indexLastBanda = -1;
//--
viewType = UNKVIEW;

mode = LINEAR;
// Se True non aggiorna il background della parte interna
// della finestra di disegno
excludeUpdateBackGr = FALSE;
excludeUpdateBkgLV = FALSE;

// vettore contenente le label dopo le quali andare a capo

rgbBackColor = RGB(192,192,192);
rgbDrawBackColor = RGB(192,192,192);
rgbAltDrawBackColor = RGB (128,128,128);

// visualizzazione label da destra a sinistra
c_mirrorLabel=FALSE;
// visualizzazione difetti da dx a sx
c_mirrorDefect=c_mirrorLabel;
//
c_reverseLabel = FALSE;

// fattore molt. per label x 
// serve per vis pos mm invece di banda
c_xFactor = 1;

c_sizeFBanda = 1;
// larghezza bande
c_sizeBanda = 1;
// bande per colonna
c_bandePerCol =1;
c_numClassi = 4;

c_numCpu = 1;
c_numLabelClassi = 1;

// visualizzazione griglia
c_useGrid=FALSE;

// disegna label a partire da primaClasse 0=A 1=B fino a primaClasse+numClassi
c_primaClasse = 0;

c_mirrorDefect = FALSE;
// contatori di marker
for (i=0;i<MAX_NUMSTRIP;i++)
	c_countStrip[i] = -1;

c_useClassicPallini = TRUE;
// default logo elval
c_dibLogoId = IDB_LOGO_ELVAL;
}

Layout& Layout::operator=(Layout& source)
{
sCaption = source.sCaption;
sLineName = source.sLineName;
sMothercoil = source.sMothercoil;
sDatetime  = source.sDatetime;
fCaption = source.fCaption;
fLabel = source.fLabel;
fNormal = source.fNormal;
c_indexClassi = source.c_indexClassi;


for (int i=0;i<MAX_NUMLABEL;i++)
	{
	c_sLabel[i] = source.c_sLabel[i];
	c_vLabel[i] = source.c_vLabel[i];
	c_nlAfterLabel [i] = source.c_nlAfterLabel[i];
	c_vOrderLabel [i] = source.c_vOrderLabel[i];
	c_pxSepLabel [i] = source.c_pxSepLabel [i]; // 10 pixel separazione
	}

for (int i=0;i<MAX_NUMCLASSI;i++)
	{
	c_cLabel[i] = source.c_cLabel[i];
	}


sPosition = source.sPosition;
sPositionDal = source.sPositionDal;
sCliente = source.sCliente;
sDimX = source.sDimX;
sProduct = source.sProduct;
sFori = source.sFori;
sAlarm = source.sAlarm;
sRicetta = source.sRicetta;
sRotolo = source.sRotolo; 

// visualizzazione label da destra a sinistra
c_mirrorLabel=source.c_mirrorLabel;
// visualizzazione difetti da dx a sx
c_mirrorDefect=source.c_mirrorDefect;

// inversione solo etichette
c_reverseLabel = source.c_reverseLabel;

c_misUnitX = source.c_misUnitX;
c_misUnitY = source.c_misUnitY;
c_xFactor = source.c_xFactor;

vPosition = source.vPosition;
vPositionDal = source.vPositionDal;
vCliente = source.vCliente;
vDimX = source.vDimX;
vProduct = source.vProduct;
vFori = source.vFori;
vAlarm = source.vAlarm;
vRicetta = source.vRicetta; 
vRotolo  = source.vRotolo; 

for (int i=0;i<sizeof(maxValY)/sizeof(double);i++)
	{
	maxValY[i] = source.maxValY[i];
	minValY[i] = source.minValY[i];
	c_alValY[i] = source.c_alValY[i];
	}
	
c_numSchede = source.c_numSchede;

viewType = source.viewType;

mode = source.mode;
// Se True non aggiorna il background della parte interna
// della finestra di disegno
excludeUpdateBackGr = source.excludeUpdateBackGr;
excludeUpdateBkgLV = source.excludeUpdateBkgLV;

// vettore contenente le label dopo le quali andare a capo

rgbBackColor = source.rgbBackColor;
rgbDrawBackColor = source.rgbDrawBackColor;

c_sizeFBanda = source.c_sizeFBanda;
// larghezza bande
c_sizeBanda = source.c_sizeBanda;
// bande per colonna
c_bandePerCol = source.c_bandePerCol;
c_numClassi = source.c_numClassi;
c_numLabelClassi = source.c_numLabelClassi;

c_viewTopPerc = source.c_viewTopPerc;
c_viewBottomPerc = source.c_viewBottomPerc;
c_viewLeftPerc = source.c_viewLeftPerc;
c_viewRightPerc = source.c_viewRightPerc;

for (int i = 0;i<20;i++)
	c_stringLabelH[i] = source.c_stringLabelH[i];
for (int i = 0;i<20;i++)
	c_stringLabelV[i] = source.c_stringLabelV[i];

return *this;
}



//	1998
//	I campi inizializzati a SCONOSCIUTO 
//	non vengono visualizzati

BOOL Layout::Create(CWnd* pParentWnd)
{

// static
if (!sCaption.Create(pParentWnd,S_CAPTION))
	return FALSE;
sCaption.setFontInfo(fCaption);
sCaption.set3Dlook(TRUE);
sCaption.setSensDbclk(TRUE);


for (int i=0;i<MAX_NUMLABEL;i++)
	{
	if (!c_sLabel[i].Create(pParentWnd,i*2))
		return FALSE;
	c_sLabel[i].setFontInfo(fNormal);
	
	if (!c_vLabel[i].Create(pParentWnd,i*2+1))
		return FALSE;
	c_vLabel[i].setFontInfo(fNormal);
	c_vLabel[i].set3Dlook(TRUE);
	c_vLabel[i].setStringBase("999999");
	}

// label delle classi
for (int i=0;i<MAX_NUMCLASSI;i++)
	{
	if (!c_cLabel[i].Create(pParentWnd,100+i))
		return FALSE;
	c_cLabel[i].setFontInfo(fLabel);
	c_cLabel[i].set3Dlook(FALSE);
	c_cLabel[i].setStringBase("9");
	}

// static
if (!sLineName.Create(pParentWnd,200))
	return FALSE;
if (!sMothercoil.Create(pParentWnd,201))
	return FALSE;
if (!sDatetime.Create(pParentWnd,202))
	return FALSE;

return TRUE;
}


void Layout::draw(CDC* pdc)
{

pdc->SaveDC();

CRect rectB;
CRect yRectLabel;
int numStepY,numLabel;

TEXTMETRIC tm;

CFont fontCaption,fontLabel,fontNormal;
fontCaption.CreatePointFont (fCaption.size,fCaption.name,pdc);
fontLabel.CreatePointFont (fLabel.size,fLabel.name,pdc);
fontNormal.CreatePointFont (fNormal.size,fNormal.name,pdc);
pdc->SelectObject(&fontLabel); 


CPen penGrid,*oldPen;
penGrid.CreatePen(PS_SOLID,2,RGB(128,128,128));

// Trovo Finestra disegno (grigia)
CalcDrawRect (rcBounds,rectB);

// Limiti Finestra Label
yRectLabel.left = rcBounds.left;
yRectLabel.right = rectB.left;
yRectLabel.top	= rcBounds.top;
yRectLabel.bottom = rcBounds.bottom;

// display logo bitmap 
if (pdc->IsPrinting())
	{
	CBitmap	bmpLogo;
	// Load the bitmap from the resource
	bmpLogo.LoadBitmap(c_dibLogoId);
	if (bmpLogo.m_hObject == NULL)
		return;

	CDC MemDCLogo;

	// Create a memory device compatible with the above CPaintDC variable
	MemDCLogo.CreateCompatibleDC(pdc);
	// Select the new bitmap
	CBitmap *BmpPrevious = MemDCLogo.SelectObject(&bmpLogo);
	
	// Copy the bits from the memory DC into the current dc
	// real size 170x40
	BITMAP bm;
    bmpLogo.GetObject(sizeof(BITMAP),&bm);
	CSize sizeResourceDib(bm.bmWidth,bm.bmHeight);
	if (pdc->IsPrinting())
		{
		sizeResourceDib.cx *= 5;
		sizeResourceDib.cy *= 5;
		}
	// top
	CPoint plt;
	plt.x = rcBounds.left + 10;
	plt.y = rcBounds.top  + 20;
 	CPoint prt;
	prt.x = rectB.right - 20;
	prt.y = rcBounds.top  + 20;
	// bottom
	CPoint plb;
	plb.x = rcBounds.left + 10;
	plb.y = rcBounds.bottom  - 20 - sizeResourceDib.cy;
	CPoint prb;
	prb.x = rcBounds.right - 10 - sizeResourceDib.cx;
	prb.y = rcBounds.bottom  - 20 - sizeResourceDib.cy;

	if (c_dibLogoId != IDB_LOGO_EMPTY)
		pdc->StretchBlt(plt.x,plt.y,sizeResourceDib.cx,sizeResourceDib.cy,&MemDCLogo,0,0,170,40,SRCCOPY);
	//pdc->StretchBlt(prt.x,prt.y,sizeResourceDib.cx,sizeResourceDib.cy,&MemDCLogo,0,0,170,40,SRCCOPY);
	//pdc->StretchBlt(plb.x,plb.y,sizeResourceDib.cx,sizeResourceDib.cy,&MemDCLogo,0,0,170,40,SRCCOPY);
	//pdc->StretchBlt(prb.x,prb.y,sizeResourceDib.cx,sizeResourceDib.cy,&MemDCLogo,0,0,170,40,SRCCOPY);

	// Restore the old bitmap
	pdc->SelectObject(BmpPrevious);

	// print extra info at top page
	pdc->GetTextMetrics(&tm);
	prt.x -= tm.tmAveCharWidth*20;	// data ora 15 char
	if (sLineName.getString() != _T("SCONOSCIUTO"))
		{
		sLineName.setPos(prt,pdc);
		sLineName.setVisible(TRUE);
		prt.y += (int)(1.5*tm.tmHeight);
		}
	if (sMothercoil.getString() != _T("SCONOSCIUTO"))
		{
		sMothercoil.setPos(prt,pdc);
		sMothercoil.setVisible(TRUE);
		prt.y += (int)(1.5*tm.tmHeight);
		}
	if (sDatetime.getString() != _T("SCONOSCIUTO"))
		{
		sDatetime.setPos(prt,pdc);
		sDatetime.setVisible(TRUE);
		prt.y += (int)(1.5*tm.tmHeight);
		}		
	}




//-------------------------------------
// 3D Look
// 
//------------------------------------------
if (!(mode & ONLY_TEXT_LABEL))
	{
	Draw3DRect (pdc,rectB);

	// Ini Text Mode
	pdc->GetTextMetrics(&tm);
	pdc->SetTextAlign(TA_RIGHT | TA_TOP);
	pdc->SetBkMode (TRANSPARENT);

	// Scala verticale
	// NumStepY Parametrizzato alla dim Finestra
	CRect subRectB [MAX_NUMCLASSI];
	subRectB[0]=rectB;
	if (mode & MSCALEV)
		{// caso multiscala verticale
		for (int i=0;i<c_numClassi;i++)
			{
			subRectB[i]=rectB;
			subRectB[i].TopLeft().y += (rectB.Size().cy/c_numClassi)*i;
			subRectB[i].BottomRight().y -= (rectB.Size().cy/c_numClassi)*(c_numClassi-(i+1));
			if ((c_numClassi > 1)&&(i>0))
				// disegno asse intermedio
				{
				CPen pen1,*oldPen;
				pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));	
				oldPen = pdc->SelectObject(&pen1);
				pdc->MoveTo(subRectB[i].left,subRectB[i].TopLeft().y);
				pdc->LineTo(subRectB[i].right,subRectB[i].TopLeft().y);		
				pdc->SelectObject(oldPen);
				pen1.DeleteObject();	
				}
			}
		}

	// SubRect tutti uguali
	numStepY = abs(subRectB[0].top - subRectB[0].bottom) / (tm.tmHeight + 10);
	ASSERT (numStepY >= 0);

	numLabel = 10;
	if ((numStepY < 10) && (numStepY > 5)) numLabel = 5;
	if ((numStepY <= 5) && (numStepY > 2)) numLabel = 2;
	if (numStepY == 2) numLabel = 1;
	if (numStepY < 2) numLabel = 0;


	CString	str;
	double label;
	int numL[4];
	double MY[4],mY[4];
	double deltaY;

	if (mode & NOLABELV)
		{
		numL[0] = 0;
		numL[1] = 0;
		numL[2] = 0;
		numL[3] = 0;
		}
	else
		{
		int j = 0;
		do 	{
			if (mode & LOGARITHM)
				{
				MY[j] = pow (10,log10(maxValY[j]));
				mY[j] = pow (10,log10(minValY[j]));
				numL[j] = (int ) ((log10(MY[j])) - log10(mY[j]));
				if (numL[j] <= 0)
					numL[j] = 1;
				c_deltaY[j] = MY[j]-mY[j];
				if (c_deltaY[j] == 0.)
					c_deltaY[j] = DBL_MIN;
				}
			else
				{// LINEAR
				c_deltaY[j] = maxValY[j]-minValY[j];
				if (c_deltaY[j] == 0.)
					c_deltaY[j] = DBL_MIN;
				numL[j] = numLabel;
				}
			}
		// fixBug era j++ -> indice arriva == numClassi
		while ((mode & MSCALEV) && ((++j)<c_numClassi));
		}

	if ((numL[0] > 0)||
		(numL[1] > 0)||
		(numL[2] > 0)||	
		(numL[3] > 0))	
		{
		int j = 0;
		do 	{
			for (int i = 0;i<=numL[j];i++)
				{
				// eliminata ultima label nei grafici intermedi
				if ((j != 0)&&(i==0))
					continue;
				//------------
				int ly,lx;
				if (mode & LOGARITHM)
					label = (pow (10,(double)numL[j]-i)) * mY[j];
				else
					label = maxValY[j] - (c_deltaY[j]/numL[j] * i); 
				if ((label < 10.)&&(label > 0.))
					str.Format("%4.2lf",label);
				else
					str.Format("%4.0lf",label);
				lx = subRectB[j].left -10;
				ly = (((subRectB[j].bottom -subRectB[j].top) / (numL[j]))*i)
					+ subRectB[j].top; // - tm.tmHeight/2;	

				pdc->SetTextAlign(TA_RIGHT | TA_TOP);

				// prima label in basso un po' + in alto
				if (i==numL[j])
					{
					CSize sz;
					sz = pdc->GetOutputTextExtent(str,str.GetLength());
					ly -= sz.cy/2;
					// pdc->SetTextAlign(TA_RIGHT | TA_BOTTOM);
					}

				// sostituita txtOut Clipped con normale!! 
				//pdc->ExtTextOut (lx,ly,ETO_CLIPPED,
				//		 yRectLabel,str,str.GetLength(),NULL);
				// pdc->LineTo((int)lx+10,(int)ly);		
				pdc->MoveTo(lx,ly);
				pdc->TextOut(lx,ly,str);
				}
			}
		// fixBug era j++ -> indice arriva == numClassi
		while ((mode & MSCALEV) && ((++j)<c_numClassi));
		}

// disegno soglia allarme
	double ly;
	int j=0;
	do 
		{
		if(c_alValY[j] != DBL_MIN)
			{
			ly = 1.* subRectB[j].bottom - ((subRectB[j].bottom -subRectB[j].top)*c_alValY[j]/c_deltaY[j]) ; // - tm.tmHeight/2;	
			CPen pen1,*oldPen;
			pen1.CreatePen(PS_DASH,1,RGB(255,0,0));	
			oldPen = pdc->SelectObject(&pen1);
			if (ly < subRectB[j].top) ly = subRectB[j].top;
			if (ly > subRectB[j].bottom) ly = subRectB[j].bottom;

			pdc->MoveTo(subRectB[j].left,(int)ly);
			pdc->LineTo(subRectB[j].right,(int)ly);		
			pdc->SelectObject(oldPen);
			pen1.DeleteObject();	
			}
		}
		while ((mode & MSCALEV) && ((++j)<c_numClassi));

//------------------------------------------------
// Scala Orizzontale
	// c_bandePerCol; 
	int larghezza = rectB.Width();
	// c_sizeBanda;
	// c_sizeFBanda;

// uso normale senza vectReal
	if (c_vectRealSizeBanda.GetSize() == 0)
		{// posizioniamo label orizzontali
		int numCol;
		numCol = getNumBande();
			
		if (numCol <= 0)
		numCol = 1;

		CString baseString;
		baseString = " 000 ";

		CSize sz;
		sz = pdc->GetOutputTextExtent(baseString,baseString.GetLength());
		sz.cx *= 2;
		if (!(mode & NOLABELH))
			{
			// Iniziamo con suddivisione in funzione dell'Hw
			//c_bandePerCol = c_numSchede / c_numCpu;
			c_bandePerCol =5 ;
				c_sizeBanda = (int) ceil(1.0 * c_bandePerCol * larghezza / ( 1.0 * numCol));
			
			if (c_sizeBanda < sz.cx)
				{
				//bandePerCol = (int ) ((double)sz.cx*numCol/larghezza);
				c_bandePerCol = (int) ceil(1.0 * sz.cx * numCol / larghezza);
				switch(c_bandePerCol)
					{
					case 3:
					case 4:
					case 5:
						c_bandePerCol = 5;
						break;
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
						c_bandePerCol = 10;
						break;
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
					c_bandePerCol = 15;
					break;
				case 16:
				case 17:
				case 18:
				case 19:
					c_bandePerCol = 20;
					break;
					}
				c_sizeBanda = c_bandePerCol * larghezza / numCol;
				}
			if (c_bandePerCol <= 0)
				c_bandePerCol=1;
			
			
			

			// sizebanda deve essere divisibile per c_bandePerCol
			// es: 5 bande per colonna -> sizebanda x*5 perche` intero
			c_sizeBanda /= c_bandePerCol;
			c_sizeBanda *= c_bandePerCol;

			// 15-09-2004
			// corretto problema dei resti sul calcolo di posizione delle label e della bande
			int numBandeToDraw = (int)ceil((double)numCol/c_bandePerCol);
			if (numBandeToDraw <= 0)
				numBandeToDraw=1;

			//c_bandeLastCol = (8 * c_numCpu) - getNumBande();
			//c_bandeLastCol = c_bandePerCol*numBandeToDraw - getNumBande();
			//if (c_bandeLastCol != 0)
			c_bandeLastCol=c_bandePerCol;
			// FixBug 29-07-09 a seconda della risoluzione stampante
			// disegna fuori dello spazio assegnato
			while((rectB.Size().cx - c_sizeBanda*numBandeToDraw)<0)
				{
				c_sizeBanda--;
				}


			double dPosX;
			int posX;
			if (c_mirrorLabel)
				dPosX = rectB.BottomRight().x - LEFTCUTTER_REFERENCE; 
			else
				dPosX = rectB.TopLeft().x + LEFTCUTTER_REFERENCE; 
		
			// CString label;

			// Centrato rispetto alla larghezza
			if (c_mirrorLabel)
				dPosX -= ((1. * rectB.Size().cx - c_sizeBanda*numBandeToDraw) / 2.);
			else
				dPosX += ((1. * rectB.Size().cx - c_sizeBanda*numBandeToDraw) / 2.);
			// oppure
			// allineato a sx

			// larghezza ultima banda
			c_sizeLBanda = 1. * c_sizeBanda/c_bandePerCol *  c_bandeLastCol;

			// larghezza prima banda
			if (c_mirrorLabel)
				c_sizeFBanda = rectB.BottomRight().x - (int)dPosX;
			else
				c_sizeFBanda = (int)dPosX - rectB.TopLeft().x ;

			for (int i=0;i<=numBandeToDraw;i++)
				{
				if (c_bandePerCol > 1)
					{// Sulla riga divisione
					//if (mode == ROLLMAP)
					// + di una banda per colonna
					// disegno banda 1 e poi tutte le altre 
					if (i < numBandeToDraw)
						{
						if (c_mirrorLabel)
							{
							posX = (int)(dPosX - (double)c_sizeBanda * i);
							}
						else
							{
							posX = (int)(dPosX + (double)c_sizeBanda * i);
							}
						}
						// posX = (int)(dPosX + (double)c_sizeBanda * i);
					else
						{
						if (c_mirrorLabel)
							{
							posX = (int)(dPosX - ((double)c_sizeBanda * (i-1) + (double)c_sizeLBanda));
							}
						else
							{
							posX = (int)(dPosX + (double)c_sizeBanda * (i-1) + (double)c_sizeLBanda);
							}
						}
						// posX = (int)(dPosX + (double)c_sizeBanda * (i-1) + (double)c_sizeLBanda);
					{
					// pdc->MoveTo(posX + i*c_sizeBanda,rectB.TopLeft().y);
					pdc->MoveTo(posX,rectB.BottomRight().y);
					pdc->LineTo(posX,rectB.BottomRight().y+10);
					}

				int iLabel;
				if (i < numBandeToDraw)
					iLabel = c_bandePerCol*i;
				else
					iLabel = c_bandePerCol*(i-1)+c_bandeLastCol;
				// use firstBanda?
				if (c_indexFirstBanda >= 0)
					iLabel += c_indexFirstBanda;

				if (i == 0)
					{
					iLabel ++;
					if (c_mirrorLabel)
						pdc->SetTextAlign(TA_RIGHT);
					else
						pdc->SetTextAlign(TA_LEFT);
					}
				else
					{
					if (iLabel == numBandeToDraw)
						{
						if (c_mirrorLabel)
							pdc->SetTextAlign(TA_LEFT);
						else
							pdc->SetTextAlign(TA_RIGHT);
						}
					else
						pdc->SetTextAlign(TA_CENTER);
					}
				if (iLabel < 0) 
					continue;
				str.Format("%d ",iLabel);
				// aggiungo unita` mis
				if ((c_misUnitX != "")
					&&(i == numBandeToDraw))
					{
					pdc->SetTextAlign(TA_LEFT);
					str += " ";
					str += c_misUnitX;
					}
				if (mode & USE_STRIPRULER)
					pdc->TextOut(posX+1,rectB.BottomRight().y,str);			

				}
			else
				{
				if (i<numBandeToDraw)
					{
					int iLabel = c_bandePerCol*(i+1);
					if (c_indexFirstBanda >= 0)
						iLabel += c_indexFirstBanda;

					// Centrato sulla colonna
					if (c_mirrorLabel)// era +
						posX  = (int)(dPosX - (double)c_sizeBanda*i);
					else
						posX  = (int)(dPosX + (double)c_sizeBanda*i);
					pdc->MoveTo(posX,rectB.BottomRight().y);
					pdc->LineTo(posX,rectB.BottomRight().y+10);
					
					if (iLabel < 0)
						continue;
					str.Format("%d",iLabel);

					pdc->SetTextAlign(TA_CENTER);
					int posX2;
					if (c_mirrorLabel)
						posX2 = (int)(dPosX - (i+1)*c_sizeBanda-(c_sizeBanda/2.));
					else
						posX2 = (int)(dPosX + (i+1)*c_sizeBanda-(c_sizeBanda/2.));
					if (mode & USE_STRIPRULER)
						pdc->TextOut(posX2,rectB.BottomRight().y,str);
					}
					
				else
					{
					if (i<=numBandeToDraw)
						{
						int iLabel = c_bandePerCol*(i+1);
						if (c_indexFirstBanda >= 0)
							iLabel += c_indexFirstBanda;
							
						// Centrato sulla colonna
						posX  = (int)(dPosX + (double)c_sizeBanda*i);
						pdc->MoveTo(posX,rectB.BottomRight().y);
						pdc->LineTo(posX,rectB.BottomRight().y+10);
					
						// grid
						if (c_useGrid)
							{
							oldPen = pdc->SelectObject(&penGrid); 
							pdc->MoveTo(posX,rectB.TopLeft().y);
							pdc->LineTo(posX,rectB.BottomRight().y);
							pdc->SelectObject(oldPen); 
							}

						if (c_mirrorLabel)
							iLabel = 1 + numCol - iLabel;
						if (c_indexFirstBanda >= 0)
							iLabel += c_indexFirstBanda;
						str.Format("%d",iLabel);
						pdc->SetTextAlign(TA_CENTER);
						int posX2 = (int)(dPosX + (i+1)*c_sizeBanda-(c_sizeBanda/2.));
						if (mode & USE_STRIPRULER)
							pdc->TextOut(posX2,rectB.BottomRight().y,str);
						}
					}
				}
			}
		}
		}
	else
		//------------------------------------------------------------
		// Caso con bande tutte differenti
		{ // !!si gestisce solo 1 banda per colonna!!!!
		//
		int numCol;
		numCol = getNumBande();
		CString baseString;
		baseString = "000 ";

		CSize sz;
		sz = pdc->GetOutputTextExtent(baseString,baseString.GetLength());

		if (!(mode & NOLABELH))
			{
			c_vectSizeBanda.RemoveAll();
			double totalRealSize=0.;
			double totalPixelSize=0.;
			for (int i=0;i<c_vectRealSizeBanda.GetSize();i++)
				{
				totalRealSize += c_vectRealSizeBanda[i];
				}
			if (totalRealSize == 0.)
				totalRealSize = 1.;

			for (int i=0;i<c_vectRealSizeBanda.GetSize();i++)
				{
				double vpx = (((double)rectB.Size().cx/totalRealSize)*c_vectRealSizeBanda[i]);
				totalPixelSize += vpx;
				for(int k=0;k<c_bandeAltColor.GetSize();k++)
					{
					if (c_bandeAltColor[k] == i)
						{
						vpx = -vpx;
						break;
						}
					}
				c_vectSizeBanda.Add ((int)vpx);
				}

			// qui le bande sono tutte differenti quindi calcolo la dimensione media delle bande 
			// come totalPixelSize / numeroBande
			c_sizeBanda = (int) ceil(1.0 * totalPixelSize / c_vectRealSizeBanda.GetSize());
				
			if (c_mirrorLabel)
				{
				CRect r;
				// rettanglo disegno colore alternativo
				r = rectB;
				double dPosX = rectB.BottomRight().x;

				// CString label;
				// Centrato rispetto alla larghezza
				dPosX -= (1. * rectB.Size().cx - totalPixelSize) / 2.;

				// larghezza prima banda
				c_sizeFBanda = rectB.BottomRight().x - (int)dPosX;

				int posX = (int)dPosX;
				for (int i=0;i<c_vectSizeBanda.GetSize();i++)
					{
					int iLabel = (i+1);
					if (c_indexFirstBanda >= 0)
						iLabel += c_indexFirstBanda;
					posX  -= abs(c_vectSizeBanda[i]);
					// separatore 
					pdc->MoveTo(posX,rectB.BottomRight().y);
					pdc->LineTo(posX,rectB.BottomRight().y+10);
			
					// grid
					if (c_useGrid)
						{
						oldPen = pdc->SelectObject(&penGrid); 
						pdc->MoveTo(posX,rectB.TopLeft().y);
						pdc->LineTo(posX,rectB.BottomRight().y);
						pdc->SelectObject(oldPen); 
						}
				
					int countIndex = i;
					if (!c_mirrorDefect)
						{
						countIndex -= c_vectSizeBanda.GetSize(); 
						countIndex ++;
						if (countIndex < 0) countIndex = -countIndex;
						}
					// colore sfondo bande alternativo
					if (i > 0)
						r.right = r.left;
					r.left = posX;
					if (c_vectSizeBanda[countIndex]<0)
						drawAltBackground(pdc,r);

					// etichetta Centrato sulla colonna
			
					iLabel =  iLabel;
					str.Format("%d",iLabel);
					pdc->SetTextAlign(TA_CENTER);
					// mirror!
					int posX2 = (int)(posX + abs(c_vectSizeBanda[c_vectSizeBanda.GetSize()-i-1])/2);
					// pdc->TextOut(posX2,rectB.BottomRight().y,str);
					// spostato in alto
					if (mode & USE_STRIPRULER)
						pdc->TextOut(posX2,rectB.TopLeft().y - sz.cy,str);
					if (c_countStrip[countIndex] >= 0)
						{
						COLORREF oldColorText;
						oldColorText = pdc->GetTextColor();
						pdc->SetBkMode (OPAQUE);
						pdc->SetBkColor (RGB(196,196,196));
					
						if (c_countStrip[countIndex] == 0)
							pdc->SetTextColor(RGB(0,255,0));
						else
							pdc->SetTextColor(RGB(255,0,0));
						// era "M = %d"
						str.Format("%d",c_countStrip[countIndex]);
						if (mode & USE_STRIPRULER)
							pdc->TextOut(posX2,rectB.BottomRight().y+5,str);					
						pdc->SetTextColor(oldColorText);
						pdc->SetBkMode (TRANSPARENT);
						}
					}
				}
			else
				{
				CRect r;
				// rettanglo disegno colore alternativo
				r = rectB;
				// etichette da sx
				double dPosX = rectB.TopLeft().x; 		
				// CString label;
				// Centrato rispetto alla larghezza
				dPosX += (1. * rectB.Size().cx - totalPixelSize) / 2.;

				// larghezza prima banda
				c_sizeFBanda = (int)dPosX - rectB.TopLeft().x;

				int posX = (int)dPosX;
//				for (int i=(c_reverseLabel)?c_vectSizeBanda.GetSize()-1:0;
//					(c_reverseLabel)?(i>=0):(i<c_vectSizeBanda.GetSize());
//					(c_reverseLabel)?i--:i++)
				for (int i=0;i<c_vectSizeBanda.GetSize();i++)
					{
					int iLabel = (i+1);
					if (c_reverseLabel)
						iLabel = c_vectSizeBanda.GetSize()-i;
					
					posX  += abs(c_vectSizeBanda[i]);
					// separatore 
					pdc->MoveTo(posX,rectB.BottomRight().y);
					pdc->LineTo(posX,rectB.BottomRight().y+10);
				
					// grid
					if (c_useGrid)
						{
						oldPen = pdc->SelectObject(&penGrid); 
						pdc->MoveTo(posX,rectB.TopLeft().y);
						pdc->LineTo(posX,rectB.BottomRight().y);
						pdc->SelectObject(oldPen); 
						}

					// colore sfondo bande alternativo
					int countIndex = i;
/*					if (!c_mirrorDefect)
						{
						countIndex -= c_vectSizeBanda.GetSize();
						countIndex ++;
						if (countIndex < 0) countIndex = -countIndex;
						}
*/
					if (i > 0)
						r.left = r.right;
					r.right = posX;
					if (c_vectSizeBanda[countIndex]<0)
						drawAltBackground(pdc,r);
					// etichetta Centrato sulla colonna
			
					iLabel =  iLabel;
						str.Format("%d",iLabel);
					pdc->SetTextAlign(TA_CENTER);
					int posX2 = (int)(posX - abs(c_vectSizeBanda[i])/2);
					// pdc->TextOut(posX2,rectB.BottomRight().y,str);
					// spostato in alto
					if (mode & USE_STRIPRULER)
						pdc->TextOut(posX2,rectB.TopLeft().y - sz.cy,str);
					// numero marker negativo => disabilitato

					if (c_countStrip[countIndex] >= 0)
						{
						COLORREF oldColorText;
						oldColorText = pdc->GetTextColor();
						pdc->SetBkMode (OPAQUE);
						pdc->SetBkColor (RGB(196,196,196));
						if (c_countStrip[countIndex] == 0)
							pdc->SetTextColor(RGB(0,255,0));
						else	
							pdc->SetTextColor(RGB(255,0,0));
						// era "M = %d"
						if (mode & USE_STRIPRULER)
							str.Format("%d",c_countStrip[countIndex]);

						pdc->TextOut(posX2,rectB.BottomRight().y+5,
							str);					
						pdc->SetTextColor(oldColorText);
						pdc->SetBkMode (TRANSPARENT);
					}
				}
			}
		}
	}
	
}

//---------------------------------------------------
// Posizionamento Label
CRect genRect;
CSize genSize;

// 
// label delle classi 
// con pallini
// 

int i=0;
for (i=0;i<c_primaClasse;i++)
	{
	c_cLabel[i].setVisible(FALSE);
	c_cLabel[i].updateLabel(pdc);
	}

for (i=c_primaClasse;(i<c_primaClasse+c_numClassi)&&(i<MAX_NUMCLASSI);i++)
	{
	CRect sbR;
	CPoint p;
	
	if (c_useClassicPallini)
		{
		sbR=rectB;
		sbR.top		+= (rectB.Size().cy/c_numClassi)*(i-c_primaClasse);
		sbR.left	 = rectB.right + 2;
		sbR.bottom  -= (rectB.Size().cy/c_numClassi)*(c_numClassi-(i-c_primaClasse+1));
		sbR.right	 = rcBounds.right;
		genSize = c_cLabel[i].getSize(pdc);

		p = CPoint(sbR.left,sbR.top+(sbR.Size().cy/2)-(genSize.cy/2));
		}
	else
		{// label alto a dx
		sbR = rcBounds;
		// sbR.top		= (rectB.Size().cy/c_numClassi)*(i-c_primaClasse);
		//sbR.left	 = rectB.right + 2;
		//sbR.bottom  -= (rectB.Size().cy/c_numClassi)*(c_numClassi-(i-c_primaClasse+1));
		//sbR.right	 = rcBounds.right;
		pdc->SetTextAlign(TA_LEFT);
		genSize = c_cLabel[i].getSize(pdc);
		p = CPoint(rectB.right + 30,sbR.top+i*(genSize.cy));
		}

	c_cLabel[i].setVisible(TRUE);
	c_cLabel[i].setPos(p,pdc);
	}

while(i<MAX_NUMCLASSI)
	{
	c_cLabel[i].setVisible(FALSE);
	c_cLabel[i].updateLabel(pdc);
	i++;
	}

// Caption

genSize = sCaption.getSize(pdc);
genRect.left = (rcBounds.right - genSize.cx)/2;
genRect.right = genRect.left + genSize.cx;
int deltay = (abs(rcBounds.top-rectB.top)-genSize.cy)/2	;
genRect.top	= rcBounds.top + deltay ; // aggiunto spazio per label orizzontali
genRect.bottom = rectB.top - deltay; 

CPoint p(genRect.left,genRect.top);
sCaption.setPos(p,pdc);

genRect.left = rcBounds.left;
genRect.right = genRect.left;
genRect.top	= rectB.bottom + genSize.cy/2 + tm.tmHeight;
genRect.bottom = genRect.top + genSize.cy; 

int invalidLabel [MAX_NUMLABEL];
for (int k=0;k<MAX_NUMLABEL;k++)
	invalidLabel[k] = k;
for (i=0;i<MAX_NUMLABEL;i++)
	{// posizione negativa non scrive
	if ((c_vOrderLabel[i] < MAX_NUMLABEL)&&(c_vOrderLabel[i]>=0))
		{
		genSize = c_sLabel[c_vOrderLabel[i]].getSize(pdc);
		genRect.left = genRect.right + c_pxSepLabel[i];
		genRect.right = genRect.left + genSize.cx;
		p.x = genRect.left;
		p.y = genRect.top;
		c_sLabel[c_vOrderLabel[i]].setVisible(TRUE);
		c_sLabel[c_vOrderLabel[i]].setPos(p,pdc);

		// Edit DimX
		genSize = c_vLabel[c_vOrderLabel[i]].getSize(pdc);
		genRect.left = genRect.right + 10;
		genRect.right = genRect.left + genSize.cx;

		p.x = genRect.left;
		p.y = genRect.top;
		c_vLabel[c_vOrderLabel[i]].setVisible(TRUE);
		c_vLabel[c_vOrderLabel[i]].setPos(p,pdc);
		if (c_nlAfterLabel [i] == 1)
			{// Riga Successiva
			genRect.top	+= genSize.cy + genSize.cy/2;
			genRect.bottom = genRect.top + genSize.cy; 
			genRect.left = rcBounds.left;
			genRect.right = genRect.left;
			}
		// tengo traccia di quelle visualizzate
		invalidLabel[c_vOrderLabel[i]] = -1;
		}
	}

for (int k=0;k<MAX_NUMLABEL;k++)
	{
	if (invalidLabel[k] >= 0)
		{
		c_sLabel[invalidLabel[k]].setVisible(FALSE);
		c_vLabel[invalidLabel[k]].setVisible(FALSE);
		c_sLabel[invalidLabel[k]].updateLabel();
		c_vLabel[invalidLabel[k]].updateLabel();

		}
	}


pdc->RestoreDC(-1);

penGrid.DeleteObject();
fontCaption.DeleteObject();
fontLabel.DeleteObject();
fontNormal.DeleteObject();
}



void Layout::CalcDrawRect (const CRect& rcBounds,CRect& rcDraw)
{

rcDraw = rcBounds;
/*
if (mode == NOLABELV)
	rcDraw.left += abs(rcDraw.Width() / 25);
else
*/
	rcDraw.left += abs((rcDraw.Width() * c_viewLeftPerc) / 100);
	
rcDraw.right	-= abs((rcDraw.Width() * c_viewRightPerc) / 100);

rcDraw.top	+= abs((rcDraw.Height() * c_viewTopPerc)/100);

rcDraw.bottom -= abs((rcDraw.Height() * c_viewBottomPerc)/100);
}



// Disegna esternamente 
void Layout::Draw3DRect (CDC* pdc,const CRect& rectB)
{
CPen dkGrayPen,whitePen,*oPen;
CRect r;

if (rectB.top > rectB.bottom)
	return;


r = rectB;

r.left -= 1;
r.top -= 1;
r.right += 1;
r.bottom += 1;

dkGrayPen.CreatePen(PS_SOLID,0,RGB(96,96,96));
whitePen.CreatePen(PS_SOLID,0,RGB(255,255,255));


oPen = pdc->SelectObject(&dkGrayPen);
pdc->MoveTo (r.left,r.bottom);
pdc->LineTo (r.left,r.top);
pdc->MoveTo (r.left,r.top);
pdc->LineTo (r.right,r.top);

// 3D Look not for print
if (!pdc->IsPrinting())
	pdc->SelectObject(&whitePen);

pdc->MoveTo (r.right,r.top);
pdc->LineTo (r.right,r.bottom);
pdc->MoveTo (r.right,r.bottom);
pdc->LineTo (r.left,r.bottom);

pdc->SelectObject(oPen);

dkGrayPen.DeleteObject();
whitePen.DeleteObject();
}

BOOL Layout::OnEraseBkgnd(CDC* pDC,CRect &rect) 
{
// TODO: Add your message handler code here and/or call default
// ByFC
//*********************************************

CBrush brushInt,brushExt,*oBrush;
CRgn rgnExt,rgnInt,rgnAnd,rgnLabelLV,rgnLabelRV;
rgnExt.CreateRectRgnIndirect(&rect);
rgnAnd.CreateRectRgnIndirect(&rect); 
CRect rcDraw;
CalcDrawRect (rect,rcDraw);

CRect rLV (rcDraw);
rLV.left = rect.left;
rLV.right = rcDraw.left; 
rgnLabelLV.CreateRectRgnIndirect(&rLV); 

CRect rRV (rcDraw);
rRV.left = rcDraw.right;
rRV.right = rect.right; 
rgnLabelRV.CreateRectRgnIndirect(&rRV); 
 
 
rgnInt.CreateRectRgnIndirect(&rcDraw);
if ((excludeUpdateBackGr)||
	(rgbBackColor != rgbDrawBackColor))
	rgnAnd.CombineRgn(&rgnExt,&rgnInt,RGN_DIFF);
else
	rgnAnd.CopyRgn(&rgnExt); 

if (excludeUpdateBkgLV)
	{
	rgnAnd.CombineRgn(&rgnAnd,&rgnLabelLV,RGN_DIFF);
	rgnAnd.CombineRgn(&rgnAnd,&rgnLabelRV,RGN_DIFF);
	}


brushExt.CreateSolidBrush (rgbBackColor);
brushExt.UnrealizeObject();
oBrush = pDC->SelectObject (&brushExt);    
pDC->FillRgn(&rgnAnd,&brushExt);

if ((!excludeUpdateBackGr)&&
	(rgbBackColor != rgbDrawBackColor))
	{
	brushInt.CreateSolidBrush (rgbDrawBackColor);
	brushInt.UnrealizeObject();
	pDC->SelectObject (&brushInt);
	pDC->FillRect(&rcDraw,&brushInt);
	}
pDC->SelectObject (oBrush);    
rgnExt.DeleteObject();
rgnAnd.DeleteObject();
rgnInt.DeleteObject();
rgnLabelLV.DeleteObject();
rgnLabelRV.DeleteObject();
brushExt.DeleteObject();
brushInt.DeleteObject();

return(TRUE);
}

BOOL Layout::OnEraseDrawBkgnd(CDC* pDC) 
{
// TODO: Add your message handler code here and/or call default
// ByFC
//*********************************************
CBrush brushInt,*oBrush;
 
CRect rcDraw;
CalcDrawRect (rcBounds,rcDraw);

brushInt.CreateSolidBrush (rgbDrawBackColor);
brushInt.UnrealizeObject();
oBrush = pDC->SelectObject (&brushInt);
pDC->FillRect(&rcDraw,&brushInt);
pDC->SelectObject (oBrush);    

brushInt.DeleteObject();


return(TRUE);
}


void Layout::drawAltBackground(CDC *pDC,CRect r)
{
// background settori alternativi
// bande tutte uguali
CBrush brushAlt,*oBrush;
brushAlt.CreateSolidBrush (rgbAltDrawBackColor);
brushAlt.UnrealizeObject();
oBrush = pDC->SelectObject (&brushAlt);
pDC->FillRect(&r,&brushAlt);
pDC->SelectObject (oBrush);    

brushAlt.DeleteObject();
}


void Layout::updateDrawRegion(CDC* pdc)
{

pdc->SaveDC();

CRect rectB;

// Trovo Finestra disegno (grigia)
CalcDrawRect (rcBounds,rectB);

//-------------------------------------
// 3D Look
// 
//------------------------------------------
//Draw3DRect (pdc,rectB);


// Scala verticale
// NumStepY Parametrizzato alla dim Finestra
CRect subRectB [MAX_NUMCLASSI];
subRectB[0]=rectB;
if (mode & MSCALEV)
	{// caso multiscala verticale
	for (int i=0;i<c_numClassi;i++)
		{
		subRectB[i]=rectB;
		subRectB[i].TopLeft().y += (rectB.Size().cy/c_numClassi)*i;
		subRectB[i].BottomRight().y -= (rectB.Size().cy/c_numClassi)*(c_numClassi-(i+1));
		if ((c_numClassi > 1)&&(i>0))
			// disegno asse intermedio
			{
			CPen pen1,*oldPen;
			pen1.CreatePen(PS_SOLID,1,RGB(0,0,0));	
			oldPen = pdc->SelectObject(&pen1);
			pdc->MoveTo(subRectB[i].left,subRectB[i].TopLeft().y);
			pdc->LineTo(subRectB[i].right,subRectB[i].TopLeft().y);		
			pdc->SelectObject(oldPen);
			pen1.DeleteObject();	
			}
		}
	}


// disegno soglia allarme
double ly;

int j=0;
do 
	{
	if(c_alValY[j] != DBL_MIN)
		{
		ly = 1.* subRectB[j].bottom - ((subRectB[j].bottom -subRectB[j].top)*c_alValY[j]/c_deltaY[j]) ; // - tm.tmHeight/2;	
		CPen pen1,*oldPen;
		pen1.CreatePen(PS_DASH,1,RGB(255,0,0));	
		oldPen = pdc->SelectObject(&pen1);
		pdc->MoveTo(subRectB[j].left,(int)ly);
		pdc->LineTo(subRectB[j].right,(int)ly);		
		pdc->SelectObject(oldPen);
		pen1.DeleteObject();	
		}
	}
while ((mode & MSCALEV) && ((++j)<c_numClassi));
	
pdc->RestoreDC(-1);
}
