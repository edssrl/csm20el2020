// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////


#include "RpcMClient.h"
#include "CustStatusBar.h"
#include "StatoAllarmi.h"

#include "PdfInfoMsf.h"

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"

#include "DAlzataDettagli.h"



class CMainFrame : public CMDIFrameWnd, public CRpcMClient
// class CMainFrame : public CFrameWnd, public CRpcClient
{
protected: // create from serialization only

	afx_msg void OnUpdateDate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTime(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusBot(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusTop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateMeter(CCmdUI* pCmdUI);

	afx_msg void OnUpdateAlLaser(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlEncoder(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlOtturatore1(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlOtturatore2(CCmdUI* pCmdUI);
	afx_msg void OnUpdateAlCpuReceiver(CCmdUI* pCmdUI);

	DECLARE_DYNAMIC(CMainFrame)

public:
	CMainFrame();
	//DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	int m_nDatePaneNo;
	int m_nTimePaneNo;

	BOOL	c_disableSystemMenu;
// pdfInfo Section
	CPdfInfo c_pdfInfo;

// Timer alarm
BOOL c_doSendSoglie;	// invia o no valori soglie insieme allo stato
UINT alarmTimerId;
UINT statusTimerId;
UINT rxPipeTimerId;
UINT rxPipeTimerMsec;
StatoAllarmi c_stAllarmiTop;
StatoAllarmi c_stAllarmiBottom;
CDAlzataDettagli c_alzataDettagli;

// Velocita` di linea
double c_lineSpeed; // Velocita` in metri/min
unsigned short c_valEncoder; // rawData Encoder
// flag per sapere se siamo in ciclo di stampa report automatici (disabilita F1 )
BOOL c_isReporting;

// Configurazione indirizzi Top e Bottom
int	RPCID_HC16_TOP;
int	RPCID_HC16_BOTTOM;


// Operations
public:
// Invia stato ad HC16
void sendStatus (BOOL stato);
virtual BOOL serverRequest(int cmd,int sender,int size,void *data);
// virtual BOOL serverRequest(int cmd,int size,void *data);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:  // control bar embedded members
	CCustStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CProgressCtrl wndProgress;
	BOOL progressBusy;

CProgressCtrl* getProgressCtrl(void);
BOOL progressCreate(void);
void progressDestroy(void);
void progressSetRange(int from,int to);
void progressSetStep(int s);
void progressStepIt(void);

// Gestisce arrivo VAL_TESTSPEED
void gestValLineSpeed (PVOID p,DWORD size);

// simulate msg system
BOOL Pump (void);

// Generated message map functions
protected:
	CDialogBar m_wndCAlarmBarTop;
	CDialogBar m_wndCAlarmBarBottom;
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnConfiguraOpzioni();
	afx_msg void OnConfiguraReport();
	afx_msg void OnRicetteCopia();
	afx_msg void OnRicetteElimina();
	afx_msg void OnRicetteModifica();
	afx_msg void OnRicetteNuova();
	afx_msg void OnRicetteVisualizza();
	afx_msg void OnConfiguraParametri();
	afx_msg void OnFileModificapassword();
	afx_msg LRESULT OnServerRequest(WPARAM wParam, LPARAM lParam);
	afx_msg void OnFileComport();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnConfiguraSoglie();
	afx_msg void OnViewRepaint();
	afx_msg void OnViewSystem();
	afx_msg void OnUpdateViewSystem(CCmdUI* pCmdUI);
	afx_msg void OnViewTrend();
	afx_msg void OnUpdateViewTrend(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConfiguraParametri(CCmdUI* pCmdUI);
	afx_msg void OnVisualizzaBotTop();
	afx_msg void OnConfiguraAvvioF1();
	afx_msg void OnViewDataByclass();
	afx_msg void OnUpdateViewDataByclass(CCmdUI* pCmdUI);
	afx_msg void OnUpdateVisualizzaBotTop(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSingleCoilMode(CCmdUI* pCmdUI);
	afx_msg void OnConfiguraLocalizzazione();
	afx_msg void OnConfiguraPdf();
	afx_msg void OnViewKreport();
	afx_msg void OnConfiguraColori();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL InitStatusBar(UINT *pIndicators, int nSize, int nSeconds);
public:
//	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnClose();

	// disable sysMenu
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnNcLButtonDblClk(UINT nHitTest, CPoint point);
//	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
//	afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

};

/////////////////////////////////////////////////////////////////////////////
