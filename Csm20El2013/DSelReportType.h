#if !defined(AFX_DSELREPORTTYPE_H__893FED8A_050E_4C97_8CA8_B28595356523__INCLUDED_)
#define AFX_DSELREPORTTYPE_H__893FED8A_050E_4C97_8CA8_B28595356523__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DSelReportType.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDSelReportType dialog

class CDSelReportType : public CDialog
{
// Construction
public:
	CLineDoc* c_pDoc;
	CDSelReportType(CWnd* pParent = NULL);   // standard constructor

	long c_elementIndex;
	long c_alzataIndex;
	long c_stripIndex;

// Dialog Data
	//{{AFX_DATA(CDSelReportType)
	enum { IDD = IDD_SEL_REPORT_TYPE };
	CStatic	m_ctrlStaticTo;
	CStatic	m_ctrlStaticFrom;
	CEdit	m_ctrlPosTo;
	CEdit	m_ctrlPosFrom;
	CComboBox	m_ctrlStrip;
	CComboBox	m_ctrlAlzate;
	CComboBox	m_ctrlElementi;
	int		m_repType;
	long	m_posFrom;
	long	m_posTo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDSelReportType)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDSelReportType)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnReportType();
	afx_msg void OnStripReport();
	afx_msg void OnSelchangeElementi();
	afx_msg void OnSelchangeAlzate();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DSELREPORTTYPE_H__893FED8A_050E_4C97_8CA8_B28595356523__INCLUDED_)
