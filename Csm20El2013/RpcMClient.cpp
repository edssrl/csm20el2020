// RpcMClient.cpp : implementation file
// It supports multiple remote client 	  14/12/96

//----------------------------------------------------
// It supports pipe connection with Rpcserver2011 ver >= 2.10  23/11/13
//--------------------------------------------------

#include "stdafx.h"
#include "Profile.h"
#include "Registry.h"

#include "resource.h"

#include "RpcMClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CRpcMClient

CRpcMClient::CRpcMClient()
{
c_procServer = "RpcServer2011.exe";
c_dirServer = "";
c_serMsgReceived = FALSE;
c_rpcPipeMode = FALSE;
c_rpcOffLine = FALSE;
}

CRpcMClient::~CRpcMClient()
{

closeAndWaitPipe();
c_rxBufferList.RemoveAll();
c_txBufferList.RemoveAll();
}



/////////////////////////////////////////////////////////////////////////////
// CRpcClient message handlers


void CRpcMClient::addRemote(int remoteId,CString &device)
{
Remote remInfo;

// Add remoteInfo to array
remInfo.id = remoteId;
remInfo.device = device;
												
c_remoteRpcId.Add (remInfo);

}

// Register this and ALL remote Client
void CRpcMClient::rpcRegister(CString& wndName,int thisId,RPCTYPE type,int size)
{
CString strValue;

// Nessuna connessione richiesta
if (c_rpcOffLine)
	return;

// Local Info unique
c_thisRpcId = thisId;
c_thisWnd = wndName;
c_type = type;
c_size = size;

// Init Server Process
CProfile registry;
// CRegistry registry ("EDS","CSM20");

c_rpcPipeMode = registry.getProfileBool(_T("SERVER"), _T("UsePipe"), 
				FALSE);

c_dirServer = registry.getProfileString(_T("SERVER"), _T("Path"), 
				"");
c_procServer = c_dirServer;

c_procServer += registry.getProfileString(_T("SERVER"), _T("Name"), 
				"RpcServer2011.exe");


// Remote on serialPort
for (int i=0;i<c_remoteRpcId.GetSize();i++)
	{
	// Create Server process if needed
	RegisterClient (c_remoteRpcId[i].device,c_remoteRpcId[i].id,size);
	}

if (c_rpcPipeMode)
	c_thisWnd = "PIPE1";

// FALSE -> do not send back notification on packet abort
RegisterClient (c_thisWnd,c_thisRpcId,size,FALSE);
if (c_rpcPipeMode)
	{
	Sleep (1000);
	openPipe(c_thisWnd);
	}
}




// Register THIS application
BOOL CRpcMClient::RegisterClient (CString &nome,RPCID id,int frameSize,BOOL abortNotify)
{
// Nessuna connessione richiesta
if (c_rpcOffLine)
	return TRUE;

ClientInfo cInfo;
COPYDATASTRUCT cds;

int i;
for (i =0;i<nome.GetLength();i++)
	cInfo.c_name [i] = nome [i];
while (i < CINFOSIZENAME)
	cInfo.c_name[i++] = 0;

cInfo.c_id = id;
cInfo.c_type = c_type;
cInfo.c_FrameSize = frameSize;
cInfo.c_abortNotify = abortNotify;

// registerClient sempre DMCS, 
// RPCID_SERVER e` ZERO quindi sia DMCS sia DMCS2
// 4 bit alti sempre zero se non si usa mai zero per altri address
// range address possibili 1-15
HRpcInfo rpcI(TYPE_DMCS,RPCID_SERVER,
		c_thisRpcId,
		REGISTER_CLIENT,
		sizeof (cInfo));

/*-------------------------------------
rpcI.info.dest = RPCID_SERVER;
rpcI.info.sender = c_thisRpcId;
rpcI.info.command = REGISTER_CLIENT;
rpcI.info.size = sizeof (cInfo);
--------------------------------------*/

cds.dwData = rpcI.getLInfo();
cds.cbData = sizeof (cInfo);
cds.lpData = (void *) &cInfo;

CWnd *server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
if (!server)
	{
	PROCESS_INFORMATION ProcessInformation;
	STARTUPINFO StartUpInfo;
	
	::GetStartupInfo (&StartUpInfo);
	BOOL fSuccess = CreateProcess((LPCSTR)c_procServer,NULL,
				NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,
				NULL,(LPCSTR)c_dirServer,
				&StartUpInfo,&ProcessInformation);
  

	if (fSuccess) 
		{
		// Wait Creation Window RpcServer
		int res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
		if (res != 0)
			{
			res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
			if (res != 0)
				{
				CString s;
				s = "Fail Wait Idle SERVER " + c_procServer;
				AfxMessageBox (s);
				}
			}
	 	CloseHandle(ProcessInformation.hThread);
		CloseHandle(ProcessInformation.hProcess);

		server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
		}
	else
		{
		CString s;
		s = "Unable to Create SERVER " + c_procServer;
		AfxMessageBox (s);
		return(TRUE);
		}
	}

if (server)
	{
	server->SendMessage (WM_COPYDATA,
		//(WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(),
		// (WPARAM) this,
		rpcI.getRpcType(),
		(LPARAM)&cds);
	}
else
	{
	CString s;
	s = "Not Found SERVER " + c_procServer;
	AfxMessageBox (s);
	return FALSE;
	}

return(TRUE);
}

BOOL CRpcMClient::openPipe(CString nome)
{

if (!CNamedPipe::ServerAvailable(_T("."),nome, 3000))
	{
	CString s;
	s = "Not Found PIPE Connection " + nome;
	AfxMessageBox (s);
	return FALSE;
	}

if (c_pipe.IsOpen())
	{
	c_pipe.DisconnectClient();
	return TRUE;
	}
// if (!c_pipe.Open(_T("."),nome,GENERIC_READ | GENERIC_WRITE,  FILE_SHARE_READ | FILE_SHARE_WRITE , NULL,	FILE_FLAG_OVERLAPPED))
if (!c_pipe.Open(_T("."),nome,GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE  , NULL,	0))
	{
	AfxMessageBox(_T("Failed to connect to the specified named pipe"));
	return FALSE;
	}

// create thread

c_pipeThCom = AfxBeginThread(pipeThreadDMCS,(LPVOID ) this);
if (c_pipeThCom = NULL)
{
	AfxMessageBox(_T("Failed to create thread pipe"));
	return FALSE;
	}


return TRUE;
}

BOOL CRpcMClient::closeAndWaitPipe(void)
{
// Nessuna connessione richiesta
if (c_rpcOffLine)
	return TRUE;

if (!c_rpcPipeMode)
	return TRUE;

if (!c_pipe.IsOpen())
	return TRUE;

c_pipe.Close();

int cnt = 100;
while ((cnt>0)&&(c_ThreadTerminated != TRUE))
	{
	cnt --;
	Sleep(20);
	}

c_pipe.Detach();

if (c_ThreadTerminated)
	return TRUE;
else
	return FALSE;
}

// Server Data/Request through WM_COPYDATA
LRESULT CRpcMClient::OnServerRequest(WPARAM wParam, LPARAM lParam)
{

COPYDATASTRUCT *pcds = (COPYDATASTRUCT *) lParam;
HRpcInfo rpcInfo(c_type);

rpcInfo = pcds->dwData;

// Test For Local Command
if (rpcInfo.getDest() == c_thisRpcId)
	{
		serverRequest (rpcInfo.getCmd(),
					rpcInfo.getSender(),
				   pcds->cbData,
				   pcds->lpData);
					
	}

return 1;
}


/////////////////////////////////////////////////////////////////////////////
// CRpcClient message handlers

// Send To all registered system
BOOL CRpcMClient::SendBroadcastGeneralCommand(Command *command)
{
BOOL valret = TRUE;
// Remote 
for (int i=0;i<c_remoteRpcId.GetSize();i++)
	{
	int remoteId = c_remoteRpcId[i].id;
	// Create Server process if needed
	if (!SendGeneralCommand (command,remoteId))
		valret = FALSE;
	
	}
return (valret);
}


BOOL CRpcMClient::SendGeneralCommand(Command *command,RPCID remId)
{

// Nessuna connessione richiesta
if (c_rpcOffLine)
	return TRUE;

if (c_rpcPipeMode)
	return SendPipeCommand(command,remId);


HRpcInfo rpcI(c_type,remId,c_thisRpcId,command->cmd,command->size);
COPYDATASTRUCT cds;

// Dest HC16
/*
rpcI.info.dest = (unsigned char) remId;					 
rpcI.info.sender = c_thisRpcId;
rpcI.info.command = (char )command->cmd;
rpcI.info.size = (char)command->size;
*/


cds.dwData = rpcI.getLInfo();
cds.cbData = command->size;
cds.lpData = (void *) command->data;

CWnd *server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
if (!server)
	{
	AfxMessageBox ("SERVER not Found");
	return(FALSE);
	}

if (server)
	{
	server->SendMessage (WM_COPYDATA,
		// (WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(),
		// (WPARAM) this,
		(WPARAM)rpcI.getRpcType(),
		(LPARAM)&cds);
	return(TRUE);
	}
else
	{
	AfxMessageBox ("Not Found SERVER");
	return (FALSE);
	}

return(TRUE);
}


BOOL CRpcMClient::serverRequest (int cmd,int sender,int size,void *pter)
{

// Override this virtual function in CMainFrame

return FALSE;
}


BOOL CRpcMClient::SendPipeCommand(Command *command,RPCID remId)
{

if (!c_pipe.IsOpen())
	return FALSE;


// This code defines a list of ints.


HRpcInfo rpcI(c_type,remId,c_thisRpcId,command->cmd,command->size);
CommandBuffer h;

h.c_block[0] = rpcI.getParamByte(0);
h.c_block[1] = rpcI.getParamByte(1);
h.c_block[2] = rpcI.getParamByte(2);
h.c_block[3] = rpcI.getParamByte(3);

if (command->size > 0)
	{
	memcpy(&h.c_block[4],command->data,command->size);
	}
h.c_size = 4 + command->size;

CSingleLock lock(&c_txBufferLock);
lock.Lock();
c_txBufferList.AddTail(h);
lock.Unlock();

return TRUE;
}

BOOL CRpcMClient::ReadPipeCommand(CommandBuffer *pCommand)
{
BOOL vret;
vret = FALSE;

if (pCommand == NULL)
	return FALSE;

CSingleLock lock(&c_rxBufferLock);
lock.Lock();

if (c_rxBufferList.GetCount() > 0)
	{
	*pCommand = c_rxBufferList.GetHead();
	c_rxBufferList.RemoveHead();
	vret = TRUE;
	}
lock.Unlock();

return vret;
}


//--------------------------------------------------------------
//
//			Pipe Thread DMCS
//
//--------------------------------------------------------------

UINT CRpcMClient::pipeThreadDMCS ( LPVOID pParam)
{
unsigned char rxBlk [1024];
ClientInfo cInfo;

CRpcMClient *mClient = (CRpcMClient *) pParam;
ASSERT (pParam != NULL);
ASSERT (mClient->c_pipe.IsOpen());

HRpcInfo rpcInfo(mClient->c_type);

TRACE(_T("Thread PipeDMCS Starting .... \n"));
mClient->c_ThreadTerminated=FALSE;

// Byte and non blocking
mClient->c_pipe.SetMode(TRUE,FALSE);

CString sDebug,s;

while (mClient->c_pipe.IsOpen())
	{//
	// pipePort->c_hPipeEvent
	DWORD nch;
	sDebug.Empty();
	// any block to send?
	CSingleLock lockTx(&mClient->c_txBufferLock);
	lockTx.Lock();
	if (mClient->c_txBufferList.GetCount() > 0)
		{// ok write
		CommandBuffer b;
		b = mClient->c_txBufferList.GetHead();
		mClient->c_pipe.Write(b.c_block,b.c_size,nch);
		mClient->c_txBufferList.RemoveHead();
		}
	lockTx.Unlock();
	// test if read

	// read all 4 byte
	DWORD lastError=ERROR_SUCCESS;
	nch = 0;
	SetLastError(ERROR_SUCCESS);
	BOOL okRead;
	if (!(okRead=mClient->c_pipe.Read(&rxBlk[0],4,nch)))
		{
		lastError = GetLastError();
		if (lastError != ERROR_NO_DATA)
			TRACE(_T("Thread PipeDMCS Error Read pipe \n"));
		}
	if ((lastError != ERROR_NO_DATA) && (nch <= 0)) break;	// end task
	if (lastError == ERROR_NO_DATA) // Sleep task
		Sleep (30);
	if ((nch>0)&&
		// aggiunto test su error_success -> da provare 
		(lastError == ERROR_SUCCESS))
		{// ok header
		if (rxBlk [3] > 0)
			{
			// read rest of block
			SetLastError(ERROR_SUCCESS);
			nch = 0;
			if (!(okRead=mClient->c_pipe.Read(&rxBlk[4+nch],(int ) rxBlk [3],nch)))
				{
				lastError = GetLastError();
				if (lastError != ERROR_NO_DATA)
					{
					TRACE(_T("Thread PipeDMCS Error Read pipe \n"));
					}
				continue;
				}
			if ((lastError != ERROR_NO_DATA) && (nch <= 0)) break; // end task
			}
		// ok forward packet 
		CommandBuffer b;
		b.c_size= (int ) rxBlk [3] + 4;
		memcpy (b.c_block,rxBlk,b.c_size);
		CSingleLock lockRx(&mClient->c_rxBufferLock);
		lockRx.Lock();
		mClient->c_rxBufferList.AddTail(b);
		lockRx.Unlock();
		// ok decode packet
		//rpcInfo.setParamByte(rxBlk [0],rxBlk [1],rxBlk [2],rxBlk [3]);
		//mClient->serverRequest (rpcInfo.getCmd(),
		//		rpcInfo.getSender(),
		//		(DWORD)rpcInfo.getSize(),
		//	    (PVOID)&rxBlk[4]);
		}// rxBlk[0]
	}// End While

// sckClose(pSockPort->c_sockRds->c_sockPort);
mClient->c_ThreadTerminated=TRUE;
TRACE(_T("Thread pipeDMCS Terminated code 1 \n"));

return 0;
}
