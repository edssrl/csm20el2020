// DBSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DBRicette DAO recordset

class DBRicette : public CDaoRecordset
{
public:
	DBRicette(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(DBRicette)

// Field/Param Data
	//{{AFX_FIELD(DBRicette, CDaoRecordset)
	CString	m_NOME;
	double	m_ALLARME_A;
	double	m_ALLARME_B;
	double	m_ALLARME_C;
	double	m_ALLARME_D;
	double	m_SPESSORE_MIN;
	double	m_SPESSORE_MAX;
	CString	m_LEGA;
	double	m_LENGTH_ALLARME;
	double	m_SOGLIA_A;
	double	m_SOGLIA_B;
	double	m_SOGLIA_C;
	double	m_SOGLIA_D;
	double	m_SOGLIA_PER;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBRicette)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
BOOL openSelectID (CString &key,BOOL closeNotFound = TRUE);
BOOL openSelectName (CArray <CString,CString &> &keyName);
BOOL openSelectLegaSpessore (CString &Lega,double spessore,BOOL closeNotFound = TRUE);
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};
/////////////////////////////////////////////////////////////////////////////
// DBOpzioni DAO recordset

class DBOpzioni : public CDaoRecordset
{

public:
	DBOpzioni(CDaoDatabase* pDatabase = NULL);
	~DBOpzioni(void){};

public:
	DECLARE_DYNAMIC(DBOpzioni)

// Field/Param Data
	//{{AFX_FIELD(DBOpzioni, CDaoRecordset)
	CString	m_NOME;
	BOOL	m_ALLARME_ACU;
	double	m_ALLARME_DURATA;
	BOOL	m_STAMPA_AUTO;
	CString	m_TL_CLASSE;
	double	m_TL_LUNGHEZZA;
	double	m_TD_DENSITA;
	double	m_TD_LUNGHEZZA;
	double	m_TD_SCALA;
	double	m_TL_SCALA;
	BOOL	m_MAPPA100;
	BOOL	m_STAMPA_AUTO_STRIP;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBOpzioni)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};
/////////////////////////////////////////////////////////////////////////////
// DBParametri DAO recordset

class DBParametri : public CDaoRecordset
{

public:
	DBParametri(CDaoDatabase* pDatabase = NULL);
	~DBParametri(void){};

public:
	DECLARE_DYNAMIC(DBParametri)

// Field/Param Data
	//{{AFX_FIELD(DBParametri, CDaoRecordset)
	CString	m_NOME;
	double	m_FCDL;
	double	m_L_RESIDUA_STOP;
	double	m_SOGLIA_A;
	double	m_SOGLIA_B;
	double	m_SOGLIA_C;
	double	m_SOGLIA_D;
	double	m_ALARM_PULSE;
	double	m_DIA_ENCODER;
	double	m_ODDEVEN_DISTANCE;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBParametri)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};

};
/////////////////////////////////////////////////////////////////////////////
// DBReport DAO recordset

class DBReport : public CDaoRecordset
{

public:
	DBReport(CDaoDatabase* pDatabase = NULL);
	~DBReport(void){};
public:
	DECLARE_DYNAMIC(DBReport)

// Field/Param Data
	//{{AFX_FIELD(DBReport, CDaoRecordset)
	CString	m_NOME;
	BOOL	m_DATI_CLIENTE;
	BOOL	m_DATA_LAVORAZIONE;
	BOOL	m_LUNGHEZZA_BOBINA;
	BOOL	m_DESCR_PERIODICI;
	BOOL	m_DESCR_RANDOM;
	BOOL	m_SOGLIE_ALLARME;
	BOOL	m_MAPPA100;
	double	m_DALMETRO;
	double	m_ALMETRO;
	double	m_INTERVALLO1;
	double	m_INTERVALLO2;
	double	m_INTERVALLO3;
	double	m_INTERVALLO10;
	double	m_INTERVALLO11;
	double	m_INTERVALLO12;
	double	m_INTERVALLO13;
	double	m_INTERVALLO14;
	double	m_INTERVALLO15;
	double	m_INTERVALLO16;
	double	m_INTERVALLO17;
	double	m_INTERVALLO18;
	double	m_INTERVALLO19;
	double	m_INTERVALLO4;
	double	m_INTERVALLO5;
	double	m_INTERVALLO6;
	double	m_INTERVALLO7;
	double	m_INTERVALLO8;
	double	m_INTERVALLO9;
	BOOL	m_DESCR_ALZATE;
	BOOL	m_DESCR_GRAPH1;
	BOOL	m_DESCR_GRAPH2;
	BOOL	m_DESCR_GRAPH3;
	BOOL	m_DESCR_GRAPH4;
	BOOL	m_DESCR_CLASSA;
	BOOL	m_DESCR_CLASSB;
	BOOL	m_DESCR_CLASSC;
	BOOL	m_DESCR_CLASSD;
	BOOL	m_DESCR_CLASSBIG;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DBReport)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	virtual void Close();
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
public:
BOOL openSelectDefault (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
BOOL openSelectManual (BOOL closeNotFound = TRUE,BOOL addOnEmpty = TRUE);
BOOL openSelect (CString name,BOOL closeNotFound,BOOL addOnEmpty);
BOOL isDbOpen(){return ((m_pDatabase != NULL) && m_pDatabase->IsOpen());};
};

