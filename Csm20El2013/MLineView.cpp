// Multi Line View : implementation file
//

#include "stdafx.h"
#include "MainFrm.h"
#include "resource.h"
#include "Profile.h"

#include <AfxTempl.h>

#include "GraphDoc.h"
#include "GraphView.h"
#include "dyntempl.h"
#include "Csm20El2013.h"

#include "MLineView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLineView

IMPLEMENT_DYNCREATE(CMLineView, CView)

CMLineView::CMLineView()
{
vType = MULTILINEVIEW;
layout.setViewType(MULTILINEVIEW);
lastActualMeter =  0;
c_numStepX=1;
c_stepScaler=1;

// Init layout Fix Attributes
CProfile profile;
CString s;

layout.fCaption.size =  profile.getProfileInt("MLineLayout","CaptionSize",120);
layout.fLabel.size = profile.getProfileInt("MLineLayout","LabelSize",100);
layout.fNormal.size = profile.getProfileInt("MLineLayout","NormalSize",110);

s = profile.getProfileString("MLineLayout","NewLine","3,6");
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_nlAfterLabel [v] = 1;
	}

s = profile.getProfileString("MLineLayout","LabelOrder","0,1,2,3,4,5,6,7,8,9");
int k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	if ((v < MAX_NUMLABEL)&&(v>=0))
		layout.c_vOrderLabel [k++] = v;
	if (k>(MAX_NUMLABEL-1))
		break;
	}

s = profile.getProfileString("MLineLayout","PixelSepLabel",
							 "10,10,10,10,10,10,10,10,10,10");
k = 0;
while (s.GetLength() >0)
	{
	CString subS;
	subS = s.SpanExcluding(",;");
	s = s.Right(s.GetLength()-subS.GetLength()-1);
	int v;
	sscanf((LPCSTR)subS,"%d",&v);
	layout.c_pxSepLabel [k++] = v;
	if (k>9)
		break;
	}

layout.c_pxSepLabelEdit = profile.getProfileInt("MLineLayout","PixelSepLabelEdit",10);

//s = profile.getProfileString("MLineLayout","SCaption",
//							 "TREND");
s.LoadString(CSM_GRAPHVIEW_MLINECAPTION);
layout.setCaption(s);

// label nel .ini
//for (int i=0;i<MAX_NUMLABEL;i++)
//	{
//	CString sIndex;
//	sIndex.Format ("SLabel%d",i);
//	s = profile.getProfileString("MLineLayout",sIndex,sIndex);
//	if (s != sIndex)
//		{
//		layout.c_sLabel[i] = s;
//		}
//	}

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL0);
layout.c_sLabel[0] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL1);
layout.c_sLabel[1] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL2);
layout.c_sLabel[2] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL3);
layout.c_sLabel[3] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL4);
layout.c_sLabel[4] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL5);
layout.c_sLabel[5] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL6);
layout.c_sLabel[6] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL7);
layout.c_sLabel[7] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL8);
layout.c_sLabel[8] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL9);
layout.c_sLabel[9] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL10);
layout.c_sLabel[10] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL11);
layout.c_sLabel[11] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL12);
layout.c_sLabel[12] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL13);
layout.c_sLabel[13] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL14);
layout.c_sLabel[14] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL15);
layout.c_sLabel[15] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL16);
layout.c_sLabel[16] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL17);
layout.c_sLabel[17] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL18);
layout.c_sLabel[18] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL19);
layout.c_sLabel[19] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL20);
layout.c_sLabel[20] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL21);
layout.c_sLabel[21] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL22);
layout.c_sLabel[22] = s;

s.LoadString(CSM_GRAPHVIEW_TOWER_SLABEL23);
layout.c_sLabel[23] = s;

for (int i=0;(i<=4);i++)
	{
	CString s;
	s = TCHAR('A'+i);
	if (i == 4)
		s = "Big";
	layout.c_cLabel[i] = s;
	layout.c_cLabel[i].setTColor(CSM20GetClassColor(i));
	layout.c_cLabel[i].setTipoIcon(IPALLINO);
	}



// CalcDrawRect
layout.c_viewTopPerc = profile.getProfileInt("MLineLayout","ViewTop%",10);
layout.c_viewBottomPerc = profile.getProfileInt("MLineLayout","ViewBottom%",25);
layout.c_viewLeftPerc = profile.getProfileInt("MLineLayout","ViewLeft%",8);
layout.c_viewRightPerc = profile.getProfileInt("MLineLayout","ViewRight%",5);

StringLabelH strLH;
strLH.pos = 100;
strLH.label = "Ris1";
layout.c_stringLabelH[0] = strLH;

strLH.pos = 300;
strLH.label = "Ris2";
layout.c_stringLabelH[1] = strLH;

strLH.pos = 500;
strLH.label = "Ris3";
layout.c_stringLabelH[2] = strLH;

layout.setMode (LINEAR | NOLABELH | MSCALEV | USE_STRING_LABELH);
}

CMLineView::~CMLineView()
{
}


BEGIN_MESSAGE_MAP(CMLineView, CView)
	//{{AFX_MSG_MAP(CMLineView)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMLineView drawing


// area disegno interna
CRect CMLineView::getDrawRect(void)
{
CRect rcBounds,rect;
GetClientRect (rcBounds);
layout.CalcDrawRect(rcBounds,rect);
return (rect);
}




/////////////////////////////////////////////////////////////////////////////
// CMLineView diagnostics

#ifdef _DEBUG
void CMLineView::AssertValid() const
{
	CView::AssertValid();
} 

void CMLineView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMLineView message handlers

BOOL CMLineView::OnEraseBkgnd(CDC* pDC) 
{
if (pDC == NULL)
	return TRUE;
// TODO: Add your message handler code here and/or call default
CRect r;
GetClientRect(&r);
layout.OnEraseBkgnd(pDC,r);

return TRUE;	
}

BOOL CMLineView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{

// TODO: Add your specialized code here and/or call the base class
if (!CView::Create(lpszClassName, lpszWindowName, 
	dwStyle, rect, pParentWnd, nID, pContext))
	return (FALSE);

DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

layout.Create(this);

for (int j=0;j<pDoc->c_difCoil.getNumClassi();j++)
	{
	trendLine[j].setPen(CSM20GetClassColor(j));
	trendLine[j].setPenBig(CSM20GetClassColor(pDoc->c_difCoil.getNumClassi()));
	}
return TRUE;
}



BOOL CMLineView::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class

	CString s;
	s.Format("Got Notify Message from %d",(int)wParam);
	AfxMessageBox (s);
	return CView::OnNotify(wParam, lParam, pResult);
}

void CMLineView::OnDraw(CDC* pdc)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

//double maxValY = pDoc->GetTrendLScalaY();
//double maxValX = pDoc->GetTrendLScalaX();

// init gloval var c_numStepX
calcNumStepX(pDoc);

CRect rectB;
CRect rcBounds;
GetClientRect (rcBounds);
// Set base Rect
layout.setDrawRect(rcBounds);


int numClassi = pDoc->c_difCoil.getNumClassi();
layout.setNumClassi(numClassi);
layout.setNumLabelClassi(numClassi+1);

//--------------------
updateLabel ();

int m = pDoc->c_difCoil.getMeter();
layout.draw(pdc);
	
// Trovo Finestra disegno (grigia)
layout.CalcDrawRect (rcBounds,rectB);


int viewTrendStep = pDoc->GetTrendStep()* c_stepScaler;
int code = pDoc->GetForiCod();

for (int i=0;i<numClassi;i++)
	{
	lineInfo[i].max = pDoc->GetTrendLScalaY();
	lineInfo[i].min = 0.;
	lineInfo[i].limit = pDoc->c_difCoil.c_sogliaAllarme[i];
	lineInfo[i].count = (int)((((int)pDoc->c_difCoil.getMeter()/viewTrendStep) < c_numStepX)?
						(pDoc->c_difCoil.getMeter()/viewTrendStep) : c_numStepX); 
	lineInfo[i].numStep = c_numStepX;
	lineInfo[i].stepScaler = c_stepScaler;
	lineInfo[i].actual	= (int) (pDoc->c_difCoil.getMeter()/viewTrendStep)% c_numStepX;
	// Aggiornato 1997 alloca memoria
	lineInfo[i].newVal (lineInfo[i].numStep+1);
	}


for (int j=0;j<= c_numStepX;j++)
	{
	// trovo indice metro dif 
	int idx = pDoc->c_difCoil.getMeterIdxLStep(j*viewTrendStep);
	for (int i=0;i<numClassi;i++)
		{
		if (idx < 0)
			{
			lineInfo[i].val[j] = 0;
			lineInfo[i].valBig[j] = 0;
			}
		else
			{
			lineInfo[i].val[j] = pDoc->c_difCoil[idx].getValNum('A'+i);
			// lineInfo[i].val[j] = pDoc->c_difCoil.getValNumLStep('A'+i,j*viewTrendStep);
			if (i == pDoc->c_difCoil.getNumClassi()-1)
				{
				lineInfo[i].valBig[j] = pDoc->c_difCoil[idx].getTotalBig();
				}
			else
				lineInfo[i].valBig[j] = 0;
			}
		}
	}


CRect subRectB[4];
for (int i=0;i<numClassi;i++)
	{
	trendLine[i].setMode (LINEAR);
	subRectB[i]=rectB;
	subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
	subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
	trendLine[i].setScale(subRectB[i],lineInfo[i]);
	trendLine[i].draw(pdc,FALSE);
	}

lastActualMeter = pDoc->c_difCoil.getMeter();
}



void CMLineView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
Invalidate(TRUE);	
}

void CMLineView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class

if ((lHint == 0) && (pHint == NULL))
	CView::OnUpdate(pSender,lHint,pHint);	
else
	{
	DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

	// 04-03-2010 fix Bug non settati top e bottom rett. disegno in update 
	// righe cursore non cancellate!
	CRect rcBounds,rectB;
	// Spazio Di Disegno
	// Trovo Finestra disegno (grigia)
	GetClientRect (rcBounds);
	layout.setDrawRect(rcBounds);
	layout.CalcDrawRect (rcBounds,rectB);


	static CRect rcInvalid;
	int oldStepX,actualStepX;

	int maxValX = pDoc->GetTrendLScalaX();
	// update global variable c_numStepX
	calcNumStepX(pDoc);
	actualStepX = ((int)pDoc->c_difCoil.getMeter()/(int)(pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene

	// oldStepX = ((int)lastActualMeter/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene
	// oldstepX e` quello prima del precedente xche` potrebbe essere stato aggiornato da 
	// pacchetto arrivato con altra classe
	oldStepX = ((int)(lastActualMeter-pDoc->GetTrendStep())/((int)pDoc->GetTrendStep()*c_stepScaler)) % (c_numStepX) ; // Uno qualunque va bene

	// estremi del quadro
	BOOL onlyUpdate=FALSE;
	if ((oldStepX<0)||(actualStepX <= 0)||(actualStepX == c_numStepX))
		{
		rcInvalid = rectB;
		rcInvalid.left -= 1;
		rcInvalid.right += 1;
		}
	else
		{
		double rxr,rxl;
		onlyUpdate=TRUE;
		rxl = rectB.left;
		rxr = rectB.right;

		rcInvalid.left = (int ) (rxl +((rxr - rxl)/c_numStepX *oldStepX))-4;
		rcInvalid.right = (int )(rxl +((rxr - rxl)/c_numStepX *actualStepX));

		rcInvalid.top	= rectB.top;
		rcInvalid.bottom = rectB.bottom;
		}

		CDC  *pDC;
		pDC = GetDC ();
		if (pDC == NULL)
			return;
		pDC->IntersectClipRect (rcInvalid);
		layout.OnEraseDrawBkgnd(pDC);
		layout.updateDrawRegion(pDC);
		updateTrend(pDC,rectB);
		updateLabel();
		// update Lastactualmeter
		lastActualMeter = pDoc->c_difCoil.getMeter();
		ReleaseDC(pDC);
		
	}
}





void CMLineView::updateLabel(void)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

// Init vLabel 
// layout.setVFori	(pDoc->getFori());
// Call Base Class Draw
layout.c_vLabel[0] = pDoc->GetVPosition();
layout.c_vLabel [1] = pDoc->GetTrendLVPositionDal();
// densita D
	{
	CString str;
	layout.setMaxY('D',pDoc->GetTrendLScalaY());
	layout.setMinY('D',0.);

	double densita=pDoc->c_difCoil.getDensityLevel('D',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [2] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('D'))
		layout.c_vLabel[2].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[2].setBColor(RGB(0,255,0));
	}
//
// densita B
	{
	CString str;
	layout.setMaxY('C',pDoc->GetTrendLScalaY());
	layout.setMinY('C',0.);

	double densita=pDoc->c_difCoil.getDensityLevel('C',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [3] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('C'))
		layout.c_vLabel[3].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[3].setBColor(RGB(0,255,0));
	}
//
// densita B
	{
	CString str;
	layout.setMaxY('B',pDoc->GetTrendLScalaY());
	layout.setMinY('B',0.);

	double densita=pDoc->c_difCoil.getDensityLevel('B',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [4] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('B'))
		layout.c_vLabel[4].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[4].setBColor(RGB(0,255,0));
	}
//
// densita A
	{
	CString str;
	layout.setMaxY('A',pDoc->GetTrendLScalaY());
	layout.setMinY('A',0.);
	double densita=pDoc->c_difCoil.getDensityLevel('A',pDoc->c_densityCalcLength);
	str.Format("%6.0lf",densita);
	layout.c_vLabel [5] = str;		// densita` 
	if (densita > pDoc->c_difCoil.getAllarme ('A'))
		layout.c_vLabel[5].setBColor(RGB(255,0,0));
	else
		layout.c_vLabel[5].setBColor(RGB(0,255,0));
	}
//-----
layout.c_vLabel [6] = pDoc->getRotolo();
layout.c_vLabel [7] = pDoc->c_lega;
CTime d(pDoc->c_startTime);
layout.c_vLabel [8] = d.Format( "%A, %B %d, %Y" );
// Visualizzazione alzata
int metriAlzata;
int posAlzata = pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.at((pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size())-1).c_yPos;
metriAlzata = pDoc->GetVPosition() - posAlzata;
if((pDoc->c_rotoloMapping.size() == 1)&&
	(pDoc->c_rotoloMapping.at(0).c_alzate.size() == 1))
	metriAlzata -= pDoc->c_lunResStop;

CString s;
s.Format("%d",metriAlzata);
layout.c_vLabel [9] = s;

s.Format("%6.0lf",pDoc->c_densityCalcLength);
layout.c_vLabel [10] = s;

//s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).c_alzate.size());
// -1 indica tutti 
//s.Format("%d",pDoc->c_rotoloMapping.at(pDoc->c_rotoloMapping.c_lastElemento).getCurAlzNumber(-1));
s = pDoc->getCurAlzString();
layout.c_vLabel [11] = s;

// Total Holes A - 02-09-04
double va = pDoc->c_difCoil.getTotDifetti('A',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",va);
layout.c_vLabel [12] = s;
// Total Holes B
double vb = pDoc->c_difCoil.getTotDifetti('B',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vb);
layout.c_vLabel [13] = s;
// Total Holes C
double vc = pDoc->c_difCoil.getTotDifetti('C',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vc);
layout.c_vLabel [14] = s;
// Total Holes D
double vd = pDoc->c_difCoil.getTotDifetti('D',posAlzata,pDoc->GetVPosition());
s.Format("%6.0lf",vd);
layout.c_vLabel [15] = s;
// Total Holes A+B+C+D
s.Format("%6.0lf",va+vb+vc+vd);
layout.c_vLabel [16] = s;
// Limit A
s.Format("%4.0lf",pDoc->sogliaA);
layout.c_vLabel [17] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaB);
layout.c_vLabel [18] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaC);
layout.c_vLabel [19] = s;
// Limit B
s.Format("%4.0lf",pDoc->sogliaD);
layout.c_vLabel [20] = s;
// Strip Density
double de = pDoc->c_difCoil.getTotalDensity('A',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('B',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('C',posAlzata,pDoc->GetVPosition());
de += pDoc->c_difCoil.getTotalDensity('D',posAlzata,pDoc->GetVPosition());
if (de < 0.)
	de = 0.;
s.Format("%6.02lf",de);
layout.c_vLabel [21] = s;



}


void CMLineView::updateTrend(CDC *pDC,CRect rectB)
{
DOC_CLASS* pDoc = (DOC_CLASS* )GetDocument();

double maxValY = pDoc->GetTrendLScalaY();
double maxValX = pDoc->GetTrendLScalaX();
int code = pDoc->GetForiCod();
int numClassi = pDoc->c_difCoil.getNumClassi();
// update global variable
calcNumStepX(pDoc);

int viewTrendStep = (int)pDoc->GetTrendStep()*c_stepScaler;

int sizeInfo= 0;
for (int i=0;i<numClassi;i++)
	{// preparazione lineInfo generale
	lineInfo[i].max = pDoc->GetTrendLScalaY();
	lineInfo[i].min = 0.;
	lineInfo[i].limit = pDoc->c_difCoil.c_sogliaAllarme[i];
	lineInfo[i].count = (int)((((int)pDoc->c_difCoil.getMeter()/viewTrendStep) < c_numStepX)?
						(pDoc->c_difCoil.getMeter()/viewTrendStep) : c_numStepX); 
	lineInfo[i].numStep = c_numStepX;
	lineInfo[i].stepScaler = c_stepScaler;
	sizeInfo= (pDoc->c_difCoil.getMeter()-lastActualMeter)/viewTrendStep;
	lineInfo[i].actual	= (int) (pDoc->c_difCoil.getMeter()/viewTrendStep)% c_numStepX;
	if (sizeInfo < 0)
		return;
	// Aggiornato 1997 alloca memoria
	lineInfo[i].newVal (sizeInfo+1);
	}

for (int j=0;j<= sizeInfo;j++)
	{
	// trovo indice metro dif 
	int idx = pDoc->c_difCoil.getMeterIdxLStep(j*viewTrendStep);
	for (int i=0;i<numClassi;i++)
		{
		if (idx < 0)
			{
			lineInfo[i].val[j] = 0;
			lineInfo[i].valBig[j] = 0;
			}
		else
			{
			lineInfo[i].val[j] = pDoc->c_difCoil[idx].getValNum('A'+i);
			// lineInfo[i].val[j] = pDoc->c_difCoil.getValNumLStep('A'+i,j*viewTrendStep);
			if (i == (pDoc->c_difCoil.getNumClassi()-1))
				{
				lineInfo[i].valBig[j] = pDoc->c_difCoil[idx].getTotalBig();
				}
			else
				lineInfo[i].valBig[j] = 0;
			}
		}
	}


CRect subRectB[4];
for (int i=0;i<numClassi;i++)
	{
	trendLine[i].setMode (LINEAR);
	subRectB[i]=rectB;
	subRectB[i].TopLeft().y += (rectB.Size().cy/numClassi)*i;
	subRectB[i].BottomRight().y -= (rectB.Size().cy/numClassi)*(numClassi-(i+1));
	//trendLine[i].setScale(subRectB[i],lineInfo[i]);
	// test se cambiata dimensione vis
	BOOL trendRes = trendLine[i].addNewlInfo(subRectB[i],lineInfo[i],sizeInfo);
	if (trendRes)
		trendLine[i].draw(pDC,TRUE);
	else
		Invalidate();		
	}

}


void CMLineView::calcNumStepX (CLineDoc* pDoc)
{
	int maxValX = pDoc->GetTrendLScalaX();
	c_numStepX = (int)((double)maxValX / pDoc->GetTrendStep());	// numero Passi divisione Disegno
	// parametrizzato numStepX al maxvalx, massimo 2000 pt di risoluzione non ha senso
	// visualizzarne di piu`
	c_stepScaler = (int) ((double)maxValX/(2000.0*pDoc->GetTrendStep()));
	if (c_stepScaler <= 0)
		c_stepScaler = 1;
	c_numStepX = c_numStepX/c_stepScaler;

	if (c_numStepX <= 0)
		c_numStepX = 1;

return;
}
