#if !defined(AFX_DSTRIPPOSITION_H__629DBEB0_08B9_40FE_BAA7_675A62080FE4__INCLUDED_)
#define AFX_DSTRIPPOSITION_H__629DBEB0_08B9_40FE_BAA7_675A62080FE4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DStripPosition.h : header file
//
 
#include "RulerWnd.h"
#include "..\..\..\..\..\Library\UnicodeListCtrl\ListCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CDStripPosition dialog
  
class CDStripPosition : public CDialog
{
// Construction
public:
	CDStripPosition(CWnd* pParent = NULL);   // standard constructor

	CRulerWnd m_stripRuler;
	// valore offset a sx per agguistare posizione rispetto a riferimento esterno alla testa
//	double c_offsetSx;
	BOOL	c_hideMode;
	BOOL	c_fingerHwMode;

	CString	c_stripListHeader;
	CString	c_stripListHeaderInit;
	CString	c_stripListRow1;
	CString	c_fingerListHeader;
	CString	c_fingerListHeaderInit;
	CString	c_fingerListRow1;
	CString	c_stripListHeaderNumber;
	CString	c_fingerListHeaderNumber;
	CString	c_stripListSize;
	CString	c_fingerListPosition;

	BOOL	saveToProfile(void);
	BOOL	loadFromProfile(BOOL useFileInterface);
	BOOL	loadFromResource(void);
	void	UpdateRulerFromList();
	BOOL fillStripSizeArray(CArray <int,int> &stripSizeArray);
	BOOL loadFromFile(CString fileName);
// Dialog Data
	//{{AFX_DATA(CDStripPosition)
	enum { IDD = IDD_STRIP_POSITION };
	CButton	m_ctrlOk;
	CButton	m_ctrlOK;
//	CButton	m_ctrlUseTop;
//	CButton	m_ctrlUseBottom;
//	CStatic	m_ctrlCoil2Label;
//	CEdit	m_ctrlCoil2;
	CComboBox	m_ctrlSelRicetta;
	CString	m_EditSel;
	gxListCtrl	m_stripList2;
	gxListCtrl	m_stripList1;
//	int		m_numStrip;
	CString	m_orderNumber;
	CString	m_alloyType;
	CString	m_coil;
	double	m_thickness;
//	double	m_cutSize;
// 	double	m_firstCutter;
	double	m_width;
	int		m_mode;
//	CButton	m_ctrlMode;
//	CButton	m_ctrlMode1;
//	CButton	m_ctrlMode2;
//	int		m_useBottomHead;
	CString	m_coil2;
	BOOL	m_skipADef;
	CString	m_firstRising;
	CString	m_freeNote;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDStripPosition)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDStripPosition)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnUpdate1();
	afx_msg void OnUpdate2();
	afx_msg void OnChangeNumStrip();
	afx_msg void OnChangeFirstCutter();
	afx_msg void OnChangeCutSize();
	afx_msg void OnMode();
	afx_msg void OnMode1();
	afx_msg void OnMode2();
	afx_msg void OnUseBottomHead();
	afx_msg void OnUseTopHead();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CButton m_ctrlUpdateStripSize;
	CEdit m_ctrlStripWidth;
	double m_trimLeft;
	double m_trimRight;
	double m_stripWidth;
	BOOL m_oneStripSize;
	CString m_msg1;
	int m_numStrip;
	// orientamento strip Right/left bool;
	int	c_rightAlignStrip;

	CString m_labelTrimLeft;
	CString m_labelTrimRight;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DSTRIPPOSITION_H__629DBEB0_08B9_40FE_BAA7_675A62080FE4__INCLUDED_)
