// BlackBox.cpp: implementation of the CBlackBox class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CSMService.h"
#include "BlackBox.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBlackBox::CBlackBox()
{

}

CBlackBox::~CBlackBox()
{
c_mapMsg.clear();
}

BlackMsg CBlackBox::getMsg(BlackRec *pRec)
{

BlackMsg msg;
BlackMsgMap::iterator it;
    
it = c_mapMsg.find(pRec->type);
if (it == c_mapMsg.end())
	{// Not Found
	msg.type = pRec->type;
	msg.msg = "ERR: Codice Messaggio Sconosciuto !!";
	}
else
	{
	msg = (*it).second;
	}

return msg;
}

BOOL CBlackBox::loadMsg(CString fileName)
{
BlackMsg bMsg;
char scratch [255];


FILE *fp;

fp = fopen((LPCSTR)fileName,"rb");
if (fp == NULL)
	return FALSE;
fseek(fp,0L,SEEK_SET);

while (!feof(fp))
	{
	if (fscanf (fp,"%d ",&bMsg.type)==-1)break;
//	if (fscanf (fp,"%x ",&val)==-1)break;
	fscanf (fp,"%[^\n]%*c",scratch);
	if (scratch[strlen(scratch)-1] == '\r')
		scratch[strlen(scratch)-1] = 0;
	bMsg.msg = scratch;
	c_mapMsg.insert(BlackMsgMap::value_type(bMsg.type,bMsg));
	}
fclose(fp);
return TRUE;
}
