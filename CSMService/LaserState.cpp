// LaserState.cpp : implementation file
//

#include "stdafx.h"
#include "CSMService.h"
#include "LaserState.h"
#include "CProfile.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLaserState

CLaserState::CLaserState()
{
CProfile profile;
// default 24 laser
setLaserNumber(24);
setBaseLaserNumber(0);
setAlarmBit(1);

int bitMask = profile.getProfileInt("init","BitMaskA",0x02);
setBitMask('A',bitMask);
bitMask = profile.getProfileInt("init","BitMaskB",0x08);
setBitMask('B',bitMask);
// gestisco solo a e b
bitMask = profile.getProfileInt("init","BitMaskC",0x01);
setBitMask('C',bitMask);
bitMask = profile.getProfileInt("init","BitMaskD",0x04);
setBitMask('D',bitMask);

}

CLaserState::~CLaserState()
{
c_laserSt.clear();
}


BEGIN_MESSAGE_MAP(CLaserState, CStatic)
	//{{AFX_MSG_MAP(CLaserState)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLaserState message handlers

void CLaserState::OnPaint() 
{
CPaintDC dc(this); // device context for painting

CRect wParentRect;
GetParent()->GetClientRect(wParentRect);

// TODO: Add your message handler code here
CRect r,rl;
GetClientRect(&r);

// Numero cella inizio/fine
CSize sz = dc.GetTextExtent(CString("0"));

// calcolo dimensione x
rl = r;
rl.DeflateRect(3,3,3,3);
rl.bottom = rl.bottom - sz.cy ;
// monitor 1920 x xxx
double divider = 1.0; 
int xSize = 1;
// auto update
while (true)
	{	
	// monitor 1920 x xxx
	xSize = (int)(1.*(rl.bottom-rl.top)/divider)*1; // era 2.6, era 2.155, 3.575
	r.right = r.left + xSize*c_numLaser + 10; // deflate
	if (r.right < (0.99 * wParentRect.Size().cx)) break;else divider += 0.01;
	}

rl.right = rl.left + xSize;

dc.Draw3dRect(&r,RGB(255,255,255),RGB(64,64,64));
CBrush brushWhite(RGB(255, 255, 255));
CBrush brushGreen(RGB(0, 255, 0));
CBrush brushRed(RGB(255, 0, 0));
CBrush brushBlue(RGB(0,0,255));
CBrush brushGrey(RGB(196, 196, 196));

UINT bm = dc.SetBkMode (TRANSPARENT);
UINT rv; 
rv = dc.SetTextAlign(TA_CENTER | TA_BOTTOM);
CFont fnt;

for (int i=0;i<c_numLaser;i++)
	{
	CBrush* pOldBrush;
	dc.FillSolidRect(&rl,RGB(196,196,196));
	switch(c_laserSt[i].code)
		{
		default:
		case LASER_UNK:
#ifdef _DEBUG
			{
			pOldBrush = dc.SelectObject(&brushWhite);
			int radius;
			radius = rl.Height()/4;
			CRect rc;
			rc = rl;
			rc.bottom = rc.top + radius;
			if(c_alarmBit > 0)
				dc.Ellipse(&rc);
			rc.top = rc.bottom;
			rc.bottom = rc.top + radius;
			if(c_alarmBit > 1)
				dc.Ellipse(&rc);
			rc.top = rc.bottom;
			rc.bottom = rc.top + radius;
			if(c_alarmBit > 2)
				dc.Ellipse(&rc);
			rc.top = rc.bottom;
			rc.bottom = rc.top + radius;
			if(c_alarmBit > 3)
				dc.Ellipse(&rc);
			}
#else
			pOldBrush = dc.SelectObject(&brushWhite);
			dc.Ellipse(&rl);
#endif
			break;
		case LASER_OK:
			pOldBrush = dc.SelectObject(&brushGreen);
			dc.Ellipse(&rl);
			break;
		case LASER_FAIL_A1:
		case LASER_FAIL_A2:
		case LASER_FAIL_B1:
		case LASER_FAIL_C1:
		case LASER_FAIL_D1:
			if (c_laserSt[i].option == 0)
				{
				pOldBrush = dc.SelectObject(&brushRed);
				dc.Ellipse(&rl);
				}
			else
				{
				CRect rc;
				int radius;
				radius = rl.Height()/4;
				rc = rl;
				rc.bottom = rc.top + radius;
				if(c_alarmBit > 0)
					{
					if (c_laserSt[i].option & c_bitMask[0])
						{
						pOldBrush = dc.SelectObject(&brushRed);
						dc.Ellipse(&rc);
						}
					else
						{
						pOldBrush = dc.SelectObject(&brushGreen);
						dc.Ellipse(&rc);
						}
					}
				rc.top = rc.bottom;
				rc.bottom = rc.top + radius;
				if(c_alarmBit > 1)
					{
					if (c_laserSt[i].option & c_bitMask[1])
						{
						pOldBrush = dc.SelectObject(&brushRed);
						dc.Ellipse(&rc);
						}
					else
						{
						pOldBrush = dc.SelectObject(&brushGreen);
						dc.Ellipse(&rc);
						}
					}
				rc.top = rc.bottom;
				rc.bottom = rc.top + radius;
				if(c_alarmBit > 2)
					{
					if (c_laserSt[i].option & c_bitMask[2])
						{
						pOldBrush = dc.SelectObject(&brushRed);
						dc.Ellipse(&rc);
						}
					else
						{
						pOldBrush = dc.SelectObject(&brushGreen);
						dc.Ellipse(&rc);
						}
					}
				rc.top = rc.bottom;
				rc.bottom = rc.top + radius;
				if(c_alarmBit > 3)
					{
					if (c_laserSt[i].option & c_bitMask[3])
						{
						pOldBrush = dc.SelectObject(&brushRed);
						dc.Ellipse(&rc);
						}
					else
						{
						pOldBrush = dc.SelectObject(&brushGreen);
						dc.Ellipse(&rc);
						}
					}
				}
			break;
		case LASER_SLOW:
			pOldBrush = dc.SelectObject(&brushBlue);
			dc.Ellipse(&rl);
			break;
		}

//	dc.Ellipse(&rl);
	// put back the old objects
	dc.SelectObject(pOldBrush);
	// scritta
	// primo piu` multipli di 8
	if ((i == 0)||
		((i+1)%8 == 0))
		{
		CString s;
		s.Format("%d",c_numBase + i);
		if ((i == 0)&&
			(s.GetLength() > 1))
			dc.TextOut(rl.left+ (rl.right-rl.left),r.bottom-1,s);
		else
			dc.TextOut(rl.left + (rl.right-rl.left)/2,r.bottom-1,s);
		}
	// new pos
	rl.left = rl.right;
	rl.right = rl.left + xSize;
	}

	
// Do not call CStatic::OnPaint() for painting messages
}

void CLaserState::setLaserNumber(int nl)
{
//-----------
LState lState;
lState.code = LASER_UNK;
lState.option = 0; 
// 

c_laserSt.clear();

c_numLaser = nl;
for (int i=0;i<c_numLaser;i++)
	// c_laserSt.push_back(LASER_UNK);
	c_laserSt.push_back(lState);
}

void CLaserState::setLaserSt(int id, int st,int opt)
{
LState lState;
lState.code = st;
lState.option = opt;
 
if (id < c_laserSt.size())
	// c_laserSt[id] = st;
	c_laserSt[id] = lState;
}

void CLaserState::setBaseLaserNumber(int base)
{
c_numBase = base;
}

int CLaserState::getNumLaser()
{
return (c_numLaser);
}
