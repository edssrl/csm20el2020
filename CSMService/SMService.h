#if !defined(AFX_SMSERVICE_H__ABED2574_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
#define AFX_SMSERVICE_H__ABED2574_C21D_11D5_AAF8_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SMService.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSMService form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "../../../../../Library/UniCodeListCtrl/ListCtrl.h"
#include "LaserState.h"

#include "../Csm20El2013\PrintLibSrc\CPage.h"
#include "../Csm20El2013\PrintLibSrc\Dib.h"

#define ID_TIMER_TESTOFFLINE 0xf5
#define ID_TIMER_BLACKBOX 0xf6
 
 
class CSMService : public CFormView
{
protected:
	CSMService();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSMService)

	BOOL c_useBottom;
	BOOL c_splitHead;

	int c_numLaser;
	int c_numLaserLeft;

// Form Data
public:
	//{{AFX_DATA(CSMService)
	enum { IDD = IDD_SMSERVICE_FORM };
	CButton	m_testSxBottom;
	CButton	m_testDxBottom;
	CLaserState	m_laserSxBottom;
	CLaserState	m_laserDxBottom;
	CStatic	m_ctrlLegend;
	CButton	m_testSxTop;
	CButton	m_testDxTop;
	CLaserState	m_laserDxTop;
	CLaserState	m_laserSxTop;
	gxListCtrl	m_msgList;
	CString	m_msgTestDxTop;
	CString	m_msgTestSxTop;
	CString	m_msgBBox;
	CString	m_msgTestDxBottom;
	CString	m_msgTestSxBottom;
	//}}AFX_DATA

// Attributes
public:

BOOL c_isPreview;

// modalita` funzionamento singolo comando per 
// ogni testa ma se splitHead visualizza su due righe
BOOL	c_singleCommandHead;
int c_toCode;
int c_fromCode;
UINT c_timerId;
// cSideTest ora diventa bitmap 
enum SIDE_TEST {NO_TEST=0,LEFT_SIDE_TOP=0x01,RIGHT_SIDE_TOP=0x02,LEFT_SIDE_BOT=0x04,RIGHT_SIDE_BOT=0x08}; 
SIDE_TEST c_sideTest;

int c_leftMarg;		// margine sinistro report in mm
int c_rightMarg;	// margine destro report in mm
int c_topMarg;		// margine alto report in mm
int c_bottomMarg;	// margine basso report in mm
// Rect Print
CRect prLimit;

// Stato hw testa, verifica stato warmup 
int c_statoHde;

// Operations
public:
	void enableButton(BOOL enable);
	void onTestOfflineBottom(int numCells, unsigned char* pCell, bool fromTop=false);
	void onTestOfflineTop(int numCells, unsigned char* pCell, bool fromBottom=false);
	void gestValStatoHde (PVOID p,DWORD size);
	BOOL addBBMsg(BlackRec bRec,BOOL last );
	CRect calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border=NULL);
	void reportTest(CDC* pDC,CRect rcBounds);

	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSMService)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CSMService();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CSMService)
	afx_msg void OnMsgAggiorna();
	afx_msg void OnEditCut();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnTestdx();
	afx_msg void OnTestsx();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnVisualizzaMessaggi();
	afx_msg void OnTestdx2();
	afx_msg void OnTestsx2();
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnUpdateFilePrint(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI *pCmdUI);
	CStatic m_ctrlMsgTestDxTop;
	CStatic m_ctrlMsgTestDxBottom;
	CStatic m_ctrlMsgTestSxTop;
	CStatic m_ctrlMsgTestSxBottom;
	CStatic m_ctrlLabelBotDx;
	CStatic m_ctrlLabelBotSx;
	CStatic m_ctrlLabelTopDx;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SMSERVICE_H__ABED2574_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
