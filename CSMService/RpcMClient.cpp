// RpcMClient.cpp : implementation file
// It support multiple remote client 	  14/12/96

#include "stdafx.h"
#include "CProfile.h"
//#include "Registry.h"

#include "resource.h"

#include "RpcMClient.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CRpcMClient

CRpcMClient::CRpcMClient()
{
c_procServer = "RpcServer.exe";
c_dirServer = "";
c_serMsgReceived = FALSE;
c_rpcOffLine = FALSE;
}

CRpcMClient::~CRpcMClient()
{
}



/////////////////////////////////////////////////////////////////////////////
// CRpcClient message handlers

BOOL CRpcMClient::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
// TODO: Add your message handler code here and/or call default
return OnServerRequest((WPARAM)pWnd, (LPARAM)pCopyDataStruct);
}

void CRpcMClient::addRemote(int remoteId,CString &device)
{
Remote remInfo;

// Add remoteInfo to array
remInfo.id = remoteId;
remInfo.device = device;
												
c_remoteRpcId.Add (remInfo);

}

// Register this and ALL remote Client
void CRpcMClient::rpcRegister(CString& wndName,int thisId,RPCTYPE type)
{
CString strValue;

// Nessuna connessione richiesta
if (c_rpcOffLine)
	return;

// Local Info unique
c_thisRpcId = thisId;
c_thisWnd = wndName;
c_type = type;

// Init Server Process
CProfile registry;
//CRegistry registry ("EDS","CSM20");


c_dirServer = registry.getProfileString(_T("SERVER"), _T("Path"), 
				"");
c_procServer = c_dirServer;

c_procServer += registry.getProfileString(_T("SERVER"), _T("Name"), 
				"RpcServer.exe");


// Remote on serialPort
for (int i=0;i<c_remoteRpcId.GetSize();i++)
	{
	// Create Server process if needed
	RegisterClient (c_remoteRpcId[i].device,c_remoteRpcId[i].id);
	}

// FALSE -> do not send back notification on packet abort
RegisterClient (c_thisWnd,c_thisRpcId,4,FALSE);
}




// Register THIS application
BOOL CRpcMClient::RegisterClient (CString &nome,RPCID id,int frameSize,BOOL abortNotify)
{

// Nessuna connessione richiesta
if (c_rpcOffLine)
	return TRUE;

URpcInfo rpcI;
ClientInfo cInfo;
COPYDATASTRUCT cds;

int i;
for (i =0;i<nome.GetLength();i++)
	cInfo.c_name [i] = nome [i];
while (i < CINFOSIZENAME)
	cInfo.c_name[i++] = 0;

cInfo.c_id = id;
cInfo.c_type = c_type;
cInfo.c_FrameSize = frameSize;
cInfo.c_abortNotify = abortNotify;


rpcI.info.dest = RPCID_SERVER;
rpcI.info.sender = c_thisRpcId;
rpcI.info.command = REGISTER_CLIENT;
rpcI.info.size = sizeof (cInfo);



cds.dwData = rpcI.lwInfo;
cds.cbData = sizeof (cInfo);
cds.lpData = (void *) &cInfo;

CWnd *server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
if (!server)
	{
	PROCESS_INFORMATION ProcessInformation;
	STARTUPINFO StartUpInfo;
	
	::GetStartupInfo (&StartUpInfo);
	BOOL fSuccess = CreateProcess((LPCSTR)c_procServer,NULL,
				NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,
				NULL,(LPCSTR)c_dirServer,
				&StartUpInfo,&ProcessInformation);
  

	if (fSuccess) 
		{
		// Wait Creation Window RpcServer
		int res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
		if (res != 0)
			{
			res = WaitForInputIdle(ProcessInformation.hProcess,10000);// wait for 10 sec
			if (res != 0)
				{
				CString s;
				s = "Fail Wait Idle SERVER " + c_procServer;
				AfxMessageBox (s);
				}
			}
	 	CloseHandle(ProcessInformation.hThread);
		CloseHandle(ProcessInformation.hProcess);

		server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
		}
	else
		{
		CString s;
		s = "Unable to Create SERVER " + c_procServer;
		AfxMessageBox (s);
		return(TRUE);
		}
	}

if (server)
	{
	server->SendMessage (WM_COPYDATA,
		(WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(),
		// (WPARAM) this,
		(LPARAM)&cds);
	}
else
	{
	CString s;
	s = "Not Found SERVER " + c_procServer;
	AfxMessageBox (s);
	}
	

return(TRUE);
}


// Server Data/Request through WM_COPYDATA
LRESULT CRpcMClient::OnServerRequest(WPARAM wParam, LPARAM lParam)
{

COPYDATASTRUCT *pcds = (COPYDATASTRUCT *) lParam;
URpcInfo rpcInfo;

rpcInfo.lwInfo = pcds->dwData;

// Test For Local Command
if (rpcInfo.info.dest == c_thisRpcId)
	{
	serverRequest (rpcInfo.info.command,
				   rpcInfo.info.sender,
				   pcds->cbData,
				   pcds->lpData);
					
	}

return 1;
}


/////////////////////////////////////////////////////////////////////////////
// CRpcClient message handlers

// Send To all registered system
BOOL CRpcMClient::SendBroadcastGeneralCommand(Command *command)
{
BOOL valret = TRUE;
// Remote 
for (int i=0;i<c_remoteRpcId.GetSize();i++)
	{
	int remoteId = c_remoteRpcId[i].id;
	// Create Server process if needed
	if (!SendGeneralCommand (command,remoteId))
		valret = FALSE;
	
	}
return (valret);
}


BOOL CRpcMClient::SendGeneralCommand(Command *command,RPCID remId)
{

// Nessuna connessione richiesta
if (c_rpcOffLine)
	return TRUE;

URpcInfo rpcI;
COPYDATASTRUCT cds;

// Dest HC16

rpcI.info.dest = (unsigned char) remId;					 
rpcI.info.sender = c_thisRpcId;
rpcI.info.command = (char )command->cmd;
rpcI.info.size = (char)command->size;



cds.dwData = rpcI.lwInfo;
cds.cbData = command->size;
cds.lpData = (void *) command->data;

CWnd *server = CWnd::FindWindow(RPC_SERVER_CLASSNAME,NULL);
if (!server)
	{
	AfxMessageBox ("SERVER not Found");
	return(FALSE);
	}

if (server)
	{
	server->SendMessage (WM_COPYDATA,
		(WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(),
		// (WPARAM) this,
		(LPARAM)&cds);
	}
else
	{
	AfxMessageBox ("Not Found SERVER");
	return (FALSE);
	}

return(TRUE);
}


BOOL CRpcMClient::serverRequest (int cmd,int sender,int size,void *pter)
{

// Override this virtual function in CMainFrame

return FALSE;
}
