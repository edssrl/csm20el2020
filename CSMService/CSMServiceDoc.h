// CSMServiceDoc.h : interface of the CCSMServiceDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CSMSERVICEDOC_H__ABED256A_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
#define AFX_CSMSERVICEDOC_H__ABED256A_C21D_11D5_AAF8_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "BlackBox.h"

class CCSMServiceDoc : public CDocument
{
protected: // create from serialization only
	CCSMServiceDoc();
	DECLARE_DYNCREATE(CCSMServiceDoc)

// Attributes
public:

	CBlackBox c_blackBox;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSMServiceDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCSMServiceDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCSMServiceDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSMSERVICEDOC_H__ABED256A_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
