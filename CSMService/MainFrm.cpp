// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "CSMService.h"

#include "CSMServicedoc.h"
#include "CProfile.h"
#include "MainFrm.h"
#include "SMService.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_COMMAND(ID_FILE_CANCELLAMEM, OnFileCancellamem)
	ON_COMMAND(ID_FILE_LANGUAGE, OnFileLanguage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
//	ID_INDICATOR_CAPS,
//	ID_INDICATOR_NUM,
//	ID_INDICATOR_SCRL,
};


/*-----------------------------------------------------------------

			SWAPLONG () 		routine swap long format
	
-----------------------------------------------------------------*/

union bytelong
	{
	unsigned char byte [4];
	unsigned long word;
	};

void swaplong (unsigned long *vett,int number)
{
union bytelong local;
int i;

unsigned long *lfrom;
unsigned char *bto;

lfrom = vett;
bto = (unsigned char *) vett;

for (i=0;i<number;i++)
	{
	local.word = *lfrom;     /* carico valore in union */
	
	bto [0] = local.byte[3];	/* swap */
	bto [1] = local.byte[2];
	bto [2] = local.byte[1];
	bto [3] = local.byte[0];
	bto += sizeof (long);	/* next word */
	lfrom ++;
	}
}	





/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
setPrinting (FALSE);	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
 
	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);

CProfile profile;
c_singleHeadDoubleHde = profile.getProfileBool("init","SingleHeadDoubleHde",TRUE);
BOOL useBottom = TRUE;
// Register to server
CString s1,s2;
s2 = profile.getProfileString(_T("init"), _T("CommDeviceTop"),_T("COM1")); 
c_hcSysIdTop = profile.getProfileInt(_T("init"), _T("IdDeviceTop"),RPCID_HC16CSM3); 

if (useBottom)
	{
	s1 = profile.getProfileString(_T("init"), _T("CommDeviceBottom"),_T("COM2")); 
	c_hcSysIdBottom = profile.getProfileInt(_T("init"), _T("IdDeviceBottom"),RPCID_HC16CSM2); 
	}

// Add remote system
addRemote(c_hcSysIdBottom,s1);	

// Add remote system
addRemote(c_hcSysIdTop,s2);
	
// Register this and remote system
c_ntSysId = profile.getProfileInt(_T("init"), _T("PCCommDevice"),RPCID_NTCSM); 
	
char name [81];
::GetClassName(GetSafeHwnd(),name,80);
s1 = name;
rpcRegister(s1,c_ntSysId);

return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers



BOOL CMainFrame::serverRequest (int cmd,int sender,int size,void *pter)
{
// Override this virtual function in CMainFrame
static int numRecord = 0;

if (isPrinting())
	return FALSE;

CSMService* pView;

pView = (CSMService*) GetActiveView();

if (pView == NULL)
	return FALSE;

switch(cmd)
	{
	case 0:
		break;
	case VAL_STATO :
		pView->gestValStatoHde(pter,size);
		break;

	case VAL_BLACKBOX:
		if (size == sizeof(long))
			{
			unsigned long* pl;
			pl = (unsigned long*)pter;
			swaplong(pl,1);
			numRecord = *pl;
			}
		break;
	case VAL_RAWBLACKBOX:
		{
		BlackRec* pbRec;
		pbRec = (BlackRec*)pter;		
		for (int i=0;i<size/sizeof(BlackRec);i++)
			{
			// se utilizzato campo data (long) bisogna swappare
			swaplong((unsigned long*)&(pbRec->date),1);
			numRecord--;
			// last ?
			pView->addBBMsg(*pbRec,!numRecord);
			pbRec ++;
			}
		}
		break;
	case VAL_TESTOFFLINE:
		{
		if (c_singleHeadDoubleHde)
			{
			pView->onTestOfflineTop(size,(unsigned char*)pter,(sender == RPCID_HC16CSM2));
			}
		else
			{
			if (sender == RPCID_HC16CSM2)
				pView->onTestOfflineBottom(size,(unsigned char*)pter,(sender == RPCID_HC16CSM3));
			else
				pView->onTestOfflineTop(size,(unsigned char*)pter,(sender == RPCID_HC16CSM2));
			}
		}
		break;
	default:
		break;
	}


return FALSE;
}

BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct) 
{
// TODO: Add your message handler code here and/or call default
return OnServerRequest((WPARAM)pWnd, (LPARAM)pCopyDataStruct);

}


void CMainFrame::OnFileCancellamem() 
{
// TODO: Add your command handler code here
CString s;
s.LoadString(CSMSERVICE_CLEAR_MSG);
// if (AfxMessageBox("Cancellare tutti i messaggi dalla memoria della testa?",
if (AfxMessageBox(s,
			MB_ICONQUESTION | MB_YESNO) == IDNO)
			return;

// start test Dx
// Invio msg 
Command cmd;
cmd.cmd = SET_CLEARBLACKBOX;
cmd.size = 0;
cmd.data = NULL;

if (!SendGeneralCommand(&cmd,(RPCID) c_hcSysIdTop))
	{
	// AfxMessageBox("Errore nella cancellazione della memoria");
	CString s;
	s.LoadString(CSMSERVICE_FAIL_CLEARMEM);
	AfxMessageBox(s);
	}
else
	{
	// AfxMessageBox("Cancellazione eseguita");
	CString s;
	s.LoadString(CSMSERVICE_OK_CLEARMEM);
	AfxMessageBox(s);
	}

	
}

void CMainFrame::OnFileLanguage() 
{
CProfile profile;
		
CString s;
//s = "Utilizzare Lingua Italiana?";
s.LoadString(SMSERVICE_ITALIAN_LANG);

if (AfxMessageBox((LPCSTR)s ,MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
	profile.writeProfileString("init","LangDll","NONE");
	CString s;
	//s = "Per cambiare la lingua chiudere e riavviare il programma";
	s.LoadString(SMSERVICE_RESTART_LANG);

	AfxMessageBox((LPCSTR)s,MB_ICONEXCLAMATION);
	return;
	}
	

// TODO: Add your command handler code here
CString szFilter ("dll Files (*.dll)|*.dll|All Files (*.*)|*.*||");

CFileDialog dialog (TRUE,(LPCSTR) "*.dll",
	(LPCSTR) "*.dll",0,(LPCSTR) szFilter);

SetCurrentDirectory(".");

if (dialog.DoModal() == IDOK)
	{
	CString dllName;
	dllName = dialog.GetPathName();
	profile.writeProfileString("init","LangDll",dllName);
	
	CString s;
	//s = "Per cambiare la lingua chiudere e riavviare il programma";
	s.LoadString(SMSERVICE_RESTART_LANG);

	AfxMessageBox((LPCSTR)s,MB_ICONEXCLAMATION);
	}


}
