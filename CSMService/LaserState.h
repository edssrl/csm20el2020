#if !defined(AFX_LASERSTATE_H__F5C6A740_C461_11D5_AAFB_00C026A019B7__INCLUDED_)
#define AFX_LASERSTATE_H__F5C6A740_C461_11D5_AAFB_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LaserState.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLaserState window

#include <vector>
using namespace std;

#define LASER_UNK		0
#define LASER_OK		1
#define LASER_SLOW		2	 
#define LASER_FAIL_A1	3 
#define LASER_FAIL_A2	4 
#define LASER_FAIL_B1	5 
#define LASER_FAIL_C1	6 
#define LASER_FAIL_D1	7 

struct LState
{
int code;
int option;
};

class CLaserState : public CStatic
{
// Construction
public:
	CLaserState();

// Attributes
public:
	vector<LState> c_laserSt;		// stato dei laser 
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLaserState)
	//}}AFX_VIRTUAL

// Implementation
public:
	int getNumLaser();
	void setBaseLaserNumber(int base);
	int getBaseLaserNumber(void){return c_numBase;};
	void setLaserNumber(int nl);
	void setLaserSt(int id,int st,int opt=0);
	void setAlarmBit(int abit){c_alarmBit = abit;};
	// code A,B,C,D quattro classi!
	void setBitMask(char code,int mask)
		{c_bitMask[code-'A']=mask;};
	virtual ~CLaserState();

private:
	int c_numLaser;
	int c_numBase;
	int c_alarmBit;
	int c_bitMask[4];


	// Generated message map functions
protected:
	//{{AFX_MSG(CLaserState)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LASERSTATE_H__F5C6A740_C461_11D5_AAFB_00C026A019B7__INCLUDED_)
