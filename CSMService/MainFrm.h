// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__ABED2568_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
#define AFX_MAINFRM_H__ABED2568_C21D_11D5_AAF8_00C026A019B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../Csm20El2013/RpcMClient.h"

class CMainFrame : public CFrameWnd, public CRpcMClient
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	int c_hcSysIdTop;
	int c_hcSysIdBottom;
	int c_ntSysId;

	// modalita` singola testa, doppio hde
	BOOL	c_singleHeadDoubleHde;

	BOOL c_isPrinting;
	CCriticalSection c_critSect;

// Operations
public:
	virtual BOOL serverRequest (int cmd,int sender,int size,void *pter);

	void setPrinting(BOOL v)
	{c_critSect.Lock();c_isPrinting = v;c_critSect.Unlock();};
	BOOL isPrinting(void)
	{BOOL vret; c_critSect.Lock();vret = c_isPrinting;c_critSect.Unlock();return vret;};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnFileCancellamem();
	afx_msg void OnFileLanguage();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__ABED2568_C21D_11D5_AAF8_00C026A019B7__INCLUDED_)
