// Dmessaggi.cpp : implementation file
//

#include "stdafx.h"
#include "CSMService.h"
#include "Dmessaggi.h"

#include "CProfile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDmessaggi dialog


CDmessaggi::CDmessaggi(CWnd* pParent /*=NULL*/)
	: CDialog(CDmessaggi::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDmessaggi)
	m_fromCode = 0;
	m_toCode = 0;
	//}}AFX_DATA_INIT
}


void CDmessaggi::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDmessaggi)
	DDX_Text(pDX, IDC_FROMCODE, m_fromCode);
	DDV_MinMaxInt(pDX, m_fromCode, 0, 255);
	DDX_Text(pDX, IDC_TOCODE, m_toCode);
	DDV_MinMaxInt(pDX, m_toCode, 0, 255);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDmessaggi, CDialog)
	//{{AFX_MSG_MAP(CDmessaggi)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDmessaggi message handlers

void CDmessaggi::saveToProfile()
{
CProfile profile;

CString s;
s.Format("%d",m_fromCode);
profile.writeProfileString("Messaggi","FromCode",(LPCSTR)s);
s.Format("%d",m_toCode);
profile.writeProfileString("Messaggi","ToCode",(LPCSTR)s);

}


void CDmessaggi::loadFromProfile()
{
CProfile profile;

m_fromCode = profile.getProfileInt("Messaggi","FromCode",0);
m_toCode = profile.getProfileInt("Messaggi","ToCode",255);

}
