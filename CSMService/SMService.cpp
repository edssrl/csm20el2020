// SMService.cpp : implementation file
//

#include "stdafx.h"
#include "CSMService.h"
#include "../Csm20El2013/RpcMClient.h"
#include "CSMServiceDoc.h"
#include "MainFrm.h"

#include "BlackBox.h"
#include "SMService.h"
#include "CProfile.h"
#include "DMessaggi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSMService

IMPLEMENT_DYNCREATE(CSMService, CFormView)

CSMService::CSMService()
	: CFormView(CSMService::IDD)
{
	//{{AFX_DATA_INIT(CSMService)
	m_msgTestDxTop = _T("");
	m_msgTestSxTop = _T("");
	m_msgBBox = _T("");
	m_msgTestDxBottom = _T("");
	m_msgTestSxBottom = _T("");
	//}}AFX_DATA_INIT

// No test
c_sideTest = NO_TEST;
c_isPreview = FALSE;

c_leftMarg = 20;		// margine sinistro report in mm
c_rightMarg = 10;	// margine destro report in mm
c_topMarg = 20;		// margine alto report in mm
c_bottomMarg = 15;	// margine basso report in mm

c_statoHde	= HDE_STATE;

}

CSMService::~CSMService()
{
}

void CSMService::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSMService)
	DDX_Control(pDX, IDC_TESTSX2, m_testSxBottom);
	DDX_Control(pDX, IDC_TESTDX2, m_testDxBottom);
	DDX_Control(pDX, IDC_LASERSX2, m_laserSxBottom);
	DDX_Control(pDX, IDC_LASERDX2, m_laserDxBottom);
	DDX_Control(pDX, IDC_LEGEND, m_ctrlLegend);
	DDX_Control(pDX, IDC_TESTSX, m_testSxTop);
	DDX_Control(pDX, IDC_TESTDX, m_testDxTop);
	DDX_Control(pDX, IDC_LASERDX, m_laserDxTop);
	DDX_Control(pDX, IDC_LASERSX, m_laserSxTop);
	DDX_Control(pDX, IDC_LIST_BLACKMSG, m_msgList);
	DDX_Text(pDX, IDC_MSGDX, m_msgTestDxTop);
	DDX_Text(pDX, IDC_MSGSX, m_msgTestSxTop);
	DDX_Text(pDX, IDC_MSG_BBOX, m_msgBBox);
	DDX_Text(pDX, IDC_MSGDX2, m_msgTestDxBottom);
	DDX_Text(pDX, IDC_MSGSX2, m_msgTestSxBottom);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_MSGDX, m_ctrlMsgTestDxTop);
	DDX_Control(pDX, IDC_MSGDX2, m_ctrlMsgTestDxBottom);
	DDX_Control(pDX, IDC_MSGSX, m_ctrlMsgTestSxTop);
	DDX_Control(pDX, IDC_MSGSX2, m_ctrlMsgTestSxBottom);
	DDX_Control(pDX, IDC_LABEL_BOTDX, m_ctrlLabelBotDx);
	DDX_Control(pDX, IDC_LABEL_BOTSX, m_ctrlLabelBotSx);
	DDX_Control(pDX, IDC_LABEL_TOPDX, m_ctrlLabelTopDx);
}


BEGIN_MESSAGE_MAP(CSMService, CFormView)
	//{{AFX_MSG_MAP(CSMService)
	ON_BN_CLICKED(IDC_MSG_AGGIORNA, OnMsgAggiorna)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_BN_CLICKED(IDC_TESTDX, OnTestdx)
	ON_BN_CLICKED(IDC_TESTSX, OnTestsx)
	ON_WM_TIMER()
	ON_COMMAND(ID_VISUALIZZA_MESSAGGI, OnVisualizzaMessaggi)
	ON_BN_CLICKED(IDC_TESTDX2, OnTestdx2)
	ON_BN_CLICKED(IDC_TESTSX2, OnTestsx2)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, OnFilePrintPreview)
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, &CSMService::OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, &CSMService::OnUpdateFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSMService diagnostics

#ifdef _DEBUG
void CSMService::AssertValid() const
{
	CFormView::AssertValid();
}

void CSMService::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSMService message handlers

void CSMService::OnInitialUpdate() 
{
CFormView::OnInitialUpdate();
	
// TODO: Add your specialized code here and/or call the base class
m_msgList.LoadColumnHeadings(IDS_HDLIST_BBOXMSG);
m_msgList.AdjustColumnWidths();	
m_msgList.modeFullRowSelect(true);

// limiti dei bbmsg visualizzati
CDmessaggi dialog;
dialog.loadFromProfile();
c_fromCode = dialog.m_fromCode;
c_toCode = dialog.m_toCode;

//-------------------------
// Init numero celle
CProfile profile;
c_numLaser = profile.getProfileInt("init","NumeroBande",272);
c_numLaserLeft = profile.getProfileInt("init","NumeroBandeLeft",136);
int nBitAlarm = profile.getProfileInt("init","AlarmBit",3);
c_singleCommandHead = profile.getProfileBool("Layout","SingleCommandHead",FALSE);

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
if (frame->c_singleHeadDoubleHde)
	{
	c_useBottom = FALSE;
	c_splitHead = TRUE;
	}
else
	{
	c_useBottom = profile.getProfileBool("Layout","UseBottom",FALSE);
	c_splitHead = profile.getProfileBool("Layout","SplitHead",FALSE);
	}

// TOP section
if (c_splitHead)
	{
	m_laserSxTop.setLaserNumber(c_numLaser/2);
	m_laserDxTop.setLaserNumber(c_numLaser/2);

	m_laserSxTop.setBaseLaserNumber(1);
	m_laserDxTop.setBaseLaserNumber(c_numLaser/2+1);
	//
	m_laserSxTop.setAlarmBit(nBitAlarm);
	m_laserDxTop.setAlarmBit(nBitAlarm);
	if (c_singleCommandHead)
		{// disablito pulsante Testa Top Dx
		m_testDxTop.ShowWindow(FALSE);
		}
	}
else
	{
	m_laserSxTop.setLaserNumber(c_numLaser);
	m_laserSxTop.setBaseLaserNumber(1);
	m_laserSxTop.setAlarmBit(nBitAlarm);
	m_laserDxTop.ShowWindow(FALSE);
	m_ctrlMsgTestDxTop.ShowWindow(FALSE);
	m_testDxTop.ShowWindow(FALSE);
	}
// BOTTOM section
if (c_useBottom)
	{
	// 
	if (c_splitHead)
		{
		m_laserSxBottom.setLaserNumber(c_numLaser/2);
		m_laserDxBottom.setLaserNumber(c_numLaser/2);

		m_laserSxBottom.setBaseLaserNumber(1);
		m_laserDxBottom.setBaseLaserNumber(c_numLaser/2+1);
		m_laserSxBottom.setAlarmBit(nBitAlarm);
		m_laserDxBottom.setAlarmBit(nBitAlarm);
		if (c_singleCommandHead)
			{// disablito pulsante Testa Top Dx
			m_testDxBottom.ShowWindow(FALSE);
			}
		}
	else
		{
		m_laserSxBottom.setLaserNumber(c_numLaser);
		m_laserSxBottom.setBaseLaserNumber(1);
		m_laserSxBottom.setBaseLaserNumber(1);
		m_laserSxBottom.setAlarmBit(nBitAlarm);
		m_laserDxBottom.ShowWindow(FALSE);
		m_ctrlMsgTestDxBottom.ShowWindow(FALSE);
		m_ctrlLabelBotDx.ShowWindow(FALSE);
		m_testDxBottom.ShowWindow(FALSE);
		}
	}
else
	{
	m_laserDxBottom.ShowWindow(FALSE);
	m_laserSxBottom.ShowWindow(FALSE);
	m_ctrlMsgTestDxBottom.ShowWindow(FALSE);
	m_ctrlMsgTestSxBottom.ShowWindow(FALSE);
	m_ctrlLabelBotDx.ShowWindow(FALSE);
	m_ctrlLabelBotSx.ShowWindow(FALSE);
	m_testDxBottom.ShowWindow(FALSE);
	m_testSxBottom.ShowWindow(FALSE);
	}


if (nBitAlarm != 3)
	m_ctrlLegend.ShowWindow(FALSE);

}

void CSMService::OnMsgAggiorna() 
{
// Invio msg di richiesta bbox
Command cmd;

cmd.cmd = INQ_BLACKBOX;
cmd.size = 0;
cmd.data = NULL;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
if (!frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysIdTop))
	{
	// m_msgBBox = "Invio del comando aggiornamento messaggi FALLITO";
	m_msgBBox.LoadString(SMSERVICE_SEND_FAIL_MSG);
	}
else
	{
	// m_msgBBox = "Aggiornamento messaggi in corso";
	m_msgBBox.LoadString( SMSERVICE_SENDING_MSG);
	m_msgList.DeleteAllItems();
	}

c_sideTest = NO_TEST;
CProfile profile;
int sec = profile.getProfileInt("init","TimeoutBlackBox",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_BLACKBOX,sec,NULL);
enableButton(FALSE);

}

BOOL CSMService::addBBMsg(BlackRec bRec,BOOL last)
{

CCSMServiceDoc *pDoc;
pDoc = (CCSMServiceDoc *)GetDocument();
if (pDoc == NULL)
	return FALSE;

BlackMsg bMsg;
bMsg = pDoc->c_blackBox.getMsg(&bRec);

if ((bRec.type >= c_fromCode)&&
	(bRec.type <= c_toCode))
	{
	CString s;
	s.Format("%d",bRec.type);
	int row = m_msgList.GetItemCount();
	m_msgList.AddRow((LPCSTR)s);
	m_msgList.AddItem(row,1,bMsg.msg);
	m_msgList.AddItem(row,2,bRec.val,"%d");
	}

if (last)
	{
	KillTimer(c_timerId);
	//m_msgBBox = "Aggiornamento messaggi Terminato con successo";
	m_msgBBox.LoadString(SMSERVICE_OK_MSG);
	enableButton(TRUE);
	UpdateData(FALSE);
	Beep(500,300);
	Sleep(200);
	Beep(1000,300);
	}

return TRUE;
}

void CSMService::OnEditCut() 
{
// TODO: Add your command handler code here

m_msgList.DeleteSelection(TRUE,TRUE);
	
}

void CSMService::OnEditCopy() 
{
	// TODO: Add your command handler code here
m_msgList.copySelToClipboard(TRUE);	
}

void CSMService::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(m_msgList.GetSelectedCount());	
}

void CSMService::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
// TODO: Add your command update UI handler code here
pCmdUI->Enable(m_msgList.GetSelectedCount());	
}

// invio richiesta Destra Top
void CSMService::OnTestdx() 
{
// TODO: Add your control notification handler code here

if(c_statoHde == HDE_WARMUP)
	{
	AfxMessageBox("Waiting Warmup,\n Please repeat Start",MB_ICONEXCLAMATION);
	return;
	}

CProfile profile;
CString s;
// s = profile.getProfileString("Messaggi","OnTestDx","!! ATTENZIONE !! Assicurarsi di aver posizionato lso schermo sulla parte destra della Testa");
s.LoadString(SMSERVICE_SET_SHIELD_DX); 

if (AfxMessageBox((LPCSTR)s,MB_OKCANCEL | MB_ICONEXCLAMATION)==IDCANCEL)
	return;

// start test Dx
// start test Sx
for(int i=0;i<m_laserDxTop.getNumLaser();i++)
	{
	m_laserDxTop.setLaserSt(i,LASER_UNK);
	}
m_laserDxTop.Invalidate();

// Invio msg 
Command cmd;
char ch;
cmd.cmd = VAL_COMMAND;
cmd.size = 1;
cmd.data = &ch;

ch = TESTOFFLINEDX;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
// invio a master
if (!frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysIdTop))
	{
	// m_msgTestDx = "Invio del comando di test FALLITO";
	m_msgTestDxTop.LoadString(SMSERVICE_SEND_FAIL_TEST);
	}
else
	{
	// m_msgTestDx = "Test Offline lato Destro in corso";
	m_msgTestDxTop.LoadString(SMSERVICE_RUNNING_TEST_DX);
	}
// test dx even
c_sideTest = RIGHT_SIDE_TOP;
int sec = profile.getProfileInt("init","TimeoutTestOffline",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_TESTOFFLINE,sec,NULL);
enableButton(FALSE);
UpdateData(FALSE);	
	
}

// invio richiesta Sinistra Top
void CSMService::OnTestsx() 
{

if(c_statoHde == HDE_WARMUP)
	{
	AfxMessageBox("Waiting Warmup,\n Please repeat Start",MB_ICONEXCLAMATION);
	return;
	}

// TODO: Add your control notification handler code here
CProfile profile;
CString s;
// s = profile.getProfileString("Messaggi","OnTestSx","");
s.LoadString(SMSERVICE_SET_SHIELD_SX);

if (AfxMessageBox((LPCSTR)s,MB_OKCANCEL | MB_ICONEXCLAMATION)==IDCANCEL)
	return;

// start test Sx
for(int i=0;i<m_laserSxTop.getNumLaser();i++)
	{
	m_laserSxTop.setLaserSt(i,LASER_UNK);
	}
m_laserSxTop.Invalidate();
// clear anche parte dx
for(int i=0;i<m_laserDxTop.getNumLaser();i++)
	{
	m_laserDxTop.setLaserSt(i,LASER_UNK);
	}
m_laserDxTop.Invalidate();

// Invio msg 
Command cmd;
char ch;
cmd.cmd = VAL_COMMAND;
cmd.size = 1;
cmd.data = &ch;

ch = TESTOFFLINESX;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
// invio a master
bool vret = false;
if (frame->c_singleHeadDoubleHde)
	vret = frame->SendBroadcastGeneralCommand(&cmd);
else
	vret = frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysIdTop);

if (!vret)
	{
	// m_msgTestSx = "Invio del comando di test FALLITO";
	m_msgTestSxTop.LoadString(SMSERVICE_SEND_FAIL_TEST);
	if (frame->c_singleHeadDoubleHde)
		// anche dx
		m_msgTestDxTop.LoadString(SMSERVICE_SEND_FAIL_TEST);
	}
else
	{
	// m_msgTestSx = "Test Offline lato Sinistro in corso";
	m_msgTestSxTop.LoadString(SMSERVICE_RUNNING_TEST_SX);
	if (frame->c_singleHeadDoubleHde)
		// anche dx
		m_msgTestDxTop.LoadString(SMSERVICE_RUNNING_TEST_DX);
	}
// test sx odd
c_sideTest = LEFT_SIDE_TOP;
if (frame->c_singleHeadDoubleHde)
	c_sideTest = static_cast<SIDE_TEST> (c_sideTest | RIGHT_SIDE_TOP);

int sec = profile.getProfileInt("init","TimeoutTestOffline",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_TESTOFFLINE,sec,NULL);
enableButton(FALSE);
UpdateData(FALSE);	
}

// aggiornamento visualizzazione, numcells contiene dimensione pacchetto 
void CSMService::onTestOfflineTop(int numCells,unsigned char *pCell,bool fromBottom)
{

if ((!fromBottom) && (numCells != c_numLaserLeft))
{AfxMessageBox(_T("Error, wrong numLaser configuration left side"),MB_ICONERROR);return;}

if ((fromBottom) && (numCells != (c_numLaser- c_numLaserLeft)))
{AfxMessageBox(_T("Error, wrong numLaser configuration right side"),MB_ICONERROR);return;}


bool fromTop = !fromBottom;
// 1 e 2 Top
// 3 e 4 Bottom
if (((c_sideTest & LEFT_SIDE_TOP)+
	(c_sideTest & RIGHT_SIDE_TOP)) == 0)
	return; 
CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
// invio a master
bool singleHeadDoubleHde = frame->c_singleHeadDoubleHde;

CProfile profile;
BOOL slowMode = FALSE;
CLaserState* pLaser;
CString *pStatic;

int nBitAlarm = profile.getProfileInt("init","AlarmBit",3);

if (fromBottom)
	{// destra
	pLaser = &m_laserDxTop;
	pStatic = &m_msgTestDxTop;
	}
else
	{// sinistra
	pLaser = &m_laserSxTop;
	pStatic = &m_msgTestSxTop;
	}

for(int i=0;i<numCells;i++)
	{
	int laserIndex;
	if (singleHeadDoubleHde)
	{
	// singolo testa due hde, visualizzazione doppia, testa left sempre piu` lunga
	// visualizzazione simmetrica sopra e sotto e asimmetrica tra hde
	//  
	if (fromTop && (i < (c_numLaser/2)))
		{
		pLaser = &m_laserSxTop;
		laserIndex = i;
		}
	else
		{// qui se fromBottom or i > numCells/2 
		// cmq su parte dx
		pLaser = &m_laserDxTop;
		if(fromTop)
			{
			laserIndex = i - c_numLaser/2;
			}
		else
			// fromBottom, i parte da zero aggiungo 
			laserIndex = i + (c_numLaserLeft - (c_numLaser/2));
		}
	}
	else
	{
	if (c_singleCommandHead)
		{// singolo comando per testa ma visualizzazione doppia
		if (i < (numCells/2))
			{
			pLaser = &m_laserSxTop;
			laserIndex = i;
			}
		else
			{
			pLaser = &m_laserDxTop;
			laserIndex = i - (numCells/2);
			}
		}
	else
		laserIndex = i;
	}
	switch(*pCell)
		{
		default:
			pLaser->setLaserSt(laserIndex,LASER_UNK);
			break;
		case 0 :
			// Ok
			pLaser->setLaserSt(laserIndex,LASER_OK);
			break;
		case 1 :	// Bit 1 class C
		case 2 :	// Bit 2 class A
		case 3 :	// Bit 3 class D
		case 4 :	// Bit 4 class B
		case 5 :	// 
		case 6 :	// 
		case 7 :	// 
		case 8 :	// 
		case 9 :	// 
		case 10 :	// 
		case 11 :	// 
		case 12 :	// 
		case 13 :	// 
		case 14 :	// class ABD
		case 15 :	// class ABCD
		// fail
			if (nBitAlarm == 1)					
				{
				pLaser->setLaserSt(laserIndex,LASER_FAIL_A1);
				break;
				}
			if (*pCell & 0x01)
				{// fail C	 
				pLaser->setLaserSt(laserIndex,LASER_FAIL_A1,*pCell);
				}
				// fail A
			if (*pCell & 0x02)
				pLaser->setLaserSt(laserIndex,LASER_FAIL_A2,*pCell);
			
			if (*pCell & 0x04)
				// fail D
				pLaser->setLaserSt(laserIndex,LASER_FAIL_D1,*pCell);
	
			if (*pCell & 0x08)
				// fail B
				pLaser->setLaserSt(laserIndex,LASER_FAIL_B1,*pCell);
			break;
		case 0xff:
			// slow rotation
			pLaser->setLaserSt(laserIndex,LASER_SLOW);
			slowMode = TRUE;
			break;
		}
	pCell ++;
	}

if (slowMode)
	{
	// *pStatic = "Foro occluso o bassa velocita`del disco"; 
	CString s;
	s.LoadString(SMSERVICE_BLIND_HOLE);
	if (singleHeadDoubleHde)
		{
		if (fromBottom) m_msgTestDxTop = (LPCSTR)s;
		if (fromTop) m_msgTestSxTop = (LPCSTR)s;
		}
	else
		{
		*pStatic = (LPCSTR)s; 
		// stesso msg anche su altra barra
		if (c_singleCommandHead)
			m_msgTestDxTop = (LPCSTR)s; 
		}
	}
else
	{
	// *pStatic = "Test terminato con successo"; 
	CString s;
	s.LoadString(SMSERVICE_OK_TEST);
	if (singleHeadDoubleHde)
		{
		if (fromBottom) m_msgTestDxTop = (LPCSTR)s;
		if (fromTop)	m_msgTestSxTop = (LPCSTR)s;
		}
	else
		{
		*pStatic = (LPCSTR)s; 
		// stesso msg anche su altra barra
		if (c_singleCommandHead)
			m_msgTestDxTop = (LPCSTR)s; 
		}
	}
c_sideTest = static_cast<SIDE_TEST> (c_sideTest & ((fromBottom)?(~RIGHT_SIDE_TOP):(~LEFT_SIDE_TOP)));

if (c_singleCommandHead)
	{
	m_laserSxTop.Invalidate();
	m_laserDxTop.Invalidate();
	}
else
	pLaser->Invalidate();
UpdateData(FALSE);

if (c_sideTest == NO_TEST)
	{
	KillTimer(c_timerId);
	enableButton(TRUE);
	Beep(500,300);
	Sleep(200);
	Beep(1000,300);
	}
}

// aggiornamento visualizzazione 
void CSMService::onTestOfflineBottom(int numCells,unsigned char *pCell, bool fromTop)
{

// 1 e 2 Top
// 3 e 4 Bottom
if (c_sideTest <= 2)
	return; 

CProfile profile;
BOOL slowMode = FALSE;
CLaserState* pLaser;
CString *pStatic;

int nBitAlarm = profile.getProfileInt("init","AlarmBit",3);

if ((c_sideTest%2)>0)
	{// sinistra
	pLaser = &m_laserSxBottom;
	pStatic = &m_msgTestSxBottom;
	}
else	
	{// destra
	pLaser = &m_laserDxBottom;
	pStatic = &m_msgTestDxBottom;
	}

for(int i=0;i<numCells;i++)
	{
	int laserIndex;
	if (c_singleCommandHead)
		{// singolo comando per testa ma vsiualizzazione doppia
		if (i < (numCells/2))
			{
			pLaser = &m_laserSxBottom;
			laserIndex = i;
			}
		else
			{
			pLaser = &m_laserDxBottom;
			laserIndex = i - (numCells/2);
			}
		}
	else
		laserIndex = i;



	switch(*pCell)
		{
		default:
			pLaser->setLaserSt(laserIndex,LASER_UNK);
			break;
		case 0 :
			// Ok
			pLaser->setLaserSt(laserIndex,LASER_OK);
			break;
		case 1 :	// Bit 1 class C
		case 2 :	// Bit 2 class A
		case 3 :	// Bit 3 class D
		case 4 :	// Bit 4 class B
		case 5 :	// 
		case 6 :	// 
		case 7 :	// 
		case 8 :	// 
		case 9 :	// 
		case 10 :	// 
		case 11 :	// 
		case 12 :	// 
		case 13 :	// 
		case 14 :	// class ABD
		case 15 :	// class ABCD
			// fail
			if (nBitAlarm == 1)					
				{
				pLaser->setLaserSt(laserIndex,LASER_FAIL_A1);
				break;
				}
			if (*pCell & 0x01)
				{// fail C	 
				pLaser->setLaserSt(laserIndex,LASER_FAIL_A1,*pCell);
				}
				// fail A
			if (*pCell & 0x02)
				pLaser->setLaserSt(laserIndex,LASER_FAIL_A2,*pCell);
			
			if (*pCell & 0x04)
				// fail D
				pLaser->setLaserSt(laserIndex,LASER_FAIL_D1,*pCell);
	
			if (*pCell & 0x08)
				// fail B
				pLaser->setLaserSt(laserIndex,LASER_FAIL_B1,*pCell);
			break;
		case 0xff:
			// slow rotation
			pLaser->setLaserSt(laserIndex,LASER_SLOW);
			slowMode = TRUE;
			break;
		}
	pCell ++;
	}

if (slowMode)
	{
	// *pStatic = "Foro occluso o bassa velocita`del disco"; 
	CString s;
	s.LoadString(SMSERVICE_BLIND_HOLE);
	*pStatic = (LPCSTR)s; 
	// stesso msg anche su altra barra
	if (c_singleCommandHead)
		m_msgTestDxBottom = (LPCSTR)s; 
	}
else
	{
	// *pStatic = "Test terminato con successo"; 
	CString s;
	s.LoadString(SMSERVICE_OK_TEST);
	*pStatic = (LPCSTR)s; 
	// stesso msg anche su altra barra
	if (c_singleCommandHead)
		m_msgTestDxBottom = (LPCSTR)s; 
	}

c_sideTest = static_cast<SIDE_TEST> (c_sideTest & (fromTop)?(~LEFT_SIDE_TOP):(~RIGHT_SIDE_TOP) );
KillTimer(c_timerId);
enableButton(TRUE);
pLaser->Invalidate();
UpdateData(FALSE);
Beep(500,300);
Sleep(200);
Beep(1000,300);

}

void CSMService::OnTimer(UINT nIDEvent) 
{
// TODO: Add your message handler code here and/or call default
CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
if (pFrame->isPrinting())
	return;

if (nIDEvent == ID_TIMER_TESTOFFLINE)
	{
	if (c_sideTest & LEFT_SIDE_TOP)
		{	
		// m_msgTestSx = "TIMEOUT Test Offline lato Sinistro";	
		m_msgTestSxTop.LoadString(SMSERVICE_TIMEOUT_SX);	
		}
	if (c_sideTest & RIGHT_SIDE_TOP)
		{
		//m_msgTestDx = "TIMEOUT Test Offline lato Destro";
		m_msgTestDxTop.LoadString(SMSERVICE_TIMEOUT_DX);
		}
	if (c_sideTest & LEFT_SIDE_BOT)
		{	
		// m_msgTestSx = "TIMEOUT Test Offline lato Sinistro";	
		m_msgTestSxBottom.LoadString(SMSERVICE_TIMEOUT_SX);	
		}
	if (c_sideTest & RIGHT_SIDE_BOT)
		{
		//m_msgTestDx = "TIMEOUT Test Offline lato Destro";
		m_msgTestDxBottom.LoadString(SMSERVICE_TIMEOUT_DX);
		}
	c_sideTest = NO_TEST;
	enableButton(TRUE);
	}
if (nIDEvent == ID_TIMER_BLACKBOX)
	{
	// m_msgBBox = "TIMEOUT Aggiornamento Messaggi";
	m_msgBBox.LoadString(SMSERVICE_TIMEOUT_MSG);
	enableButton(TRUE);
	}
KillTimer(nIDEvent);
Beep(1500,300);
}

void CSMService::enableButton(BOOL enable)
{

m_testSxTop.EnableWindow(enable);
m_testDxTop.EnableWindow(enable);
m_testSxBottom.EnableWindow(enable);
m_testDxBottom.EnableWindow(enable);
//m_bBoxUpdate.EnableWindow(enable);

UpdateData(FALSE);
}

void CSMService::OnVisualizzaMessaggi() 
{
// TODO: Add your command handler code here
CDmessaggi dialog;

dialog.loadFromProfile();

if (dialog.DoModal() == IDOK)
	{
	dialog.saveToProfile();
	c_fromCode = dialog.m_fromCode;
	c_toCode = dialog.m_toCode;
	}
	
}

void CSMService::OnTestdx2() 
{

if(c_statoHde == HDE_WARMUP)
	{
	AfxMessageBox("Waiting Warmup,\n Please repeat Start",MB_ICONEXCLAMATION);
	return;
	}

// TODO: Add your control notification handler code here
CProfile profile;
CString s;
// s = profile.getProfileString("Messaggi","OnTestDx","!! ATTENZIONE !! Assicurarsi di aver posizionato lso schermo sulla parte destra della Testa");
s.LoadString(SMSERVICE_SET_SHIELD_DX2); 

if (AfxMessageBox((LPCSTR)s,MB_OKCANCEL | MB_ICONEXCLAMATION)==IDCANCEL)
	return;

// start test Dx
// start test Sx
for(int i=0;i<m_laserDxBottom.getNumLaser();i++)
	{
	m_laserDxBottom.setLaserSt(i,LASER_UNK);
	}
m_laserDxBottom.Invalidate();

// Invio msg 
Command cmd;
char ch;
cmd.cmd = VAL_COMMAND;
cmd.size = 1;
cmd.data = &ch;

ch = TESTOFFLINEDX;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
// Master e slave
if ((!frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysIdBottom))||
	(!frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysIdTop)))
	{
	// m_msgTestSx = "Invio del comando di test FALLITO";
	m_msgTestDxBottom.LoadString(SMSERVICE_SEND_FAIL_TEST);
	}
else
	{
	// m_msgTestDx = "Test Offline lato Destro in corso";
	m_msgTestDxBottom.LoadString(SMSERVICE_RUNNING_TEST_DX);
	}
// test dx even
c_sideTest = RIGHT_SIDE_BOT;
int sec = profile.getProfileInt("init","TimeoutTestOffline",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_TESTOFFLINE,sec,NULL);
enableButton(FALSE);
UpdateData(FALSE);	
	
}

void CSMService::OnTestsx2() 
{

if(c_statoHde == HDE_WARMUP)
	{
	AfxMessageBox("Waiting Warmup,\n Please repeat Start",MB_ICONEXCLAMATION);
	return;
	}

// TODO: Add your control notification handler code here
CProfile profile;
CString s;
// s = profile.getProfileString("Messaggi","OnTestSx","");
s.LoadString(SMSERVICE_SET_SHIELD_SX2);

if (AfxMessageBox((LPCSTR)s,MB_OKCANCEL | MB_ICONEXCLAMATION)==IDCANCEL)
	return;

// start test Sx
for(int i=0;i<m_laserSxBottom.getNumLaser();i++)
	{
	m_laserSxBottom.setLaserSt(i,LASER_UNK);
	}
m_laserSxBottom.Invalidate();
// Invio msg 
Command cmd;
char ch;
cmd.cmd = VAL_COMMAND;
cmd.size = 1;
cmd.data = &ch;

ch = TESTOFFLINESX;

CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
// Master e slave
if ((!frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysIdBottom))||
	(!frame->SendGeneralCommand(&cmd,(RPCID) frame->c_hcSysIdTop)))
	{
	// m_msgTestSx = "Invio del comando di test FALLITO";
	m_msgTestSxBottom.LoadString(SMSERVICE_SEND_FAIL_TEST);
	}
else
	{
	// m_msgTestSx = "Test Offline lato Sinistro in corso";
	m_msgTestSxBottom.LoadString(SMSERVICE_RUNNING_TEST_SX);
	}
// test sx odd
c_sideTest = LEFT_SIDE_BOT;
int sec = profile.getProfileInt("init","TimeoutTestOffline",5);
sec *= 1000;
c_timerId = SetTimer(ID_TIMER_TESTOFFLINE,sec,NULL);
enableButton(FALSE);
UpdateData(FALSE);	
	
}


//---------------------------------------------------------
//
//			 STAMPA
//
//---------------------------------------------------------

// Sequenza
// OnPreparePrinting
// DoPreparePrinting

// Loop OnPrint -> 1 sola pagina quindi molto facile
// OnEndPrinting
// Init device dependent

void CSMService::OnFilePrint() 
{
// TODO: Add your command handler code here
CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->setPrinting(TRUE);

CView::OnFilePrint();	
	
}

void CSMService::OnFilePrintPreview() 
{

// TODO: Add your command handler code here
c_isPreview = TRUE;
CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->setPrinting(TRUE);

CView::OnFilePrintPreview();	

}


BOOL CSMService::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
BOOL retVal = DoPreparePrinting (pInfo);
if (!retVal)
	{
	return(retVal);
	}

// Set max Number page	1
// 
CDC dc;
dc.Attach (pInfo->m_pPD->m_pd.hDC);
// save
CRect r;
r.SetRect(0, 0, 
             dc.GetDeviceCaps(HORZRES), 
             dc.GetDeviceCaps(VERTRES)) ;

r = calcPrintInternalRect(&dc,r);

pInfo->SetMaxPage((UINT) 1);
dc.Detach();
return retVal;	

}


void CSMService::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{

// TODO: Add your specialized code here and/or call the base class
CView::OnBeginPrinting(pDC, pInfo);

}

void CSMService::OnPrint(CDC* pDC, CPrintInfo* pInfo) 
{
// TODO: Add your specialized code here and/or call the base class
CRect rectBorder;
CProfile profile;


// GetClientRect
CRect rcBounds = calcPrintInternalRect(pDC,pInfo->m_rectDraw,&rectBorder);

// draw border
CCSMServiceDoc* pDoc = (CCSMServiceDoc* )GetDocument();
CPen p,*op;
p.CreatePen (PS_SOLID,2,RGB(0,0,0));
op = pDC->SelectObject(&p);
pDC->MoveTo(rectBorder.left,rectBorder.top);
pDC->LineTo(rectBorder.right,rectBorder.top);
pDC->LineTo(rectBorder.right,rectBorder.bottom);
pDC->LineTo(rectBorder.left,rectBorder.bottom);
pDC->LineTo(rectBorder.left,rectBorder.top);
pDC->SelectObject (op);


prLimit = rcBounds;

int page = pInfo->m_nCurPage;



CFont fontNormal,*pold;
fontNormal.CreatePointFont (120,"Arial",pDC);
pold = pDC->SelectObject(&fontNormal); 
CString s,strRes;
pDC->SetTextAlign(TA_CENTER | TA_TOP);
//strRes.Format ("Page %d",page);
strRes.LoadString (CSM_GRAPHVIEW_REPORTPAGINA);
s.Format (strRes,page);
CSize pagSize;
pagSize = pDC->GetTextExtent(s);
CPoint point;
// allineato al centro
point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;;
point.y = rectBorder.bottom + pagSize.cy;
pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// data ora allineata a dx
CString formatRes;
formatRes.LoadString (CSM_GRAPHVIEW_DATETIMEFORMAT);
CTime t;
t = CTime::GetCurrentTime();
s = t.Format(formatRes);
pDC->SetTextAlign(TA_RIGHT | TA_TOP);
point.x = rectBorder.right;
point.y = rectBorder.top - 2 * pagSize.cy;
pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// allineato al centro
//s = "File : ";
// s += pDoc->getFileName();
//pDC->SetTextAlign(TA_CENTER | TA_TOP);
//point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;
//point.y = rectBorder.top - 2 * pagSize.cy;
//pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf
s = profile.getProfileString("init","FactoryLine","WT_Separator");
// s += pDoc->getFileName();
pDC->SetTextAlign(TA_CENTER | TA_TOP);
point.x = rectBorder.left+(rectBorder.right-rectBorder.left)/2;
point.y = rectBorder.top - 2 * pagSize.cy;
pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

// Cercafori allineato a sx
s.LoadString (SMSERVICE_REPORT_APP);
// s = "Cercafori - EDS Srl";
pDC->SetTextAlign(TA_LEFT | TA_TOP);
point.x = rectBorder.left;
point.y = rectBorder.top - 2*pagSize.cy;
pDC->TextOut (point.x,point.y,s,s.GetLength()); // Skip Cr/Lf

pDC->SelectObject(pold); 

// resto del report
pDC->SetTextAlign(TA_LEFT | TA_TOP);

reportTest(pDC,rcBounds);

}


void CSMService::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
// TODO: Add your specialized code here and/or call the base class
CView::OnEndPrinting(pDC, pInfo);
CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->setPrinting(FALSE);
}

void CSMService::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
// TODO: Add your specialized code here and/or call the base class
CView::OnEndPrintPreview(pDC, pInfo, point, pView);
CMainFrame *pFrame = (CMainFrame*)AfxGetMainWnd();
pFrame->setPrinting(FALSE);
}


CRect CSMService::calcPrintInternalRect(CDC* pdc,CRect rcBounds,CRect* border )
{

CRect rectB;
// GetClientRect

CSize printSize,trueSize;
// in stampa tolgo bordi
// pixel
printSize.cx = pdc->GetDeviceCaps(HORZRES);
printSize.cy = pdc->GetDeviceCaps(VERTRES);

// millimetri
trueSize.cx = pdc->GetDeviceCaps(HORZSIZE);
trueSize.cy = pdc->GetDeviceCaps(VERTSIZE);

// calcolo px per mm
CSize pixel;
pixel.cy = printSize.cy/trueSize.cy;
pixel.cx = printSize.cx/trueSize.cx;

/* Tolgo 2 cm a destra ed 1 a sx */
rcBounds.left += c_leftMarg * pixel.cx;
rcBounds.right -= c_rightMarg * pixel.cx;
/* Tolgo 3 cm in alto e 2 in basso */	
rcBounds.top += c_topMarg * pixel.cy;
rcBounds.bottom -= c_bottomMarg * pixel.cy;

CRect rect (rcBounds);

if (border != NULL)
	*border = rcBounds;

// determino interno per scrittura e grafica
// 5 millimetri in meno
rect.DeflateRect (5 * pixel.cx,5 * pixel.cy);



return(rect);
}



void CSMService::reportTest(CDC* pDC,CRect rcBounds)
{
CCSMServiceDoc* pDoc = (CCSMServiceDoc*) GetDocument();
// TODO: add draw code here
pDC->SaveDC();

// intestazione report
// provo con CPage
CPage*	ps= new CPage(rcBounds,pDC,FALSE,MM_TEXT);

double bTop,bLeft,bRight,bBottom;

bTop = ps->ConvertYToInches(rcBounds.top);
bLeft = ps->ConvertXToInches(rcBounds.left);
bBottom = ps->ConvertYToInches(rcBounds.bottom);
bRight = ps->ConvertXToInches(rcBounds.right);

// Intestazione
//--------------
	TABLEHEADER* pTable1 = NULL;        
	pTable1=new TABLEHEADER;  
	pTable1->SetSkip=TRUE;	// no fill 
	pTable1->PointSize=24;
	pTable1->LineSize=1;    // default shown only for demp purposes
	pTable1->NumPrintLines=1;
	pTable1->UseInches=TRUE;
	pTable1->AutoSize=FALSE;
	pTable1->Border=TRUE;
	pTable1->FillFlag=FILL_LTGRAY;
	pTable1->NumColumns=1;
	pTable1->NumRows = 0;
	pTable1->StartRow=bTop;
	pTable1->StartCol=bLeft;
	pTable1->EndCol=bRight;
	pTable1->HeaderLines=1;
	// Intestazione: 
	//pTable1->ColDesc[0].Init((bRight-bLeft),"OFFLINE TEST REPORT",FILL_NONE);
	CString str;
	str.LoadString(SMSERVICE_REPORT_TITLE);
	pTable1->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
	ps->setRealPrint(TRUE);
	ps->Table(pTable1);	


//-----------------------------------------------------------------------------
// Intestazione TopLeft
	TABLEHEADER* pTable2 = NULL;        
	pTable2=new TABLEHEADER;  
	pTable2->SetSkip=TRUE;	// no fill 
	pTable2->PointSize=14;
	pTable2->LineSize=1;    // default shown only for demo purposes
	pTable2->NumPrintLines=1;
	pTable2->UseInches=TRUE;
	pTable2->AutoSize=TRUE;
	pTable2->Border=TRUE;
	pTable2->VLines = FALSE;	//	true draw vertical seperator lines
	pTable2->HLines = TRUE;    // ditto on horizontal lines
	pTable2->FillFlag=FILL_LTGRAY;
	pTable2->NumColumns=1;
	pTable2->NumRows = 0;
	pTable2->StartRow=pTable1->EndRow+0.3;
	pTable2->StartCol=bLeft;
	pTable2->EndCol=bRight;
	pTable2->NoHeader=FALSE;
	pTable2->HeaderLines=1;
	ps->setRealPrint(TRUE);
	// str = "Head #1 Left : ";
	str.LoadString (SMSERVICE_REPORT_HEAD_1_LEFT);
	BOOL failure = 0;
	BOOL slow = 0;
	CString failList;
	BOOL invalid=0;
	for (int i=0;i<m_laserSxTop.getNumLaser();i++)
		{
		switch(m_laserSxTop.c_laserSt[i].code)
			{
			case LASER_OK:
				break;
			case LASER_SLOW:
				// failList = "Blind Hole or disk low speed";
				failList.LoadString(SMSERVICE_BLIND_HOLE);
				slow = 1;
				break;
			case LASER_UNK:
				// failList = " TEST NOT EXECUTED ";
				if (failList.GetLength() == 0)
					failList.LoadString(SMSERVICE_TEST_NOT_EXECUTED);
				invalid = 1;
				break;
			default:
				if ((!slow)&&(!invalid))
					{
					CString s;
					s.Format("%3d ",m_laserSxTop.getBaseLaserNumber()+i);
					failList += s;
					failure ++;
					}
			}
		}
	if (failure||invalid||slow)
		{
		CString s;
		//str += "FAIL";
		s.LoadString(SMSERVICE_FAIL);
		str += s;
		}
	else
		str += "OK";
	pTable2->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
	ps->Table(pTable2);

	// Top Left
	// Tabella 
#define CELL_X_ROW	20
	// calcolo numero di righe necessarie
	int numRows = slow ? 1 : (1 + failure/CELL_X_ROW);
	TABLEHEADER* pTable3 = NULL;        
	pTable3=new TABLEHEADER;  
	pTable3->SetSkip=TRUE;	// no fill 
	pTable3->PointSize=11;
	pTable3->LineSize=1;    // default shown only for demp purposes
	pTable3->NumPrintLines=1;
	pTable3->UseInches=TRUE;
	pTable3->AutoSize=FALSE;
	pTable3->Border=TRUE;
	pTable3->VLines = TRUE;	//	true draw vertical seperator lines
	pTable3->HLines = TRUE;    // ditto on horizontal lines
	pTable3->FillFlag=FILL_NONE;
	pTable3->NumColumns=2;
	pTable3->NumRows = numRows;
	pTable3->StartRow= pTable2->EndRow;  // +0.1
	pTable3->StartCol=bLeft;
	pTable3->EndCol=bRight;
	pTable3->NoHeader = FALSE;
	pTable3->HeaderLines=1;
	ps->setRealPrint(TRUE);
	// intestazioni tabella  
	//str = "Total Cells";
	str.LoadString(SMSERVICE_REPORT_TOTAL_CELL);
	pTable3->ColDesc[0].Init((bRight-bLeft)/5,str,FILL_NONE);
	//str = "Cells failure";
	str.LoadString(SMSERVICE_REPORT_CELL_FAILURE);
	pTable3->ColDesc[1].Init((bRight-bLeft)*4/5,str,FILL_NONE);
	ps->Table(pTable3);	

	// valori tabella
	str.Format("%d",m_laserSxTop.getNumLaser());
	ps->Print(pTable3,0,0,16,TEXT_CENTER,(LPCSTR)str);
	for (int i=0;i<numRows;i++)
		{
		str = failList.Mid(i*CELL_X_ROW*4,CELL_X_ROW*4);
		ps->Print(pTable3,i,1,16,TEXT_LEFT,(LPCSTR)str);	
		}
//-----------------------------------------------------------------------------
// Intestazione TopRight

TABLEHEADER* pTable4 = NULL;        
TABLEHEADER* pTable5 = NULL;
TABLEHEADER* pTable6 = NULL;
TABLEHEADER* pTable7 = NULL;
TABLEHEADER* pTable8 = NULL;
TABLEHEADER* pTable9 = NULL;

if (c_splitHead)
	{
	pTable4=new TABLEHEADER;  
	pTable4->SetSkip=TRUE;	//  fill 
	pTable4->PointSize=14;
	pTable4->LineSize=1;    // default shown only for demo purposes
	pTable4->NumPrintLines=1;
	pTable4->UseInches=TRUE;
	pTable4->AutoSize=TRUE;
	pTable4->Border=TRUE;
	pTable4->VLines = FALSE;	//	true draw vertical seperator lines
	pTable4->HLines = TRUE;    // ditto on horizontal lines
	pTable4->FillFlag=FILL_LTGRAY;
	pTable4->NumColumns=1;
	pTable4->NumRows = 0;
	pTable4->StartRow=pTable3->EndRow+0.3;
	pTable4->StartCol=bLeft;
	pTable4->EndCol=bRight;
	pTable4->NoHeader=FALSE;
	pTable4->HeaderLines=1;
	ps->setRealPrint(TRUE);
	//str = "Head #1 Right : ";
	str.LoadString (SMSERVICE_REPORT_HEAD_1_RIGHT);
	failure = 0;
	slow = 0;
	failList="";
	invalid=0;
	for (int i=0;i<m_laserDxTop.getNumLaser();i++)
		{
		switch(m_laserDxTop.c_laserSt[i].code)
			{
			case LASER_OK:
				break;
			case LASER_SLOW:
				//failList = "Blind Hole or disk low speed";
				failList.LoadString(SMSERVICE_BLIND_HOLE);
				slow = 1;
				break;
			case LASER_UNK:
				//failList = " TEST NOT EXECUTED ";
				if (failList.GetLength() == 0)
					failList.LoadString(SMSERVICE_TEST_NOT_EXECUTED);
				invalid = 1;
				break;
			default:
				if ((!slow)&&(!invalid))
					{
					CString s;
					s.Format("%3d ",m_laserDxTop.getBaseLaserNumber()+i);
					failList += s;
					failure ++;
					}
			}
		}
	if (failure||invalid||slow)
		{
		CString s;
		//str += "FAIL";
		s.LoadString(SMSERVICE_FAIL);
		str += s;
		}
	else
		str += "OK";
	pTable4->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
	ps->Table(pTable4);

	// Top Right
	// calcolo numero di righe necessarie
	numRows = slow ? 1 : (1 + failure/CELL_X_ROW);
	// Tabella 
	pTable5=new TABLEHEADER;  
	pTable5->SetSkip=TRUE;	// no fill 
	pTable5->PointSize=11;
	pTable5->LineSize=1;    // default shown only for demp purposes
	pTable5->NumPrintLines=1;
	pTable5->UseInches=TRUE;
	pTable5->AutoSize=FALSE;
	pTable5->Border=TRUE;
	pTable5->VLines = TRUE;	//	true draw vertical seperator lines
	pTable5->HLines = TRUE;    // ditto on horizontal lines
	pTable5->FillFlag=FILL_NONE;
	pTable5->NumColumns=2;
	pTable5->NumRows = numRows;
	pTable5->StartRow= pTable4->EndRow;  // +0.1
	pTable5->StartCol=bLeft;
	pTable5->EndCol=bRight;
	pTable5->NoHeader = FALSE;
	pTable5->HeaderLines=1;
	ps->setRealPrint(TRUE);
	// intestazioni tabella 
	//str = "Total cells";
	str.LoadString(SMSERVICE_REPORT_TOTAL_CELL);
	pTable5->ColDesc[0].Init((bRight-bLeft)/5,str,FILL_NONE);
	//str = "Cells failure";
	str.LoadString(SMSERVICE_REPORT_CELL_FAILURE);
	pTable5->ColDesc[1].Init((bRight-bLeft)*4/5,str,FILL_NONE);
	ps->Table(pTable5);	

	// valori tabella
	str.Format("%d",m_laserDxTop.getNumLaser());
	ps->Print(pTable5,0,0,16,TEXT_CENTER,(LPCSTR)str);
	for (int i=0;i<numRows;i++)
		{
		str = failList.Mid(i*CELL_X_ROW*4,CELL_X_ROW*4);
		ps->Print(pTable5,i,1,16,TEXT_LEFT,(LPCSTR)str);	
		}
	}
//-----------------------------------------------------------------------------
// Intestazione BottomLeft
if (c_useBottom)
	{
	pTable6=new TABLEHEADER;  
	pTable6->SetSkip=TRUE;	// no fill 
	pTable6->PointSize=14;
	pTable6->LineSize=1;    // default shown only for demo purposes
	pTable6->NumPrintLines=1;
	pTable6->UseInches=TRUE;
	pTable6->AutoSize=TRUE;
	pTable6->Border=TRUE;
	pTable6->VLines = FALSE;	//	true draw vertical seperator lines
	pTable6->HLines = TRUE;    // ditto on horizontal lines
	pTable6->FillFlag=FILL_LTGRAY;
	pTable6->NumColumns=1;
	pTable6->NumRows = 0;
	pTable6->StartRow=pTable5->EndRow+0.3;
	pTable6->StartCol=bLeft;
	pTable6->EndCol=bRight;
	pTable6->NoHeader=FALSE;
	pTable6->HeaderLines=1;
	ps->setRealPrint(TRUE);
	//str = "Head #2 Left : ";
	str.LoadString (SMSERVICE_REPORT_HEAD_2_LEFT);
	failure = 0;
	slow = 0;
	failList="";
	invalid=0;
	for (int i=0;i<m_laserSxBottom.getNumLaser();i++)
		{
		switch(m_laserSxBottom.c_laserSt[i].code)
			{
			case LASER_OK:
				break;
			case LASER_SLOW:
				//failList = "Blind Hole or disk low speed";
				failList.LoadString(SMSERVICE_BLIND_HOLE);
				slow = 1;
				break;
			case LASER_UNK:
				//failList = " TEST NOT EXECUTED ";
				if (failList.GetLength() == 0)
					failList.LoadString(SMSERVICE_TEST_NOT_EXECUTED);
				invalid = 1;
				break;
			default:
				if ((!slow)&&(!invalid))
					{
					CString s;
					s.Format("%3d ",m_laserSxBottom.getBaseLaserNumber()+i);
					failList += s;
					failure ++;
					}
			}
		}
	if (failure||invalid||slow)
		{
		CString s;
		//str += "FAIL";
		s.LoadString(SMSERVICE_FAIL);
		str += s;
		}
	else
		str += "OK";

	pTable6->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
	ps->Table(pTable6);

	// Bottom Left
	// calcolo numero di righe necessarie
	numRows = slow ? 1 : (1 + failure/CELL_X_ROW);
	// Tabella 
	pTable7=new TABLEHEADER;  
	pTable7->SetSkip=TRUE;	// no fill 
	pTable7->PointSize=11;
	pTable7->LineSize=1;    // default shown only for demp purposes
	pTable7->NumPrintLines=1;
	pTable7->UseInches=TRUE;
	pTable7->AutoSize=FALSE;
	pTable7->Border=TRUE;
	pTable7->VLines = TRUE;	//	true draw vertical seperator lines
	pTable7->HLines = TRUE;    // ditto on horizontal lines
	pTable7->FillFlag=FILL_NONE;
	pTable7->NumColumns=2;
	pTable7->NumRows = numRows;
	pTable7->StartRow= pTable6->EndRow;  // +0.1
	pTable7->StartCol=bLeft;
	pTable7->EndCol=bRight;
	pTable7->NoHeader = FALSE;
	pTable7->HeaderLines=1;
	ps->setRealPrint(TRUE);
	// intestazioni tabella 
	//str = "Total cells";
	str.LoadString(SMSERVICE_REPORT_TOTAL_CELL);
	pTable7->ColDesc[0].Init((bRight-bLeft)/5,str,FILL_NONE);
	//str = "Cells failure";
	str.LoadString(SMSERVICE_REPORT_CELL_FAILURE);
	pTable7->ColDesc[1].Init((bRight-bLeft)*4/5,str,FILL_NONE);
	ps->Table(pTable7);	

	// valori tabella
	str.Format("%d",m_laserSxBottom.getNumLaser());
	ps->Print(pTable7,0,0,16,TEXT_CENTER,(LPCSTR)str);
	for (int i=0;i<numRows;i++)
		{
		str = failList.Mid(i*CELL_X_ROW*4,CELL_X_ROW*4);
		ps->Print(pTable7,i,1,16,TEXT_LEFT,(LPCSTR)str);	
		}
//-----------------------------------------------------------------------------
// Intestazione BottomRight
	if (c_splitHead)
		{
		pTable8=new TABLEHEADER;  
		pTable8->SetSkip=TRUE;	//  fill 
		pTable8->PointSize=14;
		pTable8->LineSize=1;    // default shown only for demo purposes
		pTable8->NumPrintLines=1;
		pTable8->UseInches=TRUE;
		pTable8->AutoSize=TRUE;
		pTable8->Border=TRUE;
		pTable8->VLines = FALSE;	//	true draw vertical seperator lines
		pTable8->HLines = TRUE;    // ditto on horizontal lines
		pTable8->FillFlag=FILL_LTGRAY;
		pTable8->NumColumns=1;
		pTable8->NumRows = 0;
		pTable8->StartRow=pTable7->EndRow+0.3;
		pTable8->StartCol=bLeft;
		pTable8->EndCol=bRight;
		pTable8->NoHeader=FALSE;
		pTable8->HeaderLines=1;
		ps->setRealPrint(TRUE);
		//str = "Head #2 Right : ";
		str.LoadString (SMSERVICE_REPORT_HEAD_2_RIGHT);
		failure = 0;
		slow = 0;
		failList="";
		invalid=0;
		for (int i=0;i<m_laserDxBottom.getNumLaser();i++)
			{
			switch(m_laserDxBottom.c_laserSt[i].code)
				{
				case LASER_OK:
					break;
				case LASER_SLOW:
					//failList = "Blind Hole or disk low speed";
					failList.LoadString(SMSERVICE_BLIND_HOLE);
					slow = 1;
					break;
				case LASER_UNK:
					//failList = " TEST NOT EXECUTED ";
					if (failList.GetLength() == 0)
						failList.LoadString(SMSERVICE_TEST_NOT_EXECUTED);
					invalid = 1;
					break;
				default:
					if ((!slow)&&(!invalid))
						{
						CString s;
						s.Format("%3d ",m_laserDxBottom.getBaseLaserNumber()+i);
						failList += s;
						failure ++;
						}
				}
			}
		if (failure||invalid||slow)
			{
			CString s;
			//str += "FAIL";
			s.LoadString(SMSERVICE_FAIL);
			str += s;
			}
		else
			str += "OK";

		pTable8->ColDesc[0].Init((bRight-bLeft),str,FILL_NONE);
		ps->Table(pTable8);

		// Bottom Right
		// calcolo numero di righe necessarie
		numRows = slow ? 1 : (1 + failure/CELL_X_ROW);
		// Tabella 
		pTable9=new TABLEHEADER;  
		pTable9->SetSkip=TRUE;	// no fill 
		pTable9->PointSize=11;
		pTable9->LineSize=1;    // default shown only for demp purposes
		pTable9->NumPrintLines=1;
		pTable9->UseInches=TRUE;
		pTable9->AutoSize=FALSE;
		pTable9->Border=TRUE;
		pTable9->VLines = TRUE;	//	true draw vertical seperator lines
		pTable9->HLines = TRUE;    // ditto on horizontal lines
		pTable9->FillFlag=FILL_NONE;
		pTable9->NumColumns=2;
		pTable9->NumRows = numRows;
		pTable9->StartRow= pTable8->EndRow;  // +0.1
		pTable9->StartCol=bLeft;
		pTable9->EndCol=bRight;
		pTable9->NoHeader = FALSE;
		pTable9->HeaderLines=1;
		ps->setRealPrint(TRUE);
		// intestazioni tabella 
		//str = "Total cells";
		str.LoadString(SMSERVICE_REPORT_TOTAL_CELL);
		pTable9->ColDesc[0].Init((bRight-bLeft)/5,str,FILL_NONE);
		//str = "Cells failure id";
		str.LoadString(SMSERVICE_REPORT_CELL_FAILURE);
		pTable9->ColDesc[1].Init((bRight-bLeft)*4/5,str,FILL_NONE);
		ps->Table(pTable9);

		// valori tabella
		str.Format("%d",m_laserDxBottom.getNumLaser());
		ps->Print(pTable9,0,0,16,TEXT_CENTER,(LPCSTR)str);
		for (int i=0;i<numRows;i++)
			{
			str = failList.Mid(i*CELL_X_ROW*4,CELL_X_ROW*4);
			ps->Print(pTable9,i,1,16,TEXT_LEFT,(LPCSTR)str);	
			}
		}
	}
if(pTable1 != NULL)
	delete pTable1;
if(pTable2 != NULL)
	delete pTable2;
if(pTable3 != NULL)
	delete pTable3;
if(pTable4 != NULL)
	delete pTable4;
if(pTable5 != NULL)
	delete pTable5;
if(pTable6 != NULL)
	delete pTable6;
if(pTable7 != NULL)
	delete pTable7;
if(pTable8 != NULL)
	delete pTable8;
if(pTable9 != NULL)
	delete pTable9;
	delete ps;

///---------------	
}


//------------------------------------
//
//	Gestione Messagi tipo c_stato HDE
//
//------------------------------------
 
void CSMService::gestValStatoHde (PVOID p,DWORD size) 
// Gestisce arrivo VAL_STATO
{

if (size != 1)
	{// errore dimensione 
	return;
	}

int nuovoStato;
nuovoStato = (int ) *((unsigned char *) p);
	
c_statoHde = nuovoStato;
}


void CSMService::OnUpdateFilePrint(CCmdUI *pCmdUI)
{
// TODO: Add your command update UI handler code here

	pCmdUI->Enable(this->c_sideTest == 0);
}


void CSMService::OnUpdateFilePrintPreview(CCmdUI *pCmdUI)
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable(this->c_sideTest == 0);
}
