// RpcMClient.h : header file
//
//
// USO:
// Bisogna derivare CMainFrame da CRcpClient
// Implementare virtual serverRequest in CMainFrame
// RpcClient e` sviluppato per singola connessione
// Realizzato per connessioni multiple CRpcMClient 
// Inserito mittente in server request
/////////////////////////////////////////////////////////////////////////////
// CRpcMClient window

#ifndef CRPCMCLIENT
#define CRPCMCLIENT

#include "C:\User\eds\RpcServer\RpcNet.h"
#include "../System.h"

struct Command
{
int cmd;
int size;
void *data;
};


class CRpcMClient 
{
// Construction
public:
	virtual BOOL serverRequest (int cmd,int sender,int size,void *pter);
	CRpcMClient();

// Attributes
public:
// Stato sistema
BOOL c_rpcOffLine; // Funzionamento senza connessione

BOOL c_serMsgReceived;	 // MainFrame override this var 
// Operations
public:
void rpcRegister(CString& wndName,int thisId,RPCTYPE type=TYPE_DMCS);
BOOL RegisterClient (CString &nome,RPCID id,int frameSize=4,BOOL abortNotify=FALSE);
LRESULT OnServerRequest(WPARAM wParam, LPARAM lParam);
BOOL SendGeneralCommand(Command *command,RPCID remId);
BOOL SendBroadcastGeneralCommand(Command *command);


void setProcServer(CString s){c_procServer=s;};
void setDirServer(CString s){c_dirServer=s;};
void addRemote(int remoteId,CString &device);
int getThisRpcId (void) {return (c_thisRpcId);};

// Implementation
public:
	virtual ~CRpcMClient();

private:
	CString c_procServer;
	CString c_dirServer;
	CString c_thisWnd;
	RPCTYPE c_type;
	int c_thisRpcId;
	struct Remote
		{int id;
		CString device;};
	// Remote Info
	CArray <Remote,Remote &> c_remoteRpcId; 


protected:
	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);

};


#endif
/////////////////////////////////////////////////////////////////////////////
